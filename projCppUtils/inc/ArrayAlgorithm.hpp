/**
 * @author hapi01
 */

#include "Array.hpp"

namespace utils {
  void scal(Array<double>& x, double c);
  void daxpy(const Array<double>& x, double a, Array<double>& y);
}
