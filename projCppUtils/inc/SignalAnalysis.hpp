/**  @file SignalAnalysis.hpp
*    @brief header declaring SignalAnalysis class. 
*    This class is required for e.g. the adaptation of the control grid in DyOS
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 01.06.2012
*/

#pragma once

#include <vector>
#include <string>

/** 
* @enum LayerDirection
* @brief enumeration of possible directions of wavelet layers that should be considered
*/
enum LayerDirection
{
  HorizontalLayer,
  VerticalLayer,
  AllLayers
};

/** @class SignalAnalysis
  * @brief Applies a signal analysis based on wavelets
**/
class SignalAnalysis
{
protected:
  //! m_refinedGrid is the refined time grid after the wavelet analysis
  std::vector<double> m_refinedGrid;
  //! m_refinedValues are the values correspoding to the refined grid m_refinedGrid
  std::vector<double> m_refinedValues;
  //! m_type order of the control piecewise linear/constant = 1; linear = 2
  int m_type;
  //! m_maxRefinementLevel maximum refinement level which can be processed. e.g. for m_maxRefinementLevel = 10, we can have up to 2^10 grid points.
  int m_maxRefinementLevel;
  //! m_minRefinementLevel minimum scale that is returned
  int m_minRefinementLevel;
  //! m_thresholdedIndices contains all thresholded wavelet indices that were eliminated through the refinement.
  //! It can also contain previously eliminated wavelet indices.
  std::vector<int> m_thresholdedIndices;

  //! minimum mesh size in the time domain 1/(2^m_maxRefinementLevel)
  double m_hmin;
  //! maximimum number of grid points
  int m_nmax;
  //! minimum number of grid points
  int m_nmin;

  //! grid denominator; I don't know what it is. Variable taken from old dyos. faas
  int m_gridratdenom;
  //! vertical refinement depth
  int m_verticalDepth;
  //! horizontal refinement depth
  int m_horizontalDepth;

  //! m_epsilon Constant associated to the eliminateWavelets function which eliminate wavelet coefficients under this constant
  double m_epsilon;
  //! m_etres constant that marks wavelets for elimination.
  double m_etres;

  //! m_upperBound is the upper bound of the signal in the time domain
  double m_upperBound;
  //! m_lowerBound is the lower bound of the signal in the time domain
  double m_lowerBound;

  std::vector<double> m_inputCoef;
  std::vector<int> m_inputInd;

// protected members and functions
  void setDimensions();
  std::vector<double> transformTimeToWaveletScale(const std::vector<double> &roundedGrid,
                                                  const std::vector<double> &roundedValues)const;

  std::vector<int> fillToMinimumGrid(const std::vector<int>& waveletIndices)const; //F2MG
  std::vector<int> eliminateWavelets(std::vector<double>& waveCoefficients,
                                     const std::vector<int>& waveIndices); //ELNOSIG

  std::vector<double> roundOntoWaveletGrid(const std::vector<double>& timeGrid)const;
  std::vector<int> gridPointsToIndices(const std::vector<double>& timeGrid)const;
  int gridPointsToIndicesHaar(const int gridratnum)const;
  int gridPointsToIndicesHut(const int gridratnum)const;
  std::vector<double> indicesToGridPoints(const std::vector<int>& waveletIndices)const; 
  std::vector<double> indicesToGridPointsHaar(const std::vector<int>& waveletIndices)const;
  std::vector<double> indicesToGridPointsHut(const std::vector<int>& waveletIndices)const;

  std::vector<int> uniteNeighbors(const std::vector<int>& waveletIndices,
                                  const std::vector<int>& neighborIndices)const; //UVECT
  std::vector<int> getBoundaryIndices(const std::vector<int>& originalWavelets,
                                      const std::vector<int>& thresholdWavelets)const; // RANDWERTE
  //void getUniStepIndexSet(); //get_unistep_indexset
  std::vector<int> getNeighborsForSeveralLayers(const std::vector<int>& currentLayer)const; //NTREEHV
  std::vector<int> getNextLayerIndices(const std::vector<int>& currentLayer,
                                       const LayerDirection direction)const; //get_nextlay

  std::vector<int> getDirectNeighbors(const int index)const;

  std::vector<int> getVerticalNeighbors(const int index)const;
  std::vector<int> getVerticalNeighborsHaar(const int index)const;
  std::vector<int> getVerticalNeighborsHut(const int index)const;

  std::vector<int> getHorizontalNeighbors(const int index)const;
  std::vector<int> getHorizontalNeighborsHaar(const int index)const;
  std::vector<int> getHorizontalNeighborsHut(const int index)const;

  void approximateSignal(const std::vector<double> &inputCoefficients,
                         const std::vector<int> &inputIndices,
                         const int nnzra,
                         std::vector<double> &coefficients,
                         std::vector<int> &indices)const;
  std::vector<int> getSignificantWavelets(const std::vector<int>& controlWaveletIndexSetKeep,
                                          const std::vector<int>& controlWaveletIndexSetOriginal,
                                          const std::vector<int>& controlWaveletIndexSet)const;
  void setRefinedGridandValues(const std::vector<int>& significantWavelets,
                               const std::vector<double>& roundedGrid,
                               const std::vector<double>& roundedValues);
  int getNumNonZeroEntriesInBoundary(const std::vector<double>& multiScaleVals,
                                     const std::vector<int>& boundaryWavelets)const;
  // utility functions
  std::vector<double> createFinestMesh()const;
public:
  /** @brief empty standard constructor */
  SignalAnalysis(){};
  SignalAnalysis(const int type,
                 const double upperBound,
                 const double lowerBound,
                 const std::vector<int> thresholdedIndices,
                 const int maxRefinementLevel = 10,
                 const int minRefinementLevel = 3,
                 const int horRefinementDepth = 1,
                 const int verRefinementDepth = 1,
                 const double etres = 1.0e-8,
                 const double epsilon = 0.9);

  void obtainNewMesh(const std::vector<double> timeGrid,
                     const std::vector<double> timeValues);

  std::vector<double> getRefinedGrid()const;
  std::vector<double> getRefinedValues()const;
  void printWavelet(std::string fileName)const;
  int getType()const{return m_type;};
};

