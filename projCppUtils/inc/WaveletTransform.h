/* ===========================================================*/
/* DyOS - Tool for dynamic optimization                       */
/* ===========================================================*/
/* File:  wavelet_transform.h                                 */
/* Purpose: header file for module wavelet_transform.c        */
/* Author: Klaus Stockmann                                    */
/* Copyright:                                                 */
/* Prof. Dr.-Ing. W. Marquardt                                */
/* Lehrstuhl fuer Prozesstechnik,                             */
/* RWTH Aachen                                                */
/* -----------------------------------------------------------*/

#ifndef WAVELET_TRANSFORM_H
#define WAVELET_TRANSFORM_H

#ifdef __cplusplus
extern "C" {
#endif
/* haar wavelets */
void haar_fwt(double a[], int n, int isign);

/* hat wavelets */
void hat_fwt(double *signal, int n, int isign);
#ifdef __cplusplus
}
#endif

#endif //WAVELET_TRANSFORM_H
