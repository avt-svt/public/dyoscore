/**
* @file Loader.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* This class  holds a handle to a given dll \n
* derived classes can load functions of the loaded dll\n
* for each dll to be loaded by a loader an own class should be derived \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 18.7.2011
*/

#pragma once

#ifdef __cplusplus
extern "C"{
#endif
#ifdef WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif
#ifdef __cplusplus
}
#endif
#include <string>
#include <iostream>
#include <exception>
#include <stdexcept>


/**
* @brief namespace for all general utility function and classes
*/
namespace utils
{
  /**
  * @class DllLoaderException
  * @brief exception class for failing to load an dll or one of its functions
  */
  class DllLoaderException: public std::runtime_error
  {
  public:
    /**
    * @brief standard constructor
    *
    * Will generate error message "Error while loading a dll"
    */
    DllLoaderException():std::runtime_error("Error while loading a dll"){};
    /**
    * @brief constructor
    * @param message error message
    */
    DllLoaderException(std::string message_):std::runtime_error(message_.c_str()){};

    virtual ~DllLoaderException() throw() {}

  };

  /**
  * class for loading a dll dynamically and  load any functions of that dll
  */
  class Loader{
  protected:
    //! handle for the loaded dll
#ifdef WIN32
	  HMODULE handle;
#else
    void* handle;
#endif

	  virtual void init();
      void loadDLL(const std::string &);
      void unloadDLL();

  public:
      Loader();
      Loader(const std::string &);
      Loader(const Loader &l);
      virtual ~Loader();
    //! name of the loaded dll
	  std::string dllName;
	  bool isLoaded();

  //templates

  protected:

      /**
      * @brief template function to load a function of type T from the dll held by the class
      *
      * @param functionPointer function pointer of type T for the function to be loaded
      * @param functionName name of the function entry point
      */
      template<class FunctionType>
      void loadFcn(FunctionType &functionPointer, const std::string &functionName)
      {
        if(!isLoaded()){
          std::string message = "dll " + dllName + " not loaded!";
          throw DllLoaderException(message);
        }
        else if(functionPointer == NULL){
#ifdef WIN32
          functionPointer = (FunctionType)GetProcAddress(handle, functionName.c_str());
#else
          functionPointer = (FunctionType)dlsym(handle, functionName.c_str());
#endif
          if(functionPointer == NULL){
            std::string message = "could not load function " + functionName + " from " + dllName + "\n";
#ifdef WIN32
            message += "Errorcode: " + GetLastError();
#else
            std::string dl_error(dlerror());
            message += "Errorcode: " + dl_error;
#endif
            throw DllLoaderException(message);
          }
        }
      }
  };

}//namespace utils
