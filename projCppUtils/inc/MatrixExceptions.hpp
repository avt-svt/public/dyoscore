/** @file MatrixExceptions.hpp
*    @brief header declaring exception class BadIndex
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Klaus Stockmann, Tjalf Hoffmann
*   @date 05.12.2012                                       
*/
#pragma once
#include <string>
#include <sstream> 

class MatrixException : public std::exception {
protected:
  std::string m_msg;
public:
  MatrixException(const std::string& msg) : m_msg("MatrixException: " + msg) {}
  virtual ~MatrixException() throw() {}
  virtual const char* what() const throw() {
    return m_msg.c_str();
  }
};


class SingularMatrixException : public MatrixException
{
public:
  SingularMatrixException() :
    MatrixException("Matrix is singular") {}
  SingularMatrixException(const std::string &msg) : MatrixException(msg) {}
};

class EntryNotInSparseStructureException : public MatrixException
{
public:
  EntryNotInSparseStructureException() :
    MatrixException("The given point could not be found in the sparsity structure") {}
  EntryNotInSparseStructureException(const int rowIndex, const int colIndex)
    : MatrixException("The given point could not be found in the sparsity structure")
  {
    std::stringstream sstream;
    sstream << ":(" << rowIndex << "," << colIndex << ")";
    m_msg.append(sstream.str());
  }
  EntryNotInSparseStructureException(const std::string &msg) : MatrixException(msg) {}
};


//! @brief utility class for catching out of bound access
class BadIndex : public MatrixException
{
public:
  explicit BadIndex(std::string const& message) : MatrixException(message)
  {}
};

class MatrixNotInitializedException : public MatrixException
{
public:
  MatrixNotInitializedException() :
    MatrixException("Matrix has not been properly initialized (cs struct is Null pointer") {}
  MatrixNotInitializedException(const std::string &msg) : MatrixException(msg) {}
};

class MatricesNotCompatibleException : public MatrixException
{
public:
  MatricesNotCompatibleException() :
    MatrixException("Operation not possible as matrices have not compatible dimensions") {}
  MatricesNotCompatibleException(const std::string &msg) : MatrixException(msg) {}
};