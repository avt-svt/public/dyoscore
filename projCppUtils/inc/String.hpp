/** 
* @file String.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* string utilities not contained in std::string                        \n
* =====================================================================\n 
* @author Tjalf Hoffmann       
* @date 18.7.2011
*/


#pragma once
#include <string>

namespace utils
{

  void toUpperCase(std::string &s);
  void toLowerCase(std::string &s);

  bool compareStrings(std::string first, std::string second, const bool caseSensitive=false);

}//namespace utils
