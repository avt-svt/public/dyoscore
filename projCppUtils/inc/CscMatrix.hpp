
#pragma once

#include "cs.h"
#include <boost/shared_ptr.hpp>

class CsCscMatrix
{
private:
  cs* m_mat;
  bool owner;
  CsCscMatrix();
public:
  typedef boost::shared_ptr<CsCscMatrix> Ptr;
  // if values == NULL, values are not allocated.
  //CsCscMatrix(int n_rows, int n_cols, int nnz, const int* row_idx, const int* col_idx, const int* values);
  CsCscMatrix(cs* mat, bool owner=true);
  ~CsCscMatrix();

  int get_n_rows() const {return m_mat->m;}
  int get_n_cols() const {return m_mat->n;}
  int get_nnz() const {return m_mat->p[m_mat->n];}
  
  void get_values(const int numValues, double *values) const;
  cs* get_matrix_ptr() {return m_mat;}

  bool getValue(const int rowIndex, const int colIndex, double &value) const;

  /** The solution will be written to lhs, which must be allocated by the user */
  bool solve_lu(const double* rhs, double* lhs);

  /** call cs_dmperm implementation (block-generating permutation of the matrix?) */
  csd* get_dulmange_mendelsohn_permutation(int seed);

  void horzcat_in_place(const CsCscMatrix::Ptr &right);
  
  void extractColumn(const int columnIndex, double* columnValues, const int columnLength);
  
  int getMatrixRank() const;
};
