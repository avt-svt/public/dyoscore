/** @file Spline.hpp
*    @brief header declaring Spline class
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Klaus Stockmann
*   @date 08.03.2012                                       
*/
#pragma once


#include <vector>
#include <ostream>



/** @brief Spline class */
class Spline
{
protected:
  // private data members
  //! @brief spline order
  unsigned int m_order;
  std::vector<double> m_t;
  //! @brief values at timepoints t
  std::vector<double> m_x;
  //! @brief (extended) knot sequence
  std::vector<double> m_knots;

  //! @brief spline coefficients (size depends on order: t.size()-1 for order==1, t.size() for order==2)
  std::vector<double> m_d;

  // private methods
  double basisSplineW(const double t, 
                      const unsigned i, 
                      const unsigned k, 
                      const std::vector<double> &knots);

  double basisSpline(const double t, const unsigned i, const unsigned k, const std::vector<double> &knots);  
  std::vector<double>& getSplineCoefficients(const std::vector<double> &t, const std::vector<double> &x, const unsigned order);

  void assertSplineInput(const std::vector<double> &t, const std::vector<double> &x, const unsigned order);
  void initializeSpline(const std::vector<double> &t, const std::vector<double> &x, const unsigned order);
public:
  //! @brief standard constructor
  Spline(){};

  //! @brief constructor
  Spline(const std::vector<double> &t, std::vector<double> &x, const unsigned order);
  //! @brief copy constructor
  Spline(const Spline &splineIn);
  
  //! @brief destructor
  ~Spline();

  double eval(const double t);
  double dSpline(const double tp, const unsigned splineIndex);
};

