/** @file DenseMatrix.hpp
* @brief header declaring DenseMatrix class and utility class BadIndex
* 
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* Since the DenseMatrix class is not used, this module also contains   \n
* some utility functions for dense matrices (stored as vector of       \n
* vectors). The DenseMatrix class is for now deactivated               \n
* =====================================================================\n
* U
* @author Klaus Stockmann, Mathias Dunst, Tjalf Hoffmann
* @date 12.11.2013                                       
*/
#pragma once


#include <vector>
#include "CscMatrix.hpp"
#include "TripletMatrix.hpp"


void convertSparseMatrixToFull(CsCscMatrix::Ptr &sparseMatrix,
                                std::vector<std::vector<double> > &fullMatrix);
void convertSparseMatrixToFull(CsTripletMatrix::Ptr &sparseMatrix,
                               std::vector<std::vector<double> > &fullMatrix);

std::vector<std::vector<double> > multiply(std::vector<std::vector<double> > &matrix1,
                                           std::vector<std::vector<double> > &matrix2);

std::vector<std::vector<double> > invertMatrix(std::vector<std::vector<double> > &matrix);

void luDecomposition(std::vector<std::vector<double> > &matrix, std::vector<int> &index);
void luBackSubstitution(std::vector<std::vector<double> > matrix, const std::vector<int> &index, std::vector<double> &b);
//dense matrix class was part of a matrix class concept developed by Klaus Stockmann and Mathias Dunst
//the aim of the concept was to give dense and sparse matrices a common interface. However the result
//of this concept was not satisfying, so we used simple wrapper classes for cs_sparse matrix pointers.
//#include "MatrixExceptions.hpp"
//#include "Matrix.hpp"
/** @brief dense matrix class */
/*class DenseMatrix : public Matrix
{
private:
  unsigned m_rowDim;
  unsigned m_colDim;
  double *m_vals;

  double *allocMatrix(unsigned n);  

public:
  //! @brief default constructor creates a matrix of size 0 
  DenseMatrix();
  //! @brief constructor
  DenseMatrix(const unsigned rowDim, const unsigned colDim);
  //! @brief copy constructor
  DenseMatrix(const DenseMatrix &matrixIn);

  //! @brief destructor
  ~DenseMatrix();

  //! @brief scalar access operator (mutable)
  double& operator() (const unsigned rowInd, const unsigned colInd);

  //! @brief scalar access operator (immutable)
  double operator() (const unsigned rowInd, const unsigned colInd) const;

  //! @brief set matrix A into a DenseMatrix at positions specified by row and column index vectors
  void setMatrixSlices(const std::vector<unsigned> &rowIndVec, const std::vector<unsigned> &colIndVec, Matrix::Ptr &matrixIn);

  //! @brief get matrix A out of matrix B specified by row and column index vectors
  Matrix::Ptr getMatrixSlices(const std::vector<unsigned> &rowIndVec, const std::vector<unsigned> &colIndVec) const;

  //! @brief get row dimension
  unsigned getRowDim();

  //! @brief get column dimension
  unsigned getColDim();

  //! @brief assignment operator for making a deep copy of a matrix
  Matrix::Ptr& operator=(const Matrix::Ptr& matrixIn);

  //! @brief get vector of values, row indices and column pointer of SparseMatrix
  // Remark: this method is not used in DenseMatrix
  std::vector<double> assignSparseMatrix(std::vector<int> &rowIndVec, std::vector<int> &colIndVec, std::string format){std::vector<double> null(0); return null;};

  //! @brief resize of matrix
  void resize(const unsigned rowSize, const unsigned colSize);

  //! @brief transpose of matrix A^T
  Matrix::Ptr transpose();

  //! @brief compute eigenvalue of matrix
  std::vector<double> eig();

  //! @brief compute inverse of matrix
  Matrix::Ptr inverse();

  //! @brief solves a system of linear equations Ax=b
  std::vector<double> solve(std::vector<double> vectorIn);

  //! @brief return number of nonzeros of DenseMatrix
  int getNNZ();

  //! @brief print Matrix
  std::string print();

  //! @brief addition operator for two matrices A = A + B
  Matrix::Ptr operator+(const Matrix::Ptr A);

  //! @brief subtract operator for two matrices A = A - B
  Matrix::Ptr operator-(const Matrix::Ptr A);

  //! @brief multiply operator for scalar multiplication with matrix A = a * A
  Matrix::Ptr operator*(const double value);

  //! @brief multiply operator for matrix - vector multiplication A = vector * A
  std::vector<double> operator*(const std::vector<double> vectorIn);

  //! @brief multiply operator for matrix multiplication A = A * B
  Matrix::Ptr operator*(const Matrix::Ptr A);


	csd *dmperm_wrapper(int seed){return NULL;};

};*/

