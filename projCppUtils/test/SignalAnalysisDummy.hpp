/** @file SignalAnalysisDummy.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Dummy class that makes all functions public
* =====================================================================\n
* @author Fady Assassa
* @date 01.06.2012
*/

#pragma once
#include "SignalAnalysis.hpp"


/** @class SignalAnalysisDummy 
*   @brief sets all protected functions to public
*/

using namespace std;

class SignalAnalysisDummy : public SignalAnalysis
{
public:
  /** @brief does nothing */
  SignalAnalysisDummy(){};
  /** @copydoc SignalAnalysis::SignalAnalysis */
  SignalAnalysisDummy(const int order,
                      const double upperBound,
                      const double lowerBound,
                      const std::vector<int> thresholdedIndices,
                      const int maxRefinementLevel = 10,
                      const int minRefinementLevel = 3,
                      const int horRefinementDepth = 1,
                      const int verRefinementDepth = 1,
                      const double etres = 1.0e-8,
                      const double epsilon = 0.9)
                      :SignalAnalysis(order, upperBound, lowerBound, thresholdedIndices, maxRefinementLevel,
                                      minRefinementLevel, horRefinementDepth, verRefinementDepth, etres, epsilon)
  {
  };

  /** @copydoc SignalAnalysis::roundOntoWaveletGrid */
  std::vector<double> roundOntoWaveletGridPublic(const vector<double>& timeGrid){
    return SignalAnalysis::roundOntoWaveletGrid(timeGrid);
  };
  /** @copydoc SignalAnalysis::gridPointsToIndices */
  std::vector<int> gridPointsToIndicesPublic(const vector<double> timeGrid){
    return SignalAnalysis::gridPointsToIndices(timeGrid);
  };
  /** @copydoc SignalAnalysis::indicesToGridPoints */
  std::vector<double> indicesToGridPointsPublic(const vector<int> waveletIndices)
  {
    return SignalAnalysis::indicesToGridPoints(waveletIndices);
  }
  /** @copydoc SignalAnalysis::getDirectNeighbors */
  vector<int> getDirectNeighborsPublic(const int index){
    return SignalAnalysis::getDirectNeighbors(index);
  }
  /** @copydoc SignalAnalysis::getNeighborsForSeveralLayers */
  vector<int> getNeighborsForSeveralLayersPublic(const vector<int> currentLayer){
    return SignalAnalysis::getNeighborsForSeveralLayers(currentLayer);
  }
  /** @copydoc SignalAnalysis::fillToMinimumGridPublic */
  vector<int> fillToMinimumGridPublic(const vector<int> waveletIndices){
    return SignalAnalysis::fillToMinimumGrid(waveletIndices);
  }
  /** @copydoc SignalAnalysis::getBoundaryIndices */
  vector<int> getBoundaryIndicesPublic(const vector<int> originalWavelets,  // RANDWERTE
                                               const vector<int> thresholdWavelets)
  {
    return SignalAnalysis::getBoundaryIndices(originalWavelets, thresholdWavelets);
  }
  /** @copydoc SignalAnalysis::approximateSignal */
  void approximateSignalPublic(const vector<double> &inputCoefficients,
                               const vector<int> &inputIndices,
                               const int nnzra,
                               vector<double> &coefficients,
                               vector<int> &indices)
  {
    SignalAnalysis::approximateSignal(inputCoefficients, inputIndices, nnzra, coefficients, indices);
  }
  /** @copydoc SignalAnalysis::eliminateWavelets */
  vector<int> eliminateWaveletsPublic(vector<double>& waveCoefficients,
                               vector<int>& waveIndices)
  {
    return SignalAnalysis::eliminateWavelets(waveCoefficients, waveIndices);
  }
  /** @brief returns member m_thresholdedIndices
   *  @return returns member m_thresholdedIndices
   */
  vector<int> getThresholdedIndices(){
    return m_thresholdedIndices;
  }
};

