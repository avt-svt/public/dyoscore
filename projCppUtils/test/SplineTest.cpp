/** @file SplineTest.cpp
*    @brief test of Spline class
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Klaus Stockmann
*   @date 08.03.2012                                       
*/

#define BOOST_TEST_MODULE SplineTest

#include <cstdio>
#include <vector>

#include "Spline.hpp"
#include "boost/test/unit_test.hpp"

BOOST_AUTO_TEST_CASE(TestSpline)
{
  const unsigned n = 11;
  const double tarr[n] = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
  const double xarr[n] = {1.0, 2.0, 1.5, 3.0, 2.5, 1.7, 0.9, 0.8, 1.4, 0.6, 0.4};

  const unsigned order1 = 1; // piecewise constant
  const unsigned order2 = 2; // piecewise linear

  std::vector<double> t(tarr, tarr+n), x(xarr, xarr+n);

  Spline s1(t, x, order1);
  Spline s2(t, x, order2);

  Spline s3(s1);
  
  // unfortunately the original test was undocumented and did not check against a certain result
  // so for now we build up an regression test here and check the results of the function for revision 124
  const double threshold = 1e-10;
  BOOST_CHECK_CLOSE(s1.eval(0.45), 2.5, threshold);
  BOOST_CHECK_CLOSE(s2.eval(0.45), 2.1, threshold);

  // testing derivatives 
  BOOST_CHECK_CLOSE(s1.dSpline(0.45, 5), 1.0, threshold);
  BOOST_CHECK_CLOSE(s2.dSpline(0.45, 5), 0.5, threshold);
}
