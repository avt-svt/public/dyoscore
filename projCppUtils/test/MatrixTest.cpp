/** @file MatrixTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Testfile for the Matrix classes                                      \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 3.12.2013
*/

#pragma once
#define BOOST_TEST_MODULE Matrix 
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>

#include "TripletMatrixTestsuite.hpp"
#include "CscMatrixTestsuite.hpp"
#include "DenseMatrixTestsuite.hpp"