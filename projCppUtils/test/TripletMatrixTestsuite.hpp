#include "TripletMatrix.hpp"
#include "MatrixExceptions.hpp"

BOOST_AUTO_TEST_SUITE(TestTripletMatrix)


/**
* @test TestTripletMatrix - test createTripletMatrix
*
* this test checks if all Matrix constructors work correctly
* implicitly also checks get_matrix_ptr (which is a simple getter function)
*/
BOOST_AUTO_TEST_CASE(TestCreateTripletMatrix)
{
  //test empty constructor
  {
    CsTripletMatrix test;
  }
  //test construction with double fields
  {
    const int m=4, n=2, nnz=6;
    int rowIndices[nnz] = {0,1,1,2,3,3};
    int colIndices[nnz] = {0,0,1,1,0,1};
    double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
    CsTripletMatrix test(m, n, nnz, rowIndices, colIndices, values);
    cs* mat = test.get_matrix_ptr();
    BOOST_CHECK_EQUAL(m, mat->m);
    BOOST_CHECK_EQUAL(n, mat->n);
    BOOST_CHECK_EQUAL(nnz, mat->nz);
    BOOST_CHECK_EQUAL(nnz, mat->nzmax);
    for(int i=0; i<nnz; i++){
      BOOST_CHECK_EQUAL(rowIndices[i], mat->i[i]);
      BOOST_CHECK_EQUAL(colIndices[i], mat->p[i]);
      BOOST_CHECK_EQUAL(values[i], mat->x[i]);
    }
  }
  
  //test construction with cs pointer
  {
    const int m=3, n=4, nnz=5;
    int rowIndices[nnz] = {0,0,1,2,2};
    int colIndices[nnz] = {2,3,0,1,3};
    double values[nnz] = {0.5, 1.5, 2.5, 3.5, 4.5};
    cs* mat = cs_spalloc(m, n, nnz, true, true);
    mat->nz = nnz;
    for(int i=0; i<nnz; i++){
      mat->i[i] = rowIndices[i];
      mat->p[i] = colIndices[i];
      mat->x[i] = values[i];
    }
    bool owner=true;
    CsTripletMatrix test(mat,owner);
    BOOST_CHECK_EQUAL(mat, test.get_matrix_ptr());
  }
}

/**
* @test TestTripletMatrix - TestGetNumFunctions
*
* test the functions get_n_rows, get_n_cols and get_nnz
*/
BOOST_AUTO_TEST_CASE(TestGetNumFunctions)
{
  const int m=6, n=12, nnz=18;
  int rows[nnz], cols[nnz];
  double vals[nnz];
  CsTripletMatrix test(m, n, nnz, rows, cols, vals);
  BOOST_CHECK_EQUAL(test.get_n_rows(), m);
  BOOST_CHECK_EQUAL(test.get_n_cols(), n);
  BOOST_CHECK_EQUAL(test.get_nnz(), nnz);
}

/**
* @test TestTripletMatrix - test getValue
*/
BOOST_AUTO_TEST_CASE(TestGetValue)
{
  const int m=10, n=12, nnz=3;
  
  int rows[nnz] = {2,1,7};
  int cols[nnz] = {5,8,1};
  double values[nnz] = {6.2345674567, 74354.239423, 93456.05453};
  CsTripletMatrix test(m, n, nnz, rows, cols, values);
  
  BOOST_CHECK_EQUAL(test.getValue(rows[1], cols[1]), values[1]);
  BOOST_CHECK_EQUAL(test.getValue(4,3), 0.0);
}

/**
* @test TestTripletMatrix - test setValue
*
* this test assumes that function getValue works correctly
*/
BOOST_AUTO_TEST_CASE(TestSetValue)
{
  const int m=13, n=5, nnz = 3;
  int rows[nnz] = {4,7,9};
  int cols[nnz] = {2,7,2};
  double values[nnz] = {3234.234545, 23612.12333, 123535.23422};
  CsTripletMatrix test(m, n, nnz, rows, cols, values);
  const double newValue = 6.9203;
  BOOST_CHECK_NO_THROW(test.setValue(rows[1], cols[1], newValue));
  BOOST_CHECK_EQUAL(test.getValue(rows[1], cols[1]), newValue);
  BOOST_CHECK_THROW(test.setValue(4, 4, newValue), EntryNotInSparseStructureException);
}

/**
* @test TestTripletMatrix - tets transpose_in_place
*
* in this function the function getValue is assumed to be working correctly
*/
BOOST_AUTO_TEST_CASE(TestTransposeInPlace)
{
  const int m=5, n=4, nnz=5;
  int rows[nnz] = {0,1,1,2,4};
  int cols[nnz] = {0,0,2,3,3};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0};
  CsTripletMatrix test(m, n , nnz, rows, cols, values);
  test.transpose_in_place();
  
  cs* mat = test.get_matrix_ptr();
  BOOST_CHECK_EQUAL(mat->m, n);
  BOOST_CHECK_EQUAL(mat->n, m);
  BOOST_CHECK_EQUAL(mat->nz, nnz);
  for(int i=0; i<nnz; i++){
    BOOST_CHECK_EQUAL(test.getValue(cols[i], rows[i]), values[i]);
  }
}

BOOST_AUTO_TEST_CASE(TestVertCat)
{
  const int m1=2, m2=3, n=4, nnz1=4, nnz2=5;
  int rows1[nnz1] = {0,0,1,1};
  int rows2[nnz2] = {1,1,2,0,2};
  int cols1[nnz1] = {0,1,1,3};
  int cols2[nnz2] = {1,2,2,3,3};
  double values1[nnz1] = {1.1, 1.2, 1.3, 1.4};
  double values2[nnz2] = {2.1, 2.2, 2.3, 2.4, 2.5};
  
  CsTripletMatrix::Ptr matrix1(new CsTripletMatrix(m1, n, nnz1, rows1, cols1, values1));
  CsTripletMatrix::Ptr matrix2(new CsTripletMatrix(m2, n, nnz2, rows2, cols2, values2));
  CsTripletMatrix::Ptr result = matrix1->vertcat(matrix2);
  BOOST_CHECK_EQUAL(result->get_n_cols(), n);
  BOOST_CHECK_EQUAL(result->get_n_rows(), m1+m2);
  BOOST_CHECK_EQUAL(result->get_nnz(), nnz1+nnz2);
  for(int i=0; i<nnz1; i++){
    BOOST_CHECK_EQUAL(result->getValue(rows1[i], cols1[i]), values1[i]);
  }
  for(int i=0; i<nnz2; i++){
    BOOST_CHECK_EQUAL(result->getValue(rows2[i]+m1, cols2[i]), values2[i]);
  }
  
  const int m3 = 1, n3=2, nnz3 = 1;
  int rows3[nnz3] = {0};
  int cols3[nnz3] = {0};
  double values3[nnz3] = {0.0};
  CsTripletMatrix::Ptr incompatibleMatrix(new CsTripletMatrix(m3, n3, nnz3, rows3, cols3, values3));
  BOOST_CHECK_THROW(matrix1->vertcat(incompatibleMatrix), MatricesNotCompatibleException);
}

/**
* @test TestTripletMatrix - test function setSubMatrix
*/
BOOST_AUTO_TEST_CASE(TestSubMatrix)
{
  //fields of matrix must match
  const int m1=8, m2=4, n1=7, n2=5, nnz1=5, nnz2=3;
  int rows1[nnz1] = {0,3,5,5,7};
  int cols1[nnz1] = {1,2,2,4,6};
  double values1[nnz1] = {1.1, 1.2, 1.3, 1.4, 1.5};
  //matrix 2 should override values 2,3,4
  int rows2[nnz2] = {0,2,2};
  int cols2[nnz2] = {0,0,2};
  double values2[nnz2] = {2.1, 2.2, 2.3};
  CsTripletMatrix::Ptr matrix1(new CsTripletMatrix(m1, n1, nnz1, rows1, cols1, values1));
  CsTripletMatrix::Ptr matrix2(new CsTripletMatrix(m2, n2, nnz2, rows2, cols2, values2));
  
  matrix1->setSubMatrix(matrix2, 3, 2);
  
  BOOST_CHECK_EQUAL(matrix1->getValue(rows1[0], cols1[0]), values1[0]);
  BOOST_CHECK_EQUAL(matrix1->getValue(rows1[nnz1-1], cols1[nnz1-1]), values1[nnz1-1]);
  for(int i=1; i<nnz1-1; i++){
    BOOST_CHECK_EQUAL(matrix1->getValue(rows1[i], cols1[i]), values2[i-1]);
  }
}

/**
* @test TestTripletMatrix - test compress (transform triplet to CsCsc)
*/
BOOST_AUTO_TEST_CASE(TestCompress)
{
  const int m=3, n=4, nnz=5;
  int rows[nnz] = {0,0,1,2,2};
  int cols[nnz] = {1,3,0,0,2};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0};
  CsTripletMatrix test(m, n, nnz, rows, cols, values);
  CsCscMatrix::Ptr result = test.compress();
  cs* mat = result->get_matrix_ptr();
  BOOST_CHECK_EQUAL(mat->m, m);
  BOOST_CHECK_EQUAL(mat->n, n);
  BOOST_CHECK_EQUAL(mat->nz, -1);
  BOOST_CHECK_EQUAL(mat->nzmax, nnz);
  
  BOOST_CHECK_EQUAL(mat->i[0], 1);
  BOOST_CHECK_EQUAL(mat->x[0], 3.0);
  
  BOOST_CHECK_EQUAL(mat->i[1], 2);
  BOOST_CHECK_EQUAL(mat->x[1], 4.0);
  
  BOOST_CHECK_EQUAL(mat->i[2], 0);
  BOOST_CHECK_EQUAL(mat->x[2], 1.0);
  
  BOOST_CHECK_EQUAL(mat->i[3], 2);
  BOOST_CHECK_EQUAL(mat->x[3], 5.0);
  
  BOOST_CHECK_EQUAL(mat->i[4], 0);
  BOOST_CHECK_EQUAL(mat->x[4], 2.0);
  
  BOOST_CHECK_EQUAL(mat->p[0], 0);
  BOOST_CHECK_EQUAL(mat->p[1], 2);
  BOOST_CHECK_EQUAL(mat->p[2], 3);
  BOOST_CHECK_EQUAL(mat->p[3], 4);
  BOOST_CHECK_EQUAL(mat->p[4], 5);
}

/**
* @test TestTripletMatrix - test function get_matrix_slices
*/
BOOST_AUTO_TEST_CASE(TestGetMatrixSlices)
{
  const int m = 5, n =5, nnz=10, numRowsSlices = 2, numColsSlices = 3;
  int rows[nnz] = {0,0,1,1,2,2,3,3,4,4};
  int cols[nnz] = {1,3,2,4,3,4,0,1,0,2};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
  int rowsSlices[numRowsSlices] = {0,3};
  int colsSlices[numColsSlices] = {1,2,4};
  CsTripletMatrix test(m, n, nnz, rows, cols, values);
  CsTripletMatrix::Ptr matrixSlices = test.get_matrix_slices(numRowsSlices, rowsSlices, numColsSlices, colsSlices);
  cs* mat = matrixSlices->get_matrix_ptr();
  
  BOOST_CHECK_EQUAL(mat->m, numRowsSlices);
  BOOST_CHECK_EQUAL(mat->n, numColsSlices);
  BOOST_CHECK_EQUAL(mat->nz, 2);
  
  BOOST_CHECK_EQUAL(mat->i[0], 0);
  BOOST_CHECK_EQUAL(mat->p[0], 0);
  BOOST_CHECK_EQUAL(mat->x[0], 1.0);
  
  BOOST_CHECK_EQUAL(mat->i[1], 1);
  BOOST_CHECK_EQUAL(mat->p[1], 0);
  BOOST_CHECK_EQUAL(mat->x[1], 8.0);
}

/**
* @test TestTripletMatrix - test functionget_matrix_slices_keep_size
*/
BOOST_AUTO_TEST_CASE(TestGetMatrixSlicesKeepSize)
{
  const int m = 5, n =5, nnz=10, numRowsSlices = 2, numColsSlices = 3;
  int rows[nnz] = {0,0,1,1,2,2,3,3,4,4};
  int cols[nnz] = {1,3,2,4,3,4,0,1,0,2};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
  int rowsSlices[numRowsSlices] = {0,3};
  int colsSlices[numColsSlices] = {1,2,4};
  CsTripletMatrix test(m, n, nnz, rows, cols, values);
  CsTripletMatrix::Ptr matrixSlices = test.get_matrix_slices_keep_size(numRowsSlices, rowsSlices, numColsSlices, colsSlices);
  cs* mat = matrixSlices->get_matrix_ptr();
  
  BOOST_CHECK_EQUAL(mat->m, m);
  BOOST_CHECK_EQUAL(mat->n, n);
  BOOST_CHECK_EQUAL(mat->nz, 2);
  
  BOOST_CHECK_EQUAL(mat->i[0], 0);
  BOOST_CHECK_EQUAL(mat->p[0], 1);
  BOOST_CHECK_EQUAL(mat->x[0], 1.0);
  
  BOOST_CHECK_EQUAL(mat->i[1], 3);
  BOOST_CHECK_EQUAL(mat->p[1], 1);
  BOOST_CHECK_EQUAL(mat->x[1], 8.0);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestSortedCsTripletMatrix)

//SortedCsTripletMatrix inherits from CsTripletMatrix, so only the overloaded functions are tested

/**
* @test TestSortedCsTripletMatrix - test creation of a SortedCsTripletMatrix object
*
* especially test whether data is sorted
*/
BOOST_AUTO_TEST_CASE(TestCreation)
{
  const int m=4, n=5, nnz=7;
  int rows[nnz] = {3,1,0,2,1,2,0};
  int cols[nnz] = {4,2,0,2,0,3,2};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
  
  SortedCsTripletMatrix test(m, n, nnz, rows, cols, values);
  cs* mat = test.get_matrix_ptr();
  BOOST_CHECK(mat != NULL);
  BOOST_CHECK_EQUAL(mat->m, m);
  BOOST_CHECK_EQUAL(mat->n, n);
  BOOST_CHECK_EQUAL(mat->nz, nnz);
  
  BOOST_CHECK_EQUAL(mat->i[0], 0);
  BOOST_CHECK_EQUAL(mat->p[0], 0);
  BOOST_CHECK_EQUAL(mat->x[0], 3.0);
  
  BOOST_CHECK_EQUAL(mat->i[1], 0);
  BOOST_CHECK_EQUAL(mat->p[1], 2);
  BOOST_CHECK_EQUAL(mat->x[1], 7.0);
  
  BOOST_CHECK_EQUAL(mat->i[2], 1);
  BOOST_CHECK_EQUAL(mat->p[2], 0);
  BOOST_CHECK_EQUAL(mat->x[2], 5.0);
  
  BOOST_CHECK_EQUAL(mat->i[3], 1);
  BOOST_CHECK_EQUAL(mat->p[3], 2);
  BOOST_CHECK_EQUAL(mat->x[3], 2.0);
  
  BOOST_CHECK_EQUAL(mat->i[4], 2);
  BOOST_CHECK_EQUAL(mat->p[4], 2);
  BOOST_CHECK_EQUAL(mat->x[4], 4.0);
  
  BOOST_CHECK_EQUAL(mat->i[5], 2);
  BOOST_CHECK_EQUAL(mat->p[5], 3);
  BOOST_CHECK_EQUAL(mat->x[5], 6.0);
  
  BOOST_CHECK_EQUAL(mat->i[6], 3);
  BOOST_CHECK_EQUAL(mat->p[6], 4);
  BOOST_CHECK_EQUAL(mat->x[6], 1.0);
  
  const int nnz1 = 3;
  mat = cs_spalloc(m, n, nnz1, true, true);
  mat->nz = nnz1;
  
  mat->i[0] = 3;
  mat->p[0] = 2;
  mat->x[0] = 1.0;
  
  mat->i[1] = 1;
  mat->p[1] = 3;
  mat->x[1] = 2.0;
  
  mat->i[2] = 1;
  mat->p[2] = 4;
  mat->x[2] = 3.0;
  
  
  //check instantiation with cs pointer
  SortedCsTripletMatrix test1(mat, true);
  
  mat = test1.get_matrix_ptr();
  
  BOOST_CHECK(mat != NULL);
  BOOST_CHECK_EQUAL(mat->m, m);
  BOOST_CHECK_EQUAL(mat->n, n);
  BOOST_CHECK_EQUAL(mat->nz, nnz1);
  
  BOOST_CHECK_EQUAL(mat->i[0], 1);
  BOOST_CHECK_EQUAL(mat->p[0], 3);
  BOOST_CHECK_EQUAL(mat->x[0], 2.0);
  
  BOOST_CHECK_EQUAL(mat->i[1], 1);
  BOOST_CHECK_EQUAL(mat->p[1], 4);
  BOOST_CHECK_EQUAL(mat->x[1], 3.0);
  
  BOOST_CHECK_EQUAL(mat->i[2], 3);
  BOOST_CHECK_EQUAL(mat->p[2], 2);
  BOOST_CHECK_EQUAL(mat->x[2], 1.0);
}

/**
* @test TestSortedCsTripletMatrix - test getValue
*
* this test copies the test of CsTripletValue, since the functions should work similar, but
* for this class the soring of the values is used for speedup
*/
BOOST_AUTO_TEST_CASE(TestGetValue)
{
  const int m=10, n=12, nnz=3;
  
  int rows[nnz] = {2,1,7};
  int cols[nnz] = {5,8,1};
  double values[nnz] = {6.2345674567, 74354.239423, 93456.05453};
  SortedCsTripletMatrix test(m, n, nnz, rows, cols, values);
  
  BOOST_CHECK_EQUAL(test.getValue(rows[1], cols[1]), values[1]);
  BOOST_CHECK_EQUAL(test.getValue(4,3), 0.0);
}

/**
* @test TestSortedCsTripletMatrix - testSetValue
*
* this test copies the test of CsTripletValue, since the functions should work similar, but
* for this class the soring of the values is used for speedup
*/
BOOST_AUTO_TEST_CASE(testSetValue)
{
  const int m=13, n=5, nnz = 3;
  int rows[nnz] = {4,7,9};
  int cols[nnz] = {2,7,2};
  double values[nnz] = {3234.234545, 23612.12333, 123535.23422};
  SortedCsTripletMatrix test(m, n, nnz, rows, cols, values);
  const double newValue = 6.9203;
  BOOST_CHECK_NO_THROW(test.setValue(rows[1], cols[1], newValue));
  BOOST_CHECK_EQUAL(test.getValue(rows[1], cols[1]), newValue);
  BOOST_CHECK_THROW(test.setValue(4, 4, newValue), EntryNotInSparseStructureException);
}


/**
* @test TestSortedCsTripletMatrix - test function setSubMatrix
*/
BOOST_AUTO_TEST_CASE(TestSubMatrix)
{
  // fields of matrix must match
  const int m1=8, m2=4, n1=7, n2=5, nnz1=5, nnz2=3;
  int rows1[nnz1] = {0,5,3,5,7};
  int cols1[nnz1] = {1,4,2,2,6};
  double values1[nnz1] = {1.1, 1.2, 1.3, 1.4, 1.5};
  // matrix 2 should override values 2,3,4
  // to have the correct or order of values, the indices must be permutated in the same way
  // as the values of cols1, rows1
  int rows2[nnz2] = {2,0,2};
  int cols2[nnz2] = {2,0,0};
  double values2[nnz2] = {2.1, 2.2, 2.3};
  SortedCsTripletMatrix::Ptr matrix1(new CsTripletMatrix(m1, n1, nnz1, rows1, cols1, values1));
  CsTripletMatrix::Ptr matrix2(new CsTripletMatrix(m2, n2, nnz2, rows2, cols2, values2));
  
  matrix1->setSubMatrix(matrix2, 3, 2);
  
  BOOST_CHECK_EQUAL(matrix1->getValue(rows1[0], cols1[0]), values1[0]);
  BOOST_CHECK_EQUAL(matrix1->getValue(rows1[nnz1-1], cols1[nnz1-1]), values1[nnz1-1]);
  for(int i=1; i<nnz1-1; i++){
    BOOST_CHECK_EQUAL(matrix1->getValue(rows1[i], cols1[i]), values2[i-1]);
  }
}

BOOST_AUTO_TEST_CASE(TestVertCat)
{
  const int m1=2, m2=3, n=4, nnz1=4, nnz2=5;
  int rows1[nnz1] = {0,1,0,1};
  int rows2[nnz2] = {1,1,2,0,2};
  int cols1[nnz1] = {0,1,1,3};
  int cols2[nnz2] = {1,2,2,3,3};
  double values1[nnz1] = {1.1, 1.2, 1.3, 1.4};
  double values2[nnz2] = {2.1, 2.2, 2.3, 2.4, 2.5};
  
  CsTripletMatrix::Ptr matrix1(new SortedCsTripletMatrix(m1, n, nnz1, rows1, cols1, values1));
  CsTripletMatrix::Ptr matrix2(new CsTripletMatrix(m2, n, nnz2, rows2, cols2, values2));
  CsTripletMatrix::Ptr result = matrix1->vertcat(matrix2);
  BOOST_CHECK_EQUAL(result->get_n_cols(), n);
  BOOST_CHECK_EQUAL(result->get_n_rows(), m1+m2);
  BOOST_CHECK_EQUAL(result->get_nnz(), nnz1+nnz2);
  for(int i=0; i<nnz1; i++){
    BOOST_CHECK_EQUAL(result->getValue(rows1[i], cols1[i]), values1[i]);
  }
  for(int i=0; i<nnz2; i++){
    BOOST_CHECK_EQUAL(result->getValue(rows2[i]+m1, cols2[i]), values2[i]);
  }
  
  const int m3 = 1, n3=2, nnz3 = 1;
  int rows3[nnz3] = {0};
  int cols3[nnz3] = {0};
  double values3[nnz3] = {0.0};
  CsTripletMatrix::Ptr incompatibleMatrix(new CsTripletMatrix(m3, n3, nnz3, rows3, cols3, values3));
  BOOST_CHECK_THROW(matrix1->vertcat(incompatibleMatrix), MatricesNotCompatibleException);
}

/**
* @test TestSortedCsTripletMatrix - test function get_matrix_slices
*/
BOOST_AUTO_TEST_CASE(TestGetMatrixSlices)
{
  const int m = 5, n =5, nnz=10, numRowsSlices = 2, numColsSlices = 3;
  int rows[nnz] = {1,3,2,4,3,4,0,1,0,2};
  int cols[nnz] = {0,1,2,3,4,0,1,2,3,4};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
  int rowsSlices[numRowsSlices] = {0,3};
  int colsSlices[numColsSlices] = {1,2,4};
  SortedCsTripletMatrix test(m, n, nnz, rows, cols, values);
  CsTripletMatrix::Ptr matrixSlices = test.get_matrix_slices(numRowsSlices, rowsSlices, numColsSlices, colsSlices);
  cs* mat = matrixSlices->get_matrix_ptr();
  
  BOOST_CHECK_EQUAL(mat->m, numRowsSlices);
  BOOST_CHECK_EQUAL(mat->n, numColsSlices);
  BOOST_CHECK_EQUAL(mat->nz, 3);
  
  BOOST_CHECK_EQUAL(mat->i[0], 0);
  BOOST_CHECK_EQUAL(mat->p[0], 0);
  BOOST_CHECK_EQUAL(mat->x[0], 7.0);
  
  BOOST_CHECK_EQUAL(mat->i[1], 1);
  BOOST_CHECK_EQUAL(mat->p[1], 0);
  BOOST_CHECK_EQUAL(mat->x[1], 2.0);
  
  BOOST_CHECK_EQUAL(mat->i[2], 1);
  BOOST_CHECK_EQUAL(mat->p[2], 2);
  BOOST_CHECK_EQUAL(mat->x[2], 5.0);
}

/**
* @test TestSortedCsTripletMatrix - test function get_matrix_slices_keep_size
*/
BOOST_AUTO_TEST_CASE(TestGetMatrixSlicesKeepSize)
{
  const int m = 5, n =5, nnz=10, numRowsSlices = 2, numColsSlices = 3;
  int rows[nnz] = {1,3,2,4,3,4,0,1,0,2};
  int cols[nnz] = {0,1,2,3,4,0,1,2,3,4};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
  int rowsSlices[numRowsSlices] = {0,3};
  int colsSlices[numColsSlices] = {1,2,4};
  SortedCsTripletMatrix test(m, n, nnz, rows, cols, values);
  CsTripletMatrix::Ptr matrixSlices = test.get_matrix_slices_keep_size(numRowsSlices, rowsSlices, numColsSlices, colsSlices);
  cs* mat = matrixSlices->get_matrix_ptr();
  
  BOOST_CHECK_EQUAL(mat->m, m);
  BOOST_CHECK_EQUAL(mat->n, n);
  BOOST_CHECK_EQUAL(mat->nz, 3);
  
  BOOST_CHECK_EQUAL(mat->i[0], 0);
  BOOST_CHECK_EQUAL(mat->p[0], 1);
  BOOST_CHECK_EQUAL(mat->x[0], 7.0);
  
  BOOST_CHECK_EQUAL(mat->i[1], 3);
  BOOST_CHECK_EQUAL(mat->p[1], 1);
  BOOST_CHECK_EQUAL(mat->x[1], 2.0);
  
  BOOST_CHECK_EQUAL(mat->i[2], 3);
  BOOST_CHECK_EQUAL(mat->p[2], 4);
  BOOST_CHECK_EQUAL(mat->x[2], 5.0);
}

BOOST_AUTO_TEST_SUITE_END()

/*class SortedCsTripletMatrix : public CsTripletMatrix
  virtual CsTripletMatrix::Ptr get_matrix_slices(int n_rows, const int* row_idx,
                                                 int n_cols, const int* col_idx) const;
  virtual CsTripletMatrix::Ptr get_matrix_slices_keep_size(int numRows, const int* rowIndex,
                                                           int numCols, const int* colIndex) const;*/