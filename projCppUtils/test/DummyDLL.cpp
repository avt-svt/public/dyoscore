#if WIN32
#define LINKDLL __declspec(dllexport)
#else 
#define LINKDLL
#endif


extern "C" LINKDLL int returnFive(int i);


LINKDLL int returnFive(int i){
  return 5*i;
}