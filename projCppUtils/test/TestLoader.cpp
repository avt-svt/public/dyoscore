/** @file TestLoader.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Testfile for the Loader Class                                        \n
* =====================================================================\n
* @author Arne Graf
* @date 26.8.2011
*/

//! name of the testsuite
#define BOOST_TEST_MODULE LoaderTest

#include "TestLoader.h"
#include "Loader.hpp"
#include "boost/test/unit_test.hpp"
//#include "dummy.h"
using namespace utils;

/***************************************************
**            derive from Loader              **
***********************************************/

/**
* standard-constructor - calls init (sets handle to NULL)
*/
utils::DummyLoader::DummyLoader(){
    init();
}

/**
* constructor loading a given dll
*@param dllName_in name of the dll to be loaded
*/
utils::DummyLoader::DummyLoader(std::string dllName_in){
    init();
    loadDLL(dllName_in);
}

/**
* copy-constructor - gets handle to the same dll as l
* @param l Loader to be copied
*/
utils::DummyLoader::DummyLoader(const DummyLoader& l){
    init();
    loadDLL(l.dllName);
}

/**
* destructor - unload the dll held by Loader
*/
utils::DummyLoader::~DummyLoader(){
    unloadDLL();
}

/**
* dllCallOf - get the name of lib-function(string) and return a int value(of the function)
*/
int utils::DummyLoader::dllCallOf(std::string name, int in) {
  int (*dllfunc)(int) = NULL;
  loadFcn(dllfunc, name);
  return dllfunc(in);
}



/*
#################################################
##            Boost Test from here             ##
#################################################
*/

/**
* @test TestLoader - loading an existing function
*
* case:
*/
BOOST_AUTO_TEST_CASE(dll)
{
  int expected = 25;
  int out;
  DummyLoader* l = new DummyLoader("DummyDLL");
  out = l->dllCallOf("returnFive", 5);
  BOOST_CHECK_EQUAL(out, expected);
  delete(l);
}

/**
* @test TestLoader - loading a non-existing dll
*
* case:
*/
BOOST_AUTO_TEST_CASE(nodll)
{
  BOOST_CHECK_THROW (DummyLoader* l = new DummyLoader("notExisting"), DllLoaderException);
}

/**
* @test TestLoader - loading dll and calling non-existing function
*
* case:
*/
BOOST_AUTO_TEST_CASE(unknownFunction)
{
  DummyLoader* l = new DummyLoader("DummyDLL");
  BOOST_CHECK_THROW (l->dllCallOf("unknown", 5), DllLoaderException);
  delete l;
}

