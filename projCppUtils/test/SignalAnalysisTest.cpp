/** @file SignalAnalysisTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Testfile for the SignalAnalysis class                                \n
* =====================================================================\n
* @author Fady Assassa
* @date 01.06.2012
*/

#pragma once
#define BOOST_TEST_MODULE Array 
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>
#include <vector>
#include "SignalAnalysisDummy.hpp"
#include <math.h>

#define BOOST_TOL 1E-10

BOOST_AUTO_TEST_SUITE(TestSignalAnalsis)

BOOST_AUTO_TEST_CASE(TestRoundOnWaveletGridforPieceWiseConstant)
{
  int numIntervals = 8;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);


  // for a equidistant dyadic grid, we should obtain the exact same grid
  vector<double> resultDyadic = signal.roundOntoWaveletGridPublic(grid);

  BOOST_CHECK_EQUAL(resultDyadic.size(), numIntervals);
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK_CLOSE(resultDyadic[i], grid[i], BOOST_TOL);
  }

  // we will destroy the dyadic nature of the grid now.
  numIntervals = 3;
  grid.resize(numIntervals);
  values.resize(numIntervals);
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> resultNonDyadic =   signal.roundOntoWaveletGridPublic(grid);

  int maxRefinementLevel = 10;
  int numFinest = 1 << maxRefinementLevel;
  double meshSize = 1/double(numFinest);

  double pos[] = {0, 341, 683};
  BOOST_CHECK_EQUAL(resultNonDyadic.size(), numIntervals);
  for(int i=0; i<numIntervals; i++){
    double position = resultNonDyadic[i]*numFinest;
    BOOST_CHECK_CLOSE(pos[i], position, BOOST_TOL);
  }
}


BOOST_AUTO_TEST_CASE(TestRoundOnWaveletGridforPieceWiseLinear)
{
  int numIntervals = 8;
  int order = 2;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals+1);

  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());


  vector<double> resultDyadic = signal.roundOntoWaveletGridPublic(grid);

  // for a equidistant dyadic grid, we should obtain the exact same grid
  BOOST_CHECK_EQUAL(resultDyadic.size(), grid.size());
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK_CLOSE(resultDyadic[i], grid[i], BOOST_TOL);
  }

  // we will destroy the dyadic nature of the grid now.
  numIntervals = 3;
  grid.resize(numIntervals);
  values.resize(numIntervals);
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());


  vector<double> resultNonDyadic = signal.roundOntoWaveletGridPublic(grid);

  int maxRefinementLevel = 10;
  int numFinest = 1 << maxRefinementLevel; // 2^10 bitwise calculation
  double meshSize = 1/double(numFinest);

  double pos[] = {0, 341, 683};
  BOOST_CHECK_EQUAL(resultNonDyadic.size(), grid.size());
  for(int i=0; i<numIntervals; i++){
    double position = resultNonDyadic[i] * numFinest;
    BOOST_CHECK_CLOSE(pos[i], position, BOOST_TOL);
  }
}

BOOST_AUTO_TEST_CASE(TestgridPointsToIndicesforPieceWiseConstant)
{
  int numIntervals = 8;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> dyadicGrids = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrids);

  BOOST_CHECK_EQUAL(dyadicIndices.size(), numIntervals);
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK_EQUAL(dyadicIndices[i], i+1); // verfied values with old dyos
  }

  // we will destroy the dyadic nature of the grid now.
  numIntervals = 3;
  grid.resize(numIntervals);
  values.resize(numIntervals);
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> nonDyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> nonDyadicIndices = signal.gridPointsToIndicesPublic(nonDyadicGrid);
  
  int pos[] = {1, 683, 854}; // verified values with old dyos
  BOOST_CHECK_EQUAL(nonDyadicIndices.size(), numIntervals);
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK_EQUAL(pos[i], nonDyadicIndices[i]);
  }
}



BOOST_AUTO_TEST_CASE(TestgridPointsToIndicesforPieceWiseLinear)
{
  int numIntervals = 8;
  int order = 2;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);

  BOOST_CHECK_EQUAL(dyadicIndices.size(), grid.size());
  for(unsigned i=0; i<dyadicIndices.size(); i++){
    BOOST_CHECK_EQUAL(dyadicIndices[i], i+1); // verfied values with old dyos
  }

  // we will destroy the dyadic nature of the grid now.
  numIntervals = 3;
  grid.resize(numIntervals);
  values.resize(numIntervals);
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());

  vector<double> nonDyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> nonDyadicIndices = signal.gridPointsToIndicesPublic(nonDyadicGrid);
  
  int pos[] = {1, 3, 684, 855}; // verfied with old dyos
  BOOST_CHECK_EQUAL(nonDyadicIndices.size(), grid.size());
  for(unsigned i=0; i<nonDyadicIndices.size(); i++){
    BOOST_CHECK_EQUAL(pos[i], nonDyadicIndices[i]);
  }
}


/**
* @test TestSignalAnalysis - test grid point indices for piece wise constant
*/
BOOST_AUTO_TEST_CASE(TestIndicesToGridPointsPieceWiseConstant)
{
  int numIntervals = 8;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);

  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);
  vector<double> dyadicPoints = signal.indicesToGridPointsPublic(dyadicIndices);

  BOOST_CHECK_EQUAL(dyadicPoints.size(), numIntervals);
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK_EQUAL(dyadicPoints[i], grid[i]); // verfied values with old dyos
  }

  // we will destroy the dyadic nature of the grid now.
  numIntervals = 3;
  grid.resize(numIntervals);
  values.resize(numIntervals);
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> nonDyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> nonDyadicIndices = signal.gridPointsToIndicesPublic(nonDyadicGrid);
  vector<double> nonDyadicPoints = signal.indicesToGridPointsPublic(nonDyadicIndices);

  BOOST_CHECK_EQUAL(nonDyadicPoints.size(), numIntervals);
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK_EQUAL(nonDyadicPoints[i], nonDyadicGrid[i]);
  }
}

BOOST_AUTO_TEST_CASE(TestIndicesToGridPointsforPieceWiseLinear)
{
  int numIntervals = 8;
  int order = 2;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);
  vector<double> dyadicPoints = signal.indicesToGridPointsPublic(dyadicIndices);

  BOOST_CHECK_EQUAL(dyadicPoints.size(), grid.size());
  for(unsigned i=0; i<grid.size(); i++){
    BOOST_CHECK_EQUAL(dyadicPoints[i], grid[i]); // verfied values with old dyos
  }

  // we will destroy the dyadic nature of the grid now.
  numIntervals = 3;
  grid.resize(numIntervals);
  values.resize(numIntervals);
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());

  vector<double> nonDyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> nonDyadicIndices = signal.gridPointsToIndicesPublic(nonDyadicGrid);
  vector<double> nonDyadicPoints = signal.indicesToGridPointsPublic(nonDyadicIndices);

  BOOST_CHECK_EQUAL(nonDyadicPoints.size(), grid.size());
  for(unsigned i=0; i<nonDyadicPoints.size(); i++){
    BOOST_CHECK_EQUAL(nonDyadicPoints[i], nonDyadicGrid[i]);
  }
}

BOOST_AUTO_TEST_CASE(TestGetDirectNeighborsForPWC)
{
  int numIntervals = 8;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);

  vector<int> dyadicNeighbors;
  vector<vector<int> > compareNeighbors;
  compareNeighbors.resize(numIntervals);
  compareNeighbors[0].push_back(2);   compareNeighbors[4].push_back(6);
  compareNeighbors[1].push_back(3);   compareNeighbors[4].push_back(9);
  compareNeighbors[1].push_back(4);   compareNeighbors[4].push_back(10); compareNeighbors[7].push_back(7);
  compareNeighbors[2].push_back(4);   compareNeighbors[5].push_back(5);  compareNeighbors[7].push_back(15);
  compareNeighbors[2].push_back(5);   compareNeighbors[5].push_back(7);  compareNeighbors[7].push_back(16);
  compareNeighbors[2].push_back(6);   compareNeighbors[5].push_back(11);
  compareNeighbors[3].push_back(3);   compareNeighbors[5].push_back(12);
  compareNeighbors[3].push_back(7);   compareNeighbors[6].push_back(6);
  compareNeighbors[3].push_back(8);   compareNeighbors[6].push_back(8);
  compareNeighbors[6].push_back(13);  compareNeighbors[6].push_back(14);
  

  for(unsigned i=0; i<dyadicIndices.size(); i++){
    dyadicNeighbors = signal.getDirectNeighborsPublic(dyadicIndices[i]);
    BOOST_CHECK_EQUAL(dyadicNeighbors.size(), compareNeighbors[i].size());
    for(unsigned j=0; j<dyadicNeighbors.size(); j++){
      BOOST_CHECK_EQUAL(dyadicNeighbors[j], compareNeighbors[i][j]);
    }
  }
}


BOOST_AUTO_TEST_CASE(TestGetDirectNeighborsForPWL)
{
  int numIntervals = 4;
  int order = 2;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);

  vector<int> dyadicNeighbors;
  vector<vector<int> > compareNeighbors;
  compareNeighbors.resize(dyadicIndices.size());
  compareNeighbors[0].push_back(2);
  compareNeighbors[0].push_back(4);
  compareNeighbors[1].push_back(1);
  compareNeighbors[1].push_back(3);
  compareNeighbors[1].push_back(4);
  compareNeighbors[1].push_back(5);
  compareNeighbors[2].push_back(2);
  compareNeighbors[2].push_back(5);
  compareNeighbors[3].push_back(5);
  compareNeighbors[3].push_back(6);
  compareNeighbors[3].push_back(7);
  compareNeighbors[4].push_back(4);
  compareNeighbors[4].push_back(8);
  compareNeighbors[4].push_back(9);

  for(unsigned i=0; i<dyadicIndices.size(); i++){
    dyadicNeighbors = signal.getDirectNeighborsPublic(dyadicIndices[i]);
    BOOST_CHECK_EQUAL(dyadicNeighbors.size(), compareNeighbors[i].size());
    for(unsigned j=0; j<dyadicNeighbors.size(); j++){
      BOOST_CHECK_EQUAL(dyadicNeighbors[j], compareNeighbors[i][j]);
    }
  }
}

/**
* @test TestSignalAnalysis - test function getSeveralLayers
*/
BOOST_AUTO_TEST_CASE(TestGetSeveralLayers)
{
  int numIntervals = 8;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);


  vector<int> compareNeighbors;
  compareNeighbors.reserve(15);
  compareNeighbors.push_back(2);   compareNeighbors.push_back(6);
  compareNeighbors.push_back(3);   compareNeighbors.push_back(9);
  compareNeighbors.push_back(4);   compareNeighbors.push_back(10); compareNeighbors.push_back(7);
  compareNeighbors.push_back(5);  compareNeighbors.push_back(15);
  compareNeighbors.push_back(16);
  compareNeighbors.push_back(11);
  compareNeighbors.push_back(12);
  compareNeighbors.push_back(8);
  compareNeighbors.push_back(13);  compareNeighbors.push_back(14);

  // in committ 109 code was changed that double entries are removed. This obvously changed the behaviour
  // of the tested function. I have adjusted the compareNeighbors to that behaviour (without checking)- tjho
  vector<int> dyadicNeighbors = signal.getNeighborsForSeveralLayersPublic(dyadicIndices);
  sort(compareNeighbors.begin(), compareNeighbors.end());

  BOOST_CHECK_EQUAL(compareNeighbors.size(), dyadicNeighbors.size());
  for(unsigned i=0; i<dyadicNeighbors.size(); i++){
    BOOST_CHECK_EQUAL(dyadicNeighbors[i], compareNeighbors[i]);
  }
}


BOOST_AUTO_TEST_CASE(TestFillGrid)
{
  int numIntervals = 3,
    maxRefinementLevel = 10,
    minRefinementLevel = 4;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholded;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholded, maxRefinementLevel, minRefinementLevel);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);

  vector<int> filledGrid = signal.fillToMinimumGridPublic(dyadicIndices);

  int compareIndices[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 683, 854};
  BOOST_CHECK_EQUAL(filledGrid.size(), 18);
  for(unsigned i=0; i<filledGrid.size(); i++){
    BOOST_CHECK_EQUAL(filledGrid[i], compareIndices[i]);
  }
}

BOOST_AUTO_TEST_CASE(TestRandwerte)
{
  int numIntervals = 3,
    maxRefinementLevel = 10,
    minRefinementLevel = 4;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholdedempty;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholdedempty, maxRefinementLevel, minRefinementLevel);

  vector<double> grid(numIntervals), values(numIntervals);

  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
    values[i] = 2 * i * i + 3 * i + 4;
  }
  grid.push_back(1.0);
  values.push_back(values.back());

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);
  vector<int> threshholdedIndices;
  threshholdedIndices.assign(dyadicIndices.begin()++,dyadicIndices.end());
  vector<int> boundaryIndices = signal.getBoundaryIndicesPublic(dyadicIndices, threshholdedIndices);

  BOOST_CHECK_EQUAL(dyadicIndices.size(), boundaryIndices.size());
  for(unsigned i=0; i<dyadicIndices.size(); i++){
    BOOST_CHECK_EQUAL(dyadicIndices[i], boundaryIndices[i]);
  }

  vector<int> someIndices, someThreshholded;
  someIndices.push_back(1); someIndices.push_back(2); someIndices.push_back(3);
  someThreshholded.push_back(1); someThreshholded.push_back(2);

  boundaryIndices = signal.getBoundaryIndicesPublic(someIndices, threshholdedIndices);
  BOOST_CHECK_EQUAL(boundaryIndices.size(), 2);
  for(unsigned i=0; i<boundaryIndices.size(); i++){
    BOOST_CHECK_EQUAL(boundaryIndices[i], someIndices[i+1]);
  }

  
  int horizontalDepth = 0;
  SignalAnalysisDummy signalHor0(order, upperBound, lowerBound, thresholdedempty, 
                                 maxRefinementLevel, minRefinementLevel, horizontalDepth);

  boundaryIndices = signalHor0.getBoundaryIndicesPublic(someIndices, threshholdedIndices);
  BOOST_CHECK_EQUAL(boundaryIndices.size(), 2);
  for(unsigned i=0; i<boundaryIndices.size(); i++){
    BOOST_CHECK_EQUAL(boundaryIndices[i], someIndices[i+1]);
  }
}


BOOST_AUTO_TEST_CASE(TestApproximate)
{
  int numIntervals = 3,
    maxRefinementLevel = 10,
    minRefinementLevel = 4;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholdedempty;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholdedempty, maxRefinementLevel, minRefinementLevel);

  vector<double> grid(numIntervals), values(numIntervals);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
  }
  grid.push_back(1.0);
  values[0] = 0.3;
  values[1] = -0.2;
  values[2] = 1.9;


  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);

  vector<double> resultVals;
  vector<int> resultInd;
  signal.approximateSignalPublic(values, dyadicIndices, 2, resultVals, resultInd);

  double compareResultVals[] = {1.9, 0.3};
  int compareResultIndc[] = {854, 1};
  int sizeSignal = 2;
  BOOST_CHECK_EQUAL(resultInd.size(), sizeSignal);
  BOOST_CHECK_EQUAL(resultVals.size(), sizeSignal);
  for(int i=0; i<sizeSignal; i++)
  {
    BOOST_CHECK_CLOSE(compareResultVals[i], resultVals[i],BOOST_TOL);
    BOOST_CHECK_EQUAL(compareResultIndc[i], resultInd[i]);
  }

  int horDepth = 1, verDepth = 1;
  double epsilon = 1.0;
  SignalAnalysisDummy signalEps(order, upperBound, lowerBound, thresholdedempty,
                                maxRefinementLevel, minRefinementLevel, horDepth, verDepth, 1.0e-8, epsilon);
  // check other variant
  // we only take the largest coefficient
  
  resultVals.clear();
  resultInd.clear();
  signalEps.approximateSignalPublic(values, dyadicIndices, 2, resultVals, resultInd);

  sizeSignal = 1;
  BOOST_CHECK_EQUAL(resultInd.size(), sizeSignal);
  BOOST_CHECK_EQUAL(resultVals.size(), sizeSignal);
  for(int i=0; i<sizeSignal; i++)
  {
    BOOST_CHECK_CLOSE(compareResultVals[i], resultVals[i],BOOST_TOL);
    BOOST_CHECK_EQUAL(compareResultIndc[i], resultInd[i]);
  }
}


BOOST_AUTO_TEST_CASE(TestEliminate)
{
  int numIntervals = 3,
    maxRefinementLevel = 10,
    minRefinementLevel = 4;
  int order = 1;
  double upperBound = 10,
    lowerBound = -10;
  vector<int> thresholdedempty;
  int horLevel = 1, verLevel = 1;
  double etres = 0.21;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholdedempty, maxRefinementLevel, minRefinementLevel, 
                             horLevel, verLevel, etres);

  vector<double> grid(numIntervals), values(numIntervals);

  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
  }
  grid.push_back(1.0);
  values[0] = 0.3;
  values[1] = -0.2;
  values[2] = 1.9;

  vector<double> dyadicGrid = signal.roundOntoWaveletGridPublic(grid);
  vector<int> dyadicIndices = signal.gridPointsToIndicesPublic(dyadicGrid);

  vector<int> ThreshholdedIndices;
  ThreshholdedIndices.assign(dyadicIndices.begin()++,dyadicIndices.end());
  vector<int> boundaryIndices = signal.getBoundaryIndicesPublic(dyadicIndices, ThreshholdedIndices);
  
  vector<double> valuesFineGrid(1<<maxRefinementLevel);
  for(unsigned i=0; i<boundaryIndices.size(); i++){
    valuesFineGrid[boundaryIndices[i] -1] = values[i];
  }
  vector<int> significantWaves = signal.eliminateWaveletsPublic(valuesFineGrid, boundaryIndices);
  vector<int> removedIndices = signal.getThresholdedIndices();
  
  BOOST_CHECK_EQUAL(removedIndices.size(), 1);
  BOOST_CHECK_EQUAL(removedIndices[0], 683);

  vector<int> compareIndices; compareIndices.push_back(0); compareIndices.push_back(2);
  BOOST_CHECK_EQUAL(significantWaves.size(), 2);
  for(unsigned i=0; i<significantWaves.size(); i++){
    BOOST_CHECK_CLOSE(valuesFineGrid[significantWaves[i]-1], values[compareIndices[i]], BOOST_TOL);
    BOOST_CHECK_EQUAL(dyadicIndices[compareIndices[i]], significantWaves[i]);
  }
}


BOOST_AUTO_TEST_CASE(TestObtainNewMeshNonDyadic)
{
  int numIntervals = 5,
    maxRefinementLevel = 10,
    minRefinementLevel = 3;
  int order = 1;
  double upperBound = 1.8,
    lowerBound = 0;
  vector<int> thresholdedempty;
  int horLevel = 1, verLevel = 1;
  double etres = 0.21;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholdedempty, maxRefinementLevel, minRefinementLevel, 
                             horLevel, verLevel, etres);


  vector<double> grid(numIntervals+1), values(numIntervals+1);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
  }
  grid[5] = 1.0;
  values[0] = 0.3;
  values[1] = -0.2;
  values[2] = 1.9;
  values[3] = 0.9;
  values[4] = 0.0;
  values[5] = 0.0;

  signal.obtainNewMesh(grid, values);
  vector<double> newGrid = signal.getRefinedGrid();
  vector<double> compareGrid(8);
  for(unsigned i=0; i<compareGrid.size(); i++){
    compareGrid[i] = double(i)/8;
  }
  compareGrid.push_back(1.0);
  BOOST_CHECK_EQUAL(compareGrid.size(), newGrid.size());
  for (unsigned i=0; i<compareGrid.size(); i++) {
    BOOST_CHECK_CLOSE(compareGrid[i], newGrid[i], BOOST_TOL);
  }

  double compareValues[9] = {0.3, 0.3, 0.0, 0.0, 1.8, 1.8, 1.8, 0.9, 0.9};
  vector<double> newValues = signal.getRefinedValues();
  BOOST_CHECK_EQUAL(newGrid.size(), newValues.size());
  for (unsigned i=0; i<newValues.size(); i++) {
    BOOST_CHECK_CLOSE(compareValues[i], newValues[i], BOOST_TOL);
  }
}


BOOST_AUTO_TEST_CASE(TestObtainNewMeshNonDyadic2)
{
 int numIntervals = 3,
    maxRefinementLevel = 10,
    minRefinementLevel = 1;
  int order = 1;
  double upperBound = 1.8,
    lowerBound = 0;
  vector<int> thresholdedempty;
  int horLevel = 1, verLevel = 1;
  double etres = 0.21;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholdedempty, maxRefinementLevel, minRefinementLevel, 
                             horLevel, verLevel, etres);


  vector<double> grid(numIntervals+1), values(numIntervals+1);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
  }
  grid[3] = 1.0;
  values[0] = 0.3;
  values[1] = -0.2;
  values[2] = 1.9;
  values[3] = 1.9;
  
  signal.obtainNewMesh(grid, values);
  vector<double> newGrid = signal.getRefinedGrid();
  vector<double> compareGrid(2);
  for(unsigned i=0; i<compareGrid.size(); i++){
    compareGrid[i] = double(i)/2;
  }
  compareGrid.push_back(1.0);
  BOOST_CHECK_EQUAL(compareGrid.size(), newGrid.size());
  for (unsigned i=0; i<compareGrid.size(); i++) {
    BOOST_CHECK_CLOSE(compareGrid[i], newGrid[i], BOOST_TOL);
  }

  double compareValues[3] = {0.3,  0.3, 0.3};
  vector<double> newValues = signal.getRefinedValues();
  BOOST_CHECK_EQUAL(newGrid.size(), newValues.size());
  for (unsigned i=0; i<newValues.size(); i++) {
    BOOST_CHECK_CLOSE(compareValues[i], newValues[i], BOOST_TOL);
  }
}

BOOST_AUTO_TEST_CASE(TestObtainNewMeshNonDyadicPWL)
{
 int numIntervals = 8,
    maxRefinementLevel = 10,
    minRefinementLevel = 1;
  int order = 2;
  double upperBound = 2,
    lowerBound = -2;
  vector<int> thresholdedempty;
  int horLevel = 1, verLevel = 1;
  double etres = 0.21;

  SignalAnalysisDummy signal(order, upperBound, lowerBound, thresholdedempty, maxRefinementLevel, minRefinementLevel, 
                             horLevel, verLevel, etres);

  vector<double> grid(numIntervals), values(numIntervals+1);
  // create equidistant grid between 0 and 1
  for(int i=0; i<numIntervals; i++){
    grid[i] = double(i) / double(numIntervals);
  }
  grid.push_back(1.0);
  double assignValues[9] = {0.3, 0.1125, 0.0, 0.064461976882309896, 0.850244140625, 1.6360263043679, 2, 1.2, -2};
  for(int i=0; i<=numIntervals; i++)
    values[i] = assignValues[i];

  signal.obtainNewMesh(grid, values);
  vector<double> newGrid = signal.getRefinedGrid();
  int gridSize = 8;
  double compareGrid[8] = {0.0, 0.25, 0.5, 0.75, 0.8125, 0.875, 0.9375, 1.0};

  BOOST_CHECK_EQUAL(gridSize, newGrid.size());
  for (unsigned i=0; i<newGrid.size(); i++) {
    BOOST_CHECK_CLOSE(compareGrid[i], newGrid[i], BOOST_TOL);
  }


  vector<double> newValues = signal.getRefinedValues();
  double compareValues[8] = {0.3, 0.0, 0.85024414062499998, 2.0, 1.6, 1.20, -0.40000000000000013, -2.0};
  BOOST_CHECK_EQUAL(newGrid.size(), newValues.size());
  for (unsigned i=0; i<newValues.size(); i++) {
    BOOST_CHECK_CLOSE(compareValues[i], newValues[i], BOOST_TOL);
  }

  
}

BOOST_AUTO_TEST_SUITE_END()
