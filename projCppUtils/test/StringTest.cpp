/** @file TestLoader.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Testfile for the Loader Class                                        \n
* =====================================================================\n
* @author Arne Graf 
* @date 30.8.2011
*/

//! name of the testsuite
#define BOOST_TEST_MODULE StringTest

#include <string>
#include "String.hpp"
#include "boost/test/unit_test.hpp"

using namespace utils;
using std::string;
/*
#################################################
##            Boost Test from here             ##
#################################################
*/

/**
* @test TestLoader - loading an existing function
*
* case: 
*/
BOOST_AUTO_TEST_CASE(toLower) 
{ 
  string testIn = "The BOOST Tests are wonderfull";
  string expectedOut = "the boost tests are wonderfull";
  toLowerCase(testIn);
  BOOST_CHECK(expectedOut.compare(testIn)==0);

  testIn = "";
  expectedOut = "";
  toLowerCase(testIn);
  BOOST_CHECK(expectedOut.compare(testIn)==0);

  testIn = "the";
  expectedOut = "the";
  toLowerCase(testIn);
  BOOST_CHECK(expectedOut.compare(testIn)==0);
}

BOOST_AUTO_TEST_CASE(toUpper) 
{ 
  string testIn = "The BOOST Tests are wonderfull";
  string expectedOut = "THE BOOST TESTS ARE WONDERFULL";
  toUpperCase(testIn);
  BOOST_CHECK(expectedOut.compare(testIn)==0); 

  testIn = "";
  expectedOut = "";
  toUpperCase(testIn);
  BOOST_CHECK(expectedOut.compare(testIn)==0);

  testIn = "THE";
  expectedOut = "THE";
  toUpperCase(testIn);
  BOOST_CHECK(expectedOut.compare(testIn)==0);
}

BOOST_AUTO_TEST_CASE(compareString) 
{ 
  bool out;
  string a = "The BOOST Tests are wonderfull";
  string b = "THE BOOST TESTS ARE WONDERFULL";
  out = utils::compareStrings(a,b,false);
  BOOST_CHECK(out); 

  a = "The BOOST Tests are wonderfull";
  b = "THE BOOST TESTS ARE WONDERFULL";
  out = utils::compareStrings(a,b,true);
  BOOST_CHECK(!out); 

  a = "wonderfull";
  b = "THE BOOST TESTS ARE WONDERFULL";
  out = utils::compareStrings(a,b);
  BOOST_CHECK(!out); 

  a = "";
  b = "";
  out = utils::compareStrings(a,b);
  BOOST_CHECK(out); 

  a = "&[{}(=*)+]!#";
  b = "&[{}(=*)+]!#";
  out = utils::compareStrings(a,b);
  BOOST_CHECK(out); 
}
