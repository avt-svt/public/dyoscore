/** @file Array.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Testfile header for TestArray and TestWrappingArray                  \n
* The header simply defines the unit test name for the common test     \n
* module.                                                              \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 14.11.2011
*/

#include "Array.hpp"
#include <iostream>
#include <sstream>
#include <string>

using namespace utils;


BOOST_AUTO_TEST_SUITE(TestArray)

/**
* @test TestArray - test for the constructor Array(size)
*
* case: It is tested whether the Array object holds size elements
*
* @author Tjalf Hoffmann
* @date 28.6.2011
*/
BOOST_AUTO_TEST_CASE(ConstructorSize) 
{ 
  unsigned arraySize=10;
	Array<double> testArray(arraySize);
  BOOST_CHECK_EQUAL(testArray.getSize(), arraySize);
}

/**
* @test TestArray - test for the constructor Array(size, value)
* 
* case: It is tested whether the Array object holds size elements of the given value
*
* @author Tjalf Hoffmann
* @date 28.6.2011
*/
BOOST_AUTO_TEST_CASE(ConstructorSizeAndValue)
{
  unsigned arraySize=5;
  double value=7;
  Array<double> testArray(arraySize, value);
  BOOST_REQUIRE_EQUAL(testArray.getSize(), arraySize);
  for(unsigned i=0; i<arraySize; i++){
    BOOST_CHECK_EQUAL(testArray[i], value);
  }
}

/**
* @test TestArray - test for the copy constructor
*
* case: It is tested whether the copy constructor makes a deep copy of an Array
*
* @author Tjalf Hoffmann
* @date 28.6.2011
*/
BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  unsigned arraySize=3;
  double value=4;
  Array<double> original(arraySize, value);
  Array<double> copy(original);
  BOOST_REQUIRE_EQUAL(original.getSize(), copy.getSize());
  for(unsigned i=0; i<original.getSize(); i++){
    BOOST_CHECK_EQUAL(original[i], copy[i]);
  }
}

/**
* @test TestArray - test =operator
*
* case1: It is tested if the assign operator makes a deep copy of an Array
* case2: It is tested whether an ArraySizeMismatchException is thrown if an Array object
*       is assigned to an Array object of different size.
*
* @author Tjalf Hoffmann
* @date 28.6.2011
*/
BOOST_AUTO_TEST_CASE(TestAssign)
{
  //input for case1, case2
  unsigned arraySize1=3;
  unsigned arraySize2=4;
  double value=2;
  Array<double> array1(arraySize1, value);
  //input for case1
  Array<double> array2(arraySize1);
  //input for case2
  Array<double> array3(arraySize2);

  //case1
  array2=array1;
  BOOST_REQUIRE_EQUAL(array2.getSize(), array1.getSize());
  for(unsigned i=0; i<array1.getSize(); i++){
    BOOST_CHECK_EQUAL(array1[i], array2[i]);
  }

  //case2
  //BOOST_CHECK_THROW(array3=array1, ArraySizeMismatchException);
}


/**
* @test TestArray - test getData member function
*
* case: test whether the data attribute can be extracted and used outside the Array object
*
* @author Tjalf Hoffmann
* @date 28.6.2011
*/
BOOST_AUTO_TEST_CASE(TestGetData)
{
  unsigned arraySize=4;
  double value=5;
  Array<double> testArray(arraySize);
  double* data=testArray.getData();
  data[0]=value;
  BOOST_CHECK_EQUAL(testArray[0], value);
}


/**
* @test TestArray - test [] operator
*
* case1: [] already has been tested for reading access (s.a. TestGetData, TestAssign, etc.)
* case2: test [] for writing access
* case3: test throwing of ArrayIndexOutOfBoundsException
*/
BOOST_AUTO_TEST_CASE(TestRandomAccessOperator)
{
  //case2:
  unsigned arraySize = 3;
  Array<int> intArray(arraySize);
  for(unsigned i=0; i<arraySize; i++){
    intArray[i]=i;
    BOOST_CHECK_EQUAL(intArray[i], i);
  }

  //case3:
  //wrong index with writing access
  //BOOST_CHECK_THROW(intArray[arraySize]=0, ArrayIndexOutOfBoundsException);
  //wrong index with reading access
  //BOOST_CHECK_THROW(int v=intArray[arraySize], ArrayIndexOutOfBoundsException);
}

/**
* @brief support function for testcase TestOutput
*
* checks whether the output of an Array object equals an expected output string
* @param ss stringstream containing the expected output string 
*           on exit: empty
* @param tArray Array object (of any type) to be checked
*/
template<typename T>
void checkArrayOutput(std::stringstream &ss, Array<T> &tArray)
{
  std::string compareString=ss.str();
  ss.str("");
  ss << tArray;
  std::string output=ss.str();
  BOOST_CHECK_EQUAL(output, compareString);

  //clear stringstream
  ss.str("");
}

/**
* @test TestArray - operator << for ostreams
*
* case1: test integer output
* case2: test double output
* case3: test string output
*/
BOOST_AUTO_TEST_CASE(TestOutput)
{
  unsigned arraySize=3;
  std::stringstream ss (std::stringstream::in | std::stringstream::out);

 
  

  //case1
  Array<int> intArray(arraySize);
  ss<<"(";
  for(unsigned i=0; i<arraySize-1; i++){
    intArray[i]=i+1;
    ss<<i+1<<",";
  }
  intArray[arraySize-1] = arraySize;
  ss<<arraySize<<")";
  checkArrayOutput(ss, intArray);

  //case2
  Array<double> doubleArray(arraySize);
  ss<<"(";
  for(unsigned i=0; i<arraySize-1; i++){
    doubleArray[i]=i+0.5;
    ss<<i+0.5<<",";
  }
  doubleArray[arraySize-1] = arraySize - 0.5;
  ss<<arraySize-0.5<<")";
  checkArrayOutput(ss, doubleArray);

  //case3
  Array<std::string> stringArray(arraySize);
  ss<<"(";
  for(unsigned i=0; i<arraySize-1; i++){
    std::string s="constant";
    stringArray[i]=s;
    ss<<s<<",";
  }
  stringArray[arraySize-1] = "constant";
  ss<<"constant"<<")";
  checkArrayOutput(ss, stringArray);
}

BOOST_AUTO_TEST_SUITE_END()
