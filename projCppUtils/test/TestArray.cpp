/** @file TestArray.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Testfile for the Array class                                         \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 28.6.2011
*/


//! name of the testsuite
#define BOOST_TEST_MODULE Array 
#include "boost/test/unit_test.hpp"

#include "ArrayTestsuite.hpp"
#include "WrappingArrayTestsuite.hpp"
