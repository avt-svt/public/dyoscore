
#include "ArrayBlas.hpp"

void dscal_(const int* n, const double* c , double* x, const int* incx)
{
  double* end = x + *n;
  while (x<end) {
    *x *= *c;
    x += *incx;
  }
}

void daxpy_(const int* N, const double* a, const double* x, const int* incx, double* y, const int* incy)
{
  const double* end = x + *N;
  while (x<end) {
    *y += *a*(*x);
    x += *incx;
    y += *incy;
  }
}
