
#include "CscMatrix.hpp"
#include "TripletMatrix.hpp"

#include <algorithm>

  // if values == NULL, values are not allocated.
CsCscMatrix::CsCscMatrix(cs* mat, bool owner_)
  : m_mat(CS_TRIPLET(mat) ? cs_compress(mat) : mat), owner(owner_)
{
  if (CS_TRIPLET(mat) && owner)
    cs_spfree(mat);
}

CsCscMatrix::~CsCscMatrix()
{
  if (owner)
    cs_spfree(m_mat);
}

bool CsCscMatrix::getValue(const int rowIndex, const int colIndex, double &value) const
{
  value = 0.0;
  // Remark: indices are zero-based
  assert(rowIndex >= 0);
  assert(colIndex >= 0);
  assert(rowIndex < m_mat->m);
  assert(colIndex < m_mat->n);

  int indStart, indEnd;
  indStart = m_mat->p[colIndex];
  indEnd = m_mat->p[colIndex+1];
  if (indStart == indEnd) {
    return false; // no non-zeros in this column 
  }
  else {
    for (int i=indStart; i<indEnd; i++){
      if (m_mat->i[i] == rowIndex){
        value = m_mat->x[i];
        return true;
      }
    }
  }
  return false;
}

bool CsCscMatrix::solve_lu(const double* rhs, double* lhs)
{
  std::copy(rhs, rhs+get_n_rows(), lhs);
  double tol = 1e-10;
  int order = 2; /* order 0:natural, 1:Chol, 2:LU, 3:QR */
  return (cs_lusol(order, m_mat, lhs, tol) != 0);
}

csd* CsCscMatrix::get_dulmange_mendelsohn_permutation(int seed)
{
  return cs_dmperm(m_mat, seed);
}


void CsCscMatrix::get_values(const int numValues, double *values) const
{
  assert(m_mat->nzmax == numValues);
  std::copy(m_mat->x, m_mat->x + numValues, values);
}

void CsCscMatrix::horzcat_in_place(const CsCscMatrix::Ptr &right)
{
  int oldnnz = get_nnz();
  int oldCols = get_n_cols();
  int nzmax = m_mat->nzmax + right->get_matrix_ptr()->nzmax;
  int success = cs_sprealloc (this->m_mat, nzmax );

  assert(success == 1);
  for(int i=0; i<right->get_nnz(); i++){
    m_mat->i[oldnnz + i] = right->get_matrix_ptr()->i[i];
    m_mat->x[oldnnz + i] = right->get_matrix_ptr()->x[i];
  }
  int ok;
  m_mat->p = (int *) cs_realloc(m_mat->p, right->get_n_cols() + oldCols + 1, sizeof (int), &ok);
  assert(ok);
  for(int i=0; i<right->get_n_cols(); i++){
    m_mat->p[oldCols + i] = right->get_matrix_ptr()->p[i] + m_mat->p[oldCols];
  }
  m_mat->n += right->get_n_cols();
  m_mat->p[oldCols + right->get_n_cols()] = oldnnz + right->get_nnz();
}

void CsCscMatrix::extractColumn(const int columnIndex, double *columnValues, const int columnLength)
{
    assert(columnIndex >= 0);  // required, if index is of type unsigned?
    assert(columnLength == m_mat->m);
    assert(columnIndex < m_mat->n);
    

    const int startInd = m_mat->p[columnIndex];
    const int endInd = m_mat->p[columnIndex+1];
    
    for(int i=0; i<columnLength; i++){
      columnValues[i] = 0.0;
    }
    
    unsigned currRowInd;
    for (int i=startInd; i<endInd; i++) {
        currRowInd = m_mat->i[i];
        columnValues[currRowInd] = m_mat->x[i];
    }
}

int CsCscMatrix::getMatrixRank() const
{
  int doQRDecomposition = 1;
  //there seems to be no documentation what order is about and it couldn't be guessed from code
  int order = 0;
  css *cssymb = cs_sqr(order, m_mat, doQRDecomposition);
  csn *csnum = cs_qr(m_mat, cssymb);
  //now check diagonal of matrix R (csnum->_U) for nonzero entries
  //if matrix has rank n, exactly n entries should be nonzero
  int rank = 0;
  bool owner = false;
  CsCscMatrix::Ptr r(new CsCscMatrix(csnum->U, owner));
  int maxRank = std::min(r->get_n_rows(), r->get_n_cols());
  for(int i=0; i<maxRank; i++){
    double value = 0.0;
    if(r->getValue(i,i,value)){
      if(std::abs(value) > 1e-10){
        rank++;
      }
    }
  }
  
  cs_sfree(cssymb);
  cs_nfree(csnum);
  return rank;
}