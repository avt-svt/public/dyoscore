/** @file DenseMatrix.cpp
*   @brief implementation of DenseMatrix class
*    
*   =====================================================================\n
*   &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
*   =====================================================================\n
*
*   @author Klaus Stockmann, Tjalf Hoffmann
*   @date 12.11.2013                                       
*/



#include "DenseMatrix.hpp"
#include "MatrixExceptions.hpp"
#include <cassert>

/**
* @brief convert a column major sparse matrix into a dense matrix (a vector of vectors)
*
* @param[in] sparseMatrix matrix to be converted
* @param[out] converted matrix (will be resized in this function)
*/
void convertSparseMatrixToFull(CsCscMatrix::Ptr &sparseMatrix,
                               std::vector<std::vector<double> > &fullMatrix)
{
  cs* mat = sparseMatrix->get_matrix_ptr();
  fullMatrix.resize(mat->m);
  for(int i=0; i<mat->m; i++){
    fullMatrix[i].resize(mat->n, 0.0);
  }
  
  unsigned nonZeroIndex = 0;
  for(int i=0; i<mat->n; i++){
    unsigned numEntries = mat->p[i+1]-mat->p[i];
    for(unsigned j=0; j<numEntries; j++){
      assert(int(nonZeroIndex) < mat->nzmax);
      int rowIndex = mat->i[nonZeroIndex];
      int colIndex = i;
      fullMatrix[rowIndex][colIndex] = mat->x[nonZeroIndex];
      nonZeroIndex++;
    }
  }
}

/**
* @brief convert a triplet matrix into a dense matrix (a vector of vectors)
*
* @param[in] sparseMatrix matrix to be converted
* @param[out] converted matrix (will be resized in this function)
*/
void convertSparseMatrixToFull(CsTripletMatrix::Ptr &sparseMatrix,
                               std::vector<std::vector<double> > &fullMatrix)
{
  cs* mat = sparseMatrix->get_matrix_ptr();
  fullMatrix.resize(mat->m);
  for(int i=0; i<mat->m; i++){
    fullMatrix[i].resize(mat->n, 0.0);
  }
  for(int i=0; i<mat->nz; i++){
    int rowIndex = mat->i[i];
    int colIndex = mat->p[i];
    fullMatrix[rowIndex][colIndex] = mat->x[i];
  }
}

/**
* @brief multiplies two dense matrices
*
* The matrices must match and must not be empty
* @param[in] matrix1 mxn matrix to be multiplied
* @param[in] matrix2 nxo matrix to be multiplied
* @return result of the mutliplication (mxo matrix)
*/
std::vector<std::vector<double> > multiply(std::vector<std::vector<double> > &matrix1,
                                           std::vector<std::vector<double> > &matrix2)
{
  std::vector<std::vector<double> > result;
  assert(!matrix1.empty() && !matrix2.empty());
  assert(matrix1[0].size() == matrix2.size());
  result.resize(matrix1.size());
  for(unsigned i=0; i<result.size(); i++){
    result[i].resize(matrix2[0].size(), 0.0);
  }
  for(unsigned i=0; i<result.size(); i++){
    for(unsigned j=0; j<result[i].size(); j++){
      for(unsigned k=0; k<matrix2.size(); k++){
        result[i][j] += matrix1[i][k]*matrix2[k][j];
      }
    }
  }
  
  return result;
}

/**
* @brief a slightly modified routine for LU decomposition
* 
* @param[in,out] a square matrix to be transformed using LU decomposition
* @param[out] index vector of row permutations (is resized in this function)
*/
void luDecomposition(std::vector<std::vector<double> > &matrix, std::vector<int> &index)
{
  std::vector<double> pivotElements;
  const unsigned n = matrix.size();
  
  index.resize(n, 0);
  pivotElements.resize(n);
  
  //find pivot canditates
  for(unsigned i=0; i<n; i++){
    //search for maximum element in row
    double maxEl = 0.0;
    for(unsigned j=0; j<n; j++){
      maxEl= std::max(fabs(matrix[i][j]), maxEl);
    }
    if(maxEl == 0.0){
      throw SingularMatrixException();
    }
    pivotElements[i] = 1.0/maxEl;
  }
  
  unsigned imax = 0;
  for(unsigned j=0; j<n; j++){
    //calculate U part of LU-decomposition in column j: uij = aij- sum(lik*ukj),k=[0..i-1]
    for(unsigned i=0; i<j; i++){
      for(unsigned k=0; k<i; k++){
        matrix[i][j] -= matrix[i][k]*matrix[k][j];
      }
    }
    
    //calculate L part of LU-decomposition in column j: lij = (aij - sum(lik*ukj))/ujj,k=[0..j-1]
    //in this loop only aij-sum(lik*ukj) is calculated
    double maxEl = 0.0;
    for(unsigned i=j; i<n; i++){
      for(unsigned k=0; k<j; k++){
        matrix[i][j] -= matrix[i][k]*matrix[k][j];
      }
      double temp = pivotElements[i]*fabs(matrix[i][j]);
      if(temp >= maxEl){
        maxEl = temp;
        imax = i;
      }
    }
    //swap row with pivot element to top (row j)
    if(j != imax){
      for(unsigned k=0; k<n; k++){
        double temp = matrix[imax][k];
        matrix[imax][k] = matrix[j][k];
        matrix[j][k] = temp;
      }
      pivotElements[imax] = pivotElements[j];
    }
    index[j] = imax;
    
    //avoid too small pivot elements
    const double threshold = 1e-20;
    if(fabs(matrix[j][j]) < threshold){
      if(matrix[j][j] < 0.0){
        matrix[j][j] = -threshold;
      }
      else{
        matrix[j][j] = threshold;
      }
    }
    //divide column below j with ujj
    double temp = 1.0/matrix[j][j];
    for(unsigned i=j+1; i<n; i++){
      matrix[i][j] *= temp;
    }
  }
}


/**
* @brief solving Ax=b where A is LU decomposited matrix and b a given vector
*
* @param[in] matrix LU-decompisited matrix used for back substitution (nxn matrix)
* @param[in] index permutation vector of size n from LU-decomposition (b has to be permutated accordingly)
* @param[in,out] b right hand side vector on entry, solution on exit
* @note matrix and index are output of function luDecomposition
*/
void luBackSubstitution(      std::vector<std::vector<double> > matrix,
                        const std::vector<int> &index,
                              std::vector<double> &b)
{
  unsigned n = matrix.size();
  
  //apply permutation vector index to vector b
  for(unsigned i=0; i<n; i++){
    double temp = b[index[i]];
    b[index[i]] = b[i];
    b[i] = temp;
  }
  
  //first solve the equation Ly=Pb, where L is the lower triangle of matrix and P is index
  std::vector<double> &y = b; //write in place
  for(unsigned i=0; i<n; i++){
    for(unsigned j = 0; j<i; j++){
      y[i] -= matrix[i][j]*b[j];
    }
  }
  
  //Solve the equation Ux = y
  std::vector<double> &x = y; //write in place
  for(int i=int(n-1); i>=0; i--){
    for(int j = i+1; j<int(n); j++){
      x[i] -= matrix[i][j]*y[j];
    }
    x[i] /= matrix[i][i];
  }
}




/**
* @brief compute inverse of a square matrix
* @param A input matrix (square)
* @return inverse of input matrix
*/
std::vector<std::vector<double> > invertMatrix(std::vector<std::vector<double> > &matrix)
{
  assert(!matrix.empty());
  assert(matrix.size() == matrix[0].size());
  size_t n = matrix.size();
  std::vector<std::vector<double> > result(n);
  //construct unit matrix as result
  for(unsigned i=0; i<n; i++){
    result[i].resize(n, 0.0);
    result[i][i] = 1.0;
  }

  std::vector<int> index(n, 0);
  std::vector<std::vector<double> > copy = matrix;
  luDecomposition(copy, index);
  for(size_t i=0; i<n; i++){
    //this function expects a column vector - we provide a row vetor - so we have to transpose the result
    luBackSubstitution(copy, index, result[i]);
  }
  
  //transpose matrix (could be extracted into a function)
  for(unsigned i=0; i<n; i++){
    for(unsigned j=i+1; j<n; j++){
      double temp = result[i][j];
      result[i][j] = result[j][i];
      result[j][i] = temp;
    }
  }
  return result;
}