/**
 * @author hapi01
 */

#include "ArrayAlgorithm.hpp"
#include "ArrayBlas.hpp"

namespace utils {
  void scal(Array<double>& x, double c)
  {
    int inc = 1;
    int size = x.getSize();
    dscal_(&size, &c, x.getData(), &inc);
  }

  void daxpy(const Array<double>& x, double a, Array<double>& y)
  {
    assert(x.getSize()==y.getSize());
    int inc = 1;
    int size = x.getSize();
    daxpy_(&size, &a, x.getData(), &inc, y.getData(), &inc);
  }
}
