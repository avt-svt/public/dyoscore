/** \File WaveletTransform.cpp
   wavelet transformation.
  =====================================================================\n
 % (C) Lehrstuhl fuer Prozesstechnik, RWTH Aachen                      \n
  =====================================================================\n
  DyOS - Tool for dynamic optimization                                 \n
  =====================================================================\n
  This file contains functions for the wavelet transformation.         \n
  Currently Haar and hat wavelets are implemented.                     \n
  =====================================================================\n
  \author Martin Schlegel, Klaus Stockmann                             \n
  =====================================================================\n
*/



#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <vector>

#include "WaveletTransform.h"




/* ..  MATRIZEN DES LEVELS 1, DENSE*/
static double Mj01[5+1][3+1];
static double Mj11[5+1][2+1];
static double Gj01[3+1][5+1];
static double Gj11[2+1][5+1];

/* ..  Matizen des levels 2, dense */
static double Mj02[9+1][5+1];
static double Mj12[9+1][4+1];
static double Gj02[5+1][9+1];
static double Gj12[4+1][9+1];

/* ..  Matizen des levels 3, dense */
static double Mj03[17+1][9+1];
static double Mj13[17+1][8+1];
static double Gj03[9+1][17+1];
static double Gj13[8+1][17+1];

static double AU[2+1][5+1];
static double AL[2+1][5+1];
static double AM[1+1][5+1];
static double BU[1+1][3+1];
static double BL[1+1][3+1];
static double BM[1+1][3+1];
static double ATU[2+1][2+1];
static double ATL[2+1][2+1];
static double ATMU[1+1][2+1];
static double ATML[1+1][2+1];
static double BTU[4+1][3+1];
static double BTL[4+1][3+1];
static double BTMU[1+1][3+1];
static double BTML[1+1][3+1];


/* prototypes of local functions */
/* haar wavelets */

/* hat wavelets */
static void hat_init();
static void DecompG(double *signal, int n, int skale);
static void DecScal(double *signal, int n, int j,double *work, int m);
static void DecWave(double *signal, int j, double *work, int m);
static void ReconG(double *signal, int nn, int skale);






/* -------------------------------------------------------------------- */
/*    ROUTINE WHICH COMPUTES THE HAAR WAVELET TRANSFORM */
/* -------------------------------------------------------------------- */


/*    Unterprogramm Wavelet-Transformation mit orthonormalen Haar-Wavelets */

/*    Parameter(Koeffizientenvektor,Anzahl,Transformationsrichtung)        */

/*     1 -> Einzelskalen in Multiskalendarstellung                         */
/*    -1 -> Multiskalen in Einzelskalendarstellung                         */


/**
   * @brief routine which computes the Haar Wavelet Transform
   * @param a array with signal (in- and output)
   * @param n length of a
   * @param isign select direction of wavelet transform:  1: single scale -> multi-scale
   *                                                     -1: multi-scale  -> single scale
   * routine for wavelet transform with orthonormal Haar-wavelets, transform is carried out
   * in place.
   */

void haar_fwt(double a[], int n, int isign)
{

  const double zwei = 2.0;
  const double C12 = 1.0/sqrt(zwei);
  int i, j;
  int nn, nh;

  std::vector<double> wksp(n, 0.0);
  
  if (isign >=0) {
    nn=n;
    while (nn>=2) {
      nh=nn/2;
      i=0;
      for (j=0; j<nn; j+=2) {
	wksp[i]=C12*(a[j]+a[j+1]);
	wksp[i+nh]=C12*(a[j]-a[j+1]);
	i +=1;
      }
      for (int ii=0;ii<nn;ii++) a[ii]=wksp[ii];
      nn=nn/2;
    }
  }
  else {
    nn=2;
    while (nn<=n) {
      nh=nn/2;
      j=0;
      for (i=0;i<nh;i++) {
	wksp[j]=C12*(a[i]+a[i+nh]);
	wksp[j+1]=C12*(a[i]-a[i+nh]);
	j=j+2;
      }
      for (int ii=0;ii<nn;ii++) a[ii]=wksp[ii];
      nn=nn*2;
    }
  }

  for (int ii=0; ii<n; ii++) a[ii] = wksp[ii];
 
} /* end of function  haar_fwt */


/* ****************************************************************** */
/* here begins code performing the hat-wavelet transform              */
/* ****************************************************************** */

/*     void hat_fwt(signal,n,isign)                                   */
/* ..  Hutfunktion-Wavelet-Transformation                             */
/* ..                                                                 */
/*     1 -> Einzelskalen in Multiskalendarstellung                    */
/*    -1 -> Multiskalen in Einzelskalendarstellung                    */
/* ..                                                                 */


/**
   * @brief routine which computes the Hat Wavelet Transform
   * @param signal array with signal (in- and output)
   * @param n length of signal
   * @param isign select direction of wavelet transform:  1: single scale -> multi-scale
   *                                                     -1: multi-scale  -> single scale
   * routine for wavelet transform with Hat-Wavelets, transform is carried out
   * in place.
   */

void hat_fwt(double *signal, int n, int isign)
{

  int skale;

  hat_init();

  skale=(int)(log10((double)n)/log10(2.0));

  if (((1<<skale)+1 - n) != 0) {
    printf("\n Error in routine hat_fwt: wrong length of data vector");
    printf("\n scale = %i, n = %i, isign = %i",skale,n,isign);
    exit(0);
  }

  if (isign == 1) DecompG(signal, n, skale);
  else            ReconG(signal, n, skale);
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~Decomposition~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
   * @brief decomposition of Hat-Wavelets
   * @param signal array with signal (in- and output)
   * @param n length of signal
   * @param skale scale to which signal is decomposed 
   * routine for decomposition of Hat-Wavelets, i.e. for single scale to multi-scale transform.
   * Transform is carried out in place.
   */
static void DecompG(double *signal, int n, int skale)
{


/*  input     signal     signal, Vektor der L"ange 2^(j)+1 bestehend */
/*            skale      aus 2^skale+1  Skalierungsfunktionkoeffizienten */

/*            n          Laenge des Signals  */

/*  output    signal     Signal der L'ange 2^(j)+1 bestehend aus */
/*                       2^(skale) +1 Skalierungsfunktionskoeffizienten */

  int i, j, m, s;

  std::vector<double> work(n+1, 0.0);

  s = n;
  m = (n-1)/2;
  for (i=skale;i>=2;i--) {
    /* DecScal(signal,s,i,work[1],m+1); */
    DecScal(signal, s, i, &work[0], m+1);
    //DecScal(signal, s, i, work, m+1);
    /* DecWave(signal,s,i,work[m+2],m); */
    DecWave(signal, i, &work[(m+2)-1], m);
    //DecWave(signal, i, work+(m+2)-1, m);
    for (j=1;j<=s;j++) signal[j] = work[j];
    s=s-m;
    m=m/2;
  }
 
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~DecScal~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   */

/*  DecScal zerlegt das eingehende Signal in den Skalierungs-          */
/*    funktionenraum einen Level tiefer.                               */

/*  AU ist die 'upper' Matrix von (G_j0 )  mit den Randkoeffizienten   */
/*  AL ist die 'lower' Matrix von (G_j0 )  mit den Randkoeffizienten   */
/*  AM  sind die Filterkoeffizienten                                   */



/**
   * @brief decomposes haar wavelets to next scaling function level (helper function for DecompG)
   * @param signal array with signal (in- and output)
   * @param n signal length
   * @param j level
   * @param work work space vector
   * @param m length of work space vector
   *
   */

static void DecScal(double *signal, int n, int j, double *work, int m)
{
  int l, k;

  //@todo replace exit by assert
  if (j < 2) {
    printf("ScaleDec: level j muss >= 2 gewaehlt werden");
    exit(0);
  }

  /* ................. Level 2 auf Level 1 */
  if (j < 3) {
    for (l=1;l<=3;l++) {
      work[l] = 0;
      for (k=1;k<=5;k++) work[l] += Gj01[l][k] * signal[k];
    }
  }

  /* ................. Level 3 auf Level 2 */
  else if (j < 4) {
    for(l=1;l<=5;l++) {
      work[l] = 0;
      for (k=1;k<=9;k++) work[l] += Gj02[l][k] * signal[k];
    }
  }

  /* .................. sonst */
  else {
    for (l=1;l<=2;l++) {
      work[l] = 0;
      work[m+1-l] = 0;
      for (k=1;k<=5;k++) {
	work[l] += AU[l][k] * signal[k];
	work[m+1-l] += AL[3-l][k] * signal[n-5+k];
      }
    }

    for(l=3;l<=m-2;l++) {
      work[l] = 0;
      for (k=1;k<=5;k++) work[l] += signal[2*l+2-k] * AM[1][k];
    }
  }
} /* end of function DecScal*/


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~DecWave~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  */

/* .. DecWave zerlegt das eingehende Signal in den Waveletfunktionen   */
/*    raum einen Level tiefer.                                         */


/* .. Waveletfunktionen                                                */
/* .. BU ist die 'upper' Matrix von (G_j1 )  mit den Randkoeffizienten */
/* .. BL ist die 'lower' Matrix von (G_j1 )  mit den Randkoeffizienten */
/* .. BM  sind die Filterkoeffizienten                                 */


/**
   * @brief decomposes haar wavelets to next wavelet level (helper function for DecompG)
   * @param signal array with signal (in- and output)
   * @param j level
   * @param work work space vector
   * @param m length of work space vector
   *
   */

static void DecWave(double *signal, int j, double *work, int m)
{
  int l, k;

  if (j < 2) {
    printf("ScaleDec: level j muss >= 2 gewaehlt werden");
    exit(0);
  }

  /* ................. Level 2 auf Level 1 */
  if (j < 3) {
    for(l=1;l<=2;l++) {
      work[l] = 0;
      for(k=1;k<=5;k++) work[l] += Gj11[l][k] * signal[k];
    }
  }

  /* ................. Level 3 auf Level 2 */
  else if (j < 4) {
    for (l=1;l<=4;l++) {
      work[l] = 0;
      for (k=1;k<=9;k++) work[l] += Gj12[l][k] * signal[k];
    }
  }

  /* .................. sonst */
  else {
    for(l=1;l<=m;l++) {
      work[l] = 0;
      for(k=1;k<=3;k++) work[l] += BM[1][k] * signal[2*l+2-k];
    }
  }
} /* end of function DecWave*/


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~ReconG~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
   * @brief reconstruction of Hat-Wavelets
   * @param signal array with signal (in- and output)
   * @param nn length of signal
   * @param skale scale from which signal is reconstructed 
   * routine for reconstruction of Hat-Wavelets, i.e. for multe-scale to single scale transform.
   * Transform is carried out in place.
   */
static void ReconG(double *signal, int nn, int skale)
{

  /* .. input     signal     signal, Vektor der L"ange 2^(j)+1 bestehend     */
  /* ..           skale      aus 3 Skalierungsfunktionskoeffizienten und     */
  /* ..                       2^skale-2  Waveletkoeffizienten                */
  /* ..                                                                      */
  /* ..           nn          Laenge des Signals                              */
  /* ..                                                                      */
  /* .. output    signal     Signal der L'ange 2^(j)+1 bestehend aus         */
  /* ..                      2^(skale) +1 Skalierungsfunktionskoeffizienten  */


  /* include 'commonMat.inc' */
  int n, j, m, l, k;
  
  if (skale == 1) {
    return;
  }

  std::vector<double> workS(nn+1, 0.0), workW(nn+1, 0.0);

  if (skale < 1) {
    printf("Routine ReconG: Datenvektor zu kurz, level=0");
    exit(0);
  }


  /* .............. Level 1 auf Level 2 */
  for(l=1;l<=5;l++) {
    workS[l] = 0;
    for(k=1;k<=3;k++) workS[l] += Mj01[l][k] * signal[k];
  }

  for(l=1;l<=5;l++) {
    workW[0];
    for(k=1;k<=2;k++) workW[l] += Mj11[l][k] * signal[3+k];
  }

  for(l=1;l<=5;l++) signal[l] = workS[l] + workW[l];


  if (skale == 2) {
    return;
  }

  /* .............. Level 2 auf Level 3 */
  for(l=1;l<=9;l++) {
    workS[l] = 0;
    for(k=1;k<=5;k++) workS[l] += Mj02[l][k] * signal[k];
  }

  for(l=1;l<=9;l++) {
    workW[l] = 0;
    for(k=1;k<=4;k++) workW[l] += Mj12[l][k] * signal[5+k];
  }

  for(l=1;l<=9;l++) signal[l] = workS[l] + workW[l];


  if (skale == 3) {
    return;
  }

  /* .............. sonst */
  for(j=3;j<=(skale-1);j++) {
    n = (1<<j)+1;
    m = 2*n -1;
    
    for(l=1;l<=2;l++) {
      workS[l] = 0;
      workS[m-2+l] = 0;
      for(k=1;k<=2;k++) {
	workS[l] += ATU[l][k] * signal[k];
	workS[m-2+l] += ATL[l][k] * signal[n-2+k];
      }
    }
    
    k = 3;
    
    for(l=2;l<=n-1;l++) {
      workS[k] = ATMU[1][1] * signal[l];
      workS[k+1] = ATML[1][1] * signal[l] + ATML[1][2] * signal[l+1];
      k += 2;
    }
    
    
    for (l=1;l<=4;l++) {
      workW[l] = 0;
      workW[m-4+l] = 0;
      for (k=1;k<=3;k++) {
	workW[l] += BTU[l][k] * signal[k+n];
	workW[m-4+l] += BTL[l][k] * signal[m-3+k];
      }
    }
    
    k = 5;
    
    for(l=3;l<=n-3;l++) {
      workW[k] = BTMU[1][1] * signal[n+l-1] + BTMU[1][2] * signal[n+l];
      workW[k+1] = BTML[1][1] * signal[n+l-1] + BTML[1][2] *signal[n+l]
	+ BTML[1][3] * signal[n+l+1];
      k+=2;
    }
    
    workW[k] = BTMU[1][1]*signal[m-2] + BTMU[1][2]*signal[m-1];
    
    for (k=1;k<=m;k++) signal[k] = workS[k] + workW[k];
  }
  
} /* end of function ReconG*/


/**
   * @brief initialization for Hat-Wavelet transform
   *
   */
void hat_init()
{

  double root;
  int i, j;

  //@warning wenn man bei den folgenden Hilfsmatritzen das keyword 'static' auskommentiert,
  // gibt es falsche ergebnisse fuer die adapt_structure Beispiele (kst, 26.05.08)

  /* Hilfsmatrizen */
  /*Matrizen des levels 1, dense */
  static double Mj01T[3+1][5+1];
  static double Mj11T[2+1][5+1];

  /* MATRIZEN DES LEVELS 2, DENSE */
  static double Mj02T[5+1][9+1];
  static double Mj12T[4+1][9+1];
  static double Gj02T[9+1][5+1];

  /* MATRIZEN DES LEVELS 3, DENSE */
  static double Mj03T[9+1][17+1];
  static double Mj13T[8+1][17+1];




  root = sqrt(2.0);

  Mj01T[1][1] = root/2.0; Mj01T[1][2] = root/4.0;
  Mj01T[2][2] = root/4.0; Mj01T[2][3] = root/2.0;
  Mj01T[2][4] = root/4.0; Mj01T[3][4] = root/4.0;
  Mj01T[3][5] = root/2.0;

  Mj11T[1][1] = -3.0/4.0; Mj11T[1][2] = 9.0/16.0;
  Mj11T[1][3] = -1.0/8.0; Mj11T[1][4] = -1.0/16.0;
  Mj11T[2][2] = -1.0/16.0; Mj11T[2][3] = -1.0/8.0;
  Mj11T[2][4] = 9.0/16.0; Mj11T[2][5] = -3.0/4.0;

  Gj01[1][1] = 5.0/8.0*root; Gj01[1][2] = 3.0/4.0*root;
  Gj01[1][3] = -3.0/8.0*root; Gj01[2][1] = -root/16.0;
  Gj01[2][2] = root/8.0; Gj01[2][3] = 7.0/8.0*root;
  Gj01[2][4] = root/8.0; Gj01[2][5] = -root/16.0;
  Gj01[3][3] = -3.0/8.0*root; Gj01[3][4] = 3.0/4.0*root;
  Gj01[3][5] = 5.0/8.0*root;

  Gj11[1][1]  =  -1.0/2.0; Gj11[1][2]  =  1;
  Gj11[1][3]  =  -1.0/2.0; Gj11[2][3]  =  -1.0/2.0;
  Gj11[2][4]  =  1; Gj11[2][5]  =  -1.0/2.0;
  /* level  2 */
  Mj02T[1][1]  =  root/2.0; Mj02T[1][2]  =  root/4.0;
  Mj02T[2][2]  =  root/4.0; Mj02T[2][3]  =  root/2.0;
  Mj02T[2][4]  =  root/4.0; Mj02T[3][4]  =  root/4.0;
  Mj02T[3][5]  =  root/2.0; Mj02T[3][6]  =  root/4.0;
  Mj02T[4][6]  =  root/4.0; Mj02T[4][7]  =  root/2.0;
  Mj02T[4][8]  =  root/4.0; Mj02T[5][8]  =  root/4.0;
  Mj02T[5][9]  =  root/2.0;

  Mj12T[1][1]  =  -3.0/4.0; Mj12T[1][2]  =  9.0/16.0;
  Mj12T[1][3]  =  -1.0/8.0; Mj12T[1][4]  =  -1.0/16.0;
  Mj12T[2][2]  =  -0.125E0; Mj12T[2][3]  =  -0.25E0;
  Mj12T[2][4]  =  0.75E0; Mj12T[2][5]  =  -0.25E0;
  Mj12T[2][6]  =  -0.125E0; Mj12T[3][4]  =  -0.125E0;
  Mj12T[3][5]  =  -0.25E0; Mj12T[3][6]  =  0.75E0;
  Mj12T[3][7]  =  -0.25E0; Mj12T[3][8]  =  -0.125E0;
  Mj12T[4][6]  =  -1.0/16.0; Mj12T[4][7]  =  -1.0/8.0;
  Mj12T[4][8]  =  9.0/16.0; Mj12T[4][9]  =  -3.0/4.0;

  Gj02T[1][1]  =  5.0/8.0*root;  Gj02T[1][2]  =  -root/16.0;
  Gj02T[2][1]  =  3.0/4.0*root;  Gj02T[2][2]  =  root/8.0;
  Gj02T[3][1]  =  -0.375E0*root; Gj02T[3][2]  =  0.8125E0*root;
  Gj02T[3][3]  =  -0.125E0*root; Gj02T[4][2]  =  0.25E0*root;
  Gj02T[4][3]  =  0.25E0*root;   Gj02T[5][2]  =  -0.125E0*root;
  Gj02T[5][3]  =  0.75E0*root;   Gj02T[5][4]  =  -0.125E0*root;
  Gj02T[6][3]  =  0.25E0*root;   Gj02T[6][4]  =  0.25E0*root;
  Gj02T[7][3]  =  -0.125E0*root; Gj02T[7][4]  =  0.8125E0*root;
  Gj02T[7][5]  =  -0.375E0*root; Gj02T[8][4]  =  root/8.0;
  Gj02T[8][5]  =  3.0/4.0*root;  Gj02T[9][4]  =  -root/16;
  Gj02T[9][5]  =  5.0/8.0*root;

  Gj12[1][1]  =  -1.0/2.0; Gj12[1][2]  =  1;
  Gj12[1][3]  =  -1.0/2.0; Gj12[2][3]  =  -1.0/2.0;
  Gj12[2][4]  =  1;        Gj12[2][5]  =  -1.0/2.0;
  Gj12[3][5]  =  -1.0/2.0; Gj12[3][6]  =  1;
  Gj12[3][7]  =  -1.0/2.0; Gj12[4][7]  =  -1.0/2.0;
  Gj12[4][8]  =  1;        Gj12[4][9]  =  -1.0/2.0;

  /* level  3 */
  Mj03T[1][1]  =  root/2.0;  Mj03T[1][2]  =  root/4.0;
  Mj03T[2][2]  =  root/4.0;  Mj03T[2][3]  =  root/2.0;
  Mj03T[2][4]  =  root/4.0;  Mj03T[3][4]  =  root/4.0;
  Mj03T[3][5]  =  root/2.0;  Mj03T[3][6]  =  root/4.0;
  Mj03T[4][6]  =  root/4.0;  Mj03T[4][7]  =  root/2.0;
  Mj03T[4][8]  =  root/4.0;  Mj03T[5][8]  =  root/4.0;
  Mj03T[5][9]  =  root/2.0;  Mj03T[5][10]  =  root/4.0;
  Mj03T[6][10]  =  root/4.0; Mj03T[6][11]  =  root/2.0;
  Mj03T[6][12]  =  root/4.0; Mj03T[7][12]  =  root/4.0;
  Mj03T[7][13]  =  root/2.0; Mj03T[7][14]  =  root/4.0;
  Mj03T[8][14]  =  root/4.0; Mj03T[8][15]  =  root/2.0;
  Mj03T[8][16]  =  root/4.0; Mj03T[9][16]  =  root/4.0;
  Mj03T[9][17]  =  root/2.0;

  Mj13T[1][1]  =  -3.0/4.0;   Mj13T[1][2]  =  9.0/16.0;
  Mj13T[1][3]  =  -1.0/8.0;   Mj13T[1][4]  =  -1.0/16.0;
  Mj13T[2][2]  =  -0.125E0;   Mj13T[2][3]  =  -0.25E0;
  Mj13T[2][4]  =  0.75E0;     Mj13T[2][5]  =  -0.25E0;
  Mj13T[2][6]  =  -0.125E0;   Mj13T[3][4]  =  -0.125E0;
  Mj13T[3][5]  =  -0.25E0;    Mj13T[3][6]  =  0.75E0;
  Mj13T[3][7]  =  -0.25E0;    Mj13T[3][8]  =  -0.125E0;
  Mj13T[4][6]  =  -0.125E0;   Mj13T[4][7]  =  -0.25E0;
  Mj13T[4][8]  =  0.75E0;     Mj13T[4][9]  =  -0.25E0;
  Mj13T[4][10]  =  -0.125E0;  Mj13T[5][8]  =  -0.125E0;
  Mj13T[5][9]  =  -0.25E0;    Mj13T[5][10]  =  0.75E0;
  Mj13T[5][11]  =  -0.25E0;   Mj13T[5][12]  =  -0.125E0;
  Mj13T[6][10]  =  -0.125E0;  Mj13T[6][11]  =  -0.25E0;
  Mj13T[6][12]  =  0.75E0;    Mj13T[6][13]  =  -0.25E0;
  Mj13T[6][14]  =  -0.125E0;  Mj13T[7][12]  =  -0.125E0;
  Mj13T[7][13]  =  -0.25E0;   Mj13T[7][14]  =  0.75E0;
  Mj13T[7][15]  =  -0.25E0;   Mj13T[7][16]  =  -0.125E0;
  Mj13T[8][14]  =  -1.0/16.0; Mj13T[8][15]  =  -1.0/8.0;
  Mj13T[8][16]  =  9.0/16.0;  Mj13T[8][17]  =  -3.0/4.0;

  Gj03[1][1]  =  5.0/8.0*root;   Gj03[1][2]  =  3.0/4.0*root;
  Gj03[1][3]  =  -0.375E0*root;  Gj03[2][1]  =  -root/16;
  Gj03[2][2]  =  root/8;         Gj03[2][3]  =  0.8125E0*root;
  Gj03[2][4]  =  0.25E0*root;    Gj03[2][5]  =  -0.125E0*root;
  Gj03[3][3]  =  -0.125E0*root;  Gj03[3][4]  =  0.25E0*root;
  Gj03[3][5]  =  0.75E0*root;    Gj03[3][6]  =  0.25E0*root;
  Gj03[3][7]  =  -0.125E0*root;  Gj03[4][5]  =  -0.125E0*root;
  Gj03[4][6]  =  0.25E0*root;    Gj03[4][7]  =  0.75E0*root;
  Gj03[4][8]  =  0.25E0*root;    Gj03[4][9]  =  -0.125E0*root;
  Gj03[5][7]  =  -0.125E0*root;  Gj03[5][8]  =  0.25E0*root;
  Gj03[5][9]  =  0.75E0*root;    Gj03[5][10]  =  0.25E0*root;
  Gj03[5][11]  =  -0.125E0*root; Gj03[6][9]  =  -0.125E0*root;
  Gj03[6][10]  =  0.25E0*root;   Gj03[6][11]  =  0.75E0*root;
  Gj03[6][12]  =  0.25E0*root;   Gj03[6][13]  =  -0.125E0*root;
  Gj03[7][11]  =  -0.125E0*root; Gj03[7][12]  =  0.25E0*root;
  Gj03[7][13]  =  0.75E0*root;   Gj03[7][14]  =  0.25E0*root;
  Gj03[7][15]  =  -0.125E0*root; Gj03[8][13]  =  -0.125E0*root;
  Gj03[8][14]  =  0.25E0*root;   Gj03[8][15]  =  0.8125E0*root;
  Gj03[8][16]  =  root/8;        Gj03[8][17]  =  -root/16;
  Gj03[9][15]  =  -0.375E0*root; Gj03[9][16]  =  3.0/4.0*root;
  Gj03[9][17]  =  5.0/8.0*root;

  Gj13[1][1]  =  -1.0/2.0;  Gj13[1][2]  =  1;
  Gj13[1][3]  =  -1.0/2.0;  Gj13[2][3]  =  -1.0/2.0;
  Gj13[2][4]  =  1;         Gj13[2][5]  =  -1.0/2.0;
  Gj13[3][5]  =  -1.0/2.0;  Gj13[3][6]  =  1;
  Gj13[3][7]  =  -1.0/2.0;  Gj13[4][7]  =  -1.0/2.0;
  Gj13[4][8]  =  1;         Gj13[4][9]  =  -1.0/2.0;
  Gj13[5][9]  =  -1.0/2.0;  Gj13[5][10]  =  1;
  Gj13[5][11]  =  -1.0/2.0; Gj13[6][11]  =  -1.0/2.0;
  Gj13[6][12]  =  1;        Gj13[6][13]  =  -1.0/2.0;
  Gj13[7][13]  =  -1.0/2.0; Gj13[7][14]  =  1;
  Gj13[7][15]  =  -1.0/2.0; Gj13[8][15]  =  -1.0/2.0;
  Gj13[8][16]  =  1;        Gj13[8][17]  =  -1.0/2.0;


/* ............ Transponieren der Matrizen [warum auch immer ... ) */

for (i=1;i<=3;i++) {
  for (j=1;j<=5;j++)  Mj01[j][i]=Mj01T[i][j];
}

for (i=1;i<=2;i++) {
  for (j=1;j<=5;j++)  Mj11[j][i]=Mj11T[i][j];
}

for (i=1;i<=5;i++) {
  for (j=1;j<=9;j++)  Mj02[j][i]=Mj02T[i][j];
}

for (i=1;i<=4;i++) {
  for (j=1;j<=9;j++)  Mj12[j][i]=Mj12T[i][j];
}

for (i=1;i<=9;i++) {
  for (j=1;j<=5;j++)  Gj02[j][i]=Gj02T[i][j];
}

for (i=1;i<=9;i++) {
  for (j=1;j<=17;j++)  Mj03[j][i]=Mj03T[i][j];
}

for (i=1;i<=8;i++) {
  for (j=1;j<=17;j++)  Mj13[j][i]=Mj13T[i][j];
}

for (i=1;i<=2;i++) {
  for (j=1;j<=5;j++)  AU[i][j]=Gj03[i][j];
}

for (i=8;i<=9;i++) {
  for (j=13;j<=17;j++)  AL[i-7][j-12]=Gj03[i][j];
}


for (j=3;j<=7;j++) AM[1][j-2] = Gj03[3][j];
for (j=1;j<=3;j++) BU[1][j] = Gj13[1][j];
for (j=15;j<=17;j++) BL[1][j-14] = Gj13[8][j];
for (j=3;j<=5;j++) BM[1][j-2] = Gj13[2][j];

for (i=1;i<=2;i++) {
  for (j=1;j<=2;j++)  ATU[i][j]=Mj03[i][j];
}

for (i=16;i<=17;i++) {
  for (j=8;j<=9;j++)  ATL[i-15][j-7]=Mj03[i][j];
}

for (j=2;j<=3;j++) ATMU[1][j-1] = Mj03[3][j];
for (j=2;j<=3;j++) ATML[1][j-1] = Mj03[4][j];

for (i=1;i<=4;i++) {
  for (j=1;j<=3;j++)  BTU[i][j]=Mj13[i][j];
}

for (i=14;i<=17;i++) {
  for (j=6;j<=8;j++)  BTL[i-13][j-5]=Mj13[i][j];
}

for (j=2;j<=4;j++) BTMU[1][j-1] = Mj13[5][j];
for (j=2;j<=4;j++) BTML[1][j-1] = Mj13[6][j];

}
