/**  @file UtilityFunctions.cpp
*    @brief Sources of various utility functions
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 18.06.2012
*/
#include <cassert>
#include <cstddef>
#include <math.h>
#include <cstdlib>
#include <algorithm>

#include "UtilityFunctions.hpp"

namespace utils {
  // utility functions at the bottom

  /**
  * @brief special purpose rounding function
  *
  * this is a very specific rounding function which has been
  * introduced solely due to numerical problems (only in debug mode) in
  * computing the index JJ in function 'Indices2Gridpoints', respectivly.
  * E.g. log(8.0)/log(2.0) in debug mode yielded 2.9999999999999996 instead of
  * the true value of 3.0. In consequence, int(log(8.0)/log(2.0)) resulted in 2.0 (not in 3.0).
  * To remedy this behaviour, function mround() rounds towards the next larger integer, in case
  * that the difference of the input and the next larger int is less than a small tolerance.
  * @param[in] input double value to be rounded
  * @return rounded value
  */
  double mround(const double value)
  {
    const double resFactor = 0.1;
    double residual = resFactor * 1.0 / pow(2.0, 10.0);

    if ((ceil(value) - value) < residual){
      return ceil(value);
    }
    else{
      return value;
    }
  }
  /**
  * @brief counts consecutive zeros of a number in binary representation
  *
  * determines the number of consecutive zeros (counting direction: LSB->MSB)
  * of a number in binary representation until the first '1' is encountered.
  * @param[in] value number to be processed
  * @return number of consecutive zeros
  */
  int numBinaryZeros(const int value)
  {
    int negationValue = value;
    negationValue =~ negationValue;
    int nzeros=0, i=0;
    while(negationValue & (1<<i)){
      nzeros++;
      i++;
    }
    return nzeros;
  }


  /* function double *interp() performs interpolation (and extrapolation) of tabulated
  values (x0[i],y0[i]) with i=0..n0-1  at points given in vector xn[0..nn-1];
  Order is the polynomial order (^= polynomial degree +1)
  i.e. if Order==1, piecewise constant interpolation (or extrapolation) is performed
  if Order==2, piecewise linear interpolation (or extrapolation) is performed, etc. */

/**
* @brief performs interpolation (and extrapolation) of tabulated
* values (x0[i],y0[i]) with i=0..n0-1  at points given in vector xn[0..nn-1];
*
* Order is the polynomial order (^= polynomial degree +1)\n
* i.e. if Order==1, piecewise constant interpolation (or extrapolation) is performed\n
* if Order==2, piecewise linear interpolation (or extrapolation) is performed, etc.\n
* @param x0 arraypointer to x-values of the tabulated points
* @param y0 arraypointer to y-values of the tabulated points
* @param xn arraypointer to x-values of the points to be interpolated
* @param Ord order of the interpolation
* @return arraypointer to y-values of the interpolated points
*/
std::vector<double> interp(const double *x0, const double *y0, const int n0,
  const double *xn, const int nn, const int Ord_in)
{

  double y,dy;
  int z,l;
  int Ord;

  Ord = Ord_in;

  int Index; /* mind that Index has unit-offset!*/
  std::vector<double> yn(nn);

  if (n0<Ord) {
    /* my_fprintf(stdout,"\n Input array should have at least %d elements; Reducing the order \n",Ord);*/
    Ord=n0;
  }

  for (z=0;z<nn;z++){
    Index=1;
    std::vector<double> test(n0);
    for(int i=0; i<n0; i++){
      test[i] = x0[i];
    }
    //search for the index of the first value in x0 that is greater than xn[z]
    //later we take the predecessor
    std::vector<double>::iterator up = std::upper_bound(test.begin(), test.end(), xn[z]);
    Index = up - test.begin();

    /* get Index of xn[z]
    if Index==0:  xn[z]< x0[0]
    if Index==n0: xn[z]> x0[n0-1]
    if Index>0 or <n0, then xn[z] is between x0[Index-1] and x0[Index] */

    if (Ord == 1 && z == nn-1 && xn[nn-1] == x0[n0-1]){
     Index = n0; 
     }
     /* Last point has to be interpreted as
        "out of range" for piecewise constant
        => subsequent correction of upper_bound 
        so set Index on highest valid value*/
    if(xn[z]==x0[Index-1]){ /* if input point coincides with tabulated point, */
      yn[z]=y0[Index-1];    /* just retrieve tabulated value                  */
    }
    else{                   /* perform interpolation*/
      l = std::min(std::max(Index-(Ord-1)/2,1),n0+1-Ord)-1;
      
      //int errcode = polint(&x0[l]-1,&y0[l]-1,Ord,xn[z],&y,&dy);
      try{
        y = nevilleInterpolation(&x0[l], &y0[l], Ord, xn[z]);
      }
      catch(...){
        yn.clear();
        return yn;
      }
      yn[z]=y;
    }
  }
  return yn;
}


/**
*
*/
double nevilleInterpolation(const double *x, const double *y, const unsigned n, const double t)
{
  std::vector<double> g(n);
  for(unsigned i=0; i<n; i++){
    g[i] = y[i];
  }
  for(unsigned i=1; i<n; i++){
    for(unsigned j = n-1; j>=i; j--){
      if(x[j] == x[j-i]){
        throw -1;
      }
      g[j] = ((t-x[j-i])*g[j] - (t-x[j])*g[j-1])/(x[j]-x[j-i]);
    }
  }
  return g[n-1];
}

  /* function copy/
  * @param iCopySize
  * @param src
  * @param dest
  */
  //void copy(int iCopySize, std::vector <double> src, std::vector <double> dest)
  //{
  //  std::copy(src.begin(), src.begin()+iCopySize, dest.begin());
  //}

  void copy(const int iCopySize, const double *src, double *dest)
  {
    std::copy(src, src+iCopySize, dest);
  }

  /* function copy/
  * @param iCopySize
  * @param src
  * @param dest
  */
  void copy(const int iCopySize, const int *src, int *dest)
  {
    std::copy(src, src+iCopySize, dest);
  }

  /** @brief compares two double with respect to a tolerance 
  * @param[in] val1 first value
  * @param[in] val2 second value
  * @param[in] tol double comparison tolerance 
  * @return true if the values are close
  */
  bool compDbl(const double val1, const double val2, const double tol)
  {
    return fabs(val1-val2)<tol;
  }

  /** @brief finds the value to the key "val"
  * @param[in] mapArray map which is searched
  * @param[in] val key to be found
  * @return value of the key "val"
  */
  double findMapKey(const std::map<double, double> &mapArray, const double val)
  {
    std::map<double, double>::const_iterator it;
    for(it=mapArray.begin(); it!=mapArray.end(); ++it){
      if(compDbl(it->first, val))
        return it->second;
    }
    assert(false);
    return 0.0;
  }

  std::map<double, double>::const_iterator findMapIter(const std::map<double, double> &mapArray,
                                                       const double val)
  {
    std::map<double, double>::const_iterator it;
    for(it=mapArray.begin(); it!=mapArray.end(); ++it){
      if(compDbl(it->first, val))
        return it;
    }
    assert(false);
    return it;
  }

  /** @brief finds entry "val" in vector "vec" and returns the position */
  int findEntry(const double val, const std::vector<double> &vec)
  {
    for(unsigned i=0; i<vec.size(); i++)
    {
      if(compDbl(val, vec[i]))
        return i;
    }
    return -1;
  }


}