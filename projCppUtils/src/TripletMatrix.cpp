
#include "TripletMatrix.hpp"
#include "CscMatrix.hpp"
#include "MatrixExceptions.hpp"

#include <algorithm>
#include <assert.h>
#include <vector>
#include <sstream>

cs *decompress(const cs *cscmat)
{
  if(!cscmat){
    return NULL;
  }
  assert(CS_CSC(cscmat));
  cs *coomat;
  const int m = cscmat->m;
  const int n = cscmat->n;
  const int nnz = cscmat->p[n];
  int values = 1;
  if(!cscmat->x){
    values = 0;
  }
  const int triplet = 1; 
  coomat = cs_spalloc(m, n, nnz, values, triplet);

  // convert
  int counter = 0;
  int startInd, endInd;
  for (int j=0; j<n; j++) {
    startInd = cscmat->p[j];
    endInd = cscmat->p[j+1];
    for (int i=startInd; i<endInd; i++) {
      coomat->i[counter] = cscmat->i[i];
      coomat->p[counter] = j;
      if(cscmat->x){
        coomat->x[counter] = cscmat->x[counter];
      }
      counter++;
    }
  }
  coomat->nz = counter;
  assert(coomat->nz == nnz);
  return coomat;
}

CsTripletMatrix::CsTripletMatrix()
{
  m_mat=cs_spalloc(0,0,0,true, true);
  owner = true;
}

CsTripletMatrix::CsTripletMatrix(int n_rows,
                                 int n_cols,
                                 int nnz,
                                 const int* row_idx,
                                 const int* col_idx,
                                 const double* values)
  : m_mat(cs_spalloc(n_rows, n_cols, nnz, (values ? 1 : 0), true)), owner(true)
{
  m_mat->nz = nnz;
  assert(row_idx);
  assert(col_idx);
  std::copy(row_idx, row_idx+nnz, m_mat->i);
  std::copy(col_idx, col_idx+nnz, m_mat->p);
  if (values)
    std::copy(values, values+nnz, m_mat->x);
}

CsTripletMatrix::CsTripletMatrix(cs* mat, bool owner_)
  : m_mat(CS_CSC(mat) ? decompress(mat) : mat), owner(CS_CSC(mat) ? true : owner_)
{
}

CsTripletMatrix::~CsTripletMatrix()
{
  if (owner)
    cs_spfree(m_mat);
}

CsCscMatrix::Ptr CsTripletMatrix::compress() const
{
  return CsCscMatrix::Ptr(new CsCscMatrix(cs_compress(m_mat)));
}

void CsTripletMatrix::getVertCatMatrixData(CsTripletMatrix::Ptr matrix,
                                           int *rowIndices,
                                           int *colIndices,
                                           double *values)
{
  assert(this->get_nnz() > -1); // coo-matrix expected
  assert(matrix->get_nnz() > -1);
  if(this->get_n_cols() != matrix->get_n_cols()){
    std::stringstream sstream;
    sstream<<"Function vertcat not possible on given matrices - column dimensions do not match: "<<std::endl;
    sstream<<"this matrix: "<<this->get_n_cols()<<" columns"<<std::endl;
    sstream<<"appended matrix: "<<matrix->get_n_cols()<<" colums"<<std::endl;
    throw MatricesNotCompatibleException(sstream.str());
  }
  
  //add data of first matrix
  for(int i=0; i<this->get_nnz(); i++){
    values[i] = this->get_matrix_ptr()->x[i];
    rowIndices[i] = this->get_matrix_ptr()->i[i];
    colIndices[i] = this->get_matrix_ptr()->p[i];
  }

  //add data of second matrix
  int offset = this->get_n_rows();
  int thisNnz = this->get_nnz();

  for(int i=0; i<matrix->get_nnz(); i++){
    values[i+thisNnz] = matrix->get_matrix_ptr()->x[i];
    rowIndices[i+thisNnz] = (matrix->get_matrix_ptr()->i[i])+offset;//second matrix is located under first matrix
    colIndices[i+thisNnz] = matrix->get_matrix_ptr()->p[i];
  }
}


/**
* @brief append the rows of function argument matrix to this matrix
*
* 
* @note vertcat is a bit confusing name, since the matrices are concatenated horizontally
*/
CsTripletMatrix::Ptr CsTripletMatrix::vertcat(CsTripletMatrix::Ptr matrix)
{
  if(this->get_matrix_ptr() == NULL){
    throw MatrixNotInitializedException();
  }
  if(matrix->get_matrix_ptr() == NULL){
    throw MatrixNotInitializedException("Argument for function vertcat has not been initialized");
  }

  const int nnz = this->get_nnz() + matrix->get_nnz();
  int rows = this->get_n_rows()+matrix->get_n_rows();
  int cols = matrix->get_n_cols();

  //allocate memory
  std::vector<double> values(nnz);
  std::vector<int> rowIndices(nnz);
  std::vector<int> colIndices(nnz);

  getVertCatMatrixData(matrix, rowIndices.data(), colIndices.data(), values.data());

  CsTripletMatrix::Ptr resMatrix(new CsTripletMatrix(rows,
                                                     cols,
                                                     nnz,
                                                     rowIndices.data(),
                                                     colIndices.data(),
                                                     values.data()));

  return resMatrix;
}


void CsTripletMatrix::getMatrixSlicesData(int n_rows, const int *row_idx,
                                          int n_cols, const int *col_idx,
                                          cs **sliced_triplet) const
{
  std::vector<int> r(row_idx, row_idx+n_rows), c(col_idx, col_idx+n_cols);

  int row_count = 0, col_count=0;
  std::vector<int> rbool(get_n_rows(), -1), cbool(get_n_cols(), -1);
  for (int k=0; k<n_rows; ++k)
    rbool[r[k]] = row_count++;
  for (int k=0; k<n_cols; ++k)
    cbool[c[k]] = col_count++;

  int nnz = 0;
  std::vector<int> idx(m_mat->nz, 0);
  std::vector<int> sliced_row_idx(m_mat->nz, -1);
  std::vector<int> sliced_col_idx(m_mat->nz, -1);
  for (int k=0; k<m_mat->nz; ++k){
    if (rbool[m_mat->i[k]] >= 0 && cbool[m_mat->p[k]] >= 0) {
      idx[nnz] = k;
      sliced_row_idx[k] =
      nnz++;
    }
  }

  *sliced_triplet = cs_spalloc(n_rows, n_cols, nnz, 1, true);
  (*sliced_triplet)->nz = nnz;
  for (int k=0; k<nnz; ++k) {
    (*sliced_triplet)->i[k] = rbool[m_mat->i[idx[k]]];
    (*sliced_triplet)->p[k] = cbool[m_mat->p[idx[k]]];
    (*sliced_triplet)->x[k] = m_mat->x[idx[k]];
  }
}

CsTripletMatrix::Ptr CsTripletMatrix::get_matrix_slices(int n_rows, const int* row_idx, int n_cols, const int* col_idx) const
{
  cs* sliced_triplet;
  getMatrixSlicesData(n_rows, row_idx, n_cols, col_idx, &sliced_triplet);
  CsTripletMatrix::Ptr retval(new CsTripletMatrix(sliced_triplet));
  return retval;
}

void CsTripletMatrix::getMatrixSlicesKeepSizeData(int numRows, const int* rowIndex,
                                                  int numCols, const int* colIndex,
                                                  int &numNonZeroes,
                                                  std::vector<int> &reducedRow,
                                                  std::vector<int> &reducedCol,
                                                  std::vector<double> &reducedVal)const
{
  numNonZeroes = 0;
  assert(numRows>0 && numRows <= m_mat->m);
  assert(numCols>0 && numCols <= m_mat->n);
  assert(rowIndex != NULL);
  assert(colIndex != NULL);
  // collect all triplet entries that have the row index in rowIndex and the 
  // columnd index in colIndex
  // copy needed for std::find, since constant input is not allowed
  // if this is a performance problem, implement own find function
  std::vector<int> rowIndexVector, colIndexVector;
  rowIndexVector.assign(rowIndex, rowIndex + numRows);
  colIndexVector.assign(colIndex, colIndex + numCols);
  for(int i=0; i<m_mat->nz; i++){
    std::vector<int>::iterator foundRow, foundCol;
    foundRow = std::find(rowIndexVector.begin(), rowIndexVector.end(), m_mat->i[i]);
    foundCol = std::find(colIndexVector.begin(), colIndexVector.end(), m_mat->p[i]);
    if(foundRow != rowIndexVector.end() && foundCol != colIndexVector.end()){
      reducedRow.push_back(m_mat->i[i]);
      reducedCol.push_back(m_mat->p[i]);
      reducedVal.push_back(m_mat->x[i]);
      numNonZeroes++;
    }
  }
  
  //since we use &vec[0] to extract the pointer, the vectors must not be empty
  if(numNonZeroes == 0){
    reducedRow.push_back(0);
    reducedCol.push_back(0);
    reducedVal.push_back(0.0);
  }
}

/**
* @brief cut out submatrix as in get_matrix_slices, but keep original matrix size
*
* The row and col indices of the submatrix refer to the same rows and colums as before
* @param numRows size of rowIndex
* @param rowIndex indices of the rows that are to be kept(entries of all other rows are set to 0)
* @param numCols size of colIndex
* @param colIndex indices of the columns that are to be kept (entries of all
*                 other columns areset to 0)
* @return CsTripletMatrix containing only the nonzeroes of the given rows and columns
*/
CsTripletMatrix::Ptr CsTripletMatrix::get_matrix_slices_keep_size(int numRows,
                                                                  const int *rowIndex,
                                                                  int numCols,
                                                                  const int *colIndex) const
{
  std::vector<int> rowVec, colVec;
  std::vector<double> valVec;
  int numNonZeroes = 0;
  getMatrixSlicesKeepSizeData(numRows, rowIndex, numCols, colIndex,
                              numNonZeroes, rowVec, colVec, valVec);
  CsTripletMatrix::Ptr retval(new CsTripletMatrix(m_mat->m,
                                                  m_mat->n,
                                                  numNonZeroes,
                                                  &rowVec[0],
                                                  &colVec[0],
                                                  &valVec[0]));
  return retval;
}

void CsTripletMatrix::transpose_in_place()
{
  int m_orig = m_mat->m;
  int n_orig = m_mat->n;
  m_mat->m = n_orig;
  m_mat->n = m_orig;

  int *p_orig = m_mat->p;
  m_mat->p = m_mat->i;
  m_mat->i = p_orig;
}

void CsTripletMatrix::setValue(const int rowIndex, const int colIndex, const double value)
{
  for(int i=0; i<m_mat->nz; i++){
    if(rowIndex == m_mat->i[i] && colIndex == m_mat->p[i]){
      m_mat->x[i] = value;
      return;
    }
  }
  //element not found
  throw EntryNotInSparseStructureException(rowIndex, colIndex);
}

double CsTripletMatrix::getValue(const int rowIndex, const int colIndex){
  for(int i=0; i<m_mat->nz; i++){
    if(rowIndex == m_mat->i[i] && colIndex == m_mat->p[i]){
      return m_mat->x[i];
    }
  }
  return 0.0;
}

/**
* @brief sets a cs matrix into another
*
* @param[in] subMatrix matrix in COO format to be inserted - submatrix may have smaller
*                      dimensions than this matrix, but dimension + upper left col/row
*                      must fit in dimensions of this matrix
* @param[in] upperLeftRowIndex row index at which the matrix is to be inserted
* @param[in] upperLeftColIndex col index at which the matrix is to be inserted
* @note submatrix must fit on sparse pattern of this matrix
*/
void CsTripletMatrix::setSubMatrix(CsTripletMatrix::Ptr subMatrix,
                  unsigned upperLeftRowIndex,
                  unsigned upperLeftColIndex)
{
  assert(upperLeftRowIndex + subMatrix->get_n_rows() <= unsigned(this->get_n_rows()));
  assert(upperLeftColIndex + subMatrix->get_n_cols() <= unsigned(this->get_n_cols()));

  for(int i=0; i<subMatrix->get_nnz(); i++){
    const int rowIndex = subMatrix->get_matrix_ptr()->i[i];
    const int colIndex = subMatrix->get_matrix_ptr()->p[i];
    const double value = subMatrix->get_matrix_ptr()->x[i];

    this->setValue(rowIndex + upperLeftRowIndex,
                   colIndex + upperLeftColIndex,
                   value);
  }
}


SortedCsTripletMatrix::SortedCsTripletMatrix(int n_rows, int n_cols, int nnz,
                                             const int* row_idx,
                                             const int* col_idx,
                                             const double* values)
    :CsTripletMatrix(n_rows, n_cols, nnz, row_idx, col_idx, values)
{
  sort();
}

SortedCsTripletMatrix::SortedCsTripletMatrix(cs* mat, bool owner)
    :CsTripletMatrix(mat, owner)
{
  sort();
}

SortedCsTripletMatrix::~SortedCsTripletMatrix()
{}

/**
* @brief implement insertion sort (for insertion sort is fastest on presorted fields)
*/
void SortedCsTripletMatrix::sort()
{
  for (int i=1; i < m_mat->nz; i++){
    int temp = m_mat->i[i];
    int temp2 = m_mat->p[i];
    double temp3 = 0.0;
    if(m_mat->x){
      temp3 = m_mat->x[i];
    }
    int j = i - 1;

    //sort first rows then cols
    while (j >= 0 && (m_mat->i[j] > temp
                        || (m_mat->i[j] == temp && m_mat->p[j] > temp2))){
      m_mat->i[j+1] = m_mat->i[j];
      m_mat->p[j+1] = m_mat->p[j];
      if(m_mat->x){
        m_mat->x[j+1] = m_mat->x[j];
      }
      j--;
    }
    
    m_mat->i[j+1] = temp;
    m_mat->p[j+1] = temp2;
    if(m_mat->x){
      m_mat->x[j+1] = temp3;
    }
  }
}

void SortedCsTripletMatrix::setValue(const int rowIndex,
                                     const int colIndex,
                                     const double value)
{

  //use binary search for the element
  int nz = m_mat->nz;
  int middle;
  int left = 0; // smallest index
  int right = nz - 1; // highest index

  assert (   (rowIndex > m_mat->i[0] && rowIndex < m_mat->i[nz - 1])
          || (rowIndex == m_mat->i[0] && colIndex >= m_mat->p[0])
          || (rowIndex == m_mat->i[nz-1] && colIndex <= m_mat->p[nz-1]));


  // endless loop is ended by return or assert
  while(true) 
  {
    middle = left + ((right - left) / 2); //bisect search area

    if (right < left) //did not find entry
    {
      throw EntryNotInSparseStructureException(rowIndex, colIndex);
    }

    if (m_mat->i[middle] == rowIndex && m_mat->p[middle] == colIndex) // Element found?
    {
      m_mat->x[middle] = value;
      return;
    }

    if(   m_mat->i[middle] > rowIndex
       || (m_mat->i[middle] == rowIndex && m_mat->p[middle] > colIndex) ){
      right = middle - 1; //search left
    }
    else{
      left = middle + 1; // search right
    }
  }
}

double SortedCsTripletMatrix::getValue(const int rowIndex,
                                       const int colIndex)
{

  //use binary search for the element
  int nz = m_mat->nz;
  int middle;
  int left = 0; // smallest index
  int right = nz - 1; // highest index

  assert (   (rowIndex > m_mat->i[0] && rowIndex < m_mat->i[nz - 1])
          || (rowIndex == m_mat->i[0] && colIndex >= m_mat->p[0])
          || (rowIndex == m_mat->i[nz-1] && colIndex <= m_mat->p[nz-1]));


  // endless loop is ended by return or assert
  while(true) 
  {
    middle = left + ((right - left) / 2); //bisect search area

    if (right < left) //did not find entry
    {
      return 0.0;
    }

    if (m_mat->i[middle] == rowIndex && m_mat->p[middle] == colIndex) // Element found?
    {
      return m_mat->x[middle];
    }

    if(   m_mat->i[middle] > rowIndex
       || (m_mat->i[middle] == rowIndex && m_mat->p[middle] > colIndex) ){
      right = middle - 1; //search left
    }
    else{
      left = middle + 1; // search right
    }
  }
}

void SortedCsTripletMatrix::setSubMatrix(CsTripletMatrix::Ptr subMatrix,
                                                 unsigned upperLeftRowIndex,
                                                 unsigned upperLeftColIndex)
{
  CsTripletMatrix::setSubMatrix(subMatrix, upperLeftRowIndex, upperLeftColIndex);
}

CsTripletMatrix::Ptr SortedCsTripletMatrix::vertcat(CsTripletMatrix::Ptr matrix)
{

  assert(this->get_matrix_ptr() != NULL);
  assert(matrix->get_matrix_ptr() != NULL);

  const int nnz = this->get_nnz() + matrix->get_nnz();
  int rows = this->get_n_rows()+matrix->get_n_rows();
  int cols = matrix->get_n_cols();

  //allocate memory
  std::vector<double> values(nnz);
  std::vector<int> rowIndices(nnz);
  std::vector<int> colIndices(nnz);

  getVertCatMatrixData(matrix, rowIndices.data(), colIndices.data(), values.data());
  
  CsTripletMatrix::Ptr resMatrix(new SortedCsTripletMatrix(rows,
                                                           cols,
                                                           nnz,
                                                           rowIndices.data(),
                                                           colIndices.data(),
                                                           values.data()));

  return resMatrix;
}

CsTripletMatrix::Ptr SortedCsTripletMatrix::get_matrix_slices(int n_rows,
                                                              const int* row_idx,
                                                              int n_cols,
                                                              const int* col_idx) const
{
  cs* sliced_triplet;
  getMatrixSlicesData(n_rows, row_idx, n_cols, col_idx, &sliced_triplet);
  CsTripletMatrix::Ptr retval(new SortedCsTripletMatrix(sliced_triplet));
  return retval;
}

CsTripletMatrix::Ptr SortedCsTripletMatrix::get_matrix_slices_keep_size
                                                        (int numRows, const int* rowIndex,
                                                         int numCols, const int* colIndex) const
{
  std::vector<int> rowVec, colVec;
  std::vector<double> valVec;
  int numNonZeroes = 0;
  getMatrixSlicesKeepSizeData(numRows, rowIndex, numCols, colIndex,
                              numNonZeroes, rowVec, colVec, valVec);
  CsTripletMatrix::Ptr retval(new SortedCsTripletMatrix(m_mat->m,
                                                        m_mat->n,
                                                        numNonZeroes,
                                                        &rowVec[0],
                                                        &colVec[0],
                                                        &valVec[0]));
  return retval;
}