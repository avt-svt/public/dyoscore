/**
* @file OptimizerFactory.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer                                                     \n
* =====================================================================\n
* This file contains the definition of the factory for                 \n
* GenericOptimizer                                                     \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.02.2012
*/

#include "OptimizerFactory.hpp"
#include "IntegratorMetaData.hpp"
#include "IpoptWrapper.hpp"
#include "GenericIntegrator.hpp"
#include "FirstOrderOptimizationProblem.hpp"
#include "SecondOrderOptimizationProblem.hpp"
#include "SNOPTWrapper.hpp"
#include "NPSOLWrapper.hpp"
#include "FilterSQPWrapper.hpp"
#include "FinalIntegration.hpp"

#include <cassert>

using namespace FactoryInput;
/**
* @brief create a GenericOptimizer object using standard factories
*        for creating GenericIntegrator and OptimizerMetaData objects
*
* @param input OptimizerInput struct containing all information needed
*              to construct a GenericOptimizer object
* @return pointer to the created GenericOptimizer object
*/
GenericOptimizer *OptimizerFactory::createGenericOptimizer(const OptimizerInput &input)
{
  IntegratorMetaDataFactory imdFactory;
  imdFactory.setLogger(m_logger);
  IntegratorFactory integratorFactory;
  integratorFactory.setLogger(m_logger);
  OptimizerMetaDataFactory omdFactory;
  omdFactory.setLogger(m_logger);
  return createGenericOptimizer(input, imdFactory, integratorFactory, omdFactory);
}

/**
* @brief create a GenericOptimizer object using given factories
*        for creating GenericIntegrator and OptimizerMetaData objects
*
* @param input OptimizerInput struct containing all information needed
*              to construct a GenericOptimizer object
* @param imdFactory factory used to create the IntegratorMetaData objects
* @param integratorFactory factory used to create the GenericIntegrator object
* @param metaDataFactory factory used to create the OptimizerMetaData objects
* @return pointer to the created GenericOptimizer object
*/
GenericOptimizer *OptimizerFactory::createGenericOptimizer(const OptimizerInput &input,
                                                           IntegratorMetaDataFactory &imdFactory,
                                                           IntegratorFactory &integratorFactory,
                                                           OptimizerMetaDataFactory &omdFactory)
{
  GenericOptimizer *optimizer;
  OptimizationProblem::Ptr optProb;
  if(input.integratorInput.order == FactoryInput::IntegratorInput::SECOND_REVERSE){
    SecondOrderOptimizationProblem::Ptr secOrdOptProb(createSecondOrderOptimizationProblem
                                                                      (input,
                                                                       imdFactory,
                                                                       integratorFactory,
                                                                       omdFactory));
    optimizer = createSecondOrderOptimizer(input, secOrdOptProb);
  }
  else{
    assert(input.integratorInput.order != FactoryInput::IntegratorInput::ZEROTH);
    OptimizationProblem::Ptr optProb = OptimizationProblem::Ptr(createFirstOrderOptimizationProblem
                                                                      (input,
                                                                       imdFactory,
                                                                       integratorFactory,
                                                                       omdFactory));
    optimizer = createFirstOrderOptimizer(input, optProb);
  }
  const unsigned numStages = input.integratorInput.metaDataInput.stages.size();
  assert(numStages == input.metaDataInput.stages.size());
  
  return optimizer;
}

GenericOptimizer* OptimizerFactory::createFirstOrderOptimizer(
                                              const FactoryInput::OptimizerInput &input,
                                              const OptimizationProblem::Ptr& optProb)
{
  GenericOptimizer *optimizer;
  switch(input.type){
  case OptimizerInput::SNOPT:
    optimizer = createSnopt(optProb, input.optimizerOptions);
    break;
  case OptimizerInput::IPOPT:
    optimizer = createIpopt(optProb, input.optimizerOptions);
    break;
  case OptimizerInput::NPSOL:
    optimizer = createNpsol(optProb, input.optimizerOptions);
    break;
  case FactoryInput::OptimizerInput::FINAL_INTEGRATION:
    optimizer = createFinalIntegration(optProb);
    break;
  case FactoryInput::OptimizerInput::FILTER_SQP:
      throw IncompatibleIntegrationOrderException("filterSQP not usable with 1st order integrator");
  default:
    assert(false);
  }
  
  return optimizer;
}

GenericOptimizer* OptimizerFactory::createSecondOrderOptimizer(
                                               const FactoryInput::OptimizerInput &input,
                                               const SecondOrderOptimizationProblem::Ptr& optProb)
{
  GenericOptimizer *optimizer;
  switch(input.type){
    case FactoryInput::OptimizerInput::FINAL_INTEGRATION:
      optimizer = createFinalIntegration(optProb);
      break;
    case FactoryInput::OptimizerInput::FILTER_SQP:
      optimizer = createFilterSQP(optProb);
      break;
    case OptimizerInput::SNOPT:
      throw IncompatibleIntegrationOrderException("SNOPT not usable with 2nd order integrator");
      optimizer = createSnopt(optProb, input.optimizerOptions);
      break;
    case OptimizerInput::IPOPT:
      optimizer = createIpopt(optProb, input.optimizerOptions);
      break;
    case OptimizerInput::NPSOL:
      throw IncompatibleIntegrationOrderException("NPSOL not usable with 2nd order integrator");
    default:
      assert(false);
  }
  
  return optimizer;
}


OptimizationProblem *OptimizerFactory::createFirstOrderOptimizationProblem(
                                          const FactoryInput::OptimizerInput &input,
                                                IntegratorMetaDataFactory &imdFactory,
                                                IntegratorFactory &integratorFactory,
                                                OptimizerMetaDataFactory &omdFactory)
{
  GenericIntegrator::Ptr integrator;
  OptimizerMetaData::Ptr optimizerMetaData;

  if(input.metaDataInput.stages.empty()){
    //create integrator and OptimizerMetaData in single stage mode
    IntegratorSingleStageMetaData::Ptr integratorSSMetaData(
                                       imdFactory.createIntegratorSingleStageMetaData(
                                                  (input.integratorInput.metaDataInput)));

    integrator = GenericIntegrator::Ptr(integratorFactory.createGenericIntegrator
                                       (input.integratorInput, integratorSSMetaData));
    if(input.type == FactoryInput::OptimizerInput::FINAL_INTEGRATION){
      integrator->activatePlotGrid();
    }
    optimizerMetaData = OptimizerMetaData::Ptr(omdFactory.createOptimizerSingleStageMetaData
                                                          (input.metaDataInput,
                                                           integratorSSMetaData,
                                                           imdFactory.getProblemGrid().front()));
  }//if
  else{
    //create integrator and OptimizerMetaData in multi stage mode

    //create the stages for IntegratorMultiStageMetaData
    std::vector<IntegratorSingleStageMetaData::Ptr> integratorMDStages;
    integratorMDStages = imdFactory.createIntegratorSingleStageMetaDataVector
                                  (input.integratorInput.metaDataInput.stages);
    IntegratorMultiStageMetaData::Ptr integratorMSMetaData(
                                                imdFactory.createIntegratorMultiStageMetaData
                                                          (input.integratorInput.metaDataInput,
                                                           integratorMDStages));

     
    integrator = GenericIntegrator::Ptr(integratorFactory.createGenericIntegrator
                                                          (input.integratorInput,
                                                           integratorMSMetaData));
    if(input.type == FactoryInput::OptimizerInput::FINAL_INTEGRATION){
      integrator->activatePlotGrid();
    }
    optimizerMetaData = OptimizerMetaData::Ptr(omdFactory.createOptimizerMultiStageMetaData
                                                            (input.metaDataInput,
                                                             integratorMDStages,
                                                             integratorMSMetaData,
                                                             imdFactory.getProblemGrid()));
  }//else

  return createFirstOrderOptimizationProblem(integrator, optimizerMetaData, input.minimize);
}

SecondOrderOptimizationProblem *OptimizerFactory::createSecondOrderOptimizationProblem(
                                          const FactoryInput::OptimizerInput &input,
                                                IntegratorMetaDataFactory &imdFactory,
                                                IntegratorFactory &integratorFactory,
                                                OptimizerMetaDataFactory &omdFactory)
{
  GenericIntegratorReverse::Ptr integrator;
  OptimizerMetaData2ndOrder::Ptr optimizerMetaData;

  if(input.metaDataInput.stages.empty()){
    //create integrator and OptimizerMetaData in single stage mode
    IntegratorSSMetaData2ndOrder::Ptr integratorSSMetaData(
                                       imdFactory.createIntegratorSSMetaData2ndOrder(
                                                  (input.integratorInput.metaDataInput)));

    integrator = GenericIntegratorReverse::Ptr(integratorFactory.createGeneric2ndOrderIntegrator
                                              (input.integratorInput, integratorSSMetaData));

    if(input.type == FactoryInput::OptimizerInput::FINAL_INTEGRATION){
      integrator->activatePlotGrid();
    }
    optimizerMetaData = OptimizerMetaData2ndOrder::Ptr(omdFactory.createOptimizerSSMetaData2ndOrder
                                                          (input.metaDataInput,
                                                           integratorSSMetaData,
                                                           imdFactory.getProblemGrid().front()));
  }//if
  else{
    //create integrator and OptimizerMetaData in multi stage mode

    //create the stages for IntegratorMultiStageMetaData
    std::vector<IntegratorSSMetaData2ndOrder::Ptr> integratorMDStages;
    integratorMDStages = imdFactory.createIntegratorSSMetaData2ndOrderVector
                                  (input.integratorInput.metaDataInput.stages);
    IntegratorMSMetaData2ndOrder::Ptr integratorMSMetaData(
                                      imdFactory.createIntegratorMSMetaData2ndOrder
                                                  (input.integratorInput.metaDataInput,
                                                   integratorMDStages));

     
    integrator = GenericIntegratorReverse::Ptr(integratorFactory.createGeneric2ndOrderIntegrator
                                                          (input.integratorInput,
                                                           integratorMSMetaData));
    if(input.type == FactoryInput::OptimizerInput::FINAL_INTEGRATION){
      integrator->activatePlotGrid();
    }
    optimizerMetaData = OptimizerMetaData2ndOrder::Ptr(omdFactory.createOptimizerMSMetaData2ndOrder
                                                            (input.metaDataInput,
                                                             integratorMDStages,
                                                             integratorMSMetaData,
                                                             imdFactory.getProblemGrid()));
  }//else

  return createSecondOrderOptimizationProblem(integrator, optimizerMetaData, input.minimize);
}

/**
* @brief create a FirstOrderOptimizationProblem objact out of an integrator 
*        and an optimizerMetaData object
*
* @param integrator pointer to the integrator used to solve the problem
* @param optimizerMetaData pointer to the object holding all data of the optimization problem
* @return pointer to the created OptimizationPorblem object
*/
OptimizationProblem *OptimizerFactory::createFirstOrderOptimizationProblem(
                                             const GenericIntegrator::Ptr &integrator,
                                             const OptimizerMetaData::Ptr &optimizerMetaData,
                                             const bool optimizationMode)
{
  OptimizationProblem *firstOrderProblem = new FirstOrderOptimizationProblem(integrator,
                                                                             optimizerMetaData,
                                                                             optimizationMode);
  firstOrderProblem->setLogger(m_logger);
  return firstOrderProblem;
}

/**
* @brief create a SecondOrderOptimizationProblem objact out of an integrator
*        and an optimizerMetaData object
*
* @param integrator pointer to the integrator used to solve the problem
* @param optimizerMetaData pointer to the object holding all data of the optimization problem
* @return pointer to the created OptimizationPorblem object
*/
SecondOrderOptimizationProblem *OptimizerFactory::createSecondOrderOptimizationProblem(
                                          const GenericIntegratorReverse::Ptr &integrator,
                                          const OptimizerMetaData2ndOrder::Ptr &optimizerMetaData,
                                          const bool optimizationMode)
{
  SecondOrderOptimizationProblem *secondOrderProblem = new SecondOrderOptimizationProblem(integrator,
                                                                               optimizerMetaData,
                                                                               optimizationMode);
  secondOrderProblem->setLogger(m_logger);
  return secondOrderProblem;
}


GenericOptimizer *OptimizerFactory::createFilterSQP
                          (const SecondOrderOptimizationProblem::Ptr& optProb)
{
  GenericOptimizer *filterSQP = new FilterSQPWrapper(optProb);
  return filterSQP;
}

/**
* @brief create a SNOPTWrapper object
*
* @param optProb OptimizationProblem to be solved by SNOPT
* @return pointer to the created SNOPTWrapper object
*/
GenericOptimizer *OptimizerFactory::createSnopt(
                                      const OptimizationProblem::Ptr& optProb,
                                      const std::map<std::string, std::string> &optimizerOptions)
{
  GenericOptimizer *snopt = new SNOPTWrapper(optProb, optimizerOptions);
  snopt->setLogger(m_logger);
  return snopt;
}

/**
* @brief create a IpoptWrapper object
*
* @param optProb OptimizationProblem to be optimized by IPOPT
* @return pointer to the created IpoptWrapper object
*/
GenericOptimizer *OptimizerFactory::createIpopt(const OptimizationProblem::Ptr& optProb,
                                                const std::map<std::string, std::string> &optimizerOptions)
{
  GenericOptimizer *ipopt = new IpoptWrapper(optProb, optimizerOptions);
  ipopt->setLogger(m_logger);
  return ipopt;
}

/**
* @brief create a NSPOLWrapper object
*
* @param optProb OptimizationProblem to be solved by NPSOL
* @return pointer to the created NSPOLWrapper object
*/
GenericOptimizer *OptimizerFactory::createNpsol(const OptimizationProblem::Ptr& optProb,
                                                const std::map<std::string, std::string> &optimizerOptions)
{
  GenericOptimizer *npsol = new NPSOLWrapper(optProb, optimizerOptions);
  npsol->setLogger(m_logger);
  return npsol;
}

/**
* @brief create a FinalIntegration object
*
* @param optProb OptimizationProblem to be integrated by FinalIntegration
* @return pointer to the created FinalIntegration object
*/
GenericOptimizer * OptimizerFactory::createFinalIntegration
                                        (const OptimizationProblem::Ptr &optProb)
{
  GenericOptimizer *secOrdInt = new FinalIntegration(optProb);
  secOrdInt->setLogger(m_logger);
  return secOrdInt;
}