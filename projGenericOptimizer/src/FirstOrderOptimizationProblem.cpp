#include "FirstOrderOptimizationProblem.hpp"

FirstOrderOptimizationProblem::FirstOrderOptimizationProblem(const GenericIntegrator::Ptr& integrator,
                                                             const OptimizerMetaData::Ptr& opt,
                                                             const bool minimize)
  : m_new_dec_vars(true), m_new_multipliers(true), m_minimize(minimize), m_int(integrator), m_opt(opt)
{
  m_integratorResult = FAILED;
}


FirstOrderOptimizationProblem::FirstOrderOptimizationProblem(const bool minimize)
  : m_new_dec_vars(true), m_new_multipliers(true), m_minimize(minimize)
{
  m_integratorResult = FAILED;
}

const OptimizerProblemDimensions &FirstOrderOptimizationProblem::getOptProbDim() const
{
  return m_opt->getOptProbDim();
}

void FirstOrderOptimizationProblem::getDecVarBounds(utils::Array<double> &lowerBounds,
                                                    utils::Array<double> &upperBounds) const
{
  m_opt->getDecVarBounds(lowerBounds, upperBounds);
}

void FirstOrderOptimizationProblem::getDecVarValues(utils::Array<double> &values) const
{
  m_opt->getDecVarValues(values);
}

void FirstOrderOptimizationProblem::setDecVarValues(const utils::Array<const double> &decVars)
{
  m_new_dec_vars = true;
  m_opt->setDecVarValues(decVars);
}

void FirstOrderOptimizationProblem::getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                                              utils::Array<double> &upperBounds) const
{
  m_opt->getNonLinConstraintBounds(lowerBounds, upperBounds);
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::
                                getNonLinConstraintValues(utils::Array<double> &values)
{
  apply_new_x();
  if(m_integratorResult == OptimizationProblem::SUCCESS)
    m_opt->getNonLinConstraintValues(values);
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::
              getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr derivativeMatrix)
{
  apply_new_x();
  if(m_integratorResult == OptimizationProblem::SUCCESS)
    m_opt->getNonLinConstraintDerivativesDecVar(derivativeMatrix);
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::
                        getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr derivativeMatrix)
{
  apply_new_x();
  if(m_integratorResult == OptimizationProblem::SUCCESS)
    m_opt->getNonLinConstraintDerivativesConstParam(derivativeMatrix);
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::getNonLinObjValue(double &objValue)
{
  apply_new_x();
  if(m_integratorResult == OptimizationProblem::SUCCESS){
    objValue = m_opt->getNonLinObjValue();
    if(!m_minimize){
      objValue *= -1.0;
    }
  }
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::getNonLinObjDerivative
                                                              (utils::Array<double> &values)
{
  apply_new_x();
  if(m_integratorResult == OptimizationProblem::SUCCESS){
    m_opt->getNonLinObjDerivative(values);
    if(!m_minimize){
      for(unsigned i=0; i<values.getSize(); i++){
        values[i] *= -1.0;
      }
    }
  }
  return m_integratorResult;
}

void FirstOrderOptimizationProblem::setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                                           const utils::Array<double> &linMultipliers)
{
  m_new_multipliers = true;
  m_opt->setLagrangeMultipliers(nonLinMultipliers, linMultipliers);
}

void FirstOrderOptimizationProblem::getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                                           utils::Array<double> &linMultipliers) const
{
  m_opt->getLagrangeMultipliers(nonLinMultipliers, linMultipliers);
}

void FirstOrderOptimizationProblem::setDecVarLagrangeMultipliers
                                       (const utils::Array<double> &lagrangeMultiplier)
{
  m_opt->setDecVarLagrangeMultipliers(lagrangeMultiplier);
}

void FirstOrderOptimizationProblem::setObjectiveLagrangeMultiplier
                                       (const double lagrangeMultiplier)
{
  m_new_multipliers = true;
  if(m_minimize){
    m_opt->setObjectiveLagrangeMultiplier(lagrangeMultiplier);
  }
  else{
    m_opt->setObjectiveLagrangeMultiplier(-lagrangeMultiplier);
  }
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::getLinConstraintValues
                                                              (utils::Array<double> &values)
{
  apply_new_x();
  if(m_integratorResult == OptimizationProblem::SUCCESS)
    m_opt->getLinConstraintValues(values);
  return m_integratorResult;
}

void FirstOrderOptimizationProblem::getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                                           utils::Array<double> &upperBounds) const
{
  m_opt->getLinConstraintBounds(lowerBounds, upperBounds);
}

void FirstOrderOptimizationProblem::apply_new_x()
{
  //if (m_new_dec_vars) {
  if (m_new_dec_vars) {
    GenericIntegrator::ResultFlag result = m_int->solve();
    switch(result){
      case GenericIntegrator::FAILED:
        m_integratorResult = OptimizationProblem::FAILED;
        break;
      case GenericIntegrator::SUCCESS:
        m_integratorResult = OptimizationProblem::SUCCESS;
        m_new_dec_vars = false;
        break;
      default:
        assert(false);
    }
  }
}

std::vector<DyosOutput::StageOutput> FirstOrderOptimizationProblem::getOutput()
{
  return m_opt->getOutput();
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::getNonLinConstraintHessianDecVar
                                        (std::vector<std::vector<double> > &hessianMatrix)
{
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::getNonLinConstraintHessianConstParam
                                        (std::vector<std::vector<double> > &hessianMatrix)
{
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::getAdjointsDecVar(
                               std::vector<double> &adjoints1stOrder,
                               std::vector<std::vector<double> > &adjoints2ndOrder)
{
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::getAdjointsConstParam(
                                   std::vector<double> &adjoints1stOrder,
                                   std::vector<std::vector<double> > &adjoints2ndOrder)
{
  return m_integratorResult;
}

OptimizationProblem::ResultFlag FirstOrderOptimizationProblem::doFinalIntegration()
{
  apply_new_x();
  return m_integratorResult;
}
