/** @file SNOPTLoader.cpp
*    @brief definitions of member functions of SNOPTLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails SNOPTLoader
*    =====================================================================\n
*   @author Tjalf Hoffmann, Klaus Stockmann
*   @date 21.6.2010
*/


#include "SNOPTLoader.hpp"
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#ifdef WIN32
#define FORTRAN_UNDERSCORE ""
#else
#define FORTRAN_UNDERSCORE "_"
#endif

/**
* @brief standard constructor of SNOPTLoader
*
* loads the dll snopt724.dll
*/
SNOPTLoader::SNOPTLoader():Loader("snopt724")
{
  loadFunctions();
  m_snoptStringLength = 8;
}

/**
* @brief copy constructor of SNOPTLoader
*
* @param snoptLoader SNOPTLoader to be copied
*/
SNOPTLoader::SNOPTLoader(const SNOPTLoader &snoptLoader):Loader(snoptLoader)
{
  loadFunctions();
}

/**
* @brief loads all functions from SNOPT DLL which are required by DyOS
*/
void SNOPTLoader::loadFunctions()
{
  // function handles have to be initialized to NULL, otherwise in Loader.hpp
  // it is assumed that they have already been loaded
  snopt_func_handle = NULL;

  sninit_func_handle = NULL;
  snspec_func_handle = NULL;

  sngetr_func_handle = NULL;
  snsetr_func_handle = NULL;
  snseti_func_handle = NULL;
  snset_func_handle = NULL;

  sn_openfile_func_handle = NULL;
  sn_closefile_func_handle = NULL;

  // load functions from SNOPT DLL using Windows system function GetProcAddress()
  loadFcn(snopt_func_handle, "snopt" FORTRAN_UNDERSCORE);

  loadFcn(sninit_func_handle, "sninit" FORTRAN_UNDERSCORE);
  loadFcn(snspec_func_handle, "snspec" FORTRAN_UNDERSCORE);

  loadFcn(sngetr_func_handle, "sngetr" FORTRAN_UNDERSCORE);
  loadFcn(snsetr_func_handle, "snsetr" FORTRAN_UNDERSCORE);
  loadFcn(snseti_func_handle, "snseti" FORTRAN_UNDERSCORE);
  loadFcn(snset_func_handle, "snset" FORTRAN_UNDERSCORE);

  loadFcn(sn_openfile_func_handle, "sn_openfile" FORTRAN_UNDERSCORE);
  loadFcn(sn_closefile_func_handle, "sn_closefile" FORTRAN_UNDERSCORE);
}

/**
* @brief calls the FORTRAN function SNOPT from snopt dll
*
* @param m number of general constraints (i.e. linear and nonlinear constraints) - must be positive
* @param n number of optimization variables excluding slacks - must be positive
* @param ne number of nonzero entries in the constraint matrix - must be positive
* @param nnCon number of nonlinear constraints - must be >=0
* @param prob problem name - must be an eight character string
* @param confun pointer to the callback function evaluating constraint data
* @param objfun pointer to the callback function evaluating objective data
* @param a value vector of the constraint matrix (CSC sparse format)- must be of size ne
* @param ha row indices of the constraint matrix (CSC sparse format)- must be of size ne, entries must be in range [1..m]
* @param ka column indices of the constraint matrix (CSC sparse format)- must be of size n+1
*           first entry must be 1 and last entry must be ne+1
* @param bl lower bounds on variables and slacks - must be of size n+m
* @param bu upper bounds on variables and slacks - must be of size n+m
* @param hs set of initial states of variables and slacks - must be of size n+m\n
*           on exit: vector whose entries indicate the final state of each optimization variable; state of a variable may be:\n
            0 = nonbasic -> value at lower bound\n
            1 = nonbasic -> value at upper bound\n
            2 = superbasic\n
            3 = basic\n
* @param xs initial values of optimization variables and slacks - must be of size m+n\n
*           on exit final values of optimization variables and slacks
* @param[in, out] pi lagrange multiplier - must be of size m
* @param[out] rc reduced costs - must be of size m+n
* @param[out] inform result code of snopt - meaning can be extracted by function getSNOPTMsg
* @param[out] nInf number of infeasibilities
* @param[out] sInf sum of infeasibilities
* @param[out] obj final value of the objective function
* @param cw character workspace for SNOPT - will be used as user workspace as well
* @param iw integer workspace for SNOPT - will be used as user workspace as well
* @param rw real workspace for SNOPT - will be used as user workspace as well
* @note all parameters are described in the User's Guide for SNOPT
*/
void SNOPTLoader::snopt(int m, int n, int ne, int nnCon,
                        char prob[], confun_ptr confun, objfun_ptr objfun,
                        double *a, int *ha, int *ka,
                        double *bl, double *bu,
                        int *hs, double *xs, double *pi, double *rc, int &inform,
                        int &nInf, double &sInf, double &obj,
                        std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw)
{
  assert(m>0);
  assert(n>0);
  assert(ne>0);
  assert(nnCon>=0);

  char start[] =  "Cold"; // startingBasis = {"Cold", "Basis file", "Warm"}
  size_t startLength = 4; // if the start name is changed, the startLength should be adjusted.

  int nS = 0; /*! =^ final number of super basics; output variable, if WarmStart,
                  then value has to be retained between calls to SNOPT */

  int nnObj;  //! number of nonlinear objective function variables
  int nnJac;  //! number of nonlinear Jacobian variables
  //! @remarks it is assumed that the objective function as well as the constraints
  //           are nonlinear in ALL optimization variables
  nnObj = n;
  nnJac = n;

  int iObj = 0; //! under the assumption, that Jacobian is fully nonlinear, iObj must be = 0 (see SNOPT manual)
  double objAdd = 0.0; //! arbitrary value added to objective function value; not used in DyOS
  int nName = 1; /*! in SNOPT rows and columns of Jacobian may be named; a value of nName = 1 indicates,
                     that this feature is not used by DyOS */
  char *names = NULL; //! naming feature not used by DyOS
  
  int mincw, miniw, minrw; /*! on exit: indicate how many character, integer and real workspace is needed;
                                        may be used if SNOPT terminates because of insufficient storage */

  int lencw = cw.size()/m_snoptStringLength; //! length of character workspace
  int leniw = iw.size(); //! length of integer workspace
  int lenrw = rw.size(); //! length of real workspace

  snopt_func_handle(start, &m, &n, &ne, &nName, &nnCon, &nnObj, &nnJac,
                    &iObj, &objAdd, prob, confun, objfun,
                    a, ha, ka, bl, bu, names, hs, xs, pi, rc,
                    &inform, &mincw, &miniw, &minrw, &nS, &nInf, &sInf, &obj,
                    &cw[0], &lencw, &iw[0], &leniw, &rw[0], &lenrw,
                    &cw[0], &lencw, &iw[0], &leniw, &rw[0], &lenrw,
                    startLength, m_snoptStringLength, m_snoptStringLength, m_snoptStringLength, m_snoptStringLength);

  // the value inform = 0 indicates optimal solution, any other value indicates
  // some failure, in which case an exception should be raised
  if (inform != 0) {
    std::string msg = getSNOPTMsg(inform);
    /*! @todo throw an exception instead, in case of inform=82, 83 or 84 this should include
              an indication how much workspace memory is to be allocated (based on value of
              variables mincw, miniw and minrw respectively */
    m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, msg);
  }
}
/**
* @brief call FORTRAN function SNINIT from snopt dll
*
* @param iPrint file handle to the PRINT file (typically 9)
* @param iSumm file handle to the SUMMARY file (typically 6)
* @param cw character workspace for SNOPT
* @param iw integer workspace for SNOPT
* @param rw real workspace for SNOPT
*/
void SNOPTLoader::snInit(int iPrint, int iSumm, std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw)
{
  int lencw = cw.size()/m_snoptStringLength; //! length of character workspace
  int leniw = iw.size(); //! length of integer workspace
  int lenrw = rw.size(); //! length of real workspace
  //sninit_func_handle(&iPrint, &iSumm, &cw[0], lencw, &lencw, &iw[0], &leniw, &rw[0], &lenrw);
  sninit_func_handle(&iPrint, &iSumm, &cw[0], &lencw, &iw[0], &leniw, &rw[0], &lenrw, m_snoptStringLength);
}

/**
* @brief calls FORTRAN function SNSPEC from snopt dll
*
* @param optInform number of errors encountered reading the SPECS file (0 if no error occured)
* @param iSpecs file handle to the SPECS file to be read
* @param cw character workspace for SNOPT
* @param iw integer workspace for SNOPT
* @param rw real workspace for SNOPT
*/
void SNOPTLoader::snSpec(int &optInform, const int iSpecs,
                         std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw)
{
  int iSpecsToFortran = iSpecs; // copied, so we can pass it as constant to wrapper
  int lencw = cw.size()/m_snoptStringLength; //! length of character workspace
  int leniw = iw.size(); //! length of integer workspace
  int lenrw = rw.size(); //! length of real workspace
  snspec_func_handle(&iSpecsToFortran, &optInform,
                     &cw[0], &lencw, &iw[0], &leniw, &rw[0], &lenrw, m_snoptStringLength);
}

/**
* @brief calls the FORTRAN function SN_OPENFILE from snopt dll
*
* @param fileHandle handle to be used to open the file (will be used in fortran functions
*                   for read and write operations
* @param fileName name of the file to be opened
*/
void SNOPTLoader::snOpenFile(const int fileHandle, const std::string fileName)
{
  assert(fileHandle>0); // required by FORTRAN standard
  assert(fileHandle<100); // required by FORTRAN standard
  int fileHandleToFortran = fileHandle; // copied, so we can pass it as constant to wrapper
  int fileNameSize = static_cast<int>(fileName.size());
  const char* fileNameConst = fileName.c_str();

  sn_openfile_func_handle(&fileHandleToFortran, fileNameConst, fileNameSize);
  //! @todo what happens if the file could not be opened?
}

/**
*  @brief calls the FORTRAN function SN_CLOSEFILE from snopt dll
*
* @param fileHandle handle to the file (used in snOpenfile) to be closed
*/
void SNOPTLoader::snCloseFile(const int fileHandle)
{
  int fileHandleToFortran = fileHandle; // otherwise we can't declare fileHandle as const
  sn_closefile_func_handle(&fileHandleToFortran);
}

/**
* @brief translate a SNOPT info flag into an error message
*
* @param infoFlag inform value returned by FORTRAN function SNOPT
* @return message corresponding to input infoFlag
*/
std::string SNOPTLoader::getSNOPTMsg(const int infoFlag)
{
  assert(infoFlag >= 0);
  assert(infoFlag <= 142);

  std::string msg;

  switch (infoFlag) {
  case 0:    msg = "finished successfully"; break;
  case 1:    msg = "optimality conditions satisfied"; break;
  case 2:    msg = "feasible point found"; break;
  case 3:    msg = "requested accuracy could not be achieved"; break;

  case 10:    msg = "the problem appears to be infeasible"; break;
  case 11:    msg = "infeasible linear constraints"; break;
  case 12:    msg = "infeasible linear equalities"; break;
  case 13:    msg = "nonlinear infeasibilities minimized"; break;
  case 14:    msg = "infeasibilities minimized"; break;

  case 20:    msg = "the problem appears to be unbounded"; break;
  case 21:    msg = "unbounded objective"; break;
  case 22:    msg = "constraint violation limit reached"; break;

  case 30:    msg = "resource limit error"; break;
  case 31:    msg = "iteration limit reached"; break;
  case 32:    msg = "major iteration limit reached"; break;
  case 33:    msg = "the superbasics limit is too small"; break;

  case 40:    msg = "terminated after numerical difficulties"; break;
  case 41:    msg = "current point cannot be improved"; break;
  case 42:    msg = "singular basis"; break;
  case 43:    msg = "cannot satisfy the general constraints"; break;
  case 44:    msg = "ill-conditioned null-space basis"; break;

  case 50:    msg = "error in the user-supplied functions"; break;
  case 51:    msg = "incorrect objective  derivatives"; break;
  case 52:    msg = "incorrect constraint derivatives"; break;

  case 60:    msg = "undefined user-supplied functions"; break;
  case 61:    msg = "undefined function at the first feasible point"; break;
  case 62:    msg = "undefined function at the initial point"; break;
  case 63:    msg = "unable to proceed into undefined region"; break;

  case 70:    msg = "user requested termination"; break;
  case 71:    msg = "terminated during function evaluation"; break;
  case 72:    msg = "terminated during constraint evaluation"; break;
  case 73:    msg = "terminated during objective evaluation"; break;
  case 74:    msg = "terminated from monitor routine"; break;

  case 80:    msg = "insufficient storage allocated"; break;
  case 81:    msg = "work arrays must have at least 500 elements"; break;
  case 82:    msg = "not enough character storage"; break;
  case 83:    msg = "not enough integer storage"; break;
  case 84:    msg = "not enough real storage"; break;

  case 90:    msg = "input arguments out of range"; break;
  case 91:    msg = "invalid input argument"; break;
  case 92:    msg = "basis file dimensions do not match this problem"; break;

  case 140:   msg = "system error"; break;
  case 141:   msg = "wrong no of basic variables"; break;
  case 142:   msg = "error in basis package"; break;

  default:
    char buffer[250];
    sprintf(buffer, "INFORM = %d (for further information read the SNOPT Manual)", infoFlag);
    msg.assign(buffer);
  }

  msg = msg + "\n";
  return msg;
}
