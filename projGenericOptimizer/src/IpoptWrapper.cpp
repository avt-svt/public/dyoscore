#include "IpoptWrapper.hpp"
#include <exception>
#include <string>
#include <sstream>
#include <climits> //for INT_MAX and infinity
#include "SolverConfig.hpp"

#ifdef BUILD_IPOPT
#include "DyosIpoptNLP.hpp"
#include "IpIpoptApplication.hpp"

/**
* @brief exception thrown if an IPOPT option was set to an infeasible value
*/
class WrongOptionValueException : public std::exception{
protected:
public:
  WrongOptionValueException(){};
};

#else
class IpoptNotImplementedException : public std::exception{
public:
  //~IpoptNotImplementedException() throw() {}
  const char *what() const throw(){return "Ipopt was not provided at compile time - cannot use Ipopt.\n";}
};
#endif

#ifdef BUILD_IPOPT
//maybe class functions? But then the header has to include IpIpoptApplication.hpp
//the header should be independent from any IpOpt headers.
//it could be included with #ifdef HAVE_IPOPT macro

// look at definition of the functions for documentation
bool checkForStringOptions(std::string &option, Logger::Ptr &logger);

bool checkForIntOptions(std::string &option, const std::string &sValue, Ipopt::Index &value, Logger::Ptr &logger);

bool checkForDoubleOptions(std::string &option, const std::string &sValue, Ipopt::Number &value, Logger::Ptr &logger);

void rewriteSnoptOption(std::string &option);


/**
*@brief set user given IPOPT options to IpoptApplication object
* @param app pointer to created IpoptApplication objet
* @param ipoptOptions given user options
* @param logger Logger to which all output is written
*/ 
void setOptions(Ipopt::SmartPtr<Ipopt::IpoptApplication> &app,
                const std::map<std::string,std::string> &ipoptOptions,
                Logger::Ptr &logger)
{
    std::map<std::string,std::string>::const_iterator iter;
  for(iter = ipoptOptions.begin(); iter != ipoptOptions.end(); iter++){
    std::string keyword = iter->first;
    std::string value = iter->second;
    rewriteSnoptOption(keyword);
    Ipopt::Index intValue;
    Ipopt::Number doubleValue;
    try{
      if(checkForStringOptions(keyword, logger)){
        app->Options()->SetStringValue(keyword, iter->second);
      }
      else if(checkForIntOptions(keyword, value, intValue, logger)){
        app->Options()->SetIntegerValue(keyword, intValue);
      }
      else if(checkForDoubleOptions(keyword, value, doubleValue, logger)){
        app->Options()->SetNumericValue(keyword, doubleValue);
      }
      else{
        std::string message = "\nunrecognized option or not convertible value, try to set it as string option\n" ;
        logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, message);
        app->Options()->SetStringValue(iter->first, iter->second);
      }
    }
    catch(WrongOptionValueException){
    }
    catch(...){
      std::string message = "unknown exception thrown while setting options for IPOPT\n";
      logger->sendMessage(Logger::OPTIMIZER,OutputChannel::STANDARD_VERBOSITY, message);
    }
  }
}
#endif


DyosOutput::OptimizerOutput IpoptWrapper::solve()
{
#ifdef BUILD_IPOPT
  DyosTNLP* mydyosnlpPtr = new DyosTNLP(m_optProb);
  mydyosnlpPtr->setLogger(m_logger);
  Ipopt::SmartPtr<DyosTNLP> mydyosnlp(mydyosnlpPtr);
  Ipopt::SmartPtr<Ipopt::TNLP> mynlp(GetRawPtr(mydyosnlp));
  Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();
  Ipopt::ApplicationReturnStatus status;
  DyosOutput::OptimizerOutput retval;
  status = app->Initialize();
  if (status != Ipopt::Solve_Succeeded) {
    m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "\n\n*** Error during initialization!\n" );
    retval.m_informFlag = DyosOutput::OptimizerOutput::FAILED;
  }
  // in first order optimization: set Hessian to limited-memory BFGS
  if (m_optProb->getOptimizationOrder() == OptimizationProblem::FIRST_ORDER) {
    app->Options()->SetStringValue("hessian_approximation","limited-memory","");
  }  
  //should normally be outcommended, uncommend if 2nd derivative check is to be made
  //app->Options()->SetStringValue("derivative_test", "second-order"); // check hessian
  // this can be set now directly in the options
  setOptions(app, m_ipoptOptions, m_logger);

  
  mydyosnlpPtr->m_capture.beginCapture();
  status = app->OptimizeTNLP(mynlp);
  mydyosnlpPtr->m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY,
                        mydyosnlpPtr->m_capture.getCapture());

  switch(status){
    case Ipopt::Solve_Succeeded:
      retval.m_informFlag = DyosOutput::OptimizerOutput::OK;
      break;
    case Ipopt::Infeasible_Problem_Detected:
      retval.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
      break;
    case Ipopt::Maximum_Iterations_Exceeded:
      retval.m_informFlag = DyosOutput::OptimizerOutput::TOO_MANY_ITERATIONS;
      break;
    case Ipopt::Solved_To_Acceptable_Level:
    case Ipopt::Feasible_Point_Found:
      retval.m_informFlag = DyosOutput::OptimizerOutput::NOT_OPTIMAL;
      break;
    default:
      retval.m_informFlag = DyosOutput::OptimizerOutput::FAILED;
  }
  
  //retval = mydyosnlp->get_optimizer_output();
  getOptimizerOutput(retval);
  return retval;
#else
  throw (IpoptNotImplementedException());
#endif
}


#ifdef BUILD_IPOPT
//maybe class functions? But then the header has to include IpIpoptApplication.hpp

/**
* @brief check if the given option is a string option
*
* @param option to be checked
* @logger Logger to which all output is written
* @return true if option could be identified as an IPOPT string option
*/
bool checkForStringOptions(std::string &option, Logger::Ptr &logger){
  if(option.compare("print_user_options")==0){
    return true;
  }
  else if(option.compare("print_options_documentation")==0){
    return true;
  }
  else if(option.compare("output_file")==0){
    return true;
  }
  else if(option.compare("option_file_name")==0){
    return true;
  }
  else if(option.compare("print_info_string")==0){
    return true;
  }
  else if(option.compare("info_pr_output")==0){
    return true;
  }
  else if(option.compare("print_timing_statistics")==0){
    return true;
  }
  else if(option.compare("nlp_scaling_method")==0){
    return true;
  }
  else if(option.compare("honor_original_bounds")==0){
    return true;
  }
  else if(option.compare("check_derivatives_for_naninf")==0){
    return true;
  }
  else if(option.compare("fixed_variable_treatment")==0){
    return true;
  }
  else if(option.compare("jac_c_constant")==0){
    return true;
  }
  else if(option.compare("jac_d_constant")==0){
    return true;
  }
  else if(option.compare("hessian_constant")==0){
    return true;
  }
  else if(option.compare("bound_mult_init_method")==0){
    return true;
  }
  else if(option.compare("mehrotra_algorithm")==0){
    return true;
  }
  else if(option.compare("mu_strategy")==0){
    return true;
  }
  else if(option.compare("mu_oracle")==0){
    return true;
  }
  else if(option.compare("fixed_mu_oracle")==0){
    return true;
  }
  else if(option.compare("adaptive_mu_globalization")==0){
    return true;
  }
  else if(option.compare("alpha_for_y")==0){
    return true;
  }
  else if(option.compare("recalc_y")==0){
    return true;
  }
  else if(option.compare("accept_every_trial_step")==0){
    return true;
  }
  else if(option.compare("corrector_type")==0){
    return true;
  }
  else if(option.compare("warm_start_init_point")==0){
    return true;
  }
  else if(option.compare("expect_infeasible_problem")==0){
    return true;
  }
  else if(option.compare("start_with_resto")==0){
    return true;
  }
  else if(option.compare("evaluate_orig_obj_at_resto_trial")==0){
    return true;
  }
  else if(option.compare("linear_solver")==0){
    return true;
  }
  else if(option.compare("linear_system_scaling")==0){
    return true;
  }
  else if(option.compare("linear_scaling_on_demand")==0){
    return true;
  }
  else if(option.compare("hessian_approximation")==0){
    return true;
  }
  else if(option.compare("limited_memory_update_type")==0){
    return true;
  }
  else if(option.compare("limited_memory_initialization")==0){
    return true;
  }
  else if(option.compare("limited_memory_special_for_resto")==0){
    return true;
  }
  else if(option.compare("derivative_test")==0){
    return true;
  }
  else if(option.compare("derivative_test_print_all")==0){
    return true;
  }
  else if(option.compare("ma57_automatic_scaling")==0){
    return true;
  }
  else if(option.compare("ma77_order")==0){
    return true;
  }
  else if(option.compare("ma86_order")==0){
    return true;
  }
  else if(option.compare("ma86_scaling")==0){
    return true;
  }
  else if(option.compare("ma97_order")==0){
    return true;
  }
  else if(option.compare("ma97_scaling")==0){
    return true;
  }
  else if(option.compare("ma97_scaling1")==0){
    return true;
  }
  else if(option.compare("ma97_scaling2")==0){
    return true;
  }
  else if(option.compare("ma97_scaling3")==0){
    return true;
  }
  else if(option.compare("ma97_solve_blas3")==0){
    return true;
  }
  else if(option.compare("ma97_switch1")==0){
    return true;
  }
  else if(option.compare("ma97_switch2")==0){
    return true;
  }
  else if(option.compare("ma97_switch3")==0){
    return true;
  }
  else if(option.compare("pardiso_matching_strategy")==0){
    return true;
  }
  else if(option.compare("pardiso_order")==0){
    return true;
  }
  else{
    return false;
  }
}

/**
* @brief write error message if wrong option value has been set
*
* @param option option keyword of the failed option
* @param value option value of the failed option
* @param logger Logger in which the output is written
* @param lowerBound lower bound of the range of allowed values
* @param upperBound upper bound of the range of allowed values
*/
void wrongOptionError(const std::string &option, const std::string value,
                      Logger::Ptr &logger,
                      const std::string &lowerBound, const std::string &upperBound)
{
  std::stringstream sstream;
  sstream<<"Value out of bound for option "<<option<<"\n";
  sstream<<"Value "<<value<<" did not fit bounds ["<<lowerBound<<","<<upperBound<<"]\n";
  logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, sstream.str());
  throw WrongOptionValueException();
}

void checkIntOptionValue(const std::string &option, const Ipopt::Index &value, Logger::Ptr &logger,
                         int lowerBound, int upperBound)
{  
  if(value<lowerBound || value > upperBound){
    std::stringstream sstream;
    std::string sValue;
    std::string sLower;
    std::string sUpper;
    sstream<<value;
    sstream>>sValue;
    sstream<<lowerBound;
    sstream>>sLower;
    sstream<<upperBound;
    sstream>>sUpper;
    wrongOptionError(option, sValue, logger, sLower, sUpper);
  }
}

void checkDoubleOptionValue(const std::string &option, Ipopt::Number &value,
                                  Logger::Ptr &logger, 
                                  double lowerBound, double upperBound)
{
  if(value<=lowerBound ||value >= upperBound){
    std::stringstream sstream;
    std::string sValue;
    std::string sLower;
    std::string sUpper;
    sstream<<value;
    sstream>>sValue;
    sstream<<lowerBound;
    sstream>>sLower;
    sstream<<upperBound;
    sstream>>sUpper;
    wrongOptionError(option, sValue, logger, sLower, sUpper);
  }
}

bool checkForIntOptions(std::string &option, const std::string &sValue, 
                        Ipopt::Index &value, Logger::Ptr &logger){
  // caution, sstream here would convert 1e-6 to 1, there is no real convenient integer check. 
  // So the user could try to set a double value to an integer option
  std::stringstream sstream;
  try{
    sstream<<sValue;
    sstream>>value;
  }
  catch(...){
    //if anything goes amiss 
    return false;
  }
  if(option.compare("print_level")==0){
    checkIntOptionValue(option,value, logger, 0, 12);
    return true;
  }
  else if(option.compare("print_frequency_time")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("print_frequency_time")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("file_print_level")==0){
    checkIntOptionValue(option, value, logger, 0, 12);
    return true;
  }
  else if(option.compare("max_iter")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("acceptable_iter")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("quality_function_max_section_steps")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("max_soc")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("watchdog_shortened_iter_trigger")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("watchdog_trial_iter_max")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("max_refinement_steps")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("min_refinement_steps")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("limited_memory_max_history")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("limited_memory_max_skipping")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("derivative_test_first_index")==0){
    checkIntOptionValue(option, value, logger, -2, INT_MAX);
    return true;
  }
  else if(option.compare("ma57_pivot_order")==0){
    checkIntOptionValue(option, value, logger, 0, 5);
    return true;
  }
  else if(option.compare("ma57_block_size")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("ma57_small_pivot_flag")==0){
    checkIntOptionValue(option, value, logger, 0, 1);
    return true;
  }
  else if(option.compare("ma57_node_amalgamation")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("ma77_print_level")==0){
    return true;
  }
  else if(option.compare("ma77_buffer_lpage")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("ma77_buffer_npage")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("ma77_file_size")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("ma77_maxstore")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("ma77_nemin")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("ma86_print_level")==0){
    return true;
  }
  else if(option.compare("ma86_nemin")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("ma97_print_level")==0){
    return true;
  }
  else if(option.compare("ma97_nemin")==0){
    checkIntOptionValue(option, value, logger, 1, INT_MAX);
    return true;
  }
  else if(option.compare("mumps_mem_percent")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("mumps_permuting_scaling")==0){
    checkIntOptionValue(option, value, logger, 0, 7);
    return true;
  }
  else if(option.compare("mumps_pivot_order")==0){
    checkIntOptionValue(option, value, logger, 0, 7);
    return true;
  }
  else if(option.compare("mumps_scaling")==0){
    checkIntOptionValue(option, value, logger, -2, 77);
    return true;
  }
  else if(option.compare("pardiso_max_iterative_refinement_steps")==0){
    return true;
  }
  else if(option.compare("pardiso_msglvl")==0){
    checkIntOptionValue(option, value, logger, 0, INT_MAX);
    return true;
  }
  else if(option.compare("wsmp_num_threads")==0){
    return true;
  }
  else if(option.compare("wsmp_ordering_option")==0){
    checkIntOptionValue(option, value, logger, -2, 3);
    return true;
  }
  else if(option.compare("wsmp_scaling")==0){
    checkIntOptionValue(option, value, logger, 0, 3);
    return true;
  }

  else{
    return false;
  }
  return true;
}

bool checkForDoubleOptions(std::string &option, const std::string &sValue, 
                           Ipopt::Number &value, Logger::Ptr &logger){
  std::stringstream sstream;
  const double inf = std::numeric_limits<double>::infinity();
  const double eps = 1e-20;
  try{
    sstream<<sValue;
    sstream>>value;
  }
  catch(...){
    return false;
  }
  if(option.compare("print_frequency_time")==0){
    //-eps as lowerbound if 0.0 is an allowed value
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("tol")==0){
    //0.0 as lowerbound means that 0.0 is excluded
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("max_cpu_time")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("dual_inf_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("constr_viol_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("compl_inf_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("acceptable_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("acceptable_constr_viol_tol")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("acceptable_dual_inf_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("acceptable_compl_inf_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("acceptable_obj_change_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("diverging_iterates_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("obj_scaling_factor")==0){
    return true;
  }
  else if(option.compare("nlp_scaling_max_gradient")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("nlp_scaling_min_value")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("bound_relax_factor")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("nlp_lower_bound_inf")==0){
    return true;
  }
  else if(option.compare("nlp_upper_bound_inf")==0){
    return true;
  }
  else if(option.compare("bound_frac")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 0.5+eps);
    return true;
  }
  else if(option.compare("bound_push")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("slack_bound_frac")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 0.5+eps);
    return true;
  }
  else if(option.compare("slack_bound_push")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("bound_mult_init_val")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("constr_mult_init_max")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("mu_init")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("mu_max_fact")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("mu_max")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("mu_min")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("mu_target")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("barrier_tol_factor")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("mu_linear_decrease_factor")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("mu_superlinear_decrease_power")==0){
    checkDoubleOptionValue(option, value, logger, 1.0, 2.0);
    return true;
  }
  else if(option.compare("alpha_for_y_tol")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("recalc_y_feas_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("warm_start_bound_push")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("warm_start_bound_frac")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 0.5+eps);
    return true;
  }
  else if(option.compare("warm_start_slack_bound_frac")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 0.5+eps);
    return true;
  }
  else if(option.compare("warm_start_slack_bound_push")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("warm_start_mult_bound_push")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("warm_start_mult_init_max")==0){
    return true;
  }
  else if(option.compare("expect_infeasible_problem_ctol")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("expect_infeasible_problem_ytol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("soft_resto_pderror_reduction_factor")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("required_infeasibility_reduction")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 1.0);
    return true;
  }
  else if(option.compare("bound_mult_reset_threshold")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("constr_mult_reset_threshold")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("expect_infeasible_problem_ctol")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("expect_infeasible_problem_ytol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("soft_resto_pderror_reduction_factor")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("required_infeasibility_reduction")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 1.0);
    return true;
  }
  else if(option.compare("bound_mult_reset_threshold")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("constr_mult_reset_threshold")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("max_hessian_perturbation")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("min_hessian_perturbation")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("first_hessian_perturbation")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("perturb_inc_fact_first")==0){
    checkDoubleOptionValue(option, value, logger, 1.0, inf);
    return true;
  }
  else if(option.compare("perturb_inc_fact")==0){
    checkDoubleOptionValue(option, value, logger, 1.0, inf);
    return true;
  }
  else if(option.compare("perturb_dec_fact")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("jacobian_regularization_value")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("limited_memory_init_val")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("limited_meory_init_val_min")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("limited_memory_init_val_min")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("derivative_test_perturbation")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("derivative_test_tol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, inf);
    return true;
  }
  else if(option.compare("point_perturbation_radius")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("ma27_pivtol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("ma27_pivtolmax")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("ma27_liw_init_factor")==0){
    checkDoubleOptionValue(option, value, logger, 1.0-eps, inf);
    return true;
  }
  else if(option.compare("ma27_la_init_factor")==0){
    checkDoubleOptionValue(option, value, logger, 1.0-eps, inf);
    return true;
  }
  else if(option.compare("ma27_meminc_factor")==0){
    checkDoubleOptionValue(option, value, logger, 1.0-eps, inf);
    return true;
  }
  else if(option.compare("ma57_pivtol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("ma57_pivtolmax")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("ma57_pre_alloc")==0){
    checkDoubleOptionValue(option, value, logger, 1.0-eps, inf);
    return true;
  }
  else if(option.compare("ma77_small")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("ma77_static")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("ma77_u")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 0.5+eps);
    return true;
  }
  else if(option.compare("ma77_umax")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 0.5+eps);
    return true;
  }
  else if(option.compare("ma86_small")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("ma86_static")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("ma86_u")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 0.5+eps);
    return true;
  }
  else if(option.compare("ma86_max")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 0.5+eps);
    return true;
  }
  else if(option.compare("ma97_small")==0){
    checkDoubleOptionValue(option, value, logger, -eps, inf);
    return true;
  }
  else if(option.compare("ma97_u")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 0.5+eps);
    return true;
  }
  else if(option.compare("ma97_max")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 0.5+eps);
    return true;
  }
  else if(option.compare("mumps_pitvol")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 1.0+eps);
    return true;
  }
  else if(option.compare("mumps_pivtolmax")==0){
    checkDoubleOptionValue(option, value, logger, -eps, 1.0+eps);
    return true;
  }
  else if(option.compare("wsmp_pivtol")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("wsmp_pivtolmax")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else if(option.compare("wsmp_singularity_threshold")==0){
    checkDoubleOptionValue(option, value, logger, 0.0, 1.0);
    return true;
  }
  else{
    return false;
  }
  return true;
}


/**
* @brief rewrite some snopt options to corresponding IPOPT options
*
* @param option option to be rewritten
* @note Since IPOPT and SNOPT use entirely different algorithms it is not sure whether 
*       such functionality is desirable at all. It could be better to let the user switch 
*       to IPOPT options instead of doing this for very few cases automatically
*/
void rewriteSnoptOption(std::string &option)
{
  if(option.compare("Major optimality tolerance")==0){
    option = "tol";
  }
  else if(option.compare("Major iterations limit")==0){
    option = "max_iter";
  }
  else if(option.compare("Major feasibility limit")==0){
    //not entirely sure whether this makes sense
    //dual_inf_tol: Absolute tolerance on the dual infeasibility
    //Major feasibility tolerance: target nonlinear constraint violation
    option = "dual_inf_tol";
  }
  else if(option.compare("violation limit")==0){
    option = "constr_viol_tol";
  }
}
#endif