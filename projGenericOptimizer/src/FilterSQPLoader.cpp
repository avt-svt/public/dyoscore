/** @file FilterSQPLoader.cpp
*    @brief definitions of member functions of FilterSQPLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails FilterSQPLoader
*    =====================================================================\n
*   @author Mohammadsobhan Moazemi Goodarzi, Fady Assassa
*   @date 22.11.2012
*/

#include "FilterSQPLoader.hpp"
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#ifdef WIN32
#define FORTRAN_UNDERSCORE ""
#else
#define FORTRAN_UNDERSCORE "_"
#endif
/**
* @brief standard constructer of FilterSQPLoader
*
* loads the dll filterSQP.dll
*/
FilterSQPLoader::FilterSQPLoader() : Loader("filterSQP")
{
  loadFunctions();
}

/**
* @brief copy constructer of FilterSQPLoader
*
* @param filterSQPLoader FilterSQPLoader to be copied
*/
FilterSQPLoader::FilterSQPLoader(const FilterSQPLoader &filterSQPLoader) : Loader(filterSQPLoader)
{
  loadFunctions();
}

/**
* @brief load all functions from FilterSQP DLL which are required by DyOS
*/
void FilterSQPLoader::loadFunctions()
{
  // function handles have to be initialized to NULL, otherwise in Loader.hpp
  // it is assumed that they have already been loaded
  filterSQPFuncHandle = NULL;

  // load functions from FilterSQP DLL using Windows system function GetProAddress()
  loadFcn(filterSQPFuncHandle, "filtersqp" FORTRAN_UNDERSCORE);
}

void FilterSQPLoader::filterSQP(int n, int numConstraints, int kmax, int maxa, int maxf,
                 int mlp, int mxwk, int mxiwk, int iprint,
                 int nout, int &ifail, double rho, utils::Array<double> &x,
                 utils::Array<double> &c, double f, double fmin, utils::Array<double> &blo,
                 utils::Array<double> &bup, utils::Array<double> &s, utils::Array<double> &a,
                 utils::Array<int> &la, utils::Array<double> &ws, utils::Array<int> &lws,
                 utils::Array<double> &lam, utils::Array<char> &cstype, int cstypelen,
                 utils::Array<double> &user, utils::Array<int> &iuser, int max_iter,
                 utils::Array<int> &istat, utils::Array<double> &rstat,
                 objfunPtrFilterSQP_t objfun_ptr, confunPtrFilterSQP_t confun_ptr,
                 gradientPtrFilterSQP_t gradient_ptr, hessianPtrFilterSQP_t hessian_ptr, Wdotd_t wdot_ptr)
{
  filterSQPFuncHandle(&n, &numConstraints, &kmax, &maxa, &maxf, &mlp, &mxwk, &mxiwk,
                      &iprint, &nout, &ifail, &rho, x.getData(), c.getData(), &f, &fmin, blo.getData(),
                      bup.getData(), s.getData(), a.getData(), la.getData(), ws.getData(),
                      lws.getData(), lam.getData(), cstype.getData(),
                      user.getData(), iuser.getData(), &max_iter, istat.getData(), rstat.getData(), 
                      objfun_ptr, confun_ptr, gradient_ptr, hessian_ptr, wdot_ptr
                      );
}