/**
* @file NPSOLWrapper.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* NPSOLWrapper - Part of DyOS                                          \n
* =====================================================================\n
* @author Mathias Dunst
* @date 31.10.2012
*/


#include "NPSOLWrapper.hpp"
#include "NPSOLLoader.hpp"

#include "cs.h"

#include <boost/filesystem.hpp>

#include <string>
#include <cassert>

// initialization of statics. Instead of doing it in every test suite we do it here for all together.
std::weak_ptr<OptimizationProblem> NPSOLWrapper::m_staticOptProb;
std::weak_ptr<Logger> NPSOLWrapper::m_staticLogger;
int NPSOLWrapper::m_summaryFileHandleNPSOL = 0;
std::string NPSOLWrapper::m_summaryFileName;

/**
* @brief constructor: initialize resources which are independent of current problem (e.g. open output files)
*
* @param optMD pointer to an OptimizerMetaData object used for the optimization
* @param genInt pointer to an integrator object used for the optimization
*/
NPSOLWrapper::NPSOLWrapper(const OptimizationProblem::Ptr& optProb, const std::map<std::string,std::string> &optimizerOptions)
{
	boost::filesystem::path pathBoost("input");
	boost::filesystem::create_directories(pathBoost);
  
  const char* optimizerOptionsFile = "input/npsol.config";
  std::map<std::string, std::string> matchedOptimizerOptions;
  std::map<std::string, std::string> optimizerOptionsCopy;
  NPSOLOptions validOptions;

  if (!boost::filesystem::exists(optimizerOptionsFile)){
    //if needed: set standard options, key needs to be written in lower case
    optimizerOptionsCopy = validOptions.checkOptions(optimizerOptions);
    matchOptimizerOptions(matchedOptimizerOptions, optimizerOptionsCopy);
    optimizerOptionsFile = "input/npsol2.config";
    writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
  }
  else{
    //if options were added in UserInput they need to be matched with existing options in file
    if(optimizerOptions.empty()==false){
      std::vector<std::string> optionsVec(getOptimizerOptions(optimizerOptionsFile));
      matchedOptimizerOptions = validOptions.checkOptions(optionsVec);
      optimizerOptionsCopy = validOptions.checkOptions(optimizerOptions);
      matchOptimizerOptions(matchedOptimizerOptions, optimizerOptionsCopy);
      //write matchedOptions into optimizerOptionsFile
      optimizerOptionsFile = "input/npsol2.config";
      writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
    }
    else{
      std::vector<std::string> optionsVec(getOptimizerOptions(optimizerOptionsFile));
      matchedOptimizerOptions = validOptions.checkOptions(optionsVec);
      optimizerOptionsFile = "input/npsol2.config";
      writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
    }
  }

  m_optProb = optProb;

// specify output files
  const int stdoutFileHandle = 6;


  npsol.npsolReadOptionFile(4, optimizerOptionsFile);


  const std::string printFileNPSOL = "npsol.out";
  m_printFileHandleNPSOL = 9;      /* File handler for Output file, iprint  */

  m_summaryFileName = "npsol.temp";
  m_summaryFileHandleNPSOL = 10;

  // print file
  if (m_printFileHandleNPSOL != stdoutFileHandle) { // what about no output (-1 or 0?)
    npsol.npsolOpenFile(m_printFileHandleNPSOL, printFileNPSOL);
  }

  // summary file
  if (m_summaryFileHandleNPSOL != stdoutFileHandle) { // what about no outpt (-1 or 0?)
    npsol.npsolOpenFile(m_summaryFileHandleNPSOL, m_summaryFileName);
  }
}

/**
* @brief destructor: free resources allocated in constructor (e.g. close output file)
*/
NPSOLWrapper::~NPSOLWrapper()
{
  const int stdoutFileHandle = 6;

  // closing output files
  if (m_printFileHandleNPSOL != stdoutFileHandle && m_printFileHandleNPSOL > 0) {
    npsol.npsolCloseFile(m_printFileHandleNPSOL);
  }

  if (m_summaryFileHandleNPSOL != stdoutFileHandle && m_printFileHandleNPSOL > 0) {
    npsol.npsolCloseFile(m_summaryFileHandleNPSOL);
  }
}


void NPSOLWrapper::logNPSOLOutput(Logger::Ptr &logger)
{
  //Capture NPSOL output
  NPSOLLoader npsol;
  npsol.npsolCloseFile(m_summaryFileHandleNPSOL);
  std::fstream fileReader(m_summaryFileName.c_str(), std::ios_base::in);
  std::string content;
  
  bool firstline = true;
  while(!fileReader.eof()){
    if(!firstline){
      logger->newline(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY);
    }
    firstline = false;
    std::getline(fileReader, content);
    logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY,
                              content);
    
  }
  fileReader.close();
  // output sent to logger should not be read again
  // by opening the file for writing the content is deleted
  std::fstream deleteContent(m_summaryFileName.c_str(), std::ios_base::out);
  deleteContent.close();
  npsol.npsolOpenFile(m_summaryFileHandleNPSOL, m_summaryFileName);
}

/**
* @brief solve optimization problem
*/
DyosOutput::OptimizerOutput NPSOLWrapper::solve()
{
  m_staticOptProb = this->m_optProb;
  m_staticLogger = this->m_logger;
  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();

  const int numOptVars = optProbDim.numOptVars; //number of optimization variables
  const int numLinConstraintsVars = optProbDim.numLinConstraints; //number of general linear constraints
  const int numNonLinConstraintsVars = optProbDim.numNonLinConstraints; //number of nonlinear constraints

  //initializaion of matrix containing the linear constraints and matrix row dimension
  int numLinConstraintsDim = 0;//row dimension of the matrix of linear constraints
  std::vector<double> linConstraintsMatrix(numLinConstraintsDim);
  assembleLinConstraintsMatrix(linConstraintsMatrix, numLinConstraintsDim);

  //initilize lower and upper bounds of all constraints and the set of variables
  std::vector<double> lowerBounds(numOptVars+numLinConstraintsVars+numNonLinConstraintsVars);
  std::vector<double> upperBounds(numOptVars+numLinConstraintsVars+numNonLinConstraintsVars);
  assembleBounds(lowerBounds, upperBounds);

  int inform = 0; //only for output, holds information of the result
  int iterations = 0; //only for output, number of major iterations

  //holds information about the status of the constraints
  //only initialization with 0, due to default option 'Cold Start'
  std::vector<int> constraintsStatus(numOptVars+numLinConstraintsVars+numNonLinConstraintsVars);

  //vector of nonlinear constraint functions
  std::vector<double> nonLinConstraintFunctions(numNonLinConstraintsVars);

  //initialize matrix of nonlinear constraints and matrix row dimension
  int numJacDim = 0;//row dimension of the matrix of nonlinear constraints
  std::vector<double> jacobianVals(numNonLinConstraintsVars);
  assembleJacobian(jacobianVals, numJacDim);

  //only initialization with 0, due to default option 'Cold Start'
  std::vector<double> qpMultipliers(numOptVars+numLinConstraintsVars+numNonLinConstraintsVars);

  //only for output, value of final iteraion
  double finalValue = 0.0;

  //gardient of objective function
  std::vector<double> gradientOptFunction(numOptVars);

  //matrix of the Hessian of the Lagrangian
  //initialization with 0, due to default option 'Cold Start'
  const int numHesseLagrangeDim = numOptVars; //row dimension of Hessian of the Lagrangian
  std::vector<double> matrixHesseLagrange(numHesseLagrangeDim*numOptVars);

  //initialization of optimization variables
  utils::Array<double> optVariables(numOptVars, 0.0);
  m_optProb->getDecVarValues(optVariables);

  //allocate workspace required by NPSOL
  int lenInt;//lenght of integer workspace array
  int lenReal;//length of real workspace array
  allocateNPSOLWorkspace(lenInt, lenReal, numOptVars, numLinConstraintsVars, numNonLinConstraintsVars);

  assert(!linConstraintsMatrix.empty());
  assert(!lowerBounds.empty());
  assert(!upperBounds.empty());
  assert(!jacobianVals.empty());
  assert(!nonLinConstraintFunctions.empty());
  assert(!gradientOptFunction.empty());

  npsol.npsol(numOptVars, //n
              numLinConstraintsVars, //nclin
              numNonLinConstraintsVars, //ncnln
              numLinConstraintsDim, //ldA
              numJacDim, //ldJ
              numHesseLagrangeDim, //ldR
              &linConstraintsMatrix[0], //A
              &lowerBounds[0], //bl
              &upperBounds[0], //bu
              &NPSOLWrapper::confun,
              &NPSOLWrapper::objfun,
              inform,
              iterations, //iter
              &constraintsStatus[0], //istate
              &nonLinConstraintFunctions[0], //c
              &jacobianVals[0], //cJac
              &qpMultipliers[0], //clamda
              finalValue, //f
              &gradientOptFunction[0], //g
              &matrixHesseLagrange[0], //R
              optVariables.getData(), //x
              &m_intWorkspace[0], //iw
              lenInt, //leniw
              &m_realWorkspace[0], //w
              lenReal); //lenw
  
  // put output (objective function value, values of decision variables back to DyOS structs
  utils::WrappingArray<const double> optVarValuesOut(numOptVars, optVariables.getData());
  m_optProb->setDecVarValues(optVarValuesOut);// setDecVarValues needs 'const double' type
  utils::WrappingArray<double> linLagrangeMultipliers(numLinConstraintsVars,
                                                     &qpMultipliers[0] + numOptVars);
  utils::WrappingArray<double> nonLinLagrangeMultipliers(numNonLinConstraintsVars,
                                                        &qpMultipliers[0] + numOptVars
                                                        + numLinConstraintsVars);
  // npsol returns Lagrange multipliers with inverted sign (with respect to the Honeybee standard)
  // this causes adjoints to be wrong (not just with inverted sign) - so invert here Lagrange
  // multipliers before they are set to meta data
  for(unsigned i=0; i<nonLinLagrangeMultipliers.getSize(); i++){
    nonLinLagrangeMultipliers[i] *= -1.0;
  }
  for(unsigned i=0; i<linLagrangeMultipliers.getSize(); i++){
    linLagrangeMultipliers[i] *= -1.0;
  }
  m_optProb->setLagrangeMultipliers(nonLinLagrangeMultipliers, linLagrangeMultipliers);
  
  utils::WrappingArray<double> optVarLagrangeMultipliers(numOptVars, &qpMultipliers[0]);
  // for consistency invert sign of opt var Lagrange multipliers as well
  for(unsigned i=0; i < optVarLagrangeMultipliers.getSize(); i++){
    optVarLagrangeMultipliers[i] *= -1.0;
  }
  m_optProb->setDecVarLagrangeMultipliers(optVarLagrangeMultipliers);
  
  
  logNPSOLOutput(m_logger);

  //return output
  DyosOutput::OptimizerOutput optOut;
  switch(inform){
    case 0:
      //the iterates have converged to a point x that satisfies the first-order optimality conditions
      optOut.m_informFlag = DyosOutput::OptimizerOutput::OK;
      break;
    case 1:
      //the point x satisfies the first-order optimality conditions to the accuracy requested,
      //but the sequence of iterates has not yet converged
      optOut.m_informFlag = DyosOutput::OptimizerOutput::NOT_OPTIMAL;
      break;
    case 2:
      //the linear constraints and bounds have not been satisfied.
      //This means that either no feasible point exists for the given value or
      //no feasible point could be found in the number of iterations
      optOut.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
      break;
    case 3:
      //There has been a sequence of QP subproblems for which no feasible point could be found
      optOut.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
      break;
    case 4:
      //If the algorithm appears to be making progress, Major iteration limit may be too small.
      optOut.m_informFlag = DyosOutput::OptimizerOutput::TOO_MANY_ITERATIONS;
      break;
    case 6:
      //A suficient decrease in the merit function could not be attained during the final line search
      optOut.m_informFlag = DyosOutput::OptimizerOutput::NOT_OPTIMAL;
      break;
    case 7:
      //Large errors were found in the derivatives of the objective function and/or nonlinear constraints
      optOut.m_informFlag = DyosOutput::OptimizerOutput::WRONG_GRADIENTS;
      break;
    case 9:
      //An input parameter is invalid
      optOut.m_informFlag = DyosOutput::OptimizerOutput::FAILED;
      break;
    default:
      assert(false);
      break;
  }
  getOptimizerOutput(optOut);
  return optOut;
}

/**
* @brief static callback function used by the NPSOL library to set  the value
* and derivatives of the objective function
* 
* @param[in, out] mode input: is set by NPSOL to indicate which values are to be assigned during the call of funobj
*                      output: can be used to end the solution of the current problem
* @param[in] n is the number of variables - must be >0
* @param[in] x is an array of dimension at least n containing the values of the variables x
*              for which f must be evaluated
* @param[out] f must contain the computed value of f(x)
* @param[out] g must contain the assigned components of the gradient vector g(x)
* @param[in] state (NPSOL option NSTATE) is set 1 if NPSOL is calling objfun for the first time. This allows the user to save computation time if certain data must be read or calculated only once
* @note all parameters are described in the User's Guide for NPSOL
*/
void NPSOLWrapper::objfun(int *mode, int *n, double *x, double *f, double *g, int *state)
{
  assert(*mode==0 || *mode==1 || *mode==2);
  assert(*n>0);

  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();

  logNPSOLOutput(staticLogger);

  utils::WrappingArray<const double> xArray(*n, x);

  if(staticOptProb->getOptProbDim().numNonLinConstraints == 0){
    // we only need to evaluate the decision variables, when there are not constraints.
    // Because npsol doesn't change the decision variables between confun and objfun.
    // Also confun is always called first.
    staticOptProb->setDecVarValues(xArray);
  }

  OptimizationProblem::ResultFlag result;
  // get objective function value
  if(*mode==0 || *mode==2){
    result = staticOptProb->getNonLinObjValue(*f);
    if(result == OptimizationProblem::FAILED){
      *mode = -1;
    }
  }
  // store the gradient at current point x into g
  if(*mode==1 || *mode==2){
    utils::WrappingArray<double> gArray(*n, g);
    result = staticOptProb->getNonLinObjDerivative(gArray);
     if(result == OptimizationProblem::FAILED){
      *mode = -1;
    }
  }

  if(*state>=2){
    staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "DyOS: Finished optimization with NPSOL\n" );
  }
}

/**
* @brief static callback function used by the NPSOL library to set value vector and Jacobian
* of the nonlinear constraints.
*
* @param[in, out] mode input: is set by NPSOL to indicate the values that must be assigned during each call of funcon
*                      output: can be used to end the solution of the current problem
* @param[in] numNonLinConstraints is the number of nonlinear constraints - must be >=0
* @param[in] n is the number of variables - must be >0
* @param[in] rowJacobian is the leading dimension of the array jacobianConstraints - must be >=1 and >=ncnln
* @param[in] needConstraints is an array of dimension at least numNonLinConstraints containing the indices
*                            of the elements of constraints or jacobianConstraints that must be evaluated by funcon
* @param[in] x is an array of dimension at least n containing the values of the variables x for which
*              the constraints must be evaluated
* @param[out] constraintsVal contains the appropriate values of the nonlinear constraints
* @param[out] jacobianConstraints contains the appropriate elements of the Jacobian evaluated at x
* @param[in] state has the same meaning as for funobj
* @note all parameters are described in the User's Guide for NPSOL
*/
void NPSOLWrapper::confun(int *mode, int *numNonLinConstraints, int *n, int *rowJacobian, int *needConstraints,
                          double *x, double *constraintsVal, double *jacobianConstraints, int *state)
{
  //parameter needc is neglected, to act on the assumption, that all the constraints will be provided.
  assert(*mode==0 || *mode==1 || *mode==2);
  assert(*n>0);
  assert(*numNonLinConstraints>=0);

  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();
  OptimizationProblem::ResultFlag result;
  
  logNPSOLOutput(staticLogger);


  if(*state == 1){
    staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "DyOS: Starting optimization with NPSOL\n" );
  }

  utils::WrappingArray<const double> xArray(*n , x);
  staticOptProb->setDecVarValues(xArray);

  //get the nonlinear constraints of constraintsVal
  if(*mode == 2 || *mode==0){
    utils::WrappingArray<double> constraintsArray(*numNonLinConstraints, constraintsVal);
    result = staticOptProb->getNonLinConstraintValues(constraintsArray);
    if(result == OptimizationProblem::FAILED){
      *mode = -1;
    }
  }

  //get the values of cJac
  if(*mode >= 1){
    CsTripletMatrix::Ptr jacobianMatrixStruct = staticOptProb->getOptProbDim().nonLinJacDecVar;

    double *values = new double[jacobianMatrixStruct->get_nnz()];
    CsTripletMatrix::Ptr jacobianVal = CsTripletMatrix::Ptr(new CsTripletMatrix(jacobianMatrixStruct->get_n_rows(),
                                                                                jacobianMatrixStruct->get_n_cols(),
                                                                                jacobianMatrixStruct->get_nnz(),
                                                                                jacobianMatrixStruct->get_matrix_ptr()->i,
                                                                                jacobianMatrixStruct->get_matrix_ptr()->p,
                                                                                values));

    result = staticOptProb->getNonLinConstraintDerivativesDecVar(jacobianVal);
    if(result == OptimizationProblem::FAILED){
      *mode = -1;
    }

    //get the row and col indices of CsTripletMatrix
    std::vector<int> rowIndex(jacobianVal->get_nnz());
    for(int i=0; i<jacobianVal->get_nnz(); i++)
      rowIndex[i] = jacobianVal->get_matrix_ptr()->i[i];

    std::vector<int> colIndex(jacobianVal->get_nnz());
    for(int i=0; i<jacobianVal->get_nnz(); i++)
      colIndex[i] = jacobianVal->get_matrix_ptr()->p[i];

    //cJac is a matrix dense format, so fill the values to the right place
    const int rowDim = jacobianVal->get_n_rows();
    for(int i=0; i<jacobianVal->get_nnz(); i++){
      jacobianConstraints[rowDim * colIndex[i] + rowIndex[i]] = jacobianVal->get_matrix_ptr()->x[i];
    }

    delete [] values;
  }
}

/**
* @briaf assemble matrix of linear constraints
* 
* @param[out] matrix containing the value of linear constraints
* @param[out] row Dimension of matrix
*/
void NPSOLWrapper::assembleLinConstraintsMatrix(std::vector<double> &linConstraintsMatrix, int &rowDim) const
{
  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();
  //set the right dimension of matrix
  const int colDim = optProbDim.numOptVars;
  int matrixDim = 0;
  if(optProbDim.numLinConstraints==0){
    rowDim = 1;//set dimension to 1, matrix is not referenced
    //resize matrix to right dimension
    matrixDim = rowDim*colDim;
    linConstraintsMatrix.resize(matrixDim);
  }
  else{
    rowDim = optProbDim.numLinConstraints;//row dimension of the matrix of linear constraints

    CsTripletMatrix::Ptr sparseLinConstraintsStruct = optProbDim.linJac;

    //resize matrix to right dimension
    matrixDim = rowDim*colDim;
    linConstraintsMatrix.resize(matrixDim);

    const int numLinNonZeroes = sparseLinConstraintsStruct->get_nnz();
    //get the row and col indices of cs struct
    std::vector<int> rowIndex(numLinNonZeroes);
    for(int i=0; i<numLinNonZeroes; i++)
      rowIndex[i] = sparseLinConstraintsStruct->get_matrix_ptr()->i[i];
    std::vector<int> colIndex(numLinNonZeroes);
    for(int i=0; i<numLinNonZeroes; i++)
      colIndex[i] = sparseLinConstraintsStruct->get_matrix_ptr()->p[i];

    //initialize the matrix linConstraintsMatrix as dense format
    const int tmpRowDim = sparseLinConstraintsStruct->get_n_rows();
    for(int i=0; i<numLinNonZeroes; i++){
      linConstraintsMatrix[tmpRowDim * colIndex[i] + rowIndex[i]] = sparseLinConstraintsStruct->get_matrix_ptr()->x[i];
    }
  }
}

/**
* @brief assemble upper and lower bounds of variables
*
* @param[out] lowerBound Array containing lower bounds for optimization variables,
*             linear Constraints and nonlinear Constraints
* @param[out] upperBound Array containing lower bounds for optimization variables,
*             linear Constraints and nonlinear Constraints
*/
void NPSOLWrapper::assembleBounds(std::vector<double> &lowerBound, std::vector<double> &upperBound) const
{
  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();

  const unsigned numLinConstraints = optProbDim.numLinConstraints;
  const unsigned numNonLinConstraints = optProbDim.numNonLinConstraints;
  const unsigned numOptVars = optProbDim.numOptVars;

  //initialization of the lower and upper bounds for optimization variables
  utils::Array<double> lowOptVarsBound(numOptVars);
  utils::Array<double> upOptVarsBound(numOptVars);
  m_optProb->getDecVarBounds(lowOptVarsBound, upOptVarsBound);
  for(unsigned i=0; i<numOptVars; i++){
    lowerBound[i] = lowOptVarsBound[i];
    upperBound[i] = upOptVarsBound[i];
  }

  //initialization of the lower and upper bounds for linear constraints
  utils::Array<double> lowLinCon(numLinConstraints);
  utils::Array<double> upLinCon(numLinConstraints);
  m_optProb->getLinConstraintBounds(lowLinCon, upLinCon);
  unsigned offset = numOptVars;
  for(unsigned i=0; i<numLinConstraints; i++){
    lowerBound[i+offset] = lowLinCon[i];
    upperBound[i+offset] = upLinCon[i];
  }

  //initialization of the lower and upper bounds for nonlinear constraints
  utils::Array<double> lowNonLinCon(numNonLinConstraints);
  utils::Array<double> upNonLinCon(numNonLinConstraints);
  m_optProb->getNonLinConstraintBounds(lowNonLinCon, upNonLinCon);
  offset = offset + numLinConstraints;
  for(unsigned i=0; i<numNonLinConstraints; i++){
    lowerBound[i+offset] = lowNonLinCon[i];
    upperBound[i+offset] = upNonLinCon[i];
  }
}

/**
* @brief assemble Jacobian matrix for NPSOL
*
* @param[out] jacVal Jacobian values of Matrix
* @param[out] row Dimension of Jacobian Matrix
*/
void NPSOLWrapper::assembleJacobian(std::vector<double> &jacVal, int &rowDim) const
{
  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();
  CsTripletMatrix::Ptr nonLinJac = optProbDim.nonLinJacDecVar;
  assert(nonLinJac->get_nnz()>-1);//expecting matrix in coo-format

  //set the right dimension of the Jacobian matrix
  const int colDim = optProbDim.numOptVars;
  int matrixDim = 0;
  if(optProbDim.numNonLinConstraints == 0){
    rowDim=1;
    matrixDim = 1;//set dimension to 1, matrix is not referenced
    jacVal.resize(matrixDim);
  }
  else{
    rowDim = optProbDim.numNonLinConstraints;//row dimension of the matrix of nonlinear constraints
    matrixDim = rowDim * colDim;

    //get the jacobian values
    double *values = new double[nonLinJac->get_nnz()];
    CsTripletMatrix::Ptr jacobianVal(new CsTripletMatrix(nonLinJac->get_n_rows(),
                                                         nonLinJac->get_n_cols(),
                                                         nonLinJac->get_nnz(),
                                                         nonLinJac->get_matrix_ptr()->i,
                                                         nonLinJac->get_matrix_ptr()->p,
                                                         values));

    m_optProb->getNonLinConstraintDerivativesDecVar(jacobianVal);

    //get the row and col indices of cs struct
    std::vector<int> rowIndex(nonLinJac->get_nnz());
    for(int i=0; i<nonLinJac->get_nnz(); i++)
      rowIndex[i] = nonLinJac->get_matrix_ptr()->i[i];
    std::vector<int> colIndex(nonLinJac->get_nnz());
    for(int i=0; i<nonLinJac->get_nnz(); i++)
      colIndex[i] = nonLinJac->get_matrix_ptr()->p[i];

    //resize initialize the matrix linConstraintsMatrix as dense format
    jacVal.resize(matrixDim);
    for(int i=0; i<nonLinJac->get_nnz(); i++){
      jacVal[rowDim * colIndex[i] + rowIndex[i]] = jacobianVal->get_matrix_ptr()->x[i];
    }
    delete [] values;
  }
}

/*
 *@brief allocate workspace required by NPSOL
 *
 *@param[out] lenInt length of integer workspace required by NPSOL
 *@param[out] lenDouble length of real workspace required by NPSOL
 *@param[in] numOpt number of optimization variables
 *@param[in] numLinCon number of linear constraints
 *@param[in] numNonLinCon number of nonlinear constraints
 *@note the equations for the right length's are described in the User's Guide for NPSOL
 */
void NPSOLWrapper::allocateNPSOLWorkspace(int &lenInt, int &lenDouble, const int numOpt, const int numLinCon, const int numNonLinCon)
{
  //the equations for the right length's are described in the User's Guide for NPSOL
  lenInt = 3*numOpt + numLinCon + 2*numNonLinCon;//3 n + nclin+ 2ncnln.

  if(numLinCon==0 && numNonLinCon==0){
    lenDouble = 20*numOpt;// 20*n
  }
  else if(numNonLinCon==0){
    lenDouble = 2*numOpt + 20*numOpt + 11*numLinCon;// 2*n2 +20n+11nclin
  }
  else{
    lenDouble = 2*numOpt*numOpt+numOpt*numLinCon+2*numOpt*numNonLinCon+20*numOpt+11*numLinCon+21*numNonLinCon;
    // 2*n2 + n*nclin+ 2n*ncnln+ 20n+ 11nclin+21 ncnln
  }
  
  if(numLinCon == 0){
    // in NPSOL there will be access to workspace index lenDouble - numLinCon +1
    // so to avoid errors for bound checks, increase the workspace by 1
    lenDouble++;
  }

  m_intWorkspace.resize(lenInt, 0);
  m_realWorkspace.resize(lenDouble, 0.0);
}
