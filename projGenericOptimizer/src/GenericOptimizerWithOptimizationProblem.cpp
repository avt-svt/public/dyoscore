/**
* @file GenericOptimizer.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* This file contains the member definitions of class                   \n
* GenericOptimizerWithOptimizationProblem.                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 13.09.2012
*/

#include "GenericOptimizerWithOptimizationProblem.hpp"
#include "boost/filesystem.hpp"

std::vector<DyosOutput::StageOutput> GenericOptimizerWithOptimizationProblem::getOutput()
{
  return m_optProb->getOutput();
}

/**
* @brief calculate the Lagrange multipliers of the decision variables
*
* Originally this function has been implemented, because the result of SNOPT rc vectors seemed to be inaccurate.
* However this function did supply the same result.
* @param multiplier array receiving the result
* @param optimalityThreshold precision with which eh problem has been optimized
*/
void GenericOptimizerWithOptimizationProblem::calculateDecVarLagrangeMultiplier
                                                  (utils::Array<double> &multiplier,
                                                   const double optimalityThreshold) const
{
  unsigned numDecVars = m_optProb->getOptProbDim().numOptVars;
  unsigned numNonLinConstraints = m_optProb->getOptProbDim().numNonLinConstraints;
  unsigned numLinConstraints = m_optProb->getOptProbDim().numLinConstraints;
  
  assert(multiplier.getSize() == numDecVars);
  
  utils::Array<double> lowerBounds(numDecVars), upperBounds(numDecVars), values(numDecVars);
  utils::Array<double> objectiveDers(numDecVars);
  CsTripletMatrix::Ptr linConstraints = m_optProb->getOptProbDim().linJac;
  CsTripletMatrix::Ptr constraintDers = m_optProb->getOptProbDim().nonLinJacDecVar;
  utils::Array<double> linMultipliers(numLinConstraints), nonLinMultipliers(numNonLinConstraints);
  
  m_optProb->getDecVarBounds(lowerBounds, upperBounds);
  m_optProb->getDecVarValues(values);
  m_optProb->getNonLinConstraintDerivativesDecVar(constraintDers);
  m_optProb->getNonLinObjDerivative(objectiveDers);
  m_optProb->getLagrangeMultipliers(nonLinMultipliers, linMultipliers);
  for(unsigned i=0; i<numDecVars; i++){
    if((values[i]+optimalityThreshold < upperBounds[i]) &&
       (values[i] > lowerBounds[i] + optimalityThreshold)){
      multiplier[i] = 0.0;
    }
    else{
      multiplier[i] = objectiveDers[i];
      for(unsigned j=0; j<numNonLinConstraints; j++){
        multiplier[i] -= nonLinMultipliers[j]*constraintDers->getValue(j,i);
      }
      for(unsigned j=0; j<numLinConstraints; j++){
        multiplier[i] -= linMultipliers[j]*linConstraints->getValue(j,i);
      }
    }
  }
}

void GenericOptimizerWithOptimizationProblem::getOptimizerOutput(DyosOutput::OptimizerOutput &oo) const
{
  //oo.lagrangeMultiplier
  const unsigned numNonLinConstraints = m_optProb->getOptProbDim().numNonLinConstraints;
  const unsigned numLinConstraints = m_optProb->getOptProbDim().numLinConstraints;
  utils::Array<double> nonLinLagrangeMultiplier(numNonLinConstraints);
  utils::Array<double> linLagrangeMultiplier(numLinConstraints);
  m_optProb->getLagrangeMultipliers(nonLinLagrangeMultiplier,
                                    linLagrangeMultiplier);

  oo.lagrangeMultiplier.assign(nonLinLagrangeMultiplier.getData(),
                               nonLinLagrangeMultiplier.getData() + numNonLinConstraints);
  oo.lagrangeMultiplier.insert(oo.lagrangeMultiplier.end(),
                               linLagrangeMultiplier.getData(),
                               linLagrangeMultiplier.getData() + numLinConstraints);

  const unsigned numDecVarValues = m_optProb->getOptProbDim().numOptVars;
  utils::Array<double> decVarValues(numDecVarValues);
  m_optProb->getDecVarValues(decVarValues);
  oo.optDecVarValues.assign(decVarValues.getData(), decVarValues.getData() + numDecVarValues);
  m_optProb->getNonLinObjValue(oo.optObjFunVal);

  //insert 2nd order adjoints into optimizer output
  {
    std::vector<double> adjoints1stOrderDecVar, adjoints1stOrderConstParam;
    std::vector<std::vector<double> > adjoints2ndOrderDecVar, adjoints2ndOrderConstParam;
    m_optProb->getAdjointsDecVar(adjoints1stOrderDecVar, adjoints2ndOrderDecVar);
    m_optProb->getAdjointsConstParam(adjoints1stOrderConstParam, adjoints2ndOrderConstParam);

    const unsigned numRows = adjoints2ndOrderConstParam.size();
    assert(numRows == adjoints2ndOrderDecVar.size());

    oo.secondOrderOutput.compositeAdjoints.resize(numRows);
    for(unsigned i=0; i<numRows; i++){
      std::vector<double> compAdjoints(adjoints2ndOrderDecVar[i].begin(),
                                       adjoints2ndOrderDecVar[i].end());
      compAdjoints.insert(compAdjoints.end(),
                          adjoints2ndOrderConstParam[i].begin(),
                          adjoints2ndOrderConstParam[i].end());
      oo.secondOrderOutput.compositeAdjoints[i] = compAdjoints;
    }
  }

  //insert 2nd order lagrange derivatives into optimizer output
  {
    std::vector<std::vector<double> > hessianDecVar, hessianConstParam;
    m_optProb->getNonLinConstraintHessianDecVar(hessianDecVar);
    m_optProb->getNonLinConstraintHessianConstParam(hessianConstParam);

    const unsigned numRows = hessianDecVar.size();
    assert(hessianConstParam.size() == numRows);
    oo.secondOrderOutput.hessian.resize(numRows);
    oo.secondOrderOutput.constParamHessian.resize(numRows);
    for(unsigned i=0; i<numRows; i++){
      oo.secondOrderOutput.hessian[i].assign(hessianDecVar[i].begin(), hessianDecVar[i].end());
      oo.secondOrderOutput.constParamHessian[i].assign(hessianConstParam[i].begin(), hessianConstParam[i].end());
    }
  }
}
/**
* @brief writing options from given file into map
*
* @param filename, map
*/
std::vector<std::string> GenericOptimizerWithOptimizationProblem::getOptimizerOptions(const char* filename)const
{
  std::string line;
  std::vector<std::string> vec;
  std::vector<std::string> tempvec;
  std::vector<std::string> optionsVec;
  std::string optionstring;
  std::ifstream infile;
  infile.open(filename);
  while(!infile.eof()){//read whole file into vector of strings
    getline(infile, line);
    boost::trim(line);
    boost::algorithm::to_lower(line);
    vec.push_back(line);
  }
  std::vector<std::string>::iterator it = remove_if(vec.begin(), vec.end(),std::mem_fun_ref(&std::string::empty));
  vec.erase(it, vec.end());//remove empty lines from vector
  if(vec.size()<2)
    return optionsVec;
  for(unsigned i=1; i<(vec.size()-1); i++){//ignore first and last line
      tempvec.clear();
      optionstring.clear();
        std::istringstream iss(vec[i]);
        boost::algorithm::to_lower(vec[i]);
        while(iss){//split string into words
          std::string sub;
          iss >> sub;
          if(sub.find("*")!=std::string::npos)//ignore comments
            break;
          tempvec.push_back(sub);
        }
        if(tempvec.size()>0){
          for(unsigned j=0; j<tempvec.size(); j++){
            optionstring.append(tempvec[j]);
            optionstring.append(" ");
          }
          boost::trim(optionstring);//erase last space
          optionsVec.push_back(optionstring);
        }
  }
  infile.close();
  return optionsVec;
}
/**
* @brief matching two std::maps that contain optimizer options
*
* @param two maps
*/
void GenericOptimizerWithOptimizationProblem::matchOptimizerOptions(std::map<std::string, std::string> &matchedOptimizerOptions,
                                         const std::map<std::string, std::string> &optimizerOptions)const
{
  std::map<std::string,std::string>::const_iterator iter;
  std::string tempString;
  for(iter=optimizerOptions.begin(); iter!=optimizerOptions.end(); iter++){
    matchedOptimizerOptions[(*iter).first]=(*iter).second;//change existing options or create new
  }
  //search for options where key differs
  if(matchedOptimizerOptions.find("maximize")!=matchedOptimizerOptions.end())
    matchedOptimizerOptions.erase("minimize");//if user chose option maximize, minimize is deleted from map
}
/**
* @brief write the final optionsFile
*
* @param filename of optionsFile, map 
*/
void GenericOptimizerWithOptimizationProblem::writeOptimizerOptions(const char* filename,
                                         const std::map<std::string, std::string> &optimizerOptions)const
{
  // first make sure that file can be created.
  // If the directory does not exist, the file cannot be opened for writing
  //boost::filesystem::path checkfile(filename);
  //boost::filesystem::path parent = checkfile.parent_path();
  //if(!boost::filesystem::exists(parent)){
  //  boost::filesystem::create_directory(parent);
  //}
  
  std::ofstream myfile;
  myfile.open(filename, std::ios_base::trunc);
  myfile << "Begin\n";
  myfile << "* This file includes the optimizer options set in the '.config'-file\n";
  myfile << "* and options that might have been added in UserInput.\n";
  myfile << "* If there was no '.config'-file, standard options were added.\n";

  std::map<std::string,std::string>::const_iterator iter;
  for(iter=optimizerOptions.begin(); iter!=optimizerOptions.end(); iter++){
    myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
  }
  myfile << "End\n";
  myfile.close();
}
