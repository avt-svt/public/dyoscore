#include <algorithm>

#include "MultiScenarioOptProblem.hpp"

int MultiScenarioSet::get_full_index(const int scenario,const int idx) const
{
  if (m_scenario_to_shared_maps[scenario][idx] != -1) {
    return n_non_shared + m_scenario_to_shared_maps[scenario][idx];
  }
  return m_nonshared_maps[scenario][idx];
}

bool MultiScenarioSet::get_reduced_index(const int full_idx, int& scenario, int& idx) const
{
  if (full_idx >= n_non_shared) {
    idx = m_shared_to_scenario_maps[scenario][full_idx-n_non_shared];
    return false;
  }
  const std::pair<int, int> p =  m_full_map[full_idx];
  scenario = p.first;
  idx = p.second;
  return true;
}

int MultiScenarioSet::get_full_lincon_index(const int scenario, const int idx) const
{
  return m_lincon_scenario_to_full[scenario][idx];
}

void MultiScenarioSet::get_reduced_lincon_index(const int full_idx, int& scenario, int& idx) const
{
  const std::pair<int,int> p = m_lincon_full_to_scenario[full_idx];
  scenario = p.first;
  idx = p.second;
}

int MultiScenarioSet::get_full_nonlincon_index(const int scenario, const int idx) const
{
  return m_nonlincon_scenario_to_full[scenario][idx];
}

void MultiScenarioSet::get_reduced_nonlincon_index(const int full_idx, int& scenario, int& idx) const
{
  const std::pair<int,int> p = m_nonlincon_full_to_scenario[full_idx];
  scenario = p.first;
  idx = p.second;
}

int MultiScenarioSet::get_full_lin_nnz(const int scenario, const int nnz_idx) const
{
  return m_linjac_scenario_to_full[scenario][nnz_idx];
}

int MultiScenarioSet::get_full_nonlin_nnz(const int scenario, const int nnz_idx) const
{
  return m_nonlinjac_scenario_to_full[scenario][nnz_idx];
}

void MultiScenarioSet::addScenario(const OptimizationProblem::Ptr& opt_problem, const std::vector<int>& shared)
{
  const int new_scenario_index = m_scenarios.size();
  m_scenarios.push_back(opt_problem);

  /* Variable stuff */
  const OptimizerProblemDimensions& dim = opt_problem->getOptProbDim();
  const int n_vars = dim.numOptVars;

  std::vector<int> new_scenario_to_shared_maps(n_vars, -1);
  for (unsigned k=0; k<shared.size(); ++k) {
    new_scenario_to_shared_maps[shared[k]] = k;
  }
  m_scenario_to_shared_maps.push_back(new_scenario_to_shared_maps);

  m_shared_to_scenario_maps.push_back(shared);

  assert(n_non_shared==(int)m_full_map.size());
  std::vector<int> new_nonshared_map(n_vars, -1);
  m_full_map.reserve(n_non_shared + n_vars - shared.size());
  int var_counter = 0;
  for (int k=0; k<n_vars; ++k) {
    if (new_scenario_to_shared_maps[k]==-1) {
      new_nonshared_map[k] = n_non_shared + var_counter;
      std::pair<int,int> p;
      p.first = new_scenario_index;
      p.second = var_counter++;
      m_full_map.push_back(p);
    }
  }
  assert(var_counter +(int)shared.size()==n_vars);
  n_non_shared += var_counter;
  m_nonshared_maps.push_back(new_nonshared_map);

  /* Nonlin Constraint stuff */
  m_nonlincon_full_to_scenario.reserve(m_nonlincon_full_to_scenario.size() + dim.numNonLinConstraints);
  std::vector<int> new_scenario_nonlin(dim.numNonLinConstraints);
  for (unsigned k=0; k<dim.numNonLinConstraints; ++k) {
    new_scenario_nonlin[k] = m_nonlincon_full_to_scenario.size();
    std::pair<int,int> p;
    p.first = new_scenario_index;
    p.second = k;
    m_nonlincon_full_to_scenario.push_back(p);
  }
  m_nonlincon_scenario_to_full.push_back(new_scenario_nonlin);

  // non-zeros and structure
  std::vector<int> new_nonlinjac(dim.linJac->get_nnz());
  int curr_nonlin_nz = 0;
  for (std::vector<std::vector<int> >::iterator it=m_nonlinjac_scenario_to_full.begin(), end=m_nonlinjac_scenario_to_full.end(); it!=end; ++it) {
    curr_nonlin_nz += it->size();
  }
  for (int k=0; k<dim.linJac->get_nnz(); ++k) {
    new_nonlinjac[k] = curr_nonlin_nz++;
  }
  m_nonlinjac_scenario_to_full.push_back(new_nonlinjac);

  /* Lin Constraint stuff */
  m_lincon_full_to_scenario.reserve(m_lincon_full_to_scenario.size() + dim.numLinConstraints);
  std::vector<int> new_scenario_lin(dim.numLinConstraints);
  for (unsigned k=0; k<dim.numLinConstraints; ++k) {
    new_scenario_lin[k] = m_lincon_full_to_scenario.size();
    std::pair<int,int> p;
    p.first = new_scenario_index;
    p.second = k;
    m_lincon_full_to_scenario.push_back(p);
  }
  m_lincon_scenario_to_full.push_back(new_scenario_lin);

  // non-zeros and structure
  std::vector<int> new_linjac(dim.linJac->get_nnz());
  int curr_lin_nz = 0;
  for (std::vector<std::vector<int> >::iterator it=m_linjac_scenario_to_full.begin(), end=m_linjac_scenario_to_full.end(); it!=end; ++it) {
    curr_lin_nz += it->size();
  }
  for (int k=0; k<dim.linJac->get_nnz(); ++k) {
    new_linjac[k] = curr_lin_nz++;
  }
  m_linjac_scenario_to_full.push_back(new_linjac);
}

int MultiScenarioSet::getLinNonZeroes() const
{
  int nnz = 0;
  for (ProblemVector::const_iterator it=m_scenarios.begin(), end=m_scenarios.end(); it!=end; ++it) {
    const OptimizerProblemDimensions& optprob = (*it)->getOptProbDim();
    //assert(CS_TRIPLET(optprob.linJac));
    nnz += optprob.linJac->get_nnz();
  }
  return nnz;
}
int MultiScenarioSet::getNonLinNonzeroes() const
{
  int nnz = 0;
  for (ProblemVector::const_iterator it=m_scenarios.begin(), end=m_scenarios.end(); it!=end; ++it) {
    const OptimizerProblemDimensions& optprob = (*it)->getOptProbDim();
    //assert(CS_TRIPLET(optprob.nonLinJacDecVar));
    nnz += optprob.nonLinJacDecVar->get_nnz();
  }
  return nnz;
}

/** n_full: total number of variables
 *  shared: vector with indices representing the shared variables
 *
 *  returns a vector whose entries represent the indices in the non-shared variables.
 *  In this vector, the shared variables are marked with -1.
 */
std::vector<int> get_variable_mapping(const int n_full, const std::vector<int>& shared)
{
  std::vector<int> retval(n_full);
  std::vector<int>::const_iterator sh_it=shared.begin();
  int idx = 0;
  for (std::vector<int>::iterator it=retval.begin(), end=retval.end(); it!=end; ++it) {
    if (*sh_it) {
      *it++ = -1;
      continue;
    }
    *it = idx++;
  }
  assert((unsigned)idx==n_full - shared.size());
  return retval;
}

void MultiScenarioOptProblem::initialize()
{
  m_opt_dim.numOptVars = m_set->getNumOptVars();
  m_opt_dim.numLinConstraints = m_set->getNumLinConstraints();
  m_opt_dim.numNonLinConstraints = m_set->getNumNonLinConstraints();

  /* Allocate matrices */
  const int lin_nnz = m_set->getLinNonZeroes();
  const int nonlin_nnz = m_set->getNonLinNonzeroes();
  const int *indicesSpace_lin = new int [lin_nnz];
  const int *indicesSpace_nonlin = new int [nonlin_nnz];
  const double *valuesSpace_lin = new double [lin_nnz];
  const double *valuesSpace_nonlin = new double [nonlin_nnz];
  CsTripletMatrix::Ptr linJac = CsTripletMatrix::Ptr(new CsTripletMatrix(m_opt_dim.numLinConstraints,
                                                                         m_opt_dim.numOptVars,
                                                                         lin_nnz,
                                                                         indicesSpace_lin,
                                                                         indicesSpace_lin,
                                                                         valuesSpace_lin));
  CsTripletMatrix::Ptr nonLinJac(new CsTripletMatrix(m_opt_dim.numNonLinConstraints,
                                                      m_opt_dim.numOptVars,
                                                      nonlin_nnz,
                                                      indicesSpace_nonlin,
                                                      indicesSpace_nonlin,
                                                      valuesSpace_nonlin));
  delete []indicesSpace_lin;
  delete []indicesSpace_nonlin;
  delete []valuesSpace_lin;
  delete []valuesSpace_nonlin;
  /* fill up matrix entries */
  int lin_curr_nz = 0;
  int nonlin_curr_nz = 0;
  int* lin_fullrow = linJac->get_matrix_ptr()->i;
  int* lin_fullcol = linJac->get_matrix_ptr()->p;
  double* lin_fullval = linJac->get_matrix_ptr()->x;
  int* nonlin_fullrow = nonLinJac->get_matrix_ptr()->i;
  int* nonlin_fullcol = nonLinJac->get_matrix_ptr()->p;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    OptimizerProblemDimensions opt_prob_i = (*m_set)[scen_idx]->getOptProbDim();
    {// linear stuff
      CsTripletMatrix::Ptr m = opt_prob_i.linJac;
      for (int k=0; k<m->get_nnz(); ++k) {
        lin_curr_nz = m_set->get_full_lin_nnz(scen_idx, k);
        assert(lin_curr_nz>=0 && lin_curr_nz<lin_nnz);
        lin_fullrow[lin_curr_nz] = m_set->get_full_lincon_index(scen_idx, m->get_matrix_ptr()->i[k]);
        lin_fullcol[lin_curr_nz] = m_set->get_full_index(scen_idx, m->get_matrix_ptr()->p[k]);
        lin_fullval[lin_curr_nz] = m->get_matrix_ptr()->x[k];
      }
    }
    {// nonlinear stuff
      CsTripletMatrix::Ptr m = opt_prob_i.nonLinJacDecVar;
      for (int k=0; k<m->get_nnz(); ++k) {
        nonlin_fullrow[nonlin_curr_nz] = m_set->get_full_nonlincon_index(scen_idx, m->get_matrix_ptr()->i[k]);
        nonlin_fullcol[nonlin_curr_nz] = m_set->get_full_index(scen_idx, m->get_matrix_ptr()->p[k]);
        nonlin_curr_nz++;
      }
    }
  }
  m_opt_dim.linJac = linJac;
  m_opt_dim.nonLinJacDecVar = nonLinJac;
}

const OptimizerProblemDimensions &MultiScenarioOptProblem::getOptProbDim() const
{
  return m_opt_dim;
}

void MultiScenarioOptProblem::getDecVarBounds(utils::Array<double> &lowerBounds,
                                              utils::Array<double> &upperBounds) const
{
  // 1. read out bounds for all non-shared variables
  // 2. For all shared variables, use the tightest bound; warn if bounds are not identical
  assert(lowerBounds.getSize()==(unsigned)m_set->getNumOptVars());
  assert(upperBounds.getSize()==(unsigned)m_set->getNumOptVars());

  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const OptimizerProblemDimensions& opt_prob_dim = (*m_set)[scen_idx]->getOptProbDim();
    const int n_curr_var = opt_prob_dim.numOptVars;
    // usage of wrapping arrays to avoid unnecessary allocation seems not possible here
    utils::Array<double> curr_lower(n_curr_var);
    utils::Array<double> curr_upper(n_curr_var);
    (*m_set)[scen_idx]->getDecVarBounds(curr_lower, curr_upper);
    for (int k=0; k<n_curr_var; ++k) {
      const int full_idx = m_set->get_full_index(scen_idx, k);
      lowerBounds[full_idx] = curr_lower[k];
      upperBounds[full_idx] = curr_upper[k];
    }
  }
}

void MultiScenarioOptProblem::getDecVarValues(utils::Array<double> &values) const
{
  assert(values.getSize()==(unsigned)m_set->getNumOptVars());

  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const OptimizerProblemDimensions& opt_prob_dim = (*m_set)[scen_idx]->getOptProbDim();
    const int n_curr_var = opt_prob_dim.numOptVars;
    utils::Array<double> curr_var(n_curr_var);
    (*m_set)[scen_idx]->getDecVarValues(curr_var);
    for (int k=0; k<n_curr_var; ++k) {
      const int full_idx = m_set->get_full_index(scen_idx, k);
      values[full_idx] = curr_var[k];
    }
  }
}

void MultiScenarioOptProblem::setDecVarValues(const utils::Array<const double> &decVars)
{
  assert(decVars.getSize()==(unsigned)m_set->getNumOptVars());

  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const OptimizerProblemDimensions& opt_prob_dim = (*m_set)[scen_idx]->getOptProbDim();
    const int n_curr_var = opt_prob_dim.numOptVars;
    utils::Array<double> curr_var(n_curr_var);
    for (int k=0; k<n_curr_var; ++k) {
      const int full_idx = m_set->get_full_index(scen_idx, k);
      curr_var[k] = decVars[full_idx];
    }
    
    utils::WrappingArray<const double> constValues(n_curr_var, decVars.getData());
    (*m_set)[scen_idx]->setDecVarValues(constValues);
  }
}

void MultiScenarioOptProblem::getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                                        utils::Array<double> &upperBounds) const
{
  const int n_nonlincon = m_set->getNumNonLinConstraints();
  assert((int)lowerBounds.getSize()==n_nonlincon);
  assert((int)upperBounds.getSize()==n_nonlincon);

  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const OptimizerProblemDimensions& opt_prob_dim = (*m_set)[scen_idx]->getOptProbDim();
    const int n_curr_nonlincon = opt_prob_dim.numNonLinConstraints;
    utils::Array<double> lb_con(n_curr_nonlincon);
    utils::Array<double> ub_con(n_curr_nonlincon);
    (*m_set)[scen_idx]->getNonLinConstraintBounds(lb_con, ub_con);
    for (int k=0; k<n_curr_nonlincon; ++k) {
      int full_idx = m_set->get_full_index(scen_idx, k);
      lowerBounds[full_idx] = lb_con[k];
      upperBounds[full_idx] = ub_con[k];
    }
  }
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getNonLinConstraintValues
                                                        (utils::Array<double> &values) const
{
  const int n_nonlincon = m_set->getNumNonLinConstraints();
  assert((int)values.getSize()==n_nonlincon);

  OptimizationProblem::ResultFlag result;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const int n_curr_nonlin = (*m_set)[scen_idx]->getOptProbDim().numNonLinConstraints;
    utils::Array<double> curr_values(n_curr_nonlin);
    result = (*m_set)[scen_idx]->getNonLinConstraintValues(curr_values);
    if(result == OptimizationProblem::FAILED){
      break;
    }
    for (int k=0; k<n_curr_nonlin; ++k) {
      const int full_idx = m_set->get_full_index(scen_idx, k);
      values[full_idx] = curr_values[k];
    }
  }
  return result;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getLinConstraintValues
                                                         (utils::Array<double> &values) const
{
  const int n_lincon = m_set->getNumLinConstraints();
  assert((int)values.getSize()==n_lincon);

  OptimizationProblem::ResultFlag result;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const int n_curr_lincon = (*m_set)[scen_idx]->getOptProbDim().numLinConstraints;
    utils::Array<double> curr_values(n_curr_lincon);
    result = (*m_set)[scen_idx]->getLinConstraintValues(curr_values);
    if(result == OptimizationProblem::FAILED){
      break;
    }
    for (int k=0; k<n_curr_lincon; ++k) {
      const int full_idx = m_set->get_full_lincon_index(scen_idx, k);
      values[full_idx] = curr_values[k];
    }
  }
  return result;
}

void MultiScenarioOptProblem::getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                                     utils::Array<double> &upperBounds) const
{
  const int n_lincon = m_set->getNumLinConstraints();
  assert((int)lowerBounds.getSize()==n_lincon);
  assert((int)upperBounds.getSize()==n_lincon);

  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const OptimizerProblemDimensions& opt_prob_dim = (*m_set)[scen_idx]->getOptProbDim();
    const int n_curr_lincon = opt_prob_dim.numLinConstraints;
    utils::Array<double> lb_con(n_curr_lincon);
    utils::Array<double> ub_con(n_curr_lincon);
    (*m_set)[scen_idx]->getLinConstraintBounds(lb_con, ub_con);
    for (int k=0; k<n_curr_lincon; ++k) {
      const int full_idx = m_set->get_full_lincon_index(scen_idx, k);
      lowerBounds[full_idx] = lb_con[k];
      upperBounds[full_idx] = ub_con[k];
    }
  }
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getNonLinConstraintDerivativesDecVar
                                                  (CsTripletMatrix::Ptr derivativeMatrix) const
{
  CsTripletMatrix::Ptr nonlin_jac = m_opt_dim.nonLinJacDecVar;
  for (int k=0; k<nonlin_jac->get_nnz(); ++k) {
    assert(nonlin_jac->get_matrix_ptr()->i[k]==derivativeMatrix->get_matrix_ptr()->i[k]); // check that structure is the same!
    assert(nonlin_jac->get_matrix_ptr()->p[k]==derivativeMatrix->get_matrix_ptr()->p[k]);
  }

  OptimizationProblem::ResultFlag result;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const OptimizerProblemDimensions& opt_prob_dim = (*m_set)[scen_idx]->getOptProbDim();
    CsTripletMatrix::Ptr curr_m = opt_prob_dim.nonLinJacDecVar;
    result = (*m_set)[scen_idx]->getNonLinConstraintDerivativesDecVar(curr_m);
    if(result == OptimizationProblem::FAILED){
      break;
    }
    for (int k=0; k<curr_m->get_nnz(); ++k) {
      derivativeMatrix->get_matrix_ptr()->x[m_set->get_full_nonlin_nnz(scen_idx,k)] = curr_m->get_matrix_ptr()->x[k];
    }
  }
  return result;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getNonLinConstraintDerivativesConstParam
                                                    (CsTripletMatrix::Ptr derivativeMatrix) const
{
  CsTripletMatrix::Ptr nonlin_jac = m_opt_dim.nonLinJacConstPar;
  for (int k=0; k<nonlin_jac->get_nnz(); ++k) {
    assert(nonlin_jac->get_matrix_ptr()->i[k]==derivativeMatrix->get_matrix_ptr()->i[k]); // check that structure is the same!
    assert(nonlin_jac->get_matrix_ptr()->p[k]==derivativeMatrix->get_matrix_ptr()->p[k]);
  }
  OptimizationProblem::ResultFlag result;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const OptimizerProblemDimensions& opt_prob_dim = (*m_set)[scen_idx]->getOptProbDim();
    CsTripletMatrix::Ptr curr_m = opt_prob_dim.nonLinJacConstPar;
    result = (*m_set)[scen_idx]->getNonLinConstraintDerivativesDecVar(curr_m);
    if(result == OptimizationProblem::FAILED){
      break;
    }
    for (int k=0; k<curr_m->get_nnz(); ++k) {
      derivativeMatrix->get_matrix_ptr()->x[m_set->get_full_nonlin_nnz(scen_idx,k)] = curr_m->get_matrix_ptr()->x[k];
    }
  }
  return result;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getNonLinObjValue(double &objValue) const
{
  // what to do here? Weighted sum? max? new variable?
  // We'll do a weighted sum.
  objValue = 0.0;
  OptimizationProblem::ResultFlag result;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    double summand = 0;
    result = (*m_set)[scen_idx]->getNonLinObjValue(summand);
    if(result == OptimizationProblem::FAILED){
      break;
    }
    objValue += summand;
  }
  objValue /= m_set->size();
  return result;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getNonLinObjDerivative
                                                         (utils::Array<double> &values) const
{
  const unsigned n_opt_vars =  m_set->getNumOptVars();
  assert(values.getSize()== n_opt_vars);
  // again, weighted sum.
  const unsigned n_scen = m_set->size();
  for(unsigned i=0; i<n_opt_vars; i++){
    values[i] = 0.0;
  }
  OptimizationProblem::ResultFlag result;
  for (unsigned scen_idx=0; scen_idx<n_scen; ++scen_idx) {
    const int n_curr_opt_vars = (*m_set)[scen_idx]->getOptProbDim().numOptVars;
    utils::Array<double> curr_values(n_curr_opt_vars);
    result = (*m_set)[scen_idx]->getNonLinObjDerivative(curr_values);
    if(result == OptimizationProblem::FAILED){
      break;
    }
    for (int k=0; k<n_curr_opt_vars; ++k) {
      values[m_set->get_full_index(scen_idx, k)] += curr_values[k]/n_scen;
    }
  }
  return result;
}

void MultiScenarioOptProblem::setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                                     const utils::Array<double> &linMultipliers)
{
  std::vector<utils::Array<double> > all_nonlin;
  std::vector<utils::Array<double> > all_lin;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    all_nonlin.push_back(utils::Array<double>((*m_set)[scen_idx]->getOptProbDim().numNonLinConstraints));
    all_lin.push_back(utils::Array<double>((*m_set)[scen_idx]->getOptProbDim().numLinConstraints));
  }

  int scenario, idx;
  for (unsigned k=0; k<nonLinMultipliers.getSize(); ++k) {
    m_set->get_reduced_nonlincon_index(k, scenario, idx);
    all_nonlin[scenario][idx] = nonLinMultipliers[k];
  }
  for (unsigned k=0; k<linMultipliers.getSize(); ++k) {
    m_set->get_reduced_lincon_index(k, scenario, idx);
    all_lin[scenario][idx] = linMultipliers[k];
  }

  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    (*m_set)[scen_idx]->setLagrangeMultipliers(all_nonlin[scen_idx], all_lin[scen_idx]);
  }
}

void MultiScenarioOptProblem::getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                                     utils::Array<double> &linMultipliers) const
{
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    const int n_curr_nonlin = (*m_set)[scen_idx]->getOptProbDim().numNonLinConstraints;
    const int n_curr_lin = (*m_set)[scen_idx]->getOptProbDim().numLinConstraints;
    utils::Array<double> curr_nonlin_multipliers(n_curr_nonlin);
    utils::Array<double> curr_lin_multipliers(n_curr_lin);
    (*m_set)[scen_idx]->getLagrangeMultipliers(curr_nonlin_multipliers, curr_lin_multipliers);
    for (int k=0; k<n_curr_nonlin; ++k) {
      nonLinMultipliers[m_set->get_full_nonlincon_index(scen_idx, k)] = curr_nonlin_multipliers[k];
    }
    for (int k=0; k<n_curr_lin; ++k) {
      linMultipliers[m_set->get_full_lincon_index(scen_idx, k)] = curr_lin_multipliers[k];
    }
  }
}

void MultiScenarioOptProblem::setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier)
{
  std::vector<utils::Array<double> > all_lagrangeMult;
  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    all_lagrangeMult.push_back(utils::Array<double>((*m_set)[scen_idx]->getOptProbDim().numOptVars));
  }

  int scenario, idx;
  for (unsigned k=0; k<lagrangeMultiplier.getSize(); ++k) {
    m_set->get_reduced_nonlincon_index(k, scenario, idx);
    all_lagrangeMult[scenario][idx] = lagrangeMultiplier[k];
  }

  for (unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx) {
    (*m_set)[scen_idx]->setDecVarLagrangeMultipliers(all_lagrangeMult[scen_idx]);
  }
}

void MultiScenarioOptProblem::setObjectiveLagrangeMultiplier(const double lagrangeMultiplier)
{
  for(unsigned scen_idx=0; scen_idx<m_set->size(); ++scen_idx){
    (*m_set)[scen_idx]->setObjectiveLagrangeMultiplier(lagrangeMultiplier);
  }
}

MultiScenarioOptProblem::MultiScenarioOptProblem(const MultiScenarioSet::Ptr& set)
  : m_set(set)
{
  initialize();
}


MultiScenarioOptProblem::~MultiScenarioOptProblem()
{
//  cs_spfree(m_opt_dim.linJac);
//  cs_spfree(m_opt_dim.nonLinJacDecVar);
}

std::vector<DyosOutput::StageOutput> MultiScenarioOptProblem::getOutput()
{
  // todo implement functionality
  std::vector<DyosOutput::StageOutput> out;
  return out;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getNonLinConstraintHessianDecVar
                                        (std::vector<std::vector<double> > &hessianMatrix)
{
  return OptimizationProblem::SUCCESS;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getNonLinConstraintHessianConstParam
                                        (std::vector<std::vector<double> > &hessianMatrix)
{
  return OptimizationProblem::SUCCESS;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getAdjointsDecVar(
                               std::vector<double> &adjoints1stOrder,
                               std::vector<std::vector<double> > &adjoints2ndOrder)
{
  return OptimizationProblem::SUCCESS;
}

OptimizationProblem::ResultFlag MultiScenarioOptProblem::getAdjointsConstParam(
                                   std::vector<double> &adjoints1stOrder,
                                   std::vector<std::vector<double> > &adjoints2ndOrder)
{
  return OptimizationProblem::SUCCESS;
}
