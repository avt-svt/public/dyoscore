/**
* @file SecondOrderOptimizationProblem.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* This file contains member functions of OptimizationProblem class     \n
* for 2nd order Optimization problem                                   \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 14.09.2012
*/

#include "SecondOrderOptimizationProblem.hpp"

/**
* @note this function will probably be obsolete if matrix class is used
*/
void convertSparseToVectorMatrix(const CsTripletMatrix::Ptr sparseMatrix, std::vector<std::vector<double> > &vectorMatrix)
{
  //assume triplet format for sparseMatrix
  assert(sparseMatrix->get_nnz() >= 0);
  vectorMatrix.resize(sparseMatrix->get_n_rows());
  for(int i=0; i<sparseMatrix->get_n_rows(); i++){
    vectorMatrix[i].resize(sparseMatrix->get_n_cols(), 0.0);
  }
  
  for(int i=0; i<sparseMatrix->get_nnz(); i++){
    const unsigned rowIndex = sparseMatrix->get_matrix_ptr()->i[i];
    const unsigned colIndex = sparseMatrix->get_matrix_ptr()->p[i];
    vectorMatrix[rowIndex][colIndex] = sparseMatrix->get_matrix_ptr()->x[i]; 
  }
}

SecondOrderOptimizationProblem::SecondOrderOptimizationProblem(
                                 const GenericIntegratorReverse::Ptr& integrator,
                                 const OptimizerMetaData2ndOrder::Ptr& opt,
                                 const bool minimize)
                                 :FirstOrderOptimizationProblem(integrator, opt, minimize)
{
  m_opt2ndOrder = opt;
  m_intReverse = integrator;
}

OptimizationProblem::ResultFlag SecondOrderOptimizationProblem::getNonLinConstraintHessianDecVar(
                                     std::vector<std::vector<double> > &hessianMatrix)
{
  apply_new_x();
  applyNewLagrange();
  if(m_integratorResult == OptimizationProblem::SUCCESS){
    CsTripletMatrix::Ptr csHessian;
    m_opt2ndOrder->getNonLinConstraintHessianDecVar(csHessian);
    convertSparseToVectorMatrix(csHessian, hessianMatrix);
  }
  return m_integratorResult;
}

OptimizationProblem::ResultFlag SecondOrderOptimizationProblem::getNonLinConstraintHessianConstParam(
                                     std::vector<std::vector<double> > &hessianMatrix)
{
  apply_new_x();
  applyNewLagrange();
  if(m_integratorResult == OptimizationProblem::SUCCESS){
    CsTripletMatrix::Ptr csHessian;
    m_opt2ndOrder->getNonLinConstraintHessianConstParam(csHessian);
    convertSparseToVectorMatrix(csHessian, hessianMatrix);
  }
  return m_integratorResult;
}

OptimizationProblem::ResultFlag SecondOrderOptimizationProblem::getAdjointsDecVar(
                               std::vector<double> &adjoints1stOrder,
                               std::vector<std::vector<double> > &adjoints2ndOrder)
{
  apply_new_x();
  applyNewLagrange();
  if(m_integratorResult == OptimizationProblem::SUCCESS){
    CsTripletMatrix::Ptr csAdjoints2ndOrder;
    m_opt2ndOrder->getAdjointsDecVar(adjoints1stOrder, csAdjoints2ndOrder);

    convertSparseToVectorMatrix(csAdjoints2ndOrder, adjoints2ndOrder);
  }
  return m_integratorResult;
}

OptimizationProblem::ResultFlag SecondOrderOptimizationProblem::getAdjointsConstParam(
                                   std::vector<double> &adjoints1stOrder,
                                   std::vector<std::vector<double> > &adjoints2ndOrder)
{
  apply_new_x();
  applyNewLagrange();
  if(m_integratorResult == OptimizationProblem::SUCCESS){
    CsTripletMatrix::Ptr csAdjoints2ndOrder;
    m_opt2ndOrder->getAdjointsConstParam(adjoints1stOrder, csAdjoints2ndOrder);

    convertSparseToVectorMatrix(csAdjoints2ndOrder, adjoints2ndOrder);
  }
  return m_integratorResult;
}

void SecondOrderOptimizationProblem::apply_new_x()
{
  if (m_new_dec_vars) {
    GenericIntegrator::ResultFlag result = m_intReverse->solveForward();
    switch(result){
      case GenericIntegrator::FAILED:
        m_integratorResult = OptimizationProblem::FAILED;
        break;
      case GenericIntegrator::SUCCESS:
        m_integratorResult = OptimizationProblem::SUCCESS;
        m_new_dec_vars = false;
        break;
      default:
        assert(false);
    }
  }
}

/**
*@brief check if new lagrange Multipliers are set and if so integrate
*
* this function is currently a copy of apply_new_x
* as soon as we can split forward and backward integration (to save time in optimizers
* like filter_sqp), this function simply performs a reverse integration if new 
* lagrange multipliers have been set (and first forward sweep still has current data)
*/
void SecondOrderOptimizationProblem::applyNewLagrange()
{
  if(m_new_multipliers && m_integratorResult == OptimizationProblem::SUCCESS) {
    m_intReverse->solveReverse();
    m_new_multipliers = false;
  }
}