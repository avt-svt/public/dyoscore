/**
* @file FilterSQPWrapper.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* FilterSQPWrapper - Part of DyOS                                          \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi, Fady Assassa
* @date 31.10.2012
*/

#include "FilterSQPWrapper.hpp"
#include "FilterSQPLoader.hpp"

#include "cs.h"
#include <algorithm> 

#include <boost/filesystem.hpp>

#include <string>
#include <cassert>


// initialization of statics. Instead of doing it in every test suite we do it here for all together.
std::weak_ptr<OptimizationProblem> FilterSQPWrapper::m_staticOptProb;
std::weak_ptr<Logger> FilterSQPWrapper::m_staticLogger;
std::fstream FilterSQPWrapper::m_staticFileReader;
std::vector<double> FilterSQPWrapper::m_staticLastSetDecVars;

/**
* @brief constructor: initialize resources which are independent of current problem (e.g. open output files)
*
* @param optMD pointer to an OptimizerMetaData object used for the optimization
* @param genInt pointer to an integrator object used for the optimization
*/
FilterSQPWrapper::FilterSQPWrapper(const OptimizationProblem::Ptr& optProb)
{
  m_optProb = optProb;
  m_isSparse = true; // sparse option chosen
}

void FilterSQPWrapper::openLogFile()
{
  std::string summary = "filterSQP.summary";
  //delete old file
  std::fstream deleter(summary.c_str(), std::ios_base::out);
  deleter.close();
  
  m_staticFileReader.open(summary.c_str(), std::ios_base::in);
  // if file is still held open by filterSQP,
  // set file pointer to the end of the file
  std::string content;
  std::streampos pos;
  //find the end of the file
  while(!m_staticFileReader.eof()){ 
    pos = m_staticFileReader.tellg();
    std::getline(m_staticFileReader, content);
  }
  // reset eof bit and fail bit
  m_staticFileReader.clear();
  // set position of the file reader to the last valid position
  m_staticFileReader.seekg(pos);
}
void FilterSQPWrapper::closeLogFile()
{
  m_staticFileReader.close();
}


void FilterSQPWrapper::logFilterSQPOutput(Logger::Ptr &logger)
{
  std::string content;
  bool firstline = true;
  static std::streampos pos;
  while(!m_staticFileReader.eof()){ 
    //collect the current position before reading the next line
    pos = m_staticFileReader.tellg();
    if(!firstline){
      logger->newline(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY);
    }
    firstline = false;
    //if reading fails, content will be empty - don't update position in this case
    std::getline(m_staticFileReader, content);
    logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY,
                              content);
  }
  // reset eof bit and fail bit
  m_staticFileReader.clear();
  // set position of the file reader to the last valid position
  //if FilterSQP prints further data to the file, it will be read at the next call to logFilterSQPOutput
  m_staticFileReader.seekg(pos);
}

/**
* @brief solve optimization problem
*/
DyosOutput::OptimizerOutput FilterSQPWrapper::solve()
{
  DyosOutput::OptimizerOutput output;
  m_staticOptProb = this->m_optProb;
  m_staticLogger = this->m_logger;
  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();

  const int numOptVars = optProbDim.numOptVars; //number of optimization variables
  const int numLinConstraintsVars = optProbDim.numLinConstraints; //number of general linear constraints
  const int numNonLinConstraintsVars = optProbDim.numNonLinConstraints; //number of nonlinear constraints
  // the order of the constraints doesn't matter. We will assume first linear constraints and then nonlinear.
  const int numConstraints = numLinConstraintsVars + numNonLinConstraintsVars; // m
  const int kmax = numOptVars; // maximum size of null-space

  const int maxf = 100; // maximum size of the filter; value according to manual
  const int mlp = 100; // maximum level parameter for resolving degeneracy in bqpd
  const int mxwk = 100000;  // max size of real workspace
  const int mxiwk = 100000; // max size of integer worlspace
  const int iprint = 1; // print flag (todo: this should be set in the options)
  const int nout = 12; // output channel (6 = screen)
  int ifail = 0; // fail flag indicating successful run
  const double rho = 10; // initial trurst-region radius; default is 10
  utils::Array<double> x(numOptVars, 0.0); // starting point and final solution
  m_optProb->getDecVarValues(x);
  utils::Array<double> c(numConstraints, 0.0); // vector to store the final values of the general constraints
  double f = 0.0; // the final objective value
  const double fmin = -1e20; // lower bound on the objective value (todo could be set by the user)
  // the bounds arrays include decisionvariable bounds and constraint bounds. 
  // first linear constraints then nonlinear constraints
  utils::Array<double> lowerBounds(numOptVars+numConstraints, 0.0); // vector of lower bounds
  utils::Array<double> upperBounds(numOptVars+numConstraints, 0.0); // vector of upper bounds
  assembleBounds(lowerBounds, upperBounds);

  utils::Array<double> s(numOptVars+numConstraints, 1.0); // scale factors; thus no scaling selected

  // assemble Jacobian, in general case non-linear and linear
  std::vector<double> jacVals;
  std::vector<int> jacRowIndices, jacColPointer;
  int nnzLinear, nnzNonlinear;
  assembleJacobian(jacVals, jacRowIndices, jacColPointer, nnzLinear, nnzNonlinear);
  // maximum number of nonzero entries allowed in jacobian matrix a
  int maxa = numOptVars + jacVals.size();

  // this could be specified by the user if required.
  utils::Array<double> a(maxa, 0.0); // stores the objective gradient and constraint normals
  utils::Array<int> la(a.getSize() + numConstraints + 3, 0);// column indices and length of rows of entries in a

  la[0] = maxa + 1;
  for(int i=1; i<=numOptVars; i++){
    // first gradients of the objective function
    la[i] = i; // fortran notation
  }
  for(unsigned i=0; i< jacRowIndices.size();i++){
    // rows of the jacobian function
    la[i + numOptVars + 1] = jacRowIndices[i]; // fortran notation
  }
  unsigned startOfColIndices = jacRowIndices.size() + numOptVars + 1;
  la[startOfColIndices] = 1;

  for(unsigned i=0; i<jacColPointer.size() - 1; i++){
    // point to the first element of the Jacobian matrix A
    la[i + startOfColIndices + 1] = jacColPointer[i] + numOptVars;
  }
  // last entry
  int lastLaEntry = a.getSize() + numConstraints + 2;
  la[lastLaEntry] = la[0];


  utils::Array<double> ws(mxwk, 0.0); // real workspace
  utils::Array<int> lws(mxiwk, 0); // integer workspace
  utils::Array<double> lam(numOptVars+numConstraints, 0.0); // lagrange multipliers
  utils::Array<char> cstype(numConstraints, 'N');
  for(int i=0; i<numLinConstraintsVars; i++){
    cstype[i] = 'L'; // indicates a linear constraint
  }
  const int cstypelen = numConstraints;
  // was hier?
  utils::Array<double> user(1, 0.0); // real workspace passed through user routines
  utils::Array<int> iuser(4); // integer workspace passed through user routines
  iuser[0] = numLinConstraintsVars;
  iuser[1] = numNonLinConstraintsVars;
  iuser[2] = nnzLinear;
  iuser[3] = nnzNonlinear;
  int max_iter = 150; //user supplied iteration unit for SQP solver //todo: option
  utils::Array<int> istat(14, 1); // integer space for solution statistics
  utils::Array<double> rstat(7, 0.0); // real space for solution statistics

  assert(!jacVals.empty());
  assert(!jacRowIndices.empty());
  assert(!jacColPointer.empty());

  openLogFile();
  filtersqp.filterSQP(numOptVars,
    numConstraints,
    kmax,
    maxa,
    maxf,
    mlp,
    mxwk,
    mxiwk,
    iprint,
    nout,
    ifail,
    rho,
    x,
    c,
    f,
    fmin,
    lowerBounds,
    upperBounds,
    s,
    a,
    la,
    ws,
    lws,
    lam,
    cstype,
    cstypelen,
    user,
    iuser,
    max_iter,
    istat,
    rstat,
    &FilterSQPWrapper::objfun,
    &FilterSQPWrapper::confun,
    &FilterSQPWrapper::gradient,
    &FilterSQPWrapper::hessian,
    &FilterSQPWrapper::Wdotd);

  logFilterSQPOutput(m_logger);
  closeLogFile();
  // put output (objective function value, values of decision variables back to DyOS structs
  utils::WrappingArray<const double> optVarValuesOut(numOptVars, x.getData());
  m_optProb->setDecVarValues(optVarValuesOut);// setDecVarValues needs 'const double' type

  utils::WrappingArray<double> linLagrangeMultipliers(numLinConstraintsVars,
    &lam[numOptVars]);
  utils::WrappingArray<double> nonLinLagrangeMultipliers(numNonLinConstraintsVars,
    &lam[numOptVars + numLinConstraintsVars]);
  m_optProb->setLagrangeMultipliers(nonLinLagrangeMultipliers,
    linLagrangeMultipliers);
  utils::WrappingArray<double> optVarLagrangeMultiplier(numOptVars, &lam[0]);
  m_optProb->setDecVarLagrangeMultipliers(optVarLagrangeMultiplier);

  switch(ifail)
  {
  case 0:
    // successful run, solution found
    output.m_informFlag = DyosOutput::OptimizerOutput::OK;
    break;
  case 1:
    // unbounded, feasible point x with f(x)<=fmin found
    output.m_informFlag = DyosOutput::OptimizerOutput::OK;
    break;
  case 2:
    // linear constraints are inconsistent
    output.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
    break;
  case 3:
    // nonlinear infeasible, opimal solution to feasibility problem found
    output.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
    break;
  case 4:
    // terminate at point with h(x)<=eps but qp infeasible
    output.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
    break;
  case 5:
    // termination with rho<eps  ???????
    output.m_informFlag = DyosOutput::OptimizerOutput::NOT_OPTIMAL;
    break;
  case 6:
    // termination with iter>max_iter  ???????
    output.m_informFlag = DyosOutput::OptimizerOutput::TOO_MANY_ITERATIONS;
    break;
  case 7:
    // crash in user routine (ieee error) could not be resolved
    output.m_informFlag = DyosOutput::OptimizerOutput::FAILED;
    break;
  case 8:
    // unexpected ifail from qp solver  ???????
    output.m_informFlag = DyosOutput::OptimizerOutput::FAILED;
    break;
  case 9:
    // not enough real workspace
    output.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
    break;
  case 10:
    // not enough integer workspace
    output.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
    break;
  default:
    output.m_informFlag = DyosOutput::OptimizerOutput::NOT_OPTIMAL;
  }
  getOptimizerOutput(output);
  return output;
}

void FilterSQPWrapper::setDecVars(double *x, int*n)
{
  utils::WrappingArray<const double> decVar(*n, x);
  if(m_staticLastSetDecVars.empty()){
    m_staticLastSetDecVars.assign(x, x + *n);
  }
  bool isDifferent = false;
  const double threshold = 1e-16;
  for(int i=0; i<*n; i++){
    if(fabs(x[i] - m_staticLastSetDecVars[i])>threshold){
      isDifferent = true;
      break;
    }
  }
  if(isDifferent){
    m_staticOptProb.lock()->setDecVarValues(decVar);
    m_staticLastSetDecVars.assign(x, x + *n);
  }
}

/** @brief evaluate the linear and nonlienar constraints of the problem
@param[in] x x(n) the value of the current variable
@param[in] n number of variables
@param[in] m number of constraints (linear + nonlinear)
@param[out] c vector of linear and nonlinear constraints
@param[in/out] a the jacobian vector storing the nonzeros of the jacobian
@param[in] la column indices for a and pointer to start of each row
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[out] flag Set to 1 if arithemtics exception occured in confun, 0 otherwise. If an arithemtic
exception occured, then the constraints must not be modified by the function
*/
void FilterSQPWrapper::confun(double *x,
                              int *n,
                              int *m,
                              double *c,
                              double *a,
                              int *la,
                              double *user,
                              int *iuser,
                              int *flag)
{
  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();
  logFilterSQPOutput(staticLogger);

  // put current values of optimization variables to OptimizerSingleStageMetaData
  setDecVars(x,n);

  OptimizationProblem::ResultFlag result;
  // get values of linear and nonlinear constraints
  int numLinear = iuser[0];
  int numNonlienar = iuser[1];

  utils::WrappingArray<double> linear_fcon(numLinear, c);
  utils::WrappingArray<double> nonlinear_fcon(numNonlienar , c + numLinear );
  // it should be enough to check only one result flag
  result = staticOptProb->getNonLinConstraintValues(nonlinear_fcon);
  staticOptProb->getLinConstraintValues(linear_fcon);
  *flag = 0;// set to 1 if arithetics exception occurred in confun; 0 otherwise
  if(result == OptimizationProblem::FAILED){
    staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "integration failed\n" );
    *flag = 1; 
  }
}

/** @brief evaluate the objective funtion of the problem 
@param[in] x x(n) the value of the current variable
@param[in] n number of variables
@param[out] f objective function
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[out] flag Set to 1 if arithemtics exception occured in objfun, 0 otherwise. If an arithemtic
exception occured, then the objective must not be modified by the function
*/
void FilterSQPWrapper::objfun(double *x,
                              int *n,
                              double *f,
                              double *user,
                              int *iuser,
                              int *flag)
{
  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();
  logFilterSQPOutput(staticLogger);

  // put current values of optimization variables to OptimizerSingleStageMetaData
  setDecVars(x,n);

  OptimizationProblem::ResultFlag result;
  // get objective function value
  result = staticOptProb->getNonLinObjValue(*f);
  *flag = 0;// set to 1 if arithetics exception occurred in confun; 0 otherwise
  if(result == OptimizationProblem::FAILED){
    *flag = 1;
  }
}
/** @brief evaluate the gradient of the objective and the constraints (linear/nonlinear)
@param[in] n number of variables
@param[in] m number of constraints (linear + nonlinear)
@param[in] x x(n) the value of the current variable
@param[in/out] a the jacobian vector storing the nonzeros of the jacobian
@param[in] la column indices for a and pointer to start of each row
@param[in] maxa maximum size of a
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[out] flag Set to 1 if arithemtics exception occured in gradient, 0 otherwise. If an arithemtic
exception occured, then the gradient must not be modified by the function
*/
void FilterSQPWrapper::gradient(int *n,
                                int *m,
                                int *mxa,
                                double *x,
                                double *a,
                                int *la,
                                int *maxa,
                                double *user,
                                int *iuser,
                                int *flag)
{
  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();
  logFilterSQPOutput(staticLogger);

  // put current values of optimization variables to OptimizerSingleStageMetaData
  setDecVars(x,n);

  // objective gradients
  // first entries of a are always the objetive gradients
  utils::WrappingArray<double> objGradients(*n, a);
  OptimizationProblem::ResultFlag result
      = staticOptProb->getNonLinObjDerivative(objGradients);
  *flag = 0;
  if(result == OptimizationProblem::FAILED){
    *flag = 1;
  }

  // constraint gradients
  CsTripletMatrix::Ptr linearJacobian = staticOptProb->getOptProbDim().linJac;
  CsTripletMatrix::Ptr jacobianMatrixStruct = staticOptProb->getOptProbDim().nonLinJacDecVar;
  int nnzLinear = iuser[2];
  int nnzNonlinear = iuser[3];
  utils::Array<double> values(nnzNonlinear, 0.0);
  CsTripletMatrix::Ptr constDerMatrix = 
    CsTripletMatrix::Ptr( new CsTripletMatrix(jacobianMatrixStruct->get_n_rows(),
                                              jacobianMatrixStruct->get_n_cols(),
                                              nnzNonlinear,
                                              jacobianMatrixStruct->get_matrix_ptr()->i,
                                              jacobianMatrixStruct->get_matrix_ptr()->p,
                                              values.getData()));

  result = staticOptProb->getNonLinConstraintDerivativesDecVar(constDerMatrix);
  if(result == OptimizationProblem::FAILED){
    staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "integration failed\n" );
    *flag = 1;
  }
  
  
  CsTripletMatrix::Ptr assembledTriplet = linearJacobian->vertcat(constDerMatrix);
  assembledTriplet->transpose_in_place();
  CsCscMatrix::Ptr compressedMatrix = assembledTriplet->compress();
  int startOfConstraintGradients = *n;
  assert(compressedMatrix->get_nnz() == nnzLinear + nnzNonlinear);
  for(int i=0; i<nnzLinear + nnzNonlinear; i++){
    a[i + startOfConstraintGradients] = compressedMatrix->get_matrix_ptr()->x[i];
  }
}

/** @brief evaluate the hessian matrix of the problem
@param[in] x x(n) the value of the current variable
@param[in] n number of variables
@param[in] m number of constraints (linear + nonlinear)
@param[in] phase indicates what kind of hessian matrix is required; phase = 2 Hessian
of the lagrangian, phase = 1 hessian of the langrangian without the objective hessian
@param[in] lam vector of lagrange multipliers (size (n+m))
@param[out] ws workspace of the hessian, passed by Wdotd (real)
@param[out] lws workspace of the hessian, passed by Wdotd (integer)
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[in/out] l_hess On enry: max. space allows for hessian storage in ws.
On exit: actual amount of hessian storage used in ws. (double)
@param[in/out] li_hess On enry: max. space allows for hessian storage in ws.
On exit: actual amount of hessian storage used in ws. (integer)
@param[out] flag Set to 1 if arithemtics exception occured in hessian, 0 otherwise. If an arithemtic
exception occured, then the Hessian must not be modified by the function
*/
void FilterSQPWrapper::hessian(double *x,
                               int *n,
                               int *m,
                               int *phase,
                               double *lam,
                               double *ws,
                               int *lws,
                               double *user,
                               int *iuser,
                               int *l_hess,
                               int *li_hess,
                               int *flag)
{
  Logger::Ptr staticLogger = m_staticLogger.lock();
  logFilterSQPOutput(staticLogger);
  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  //set x and lam here to prevent Wdotd to evaluate model everytime
  if(*phase == 2){
    // hessian with the objective function
    staticOptProb->setObjectiveLagrangeMultiplier(1.0);
  }else{
    // hessian without the objtive function
    staticOptProb->setObjectiveLagrangeMultiplier(0.0);
  }

  setDecVars(x,n);

  // separate lagrange multipliers
  int numLinear = iuser[0];
  int numNonlinear = iuser[1];
  utils::WrappingArray<double> decVarLam(*n, lam);
  double *startLamLinear = lam + *n;
  utils::WrappingArray<double> linearLam(numLinear, startLamLinear);
  double *startLamNonlinear = startLamLinear + numLinear;
  utils::WrappingArray<double> nonlinearLam(numNonlinear, startLamNonlinear);
  
  for (int i=0; i<numNonlinear; i++)
    nonlinearLam[i] *= -1;
  for (int i=0; i<numLinear; i++)
    linearLam[i] *= -1;
  for (int i=0; i<*n; i++)
    decVarLam[i] *= -1;

  staticOptProb->setLagrangeMultipliers(nonlinearLam, linearLam);
  staticOptProb->setDecVarLagrangeMultipliers(decVarLam);

  //check if hessian is computable
  std::vector<std::vector<double> > hessian;
  OptimizationProblem::ResultFlag result
      = staticOptProb->getNonLinConstraintHessianDecVar(hessian);
  if(result == OptimizationProblem::FAILED){
    *flag = 1;
  }
  else{
    *flag = 0;
    assert(*li_hess >= 2);
    int hessianRowSize = hessian.size();
    int hessianColSize = 0;
    if(!hessian.empty()){
      hessianColSize = hessian.front().size();
    }
    lws[0] = hessianRowSize;
    lws[1] = hessianColSize;

    int hessianSize = hessianColSize * hessianRowSize;
    assert(*l_hess >= hessianSize);
    for(int i=0; i<hessianRowSize; i++){
      for(int j=0; j<hessianColSize; j++){
        ws[i*hessianColSize + j] = hessian[i][j];
      }
    }
    *l_hess = hessianSize;
    *li_hess = 2;
  }
}

void FilterSQPWrapper::Wdotd(int *n, double *d, double *ws, int *lws, double *v)
{
  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();
  logFilterSQPOutput(staticLogger);

  utils::WrappingArray<double> result(*n, v);
  utils::WrappingArray<double> inputVec(*n, d);
  //staticOptProb->getNonLinConstraintHessianDecVarDotD(inputVec, result);
  int rowSize = lws[0];
  int colSize = lws[1];
  
  assert(*n == rowSize);
  assert(*n == colSize);
  
  for(int i=0; i<rowSize; i++){
    v[i] = 0.0;
    for(int j=0; j<colSize; j++){
      v[i] += ws[i*colSize + j]*d[j];
    }
  }
}


/** @brief calculates the real workspace for SQP, QP and linear algebra solvers according to the manual
* @param optionaFlag switches between sparse and dense
* @param n number of variables
* @param m number of constraints (linear + nonlinear)
* @param mlp maximum level parameter for solving degeneracy in bqpd
* @param maxf maximum size of the filter
* @param kmax maximum sizfe of the null-space
* @return size of the real workspace for SQP, QP and linear algebra solvers
*/
int FilterSQPWrapper::maxRealWS(const int n,
                                const int m,
                                const int mlp,
                                const int maxf,
                                const int kmax) const
{
  int iRet;
  int mxm1;
  if (n<=m+1)
    mxm1 = n;
  else
    mxm1 = m+1;
  if(m_isSparse == false)
  {
    // dense linear algebra solver
    iRet = 16*n + 8*m + mlp + 8*maxf + kmax*(kmax+9)/2 + mxm1*(mxm1+3)/2;
  }
  else{ 
    // sparse linear algebra solver
    iRet = 16*n + 8*m + mlp + 8*maxf + kmax*(kmax+9)/2 + 5*n + 20*n;
  }
  return iRet;
}

/** @brief calculates the integer workspace for SQP, QP and linear algebra solvers according to the manual
* @param optionaFlag switches between sparse and dense
* @param n number of variables
* @param m number of constraints (linear + nonlinear)
* @param mlp maximum level parameter for solving degeneracy in bqpd
* @param kmax maximum sizfe of the null-space
* @return size of the integer workspace for SQP, QP and linear algebra solvers
*/
int FilterSQPWrapper::maxIntWS(const int n,
                               const int m,
                               const int mlp,
                               const int kmax) const
{
  int iRet;
  int mxm1;
  if (n<=m+1)
    mxm1 = n;
  else
    mxm1 = m+1;
  if(m_isSparse == false)
  {
    // dense linear algebra solver
    iRet = 4*n + 3*m + mlp + 100 + kmax + mxm1;
  }
  else{
    // sparse linear algebra solver
    iRet = 4*n + 3*m + mlp + 100 + kmax + 9*n + m;
  }
  return iRet;
}

/** @brief assembles bounds of decision variables and linear, then nonlinear constraints.
* @param[in/out] lowerBounds lower bounds of decision vars, linear and nonlinear constraints
* @param[in/out] upperBounds upper bounds of decision vars, linear and nonlinear constraints
*/
void FilterSQPWrapper::assembleBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds)const
{

  OptimizerProblemDimensions probDim = m_optProb->getOptProbDim();

  utils::Array<double> decVarL(probDim.numOptVars), decVarU(probDim.numOptVars);
  m_optProb->getDecVarBounds(decVarL, decVarU);

  utils::Array<double> linConstraintsL(probDim.numLinConstraints),
    linConstraintsU(probDim.numLinConstraints);
  m_optProb->getLinConstraintBounds(linConstraintsL, linConstraintsU);

  utils::Array<double> nonlinConstraintsL(probDim.numNonLinConstraints),
    nonlinConstraintsU(probDim.numNonLinConstraints);
  m_optProb->getNonLinConstraintBounds(nonlinConstraintsL, nonlinConstraintsU);

  assert(lowerBounds.getSize() == upperBounds.getSize());
  assert(lowerBounds.getSize() == probDim.numOptVars + probDim.numLinConstraints + probDim.numNonLinConstraints);

  for(unsigned i=0; i<probDim.numOptVars; i++){
    lowerBounds[i] = decVarL[i];
    upperBounds[i] = decVarU[i];
  }

  for(unsigned i=0; i<probDim.numLinConstraints; i++){
    lowerBounds[i + probDim.numOptVars] = linConstraintsL[i];
    upperBounds[i + probDim.numOptVars] = linConstraintsU[i];
  }

  for(unsigned i=0; i<probDim.numNonLinConstraints; i++){
    lowerBounds[i + probDim.numOptVars + probDim.numLinConstraints] = nonlinConstraintsL[i];
    upperBounds[i + probDim.numOptVars + probDim.numLinConstraints] = nonlinConstraintsU[i];
  }
}

/*  ******copied from snopt******
*@brief assemble Jacobian matrix for FITERSQP in CSC format
*
* assemble complete Jacobian matrix from linear and nonlinear parts; value
* vector is only a dummy but allocated to the correct size
*
*@param[out] jacValsDummy dummy ExtendableArray for Jacobian values
*@param[out] jacRowIndices ExtendableArray containing row indices of Jacobian
*@param[out] jacColPointer ExtendableArray containing column pointers
@ remarks if optProbDim.nonLinJac was in csc format we could directly pass the
rowind, colptr and val vector to FITERSQP; Contrary to the old version of DyOS,
in the new one there are only piecewise constant and piecewise linear control
functions and the constraints due to the controls as treated as simple bounds
Hence there is no linear matrix (this is true only for single stage problems)
*/
void FilterSQPWrapper::assembleJacobian(std::vector<double> &jacValsDummy,
                                        std::vector<int> &jacRowIndices,
                                        std::vector<int> &jacColPointer,
                                        int &nnzLinear,
                                        int &nnzNonlinear) const
{
  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();

  // nonlinear Jacobian
  // Jacobian of constraints in compressed sparse column (CSC) format (only
  // structural data, value vector is NULL pointer)
  CsTripletMatrix::Ptr nonLinJacCoo = optProbDim.nonLinJacDecVar;
  CsTripletMatrix::Ptr linJacCoo = optProbDim.linJac;

  nnzLinear = linJacCoo->get_nnz();
  nnzNonlinear = nonLinJacCoo->get_nnz();

  CsTripletMatrix::Ptr assembledTriplet = linJacCoo->vertcat(nonLinJacCoo);
  assembledTriplet->transpose_in_place();
  CsCscMatrix::Ptr assembledCsc = assembledTriplet->compress();

  std::vector<double>jacValsDummy1(assembledCsc->get_nnz(),0.0);
  std::vector<double>jacRowIndices1(assembledCsc->get_nnz(),0);
  std::vector<double>jacColPointer1(assembledCsc->get_n_cols()+1,0);

  jacValsDummy.resize(assembledCsc->get_nnz(), 0.0); // dummy array
  jacRowIndices.resize(assembledCsc->get_nnz(), 0);
  jacColPointer.resize(assembledCsc->get_n_cols()+1, 0);

  for (int i=0; i < assembledCsc->get_nnz(); i++) {
    jacRowIndices[i] = assembledCsc->get_matrix_ptr()->i[i] + 1; // in fortran indices have offset 1
    jacValsDummy[i] = assembledCsc->get_matrix_ptr()->x[i];
  }
  for(int j=0; j<assembledCsc->get_n_cols()+1; j++){
    jacColPointer[j] = assembledCsc->get_matrix_ptr()->p[j] + 1; // in fortran indices have offset 1
  }
}
