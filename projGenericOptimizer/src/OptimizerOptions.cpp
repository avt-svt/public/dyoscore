/** @file OptimizerOptions.cpp
*    @brief implementation of NPSOLOptions functions
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails OptimizerOptions
*    =====================================================================\n
*   @author Ann-Kathrin Dombrowski
*   @date 17.6.2013
*/

#include "OptimizerOptions.hpp"
#include <cmath>
#include <iostream>
using namespace std;


/**
* @brief check if a string is an integer, a double or no number at all
*
* @param string to be checked
* @return enum for NumberType, double d
*/
pair<OptimizerOptions::NumberType, double> OptimizerOptions::number(const std::string& s)const
{
  if(s.empty())
    return make_pair<NumberType, double>(EMPTY, 0);

  double d;
  const char *str=s.c_str();//convert string to char* for function strtod
  char* end = 0 ;

  d = strtod( str, &end );
  if(*end ==0 ){
    if(fmod(d,1)==0){//integer
      if(d>=0)
		  return make_pair<NumberType, double>(POS_INTEGER, std::move(d));
      return make_pair<NumberType, double>(INTEGER, std::move(d));
    }
    if(d>=0)
        return make_pair<NumberType, double>(POS_DBL, std::move(d));
    return make_pair<NumberType, double>(DBL, std::move(d));//double
  }
  else
    return make_pair<NumberType, double>(NO_NUMBER, 0);//string didn't represent a number
}

/**
* @brief extract option from string, check if valid
* required to check the options coming from config file.
*
* @param vector of strings to be checked
* @return map with options(everything lower case, only one space between words)
*/
std::map<std::string, std::string> OptimizerOptions::checkOptions(std::vector<std::string> &optionVec)
{
  bool optionFound = false;
  std::map<std::string, std::vector<NumberType> >::const_iterator iter;
  std::map<std::string, std::string> optionsMap;
  std::pair<NumberType, double> optionValue;
  std::string temp;
  std::string validOption="";

  for(unsigned i=0; i<optionVec.size(); i++){
    for(iter=validOptions.begin(); iter!=validOptions.end(); iter++){
      if(optionVec[i].find((*iter).first)!=string::npos){
        if(((*iter).first).size()>validOption.size()){//look for longest matching string
          validOption = (*iter).first;
          optionFound = true;
        }
      }
    }
    if(optionFound == false){
      std::cout<<"warning: invalid Option "+optionVec[i]<<std::endl;
      //throw "unvalidOption"-exception
    }
    temp = optionVec[i];
    temp.erase(temp.begin(), temp.begin()+validOption.size());
    boost::trim(temp);
    optionValue = number(temp);
    //here only the type of the optionValue is checked. further checks may be necessary
    if(find (validOptions[validOption].begin(), validOptions[validOption].end(), optionValue.first)==validOptions[validOption].end()){
      if(optionValue.first == POS_INTEGER){
        optionValue.first = INTEGER;
      }
      else if(optionValue.first == POS_DBL){
        optionValue.first = DBL;
      }
       if(find (validOptions[validOption].begin(), validOptions[validOption].end(), optionValue.first)==validOptions[validOption].end()){
         std::cout<<"warning: invalid Option Input '"+temp+"' for option "+validOption<<std::endl;
         //throw "unvalidOptionInput"-exception
       }
      }
    //if everything is fine option and value are added to modifiedUserOptions
    optionsMap[validOption]=temp;
    validOption = "";
    optionFound = false;
  }
  return optionsMap;
}




/**
* @brief check if all options in a map are valid
* required to check options coming from user input.
*
* @param map to be checked
* @return modified map(everything lower case, only one space between words)
*/
std::map<std::string, std::string> OptimizerOptions::checkOptions(const std::map<std::string, std::string> &userOptions)
{
  std::map<std::string, std::string>::const_iterator iter;
  std::map<std::string, std::string> modifiedUserOptions;
  std::string tempString;
  std::vector<std::string> tempvec;
  std::pair<NumberType, double> optionValue;
  std::string sub;
  
  for(iter=userOptions.begin(); iter!=userOptions.end(); iter++){
    std::istringstream iss((*iter).first);
    tempvec.clear();
    while(iss){//split string into words to avoid faults due to additional white spaces
      iss >> sub;
      tempvec.push_back(sub);
    }
    assert(tempvec.size()>0);
    tempvec.erase(tempvec.end()-1);//last entry is space
    tempString.clear();
    for(unsigned j=0; j<tempvec.size(); j++){
      tempString.append(tempvec[j]);
      tempString.append(" ");
    }
    boost::algorithm::to_lower(tempString);
    boost::trim(tempString);

    if(validOptions.find(tempString)==validOptions.end()){//key not found in validOptions
      std::cout<<"warning: unvalid Option "+tempString<<std::endl;
      //throw "unvalidOption"-exception
    }

    optionValue = number((*iter).second);
    //here only the type of the optionValue is checked. further checks may be necessary
    if(find (validOptions[tempString].begin(), validOptions[tempString].end(), optionValue.first)==validOptions[tempString].end()){//valueType of optionInput is not allowed (f.e. valueType DBL for an iteration limit)
      if(optionValue.first == POS_INTEGER){
        optionValue.first = INTEGER;
      }
      else if(optionValue.first == POS_DBL){
        optionValue.first = DBL;
      }
      if(find (validOptions[tempString].begin(), validOptions[tempString].end(), optionValue.first)==validOptions[tempString].end()){
        std::cout<<"warning: invalid Option Input '"+(*iter).second+"' for option "+tempString<<std::endl;
        //throw "unvalidOptionInput"-exception
      }
    }
    modifiedUserOptions[tempString]=(*iter).second;//if everything is fine option and value are added to modifiedUserOptions
  }
  return modifiedUserOptions;
}
