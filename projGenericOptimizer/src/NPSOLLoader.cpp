/** @file NPSOLLoader.cpp
*    @brief definitions of member functions of NPSOLLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails NPSOLLoader
*    =====================================================================\n
*   @author Mathias Dunst
*   @date 31.10.2012
*/

#include "NPSOLLoader.hpp"
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#ifdef WIN32
#define FORTRAN_UNDERSCORE ""
#else
#define FORTRAN_UNDERSCORE "_"
#endif

/**
* @brief standard constructer of NPSOLLoader
*
* loads the dll npsol502.dll
*/
NPSOLLoader::NPSOLLoader() : Loader("npsol502")
{
  loadFunctions();
}

/**
* @brief copy constructer of NPSOLLoader
*
* @param npsolLoader NPSOLLoader to be copied
*/
NPSOLLoader::NPSOLLoader(const NPSOLLoader &npsolLoader) : Loader(npsolLoader)
{
  loadFunctions();
}

/**
* @brief load all functions from NPSOL DLL which are required by DyOS
*/
void NPSOLLoader::loadFunctions()
{
  // function handles have to be initialized to NULL, otherwise in Loader.hpp
  // it is assumed that they have already been loaded
  npsolFuncHandle = NULL;

  npsolOpenFileFuncHandle = NULL;
  npsolCloseFileFuncHandle = NULL;
  npsolOptionFileFuncHandle = NULL;

  // load functions from NPSOL DLL using Windows system function GetProAddress()
  loadFcn(npsolFuncHandle, "npsol" FORTRAN_UNDERSCORE);

  loadFcn(npsolOpenFileFuncHandle, "np_openfile" FORTRAN_UNDERSCORE);
  loadFcn(npsolCloseFileFuncHandle, "np_closefile" FORTRAN_UNDERSCORE);
  loadFcn(npsolOptionFileFuncHandle, "npfile" FORTRAN_UNDERSCORE);
}

/**
* @brief calls the FORTRAN function NPSOL from npsol dll
*
* @param[in] n number of optimization variables - must be positive
* @param[in] numLinConstraints number of general linear constraints - must be >=0. Named in NPSOL as nclin
* @param[in] numNonLinConstraints number of nonlinear constraints - must be >=0 Named in NPSOL as ncnln
* @param[in] rowMatrixA is the row dimension of the Array matrixA - must be >=1 and >=numLinConstraints.
*                       Set rowMatrixA to 1 if numLinConstraints==0
* @param[in] rowJacobian is the row dimension of the array jacobianConstraints - must be >=1 and >=numNonLinConstraints.
*                        Set rowJacobian to 1 if numNonLinConstraints==0
* @param[in] rowMatrixR is the row dimension of the array matrixR - must be >=n
* @param[in] matrixA is an array that contains the matrix A for the linear constraints - dimesnion (rowMatrixA,k) for some k>=n
*            If numLinConstraints is zero, matrixA is not referenced. In that case, matrixA may be dimensioned (rowMatrixA,1) with rowMatrixA=1,
*            or could be any convenient array
* @param[in] lowerBounds contains the lower bounds on the variables and constraints
* @param[in] upperBounds contains the upper bounds on the variables and constraints
* @param[in] confun is the name of a subroutine that calculates the vector c(x) of nonlinear constraint functions
* @param[in] objfun is the name of a subroutine that calculates the objective function f(x)
* @param[out] inform reports the result of the call to NPSOL
* @param[out] iter is the number of major iterations performed
* @param[in, out] istate input: is an array - dimension must be >=n+nclin+ncnln. It need not be initialized
*                               NPSOL is called with a Cold Start (default option).
*                               look into manual for further information
*                        output: describes the status of constraints
* @param[out] constraints is an array - must be >=numNonLinConstraints. If numNonLinConstraints =0, constraints is not accessed and may then be declared 
*                         to be of dimension (1), or the actual parameter may be any convenient array
* @param[in, out] jacobianConsrtaints input: is an array - dimension must be (rowJacobian,k) for some k>=n. If numNonLinConstraints = 0,
*                                     jacobianConstraints is not referenced. In that case, jacobianConstraints may be dimensioned (rowJacobian,1) with rowJacobian=1
*                                     output: contains the Jacobian matrix of the nonlinear constraints at the final iterate
* @param[in, out] clamda input: is an array - dimension must be >=n+nclin+ncnln. It need not be initialized if NPSOL
*                               is called with a Cold Start (default). Look into the manual for further information
*                        ouput: contains the QP multipliers from the last QP subproblem
* @param[out] f is the value of the objective f(x) at the final iterate
* @param[out] g is an array - must be >=n, that contains the objective gradient
* @param[in, out] matrixR input: is an array - dimension must be (rowMatrixR,k) for some k>=n. matrixR need not be initialized if NPSOL
*                          is called with a Cold Start(default), and will be taken as the identity.
*                          Look into the manual for further information
*                         output: contains information about H, the Hessian of the Lagrangian
* @param[in, out] x input: is an array - must be >=n. It contains an initial estimate of the solution
* @param[in] intWorkSpace is an array - must be >=leniw. Provides integer workspace for NPSOL 
* @param[in] leniw is the dimension of intWorkSpace - must be >=3*n + nclin+ 2*ncnln
* @param[in] realWorkSpace is an array - must be lenw. Provides real workspace for NPSOL
* @param[in] lenw is the dimension of realWorkSpace
* @note all parameters are described in the User's Guide for NPSOL
*/
void NPSOLLoader::npsol(int n, int numLinConstraints, int numNonLinConstraints,
                        int rowMatrixA, int rowJacobian, int rowMatrixR,
                        double matrixA[], double lowerBounds[], double upperBounds[],
                        confunPtrNPSOL confun, objfunPtrNPSOL objfun,
                        int inform, int iter, int istate[],
                        double constraints[], double jacobianConstraints[], double clamda[],
                        double f, double g[], double matrixR[], double x[],
                        int intWorkSpace[], int leniw, double realWorkSpace[], int lenw)
{
  assert(n > 0);
  assert(numLinConstraints >= 0);
  assert(numNonLinConstraints >= 0);
  assert(rowMatrixA >= 1 && rowMatrixA >= numLinConstraints);
  assert(rowJacobian >= 1 && rowJacobian >= numNonLinConstraints);
  assert(rowMatrixR >= n);

  npsolFuncHandle(&n, &numLinConstraints, &numNonLinConstraints,
                  &rowMatrixA, &rowJacobian, &rowMatrixR,
                  matrixA, lowerBounds, upperBounds,
                  confun, objfun,
                  &inform, &iter, istate,
                  constraints, jacobianConstraints, clamda,
                  &f, g, matrixR, x,
                  intWorkSpace, &leniw, realWorkSpace, &lenw);
}

/**
* @brief calls the FORTRAN function NP_OPENFILE from npsol dll
*
* @param fileHandle handle to be used to open the file (will be used in fortran functions
*                   for read and write operations
* @param fileName name of the file to be opened
*/
void NPSOLLoader::npsolOpenFile(const int fileHandle, const std::string fileName)
{
  assert(fileHandle>0); // required by FORTRAN standard
  assert(fileHandle<100); // required by FORTRAN standard
  int fileHandleToFortran = fileHandle; // copied, so we can pass it as constant to wrapper

  char fileNameToFortran[301];
  assert(fileName.size() < 301);

  npsolOpenFileFuncHandle(&fileHandleToFortran, fileName.c_str(),
                          static_cast<int>(fileName.size()));

  //! @todo what happens if the file could not be opened?
}

/**
*  @brief calls the FORTRAN function NP_CLOSEFILE from npsol dll
*
* @param fileHandle handle to the file (used in snOpenfile) to be closed
*/
void NPSOLLoader::npsolCloseFile(const int fileHandle)
{
  int fileHandleToFortran = fileHandle; // otherwise we can't declare fileHandle as const
  npsolCloseFileFuncHandle(&fileHandleToFortran);
}

/**
* @brief calls the FORTRAN function NPFILE from npsol dll
*
* @param fileHandle handle to be used to open the file (will be used in fortran functions
*                   for read and write operations
* @param fileName name of the file to be opened
*/
void NPSOLLoader::npsolReadOptionFile(const int fileHandle, const std::string fileName)
{
  assert(fileHandle>0); // required by FORTRAN standard
  assert(fileHandle<100); // required by FORTRAN standard
  int fileHandleToFortran = fileHandle; // copied, so we can pass it as constant to wrapper
  int outputHandle = 0;

  char fileNameToFortran[301];
  assert(fileName.size() < 301);

  strcpy(fileNameToFortran, fileName.c_str()); //! copied, so we can pass it as constant string to wrapper
  //int stringLength = strlen(fileNameToFortran);
  npsolOpenFile(fileHandleToFortran, fileName);
  npsolOptionFileFuncHandle(&fileHandleToFortran, &outputHandle);
  npsolCloseFile(fileHandleToFortran);

  //! @todo what happens if the file could not be opened?
}
