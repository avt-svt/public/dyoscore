/**
* @file SNOPTWrapper.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* SNOPTWrapper - Part of DyOS                                          \n
* =====================================================================\n
* This file contains the definitions of the member functions of        \n
* SNOPTWrapper according to the UML Diagramm METAESO_8                 \n
* =====================================================================\n
* @author Klaus Stockmann
* @date 12.12.2011
*/


#include "SNOPTWrapper.hpp"
#include "SNOPTLoader.hpp"
#include "OptimizerExceptions.hpp"

#include "cs.h"

#include <algorithm>
#include "SNOPTOptions.hpp"
#include <string>
#include <cassert>
#include "boost/date_time/posix_time/posix_time.hpp"

// initialization of statics. Instead of doing it in every test suite we do it here for all together.
std::weak_ptr<OptimizationProblem> SNOPTWrapper::m_staticOptProb;
std::weak_ptr<Logger> SNOPTWrapper::m_staticLogger;
int SNOPTWrapper::m_summaryFileHandleSNOPT;
std::string SNOPTWrapper::m_summaryFileName;

/**
* @brief constructor: initialize resources which are independent of current problem (e.g. open output files)
*
* @param optMD pointer to an OptimizerMetaData object used for the optimization
* @param genInt pointer to an integrator object used for the optimization
*/
SNOPTWrapper::SNOPTWrapper(const OptimizationProblem::Ptr& optProb, const std::map<std::string, std::string> &optimizerOptions)
{
  boost::filesystem::path pathBoost("input");
  boost::filesystem::create_directories(pathBoost);
  snopt.setLogger(m_logger);
  const char* optimizerOptionsFile = "input/snopt.config";
  std::map<std::string, std::string> matchedOptimizerOptions;
  std::map<std::string, std::string> optimizerOptionsCopy;
  SNOPTOptions validOptions;

  //  if there is not already a snoptoptionsfile a new one is created
  if (!boost::filesystem::exists(optimizerOptionsFile)){
    //set standard options, key needs to be written in lower case
    matchedOptimizerOptions["solution"] = "Yes";
    matchedOptimizerOptions["system information"] = "Yes";
    matchedOptimizerOptions["iterations limit"] = "10000";
    matchedOptimizerOptions["major optimality tolerance"] = "1.0E-6";
    matchedOptimizerOptions["major feasibility tolerance"] = "1.E-6";
    matchedOptimizerOptions["function precision"] = "1.E-8";
    matchedOptimizerOptions["verify level"] = "-1";
    matchedOptimizerOptions["major iterations limit"] = "50";
    matchedOptimizerOptions["minor iterations limit"] = "50";
    matchedOptimizerOptions["jacobian"] = "sparse";
    matchedOptimizerOptions["derivative level"] = "3";
    matchedOptimizerOptions["linesearch tolerance"] = "0.5";
    matchedOptimizerOptions["major step limit"] = "2.0";
    matchedOptimizerOptions["minimize"] = "";

    optimizerOptionsCopy = validOptions.checkOptions(optimizerOptions);
    matchOptimizerOptions(matchedOptimizerOptions, optimizerOptionsCopy);
    optimizerOptionsFile = "input/snopt2.config";
    writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
  }
  else{
    //if options were added in UserInput they need to be matched with existing options in file
    if(optimizerOptions.empty()==false){
      std::vector<std::string> optionsVec(getOptimizerOptions(optimizerOptionsFile));
      matchedOptimizerOptions = validOptions.checkOptions(optionsVec);
      optimizerOptionsCopy = validOptions.checkOptions(optimizerOptions);
      matchOptimizerOptions(matchedOptimizerOptions, optimizerOptionsCopy);
      optimizerOptionsFile = "input/snopt2.config";
      writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
    }
    else{
      std::vector<std::string> optionsVec(getOptimizerOptions(optimizerOptionsFile));
      matchedOptimizerOptions = validOptions.checkOptions(optionsVec);
      optimizerOptionsFile = "input/snopt2.config";
      writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
    }
  }

  std::string specFileSNOPT = optimizerOptionsFile;
  m_specFileHandleSNOPT  = 4;      /* File handler for Specs file, ispecs */
  m_optProb = optProb;

  // specify output files
  const int stdoutFileHandle = 6;

  boost::posix_time::ptime timeStamp(boost::posix_time::second_clock::local_time());
  std::string timeString = boost::posix_time::to_iso_string(timeStamp.time_of_day());

  std::string printFileSNOPT = "snopt";
  //printFileSNOPT += timeString;
  printFileSNOPT += ".out";

  m_printFileHandleSNOPT = 9;      /* File handler for Output file, iprint  */

  // -> will translate to screen output, what about no output -> input option to constructor
  m_summaryFileName = "snopt.log";
  m_summaryFileHandleSNOPT = 10;

  // spec file
 if (m_specFileHandleSNOPT != stdoutFileHandle && m_specFileHandleSNOPT > 0) {
   if ( !boost::filesystem::exists(specFileSNOPT) ) {
     throw NonExistingFileException(specFileSNOPT);
   }
 }

  snopt.snOpenFile(m_specFileHandleSNOPT, specFileSNOPT);

  // print file
  if (m_printFileHandleSNOPT != stdoutFileHandle) { // what about no output (-1 or 0?)
    snopt.snOpenFile(m_printFileHandleSNOPT, printFileSNOPT);
  }

  // summary file
  if (m_summaryFileHandleSNOPT != stdoutFileHandle) { // what about no outpt (-1 or 0?)
    snopt.snOpenFile(m_summaryFileHandleSNOPT, m_summaryFileName);
  }

  // allocate character, integer and real workspaces
  OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();

  const int numConstraints = optProbDim.numLinConstraints + optProbDim.numNonLinConstraints; // =^ n
  const int numOptVars = optProbDim.numOptVars;

  allocateSNOPTWorkspace(m_charWorkspace, m_intWorkspace, m_realWorkspace,
                         numOptVars, numConstraints);

  // call sninit routine
  snopt.snInit(m_printFileHandleSNOPT, m_summaryFileHandleSNOPT,
               m_charWorkspace, m_intWorkspace, m_realWorkspace);

  int optInform = -1; // number of errors while reading options file

  snopt.snSpec(optInform, m_specFileHandleSNOPT,
               m_charWorkspace, m_intWorkspace, m_realWorkspace);

  snopt.snCloseFile(m_specFileHandleSNOPT);

  // if (optInform > 0) { //
  //   assert(1==0); // @todo throw an exception instead
  // }


}

/**
* @brief destructor: free resources allocated in constructor (e.g. close output file)
*/
SNOPTWrapper::~SNOPTWrapper()
{
  int stdoutFileHandle = 6;
  
  if (m_printFileHandleSNOPT != stdoutFileHandle && m_printFileHandleSNOPT > 0) {
    snopt.snCloseFile(m_printFileHandleSNOPT);
  }

  if (m_summaryFileHandleSNOPT != stdoutFileHandle && m_printFileHandleSNOPT > 0) {
    snopt.snCloseFile(m_summaryFileHandleSNOPT);
  }
}

void SNOPTWrapper::logSNOPTOutput(Logger::Ptr &logger)
{
  //capture the printed SNOPT standard output
  SNOPTLoader snopt;
  snopt.snCloseFile(m_summaryFileHandleSNOPT);
  std::fstream fileReader(m_summaryFileName.c_str(), ios_base::in);
  std::string content;
  
  bool firstline = true;
  while(!fileReader.eof()){
    if(!firstline){
      logger->newline(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY);
    }
    firstline = false;
    std::getline(fileReader, content);
    logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY,
                              content);
    
  }
  fileReader.close();
  
  // output sent to logger should not be read again
  // by opening the file for writing the content is deleted
  std::fstream deleteContent(m_summaryFileName.c_str(), ios_base::out);
  deleteContent.close();
  snopt.snOpenFile(m_summaryFileHandleSNOPT, m_summaryFileName);
}

/**
* @brief solve optimization problem
*/
DyosOutput::OptimizerOutput SNOPTWrapper::solve()
{
  // set static data object to current attributes - needed by callback functions
  m_staticOptProb = this->m_optProb;
  m_staticLogger = this->m_logger;

  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();


  const int numConstraints = optProbDim.numLinConstraints + optProbDim.numNonLinConstraints; // =^ m
  const int numOptVars = optProbDim.numOptVars; // =^ n

  const int numNonLinConstraints = optProbDim.numNonLinConstraints; // =^ nnCon

  char probName[] = "--DyOS--"; // not really required(may be blank), only for output purposes

  // assemble Jacobian, (non-linear and linear)
  std::vector<double> jacValsDummy;
  std::vector<int> jacRowIndices, jacColPointer;

  assembleJacobian(jacValsDummy, jacRowIndices, jacColPointer);

  const int nnzA = jacValsDummy.size(); // =^ne


  // lower and upper bounds of variables AND slacks (x,s)
  std::vector<double> upperBoundsOnVarsAndSlacks(numOptVars+numConstraints); // =^ bu
  std::vector<double> lowerBoundsOnVarsAndSlacks(numOptVars+numConstraints); // =^ bl
  assembleBounds(lowerBoundsOnVarsAndSlacks, upperBoundsOnVarsAndSlacks);


  const unsigned n = numOptVars + numConstraints;

  utils::Array<double> optVarAndSlackValues(n, 0.0);
  utils::WrappingArray<double> optVarValues(numOptVars, optVarAndSlackValues.getData());
  m_optProb->getDecVarValues(optVarValues);

  utils::Array<double> lagrangeMultipliers(numConstraints, 0.0); // =^pi
  utils::WrappingArray<double> nonLinLagrangeMultipliers(optProbDim.numNonLinConstraints,
                                                         lagrangeMultipliers.getData());
  utils::WrappingArray<double> linLagrangeMultipliers(optProbDim.numLinConstraints,
                                                      lagrangeMultipliers.getData()
                                                    + optProbDim.numNonLinConstraints);
  m_optProb->getLagrangeMultipliers(nonLinLagrangeMultipliers,
                                  linLagrangeMultipliers);

  int inform; // only output

  int numInfeas;  // =^ nInf, output
  double sumInfeas; // =^ sInf, output
  double objFunValAtSolution; // =^ Obj, output

  //initialize hs with 0 (cold start without basis file)
  utils::Array<int> hs(n, 0);

  utils::Array<double> rc(n, 0.0);

  if(nnzA == 0){
    throw NoJacobianForSNOPTException();
  }
  assert(!jacValsDummy.empty());
  assert(!jacRowIndices.empty());
  assert(!jacColPointer.empty());
  assert(!lowerBoundsOnVarsAndSlacks.empty());
  assert(!upperBoundsOnVarsAndSlacks.empty());
  // these variables have to be either 1) hard-coded or
  //                                   2) retrieved from OptimizerSingleStageMetaData
  
  snopt.snopt(numConstraints,
              numOptVars,
              nnzA,
              numNonLinConstraints,
              probName,
              &SNOPTWrapper::confun, // not required, function names do not change
              &SNOPTWrapper::objfun, // not required, function names do not change
              &jacValsDummy[0], &jacRowIndices[0], &jacColPointer[0],
              &lowerBoundsOnVarsAndSlacks[0], &upperBoundsOnVarsAndSlacks[0],
              hs.getData(),
              optVarAndSlackValues.getData(),
              lagrangeMultipliers.getData(),
              rc.getData(),
              inform,
              numInfeas,
              sumInfeas,
              objFunValAtSolution,
              m_charWorkspace,
              m_intWorkspace,
              m_realWorkspace);
  
  logSNOPTOutput(m_logger);
  
  // put output (objective function value, values of decision variables, Lagrange
  // multiplier, etc.) back to DyOS structs

  // adca01: as I understad what SNOPT does, the final iteration is the solution, so there is no need for further intergration which would be done be setting the values.
  //utils::WrappingArray<const double> optVarValuesOut(numOptVars, optVarAndSlackValues.getData());
  //m_optProb->setDecVarValues(optVarValuesOut);


  // snopt returns Lagrange multipliers with inverted sign (with respect to the Honeybee standard)
  // this causes adjoints to be wrong (not just with inverted sign) - so invert here Lagrange
  // multipliers before they are set to meta data
  for(unsigned i=0; i<nonLinLagrangeMultipliers.getSize(); i++){
    nonLinLagrangeMultipliers[i] *= -1.0;
  }
  for(unsigned i=0; i<linLagrangeMultipliers.getSize(); i++){
    linLagrangeMultipliers[i] *= -1.0;
  }
  m_optProb->setLagrangeMultipliers(nonLinLagrangeMultipliers,
                                    linLagrangeMultipliers);
  // set multipliers of active constraints to zero
  utils::WrappingArray<double> optVarLagrangeMultiplier(numOptVars,
                                                        rc.getData());
  for(unsigned i=0; i < optVarLagrangeMultiplier.getSize(); i++){
    // for consistency invert sign of opt var Lagrange multipliers as well
    optVarLagrangeMultiplier[i] *= -1.0;
    if(hs[i] > 1){
      optVarLagrangeMultiplier[i] = 0.0;
    }
  }
  m_optProb->setDecVarLagrangeMultipliers(optVarLagrangeMultiplier);
  // return output
  // TOO_MANY_ITERATIONS WRONG_GRADIENTS NOT_OPTIMAL OK FAILED
  DyosOutput::OptimizerOutput  optOut;
  switch(inform){
    case 0:
    case 1:
    case 2:
      optOut.m_informFlag = DyosOutput::OptimizerOutput::OK;
      break;
    case 3:
    case 41:
      optOut.m_informFlag = DyosOutput::OptimizerOutput::NOT_OPTIMAL;
      break;
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 43:
      optOut.m_informFlag = DyosOutput::OptimizerOutput::INFEASIBLE;
      break;
    case 30:
    case 31:
    case 32:
    case 33:
      optOut.m_informFlag = DyosOutput::OptimizerOutput::TOO_MANY_ITERATIONS;
      break;
    case 51:
    case 52:
      optOut.m_informFlag = DyosOutput::OptimizerOutput::WRONG_GRADIENTS;
      break;
    default:
      optOut.m_informFlag = DyosOutput::OptimizerOutput::FAILED;
  }

  getOptimizerOutput(optOut);
  
  return optOut;
}

/**
  * @brief static callback function used by the SNOPT library to set  the value
  * and derivatives of the objective function
  *
  * @param[in,out] mode  on input: indicates whether fobj, gobj or both must be
  *                                assigned during the present call of funobj (0<=mode<=2).\n
  *                                -if mode=2, assign fobj and known components of gobj\n
  *                                -if mode=1, assign the known components of gobj; fobj is ignored\n
  *                                -if mode=0, only fobj needs to be assigned; gobj is ignored\n
  *                      on exit:  if mode = -1: indicates, that fobj has not been computed (for any reason)
  *                                if mode < -1: tell to SNOPT to terminate optimization
  *
  * @param[in] nnobj number of variables involved in the objective function
  *                  (the first nnobj variables in vector x)
  * @param[in] x contains the nonlinear objective variables x
  * @param[out] fobj must contain computed value of the objective function, except for mode=1
  * @param[out] gobj must contain the known components of the gradient vector of the objective
  *                  function , except for mode=0
  * @param[in] nstate indicates the first and last calls to objfun\n
  *                   -if nstate=0, nothing special about the current call\n
  *                   -if nstate=1, SNOPT is calling the subroutine for the first time.\n
  *                   -if nstate>=2 SNOPT is calling the subroutine for the last time.\n
  *                    Note that confun will always be called before objfun, if there are any
  *                    nonlinear constraints.\n
  *                    At the last call nstate = 2+inform.
  * @param[in,out] cu character array of user data
  * @param[in] flencu length of the field cu (needed for certain fortran builds)
  * @param[in] lencu length of the field cu
  * @param[in,out] iu integer array of user data
  * @param[in] lencu length of the field iu
  * @param[in,out] ru double field of user data
  * @param[in] lenru length of field ru
  */
void SNOPTWrapper::objfun(int *mode, // input, output
                          int *nnobj,
                          double *x,
                          double *fobj, // output
                          double *gobj, // output
                          int *nstate,
                          char *cu, int *lencu, // character workspace; not used by DyOS -> into wrapper
                          int *iu, int *leniu,              // integer workspace; not used by DyOS   -> into wrapper
                          double *ru, int *lenru,           // real workspace; not used by DyOS      -> into wrapper
                          int flencu)
{
  assert(*mode == 0 || *mode == 1 || *mode == 2);
  assert(*nstate >= 0);
  assert(*nnobj > 0);

  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();
  
  logSNOPTOutput(staticLogger);

  // put current values of optimization variables to OptimizerSingleStageMetaData
  utils::WrappingArray<const double> dummy_x(*nnobj, x);

  if(staticOptProb->getOptProbDim().numNonLinConstraints == 0){
    // we only need to evaluate the decision variables, when there are not constraints.
    // Because snopt doesn't change the decision variables between confun and objfun.
    // Also confun is always called first.
    staticOptProb->setDecVarValues(dummy_x);
  }

  OptimizationProblem::ResultFlag result;
  // get objective function value
  if(*mode==0 || *mode==2){
    result = staticOptProb->getNonLinObjValue(*fobj);
    if(result == OptimizationProblem::FAILED){
      *mode = -1;
    }
  }

  // store the gradient at current point x into gObj
  if(*mode==1||*mode==2){
    utils::WrappingArray<double> dummy_gobj(*nnobj, gobj);
    result = staticOptProb->getNonLinObjDerivative(dummy_gobj);
    if(result == OptimizationProblem::FAILED){
      *mode = -1;
    }
  }

  // print status message
  if(*nstate>=2){
    
    staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "DyOS: Finished optimization with SNOPT\n" );
  }
}

/**
  *@brief static callback function used by the SNOPT library to set value vector and Jacobian
  *of the nonlinear constraints.
  *
  * @param[in] mode indicates whether fobj, gobj or both must be assigned during the present
  *            call of funobj (0<=mode<=2).\n
  *            -if mode=2, assign fobj and known components of gobj\n
  *            -if mode=1, assign the known components of gobj, fobj is ignored\n
  *            -if mode=0, only fobj needs to be assigned, gobj is ignored\n
  *            May be used to indicate that in this method the objective function
  *            is not set for any reason by setting mode to -1. If the solution of the
  *            current problem is to be terminated, set mode to any other negative value.
  * @param[in] nncon number if nonlinear constraints. These must be the first nnCon constraints
  *                  in the problem.
  * @param[in] nnjac number of nonlinear variables in the constraint matrix. These must be the
  *                  first nnJac variables in the problem
  * @param[in] nejac number of Jacobian entries in the nonlinear Jacobian matrix (nncon*nnjac)
  * @param[in] x contains the current values of the nonlinear variables in the constraint matrix.
  * @param[out] fcon contains the computed constraint vector , except for mode=1
  * @param[out] gcon contains computed Jacobian, except  for mode=0. These gradient elements
  *                  must be stored in gCon in exactly the same positions as implied by the
  *                 definitions of SNOPT arrays a, ha, ka. There is no internal check for consistency.
  * @param[in] nstate indicates the first and last calls to objfun\n
  *                   -if nstate=0, nothing special about the current call\n
  *                   -if nstate=1, SNOPT is calling the subroutine for the first time.\n
  *                   -if nstate>=2 SNOPT is calling the subroutine for the last time.\n
  *                    Note that confun will always be called before objfun, if there are any
  *                    nonlinear constraints.\n
  *                    At the last call nstate has generally the value of 2+inform.
  * @param[in,out] cu character array of user data
  * @param[in] flencu length of the field cu (needed for certain fortran builds
  * @param[in] lencu length of the field cu
  * @param[in,out] iu integer array of user data
  * @param[in] lencu length of the field iu
  * @param[in,out] ru double field of user data
  * @param[in] lenru length of field ru
  */
void SNOPTWrapper::confun(int *mode,
                          int *nncon,
                          int *nnjac,
                          int *nejac,
                          double *x,
                          double *fcon,
                          double *gcon,
                          int *nstate,
                          char *cu, int *lencu, // character workspace; not used by DyOS -> into wrapper
                          int *iu, int *leniu,              // integer workspace; not used by DyOS   -> into wrapper
                          double *ru, int *lenru,            // real workspace; not used by DyOS      -> into wrapper
                          int flencu)
{
  assert(*mode == 0 || *mode == 1 || *mode == 2);
  assert(*nstate >= 0);
  assert(*nnjac > 0);

  //OptimizerMetaDataPtr staticOptMD = m_staticOptMD.lock();
  //GenericIntegratorPtr staticGenInt = m_staticGenInt.lock();
  OptimizationProblem::Ptr staticOptProb = m_staticOptProb.lock();
  Logger::Ptr staticLogger = m_staticLogger.lock();
  
  logSNOPTOutput(staticLogger);

  // print status message (why here and not in objfun ?
  if(*nstate == 1){  /* first call of confun_snopt*/
    staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "DyOS: Starting optimization with SNOPT\n");
  }
  
  OptimizationProblem::ResultFlag result;
  // put current values of optimization variables to OptimizerSingleStageMetaData
  utils::WrappingArray<const double> dummy_x(*nnjac, x);

  staticOptProb->setDecVarValues(dummy_x);

  // what about *mode == 1 ?
  // get values of fcon
  utils::WrappingArray<double> dummy_fcon(*nncon, fcon);
  result = staticOptProb->getNonLinConstraintValues(dummy_fcon);
  if(result == OptimizationProblem::FAILED){
    staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "integration failed\n" );
    *mode = -1;
  }

  // get values of gcon
  // what about linear constraints
  if (*mode == 2) {
    CsTripletMatrix::Ptr jacobianMatrixStruct = staticOptProb->getOptProbDim().nonLinJacDecVar;
    double *values = new double[*nejac];
    CsTripletMatrix::Ptr constDerMatrix = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(jacobianMatrixStruct->get_n_rows(),
                                                                                         jacobianMatrixStruct->get_n_cols(),
                                                                                         jacobianMatrixStruct->get_nnz(),
                                                                                         jacobianMatrixStruct->get_matrix_ptr()->i,
                                                                                         jacobianMatrixStruct->get_matrix_ptr()->p,
                                                                                         values));

    assert(constDerMatrix->get_nnz() == *nejac);
    result = staticOptProb->getNonLinConstraintDerivativesDecVar(constDerMatrix);
    if(result == OptimizationProblem::FAILED){
      staticLogger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "integration failed\n");
      *mode = -1;
    }
    CsCscMatrix::Ptr compressedMatrix = constDerMatrix->compress();
    for(int i=0; i<*nejac; i++){
      gcon[i] = compressedMatrix->get_matrix_ptr()->x[i];
    }
    delete []values;
  }
}



// utility functions

/*
* @brief assemble Jacobian matrix for SNOPT in CSC format
*
* assemble complete Jacobian matrix from linear and nonlinear parts; value
* vector is only a dummy but allocated to the correct size
*
* @param[out] jacValsDummy dummy ExtendableArray for Jacobian values
* @param[out] jacRowIndices ExtendableArray containing row indices of Jacobian
* @param[out] jacColPointer ExtendableArray containing column pointers
* @ remarks if optProbDim.nonLinJac was in csc format we could directly pass the
*   rowind, colptr and val vector to SNOPT
*/
void SNOPTWrapper::assembleJacobian(std::vector<double> &jacValsDummy,
                                    std::vector<int> &jacRowIndices,
                                    std::vector<int> &jacColPointer) const
{
  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();

  // nonlinear Jacobian
  // Jacobian of constraints in compressed sparse column (CSC) format (only
  // structural data, value vector is NULL pointer)
  CsTripletMatrix::Ptr nonLinJacCoo = optProbDim.nonLinJacDecVar;
  CsTripletMatrix::Ptr linJacCoo = optProbDim.linJac;

  //write nonLinJacCoo and linJacCoo into same tripletMatrix
  CsTripletMatrix::Ptr assembledTriplet = nonLinJacCoo->vertcat(linJacCoo);
  CsCscMatrix::Ptr assembledCsc = assembledTriplet->compress();
   
  int nnz = assembledCsc->get_nnz();
  jacValsDummy.resize(nnz, 1.2); // dummy array
  jacRowIndices.resize(nnz, 0);
  jacColPointer.resize((assembledCsc->get_n_cols())+1, 0);
  
  for (int i=0; i < nnz; i++) {
    jacRowIndices[i] = assembledCsc->get_matrix_ptr()->i[i] + 1; // in fortran indices have offset 1
    jacValsDummy[i] = assembledCsc->get_matrix_ptr()->x[i];
  }
    // note that values of the linear constraints should be set as well
    // otherwise the problem will be infeasible in SNOPT
  for(int j=0; j<assembledCsc->get_n_cols()+1; j++){
    jacColPointer[j] = assembledCsc->get_matrix_ptr()->p[j] + 1; // in fortran indices have offset 1
  }
}

/*
 *@brief assemble upper and lower bounds of variables and slacks
 *
 *@param[out] lowerBounds Array containing lower bounds for optimization variables and slack variables
 *@param[out] upperBounds Array containing upper bounds for optimization variables and slack variables
 */
void SNOPTWrapper::assembleBounds(std::vector<double> &lowerBoundsOnVarsAndSlacks,
                                  std::vector<double> &upperBoundsOnVarsAndSlacks) const
{

  const OptimizerProblemDimensions optProbDim = m_optProb->getOptProbDim();

  const int numOptVars = optProbDim.numOptVars;

  // get bounds on variables
  utils::Array<double> upperBounds(numOptVars), lowerBounds(numOptVars); // only variables
  m_optProb->getDecVarBounds(lowerBounds, upperBounds);
  for (int i=0; i<numOptVars; i++) {
    assert(lowerBounds[i] <= upperBounds[i]);
  }

  // get bounds on slacks
  utils::Array<double> nonLinConstraintLowerBounds(optProbDim.numNonLinConstraints),
                       nonLinConstraintUpperBounds(optProbDim.numNonLinConstraints);
  m_optProb->getNonLinConstraintBounds(nonLinConstraintLowerBounds, nonLinConstraintUpperBounds);

  utils::Array<double> linConstraintLowerBounds(optProbDim.numLinConstraints),
                       linConstraintUpperBounds(optProbDim.numLinConstraints);
  m_optProb->getLinConstraintBounds(linConstraintLowerBounds, linConstraintUpperBounds);

  // add bounds on variables and on slacks
  for (int i=0; i<numOptVars; i++) {
    lowerBoundsOnVarsAndSlacks[i] = lowerBounds[i];
    upperBoundsOnVarsAndSlacks[i] = upperBounds[i];
  }

  int offset = numOptVars;
  for (unsigned i=0; i<optProbDim.numNonLinConstraints ; i++) {
    lowerBoundsOnVarsAndSlacks[i+offset] = nonLinConstraintLowerBounds[i];
    upperBoundsOnVarsAndSlacks[i+offset] = nonLinConstraintUpperBounds[i];
  }

  offset += optProbDim.numNonLinConstraints;
  for (unsigned i=0; i<optProbDim.numLinConstraints ; i++) {
    lowerBoundsOnVarsAndSlacks[i+offset] = linConstraintLowerBounds[i];
    upperBoundsOnVarsAndSlacks[i+offset] = linConstraintUpperBounds[i];
  }
}

/*
 *@brief allocate workspace required by SNOPT
 *
 *@param[out] cw character workspace required by SNOPT
 *@param[out] iw integer workspace required by SNOPT
 *@param[out] rw real workspace required by SNOPT
 *@param[in] numVars number of optimization variables
 *@param[in] numConstraints number of constraints
 */
void SNOPTWrapper::allocateSNOPTWorkspace(      std::vector<char> &cw,
                                                std::vector<int> &iw,
                                                std::vector<double> &rw,
                                          const int numVars,
                                          const int numConstraints) const
{
  assert(numVars>0);

   /* numbers according to SNOPT manual */
  const int lencw = 5000; /* 550; */
  int leniw = 1100*(numConstraints + numVars);
  const int lenrw = max(2200*(numConstraints + numVars), 500);
  if (leniw <= 550) leniw = 550; /* Assure minimum allocation */

  //information vectors for SNOPT
  const int snoptStringLength = 8;
  cw.resize(lencw * snoptStringLength);
  rw.resize(lenrw, 0.0);
  iw.resize(leniw, 0);
}

/**
* @brief check if option exists and if input for option is valid. write options into file
*
* @param filename, map<string,string> with optimizerOptions
*/
//void SNOPTWrapper::checkOptimizerOptions(const char* filename,
//                                         const std::map<std::string, std::string> &optimizerOptions)
//{
//  //note: in this function not all possible options are checked but only the most common ones
//  ofstream myfile;
//  myfile.open (filename);
//  myfile << "Begin options\n";
//
//  std::map<std::string,std::string>::const_iterator iter;
//  for(iter=optimizerOptions.begin(); iter!=optimizerOptions.end(); iter++){
//    if(boost::iequals((*iter).first, "minimize")){
//      myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "maximize")){
//      myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "solution")){
//      if(boost::iequals((*iter).second, "no"))
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//      else if(boost::iequals((*iter).second, "yes"))
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//      else
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//    }
//    else if(boost::iequals((*iter).first, "jacobian")){
//      if(boost::iequals((*iter).second, "sparse"))
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//      else if(boost::iequals((*iter).second, "dense"))
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//      else
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//    }
//    else if(boost::iequals((*iter).first, "system information")){
//      if(boost::iequals((*iter).second, "yes"))
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//      else if(boost::iequals((*iter).second, "no"))
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//      else
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//    }
//    else if(boost::iequals((*iter).first, "iterations limit")){
//      if((number((*iter).second)).first==INTEGER || (number((*iter).second)).second>0)//needs to be integer >0
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//      else
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//    }
//    else if(boost::iequals((*iter).first, "major optimality tolerance")){
//      if((number((*iter).second)).first==NO_NUMBER || (number((*iter).second)).second<=0)//needs to be double >0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "major feasibility tolerance")){
//      if((number((*iter).second)).first==NO_NUMBER || (number((*iter).second)).second<=0)//needs to be double >0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "function precision")){
//      if((number((*iter).second)).first==NO_NUMBER || (number((*iter).second)).second<=0)//needs to be doubl >0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "verify level")){
//      if((number((*iter).second)).first!=INTEGER)//needs to be an integer
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "major iterations limit")){
//      if((number((*iter).second)).first!=INTEGER || (number((*iter).second)).second<0)//needs to be integer and >=0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "minor iterations limit")){
//      if((number((*iter).second)).first!=INTEGER || (number((*iter).second)).second<=0)//needs to be integer and >0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "derivative level")){
//      if((number((*iter).second)).first!=INTEGER || (number((*iter).second)).second<0)//needs to be integer and >=0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "linesearch tolerance")){
//      if((number((*iter).second)).first==NO_NUMBER || (number((*iter).second)).second<=0)//needs to be integer and >=0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else if(boost::iequals((*iter).first, "major step limit")){
//      if((number((*iter).second)).first==NO_NUMBER || (number((*iter).second)).second<=0)//needs to be double and >=0
//        throw FalseOptionInputException((*iter).second, (*iter).first);
//      else
//        myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
//    }
//    else
//      throw FalseOptionNameException((*iter).first, "SNOPT");
//  }
//  myfile << "End options\n";
//  myfile.close();
//}
