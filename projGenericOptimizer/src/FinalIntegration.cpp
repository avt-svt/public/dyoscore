/**
* @file FinalIntegration.cpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* GenericOptimizer - Part of DyOS                                         \n
* ========================================================================\n
* This file contains the member definitions of the FinalIntegration class \n
* ========================================================================\n
* @author Tjalf Hoffmann
* @date 09.01.2013
*/

#include "FinalIntegration.hpp"

FinalIntegration::FinalIntegration(const OptimizationProblem::Ptr& optProb)
{
  m_optProb = optProb;
}

FinalIntegration::~FinalIntegration()
{}

/**
* @brief make a simulation with an optimization result
*/
DyosOutput::OptimizerOutput FinalIntegration::solve()
{
  DyosOutput::OptimizerOutput  optOut;
  m_optProb->setObjectiveLagrangeMultiplier(1.0);
  m_optProb->doFinalIntegration();
  getOptimizerOutput(optOut);
  
  return optOut;
}