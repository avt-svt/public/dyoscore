/**
* @file NPSOLWrapper.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* NPSOLWrapper - Part of DyOS                                             \n
* ========================================================================\n
* This file contains the class definitions of the NPSOLWrapper class      \n
* ========================================================================\n
* @author Mathias Dunst
* @date 31.10.2012
*/
#pragma once

#include<vector>
#include <boost/weak_ptr.hpp>
#include <memory>

#include "GenericOptimizerWithOptimizationProblem.hpp"
#include "NPSOLLoader.hpp"
#include "NPSOLOptions.hpp"

/**
* @class NPSOLWrapper
*
* @brief Class defining a GenericOptimizer
*/
class NPSOLWrapper : public GenericOptimizerWithOptimizationProblem
{
protected:
  static std::weak_ptr<OptimizationProblem> m_staticOptProb;
  static std::weak_ptr<Logger> m_staticLogger;

  std::vector<int> m_intWorkspace;
  std::vector<double> m_realWorkspace;

  // Fortran file handles: <=0 : no output
  //                         6 : stdout
  //                       all other values: filename specified by user
  // handles of output files
  int m_printFileHandleNPSOL;
  static int m_summaryFileHandleNPSOL;
  static std::string m_summaryFileName;
  int m_OptionFileHandleNPSOL;

  NPSOLLoader npsol;

  static void objfun(int *mode, int *n, double *x, double *f, double *g, int *nstate);
  static void confun(int *mode, int *ncnln, int *n, int *ldJ, int *needc,
                     double *x, double *c, double *cJac, int *nstate);

  //utility functions
  static void logNPSOLOutput(Logger::Ptr &logger);
  
  void assembleLinConstraintsMatrix(std::vector<double> &linConstraintsMatrix, int &rowDim) const;

  void assembleBounds(std::vector<double> &lowewrBounds,
                      std::vector<double> &upperBounds) const;

  void assembleJacobian(std::vector<double> &jacVal , int &rowDim) const;

  void allocateNPSOLWorkspace(int &lenInt, int &lenDouble,
                              const int numOpt, const int numLinCon, const int numNonLinCon);

public:
  NPSOLWrapper(const OptimizationProblem::Ptr& optProb,
               const std::map<std::string, std::string> &optimizerOptions = std::map<std::string,std::string>());
  ~NPSOLWrapper();
  DyosOutput::OptimizerOutput solve();
};