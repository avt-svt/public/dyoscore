/**
* @file MultiScenarioOptProblem.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* Class definition of MultiScenarioSet                                 \n
* =====================================================================\n
* @author Hans Prinay, Tjalf Hoffmann
* @date 14.06.2013
*/


#pragma once

#include <map>

#include "OptimizationProblem.hpp"

typedef std::vector<OptimizationProblem::Ptr> ProblemVector;

/**
 * @brief Implements a set of scenarios and a mapping for the variables between
 *        the single scenarios and the full problem
 */
class MultiScenarioSet
{
private:
  bool m_is_initialized;
  ProblemVector m_scenarios;
  /** if true, the variable at scenario i, index j is a shared variable */
  //std::vector< std::vector<bool> > m_is_shared_maps;
  /** for a given scenario i and variable index j, returns the shared variable index.
   *  if the given variable is not a shared variable, returns -1.*/
  std::vector< std::vector<int> > m_scenario_to_shared_maps;
  /** for a given scenario and shared variable index, returns the variable
   *  index w.r.t this scenario. If the given shared variable is not in this
   *  scenario, returns -1.*/
  std::vector< std::vector<int> > m_shared_to_scenario_maps;
  /** The full variable vector index for a given scenario and scenario-variable-index.
   *  is -1, if the given scenario-variable-index is a shared variable. */
  std::vector< std::vector<int> > m_nonshared_maps;
  /** of length n_non_shared, this vector returns the scenario index */
  std::vector< std::pair<int, int> > m_full_map;
  int n_non_shared;

  std::vector< std::pair<int,int> > m_lincon_full_to_scenario;
  std::vector< std::vector<int> > m_lincon_scenario_to_full;
  std::vector< std::pair<int,int> > m_nonlincon_full_to_scenario;
  std::vector< std::vector<int> > m_nonlincon_scenario_to_full;
  /** maps a scenario and a jacobian index to a full jacobian index (triplet format?)*/
  std::vector< std::vector<int> > m_linjac_scenario_to_full;
  std::vector< std::vector<int> > m_nonlinjac_scenario_to_full;
public:
  typedef boost::shared_ptr<MultiScenarioSet> Ptr;
  int get_full_index(const int scenario, const int idx) const;
  /** returns true, if the reduced index can be determined.
   *  if returns false, the index is not unique, e.g. it is a shared variable.
   *  In that case, the index of that shared variable given in the scenario variable is returned.
   */
  bool get_reduced_index(const int full_idx, int& scenario, int& idx) const;
  int get_full_lincon_index(const int scenario, const int idx) const;
  void get_reduced_lincon_index(const int full_idx, int& scenario, int& idx) const;
  int get_full_nonlincon_index(const int scenario, const int idx) const;
  void get_reduced_nonlincon_index(const int full_idx, int& scenario, int& idx) const;
  int get_full_lin_nnz(const int scenario, const int nnz_idx) const;
  int get_full_nonlin_nnz(const int scenario, const int nnz_idx) const;
  void addScenario(const OptimizationProblem::Ptr& opt_problem, const std::vector<int>& shared);
  int getNumLinConstraints() const {return m_lincon_full_to_scenario.size();}
  int getNumNonLinConstraints() const {return m_nonlincon_full_to_scenario.size();}
  int getNumOptVars() const {return n_non_shared + m_shared_to_scenario_maps.begin()->size();}
  /** Get number of nonzero entries in the full linear constraint matrix */
  int getLinNonZeroes() const;
  /** Get number of nonzero entries in the full non-linear constraint matrix */
  int getNonLinNonzeroes() const;
  /** returns true, if a full variable vector index belongs to the set of shared variables */
  bool is_shared_idx(int full_idx) const;

  void initialize();

  ProblemVector::const_iterator begin() const {return m_scenarios.begin();}
  ProblemVector::const_iterator end() const {return m_scenarios.end();}
  unsigned size() const {return m_scenarios.size();}
  const OptimizationProblem::Ptr& operator[](int i) {return m_scenarios[i];}
};

/**
 * This class implements an optimization problem, which is composed of a number
 * of similar optimization problems which only differ in their parameters.
 *
 * This structure is often encountered in robust and stochastic optimization.
 * For these problems, all constraints of all subproblems have to be observed
 * at all times. The main difference lies in their objective function: Either
 * it is a weighted sum of all objective functions (stochastic optimization) or
 * the maximum (robust optimization).
 */
class MultiScenarioOptProblem : public OptimizationProblem
{
public:
  typedef boost::shared_ptr<MultiScenarioOptProblem> Ptr;

  /// set up a multiscenario problem with n_common common variables that appear in each scenario.
  //MultiScenarioOptProblem(const std::vector<int>& shared_variables) {}
  MultiScenarioOptProblem(const MultiScenarioSet::Ptr& set);
  virtual ~MultiScenarioOptProblem();

  /// the shared variables need to be always the same variables in this order
  /// the other variables can be anything.
  //void addScenario(const OptimizationProblem::Ptr& scenario);

  virtual const OptimizerProblemDimensions &getOptProbDim() const;
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const;
  virtual void getDecVarValues(utils::Array<double> &values) const;
  virtual void setDecVarValues(const utils::Array<const double> &decVars);
  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const;
  virtual OptimizationProblem::ResultFlag getNonLinConstraintValues(utils::Array<double> &values) const;
  virtual OptimizationProblem::ResultFlag getNonLinConstraintDerivativesDecVar
                                          (CsTripletMatrix::Ptr derivativeMatrix) const;
  virtual OptimizationProblem::ResultFlag getNonLinConstraintDerivativesConstParam
                                          (CsTripletMatrix::Ptr derivativeMatrix) const;
  virtual OptimizationProblem::ResultFlag getNonLinObjValue(double &objValue) const;
  virtual OptimizationProblem::ResultFlag getNonLinObjDerivative(utils::Array<double> &values) const;
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers);
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const;
  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier);
  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier);
  virtual OptimizationProblem::ResultFlag getLinConstraintValues(utils::Array<double> &values) const;
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds) const;
  virtual OptimizationProblem::ResultFlag getNonLinConstraintHessianDecVar
                                (std::vector<std::vector<double> > &hessianMatrix);
  virtual OptimizationProblem::ResultFlag getNonLinConstraintHessianConstParam
                                (std::vector<std::vector<double> > &hessianMatrix);
  virtual OptimizationProblem::ResultFlag getAdjointsDecVar
                                (std::vector<double> &adjoints1stOrder,
                                 std::vector<std::vector<double> > &adjoints2ndOrder);
  virtual OptimizationProblem::ResultFlag getAdjointsConstParam
                                    (std::vector<double> &adjoints1stOrder,
                                     std::vector<std::vector<double> > &adjoints2ndOrder);
  virtual std::vector<DyosOutput::StageOutput> getOutput();
private:
  void initialize();
  MultiScenarioSet::Ptr m_set;
  OptimizerProblemDimensions m_opt_dim;
  /* map from shared variable index to full variable vector index */
  std::vector<int> m_shared;
  /* map from full vector index to shared variable index (inverts previous vector)*/
  std::map<int,int> m_shared_idx;

  /** Initialize number of variables, number of constraints */
  void initialize_problem_dimensions();
  /** Initialize nonzero structure of sparse matrices */
  void initialize_matrices();
};

class MultiScenarioException : std::exception
{
private:
  std::string m_msg;
public:
  MultiScenarioException(std::string msg) : m_msg(std::string("Error in MultiScenarioOptProblem: ") + msg) {}
  virtual ~MultiScenarioException() throw() {}
  virtual const char* what() const throw() { return m_msg.c_str();}
};
