#pragma once
#include "GenericOptimizerWithOptimizationProblem.hpp"
#include "OptimizationProblem.hpp"
#include <map>

class IpoptWrapper : public GenericOptimizerWithOptimizationProblem {
protected:
  std::map<std::string, std::string> m_ipoptOptions;
public:
  IpoptWrapper(const OptimizationProblem::Ptr& optProb,
               const  std::map<std::string,std::string> &ipoptOptions 
                      = std::map<std::string,std::string>())
  {
    m_optProb = optProb;
    m_ipoptOptions = ipoptOptions;
  }

  virtual ~IpoptWrapper() {}

  virtual DyosOutput::OptimizerOutput solve();
  
};
