/**
* @file OptimizerExceptions.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Optimizer                                                            \n
* =====================================================================\n
* Definitions of all exceptions that are thrown by the optimizer module\n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski
* @date 02.02.2013
*/

#pragma once
#include <exception>
#include <string>


class OptimizerException : public std::exception {
  std::string m_msg;
public:
  OptimizerException(const std::string& msg) : m_msg("OptimizerException: " + msg) {}
  virtual ~OptimizerException() throw() {}
  virtual const char* what() const throw() {
    return m_msg.c_str();
  }
};

class NonExistingFileException : public OptimizerException
{
public:
  NonExistingFileException(std::string fileName)
    : OptimizerException("A file named "+fileName+" does not exist.")
  {
  }
};

//file is probably obsolete

//class FalseOptionNameException : public InputException
//{
//public:
//  FalseOptionNameException(std::string optionName, std::string optimizerName)
//    : InputException("An option named "+optionName+" does not exist for "+optimizerName)
//  {
//  }
//};
//
//class FalseOptionInputException : public InputException
//{
//public:
//  FalseOptionInputException(std::string input, std::string optionName)
//    : InputException(input+" is no valid input for option "+optionName)
//  {
//  }
//};

class NoJacobianForSNOPTException : public OptimizerException
{
public:
  NoJacobianForSNOPTException() : OptimizerException("Defined constraints do not have parameters "
                                                     "with sensitivity information on that"
                                                     " timepoint. To fix this, set senstype of"
                                                     " parameters from NO to FRACTIONAL"){}
};