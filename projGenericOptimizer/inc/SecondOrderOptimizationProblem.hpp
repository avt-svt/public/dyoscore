/**
* @file SecondOrderOptimizationProblem.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* This file contains OptimizationProblem class for 2nd order           \n
* Optimization problem                                                 \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 14.09.2012
*/

#pragma once

#include "FirstOrderOptimizationProblem.hpp"
#include "GenericIntegratorReverse.hpp"
#include "OptimizerMetaData.hpp"

class SecondOrderOptimizationProblem : public FirstOrderOptimizationProblem
{
protected:
  OptimizerMetaData2ndOrder::Ptr m_opt2ndOrder;
  GenericIntegratorReverse::Ptr m_intReverse;
  
  void applyNewLagrange();
  virtual void apply_new_x();
public:
  typedef std::shared_ptr<SecondOrderOptimizationProblem> Ptr;

  SecondOrderOptimizationProblem(const bool minimize = true) 
      : FirstOrderOptimizationProblem(minimize){}
  virtual ~SecondOrderOptimizationProblem(){}

  SecondOrderOptimizationProblem(const GenericIntegratorReverse::Ptr& integrator,
                                 const OptimizerMetaData2ndOrder::Ptr& opt,
                                 const bool minimize = true);

  virtual OptimizationProblem::OptimizationOrder getOptimizationOrder() const
  {
    return OptimizationProblem::SECOND_ORDER;
  }

  virtual OptimizationProblem::ResultFlag getNonLinConstraintHessianDecVar
                                  (std::vector<std::vector<double> > &hessianMatrix);
  virtual OptimizationProblem::ResultFlag getNonLinConstraintHessianConstParam
                                  (std::vector<std::vector<double> > &hessianMatrix);
  virtual OptimizationProblem::ResultFlag getAdjointsDecVar
                                (std::vector<double> &adjoints1stOrder,
                                 std::vector<std::vector<double> > &adjoints2ndOrder);
  virtual OptimizationProblem::ResultFlag getAdjointsConstParam
                                    (std::vector<double> &adjoints1stOrder,
                                     std::vector<std::vector<double> > &adjoints2ndOrder);
};


