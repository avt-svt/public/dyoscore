/**
* @file GenericOptimizer.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* This file contains the class definitions of GenericOptimizer using a \n
* OptimizationProblem object. So the generic creation of the output is \n
* implemented in this class.                                           \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 13.09.2012
*/

#pragma once

#include "GenericOptimizer.hpp"
#include "OptimizationProblem.hpp"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

class GenericOptimizerWithOptimizationProblem : public GenericOptimizer
{
protected:
  OptimizationProblem::Ptr m_optProb;
public:
  typedef boost::shared_ptr<GenericOptimizerWithOptimizationProblem> Ptr;
  
  virtual std::vector<DyosOutput::StageOutput> getOutput();
  virtual void getOptimizerOutput(DyosOutput::OptimizerOutput &oo) const;
  virtual void calculateDecVarLagrangeMultiplier(utils::Array<double> &multiplier,
                                                 const double optimalityThreshold) const;
  std::vector<std::string> getOptimizerOptions(const char* filename)const;
  void writeOptimizerOptions(const char* filename,
                             const std::map<std::string, std::string> &optimizerOptions)const;
  void matchOptimizerOptions(std::map<std::string, std::string> &matchedOptimizerOptions,
                             const std::map<std::string, std::string> &optimizerOptions)const;
};