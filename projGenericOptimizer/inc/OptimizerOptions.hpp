/** @file NPSOLOptions.hpp
*    @brief header declaring NPSOLOptions class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails NPSOLOptions
*    =====================================================================\n
*   @author Ann-Kathrin Dombrowski
*   @date 12.6.2013
*/

#pragma once
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <utility>

class OptimizerOptions
{
  public:
  enum NumberType{INTEGER, DBL, NO_NUMBER, POS_INTEGER, POS_DBL, EMPTY};
  
  protected:
  //map with name of valid option and type of optionInput (if no input NumberType == EMPTY)
    std::map<std::string, std::vector<NumberType> > validOptions;

  public:
    std::pair<NumberType, double> number(const std::string& s)const;
    OptimizerOptions(){}
    std::map<std::string, std::string> checkOptions(const std::map<std::string, std::string> &userOptions);
    std::map<std::string, std::string> checkOptions(std::vector<std::string> &optionsVec);
};
