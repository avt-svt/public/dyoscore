/**
* @file FinalIntegration.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* GenericOptimizer - Part of DyOS                                         \n
* ========================================================================\n
* This file contains the declaration of the FinalIntegration class        \n
* ========================================================================\n
* @author Tjalf Hoffmann
* @date 09.01.2013
*/

#pragma once

#include "GenericOptimizerWithOptimizationProblem.hpp"
#include "SecondOrderOptimizationProblem.hpp"

/**
* @class FinalIntegration
* @brief class doing simply an integration
*
* Since for second order information data is needed that is set by the OptimizerMetaData,
* this class acts as an optimizer and thus having access to the OptimizerMetaData class
* (using the OptimizationProblem).
* In addition this class is used for a final state integration using a plot grid different from
* the optimization grid (a new Integrator must be instatiated for that task).
*/
//note: The class must produce OptimizerOutput and must therefor inherit from GenericOptimizer
class FinalIntegration : public GenericOptimizerWithOptimizationProblem
{
public:
  FinalIntegration(const OptimizationProblem::Ptr& optProb);
  ~FinalIntegration();
  DyosOutput::OptimizerOutput solve();
};