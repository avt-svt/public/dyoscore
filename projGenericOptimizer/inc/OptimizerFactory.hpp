/**
* @file OptimizerFactory.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer                                                     \n
* =====================================================================\n
* This file contains the declarations of class OptimizerFactory        \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.02.2012
*/
#pragma once

#include "OptimizerInput.hpp"
#include "GenericOptimizer.hpp"
#include "OptimizationProblem.hpp"
#include "SecondOrderOptimizationProblem.hpp"
#include "IntegratorFactory.hpp"
#include "MetaDataFactories.hpp"
#include "DyosObject.hpp"
#include "SolverConfig.hpp"

class IpoptWrapper;
class SNOPTWrapper;
class NPSOLWrapper;

/**
* @class IncompatibleIntegrationOrderException
* @brief exception class for any exception of the Optimizer module
* @note only temporary - should be replaced by DyosException in module DyosObject
*/
class IncompatibleIntegrationOrderException : public std::exception
{
protected:
  const std::string m_errorMessage;
public:
  /// @brief standard constructor setting a standard error message
  IncompatibleIntegrationOrderException()
    : m_errorMessage("Optimizer does not support integration order.")
  {

  }
  
  /**
  * @brief constructor
  * @param[in] message string containing a specific error message
  */ 
  IncompatibleIntegrationOrderException(const std::string &message)
    : m_errorMessage(message)
  {
  }

  /// @brief destructor
  virtual ~IncompatibleIntegrationOrderException() throw() {}

  /**
  * @brief get error message
  * @return error message of the exception
  */
  const char *what() const throw()
  {
    return m_errorMessage.c_str();
  }

};

/**
* @class OptimizerFactory
* @brief factory class for creating GenericOptimizer objects
*/
class OptimizerFactory : public DyosObject
{
public:
  GenericOptimizer *createGenericOptimizer(const FactoryInput::OptimizerInput &input);
  GenericOptimizer *createGenericOptimizer(const FactoryInput::OptimizerInput &input,
                                                 IntegratorMetaDataFactory &imdFactory,
                                                 IntegratorFactory &integratorFactory,
                                                 OptimizerMetaDataFactory &metaDataFactory);
  GenericOptimizer *createFirstOrderOptimizer(const FactoryInput::OptimizerInput &input,
                                              const OptimizationProblem::Ptr& optProb);
  GenericOptimizer *createSecondOrderOptimizer(const FactoryInput::OptimizerInput &input,
                                               const SecondOrderOptimizationProblem::Ptr& optProb);
  OptimizationProblem *createFirstOrderOptimizationProblem(
                                            const FactoryInput::OptimizerInput &input,
                                                  IntegratorMetaDataFactory &imdFactory,
                                                  IntegratorFactory &integratorFactory,
                                                  OptimizerMetaDataFactory &metaDataFactory);
  SecondOrderOptimizationProblem *createSecondOrderOptimizationProblem(
                                            const FactoryInput::OptimizerInput &input,
                                                  IntegratorMetaDataFactory &imdFactory,
                                                  IntegratorFactory &integratorFactory,
                                                  OptimizerMetaDataFactory &metaDataFactory);
  OptimizationProblem *createFirstOrderOptimizationProblem(
                                     const GenericIntegrator::Ptr &integrator,
                                     const OptimizerMetaData::Ptr &optimizerMetaData,
                                     const bool optimizationMode);
  SecondOrderOptimizationProblem *createSecondOrderOptimizationProblem(
                                     const GenericIntegratorReverse::Ptr &integrator,
                                     const OptimizerMetaData2ndOrder::Ptr &optimizerMetaData,
                                     const bool optimizationMode);
  GenericOptimizer *createFilterSQP(const SecondOrderOptimizationProblem::Ptr& optProb);
  GenericOptimizer *createSnopt(const OptimizationProblem::Ptr& optProb,
                                const std::map<std::string, std::string> &optimizerOptions);
  GenericOptimizer *createIpopt(const OptimizationProblem::Ptr& optProb,
                                const std::map<std::string, std::string> &optimizerOptions);
  GenericOptimizer *createNpsol(const OptimizationProblem::Ptr& optProb,
                                const std::map<std::string, std::string> &optimizerOptions);
  GenericOptimizer *createFinalIntegration
                               (const OptimizationProblem::Ptr &optProb);
};