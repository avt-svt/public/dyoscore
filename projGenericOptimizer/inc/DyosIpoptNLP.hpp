#pragma once

#include "IpTNLP.hpp"
#include "IntegratorMetaData.hpp"
#include "OptimizationProblem.hpp"
#include "GenericOptimizer.hpp"


class DyosTNLP : public Ipopt::TNLP , public DyosObject{
  // Implementation info:
  // constraints are sorted as first nonlinear then linear
  // as are variables
private:
  OptimizationProblem::Ptr m_optProb;

  Ipopt::Index n_;
  Ipopt::Index m_nonlin_;
  Ipopt::Index m_lin_;
  Ipopt::Index m_;
  Ipopt::Index nnz_jac_g_;

  DyosOutput::OptimizerOutput m_output;
public:
  StdCapture m_capture;
  DyosTNLP(){}
  DyosTNLP(const DyosTNLP&)
  {
  }

  DyosTNLP(const OptimizationProblem::Ptr& optProb)
    : m_optProb(optProb)
  {}

  ~DyosTNLP() {}

  virtual bool get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g,
                            Ipopt::Index& nnz_h_lag, IndexStyleEnum& index_style);

  virtual bool get_var_con_metadata(Ipopt::Index n,
                                    StringMetaDataMapType& var_string_md,
                                    IntegerMetaDataMapType& var_integer_md,
                                    NumericMetaDataMapType& var_numeric_md,
                                    Ipopt::Index m,
                                    StringMetaDataMapType& con_string_md,
                                    IntegerMetaDataMapType& con_integer_md,
                                    NumericMetaDataMapType& con_numeric_md);



  /** overload this method to return the information about the bound
   *  on the variables and constraints. The value that indicates
   *  that a bound does not exist is specified in the parameters
   *  nlp_lower_bound_inf and nlp_upper_bound_inf.  By default,
   *  nlp_lower_bound_inf is -1e19 and nlp_upper_bound_inf is
   *  1e19. (see TNLPAdapter) */
  virtual bool get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u,
                               Ipopt::Index m, Ipopt::Number* g_l, Ipopt::Number* g_u);

  /** overload this method to return scaling parameters. This is
   *  only called if the options are set to retrieve user scaling.
   *  There, use_x_scaling (or use_g_scaling) should get set to true
   *  only if the variables (or constraints) are to be scaled.  This
   *  method should return true only if the scaling parameters could
   *  be provided.
   */
  virtual bool get_scaling_parameters(Ipopt::Number& obj_scaling,
                                      bool& use_x_scaling, Ipopt::Index n,
                                      Ipopt::Number* x_scaling,
                                      bool& use_g_scaling, Ipopt::Index m,
                                      Ipopt::Number* g_scaling);

  /** overload this method to return the variables linearity
   * (TNLP::Linear or TNLP::NonLinear). The var_types
   *  array should be allocated with length at least n. (default implementation
   *  just return false and does not fill the array).*/
  virtual bool get_variables_linearity(Ipopt::Index n, LinearityType* var_types);

  /** overload this method to return the constraint linearity.
   * array should be alocated with length at least n. (default implementation
   *  just return false and does not fill the array).*/
  virtual bool get_constraints_linearity(Ipopt::Index m, LinearityType* const_types);

  /** overload this method to return the starting point. The bool
   *  variables indicate whether the algorithm wants you to
   *  initialize x, z_L/z_u, and lambda, respectively.  If, for some
   *  reason, the algorithm wants you to initialize these and you
   *  cannot, return false, which will cause Ipopt to stop.  You
   *  will have to run Ipopt with different options then.
   */
  virtual bool get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x,
                                  bool init_z, Ipopt::Number* z_L, Ipopt::Number* z_U,
                                  Ipopt::Index m, bool init_lambda,
                                  Ipopt::Number* lambda);

  /** overload this method to provide an Ipopt iterate (already in
   *  the form Ipopt requires it internally) for a warm start.
   *  Since this is only for expert users, a default dummy
   *  implementation is provided and returns false. */
  virtual bool get_warm_start_iterate(Ipopt::IteratesVector& warm_start_iterate);

  /** overload this method to return the value of the objective function */
  virtual bool eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                      Ipopt::Number& obj_value);

  /** overload this method to return the vector of the gradient of
   *  the objective w.r.t. x */
  virtual bool eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                           Ipopt::Number* grad_f);

  /** overload this method to return the vector of constraint values */
  virtual bool eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                      Ipopt::Index m, Ipopt::Number* g);
  /** overload this method to return the jacobian of the
   *  constraints. The vectors iRow and jCol only need to be set
   *  once. The first call is used to set the structure only (iRow
   *  and jCol will be non-NULL, and values will be NULL) For
   *  subsequent calls, iRow and jCol will be NULL. */
  virtual bool eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                          Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index* iRow,
                          Ipopt::Index *jCol, Ipopt::Number* values);

  /** overload this method to return the hessian of the
   *  lagrangian. The vectors iRow and jCol only need to be set once
   *  (during the first call). The first call is used to set the
   *  structure only (iRow and jCol will be non-NULL, and values
   *  will be NULL) For subsequent calls, iRow and jCol will be
   *  NULL. This matrix is symmetric - specify the lower diagonal
   *  only.  A default implementation is provided, in case the user
   *  wants to se quasi-Newton approximations to estimate the second
   *  derivatives and doesn't not neet to implement this method. */
  virtual bool eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                      Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number* lambda,
                      bool new_lambda, Ipopt::Index nele_hess,
                      Ipopt::Index* iRow, Ipopt::Index* jCol, Ipopt::Number* values);
  //@}

  /** @name Solution Methods */
  //@{
  /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
  virtual void finalize_solution(Ipopt::SolverReturn status,
                                 Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L, const Ipopt::Number* z_U,
                                 Ipopt::Index m, const Ipopt::Number* g, const Ipopt::Number* lambda,
                                 Ipopt::Number obj_value,
                                 const Ipopt::IpoptData* ip_data,
                                 Ipopt::IpoptCalculatedQuantities* ip_cq);
  /** This method is called just before finalize_solution.  With
   *  this method, the algorithm returns any metadata collected
   *  during its run, including the metadata provided by the user
   *  with the above get_var_con_metada.  Each metadata can be of
   *  type string, integer, and numeric. It can be associated to
   *  either the variables or the constraints.  The metadata that
   *  was associated with the primal variable vector is stored in
   *  var_..._md.  The metadata associated with the constraint
   *  multipliers is stored in con_..._md.  The metadata associated
   *  with the bound multipliers is stored in var_..._md, with the
   *  suffixes "_z_L", and "_z_U", denoting lower and upper
   *  bounds. */
  virtual void finalize_metadata(Ipopt::Index n,
                                 const StringMetaDataMapType& var_string_md,
                                 const IntegerMetaDataMapType& var_integer_md,
                                 const NumericMetaDataMapType& var_numeric_md,
                                 Ipopt::Index m,
                                 const StringMetaDataMapType& con_string_md,
                                 const IntegerMetaDataMapType& con_integer_md,
                                 const NumericMetaDataMapType& con_numeric_md);


  /** Intermediate Callback method for the user.  Providing dummy
   *  default implementation.  For details see IntermediateCallBack
   *  in IpNLP.hpp. */
  virtual bool intermediate_callback(Ipopt::AlgorithmMode mode,
                                     Ipopt::Index iter, Ipopt::Number obj_value,
                                     Ipopt::Number inf_pr, Ipopt::Number inf_du,
                                     Ipopt::Number mu, Ipopt::Number d_norm,
                                     Ipopt::Number regularization_size,
                                     Ipopt::Number alpha_du, Ipopt::Number alpha_pr,
                                     Ipopt::Index ls_trials,
                                     const Ipopt::IpoptData* ip_data,
                                     Ipopt::IpoptCalculatedQuantities* ip_cq);
  //@}

  /** @name Methods for quasi-Newton approximation.  If the second
   *  derivatives are approximated by Ipopt, it is better to do this
   *  only in the space of nonlinear variables.  The following
   *  methods are call by Ipopt if the quasi-Newton approximation is
   *  selected.  If -1 is returned as number of nonlinear variables,
   *  Ipopt assumes that all variables are nonlinear.  Otherwise, it
   *  calls get_list_of_nonlinear_variables with an array into which
   *  the indices of the nonlinear variables should be written - the
   *  array has the lengths num_nonlin_vars, which is identical with
   *  the return value of get_number_of_nonlinear_variables().  It
   *  is assumed that the indices are counted starting with 1 in the
   *  FORTRAN_STYLE, and 0 for the C_STYLE. */
  //@{
  virtual Ipopt::Index get_number_of_nonlinear_variables();

  virtual bool get_list_of_nonlinear_variables(Ipopt::Index num_nonlin_vars,
                                               Ipopt::Index* pos_nonlin_vars);

  void apply_new_x(bool new_x, const Ipopt::Number* x);

  DyosOutput::OptimizerOutput get_optimizer_output() const {return m_output;};
};

typedef boost::shared_ptr<DyosTNLP> DyosTNLPPtr;
