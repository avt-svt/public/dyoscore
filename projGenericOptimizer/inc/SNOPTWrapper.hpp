/**
* @file SNOPTWrapper.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* SNOPTWrapper - Part of DyOS                                             \n
* ========================================================================\n
* This file contains the class definitions of the SNOPTWrapper class      \n
* ========================================================================\n
* @author Klaus Stockmann
* @date 12.12.2011
*/
#pragma once

#include<vector>
//#include <boost/weak_ptr.hpp>
#include <memory>
#include <boost/filesystem.hpp>

#include "GenericOptimizerWithOptimizationProblem.hpp"
#include "SNOPTLoader.hpp"
#include "Input.hpp"

/**
* @class SNOPTWrapper
*
* @brief Class defining a GenericOptimizer
*
* Derived class of the class GenericOptimizer. With this class, the user is able to call the NLP
* optimizer SNOPT (Gill, Murray and Saunders)
*/
class SNOPTWrapper : public GenericOptimizerWithOptimizationProblem
{
protected:
  static std::weak_ptr<OptimizationProblem> m_staticOptProb;
  static std::weak_ptr<Logger> m_staticLogger;

  std::vector<char> m_charWorkspace;
  std::vector<int> m_intWorkspace;
  std::vector<double> m_realWorkspace;

  // Fortran file handles: <=0 : no output
  //                         6 : stdout
  //                       all other values: filename specified by user
  // handle of options file
  int m_specFileHandleSNOPT;

  // handles of output files
  int m_printFileHandleSNOPT;
  
  static int m_summaryFileHandleSNOPT;
  static std::string m_summaryFileName;

  SNOPTLoader snopt;

  static void objfun(int *mode, int *nnobj, double *x, double *fobj,
                     double *gobj, int *nstate, char *cu, int *lencu,
                     int *iu, int *leniu, double *ru, int *lenru, int flencu);

  static void confun(int *mode, int *nncon, int *nnjac, int *nejac, double *x,
                     double *fcon, double *gcon, int *nstate,
                     char *cu, int *lencu, int *iu, int *leniu,
                     double *ru, int *lenru,  int flencu);


  // utility functions
  static void logSNOPTOutput(Logger::Ptr &logger);
  
  void assembleJacobian(std::vector<double> &jacValsDummy,
                        std::vector<int> &jacRowIndices,
                        std::vector<int> &jacColPointer) const;

  void assembleBounds(std::vector<double> &lowerBounds,
                      std::vector<double> &upperBounds) const;

  void allocateSNOPTWorkspace(std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw,
                              const int numVars, const int numConstraints) const;
  //void checkOptimizerOptions(const char* filename,
  //                           const std::map<std::string, std::string> &optimizerOptions);

public:
  SNOPTWrapper(const OptimizationProblem::Ptr& optProb,
               const std::map<std::string, std::string> &optimizerOptions = std::map<std::string,std::string>());
  ~SNOPTWrapper();
  DyosOutput::OptimizerOutput solve();
};
