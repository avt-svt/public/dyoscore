
#pragma once

#include "OptimizationProblem.hpp"
#include "GenericIntegrator.hpp"
#include "OptimizerMetaData.hpp"

class FirstOrderOptimizationProblem : public OptimizationProblem
{
protected:
  bool m_new_dec_vars;
  bool m_new_multipliers;
  bool m_minimize;
  GenericIntegrator::Ptr m_int;
  OptimizationProblem::ResultFlag m_integratorResult;
  OptimizerMetaData::Ptr m_opt;
  
  virtual void apply_new_x();
public:
  typedef boost::shared_ptr<FirstOrderOptimizationProblem> Ptr;

  FirstOrderOptimizationProblem(const bool minimize = true);
  virtual ~FirstOrderOptimizationProblem(){}

  FirstOrderOptimizationProblem(const GenericIntegrator::Ptr& integrator,
                                const OptimizerMetaData::Ptr& opt,
                                const bool minimize = true);

  virtual OptimizationProblem::OptimizationOrder getOptimizationOrder() const
  {
    return OptimizationProblem::FIRST_ORDER;
  }

  virtual const OptimizerProblemDimensions &getOptProbDim() const;

  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const;

  virtual void getDecVarValues(utils::Array<double> &values) const;

  virtual void setDecVarValues(const utils::Array<const double> &decVars);

  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const;

  virtual OptimizationProblem::ResultFlag getNonLinConstraintValues(utils::Array<double> &values);

  virtual OptimizationProblem::ResultFlag getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr derivativeMatrix);

  virtual OptimizationProblem::ResultFlag getNonLinConstraintDerivativesConstParam
                                          (CsTripletMatrix::Ptr derivativeMatrix);

  virtual OptimizationProblem::ResultFlag getNonLinObjValue(double &objValue);

  virtual OptimizationProblem::ResultFlag getNonLinObjDerivative(utils::Array<double> &values);

  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers);
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const;
  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier);
  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier);
  
  virtual OptimizationProblem::ResultFlag getLinConstraintValues(utils::Array<double> &values);
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds) const;
  virtual OptimizationProblem::ResultFlag getNonLinConstraintHessianDecVar
                                     (std::vector<std::vector<double> > &hessianMatrix);
  virtual OptimizationProblem::ResultFlag getNonLinConstraintHessianConstParam
                                     (std::vector<std::vector<double> > &hessianMatrix);
  virtual OptimizationProblem::ResultFlag getAdjointsDecVar
                                (std::vector<double> &adjoints1stOrder,
                                 std::vector<std::vector<double> > &adjoints2ndOrder);
  virtual OptimizationProblem::ResultFlag getAdjointsConstParam
                                    (std::vector<double> &adjoints1stOrder,
                                     std::vector<std::vector<double> > &adjoints2ndOrder);
  virtual OptimizationProblem::ResultFlag doFinalIntegration();
  virtual std::vector<DyosOutput::StageOutput> getOutput();
};
