/**
* @file FilterSQPWrapper.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* NPSOLWrapper - Part of DyOS                                             \n
* ========================================================================\n
* This file contains the class definitions of the FilterSQPWrapper class  \n
* ========================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi, Fady Assassa
* @date 27.11.2012
*/
#pragma once

#include<vector>
//#include <boost/weak_ptr.hpp>
#include <memory>

#include "GenericOptimizerWithOptimizationProblem.hpp"
#include "FilterSQPLoader.hpp"

/**
* @class FilterSQPWrapper
*
* @brief Class defining a GenericOptimizer
*/
class FilterSQPWrapper : public GenericOptimizerWithOptimizationProblem
{
protected:
  static std::weak_ptr<OptimizationProblem> m_staticOptProb;
  static std::weak_ptr<Logger> m_staticLogger;
  static std::fstream m_staticFileReader;
  static std::vector<double> m_staticLastSetDecVars;
  
  std::vector<int> m_intWorkspace;
  std::vector<double> m_realWorkspace;
  bool m_isSparse;

  // Fortran file handles: <=0 : no output
  //                         6 : stdout
  //                       all other values: filename specified by user
  // handles of output files
  int m_printFileHandleFilterSQP;
  int m_summaryFileHandleFilterSQP;

  FilterSQPLoader filtersqp;

  static void confun(double *x, int *n, int *m, double *c, double *a, int *la,
                     double *user, int *iuser, int *flag);
  static void objfun(double *x, int *n, double *f, double *user, int *iuser, int *flag);
  static void gradient(int *n, int *m, int *mxa, double *x, double *a, int *la,
                       int *maxa, double *user, int *iuser, int *flag);
  static void hessian(double *x, int *n, int *m, int *phase, double *lam, double *ws, int *lws,
                      double *user, int *iuser, int *l_hess, int *li_hess, int *flag);
  static void Wdotd(int *, double*, double*, int* , double*); //n, d, ws, lws, v

  //utility functions
  static void openLogFile();
  static void closeLogFile();
  static void logFilterSQPOutput(Logger::Ptr &logger);
  static void setDecVars(double *x, int *n);
  
  void assembleJacobian(std::vector<double> &jacValsDummy,
                        std::vector<int> &jacRowIndices,
                        std::vector<int> &jacColPointer,
                        int &nnzLinear,
                        int &nnzNonlinear) const;

  int maxRealWS(const int n,
                const int m,
                const int mlp,
                const int maxf,
                const int kmax)const;
  
  int maxIntWS(const int n,
               const int m,
               const int mlp,
               const int kmax) const;

  void assembleBounds(utils::Array<double> &lowerBounds,
                      utils::Array<double> &upperBounds)const;
public:
  FilterSQPWrapper(const OptimizationProblem::Ptr& optProb);
  ~FilterSQPWrapper(){};
  DyosOutput::OptimizerOutput solve();
};