/**
* @file GenericOptimizer.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* This file contains the class definitions of the Optimizer class      \n
* =====================================================================\n
* @author Klaus Stockmann, Tjalf Hoffmann
* @date 11.04.2011
*/
#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>
#include "OptimizerOutput.hpp"
#include "MetaDataOutput.hpp"
#include "DyosObject.hpp"


/**
* @class GenericOptimizer
*
* @brief abstract class for NLP optimizers used for a problem defined as OptimizerSingleStageMetaData object
*
* Abstract class of the optimizer
*/
class GenericOptimizer : public DyosObject
{
public:
  typedef boost::shared_ptr<GenericOptimizer> Ptr;
  /**
  *  @brief destructor
  */
  virtual ~GenericOptimizer(){}
  /**
  * @brief solves NLP problem
  *
  * The problem defined by the OptimizerSingleStageMetaData object is solved.
  * @return output struct containing the general values
  */
  virtual DyosOutput::OptimizerOutput solve() = 0;

  virtual std::vector<DyosOutput::StageOutput> getOutput() = 0;
};
