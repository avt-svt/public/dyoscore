/**
* @file OptimizerOutput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* This file contains definition of the OptimizerOutput                 \n
* =====================================================================\n
* @author Kathrin Frankl, Tjalf Hoffmann
* @date 14.09.2012
*/

#pragma once

#include <map>
#include <string>

namespace DyosOutput
{
  struct SecondOrderOutput
  {
    std::map<std::string, int> indicesHessian;
    std::vector<std::vector<double> > hessian;
    std::vector<std::vector<double> > constParamHessian;
    std::vector<std::vector<double> > compositeAdjoints;
  };
  
  /**
  * @struct OptimizerOutput
  * @brief output information of the optimizer, i.e. optimal objective function value, optimal lagrange multiplier values,
  *        optimal values of the decision variables and the status how the optimization was finished
  */
  struct OptimizerOutput{
    /**
    * @enum OptInformFlag exit status of the optimization
    */
    enum OptInformFlag
    {
      OK,
      TOO_MANY_ITERATIONS,
      INFEASIBLE,
      WRONG_GRADIENTS,
      NOT_OPTIMAL, ///< standard value
      FAILED
    };
    //! objective function value
    double optObjFunVal;
    //! intermediate constraint violation
    double intermConstrVio;
    //! values of Lagrange Multipliers
    std::vector<double> lagrangeMultiplier;
    //! final values of decision variables
    std::vector<double> optDecVarValues;
    //! Lagrange Multipliers of the decision variables
    std::vector<double> optDecVarLagrangeMultiplier;
    //! exit status of optimizer; as this has to fit all optimizers, the values are rather generic
    OptInformFlag m_informFlag;
    
    SecondOrderOutput secondOrderOutput;
    
    /// @brief constructor
    OptimizerOutput()
    {
      m_informFlag = NOT_OPTIMAL;
      optObjFunVal = 0.0;
      intermConstrVio = 0.0;
    }

  };
}
