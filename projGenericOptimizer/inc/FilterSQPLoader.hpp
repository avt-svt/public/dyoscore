/** @file FilterSQPLoader.hpp
*    @brief header declaring FilterSQPLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails FilterSQPLoader
*    =====================================================================\n
*   @author Mohammadsobhan Moazemi Goodarzi, Fady Assassa
*   @date 22.11.2012
*/
#pragma once

#include "Loader.hpp"
#include <vector>
#include "Array.hpp"
#include "DyosObject.hpp"

/** typedef of function pointer for call of FilterSQP - only for better readability of the filterSQPFunc typedef
  * This is a C function.
  */
typedef void (*objfunPtrFilterSQP_t)(double*, // x
                                   int *, // n
                                   double*, // f
                                   double*, // user
                                   int*, // iuser
                                   int* // flag
                                   );

typedef void (*Wdotd_t)(int *, double*, double*, int* , double*);

/** typedef of function pointer for call of FilterSQP - only for better readability of the filterSQPFunc typedef
  * This is a C function.
  */
typedef void (*confunPtrFilterSQP_t)(double*, // x
                                   int *, // n
                                   int *, // m
                                   double*, // c
                                   double*, // a
                                   int*, //la
                                   double*, // user
                                   int*, // iuser
                                   int * // flag
                                   );

/** typedef of function pointer for call of FilterSQP - only for better readability of the filterSQPFunc typedef
  * This is a C function.
  */
typedef void (*gradientPtrFilterSQP_t)(int *, // n
                                     int *, // m
                                     int *, // mxa
                                     double*, // x
                                     double*, // a
                                     int*, //la
                                     int *, // maxa
                                     double*, // user
                                     int*, //iuser
                                     int * //flag
                                     );

/** typedef of function pointer for call of FilterSQP - only for better readability of the filterSQPFunc typedef
  * This is a C function.
  */
typedef void (*hessianPtrFilterSQP_t)(double*, // x
                                    int *, // n
                                    int *, // m
                                    int *, // phase
                                    double*, // lam
                                    double*, // ws
                                    int*, // lws
                                    double*, // user
                                    int*, // iuser
                                    int *, // l_hess
                                    int *, // li_hess
                                    int * // flag
                                    );


/**
* typedef of FORTRAN routine from filterSQP DLL for dynamic loading
* This is a FORTRAN function called from C.
*/
typedef void( *filterSQPFunc)(int *, // n
                                      int *, // m
                                      int *, // kmax
                                      int *, // maxa
                                      int *, // maxf
                                      int *, // mlp
                                      int *, // mxwk
                                      int *, // mxiwk
                                      int *, // iprint
                                      int *, // nout
                                      int *, // ifail
                                      double*, // rho
                                      double*, // x
                                      double*, // c
                                      double*, // f
                                      double*, // fmin
                                      double*, // blo,
                                      double*, // bup,
                                      double*, // s,
                                      double*, // a,
                                      int*, // la,
                                      double*, // ws,
                                      int*, // lws,
                                      double*, // lam,
                                      char*, //cstype,
                                      double*, // user,
                                      int*, // iuser,
                                      int *, // max_iter,
                                      int*, // istat,
                                      double*, // rstat,
                                      objfunPtrFilterSQP_t,
                                      confunPtrFilterSQP_t,
                                      gradientPtrFilterSQP_t,
                                      hessianPtrFilterSQP_t,
                                      Wdotd_t
                                      //int //length of cstype
                                      );


/** @brief load filterSQP DLL */
class FilterSQPLoader: public utils::Loader
{
protected:
  filterSQPFunc filterSQPFuncHandle;
  void loadFunctions();

public:
  FilterSQPLoader();
  FilterSQPLoader(const FilterSQPLoader &filterSQPLoader);
  ~FilterSQPLoader(){};
  void filterSQP(int n, int numConstraints, int kmax, int maxa, int maxf,
                 int mlp, int mxwk, int mxiwk, int iprint,
                 int nout, int &ifail, double rho, utils::Array<double> &x,
                 utils::Array<double> &c, double f, double fmin, utils::Array<double> &blo,
                 utils::Array<double> &bup, utils::Array<double> &s, utils::Array<double> &a,
                 utils::Array<int> &la, utils::Array<double> &ws, utils::Array<int> &lws,
                 utils::Array<double> &lam, utils::Array<char> &cstype, int cstypelen,
                 utils::Array<double> &user, utils::Array<int> &iuser, int max_iter,
                 utils::Array<int> &istat, utils::Array<double> &rstat,
                 objfunPtrFilterSQP_t objfun_ptr, confunPtrFilterSQP_t confun_ptr,
                 gradientPtrFilterSQP_t gradient_ptr, hessianPtrFilterSQP_t hessian_ptr,
                 Wdotd_t wdot_ptr);
};