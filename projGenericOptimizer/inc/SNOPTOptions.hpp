/** @file SNOPTOptions.hpp
*    @brief header declaring SNOPTOptions class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails SNOPTOptions
*    =====================================================================\n
*   @author Ann-Kathrin Dombrowski
*   @date 12.6.2013
*/

#pragma once
#include "OptimizerOptions.hpp"

class SNOPTOptions : public OptimizerOptions
{
  public:
    SNOPTOptions()
    {
      //key needs to be written in lower case
      validOptions["minimize"].push_back(EMPTY);
      validOptions["maximize"].push_back(EMPTY);
      validOptions["solution"].push_back(NO_NUMBER);
      validOptions["system information"].push_back(NO_NUMBER);
      validOptions["iterations limit"].push_back(POS_INTEGER);
      validOptions["major optimality tolerance"].push_back(POS_DBL);
      validOptions["major feasibility tolerance"].push_back(POS_DBL);
      validOptions["minor feasibility tolerance"].push_back(POS_DBL);
      validOptions["function precision"].push_back(POS_DBL);
      validOptions["verify level"].push_back(INTEGER);
      validOptions["verify level"].push_back(POS_INTEGER);
      validOptions["major iterations limit"].push_back(POS_INTEGER);
      validOptions["minor iterations limit"].push_back(POS_INTEGER);
      validOptions["jacobian"].push_back(NO_NUMBER);
      validOptions["derivative level"].push_back(INTEGER);
      validOptions["derivative level"].push_back(POS_INTEGER);
      validOptions["linesearch tolerance"].push_back(POS_DBL);
	  validOptions["linesearch tolerance"].push_back(POS_INTEGER);
      validOptions["major step limit"].push_back(POS_DBL);
      validOptions["major step limit"].push_back(POS_INTEGER);
      validOptions["backup basis file"].push_back(INTEGER);   //a safeguard against losing the results of a long run.
      validOptions["central difference interval"].push_back(DBL);
      validOptions["difference interval"].push_back(DBL);
      validOptions["check frequency"].push_back(POS_INTEGER);
      validOptions["cold Start"].push_back(EMPTY);    //Requests that the CRASH procedure be used to choose an initial basis, unless a basis file is provided via Old basis, Insert or Load in the Specs file.
      validOptions["crash option"].push_back(INTEGER);
      validOptions["crash tolerance"].push_back(DBL);   //allows the starting procedure CRASH to ignore certain �small� nonzeros in each column of A.
      validOptions["derivative linesearch"].push_back(EMPTY);
      validOptions["nonderivative linesearch"].push_back(EMPTY);
      validOptions["derivative option"].push_back(INTEGER);   //is intended for snOptA only and should not be used with any other interface
      validOptions["dump file"].push_back(DBL);
      validOptions["elastic weight"].push_back(POS_DBL);
	  validOptions["elastic weight"].push_back(POS_INTEGER);
      validOptions["expand frequency"].push_back(DBL);    //is part of the EXPAND anti-cycling procedure [13] designed to make progress even on highly degenerate problems
      validOptions["factorization frequency"].push_back(POS_INTEGER);   //At most k basis changes will occur between factorizations of the basis matrix.
      validOptions["feasible point"].push_back(EMPTY);    //means �Ignore the objective function� while finding a feasible point for the linear and nonlinear constraints.
      validOptions["feasibility tolerance"].push_back(POS_DBL);
      validOptions["hessian dimension"].push_back(INTEGER);
      validOptions["hessian full memory"].push_back(DBL);
      validOptions["hessian limited memory"].push_back(DBL);
      validOptions["hessian frequency"].push_back(POS_INTEGER);
      validOptions["hessian updates"].push_back(INTEGER);
      validOptions["insert file"].push_back(INTEGER);
      validOptions["infinite bound"].push_back(DBL);
      validOptions["iterations limit"].push_back(POS_INTEGER);
      validOptions["load file"].push_back(DBL);
      validOptions["log frequency"].push_back(POS_INTEGER);
      validOptions["lu factor tolerance"].push_back(DBL);
      validOptions["lu update tolerance"].push_back(DBL);
      validOptions["lu partial pivoting"].push_back(DBL);
      validOptions["lu rook pivoting"].push_back(DBL);
      validOptions["lu complete pivoting"].push_back(DBL);
      validOptions["lu density tolerance"].push_back(DBL);
      validOptions["lu singularity pivoting"].push_back(DBL);
      validOptions["major print level"].push_back(DBL);
      validOptions["minor print level"].push_back(DBL);
      validOptions["new basis file"].push_back(DBL);
      validOptions["new superbasics limit"].push_back(INTEGER);
      validOptions["objective Row"].push_back(INTEGER);
      validOptions["old basis file"].push_back(DBL);
      validOptions["partial price"].push_back(INTEGER);
      validOptions["pivot tolerance"].push_back(DBL);
      validOptions["print file"].push_back(DBL);
      validOptions["print frequency"].push_back(POS_INTEGER);
      validOptions["proximal point method"].push_back(POS_INTEGER);
      validOptions["punch file"].push_back(DBL);
      validOptions["qpsolver"].push_back(NO_NUMBER);
      validOptions["qpsolver"].push_back(NO_NUMBER);
      validOptions["qpsolver"].push_back(NO_NUMBER);
      validOptions["reduced hessian dimension"].push_back(INTEGER);
      validOptions["save frequency"].push_back(INTEGER);
      validOptions["scale option"].push_back(INTEGER);
      validOptions["scale tolerance"].push_back(DBL);   //affects how many passes might be needed through the constraint matrix.
      validOptions["scale print"].push_back(DBL);
      validOptions["solution file"].push_back(DBL);
      validOptions["start objective check at column"].push_back(INTEGER);
      validOptions["stop objective check at column"].push_back(INTEGER);
      validOptions["start constraint check at column"].push_back(INTEGER);
      validOptions["stop constraint check at column"].push_back(INTEGER);
      validOptions["sticky parameters"].push_back(NO_NUMBER);
      validOptions["summary file"].push_back(INTEGER);
      validOptions["summary frequency"].push_back(INTEGER);
      validOptions["superbasics limit"].push_back(INTEGER);
      validOptions["suppress parameters"].push_back(INTEGER);
      validOptions["total real workspace"].push_back(INTEGER);
      validOptions["total integer workspace"].push_back(INTEGER);
      validOptions["total character workspace"].push_back(INTEGER);
      validOptions["user real workspace"].push_back(INTEGER);
      validOptions["user integer workspace"].push_back(INTEGER);
      validOptions["user character workspace"].push_back(INTEGER);
      validOptions["timing level"].push_back(INTEGER);
      validOptions["Unbounded objective value"].push_back(DBL);
      validOptions["Unbounded step size"].push_back(DBL);
      validOptions["violation limit"].push_back(POS_DBL);   //defines an absolute limit on the magnitude of the maximum constraint violation after the linesearch
      validOptions["warm start"].push_back(EMPTY);    //indicates that a basis is already specified via the input arrays for SNOPT.
    }
};
