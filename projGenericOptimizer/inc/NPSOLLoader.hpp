/** @file NPSOLLoader.hpp
*    @brief header declaring NPSOLLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails NPSOLLoader
*    =====================================================================\n
*   @author Mathias Dunst
*   @date 30.10.2012
*/
#pragma once

#include "Loader.hpp"
#include <vector>

/** typedef of function pointer for call of NPSOL - only for better readability of the npsol_func typedef
  * This is a C function.
  */
typedef void (*confunPtrNPSOL)(int *, // mode
                          int *, // ncnln
                          int *, // n
                          int *, // ldJ
                          int *, // needc
                          double *, // x
                          double *, // c
                          double *, // cJac
                          int *); //nstate

/** typedef of function pointer for call of NPSOL - only for better readability of the npsol_func typedef
  * This is a C function.
  */
typedef void (*objfunPtrNPSOL)(int *, // mode
                                 int *, // n
                                 double *, // x
                                 double *, // f
                                 double *, // g
                                 int *); // nstate

/**
* typedef of FORTRAN routine from NPSOL DLL for dynamic loading
* This is a FORTRAN function called from C.
*/
typedef void(*npsolFunc)(int *, // n
                                   int *, // nclin
                                   int *, // ncnln
                                   int *, // ldA
                                   int *, // ldJ
                                   int *, // ldR
                                   double[], // A
                                   double[], // bl
                                   double[], // bu
                                   confunPtrNPSOL, // funcon
                                   objfunPtrNPSOL, // funobj
                                   int *, // inform
                                   int *, // iter
                                   int[], // istate
                                   double[], // c
                                   double[], // cJac
                                   double[], // clamda
                                   double *, // f
                                   double[], // g
                                   double[], // R
                                   double[], // x
                                   int[], // iw
                                   int *, // leniw
                                   double[], // w
                                   int *); // lenw

/**
* typedef of FORTRAN routine from NPSOL DLL for dynamic loading
* This is a FORTRAN function called from C.
*/
typedef void(*npsolOpenFileFunc)(int*,
                                 const char[],
                                 int);

/**
* typedef of FORTRAN routine from NPSOL DLL for dynamic loading
* This is a FORTRAN function called from C.
*/
typedef void(*npsolCloseFileFunc)(int*);

/**
* typedef of FORTRAN routine from NPSOL DLL for dynamic loading
* This is a FORTRAN function called from C.
*/
typedef void(*npsolOptionFileFunc)(int*,
                                   int*);

/** @brief load NPSOL DLL */
class NPSOLLoader: public utils::Loader
{
protected:
  /** @brief  handle to NPSOL main routine */
  npsolFunc npsolFuncHandle;

  /** @brief  handle to function which opens file for NPSOL from within C code*/
  npsolOpenFileFunc npsolOpenFileFuncHandle;
  /** @brief  handle to function which closes file for NPSOL from within C code*/
  npsolCloseFileFunc npsolCloseFileFuncHandle;
  /** @brief  handle to function which reads optimizerOptions from within C code*/
  npsolOptionFileFunc npsolOptionFileFuncHandle;

  void loadFunctions();

public:
  NPSOLLoader();
  NPSOLLoader(const NPSOLLoader &npsolLoader);
  ~NPSOLLoader(){};

  void npsol(int n, int numLinConstraints, int numNonLinConstraints, int rowMatrixA, int rowJacobian, int rowMatrixR,
             double matrixA[], double lowerBounds[], double upperBounds[],
             confunPtrNPSOL confun, objfunPtrNPSOL objfun,
             int inform, int iter, int istate[],
             double constrinats[], double jacobianConstraints[], double clamda[],
             double f, double g[], double matrixR[], double x[],
             int intWorkSpace[], int leniw, double realWorkSpace[], int lenw);

  void npsolOpenFile(const int fileHandle, const std::string fileName);
  void npsolCloseFile(const int fileHandle);
  void npsolReadOptionFile(const int fileHandle, const std::string fileName);
};
