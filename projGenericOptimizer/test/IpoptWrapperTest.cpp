/**
* @file IpoptWrapperTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* IpoptWrapperTest - Part of DyOS                                      \n
* =====================================================================\n
* Test of class GenericOptimizer                                       \n
* =====================================================================\n
* @author Hans Pirnay
* @date 2012-06-28
*/

//#include <vector>

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE GenericOptimizer
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>

#include "IpoptWrapper.hpp"

/**
* @def THRESHOLD
* @brief general threshold for BOOST_*_CLOSE macros
*/
#define THRESHOLD 1e-16


#include "ipoptTestsuite.hpp"
//#include "SingleStageSystemTestsuite.hpp"
//#include "MultiStageSystemTestsuite.hpp"
