/**
* @file SingleStageTestProblemFactory.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Header of factory file for a single stage test problem used for an   \n
* optimization system test.                                            \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 11.04.2012
*/

#pragma once

#include "JadeGenericEso.hpp"
#include "GenericOptimizer.hpp"
#include "ForwardIntegratorNixe.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include "MetaDataFactories.hpp"
#include "SNOPTWrapper.hpp"

int findEsoIndex(GenericEso::Ptr &eso, const std::string &name);

GenericEso::Ptr createJadeOldCarExample();
GenericEso::Ptr createJadeCarExample();

IntegratorSingleStageMetaData::Ptr createCarIntegratorMetaData(GenericEso::Ptr &genEso);
IntegratorSingleStageMetaData::Ptr createOldCarIntegratorMetaData(GenericEso::Ptr &genEso);

GenericIntegrator::Ptr createNixe(IntegratorMetaData::Ptr &integratorMetaData);

OptimizerMetaData::Ptr createOptimizerSingleStageMetaData
                                 (IntegratorSingleStageMetaData::Ptr &integratorMetaData);
OptimizerMetaData::Ptr createOptimizerOldCarSingleStageMetaData
                                 (IntegratorSingleStageMetaData::Ptr &integratorMetaData,
                                  GenericEso::Ptr &genEso);

GenericOptimizer *createSNOPT(GenericEso::Ptr &genericEso);
GenericOptimizer *createSNOPTOldCar();
