/**
* @file ipopt.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ipopt.testsuite - Part of DyOS                                       \n
* =====================================================================\n
* Test Ipopt Wrapper                                                   \n
* =====================================================================\n
* @author Hans Pirnay
* @date 2012-07-06
*/


#include "FirstOrderOptimizationProblem.hpp"
#include "GenericOptimizerTestDummies.hpp"
#include "IpoptWrapper.hpp"
#include "OptimizerSingleStageMetaData.hpp"

BOOST_AUTO_TEST_SUITE(TestIpopt)

/**
* @brief GenericOptimizerTest - test hardcoded Rosenbrock problem
* @author Tjalf Hoffmann, Kathrin Frankl
*/
BOOST_AUTO_TEST_CASE(RosenbrockIpoptTest)
{
  const double lagrange_threshold = 1.0e-8;
  const double decvar_threshold = 1.0e-2;
  GenericIntegrator::Ptr dummyIntegrator (new EmptyGenericIntegratorDummy());
  OptimizerMetaData::Ptr rosenbrockOmd (new RosenbrockOptimizerMetaData());
  OptimizationProblem::Ptr optProblem(new FirstOrderOptimizationProblem(dummyIntegrator, rosenbrockOmd));
  IpoptWrapper ipopt(optProblem);
  DyosOutput::OptimizerOutput m_optOut = ipopt.solve();

  BOOST_CHECK_SMALL(m_optOut.lagrangeMultiplier[0], lagrange_threshold);
  BOOST_CHECK_CLOSE(m_optOut.optDecVarValues[0], 1.0, decvar_threshold);
  BOOST_CHECK_CLOSE(m_optOut.optDecVarValues[1], 1.0, decvar_threshold);
}


BOOST_AUTO_TEST_SUITE_END()

