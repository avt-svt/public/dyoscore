/**
* @file SingleStageTestProblemFactory.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Factory file for a single stage test problem used for an optimization\n
* system test.                                                         \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 11.04.2012
*/

#include "SingleStageTestProblemFactory.hpp"
#include "FirstOrderOptimizationProblem.hpp"

#include "boost/test/unit_test.hpp"

/**
* @brief find the index to a given variable name in a given GenericEso object
*
* The search is case sensitive, so lower case strings will not match upper case strings
* @param eso the GenericEso object in which the variable is searched for
* @param name name of the variable that is searched for
* @return index of the given variable, -1 if variable could not be found
*/
int findEsoIndex(GenericEso::Ptr &eso, const std::string &name)
{
  int index = -1;
  const unsigned numVars = eso->getNumVariables();
  std::vector<std::string> names(numVars);
  eso->getVariableNames(names);
  for(unsigned i=0; i<names.size(); i++){
    if(names[i] == name){
      index = i;
    }
  }
  return index;
}

/**
* @brief factory function for a JadeGenericEso object loading the old car example
* @return pointer to the created GenericEso object
*/
GenericEso::Ptr createJadeOldCarExample()
{
  GenericEso::Ptr carExample (new JadeGenericEso("carFreeFinalTime"));
  return carExample;
}

/**
* @brief factory function for a JadeGenericEso object loading the car examle
* @return pointer to the created GenericEso object
*/
GenericEso::Ptr createJadeCarExample()
{
  GenericEso::Ptr carExample (new JadeGenericEso("carErweitert"));
  return carExample;
}

/**
* @brief factory function for a MetaSingleStageEso object
*
* @param genEso GenericEso object used to create the MetaSingleStageEso -
*               expected be the extended car example
* @return pointer to the created MetaSingleStageEso object
*/
IntegratorSingleStageMetaData::Ptr createCarIntegratorMetaData(GenericEso::Ptr &genEso)
{
  //initialize controls
  //the problem has only one control which is to be set up with a grid of 4 intervals
  const int numIntervals = 4;
  std::vector<double> parameterizationValues(numIntervals, 0.0);
  //the first and last interval are initialized with 2 and -2
  parameterizationValues.front() = 2.0;
  parameterizationValues.back() = -2.0;
  ParameterizationGrid::Ptr grid(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  grid->setAllParameterizationValues(parameterizationValues);

  std::vector<ParameterizationGrid::Ptr> paramGrid;
  paramGrid.push_back(grid);

  //initialize control
  Control accel;
  accel.setLowerBound(-2.0);
  accel.setUpperBound(2.0);
  accel.setControlGrid(paramGrid);

  std::vector<int> assignedIndices;
  genEso->getParameterIndex(assignedIndices.size(), &assignedIndices[0]);
  BOOST_REQUIRE_EQUAL(assignedIndices.size(), 1);
  accel.setEsoIndex(assignedIndices[0]);
  accel.setIsInvariant(false);

  std::vector<Control> controls;
  controls.push_back(accel);

  std::vector<InitialValueParameter::Ptr> initials;

  const double globalDuration = 4.0;
  DurationParameter::Ptr durationParameter(new DurationParameter());
  durationParameter->setParameterValue(globalDuration);

  std::vector<double> userGrid;

  IntegratorSingleStageMetaData::Ptr integratorMetaData (new IntegratorSingleStageMetaData(genEso,
                                                                              controls,
                                                                              initials,
                                                                              durationParameter,
                                                                              userGrid));

  return integratorMetaData;
}

/**
* @brief Creates an MetaSingleStageEso using a given GenericEso (old car example)
*
* Specifies the control VW.ACCEL and sets bounds and grids
* @param genEso pointer to an GenericEso object used to create the MetaSingleStageEso
* @return Pointer to the created MetaSingleStageEso object
*/
IntegratorSingleStageMetaData::Ptr createOldCarIntegratorMetaData(GenericEso::Ptr &genEso)
{
  const double globalDuration = 40.0;
  const double lowerBound = 10.0;
  const double upperBound = 100.0;
  DurationParameter::Ptr globalDurationParameter(new DurationParameter());
  globalDurationParameter->setParameterValue(globalDuration);
  globalDurationParameter->setParameterLowerBound(lowerBound);
  globalDurationParameter->setParameterUpperBound(upperBound);
  globalDurationParameter->setParameterSensActivity(true);
  globalDurationParameter->setWithSensitivity(true);

  //initialize controls
  //the problem has only one control which is to be set up with a grid of 4 intervals
  const int numIntervals = 10;
  std::vector<double> parameterizationValues(numIntervals, 0.0);
  ParameterizationGrid::Ptr grid(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  grid->setAllParameterizationValues(parameterizationValues);
  grid->setDurationPointer(globalDurationParameter);

  std::vector<ParameterizationGrid::Ptr> paramGrid;
  paramGrid.push_back(grid);

  //initialize control
  Control accel;
  accel.setLowerBound(-2.0);
  accel.setUpperBound(2.0);
  accel.setControlGrid(paramGrid);

  int assignedIndex = findEsoIndex(genEso, "accel");
  BOOST_REQUIRE(assignedIndex >= 0);
  accel.setEsoIndex(assignedIndex);
  accel.setIsInvariant(false);

  

  std::vector<InitialValueParameter::Ptr> initials;


  
  std::vector<Control> controls;
  controls.push_back(accel);
  std::vector<double> userGrid;

  IntegratorSingleStageMetaData::Ptr integratorMetaData (new IntegratorSingleStageMetaData(genEso,
                                                                              controls,
                                                                              initials,
                                                                              globalDurationParameter,
                                                                              userGrid));

  return integratorMetaData;
}

/**
* @brief factory function for a ForwardIntegratorNixe object
*
* @param integratorMetaData pointer to the MetaSingleStageEso object used to create the GenericIntegrator
* @return Tpointer to the created ForwardIntegratorNixe object
*/
GenericIntegrator::Ptr createNixe(IntegratorMetaData::Ptr &integratorMetaData)
{
  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  GenericIntegrator::Ptr integrator (new ForwardIntegratorNixe(integratorMetaData, options));
  return integrator;
}

/**
* @brief factory function for an OptimizerMetaData object
*
* This factory sets one state path constraint for the second equation defining five grid points.
* @param integratorMetaData pointer to the MetaSingleStageEso object used to create the OptimizerMetaData
* @return pointer to the created OptimizerMetaData object
*/
OptimizerMetaData::Ptr createOptimizerSingleStageMetaData(IntegratorSingleStageMetaData::Ptr &integratorMetaData)
{
  OptimizerSingleStageMetaData *optimizerMetaData = new OptimizerSingleStageMetaData(integratorMetaData);

  const unsigned numGridPoints = 5;
  std::vector<StatePointConstraint> constraints(numGridPoints);
  for(unsigned i=0; i<numGridPoints; i++){
    constraints[i].setEsoIndex(2);
    constraints[i].setLowerBound(0.0);
    constraints[i].setUpperBound(10.0);
    constraints[i].setTime(double(i)/(numGridPoints-1));
  }

  optimizerMetaData->setConstraints(constraints);

  StatePointConstraint objective;
  objective.setEsoIndex(3);
  objective.setLowerBound(-100000.0);
  objective.setUpperBound(100000.0);
  objective.setTime(1.0);
  optimizerMetaData->setObjective(objective);
  optimizerMetaData->calculateOptimizerProblemDimensions();

  OptimizerMetaData::Ptr retOptMD(optimizerMetaData);
  return retOptMD;
}

/**
* @brief factory function for an OptimizerMetaData object
*
* @param integratorMetaData pointer to the MetaSingleStageEso object used to create the OptimizerMetaData
* @param genEso pointer to the GenericEso with which the given MetaSingleStageEso was created. The object is used
*               for finding the indices of some variables. genEso must be a GenericEso for the old car example.
*/
OptimizerMetaData::Ptr createOptimizerOldCarSingleStageMetaData(IntegratorSingleStageMetaData::Ptr &integratorMetaData, GenericEso::Ptr &genEso)
{
  OptimizerSingleStageMetaData *optimizerMetaData = new OptimizerSingleStageMetaData(integratorMetaData);

  const int veloIndex = findEsoIndex(genEso, "velo");
  BOOST_REQUIRE(veloIndex >= 0);

  const int distIndex = findEsoIndex(genEso, "dist");
  BOOST_REQUIRE(distIndex >= 0);

  const int timIndex = findEsoIndex(genEso, "ttime");
  BOOST_REQUIRE(timIndex >= 0);

  // first set path cosntraint for VM.Velo
  // there should be 11 time points ([0.0, 0.1,...,1.0]
  // at point 0.0 no constraint is defined, at point 1.0 constraints are defined sperately
  // so only 9 points remain to be set for the path constraint
  const unsigned numInteriorPoints = 9;
  const unsigned numEndPoints = 2;
  std::vector<StatePointConstraint> constraints(numInteriorPoints + numEndPoints);
  for(unsigned i=0; i<numInteriorPoints; i++){
    constraints[i].setEsoIndex(veloIndex);
    constraints[i].setLowerBound(0.0);
    constraints[i].setUpperBound(10.0);
    constraints[i].setTime(double(i+1)/(numInteriorPoints+1));
  }

  //add endpoint constraints for VM.Velo and VM.DIST
  constraints[numInteriorPoints].setEsoIndex(veloIndex);
  constraints[numInteriorPoints].setLowerBound(0.0);
  constraints[numInteriorPoints].setUpperBound(0.0);
  constraints[numInteriorPoints].setTime(1.0);

  constraints.back().setEsoIndex(distIndex);
  constraints.back().setLowerBound(300.0);
  constraints.back().setUpperBound(300.0);
  constraints.back().setTime(1.0);

  optimizerMetaData->setConstraints(constraints);

  StatePointConstraint objective;
  objective.setEsoIndex(timIndex);
  objective.setLowerBound(-10000000.0);
  objective.setUpperBound(10000000.0);
  objective.setTime(1.0);
  optimizerMetaData->setObjective(objective);
  optimizerMetaData->calculateOptimizerProblemDimensions();

  OptimizerMetaData::Ptr retOptMD(optimizerMetaData);
  return retOptMD;
}

/**
* @brief factory function for a GenericOptimizer object
*
* @param genericEso Pointer to a GenericEso object used to create the GenericOptimizer object.
*                   This function expects generciEso to be the extended car model (Jade).
* @return pointer to the created GenericOptimizer object
*/
GenericOptimizer *createSNOPT(GenericEso::Ptr &genericEso)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData = createCarIntegratorMetaData(genericEso);

  IntegratorMetaDataFactory imdFactory;
  IntegratorMetaData::Ptr metaDataForIntegrator(
        imdFactory.createIntegratorMultiStageMetaDataWithSingleStage(integratorMetaData));

  GenericIntegrator::Ptr genericIntegrator = createNixe(metaDataForIntegrator);
  OptimizerMetaData::Ptr optimizerMetaData = createOptimizerSingleStageMetaData(integratorMetaData);
  OptimizationProblem::Ptr optimizationProblem(new FirstOrderOptimizationProblem(genericIntegrator, optimizerMetaData));
  SNOPTWrapper *optimizer = new SNOPTWrapper(optimizationProblem);
  return optimizer;
}

/**
* @brief factory function for a GenericOptimizer object
*
* to create the optimizer the old car example is initialized
* @return pointer to the created GenericOptimizer object
*/
GenericOptimizer *createSNOPTOldCar()
{
  GenericEso::Ptr genEso(createJadeOldCarExample());
  IntegratorSingleStageMetaData::Ptr integratorMetaData = createOldCarIntegratorMetaData(genEso);

  IntegratorMetaDataFactory imdFactory;
  IntegratorMetaData::Ptr metaDataForIntegrator(
        imdFactory.createIntegratorMultiStageMetaDataWithSingleStage(integratorMetaData));

  OptimizerMetaData::Ptr optimizerMetaData = createOptimizerOldCarSingleStageMetaData(integratorMetaData, genEso);
  GenericIntegrator::Ptr genericIntegrator = createNixe(metaDataForIntegrator);
  OptimizationProblem::Ptr optimizationProblem(new FirstOrderOptimizationProblem(genericIntegrator, optimizerMetaData));
  SNOPTWrapper *optimizer = new SNOPTWrapper(optimizationProblem);
  return optimizer;
}
