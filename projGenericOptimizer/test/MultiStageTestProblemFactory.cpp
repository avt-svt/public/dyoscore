/**
* @file MultiStageTestProblemFactory.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Factory file for a multi stage test problem used for an optimization \n
* system test.                                                         \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 11.04.2012
*/

#include <vector>
#include <string>

#include "boost/test/unit_test.hpp"

#include "JadeGenericEso.hpp"
#include "FirstOrderOptimizationProblem.hpp"
#include "IntegratorSingleStageMetaData.hpp"
#include "MetaDataFactories.hpp"
#include "MultiStageTestProblemFactory.hpp"
#include "SingleStageTestProblemFactory.hpp" //for function findEsoIndex


/**
* @brief create an JadeGenericEso pointer for multiStageTest
*/
GenericEso::Ptr createJadeMultiStageTestEso()
{
  GenericEso::Ptr eso(new JadeGenericEso("wobatchinactive"));
  return eso;
}

/**
* @brief create an IntegratorSingleStageMetaData for multiStageTest problem
*
* set up a stage for a multistage test problem creating three controls for 
* tw, fbin and tf
* @param eso GenericEso for multi stage test used in this stage (either Jade or FMI)
* @param twName string containing the name of the variable tw used in eso
* @param fbinName string containing the name of the variable fbin used in eso
* @param durationName string containing the name of the variable finaltiem used in eso
* @param numStages total number of stages later used in the multistage problem
* @return pointer to the created IntegratorSingleStageMetaData object
* @note variables may have different prefixes in Jade and FMI
*/
IntegratorSingleStageMetaData::Ptr createStage(GenericEso::Ptr eso,
                                    std::string &twName,
                                    std::string &fbinName,
                                    std::string &durationName,
                                    const unsigned numStages)
{
  std::vector<Control> controls(3);
  const unsigned numIntervals = 2;
  const double globalDurationValue = 1000.0;
  
  DurationParameter::Ptr globalDuration(new DurationParameter());
  globalDuration->setWithSensitivity(true);
  globalDuration->setParameterLowerBound(DURATION_LOWERBOUND_TOLERANCE);
  globalDuration->setParameterUpperBound(1.0);
  globalDuration->setParameterSensActivity(true);
  globalDuration->setParameterValue(1.0/numStages);

  //create control for tw
  {
    const int esoIndex = findEsoIndex(eso, twName);
    BOOST_REQUIRE(esoIndex >= 0);
    const double twMin = 0.02;
    const double twMax = 0.1;

    std::vector<ParameterizationGrid::Ptr> grids(1);
    grids[0] = ParameterizationGrid::Ptr(new ParameterizationGrid(numIntervals, PieceWiseConstant));
    std::vector<double> paramValues(numIntervals, twMin);
    grids[0]->setAllParameterizationValues(paramValues);
    grids[0]->setDurationPointer(globalDuration);
    controls[0].setEsoIndex(esoIndex);
    controls[0].setLowerBound(twMin);
    controls[0].setUpperBound(twMax);
    controls[0].setControlGrid(grids);
  }

  //create control for fbin
  {
    const int esoIndex = findEsoIndex(eso, fbinName);
    BOOST_REQUIRE(esoIndex >= 0);
    const double fbinMin = 0.0;
    const double fbinMax = 5.784;

    std::vector<ParameterizationGrid::Ptr> grids(1);
    grids[0] = ParameterizationGrid::Ptr(new ParameterizationGrid(numIntervals, PieceWiseConstant));
    std::vector<double> paramValues(numIntervals, fbinMax);
    grids[0]->setAllParameterizationValues(paramValues);
    grids[0]->setDurationPointer(globalDuration);


    controls[1].setEsoIndex(esoIndex);
    controls[1].setLowerBound(fbinMin);
    controls[1].setUpperBound(fbinMax);
    controls[1].setControlGrid(grids);
  }

  //create control for duration
  {
    const int esoIndex = findEsoIndex(eso, durationName);
    BOOST_REQUIRE(esoIndex >= 0);

    const double value = globalDurationValue;
    ControlFactory cFac;
    FactoryInput::ParameterInput pInput;
    pInput.esoIndex = esoIndex;
    pInput.lowerBound = value;
    pInput.upperBound = value;
    pInput.value = value;
    pInput.type = FactoryInput::ParameterInput::TIME_INVARIANT;
    pInput.sensType = FactoryInput::ParameterInput::NO;
    pInput.grids.resize(1);
    pInput.grids[0].duration = 1.0/numStages;
    controls[2] = cFac.createTimeInvariantParameter(pInput);
    controls[2].setControlWithoutSens();
  }

  //declare unused input
  std::vector<InitialValueParameter::Ptr> initials;

  //path constraint declared on a grid twice as fine as parameterization grid
  //unsigned numAdditionalGridpoints = numIntervals;
  const unsigned numAdditionalGridpoints = 2;
  std::vector<double> userGrid(numAdditionalGridpoints);
  for(unsigned i=0; i<numAdditionalGridpoints; i++){
    userGrid[i] = 1.0/(2*numIntervals) + 1.0/numIntervals*i;
  }

  IntegratorSingleStageMetaData::Ptr stage(new IntegratorSingleStageMetaData(eso,
                                                                  controls,
                                                                  initials,
                                                                  globalDuration,
                                                                  userGrid));
  return stage;
}

/**
* @brief create a stage using a JadeGenericEso
* @param numStages number of stages in the multistage problem for which the stage is created
* @return pointer to the created stage
*/
IntegratorSingleStageMetaData::Ptr createJadeStage(const unsigned numStages){
  GenericEso::Ptr eso = createJadeMultiStageTestEso();
  std::string twName = "TwCur";
  std::string fbinName = "FbinCur";
  std::string durationName = "FinalTime";
  return createStage(eso, twName, fbinName, durationName, numStages);
}

/**
* @brief create a vector of stages using JadeGenericEso
* @param numStages number of stages in the multistage problem for which the stage is created
* @return vector containing created stages (size is numStages)
*/
std::vector<IntegratorSingleStageMetaData::Ptr> createJadeStageVector(const unsigned numStages)
{
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  for(unsigned i=0; i<numStages; i++){
    stages[i] = createJadeStage(numStages);
  }
  return stages;
}

/**
* @brief create a IntegratorMultiStageMetaData object using a vector of single stage objects
*
* Full mapping will be applied to the  multi stage problem
* @param stages single stage objects used to create the multi stage meta data
* @return pointer to the created IntegratorMultiStageMetaData object
*/
IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaData
                                (std::vector<IntegratorSingleStageMetaData::Ptr> &stages)
{
  //create a full state mapping
  std::vector<Mapping::Ptr> mappings(stages.size()-1);
  const unsigned numStates = stages.front()->getGenericEso()->getNumStates();
  const unsigned numEquations = stages.front()->getGenericEso()->getNumEquations();
  std::vector<int> stateIndices(numStates);
  stages.front()->getGenericEso()->getStateIndex(stateIndices.size(), &stateIndices[0]);
  iiMap varIndexMap, eqnIndexMap;
  for(unsigned i=0; i<numStates; i++){
    varIndexMap[stateIndices[i]] = stateIndices[i];
  }
  for(unsigned i=0; i<numEquations; i++){
    eqnIndexMap[i] = i;
  }
  for(unsigned i=0; i<mappings.size(); i++){
    mappings[i] = Mapping::Ptr(new SingleShootingMapping());
    mappings[i]->setIndexMaps(varIndexMap, eqnIndexMap);
  }
  IntegratorMultiStageMetaData::Ptr intMSMD(new IntegratorMultiStageMetaData(mappings, stages));
  return intMSMD;
}

/**
* @create an OptimizerSingleStageMetaData object for a multi stage problem
*
* @param integratorMetaData IntegratorMetaData pointer of the corresponding stage
* @param pathConstraintName string containing the path constraint variable name 
* @param pathConstraintEquationIndex equation index of the path constraint
* @param endpointConstraintName string containing the endpoint constraint variable name
* @param endpoitnConstraintEquationIndex equation index of the endpoint constraint
* @param objective name string containing the objective variable name
* @param objectiveEquationIndex equation index of the objective
* @note variables may have different prefixes in Jade and FMI
*/
OptimizerSingleStageMetaData::Ptr createOptimizerStage(IntegratorSingleStageMetaData::Ptr integratorMetaData,
                                            std::string &pathConstraintName,
                                            unsigned pathConstraintEquationIndex,
                                            std::string &endpointConstraintName,
                                            unsigned endpointConstraintEquationIndex,
                                            std::string objectiveName,
                                            unsigned objectiveEquationIndex)
{
  OptimizerSingleStageMetaData::Ptr optMD(new OptimizerSingleStageMetaData(integratorMetaData));

  const unsigned numIntervals = 4;
  unsigned numEndPoints = 0; //path constraint is also defined at the endpoint, but not at t0
  //at the last stage define an additional endpoint constraint
  if(endpointConstraintName != ""){
    numEndPoints++;
  }

  std::vector<StatePointConstraint> constraints(numIntervals + numEndPoints);

  //define the path constraint
  GenericEso::Ptr integrator_eso(integratorMetaData->getGenericEso());
  const unsigned pathConstraintEsoIndex = findEsoIndex(integrator_eso,
                                                 pathConstraintName);
  BOOST_REQUIRE(pathConstraintEsoIndex >= 0);
  for(unsigned i=0; i<numIntervals; i++){
    constraints[i].setEsoIndex(pathConstraintEsoIndex);
    constraints[i].setLagrangeMultiplier(0.0);
    constraints[i].setLowerBound(60.0);
    constraints[i].setUpperBound(90.0);
    constraints[i].setTime(double(i+1)/numIntervals);
  }

  //define endpoint constraint for the last stage
  if(endpointConstraintName != ""){
    const unsigned endpointConstraintEsoIndex = findEsoIndex(integrator_eso,
                                                       endpointConstraintName);
    BOOST_REQUIRE(endpointConstraintEsoIndex >= 0);
    constraints.back().setEsoIndex(endpointConstraintEsoIndex);
    constraints.back().setLagrangeMultiplier(0.0);
    constraints.back().setLowerBound(0.0);
    constraints.back().setUpperBound(5.0);
    constraints.back().setTime(1.0);
  }

  optMD->setConstraints(constraints);

  //define objective
  StatePointConstraint objective;
  const unsigned objectiveEsoIndex = findEsoIndex(integrator_eso,
                                            objectiveName);
  BOOST_REQUIRE(objectiveEsoIndex >= 0);
  objective.setEsoIndex(objectiveEsoIndex);
  //objective is unbounded - todo set on lowest/highest real
  objective.setLowerBound(-1e20);
  objective.setUpperBound(1e20);
  objective.setTime(1.0);

  optMD->setObjective(objective);

  optMD->calculateOptimizerProblemDimensions();
  return optMD;
}

/**
* @brief create a vector of OptimizerSingleStageMetaData objects using Jade as Modelserver
*
* @param integratorStages stage vector created by function createJadeStageVector
* @return vector of OptimizerSingleStageMetaData pointers - has the same length as integratorStages
*/
std::vector<OptimizerSingleStageMetaData::Ptr> createJadeOptimizerStageVector
                                    (std::vector<IntegratorSingleStageMetaData::Ptr> &integratorStages)
{
  const unsigned numStages = integratorStages.size();
  std::vector<OptimizerSingleStageMetaData::Ptr> optimizerStages(numStages);

  std::string pathConstraintName = "X7";
  const unsigned pathConstraintEquationIndex = 6;

  std::string endpointConstraintName = "X8";
  const unsigned endpointConstraintEquationIndex = 7;

  std::string objectiveName = "X9";
  const unsigned objectiveEquationIndex = 8;

  //create first stages without endpoint constraints
  for(unsigned i=0; i<numStages-1; i++){
    std::string noEndpointConstraint = "";
    optimizerStages[i] = createOptimizerStage(integratorStages[i],
                                              pathConstraintName,
                                              pathConstraintEquationIndex,
                                              noEndpointConstraint, 0,
                                              objectiveName,
                                              objectiveEquationIndex);
  }
  //create last stage with endpoint constraint
  optimizerStages.back() = createOptimizerStage(integratorStages.back(),
                                                pathConstraintName,
                                                pathConstraintEquationIndex,
                                                endpointConstraintName,
                                                endpointConstraintEquationIndex,
                                                objectiveName,
                                                objectiveEquationIndex);
  return optimizerStages;
}

/**
* @brief create an OptimizerMultiStageMetaData object
*
* @param optSSMDVec vector of OptimizerMetaData stages
* @param intMSMD IntegratorMultiStageMetaData containing the corresponding IntegratorMetaData stages
* @return pointer to the created OptimizerMultiStageMetaData object
*/
OptimizerMetaData::Ptr createOptimizerMultiStageMetaData(std::vector<OptimizerSingleStageMetaData::Ptr> &optSSMDVec,
                                                         IntegratorMultiStageMetaData::Ptr intMSMD)
{

  //only objective of the last stage is taken into account
  std::vector<bool> treatObjective(optSSMDVec.size(), false);
  treatObjective.back() = true;
  OptimizerMultiStageMetaData *optMSMDPtr = new OptimizerMultiStageMetaData(optSSMDVec,
                                                                         intMSMD,
                                                                         treatObjective);
  //total time is fixed at 1.0
  optMSMDPtr->setTotalTimeConstraint(1.0, 1.0, 0.0);
  OptimizerMetaData::Ptr optimizerMetaData(optMSMDPtr);
  return optimizerMetaData;
}

/**
* @brief create a SNOPTWrapper object using Nixe as integrator and Jade as EsoServer
* @return pointer to the created GenericOptimizer object
*/
GenericOptimizer *createJadeNixeSNOPTMultiStageTestObject()
{
  const unsigned numStages = 1;
  std::vector<IntegratorSingleStageMetaData::Ptr> integratorStages = createJadeStageVector(numStages);
  std::vector<OptimizerSingleStageMetaData::Ptr> optSSMDVec = createJadeOptimizerStageVector(integratorStages);
  IntegratorMultiStageMetaData::Ptr intMSMD = createIntegratorMultiStageMetaData(integratorStages);
  OptimizerMetaData::Ptr optMD = createOptimizerMultiStageMetaData(optSSMDVec, intMSMD);
  IntegratorMetaData::Ptr integratorMetaData = intMSMD;
  GenericIntegrator::Ptr genInt = createNixe(integratorMetaData);
  OptimizationProblem::Ptr optProb(new FirstOrderOptimizationProblem(genInt, optMD));
  return new SNOPTWrapper(optProb);
}
