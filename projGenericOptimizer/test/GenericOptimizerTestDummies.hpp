/**
* @file GenericOptimizerTestDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer - Part of DyOS                                      \n
* =====================================================================\n
* Dummyclasses for GenericOptimizerTest                                \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 10.01.2012
*/

#pragma once

#include <cassert>
#include <float.h>

#include "GenericIntegrator.hpp"
#include "GenericIntegratorReverse.hpp"
#include "OptimizerMetaData.hpp"
#include "IntegratorSingleStageMetaData.hpp"

/**
* @class EmptyGenericIntegratorDummy
* @brief Dummy class doing nothing (for testing purpose)
*/
class EmptyGenericIntegratorDummy : public GenericIntegrator
{
public:
  /**
  * @brief since EmptyGenericIntegratorDummy is not used at all, the solve function is left empty
  */
  ResultFlag solve()
  {
    return SUCCESS;
  };

  /**
  * @brief since EmptyGenericIntegratorDummy is not used at all, the solveInterval function is left empty
  */
  ResultFlag solveInterval(bool isFirstInterval, unsigned stageIndex){return SUCCESS;};
  
  void activatePlotGrid(){};

  std::vector<DyosOutput::StageOutput> getOutput()
  {
    std::vector<DyosOutput::StageOutput> out;
    return out;
  }
};

class EmptyGenericIntegratorDummyReverse : public EmptyGenericIntegratorDummy, public GenericIntegratorReverse
{
  ResultFlag solve()
  {
    return SUCCESS;
  };
  
  ResultFlag solveForward()
  {
    return SUCCESS;
  }
  
  void solveReverse()
  {}
  
  virtual IntegratorMetaData::Ptr getIntegratorMDPtr() const
  {
    IntegratorMetaData::Ptr dummy;
    return dummy;
  }
  
  virtual DaeInitialization::Ptr getDaeInitializationPtr() const
  {
    DaeInitialization::Ptr dummy;
    return dummy;
  }

  virtual SolveStepResult solveStep(double& starttime,
                                    const double endtime,
                                    utils::Array<double> &finalStates,
                                    utils::Array<double> &finalSensitivities)
  {
    return GenericIntegratorForward::SolveStepSuccess;
  }
  
  virtual void solveStepReverse(double& reverseStartTime,
                                const double reverseEndTime,
                                utils::Array<double> &adjoints,
                                utils::Array<double> &lagrangeDerivatives){}

  virtual void evaluateAtInitialPointReverse(utils::Array<double> &adjoints,
                                             utils::Array<double> &lagrangeDerivatives){}
};

/**
* @class RosenbrockOptimizerMetaData
* @brief OptimizerMetaData prividing the metaData for the Rosenbrock optimization problem
*
* Rosenbrock problem:  minimize over x,y                   (x-1)�+100(y-x�)�
* To add slightly more complexity to the example add a path constraint: (x-1)� + (y-1)�<=4
* expected optimum will be at (1,1), objective after optimization should be 0
*/
class RosenbrockOptimizerMetaData : public OptimizerMetaData2ndOrder
{
protected:
  //! OptimizerProblemDimensions for the Rosenbrock problem
  OptimizerProblemDimensions m_optProbDim;
  //! optimization variable x
  double m_x;
  //! optimization variable y
  double m_y;
  //! dummy lagrange multiplier
  double m_lagrangeMultiplier;
  //! objective lagrange multiplier
  double m_objMultiplier;
public:
  /**
  * @brief standard constructor setting up constant data for the Rosenbrock problem
  */
  RosenbrockOptimizerMetaData()
  {

    // create nonlinear Jacobian matrix
    const int numConstraints = 1;
    const int numJacobianVariables = 2;
    const int numNonZeroJacobianEntries = 2;
    utils::Array<int> rowIndices(numNonZeroJacobianEntries, 0);
    utils::Array<int> colIndices(numNonZeroJacobianEntries, 0);
    utils::Array<double> values(numNonZeroJacobianEntries, 0.0);
    
    // only one constraint, so row index is 0
    rowIndices[0] = 0;
    rowIndices[1] = 0;
    // both nonlinear Jacobian variables (x and y) are in the constraint
    colIndices[0] = 0;
    colIndices[1] = 1;
    m_optProbDim.nonLinJacDecVar = CsTripletMatrix::Ptr(new CsTripletMatrix(numConstraints,
                                                                            numJacobianVariables,
                                                                            numNonZeroJacobianEntries,
                                                                            rowIndices.getData(),
                                                                            colIndices.getData(),
                                                                            values.getData()));

     //in single stagte the linear Jacobian is empty
    m_optProbDim.linJac = CsTripletMatrix::Ptr(new CsTripletMatrix(0,
                                                                   numJacobianVariables,
                                                                   0,
                                                                   rowIndices.getData(),
                                                                   colIndices.getData(),
                                                                   values.getData()));

    // objective index is 0 (since the first equation is the objective)
    m_optProbDim.nonLinObjIndex = 0;
    // no linear constraints defined
    m_optProbDim.numLinConstraints = 0;
    // one nonlinear constraint defined
    m_optProbDim.numNonLinConstraints = 1;
    // optimization variables are x and y
    m_optProbDim.numOptVars = 2;

    // start with an infeasible point
    m_x = 7.0;
    m_y = 1.2;

    m_lagrangeMultiplier = 0.0;
  }

  /**
  * @brief destructor
  */
  ~RosenbrockOptimizerMetaData()
  {
  }

  /**
  * @copydoc OptimizerMetaData::getOptProbDim
  */
  virtual const OptimizerProblemDimensions &getOptProbDim() const
  {
    return m_optProbDim;
  };

  /**
  * @copydoc OptimizerMetaData::getDecVarBounds
  */
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const
  {
    const int numOptVars = m_optProbDim.numOptVars;
    assert(numOptVars == 2);
    assert(upperBounds.getSize() == (unsigned)numOptVars);
    assert(lowerBounds.getSize() == (unsigned)numOptVars);

    //set bounds for x
    lowerBounds[0] = -5.0;
    upperBounds[0] = 5.0;

    //set bounds for y
    lowerBounds[1] = -3.0;
    upperBounds[1] = 1.0;
  };

  /**
  * @copydoc OptimizerMetaData::getDecVarValues
  */
  virtual void getDecVarValues(utils::Array<double> &values) const
  {
    assert(values.getSize() == 2);

    values[0] = m_x;
    values[1] = m_y;
  };

  /**
  * @copydoc OptimizerMetaData::setDecVarValues
  */
  virtual void setDecVarValues(const utils::Array<const double> &decVars)
  {
    assert(decVars.getSize() == 2);
    m_x = decVars[0];
    m_y = decVars[1];
  };

  /**
  * @copydoc OptimizerMetaData::getConstraintBounds
  */
  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const
  {
    const unsigned numNonLinConstraints = m_optProbDim.numNonLinConstraints;
    assert(1 == numNonLinConstraints);
    assert(lowerBounds.getSize() == numNonLinConstraints);
    assert(upperBounds.getSize() == numNonLinConstraints);

    lowerBounds[0] = -DYOS_DBL_MAX;
    upperBounds[0] = 4.0;
  };

  /**
  * @copydoc OptimizerMetaData::getNonLinConstraintValues
  */
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const
  {
    const double x = m_x;
    const double y = m_y;
    const unsigned numNonLinConstraints = m_optProbDim.numNonLinConstraints;
    assert(numNonLinConstraints == 1);
    assert(values.getSize() == numNonLinConstraints);
    //(x-1)� + (y-1)�
    values[0] = (x-1)*(x-1) + (y-1)*(y-1);
  };

  /**
  * @copydoc OptimizerMetaData::getNonLinConstraintDerivativesDecVar
  */
  virtual void getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const
  {
    assert(derivativeMatrix->get_nnz() == m_optProbDim.nonLinJacDecVar->get_nnz());
    //derivative of constraint wrt x is 2(x-1)
    derivativeMatrix->get_matrix_ptr()->x[0] = 2*(m_x-1);
    //derivative of constraint wrt y is 2(y-1)
    derivativeMatrix->get_matrix_ptr()->x[1] = 2*(m_y-1);
  };

   /**
  * @copydoc OptimizerMetaData::getNonLinConstraintDerivativesConstParam
  *
  * No constant parameters in this problem
  */
  virtual void getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr &derivativeMatrix) const
  {
    //provide a dummy triplet matrix
    int size = 1;
    int dummy_rows = 2;
    int dummy_cols = 0;
    int dummy_nnz = 0;
    utils::Array<int> dummy_row_idx(size,0), dummy_col_idx(size,0);
    utils::Array<double> dumm_vals(size,0.0);

    derivativeMatrix = CsTripletMatrix::Ptr( new CsTripletMatrix(dummy_rows,dummy_cols,
      dummy_nnz, dummy_row_idx.getData(), dummy_col_idx.getData(), dumm_vals.getData()));
  }

  /**
  * @copydoc OptimizerMetaData::getNonLinObjValue
  */
  virtual double getNonLinObjValue() const
  {
    const double x = m_x;
    const double y = m_y;
    //objective: (x-1)�+100(y-x�)�
    return (x-1)*(x-1) + 100*(y - x*x)*(y - x*x);
  };

   /**
  * @copydoc OptimizerMetaData::getNonLinObjDerivative
  */
  virtual void getNonLinObjDerivative(utils::Array<double> &values) const
  {
    const double x = m_x;
    const double y = m_y;
    assert(m_optProbDim.numOptVars == 2);
    assert(values.getSize() == m_optProbDim.numOptVars);
    //derivative of objective wrt x is 2(x-1) + 100*2*(y-x�)*(-2x)
    values[0] = 2*(x-1) - 400*x*(y - x*x);
    //derivative of objective wrt y is 100*2*(y-x�)*1
    values[1] = 200*(y - x*x);
  };

   /**
  * @copydoc OptimizerMetaData::setLagrangeMultipliers
  */
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers)
  {
    assert(m_optProbDim.numNonLinConstraints == 1);
    assert(nonLinMultipliers.getSize() == m_optProbDim.numNonLinConstraints);
    m_lagrangeMultiplier = nonLinMultipliers[0];
  };

   /**
  * @copydoc OptimizerMetaData::getLagrangeMultipliers
  */
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const
  {
    assert(m_optProbDim.numNonLinConstraints == 1);
    assert(nonLinMultipliers.getSize() == m_optProbDim.numNonLinConstraints);
    nonLinMultipliers[0] = m_lagrangeMultiplier;
  };

  virtual void getLinConstraintValues(utils::Array<double> &values)const{};
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds, utils::Array<double> &upperBounds)const{};
  virtual std::vector<DyosOutput::StageOutput> getOutput()
  {
    std::vector<DyosOutput::StageOutput> out;
    return out;
  }

  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier) {}

  virtual void getNonLinConstraintHessianDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const
  {
    int size = 2;
    int dummy_rows = size;
    int dummy_cols = size;
    int dummy_nnz = 4;
    utils::Array<int> dummy_row_idx(dummy_nnz), dummy_col_idx(dummy_nnz);
    utils::Array<double> dumm_vals(dummy_nnz);
    dummy_row_idx[0] = 0; dummy_row_idx[1] = 0; dummy_row_idx[2] = 1; dummy_row_idx[3] = 1;
    dummy_col_idx[0] = 0; dummy_col_idx[1] = 1; dummy_col_idx[2] = 0; dummy_col_idx[3] = 1;

    dumm_vals[0] = m_objMultiplier * (2 - 400 * m_y + 1200 * m_x * m_x) + m_lagrangeMultiplier *2;
    dumm_vals[1] = m_objMultiplier * (-400 * m_x);
    dumm_vals[2] = m_objMultiplier * (-400 * m_x);
    dumm_vals[3] = m_objMultiplier * (200) + m_lagrangeMultiplier *2;

    derivativeMatrix = CsTripletMatrix::Ptr( new CsTripletMatrix(dummy_rows,dummy_cols,
      dummy_nnz, dummy_row_idx.getData(), dummy_col_idx.getData(), dumm_vals.getData()));

  }

  virtual void getNonLinConstraintHessianConstParam(CsTripletMatrix::Ptr &nonLinConstGradients) const
  {
    //use dummy matrix from getNonLinConstraintDerivativesConstParam
    getNonLinConstraintDerivativesConstParam(nonLinConstGradients);
  }

  virtual void getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
    CsTripletMatrix::Ptr &adjoints2ndOrder) const
  {
    int size = 1;
    int dummy_rows = size;
    int dummy_cols = size;
    int dummy_nnz = size;
    utils::Array<int> dummy_row_idx(size,0), dummy_col_idx(size,0);
    utils::Array<double> dumm_vals(size,0.0);
    adjoints1stOrder.resize(size, 0.0);

    adjoints2ndOrder = CsTripletMatrix::Ptr( new CsTripletMatrix(dummy_rows,dummy_cols,
      dummy_nnz, dummy_row_idx.getData(), dummy_col_idx.getData(), dumm_vals.getData()));
  }
  virtual void getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                     CsTripletMatrix::Ptr &adjoints2ndOrder) const
  {
    int size = 1;
    int dummy_rows = size;
    int dummy_cols = size;
    int dummy_nnz = size;
    utils::Array<int> dummy_row_idx(size,0), dummy_col_idx(size,0);
    utils::Array<double> dumm_vals(size,0.0);
    adjoints1stOrder.resize(size, 0.0);

    adjoints2ndOrder = CsTripletMatrix::Ptr( new CsTripletMatrix(dummy_rows,dummy_cols,
      dummy_nnz, dummy_row_idx.getData(), dummy_col_idx.getData(), dumm_vals.getData()));
  }

  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier){
    m_objMultiplier = lagrangeMultiplier;
  }


};
