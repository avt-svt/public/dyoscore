/**
* @file GenericOptimizerTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Test of class GenericOptimizer                                       \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 11.04.2012
*/

#include <vector>
#include "SolverConfig.hpp"
#include <iostream>
/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE GenericOptimizer
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>

/**
* @def THRESHOLD
* @brief general threshold for BOOST_*_CLOSE macros
*/
#define THRESHOLD 1e-16

#ifdef BUILD_SNOPT
#include "SNOPTWrapper.hpp"
#endif
#ifdef BUILD_NPSOL
#include "NPSOLWrapper.hpp"
#endif

//Testsuites
#include "SimulationTestsuite.hpp"

#ifdef BUILD_SNOPT
#include "SNOPTTestsuite.hpp"
#include "SingleStageSystemTestsuite.hpp"
#include "MultiStageSystemTestsuite.hpp"
#endif

#ifdef BUILD_NPSOL
#include "NPSOLTestsuite.hpp"
#endif

#ifdef BUILD_FILTERSQP
#include "filterSQPTestsuite.hpp"
#endif

#ifdef BUILD_IPOPT
#include "ipoptTestsuite.hpp"
#endif
