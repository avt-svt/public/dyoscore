#include "MultiScenarioTest.cpp"

static double TOL = 1e-12;

OptimizationProblem::Ptr createMultiScenarioProblem()
{
  std::vector<int> shared(1, 1);
  MultiScenarioSet::Ptr opt_set(new MultiScenarioSet());
  OptimizationProblem::Ptr opt1(new SmallTestProblem(1.0));
  OptimizationProblem::Ptr opt2(new SmallTestProblem(2.0));
  opt_set->addScenario(opt1, shared);
  opt_set->addScenario(opt2, shared);
  MultiScenarioOptProblem::Ptr opt(new MultiScenarioOptProblem(opt_set));
  return opt;
}


BOOST_AUTO_TEST_SUITE(TestSmallProblem)
/* This tests the class used for testing ;) */
BOOST_AUTO_TEST_CASE(testSmallProblem_getoptprobdim)
{
  OptimizationProblem::Ptr opt1(new SmallTestProblem(1.0));
  const OptimizerProblemDimensions& opd = opt1->getOptProbDim();
  BOOST_CHECK_EQUAL(opd.numOptVars,2);
  BOOST_CHECK_EQUAL(opd.numLinConstraints,1);
  BOOST_CHECK_EQUAL(opd.numNonLinConstraints,1);
}

BOOST_AUTO_TEST_CASE(testSmallProblem_getdecvarbounds)
{
  OptimizationProblem::Ptr opt1(new SmallTestProblem(1.0));
  std::vector<double> lb(2);
  std::vector<double> ub(2);
  opt1->getDecVarBounds(lb, ub);
  BOOST_CHECK_EQUAL(lb[0], 0.0);
  BOOST_CHECK_EQUAL(lb[1], -1.0);
  BOOST_CHECK_EQUAL(ub[0], 100.0);
  BOOST_CHECK_EQUAL(ub[1], 200.0);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestMultiScenarioOptimizationProblem)

BOOST_AUTO_TEST_CASE(testMultiScenarioOptProbDim)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  const OptimizerProblemDimensions& opd = opt->getOptProbDim();
  BOOST_CHECK_EQUAL(opd.numOptVars, 3);
  BOOST_CHECK_EQUAL(opd.numLinConstraints, 2);
  BOOST_CHECK_EQUAL(opd.numNonLinConstraints, 2);
}

BOOST_AUTO_TEST_CASE(testMultiScenarioLinJac)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  const OptimizerProblemDimensions& opd = opt->getOptProbDim();
  BOOST_CHECK(CS_TRIPLET(opd.linJac));
  BOOST_CHECK_EQUAL(opd.linJac->nzmax,4);
  BOOST_CHECK_EQUAL(opd.linJac->m,2);
  BOOST_CHECK_EQUAL(opd.linJac->n,3);
  BOOST_CHECK_EQUAL(opd.linJac->nz,4);
  BOOST_CHECK_EQUAL(opd.linJac->i[0],0);
  BOOST_CHECK_EQUAL(opd.linJac->p[0],0);
  BOOST_CHECK_EQUAL(opd.linJac->x[0],1.0);
  BOOST_CHECK_EQUAL(opd.linJac->i[1],0);
  BOOST_CHECK_EQUAL(opd.linJac->p[1],2);
  BOOST_CHECK_EQUAL(opd.linJac->x[1],1.0);
  BOOST_CHECK_EQUAL(opd.linJac->i[2],1);
  BOOST_CHECK_EQUAL(opd.linJac->p[2],1);
  BOOST_CHECK_EQUAL(opd.linJac->x[2],2.0);
  BOOST_CHECK_EQUAL(opd.linJac->i[3],1);
  BOOST_CHECK_EQUAL(opd.linJac->p[3],2);
  BOOST_CHECK_EQUAL(opd.linJac->x[3],1.0);
}

BOOST_AUTO_TEST_CASE(testMultiScenarioNonLinJac)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  const OptimizerProblemDimensions& opd = opt->getOptProbDim();
  BOOST_CHECK(CS_TRIPLET(opd.nonLinJac));
  BOOST_CHECK_EQUAL(opd.nonLinJac->nzmax,4);
  BOOST_CHECK_EQUAL(opd.nonLinJac->m,2);
  BOOST_CHECK_EQUAL(opd.nonLinJac->n,3);
  BOOST_CHECK_EQUAL(opd.nonLinJac->nz,4);
  BOOST_CHECK_EQUAL(opd.nonLinJac->i[0],0);
  BOOST_CHECK_EQUAL(opd.nonLinJac->p[0],0);
  BOOST_CHECK_EQUAL(opd.nonLinJac->i[1],0);
  BOOST_CHECK_EQUAL(opd.nonLinJac->p[1],2);
  BOOST_CHECK_EQUAL(opd.nonLinJac->i[2],1);
  BOOST_CHECK_EQUAL(opd.nonLinJac->p[2],1);
  BOOST_CHECK_EQUAL(opd.nonLinJac->i[3],1);
  BOOST_CHECK_EQUAL(opd.nonLinJac->p[3],2);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getDecVarBounds)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> lb(3);
  std::vector<double> ub(3);
  opt->getDecVarBounds(lb, ub);
  BOOST_CHECK_EQUAL(lb[0], 0.0);
  BOOST_CHECK_EQUAL(lb[1], 0.0);
  BOOST_CHECK_EQUAL(lb[2], -1.0);
  BOOST_CHECK_EQUAL(ub[0], 100.0);
  BOOST_CHECK_EQUAL(ub[1], 100.0);
  BOOST_CHECK_EQUAL(ub[2], 200.0);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getsetDecVarValues)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> in(3);
  in[0] = 1.0;
  in[1] = 2.0;
  in[2] = 3.0;
  opt->setDecVarValues(in);
  std::vector<double> out(3);
  opt->getDecVarValues(out);
  for (int k=0; k<3; ++k) {
    BOOST_CHECK_EQUAL(in[k],out[k]);
  }
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getNonLinConstraintBounds)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> lb(2), ub(2);
  opt->getNonLinConstraintBounds(lb,ub);
  BOOST_CHECK_EQUAL(lb[0], 0.0);
  BOOST_CHECK_EQUAL(lb[1], 0.0);
  BOOST_CHECK_EQUAL(ub[0], 10.0);
  BOOST_CHECK_EQUAL(ub[1], 10.0);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getNonLinConstraintDerivatives)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  const OptimizerProblemDimensions& opd = opt->getOptProbDim();
  cs* derivatives = cs_spalloc(opd.nonLinJac->m, opd.nonLinJac->n, opd.nonLinJac->nzmax, 1, 1);
  cs_free(derivatives->i);
  cs_free(derivatives->p);
  derivatives->i = opd.nonLinJac->i;
  derivatives->p = opd.nonLinJac->p;
  derivatives->nz = opd.nonLinJac->nz;
  BOOST_CHECK_EQUAL(derivatives->nz, 4);
  std::vector<double> v(3);
  v[0] = 1.1; v[1] = 4.76; v[2] = 1.252;
  opt->setDecVarValues(v);
  opt->getNonLinConstraintDerivatives(derivatives);
  int* row = derivatives->i;
  int* col = derivatives->p;
  double* val = derivatives->x;
  BOOST_CHECK_EQUAL(row[0], 0);
  BOOST_CHECK_EQUAL(row[1], 0);
  BOOST_CHECK_EQUAL(row[2], 1);
  BOOST_CHECK_EQUAL(row[3], 1);
  BOOST_CHECK_EQUAL(col[0], 0);
  BOOST_CHECK_EQUAL(col[1], 2);
  BOOST_CHECK_EQUAL(col[2], 1);
  BOOST_CHECK_EQUAL(col[3], 2);
  BOOST_CHECK_CLOSE(val[0], v[2], 1e-8);
  BOOST_CHECK_CLOSE(val[1], v[0], 1e-8);
  BOOST_CHECK_CLOSE(val[2], 2.0*v[2], 1e-8);
  BOOST_CHECK_CLOSE(val[3], 2.0*v[1], 1e-8);
  derivatives->i = NULL;
  derivatives->p = NULL;
  cs_spfree(derivatives);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getLinConstraintBounds)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> lb(2), ub(2);
  opt->getLinConstraintBounds(lb, ub);
  BOOST_CHECK_EQUAL(lb[0], 0.0);
  BOOST_CHECK_EQUAL(lb[1], 0.0);
  BOOST_CHECK_EQUAL(ub[0], 10.0);
  BOOST_CHECK_EQUAL(ub[1], 10.0);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getLinConstraintValues)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> in(3), values(2);
  in[0] = 5.24; in[1] = 7.23; in[2] = 1.2127;
  opt->setDecVarValues(in);
  opt->getLinConstraintValues(values);
  BOOST_CHECK_CLOSE(values[0], in[0] + in[2], 1e-12);
  BOOST_CHECK_CLOSE(values[1], 2*in[1] + in[2], 1e-12);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getNonLinObjValue)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> in(3);
  in[0] = 5.24; in[1] = 7.23; in[2] = 1.2127;
  opt->setDecVarValues(in);
  double obj = opt->getNonLinObjValue();
  BOOST_CHECK_CLOSE(obj, 0.5*(in[0]+in[1]) + in[2], 1e-12);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getNonLinObjDerivative)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> in(3), out(3);
  in[0] = 5.24; in[1] = 7.23; in[2] = 1.2127;
  opt->setDecVarValues(in);
  opt->getNonLinObjDerivative(out);
  BOOST_CHECK_CLOSE(out[0], 0.5, TOL);
  BOOST_CHECK_CLOSE(out[1], 0.5, TOL);
  BOOST_CHECK_CLOSE(out[2], 1.0, TOL);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_getsetLagrangeMultipliers)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  std::vector<double> nonlin(2), lin(2);
  nonlin[0] = 7.66; nonlin[1] = 1.236;
  lin[0] = 6.76; lin[1] = 2.36;
  opt->setLagrangeMultipliers(nonlin, lin);

  std::vector<double> out_nonlin(2), out_lin(2);
  opt->getLagrangeMultipliers(out_nonlin, out_lin);
  BOOST_CHECK_EQUAL(nonlin[0], out_nonlin[0]);
  BOOST_CHECK_EQUAL(nonlin[1], out_nonlin[1]);
  BOOST_CHECK_EQUAL(lin[0], out_lin[0]);
  BOOST_CHECK_EQUAL(lin[1], out_lin[1]);
}

BOOST_AUTO_TEST_CASE(testMultiScenario_Optimize)
{
  OptimizationProblem::Ptr opt = createMultiScenarioProblem();
  GenericOptimizer::Ptr optimizer(new SNOPTWrapper(opt));
  optimizer->solve();
}

BOOST_AUTO_TEST_SUITE_END()
