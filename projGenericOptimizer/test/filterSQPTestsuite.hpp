/**
* @file GenericOptimizerTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Test of class GenericOptimizer                                       \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 22.11.2012
*/

#include "FilterSQPLoader.hpp"
#include "FilterSQPWrapper.hpp"
#include "SecondOrderOptimizationProblem.hpp"
#include <boost/shared_ptr.hpp>

BOOST_AUTO_TEST_SUITE(TestGenericOptimizerWithFilterSQP)

/**
* @brief GenericOptimizerTest - test hardcoded Rosenbrock problem
*
* remark: functionPrecision is set down to 1e-3 from 1e-8.
*         1e-3 is the next possible functionPrecision, so the RosenbrockTest can run through.
*/
BOOST_AUTO_TEST_CASE(SimpleDLLLoadTest)
{
  BOOST_CHECK_NO_THROW(FilterSQPLoader filterSQP);
}

BOOST_AUTO_TEST_CASE(RosenbrockTest)
{
  GenericIntegratorReverse::Ptr dummyIntegrator (new EmptyGenericIntegratorDummyReverse());
  OptimizerMetaData2ndOrder::Ptr rosenbrockOmd (new RosenbrockOptimizerMetaData());
  OptimizationProblem::Ptr optProblem(new SecondOrderOptimizationProblem(dummyIntegrator, rosenbrockOmd));
  FilterSQPWrapper filterSQP(optProblem);
  DyosOutput::OptimizerOutput optOut = filterSQP.solve();
  //precision set in the snopt.config file
  const double functionPrecision = 1e-8;
  BOOST_CHECK_CLOSE(optOut.lagrangeMultiplier[0], 0.0, THRESHOLD);
  BOOST_CHECK_CLOSE(optOut.optDecVarValues[0], 1.0, functionPrecision);
  BOOST_CHECK_CLOSE(optOut.optDecVarValues[1], 1.0, functionPrecision);
}

BOOST_AUTO_TEST_SUITE_END()
