/** 
* @file MultiStageTestProblemFactory.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Header of factory file for a multi stage test problem used for an    \n
* optimization system test.                                            \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 11.04.2012
*/

#pragma once

#include "GenericEso.hpp"
#include "IntegratorMultiStageMetaData.hpp"
#include "OptimizerMultiStageMetaData.hpp"
#include "SNOPTWrapper.hpp"
#include "ForwardIntegratorNixe.hpp"

GenericEso::Ptr createJadeMultiStageTestEso();

IntegratorSingleStageMetaData::Ptr createStage(GenericEso::Ptr eso,
                                    std::string &twName,
                                    std::string &fbinName,
                                    const unsigned numStages);
IntegratorSingleStageMetaData::Ptr createJadeStage(const unsigned numStages);
std::vector<IntegratorSingleStageMetaData::Ptr> createJadeStageVector(const unsigned numStages);

IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaData
                                (std::vector<IntegratorSingleStageMetaData::Ptr> &stages);

OptimizerSingleStageMetaData::Ptr createOptimizerStage(IntegratorSingleStageMetaData::Ptr integratorMetaData,
                                            std::string &pathConstraintName,
                                            unsigned pathConstraintEquationIndex,
                                            std::string &endpointConstraintName,
                                            unsigned endpointConstraintEquationIndex,
                                            std::string objectiveName,
                                            unsigned objectiveEquationIndex);

std::vector<OptimizerSingleStageMetaData::Ptr> createJadeOptimizerStageVector
                                    (std::vector<IntegratorSingleStageMetaData::Ptr> &integratorStages);

GenericOptimizer *createJadeNixeSNOPTMultiStageTestObject();
