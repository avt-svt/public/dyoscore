/** 
* @file MultiStageSystem.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* A complete system test is run here concerning multi stage mode. The  \n
* system test tests some combinations of integrators, optimizers and   \n
* Eso servers performing a complete optimization.                      \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 12.04.2012
*/

#include "MultiStageTestProblemFactory.hpp"


BOOST_AUTO_TEST_SUITE(OptimizerMultiStageSystemTest)

/**
* @test OptimizerMultiStageSystemTest - test optimization using SNOPT, Nixe and Jade
*/
BOOST_AUTO_TEST_CASE(TestJadeNixeSNOPT)
{
  GenericOptimizer *testObj = createJadeNixeSNOPTMultiStageTestObject();
  DyosOutput::OptimizerOutput out = testObj->solve();
  BOOST_CHECK(out.m_informFlag != DyosOutput::OptimizerOutput::FAILED);
  BOOST_CHECK(out.m_informFlag != DyosOutput::OptimizerOutput::WRONG_GRADIENTS);
  BOOST_CHECK(out.m_informFlag != DyosOutput::OptimizerOutput::INFEASIBLE);
  delete testObj;
}

BOOST_AUTO_TEST_SUITE_END()