/*!
* @file DaeInitialization.hpp
* @brief header for the DaeInitialization interface
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the abstact DaeInitialization class                   \n
* Contains alos the definitions of classes NoDaeInitialization and     \n
* SolverDaeInitialization                                              \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 01.10.2012
*/


#pragma once

#include "SolverConfig.hpp"
#include "HaveFmuConfig.hpp"
#include "GenericEso.hpp"
//#include "boost/shared_ptr.hpp"
#include <memory>

/**
* @class DaeInitialization
* @brief class managing the DAE initialization of an GenericEso object
*/
class DaeInitialization
{
public:
  typedef std::shared_ptr<DaeInitialization> Ptr;
  
  /// result flag of the initialization
  enum ResultFlag{
    OK, ///< initialization succeeded
    NOT_INITIALIZED, ///< no initialization performed
    FAILED, ///< solver has reported an error
    PRECISION_TOO_LOW, ///< initialization result not sufficient for the set precision
    SYSTEM_NOT_SEMI_EXPLICIT  ///< system is not semi explicit and cannot be solved
  };
  
  /// result data of the initialization
  struct InitializationResult{
    double maxAbsError; ///< maximum absolute error
    ResultFlag flag;///< result flag of the initialization
  };
  
  /**
  * @brief initialize the algebraic variables of the given GenericEso pointer
  * @param[in,out] genericEso pointer to the GenericEso that is to be initialized
  * @return result data of the initialization
  */
  virtual InitializationResult initializeAlgebraicVariables(const GenericEso::Ptr &genericEso) const = 0;
  
  /**
  * @brief initialize the dae derivatives of the given GenericEso pointer
  * @param[in,out] genericEso pointer to the GenericEso that is to be initialized
  * @return result data of the initialization
  */
  virtual InitializationResult initializeDaeDerivatives(const GenericEso::Ptr &genericEso) const = 0;
};

/**
* @class NoDaeInitialization
* @brief this class is used, if no dae initialization is to be done
*/
class NoDaeInitialization : public DaeInitialization
{
public:
  typedef std::shared_ptr<NoDaeInitialization> Ptr;
  
  /// @brief standard constructor
  NoDaeInitialization(){}
  /**
  * @brief copy constructor
  * @param[in] toCopy object to be copied
  */
  NoDaeInitialization(const NoDaeInitialization &toCopy){}
  /// destructor
  virtual ~NoDaeInitialization(){}
  
 
  /**
  * @brief no initialization is done
  * @param[in] genericEso pointer of the original interface - unused
  * @return result struct with flag set to NOT_INITIALIZED
  */
  DaeInitialization::InitializationResult initializeAlgebraicVariables(const GenericEso::Ptr &genericEso) const
  {
    DaeInitialization::InitializationResult ir;
    ir.flag = DaeInitialization::NOT_INITIALIZED;
    return ir;
  }
  
  /**
  * @copydoc DaeInitialization::InitializationResult
  */
  DaeInitialization::InitializationResult initializeDaeDerivatives(const GenericEso::Ptr &genericEso) const
  {
    DaeInitialization::InitializationResult ir;
    ir.flag = DaeInitialization::NOT_INITIALIZED;
    return ir;
  }
};