/** 
* @file Ma28Solver.hpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the linear solver class Ma28Solver                    \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 04.10.2012
*/
#pragma once

#include "Solver.hpp"

/**
* @class Ma28Solver
* @brief wrapper class for the linear solver Ma28
*/
class Ma28Solver : public LinearSolver
{
protected:
  
public:
  typedef std::shared_ptr<Ma28Solver> Ptr;
  
  Ma28Solver();
  virtual ~Ma28Solver();
  
  virtual LinearSolver::ResultFlag solve(const CsTripletMatrix::Ptr &matrix, utils::Array<double> &xb) const;
};