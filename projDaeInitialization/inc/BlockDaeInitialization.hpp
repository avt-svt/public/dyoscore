/*!
* @file BlockDaeInitialization.hpp
* @brief header for the BlockDaeInitialization class
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the BlockDaeInitialization class                      \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 01.10.2012
*/

#pragma once

#include "FullDaeInitialization.hpp"
#include "boost/shared_ptr.hpp"
#include <memory>

/**
* @class BlockDaeInitialization 
* @brief DaeInitialization class using block decomposition for
*        initialization of algebraic variables
*/
class BlockDaeInitialization : public FullDaeInitialization
{
protected:
  BlockDaeInitialization();
  
  std::vector<unsigned> getTripletIndices(const CsCscMatrix::Ptr &jac,
                                          const utils::Array<int> &rowPerm,
                                          const utils::Array<int> &colPerm) const;
public:
  typedef std::shared_ptr<BlockDaeInitialization> Ptr;
  BlockDaeInitialization(const LinearSolver::Ptr &linSolver,
                         const NonLinearSolver::Ptr &nonLinSolver,
                         const double maxErrorTolerance);
  virtual ~BlockDaeInitialization();
  
  virtual DaeInitialization::InitializationResult initializeAlgebraicVariables
                                                  (const GenericEso::Ptr &genericEso) const;

};
