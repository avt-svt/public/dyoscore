/**
* @file FmiDaeInitialization.hpp
* @brief header for the FmiDaeInitialization class
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Lehrstuhl für Systemverfahrenstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the FmiDaeInitialization class                       \n
* =====================================================================\n
* @author Adrian Caspari
* @date 22.03.2018
*/

#pragma once

#include "DaeInitialization.hpp"

#include <vector>

/**
* @class FullDaeInitialization
* @brief DaeInitialization class for Fmi initialization
*/
class FmiDaeInitialization : public DaeInitialization
{
protected:

 

public:

  typedef std::shared_ptr<FmiDaeInitialization> Ptr;
  
  FmiDaeInitialization();

  virtual ~FmiDaeInitialization();
  
  virtual DaeInitialization::InitializationResult initializeAlgebraicVariables
                                                  (const GenericEso::Ptr &genericEso) const ;
  virtual DaeInitialization::InitializationResult initializeDaeDerivatives
                                                  (const GenericEso::Ptr &genericEso) const;
};