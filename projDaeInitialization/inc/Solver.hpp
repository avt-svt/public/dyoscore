/** 
* @file Solver.hpp
* @brief header for Solver interfaces
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the abstact LinearSolver and NonLinearSolver classes  \n
* =====================================================================\n 
* @author Tjalf Hoffmann
* @date 01.10.2012
*/

#pragma once

//#include "boost/shared_ptr.hpp"
#include <memory>
#include "GenericEso.hpp"
#include "TripletMatrix.hpp"
#include "Array.hpp"

/**
* @class LinearSolver
* @brief interface for linear Solver that are used in FullDaeInitialization class
*/
class LinearSolver
{
public:
  virtual ~LinearSolver(){};
  /// @brief result flag of the solve function
  enum ResultFlag{
    OK, ///< linear system is solved
    FAILED ///< Solver reported an error
  };

  typedef std::shared_ptr<LinearSolver> Ptr;
  
  /**
  * @brief solve the linear system Ax = b
  * @param[in] matrix matrix A
  * @param[in,out] xb on entry vector b, on exit vector x
  */
  virtual ResultFlag solve(const CsTripletMatrix::Ptr &matrix, utils::Array<double> &xb) const = 0;
};

/**
* @class NonLinearSolver
* @brief interface for non linear solver that are used in FullDaeInitialization class
*/
class NonLinearSolver
{
public:
  virtual ~NonLinearSolver(){}
  /// @brief result flag of the solve function
  enum ResultFlag{
    OK, ///< linear system is solved
    FAILED ///< Solver reported an error
  };

  typedef std::shared_ptr<NonLinearSolver> Ptr;
  
  /**
  * @brief solve the nonlinear system defined by the problem in GenericEso and by the index vectors
  * @param[in,out] genEso GenericEso containing the complete problem,
  *                setter will be used during call
  * @param[in,out] x initial estimate on entry, solution on exit
  * @param[in] algEqnIndices row indices of the block to be solved
  * @param[in] algVarIndices column indices of the block to be solved
  * @param[in] tripletIndices indices of the Jacobian entries of the block
  * @return OK if system could be solved, FAILED otherwise
  */
  virtual ResultFlag solve(const GenericEso::Ptr &genEso,
                                 utils::Array<EsoDouble> &x,
                           const utils::Array<EsoIndex> &algEqnIndices,
                           const utils::Array<EsoIndex> &algVarIndices,
                           const utils::Array<unsigned> &tripletIndices) = 0;
};