/*!
 * @file InitializeDaeDerivatives.hpp
 * @brief header for the initializeDaeDerivatives interface
 *
 * =====================================================================\n
 * &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
 * =====================================================================\n
 *\n
 * =====================================================================\n 
 * @author Klaus Stockmann
 * @date 15.03.2012
 */

#include <vector>

#pragma once

int initializeDaeDerivatives(GenericEso *genericEso, double *maxabserr);
