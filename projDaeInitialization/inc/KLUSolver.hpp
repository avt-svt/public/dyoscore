/** 
* @file KLUSolver.hpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the linear solver class KLU Solver                    \n
* =====================================================================\n
* @author Sascha Bastian
* @date 31.03.2014
*/
#pragma once

#include "Solver.hpp"
#include "klu.h"

/**
* @class KLUSolver
* @brief wrapper class for the linear solver KLU
*/
class KLUSolver : public LinearSolver
{
protected:
  
public:
  typedef std::shared_ptr<KLUSolver> Ptr;
  
  KLUSolver();
  virtual ~KLUSolver();
  
  virtual LinearSolver::ResultFlag solve(const CsTripletMatrix::Ptr &matrix, utils::Array<double> &xb) const;
};