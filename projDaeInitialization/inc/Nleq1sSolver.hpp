/** 
* @file Nleq1sSolver.hpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the linear solver class Nleq1sSolver                  \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 04.10.2012
*/

#pragma once

#include "Solver.hpp"
//#include "boost/weak_ptr.hpp"
#include <memory>

/**
* @class Nleq1sSolver
* @brief wrapper class for the non linear solver NLEQ1S
*/
class Nleq1sSolver : public NonLinearSolver
{
protected:
  //callbacks
  static void fcnAlgeb(int *N, double *x, double *F, int *errorFlag);
  static void jacAlgeb(int *N, double *x, double *dfdx, int *iRow,
                       int *iCol, int *nFill, int *errorFlag);

  //workspace used by callback functions
  // static GenericEso pointers have to be weak pointers
  // otherwise there will be problems when the Generic Eso object is destructed
  /// @brief GenericEso object used for evaluation of residuals and Jacobian
  static std::weak_ptr<GenericEso> m_staticGenEso;
  /// @brief indices of the Jacobian entries of the block
  static utils::WrappingArray<const EsoIndex> m_staticTripletIndices;
  /// @brief column indices corresponding to triplet indices
  static utils::WrappingArray<EsoIndex> m_staticColJacIndices;
  /// @brief row indices corresponding to triplet indices
  static utils::WrappingArray<EsoIndex> m_staticRowJacIndices;
  
  /**
  * @brief column indices of the block to be solved
  * (all algebraic variable indices for simultaneous solve)
  */
  static utils::WrappingArray<const int> m_staticAlgVarIndices;
  /**
  * @brief row indices of the block to be solved
  * (all algebraic variable indices for simultaneous solve)
  */
  static utils::WrappingArray<const int> m_staticAlgEqnIndices;
  
  /// @brief NLEQ1S options
  utils::Array<int> m_options;
  /// @rbief tolerance used by NLEQ1S 
  double m_tolerance;
  
public:
  Nleq1sSolver(const double tolerance = 1.0e-10);
  virtual ~Nleq1sSolver();
  typedef std::shared_ptr<Nleq1sSolver> Ptr;
  
  virtual NonLinearSolver::ResultFlag solve(const GenericEso::Ptr &genEso,
                                                  utils::Array<EsoDouble> &x,
                                            const utils::Array<EsoIndex> &algEqnIndices,
                                            const utils::Array<EsoIndex> &algVarIndices,
                                            const utils::Array<unsigned> &tripletIndices);
};