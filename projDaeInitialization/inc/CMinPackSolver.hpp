/**
* @file CMinPackSolver
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the linear solver class Nleq1sSolver                  \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 04.02.2015
*/

#pragma once

#include "Solver.hpp"
//#include "boost/weak_ptr.hpp"
#include <memory>
/**
* @class CMinPackSolver
* @brief wrapper class for the non linear solver CMinPack
*/
class CMinPackSolver : public NonLinearSolver
{
protected:
  //callbacks
  static int FuncJac(void *p, int nvar, const double x[], double f[],
                      double jac[], int ldfjac, int iflag);
  static void evalFunc(const int *nvar, double f[]);
  static void evalJac(const int *nvar, double jac[], const int *ldjac);
//workspace used by callback functions
// static GenericEso pointers have to be weak pointers
// otherwise there will be problems when the Generic Eso object is destructed
/// @brief GenericEso object used for evaluation of residuals and Jacobian
static std::weak_ptr<GenericEso> m_staticGenEso;
/// @brief indices of the Jacobian entries of the block
static utils::WrappingArray<const EsoIndex> m_staticTripletIndices;
/// @brief column indices correstponding to triplet indices
static utils::WrappingArray<EsoIndex> m_staticColJacIndices;
/// @brief row indices corresponding to triplet indices
static utils::WrappingArray<EsoIndex> m_staticRowJacIndices;
/// @brief column indices of the block to be solved
static utils::WrappingArray<const int> m_staticAlgVarIndices;
/// @brief row indices of the block to be solved
static utils::WrappingArray<const int> m_staticAlgEqnIndices;
  
  double m_tolerance;
public:
  CMinPackSolver(const double tolerance = 1.0e-12);
  virtual ~CMinPackSolver();
  typedef std::shared_ptr<CMinPackSolver> Ptr;
  
  virtual NonLinearSolver::ResultFlag solve(const GenericEso::Ptr &genEso,
                                                  utils::Array<EsoDouble> &x,
                                            const utils::Array<EsoIndex> &algEqnIndices,
                                            const utils::Array<EsoIndex> &algVarIndices,
                                            const utils::Array<unsigned> &tripletIndices);
};