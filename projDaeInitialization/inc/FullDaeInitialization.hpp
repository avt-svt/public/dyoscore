/**
* @file FullDaeInitialization.hpp
* @brief header for the FullDaeInitialization class
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the FullDaeInitialization class                       \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 02.10.2012
*/

#pragma once

#include "DaeInitialization.hpp"
#include "Solver.hpp"

#include <vector>

/**
* @class FullDaeInitialization
* @brief DaeInitialization class solving the fulll system simultaneously
*/
class FullDaeInitialization : public DaeInitialization
{
protected:
  /// nonlinear solver used for the initialization of the algebraic variables
  NonLinearSolver::Ptr m_nonLinSolver;
  /// linear solver used for the initialization of the dae derivatives
  LinearSolver::Ptr m_linSolver;
  /// tolerance of the maximum absolute error of the residuals
  double m_maxErrorTolerance;
  
  FullDaeInitialization();
  
  std::vector<unsigned> getTripletIndices(const GenericEso::Ptr &genericEso,
                                          const utils::Array<EsoIndex> &varIndices,
                                          const utils::Array<EsoIndex> &eqnIndices) const;
  double getMaxAbsError(const utils::Array<EsoDouble> &vec) const;
public:
  typedef std::shared_ptr<FullDaeInitialization> Ptr;
  
  FullDaeInitialization(const LinearSolver::Ptr &linSolver,
                        const NonLinearSolver::Ptr &nonLinSolver,
                        const double maxErrorTolerance);
  FullDaeInitialization(const FullDaeInitialization &toCopy);
  virtual ~FullDaeInitialization();
  
  virtual DaeInitialization::InitializationResult initializeAlgebraicVariables
                                                  (const GenericEso::Ptr &genericEso) const;
  virtual DaeInitialization::InitializationResult initializeDaeDerivatives
                                                  (const GenericEso::Ptr &genericEso) const;
};