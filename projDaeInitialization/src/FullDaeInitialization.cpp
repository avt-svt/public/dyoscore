/**
* @file FullDaeInitialization.cpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Implementation of the FullDaeInitialization class                   \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 02.10.2012
*/

#include "FullDaeInitialization.hpp"
#include "Array.hpp"
#include "EsoUtilities.hpp"

#include <algorithm>


/// @brief standard constructor
FullDaeInitialization::FullDaeInitialization()
{}

/**
* @brief constructor
* @param[in] linSolver solver used for the initialization of the dae derivatives
* @param[in] nonLinSolver solver used for the initialization of the algebraic variables
* @param[in] maxErrorTolerance  tolerance of the maximum absolute error of the residuals
*/
FullDaeInitialization::FullDaeInitialization(const LinearSolver::Ptr &linSolver,
                                             const NonLinearSolver::Ptr &nonLinSolver,
                                             const double maxErrorTolerance)
{
  m_linSolver = linSolver;
  m_nonLinSolver = nonLinSolver;
  m_maxErrorTolerance = maxErrorTolerance;
}

/// @brief destructor
FullDaeInitialization::~FullDaeInitialization()
{}

/**
* @brief copy constructor
*
* Pointer to the objects are copied, no
* @param[in] toCopy object to be copied
*/
FullDaeInitialization::FullDaeInitialization(const FullDaeInitialization &toCopy)
{
  m_linSolver = toCopy.m_linSolver;
  m_nonLinSolver = toCopy.m_nonLinSolver;
  m_maxErrorTolerance = toCopy.m_maxErrorTolerance;
}

/**
* @copydoc DaeInitialization::initializeAlgebraicVariables
*/
DaeInitialization::InitializationResult FullDaeInitialization::initializeAlgebraicVariables
                                                                (const GenericEso::Ptr &genericEso) const
{
  DaeInitialization::InitializationResult ir;

  // check if DAE system is semi-explicit (prerequisite for our approach to
  // separately initialize the algebraic variables and the derivatives of
  // the differential variables)
  //if(!isSemiExplicit(genericEso)){
  //  ir.flag = DaeInitialization::SYSTEM_NOT_SEMI_EXPLICIT;
  //  return ir;
  //}
  
  //initialize 
  const EsoIndex numAlgEqns = genericEso->getNumAlgEquations();
  const EsoIndex numAlgVars = genericEso->getNumAlgebraicVariables();
  
  utils::Array<EsoIndex> algEqnIndices (numAlgEqns);
  genericEso->getAlgEquationIndex(algEqnIndices.getSize(), algEqnIndices.getData());

  utils::Array<EsoDouble> algResiduals(numAlgEqns);
  genericEso->getResiduals(algResiduals.getSize(),
                           algResiduals.getData(),
                           algEqnIndices.getData());

  ir.maxAbsError = getMaxAbsError(algResiduals);

  if (ir.maxAbsError < m_maxErrorTolerance) {
    ir.flag = DaeInitialization::OK;
    return ir;
  }
  
  utils::Array<EsoIndex> algVarIndices(numAlgVars);
  genericEso->getAlgebraicIndex(algVarIndices.getSize(), algVarIndices.getData());
  
  utils::Array<EsoDouble> x(numAlgVars);
  genericEso->getAlgebraicVariableValues(x.getSize(), x.getData());

  std::vector<unsigned> tripletIndicesVec = getTripletIndices(genericEso,
                                                              algVarIndices,
                                                              algEqnIndices);

  // conversion from vector to pointer. If vector has length zero, no pointer possible.
  unsigned * indicesVecPtr = NULL;
  const unsigned indicesVecSize = tripletIndicesVec.size();
  if(indicesVecSize > 0){
    indicesVecPtr = &tripletIndicesVec[0];
  }
  
  utils::WrappingArray<unsigned> tripletIndices(indicesVecSize, indicesVecPtr);
  
  // solve system
  NonLinearSolver::ResultFlag result = m_nonLinSolver->solve(genericEso,
                                                             x,
                                                             algEqnIndices,
                                                             algVarIndices,
                                                             tripletIndices);

  // pass solution to ESO
  genericEso->setAlgebraicVariableValues(x.getSize(), x.getData());
  genericEso->getAlgebraicResiduals(algResiduals.getSize(),
                                    algResiduals.getData());

  // create output
  ir.maxAbsError = getMaxAbsError(algResiduals);

  switch(result){
    case NonLinearSolver::OK:
      if(ir.maxAbsError > m_maxErrorTolerance){
        ir.flag = DaeInitialization::PRECISION_TOO_LOW;
      }
      else{
        ir.flag = DaeInitialization::OK;
      }
      break;
    case NonLinearSolver::FAILED:
      ir.flag = DaeInitialization::FAILED;
      break;
    default:
      assert(false);
  }

  
  return ir;
}

/**
* @copydoc DaeInitialization::initializeDaeDerivatives
*/
DaeInitialization::InitializationResult FullDaeInitialization::initializeDaeDerivatives
                                                               (const GenericEso::Ptr &genericEso) const
{
  DaeInitialization::InitializationResult ir;

  // extract subsystem with differential equations 
  const EsoIndex numDiffEqns = genericEso->getNumDiffEquations();  
  CsTripletMatrix::Ptr bMatrix = getBMatrix(genericEso);
  
  // extract rhs ( = negative of residual of differential equations)
  utils::Array<EsoDouble> diffResiduals(numDiffEqns, 0.0);
  //preset derivatives to 0
  genericEso->setDerivativeValues(diffResiduals.getSize(), diffResiduals.getData()); 
  genericEso->getDifferentialResiduals(diffResiduals.getSize(), diffResiduals.getData());

  assert(bMatrix->get_n_rows() == numDiffEqns);
  utils::Array<EsoDouble> rhs(numDiffEqns);
  for (EsoIndex i=0; i<numDiffEqns; i++){
    rhs[i] = -diffResiduals[i];
  }
  
  // solve linear system
  LinearSolver::ResultFlag result = m_linSolver->solve(bMatrix, rhs);
  
  // pass solution to ESO
  genericEso->setDerivativeValues(rhs.getSize(), rhs.getData());

  // create output
  genericEso->getDifferentialResiduals(diffResiduals.getSize(), diffResiduals.getData());
  ir.maxAbsError = getMaxAbsError(diffResiduals);

  switch(result){
    case NonLinearSolver::OK:
      if(ir.maxAbsError > m_maxErrorTolerance){
        ir.flag = DaeInitialization::PRECISION_TOO_LOW;
      }
      else{
        ir.flag = DaeInitialization::OK;
      }
      break;
    case NonLinearSolver::FAILED:
      ir.flag = DaeInitialization::FAILED;
      break;
    default:
      assert(false);
  }

  return ir;
}

/**
* @brief extract the indices of the sparse entries for the given rows and colums
* @param genericEso object containing the sparse Jacobian struct
* @param varIndices index vector containing the colum indices of the Jacobian struct
*                   that are to be extracted
* @param eqnIndices index vector containing the row indices of the Jacobian struct
*                   that are to be extracted
* @return index vector containing the extracted indices of the triplet entries
*/
std::vector<unsigned> FullDaeInitialization::getTripletIndices(
                                          const GenericEso::Ptr &genericEso,
                                          const utils::Array<EsoIndex> &varIndices,
                                          const utils::Array<EsoIndex> &eqnIndices) const
{
  std::vector<unsigned> tripletIndices;
  
  const EsoIndex numNonZeroes = genericEso->getNumNonZeroes();
  utils::Array<EsoIndex> cols(numNonZeroes), rows(numNonZeroes);
  
  genericEso->getJacobianStruct(numNonZeroes, rows.getData(), cols.getData());
  
  for(EsoIndex i=0; i<numNonZeroes; i++){
    // find col index in varIndices
    const EsoIndex *varIndexFound = std::find(varIndices.getData(),
                                              varIndices.getData() + varIndices.getSize(),
                                              cols[i]);
    // find row index in colIndices
    const EsoIndex *eqnIndexFound = std::find(eqnIndices.getData(),
                                              eqnIndices.getData() + eqnIndices.getSize(),
                                              rows[i]);

    // if both are found, then i is a relevant triplet index
    if(   (varIndexFound != varIndices.getData() + varIndices.getSize())
       && (eqnIndexFound != eqnIndices.getData() + eqnIndices.getSize())){
      tripletIndices.push_back(unsigned(i));
    }
  }
  
  return tripletIndices;
}

/**
* @brief calculate the maximum absolute error of a givven array
* @param[in] vec value array
* @return maximum absolute value of the error
*/
double FullDaeInitialization::getMaxAbsError(const utils::Array<EsoDouble> &vec) const
{
  if(vec.getSize() == 0){
    return 0.0;
  }
  const double maxError =  *(std::max_element(vec.getData(),

                                        vec.getData() + vec.getSize()));
  const double minError = *(std::min_element(vec.getData(),
                                       vec.getData() + vec.getSize()));
  return std::max(maxError, -minError);
}
