/**
* @file FmiDaeInitialization.hpp
* @brief header for the FmiDaeInitialization class
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Lehrstuhl für Systemverfahrenstechnik, RWTH Aachen        \n
* =====================================================================\n
* Declaration of the FmiDaeInitialization class                       \n
* =====================================================================\n
* @author Adrian Caspari
* @date 22.03.2018
*/

#pragma once

#include "FmiDaeInitialization.hpp"

#include <vector>


FmiDaeInitialization::FmiDaeInitialization()
{}


FmiDaeInitialization::~FmiDaeInitialization()
{}

DaeInitialization::InitializationResult FmiDaeInitialization::initializeAlgebraicVariables(const GenericEso::Ptr &genericEso) const
  {
    DaeInitialization::InitializationResult ir;


	genericEso->solveAlgebraicEquations();


    ir.flag = DaeInitialization::OK;
    return ir;
  }
  
  /**
  * @copydoc DaeInitialization::InitializationResult
  */
DaeInitialization::InitializationResult FmiDaeInitialization::initializeDaeDerivatives(const GenericEso::Ptr &genericEso) const
  {
    DaeInitialization::InitializationResult ir;

	genericEso->solveDifferentialEquations();

    ir.flag = DaeInitialization::OK;
    return ir;
  }