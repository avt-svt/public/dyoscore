/**
* @file CMinPackSolver.cpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Implementation of the nonlinear solver class CMinpack               \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 04.02.2015
*/

#include "CMinPackSolver.hpp"

#include "cminpack.h"
#include <cassert>

static EsoIndex temp = 0;

//initialize static data
std::weak_ptr<GenericEso> CMinPackSolver::m_staticGenEso;
utils::WrappingArray<const EsoIndex> CMinPackSolver::m_staticTripletIndices(0, &temp);
utils::WrappingArray<EsoIndex> CMinPackSolver::m_staticColJacIndices(0, &temp);
utils::WrappingArray<EsoIndex> CMinPackSolver::m_staticRowJacIndices(0, &temp);
utils::WrappingArray<const EsoIndex> CMinPackSolver::m_staticAlgVarIndices(0, &temp);
utils::WrappingArray<const EsoIndex> CMinPackSolver::m_staticAlgEqnIndices(0, &temp);

/**
* @brief standard constructor
*/
CMinPackSolver::CMinPackSolver(const double tolerance)
{
  m_tolerance = tolerance;
}

/**
* @brief destructor
*/
CMinPackSolver::~CMinPackSolver()
{}

/**
* @brief callback function FuncJac
* 
* @param p object pointer for user data
* @param nvar number of variables (and equations)
* @param x current state values
* @param f vector with residuals of the equations (length nvar)
* @param jac matrix with the Jacobian of size (ldfjac,nvar)
* @param ldfjac number of colums in jac
* @param iflag if set to 1 then evaluate f only, if set to 2 then evaluate jac only
*
* @note p could be used to avoid the complete static data of the class
* @todo remove static data and use p instead
*/
int CMinPackSolver::FuncJac(void *p, int nvar, const double x[], double f[], double jac[], 
                             int ldfjac, int iflag)
{
  //set states to Eso
  GenericEso::Ptr genEso = m_staticGenEso.lock();
  genEso->setIndependentVariable(0.0);
  const EsoIndex numVars = m_staticAlgVarIndices.getSize();
  assert(numVars == nvar);
  genEso->setVariableValues(numVars, x, m_staticAlgVarIndices.getData());
  switch(iflag){
    //evaluate rhs
    case 1:
    {
      const  EsoIndex numEqns = m_staticAlgEqnIndices.getSize();
      assert(numEqns == nvar);
      GenericEso::RetFlag ret = genEso->getResiduals(numEqns, f, m_staticAlgEqnIndices.getData());
      if(ret != GenericEso::OK){
        std::cout<<" *** Evaluation of residuals for CMinPack failed"<<std::endl;
        //this ends the initialization routine
        return -1;
      }
      break;
    }
    //evaluate Jacobian
    case 2:
    {
      const unsigned numAlgParams = genEso->getNumAlgebraicVariables();
      const unsigned numJac = m_staticTripletIndices.getSize();
      assert(ldfjac == int(numAlgParams));
      assert(m_staticRowJacIndices.getSize() == numJac);
      assert(m_staticColJacIndices.getSize() == numJac);
      utils::Array<EsoDouble> jacVals(numJac);
      GenericEso::RetFlag ret = genEso->getJacobianValues(numJac, jacVals.getData(), m_staticTripletIndices.getData());
      if(ret != GenericEso::OK){
        std::cout<<" *** Evaluation of Jacobian for CMinPack failed."<<std::endl;
        //this ends the initialization routine
        return -1;
      }
      break;
    }
    //userflag should always be 1 or 2
    default:
      assert(false);
  }
  return 1;
}

NonLinearSolver::ResultFlag CMinPackSolver::solve(const GenericEso::Ptr &genEso,
                                                        utils::Array<EsoDouble> &x,
                                                  const utils::Array<EsoIndex> &algEqnIndices,
                                                  const utils::Array<EsoIndex> &algVarIndices,
                                                  const utils::Array<unsigned> &tripletIndices)
{
  //provide static data for callback function
  m_staticGenEso = genEso;
  //convert tripletIndices from unsigned to EsoIndex
  utils::Array<EsoIndex> tripletCopy(tripletIndices.getSize());
  for(unsigned i=0; i<tripletIndices.getSize(); i++){
    tripletCopy[i] = EsoIndex(tripletIndices[i]);
  }
  
  m_staticTripletIndices.setData(tripletCopy.getSize(), tripletCopy.getData());
  m_staticAlgVarIndices.setData(algVarIndices.getSize(), algVarIndices.getData());
  m_staticAlgEqnIndices.setData(algEqnIndices.getSize(), algEqnIndices.getData());
  
  const EsoIndex numNonZeroes = genEso->getNumNonZeroes();
  utils::Array<EsoIndex> jacCols(numNonZeroes), jacRows(numNonZeroes);
  genEso->getJacobianStruct(numNonZeroes, jacRows.getData(), jacCols.getData());
  const EsoIndex numBlockJac = tripletIndices.getSize();
  utils::Array<EsoIndex> blockJacRow(numBlockJac), blockJacCol(numBlockJac);
  for(EsoIndex i=0; i<numBlockJac; i++){
    blockJacRow[i] = jacRows[tripletIndices[i]];
    blockJacCol[i] = jacCols[tripletIndices[i]];
  }
  m_staticRowJacIndices.setData(numBlockJac, blockJacRow.getData());
  m_staticColJacIndices.setData(numBlockJac, blockJacCol.getData());
  
  EsoIndex numAlgParams = genEso->getNumAlgebraicVariables();
  unsigned numAlgEqns = m_staticAlgEqnIndices.getSize();
  assert(x.getSize() == numAlgEqns);
  utils::Array<double> residuals(numAlgEqns, 0.0), diag(numAlgEqns, 1.0);
  utils::Array<double> jacobian(numAlgEqns * numAlgParams, 0.0);
  //scaling of the algebraic equation system
  if(numAlgEqns > 1){
    for(unsigned i=0; i<numAlgEqns;i++){
      double val = fabs(x[i]);
      while(val < 1.0){
        val *= 10;
        diag[i] *= 10;
      }
      while(val >= 10.0){
        val /= 10;
        diag[i] /= 10;
      }
      if(diag[i] == 0.0){
        diag[i] = 1.0;
      }
    }
  }
  //all settable for user?
  //terminate, if FuncJac has been called maxfev times already wiht iflag 1
  int maxfev = 100*(x.getSize() + 1);
  // mode = 1 scale variables are scaled internally, mode = 2 scaling is spefified by diag
  int mode = 2;
  int nprint = 0; //print level (0 = no extra calls of FuncJac for printing)
  double factor = 100.0;//initial step bound
  
  //output variables of hybrj (name of CMinPack solver)
  int nfev = 0; // number of calls to FuncJac with iflag 1
  int njev = 0; // number of calls to FuncJac with iflag 2
  
  int lr = numAlgEqns*(numAlgEqns+1)/2;
  utils::Array<double> r(lr, 0.0);
  utils::Array<double> qtf(numAlgEqns, 0.0);
  utils::Array<double> wa1(numAlgEqns, 0.0);
  utils::Array<double> wa2(numAlgEqns, 0.0);
  utils::Array<double> wa3(numAlgEqns, 0.0);
  utils::Array<double> wa4(numAlgEqns, 0.0);
  
  
  void *p = NULL;
  int fail = hybrj(&this->FuncJac, p, x.getSize(), x.getData(), residuals.getData(), jacobian.getData(), numAlgParams,
                   m_tolerance, maxfev, diag.getData(), mode, factor, nprint, &nfev, &njev, r.getData(), lr,
                   qtf.getData(),wa1.getData(),wa2.getData(),wa3.getData(),wa4.getData());
  
  NonLinearSolver::ResultFlag flag;
  if(fail == 0 || fail == 3){
    flag = NonLinearSolver::FAILED;
  }
  else{
    flag = NonLinearSolver::OK;
  }
  return flag;
}
