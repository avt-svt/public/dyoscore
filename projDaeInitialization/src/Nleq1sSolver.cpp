/**
* @file Nleq1sSolver.cpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Implementation of the linear solver class Nleq1sSolver               \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 04.10.2012
*/

#include "Nleq1sSolver.hpp"
#include <set>
#include <vector>
#include <memory>

static EsoIndex temp = 0;

//initialize static data
std::weak_ptr<GenericEso> Nleq1sSolver::m_staticGenEso;
utils::WrappingArray<const EsoIndex> Nleq1sSolver::m_staticTripletIndices(0, &temp);
utils::WrappingArray<EsoIndex> Nleq1sSolver::m_staticColJacIndices(0, &temp);
utils::WrappingArray<EsoIndex> Nleq1sSolver::m_staticRowJacIndices(0, &temp);


utils::WrappingArray<const EsoIndex> Nleq1sSolver::m_staticAlgVarIndices(0, &temp);
utils::WrappingArray<const EsoIndex> Nleq1sSolver::m_staticAlgEqnIndices(0, &temp);


/**
* @brief interface definition of NLEQ1S
* @sa subroutine nleg1s in nleq1s.f
*/
#ifdef WIN32
#define NLEQ1S nleq1s
#else
#define NLEQ1S nleq1s_
#endif
// declare own function header for Fortran function
extern "C" void NLEQ1S(int*,
                        int*,
                        void (*) (int *, double *, double *, int *),
                        void(*)(int *, double *, double *, int *, int*, int *, int*),
                        double*,
                        double*,
                        double*,
                        int*,
                        int*,
                        int*,
                        int*,
                        int*,
                        int*,
                        int*,
                        double*);

/**
* @brief (standard) constructor
*
* presizes options
* @param[in] tolerance tolerance used by NLEQ1S (default 1.0e-10)
*/
Nleq1sSolver::Nleq1sSolver(const double tolerance):m_options(50)
{
  m_tolerance = tolerance;
}

/// @brief destructor
Nleq1sSolver::~Nleq1sSolver()
{}

/**
* @brief callback routine for function evaluation
* @param[in] n number of vector components
* @param[in] x vector of unknowns
* @param[out] f vector of function values
* @param[in,out] iFail always 0 on input, indicates failure of fcn evaluation on output:
*                If <0: NLEQ1S will be terminated with error code = 82, and IFAIL stored
*                       to iwk[23]
*                If =1: A new trial Newton iterate will computed, with the damping factor
*                       reduced to it's half.
*                If =2: A new trial Newton iterate will computed, with the damping factor
*                       reduced by a reduct. factor, which must be output through f(1) by FCN,
                        and it's value must be >0 and < 1.
*/
void Nleq1sSolver::fcnAlgeb(int *n, double *x, double *f, int *iFail)
{

  GenericEso::Ptr genEso = m_staticGenEso.lock();
  genEso->setIndependentVariable(0.0);

  const EsoIndex numVars = m_staticAlgVarIndices.getSize();
  assert(numVars == *n);
  genEso->setVariableValues(numVars, x, m_staticAlgVarIndices.getData());

  const EsoIndex numEqns = m_staticAlgEqnIndices.getSize();
  assert(numEqns == *n);

  GenericEso::RetFlag ret;
  ret = genEso->getResiduals(numEqns, f, m_staticAlgEqnIndices.getData());

  if(ret != GenericEso::OK) {
    *iFail = 1;
    std::cout << std::endl << " *** Evaluation of residuals for NLEQ1S failed";
    std::cout<< " => reducing damping factor *** " << std::endl;
  }
}

/**
* @brief callback routine for Jacobian evaluation
*
* @param[in] n number of vector components
* @param[in] *x vector with current values of algebraic block variables
* @param[out] *dfdx Jacobian entries of algebraic variables
* @param[out] *iRow row indices of Jacobian entries of algebraic variables
* @param[out] *iCol column indices of Jacobian entries of algebraic variables
* @param[out] *nFill number of nonzeroes of Jacobian of algebraic variables
* @param[out] *iFail jac evaluation-failure indicator. (output)
C                    Has always value 0 (zero) on input. Indicates failure of JAC evaluation
C                    and causes termination of NLEQ1S, if set to a negative value on output
*/
void Nleq1sSolver::jacAlgeb(int *n, double *x, double *dfdx, int *iRow, int *iCol,
                            int *nFill, int *iFail)
{
  const int numJac = m_staticTripletIndices.getSize();
  assert(numJac == *nFill);
  assert(m_staticRowJacIndices.getSize() == unsigned(numJac));
  assert(m_staticColJacIndices.getSize() == unsigned(numJac));
  utils::Array<EsoDouble> vals(numJac);

  const EsoIndex numVars = EsoIndex(m_staticAlgVarIndices.getSize());
  assert(numVars == *n);

  GenericEso::RetFlag ret;
  GenericEso::Ptr genEso = m_staticGenEso.lock();
  genEso->setVariableValues(numVars, x, m_staticAlgVarIndices.getData());
  ret = genEso->getJacobianValues(numJac, vals.getData(), m_staticTripletIndices.getData());

  // A failure in the jacobian evaluation fires the need for a reduced damping factor
  // cf. NLEQ1S documentation
  if(ret != GenericEso::OK){
    *iFail = 1;
    std::cout << std::endl << " *** Evaluation of jacobian for NLEQ1S failed";
    std::cout << "=> reducing damping factor *** " << std::endl;
  }

  // get Irow, Icol
  if (numJac == 1) {
    iRow[0] = 1; // 1-offset in Fortran
    iCol[0] = 1; // 1-offset in Fortran
    dfdx[0] = vals[0];
  }
  else {
    const unsigned numTotalVars = genEso->getNumVariables();
    const unsigned numTotalEqns = genEso->getNumEquations();


    //MatrixClass: SparseMAtrix
    CsTripletMatrix::Ptr jac(new CsTripletMatrix(numTotalEqns,
                                                 numTotalVars,
                                                 numJac,
                                                 m_staticRowJacIndices.getData(),
                                                 m_staticColJacIndices.getData(),
                                                 vals.getData()));

    //reduce dimensions of matrix
    CsTripletMatrix::Ptr redJac = jac->get_matrix_slices(m_staticAlgEqnIndices.getSize(),
                                                         m_staticAlgEqnIndices.getData(),
                                                         m_staticAlgVarIndices.getSize(),
                                                         m_staticAlgVarIndices.getData());

    const cs *redJacPtr = redJac->get_matrix_ptr();
    for (int i=0; i<numJac; i++) {
      iRow[i] = redJacPtr->i[i] + 1; // 1-offset in Fortran
      iCol[i] = redJacPtr->p[i] + 1; // 1-offset in Fortran
      dfdx[i] = redJacPtr->x[i];
    }
  }//else
}

/**
* @copydoc NonLinearSolver::solve
*/
NonLinearSolver::ResultFlag Nleq1sSolver::solve(
                                            const GenericEso::Ptr &genEso,
                                                  utils::Array<EsoDouble> &x,
                                            const utils::Array<EsoIndex> &algEqnIndices,
                                            const utils::Array<EsoIndex> &algVarIndices,
                                            const utils::Array<unsigned> &tripletIndices)
{


  // provide static data for callbacks
  m_staticGenEso = genEso;
  utils::Array<EsoIndex> tripletCopy(tripletIndices.getSize());
  for(unsigned i=0; i<tripletIndices.getSize(); i++){
    tripletCopy[i] = EsoIndex(tripletIndices[i]);
  }
  m_staticTripletIndices.setData(tripletCopy.getSize(), tripletCopy.getData());
  m_staticAlgVarIndices.setData(algVarIndices.getSize(), algVarIndices.getData());
  m_staticAlgEqnIndices.setData(algEqnIndices.getSize(), algEqnIndices.getData());


  const EsoIndex numNonZeroes = genEso->getNumNonZeroes();
  utils::Array<EsoIndex> jacCols(numNonZeroes), jacRows(numNonZeroes);
  genEso->getJacobianStruct(numNonZeroes, jacRows.getData(), jacCols.getData());

  const EsoIndex numBlockJac = tripletIndices.getSize();
  utils::Array<EsoIndex> blockJacRow(numBlockJac), blockJacCol(numBlockJac);
  for(EsoIndex i=0; i<numBlockJac; i++){
    blockJacRow[i] = jacRows[tripletIndices[i]];
    blockJacCol[i] = jacCols[tripletIndices[i]];
  }
  m_staticRowJacIndices.setData(numBlockJac, blockJacRow.getData());
  m_staticColJacIndices.setData(numBlockJac, blockJacCol.getData());

  // initialize inputs and outputs
  int iErr; // return value

  int nleqDim = int(x.getSize());
  int numJacMax = tripletIndices.getSize();

  utils::Array<double> xscale(nleqDim, 0.0);

  int liwk = 10 * nleqDim + 57; // minimum is 8*N+57
  utils::Array<int> iwk(liwk, 0);

  int li2wk = 8*numJacMax + 5 * nleqDim; // minimum is (7.5*NFMAX + 5*N;
  utils::Array<int> i2wk(li2wk, 0);

  const int nbroy = std::max(int(numJacMax/nleqDim), 10);
  // minimum (for standard linear systemsolver): 3*NFMAX+(11+NBROY)*N+62
  // NBROY = Maximum number of Broyden steps
  // (Default: if Broyden steps are enabled, e.g. IOPT(32)=1
  //  NBROY = MAX( INT(NFMAX/N), 10 ),
  //  else (if IOPT(32)=0) - NBROY=0 ;
  int lrwk = 3*numJacMax + (11 + nbroy) * nleqDim + 62;
  utils::Array<double> rwk(lrwk, 0.0);

  for(unsigned i=0; i<m_options.getSize(); i++){
    m_options[i] = 0;
  }
  m_options[30] = 3;
  m_options[10] = 3;
  m_options[38] = 1;
  m_options[36] = 1;

  double tolerance = m_tolerance;

  NLEQ1S(&nleqDim, &numJacMax, &Nleq1sSolver::fcnAlgeb, &Nleq1sSolver::jacAlgeb,
         x.getData(), xscale.getData(), &tolerance, m_options.getData(), &iErr,
         &liwk, iwk.getData(), &li2wk, i2wk.getData(), &lrwk, rwk.getData());

  NonLinearSolver::ResultFlag flag;
  if(iErr == 0){
    flag = NonLinearSolver::OK;
  }
  else{
    flag = NonLinearSolver::FAILED;
  }
  return flag;
}
