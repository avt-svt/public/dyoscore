/**
* @file BlockDaeInitialization.cpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Implementation of the BlockDaeInitialization class                   \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 02.10.2012
*/

#include "BlockDaeInitialization.hpp"
#include "Array.hpp"
#include "EsoUtilities.hpp"
#include "cs.h"

#include <algorithm>
#include <numeric>

/// @brief standard constructor
BlockDaeInitialization::BlockDaeInitialization()
{}

/**
* @copydoc FullDaeInitialization::FullDaeInitialization(const LinearSolver::Ptr &linSolver,
*                                                       const NonLinearSolver::Ptr &nonLinSolver,
*                                                       const double maxErrorTolerance)
*/
BlockDaeInitialization::BlockDaeInitialization(const LinearSolver::Ptr &linSolver,
                                               const NonLinearSolver::Ptr &nonLinSolver,
                                               const double maxErrorTolerance)
:FullDaeInitialization(linSolver, nonLinSolver, maxErrorTolerance)
{}

/// @brief destructor
BlockDaeInitialization::~BlockDaeInitialization()
{}

/**
* @copydoc DaeInitialization::initializeAlgebraicVariables
*/
DaeInitialization::InitializationResult BlockDaeInitialization::initializeAlgebraicVariables
                                                                (const GenericEso::Ptr &genericEso) const
{
  DaeInitialization::InitializationResult ir;
  ir.flag = DaeInitialization::NOT_INITIALIZED;

  // define Variables
  const EsoIndex numVars = genericEso->getNumVariables();
  const EsoIndex numEqns = genericEso->getNumEquations();
  

  const EsoIndex numAlgVars = genericEso->getNumAlgebraicVariables();
  utils::Array<EsoIndex> algVarIndices(numAlgVars);
  genericEso->getAlgebraicIndex(algVarIndices.getSize(), algVarIndices.getData());

  const EsoIndex numAlgEqns = genericEso->getNumAlgEquations();
  utils::Array<EsoIndex> algEqnIndices(numAlgEqns);
  genericEso->getAlgEquationIndex(algEqnIndices.getSize(), algEqnIndices.getData());

  assert(numAlgVars == numAlgEqns);
  //todo: check if current point is already initialized

  // get Jacobian
  const EsoIndex numNonZeroes = genericEso->getNumNonZeroes();
  // in function getTripletIndices we need the Jacobian struct to contain
  // the triplet indices as values
  utils::Array<double> tripletIndexValues(numNonZeroes);
  for(EsoIndex i=0; i<numNonZeroes; i++){
    tripletIndexValues[i] = double(i);
  }

  utils::Array<EsoIndex> jacRowInd(numNonZeroes), jacColInd(numNonZeroes);
  genericEso->getJacobianStruct(numNonZeroes, jacRowInd.getData(), jacColInd.getData());
  
  //add index vector to jacobian that is needed for permutation
  CsTripletMatrix::Ptr jac(new CsTripletMatrix(numEqns,
                                               numVars,
                                               numNonZeroes,
                                               jacRowInd.getData(),
                                               jacColInd.getData(),
                                               tripletIndexValues.getData()));

  CsCscMatrix::Ptr jacCompressed = jac->compress();
  //get algebraic subsystem
  CsTripletMatrix::Ptr jacAlg = jac->get_matrix_slices(algEqnIndices.getSize(),
                                                       algEqnIndices.getData(),
                                                       algVarIndices.getSize(),
                                                       algVarIndices.getData());


  /*If seed=0, the non-randomized algorithm is used
  (columns are considered in order 1:n).  If seed=-1, columns are considered
  in reverse order.  Otherwise, the columns are considered in a random order,
  using seed as the random number generator seed.*/
  const int seed = 0;

  //typedef struct cs_dmperm_results    /* cs_dmperm or cs_scc output */
  //{
  //    int *p ;        /* size m, row permutation */
  //    int *q ;        /* size n, column permutation */
  //    int *r ;        /* size nb+1, block k is rows r[k] to r[k+1]-1 in A(p,q) */
  //    int *s ;        /* size nb+1, block k is cols s[k] to s[k+1]-1 in A(p,q) */
  //    int nb ;        /* # of blocks in fine dmperm decomposition */
  //    int rr [5] ;    /* coarse row decomposition */
  //    int cc [5] ;    /* coarse column decomposition */
  //} csd ;
  csd *D = jacAlg->compress()->get_dulmange_mendelsohn_permutation(seed);

  const int  numBlocks = D->nb;

  utils::Array<double> algValues(numAlgVars);
  genericEso->getAlgebraicVariableValues(algValues.getSize(), algValues.getData());

  utils::WrappingArray<int> blockBoundCol(D->nb+1, D->s);
  utils::WrappingArray<int> rowPerm(numAlgEqns, D->p);
  utils::WrappingArray<int> colPerm(numAlgVars, D->q);
  utils::Array<EsoIndex> rowIndicesOriginal(numAlgEqns), colIndicesOriginal(numAlgVars);
  for(EsoIndex i=0; i<numAlgEqns; i++){
    rowIndicesOriginal[i] = EsoIndex(algEqnIndices[rowPerm[i]]);
    colIndicesOriginal[i] = EsoIndex(algVarIndices[colPerm[i]]);
  }

  int blockCounter = 0, numFailedBlocks = 0;
  int blockSize;

  for (int iBlock = numBlocks-1; iBlock>=0; iBlock--) {
    ++blockCounter;

    // col
    const int indStart = blockBoundCol[iBlock];
    const int indEnd = blockBoundCol[iBlock+1];
    blockSize = indEnd - indStart;

    // get algebraic values of the block
    utils::Array<double> x(blockSize);
    for (int j=0; j<blockSize; j++){
      const unsigned blockColumnIndex = colPerm[indStart + j];
      x[j] = algValues[blockColumnIndex];
    }


    utils::WrappingArray<EsoIndex> rowIndices(blockSize, rowIndicesOriginal.getData() + indStart);
    utils::WrappingArray<EsoIndex> colIndices(blockSize, colIndicesOriginal.getData() + indStart);
    std::vector<unsigned> tripletIndVector = getTripletIndices(jacCompressed,
                                                               rowIndices,
                                                               colIndices);
    assert(!tripletIndVector.empty());
    utils::WrappingArray<unsigned> tripletIndices(tripletIndVector.size(), &tripletIndVector[0]);
	//solution for algebraic variables is set to the ESO during the callbacks of the nonlinear equation solver
    NonLinearSolver::ResultFlag flag = m_nonLinSolver->solve(genericEso,
                                                             x,
                                                             rowIndices,
                                                             colIndices,
                                                             tripletIndices);

    if (flag != NonLinearSolver::OK) {
      numFailedBlocks++;
    }
  } // end of loop over blocks

  // create output
  utils::Array<EsoDouble> algResiduals(numAlgEqns);
  genericEso->getAlgebraicResiduals(algResiduals.getSize(), algResiduals.getData());
  ir.maxAbsError = getMaxAbsError(algResiduals);

  if (numFailedBlocks == 0){
      if(ir.maxAbsError > m_maxErrorTolerance){
        ir.flag = DaeInitialization::PRECISION_TOO_LOW;
      }
      else{
       ir.flag = DaeInitialization::OK;
      }
  } else {
      ir.flag = DaeInitialization::FAILED;
  }


  cs_dfree(D);
  return ir;
}

/**
* @brief extract the indices of the sparse entries for the given rows and colums of the current block
* @param[in] jac the Jacobian matrix extracted from the GenericEso
* @param[in] rowPerm index vector containing the permutated row indices of the current block
*                   that are to be extracted
* @param[in] colPerm index vector containing the permutated colum indices of the current block
*                   that are to be extracted
* @return index vector containing the extracted indices of the triplet entries
*/
std::vector<unsigned> BlockDaeInitialization::getTripletIndices(
                                          const CsCscMatrix::Ptr &jac,
                                          const utils::Array<EsoIndex> &rowPerm,
                                          const utils::Array<EsoIndex> &colPerm) const
{
  std::vector<unsigned> tripletIndices;

  for(unsigned i=0; i<rowPerm.getSize(); i++){
    for(unsigned j=0; j<colPerm.getSize(); j++){
      double tripletIndex = 0.0;
      bool containsValue = jac->getValue(rowPerm[i], colPerm[j], tripletIndex);
      if(containsValue){
        tripletIndices.push_back((unsigned)tripletIndex);
      }
    }
  }
  return tripletIndices;
}
