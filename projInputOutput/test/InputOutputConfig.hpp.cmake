/**
* @file InputOutputConfig.hpp.cmake
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Configuration Header for Dyos                                          \n
* =====================================================================\n
* configure header         \n
* =====================================================================\n
* @author Johannes M. M. Faust
* @date 03.03.2018
*/

#ifndef Config_InputOutput_HPP
#define Config_InputOutput_HPP


// paths to 
#cmakedefine PATH_TO_woJadeout_JSON "@PATH_TO_woJadeout_JSON@"


#endif