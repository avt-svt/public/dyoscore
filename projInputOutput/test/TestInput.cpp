/**
* @file TestInput.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Input  - part of dyos                                                \n
* =====================================================================\n
* This file contains unit tests for the Input module                   \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 26.03.2012
*/



/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE Input
#include "boost/test/unit_test.hpp"
#include "TestInputDummies.hpp"
#include "ConvertToXmlJson.hpp"
#include "ConvertFromXmlJson.hpp"
#include <algorithm>
#include <GenericEsoFactory.hpp>

using namespace UserInput;
/**
* @def TEST_OBJECT_NAME
* @brief name of the Eso test object (JADE)
*/
#define TEST_OBJECT_NAME "carErweitert"



BOOST_AUTO_TEST_SUITE(TestInput)
/** @test we are testing the functionality of the help function getEsoIndex()
*/

BOOST_AUTO_TEST_CASE(testGetEsoIndex)
{
  FactoryInput::EsoInput eso;
  GenericEsoFactory genEsoFac;
  GenericEso::Ptr esoPtr;
  InputConverter inputConverter;
  // checking whether the exception for no name EsoInput works
  std::vector<std::string> esoNames;
  BOOST_CHECK_THROW(esoPtr = genEsoFac.create(eso), MissingModelNameException);
  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  // checking whether the exception for EsoInput without model name works
  BOOST_REQUIRE_NO_THROW(esoPtr = genEsoFac.create(eso));
  inputConverter.getEsoNames(esoNames, esoPtr);
  BOOST_CHECK_THROW(inputConverter.getEsoIndex(esoNames, ""), MissingVariableNameException);
  

  // checking whether the exception for not found name in EsoInput works
  unsigned uEsoOutput;
  BOOST_CHECK_THROW(inputConverter.getEsoIndex(esoNames, "NoName"), VariableNotFoundException);

  // checking whether the match name in EsoInput works
  BOOST_REQUIRE_NO_THROW(uEsoOutput = inputConverter.getEsoIndex(esoNames, "obj"));
  const unsigned uCheckValue = 3;
  BOOST_CHECK_EQUAL(uCheckValue, uEsoOutput);
}

/** @test we are testing the functionality of the help function checkConstraintNames()
*/
BOOST_AUTO_TEST_CASE(testCheckConstraintNames)
{
  FactoryInput::EsoInput eso;
  GenericEsoFactory genEsoFac;
  GenericEso::Ptr esoPtr;
  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  InputConverter inputConverter;

  FactoryInput::IntegratorMetaDataInput imdInput;
  imdInput.controls.resize(1);
  std::vector<std::string> esoNames;
  BOOST_REQUIRE_NO_THROW(esoPtr = genEsoFac.create(eso));
  inputConverter.getEsoNames(esoNames, esoPtr);
  BOOST_REQUIRE_NO_THROW(imdInput.controls[0].esoIndex 
                          = inputConverter.getEsoIndex(esoNames, "t"));


  std::vector<UserInput::ConstraintInput> constraints(1);
  constraints[0].name = "t";

  BOOST_CHECK_THROW(inputConverter.checkConstraintNames(constraints, imdInput, esoPtr),
                    DefinedConstraintForParameterException);
  constraints[0].name = "s";
  BOOST_CHECK_NO_THROW(inputConverter.checkConstraintNames(constraints, imdInput, esoPtr));
}

/** @test we are testing the functionality of the help function getEquationIndex()
*/
BOOST_AUTO_TEST_CASE(testGetEquationIndex)
{
  FactoryInput::EsoInput eso;
  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  InputConverter inputConverter;

  FactoryInput::IntegratorMetaDataInput imdInput;
  imdInput.controls.resize(1);
  imdInput.controls[0].esoIndex = 5;
}

/** @test we are testing the functionality of the help function getDifferentialEquationIndex()
*/
//BOOST_AUTO_TEST_CASE(testGetDifferentialEquationIndex)
//{
//}
// has been empty...

/** @test we are testing the functionality of the help function getAlgebraicEquationIndex()
*/
//BOOST_AUTO_TEST_CASE(testGetAlgebraicEquationIndex)
//{
//}
// has been empty...

/** @test we are testing the functionality of the help function getUserGrid()
*/
BOOST_AUTO_TEST_CASE(testGetUserGrid)
{
  InputConverter inputConverter;

  std::vector<UserInput::ConstraintInput> vConstraintInput;
  std::vector<double> vCheckOutputGrid;
  vConstraintInput.resize(5);
  vConstraintInput[0].type = UserInput::ConstraintInput::PATH;
  vConstraintInput[1].type = UserInput::ConstraintInput::POINT;
  vConstraintInput[2].type = UserInput::ConstraintInput::POINT;
  vConstraintInput[3].type = UserInput::ConstraintInput::ENDPOINT;
  vConstraintInput[4].type = UserInput::ConstraintInput::POINT;

  //Checking equal values for some POINTs
  vConstraintInput[0].timePoint = 0.12;
  vConstraintInput[1].timePoint = 12.34;
  vConstraintInput[2].timePoint = 23.45;
  vConstraintInput[3].timePoint = 56.78;
  vConstraintInput[4].timePoint = 12.34;
  vCheckOutputGrid = inputConverter.getUserGrid(vConstraintInput);
  BOOST_CHECK_EQUAL(12.34,vCheckOutputGrid[0]);
  BOOST_CHECK_EQUAL(23.45,vCheckOutputGrid[1]);

  //Checking not ordered values for some POINTs
  vConstraintInput[0].timePoint = 0.12;
  vConstraintInput[1].timePoint = 23.45;
  vConstraintInput[2].timePoint = 34.56;
  vConstraintInput[3].timePoint = 56.78;
  vConstraintInput[4].timePoint = 12.34;
  vCheckOutputGrid = inputConverter.getUserGrid(vConstraintInput);
  BOOST_CHECK_EQUAL(12.34,vCheckOutputGrid[0]);
  BOOST_CHECK_EQUAL(23.45,vCheckOutputGrid[1]);
  BOOST_CHECK_EQUAL(34.56,vCheckOutputGrid[2]);

  //Checking simple input scenario
  for(unsigned i=0; i<vConstraintInput.size(); i++)
    vConstraintInput[i].timePoint = 2*i+1;
  vCheckOutputGrid = inputConverter.getUserGrid(vConstraintInput);
  BOOST_CHECK_EQUAL(3,vCheckOutputGrid[0]);
  BOOST_CHECK_EQUAL(5,vCheckOutputGrid[1]);
  BOOST_CHECK_EQUAL(9,vCheckOutputGrid[2]);
}

/**
* @test we are testing the functionality of the help function getCompleteGrid()
*/
BOOST_AUTO_TEST_CASE(testGetCompleteGrid)
{
  FactoryInput::EsoInput eso;
  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  InputConverter inputConverter;
  double duration = 123.456;
  FactoryInput::IntegratorMetaDataInput imdInput;
  imdInput.controls.resize(3);
  imdInput.controls[0].esoIndex = 0;
  imdInput.controls[1].esoIndex = 1;
  imdInput.controls[2].esoIndex = 2;

  for(unsigned i=0; i< imdInput.controls.size(); i++)
  {
    imdInput.controls[i].type = FactoryInput::ParameterInput::CONTROL;
    imdInput.controls[i].value = 2*i+1;
    imdInput.controls[i].grids.resize(2);
    imdInput.controls[i].sensType = FactoryInput::ParameterInput::FULL;
    for(unsigned j=0; j<imdInput.controls[i].grids.size(); j++)
    {
      imdInput.controls[i].grids[j].duration = (i+1)*(j+1);
      imdInput.controls[i].grids[j].timePoints.push_back(0.0);
      imdInput.controls[i].grids[j].timePoints.push_back(1.0);
      imdInput.controls[i].grids[j].values.resize(1);
      imdInput.controls[i].grids[j].approxType = PieceWiseConstant;
    }
  }
  
  imdInput.duration.value = duration;
  imdInput.userGrid.resize(5);
  imdInput.userGrid[0] = 56.78;
  imdInput.userGrid[1] = 23.45;
  imdInput.userGrid[2] = 1;
  imdInput.userGrid[3] = 0.12;
  imdInput.userGrid[4] = 45.67;

  std::vector<double> vOutGrid;
//  BOOST_CHECK_THROW(vOutGrid = getCompleteGrid(imdInput),InputException);
  vOutGrid = inputConverter.getCompleteGrid(imdInput);
  BOOST_CHECK_EQUAL(0/duration, vOutGrid[0]);
  BOOST_CHECK_EQUAL(0.12/duration, vOutGrid[1]);
  BOOST_CHECK_EQUAL(1.0/duration, vOutGrid[2]);
  BOOST_CHECK_EQUAL(2.0/duration, vOutGrid[3]);
  BOOST_CHECK_EQUAL(3.0/duration, vOutGrid[4]);
  BOOST_CHECK_EQUAL(6.0/duration, vOutGrid[5]);
  BOOST_CHECK_EQUAL(9.0/duration, vOutGrid[6]);
  BOOST_CHECK_EQUAL(23.45/duration, vOutGrid[7]);
  BOOST_CHECK_EQUAL(45.670000000000002/duration, vOutGrid[8]);
  BOOST_CHECK_EQUAL(56.780000000000001/duration, vOutGrid[9]);
}

/** @test we are testing the functionality of the help function convertEsoType()
*/
BOOST_AUTO_TEST_CASE(testConvertEsoType)
{
  UserInput::EsoInput::EsoType inputType;
  FactoryInput::EsoInput::EsoType outputType;
  InputConverter inputConverter;
  //test case JADE
  inputType =  UserInput::EsoInput::JADE;
  outputType = inputConverter.convertEsoType(inputType);
  BOOST_CHECK_EQUAL(outputType, FactoryInput::EsoInput::JADE);
}

/** @test we are testing the functionality of the help function convertEsoInput()
*/
BOOST_AUTO_TEST_CASE(testConvertEsoInput)
{
  UserInput::EsoInput input;
  FactoryInput::EsoInput output;
  InputConverter inputConverter;
  
  BOOST_CHECK_THROW(inputConverter.convertEsoInput(input), MissingModelNameException);
}

/** @test we are testing the functionality of the help function convertApproximationType()
*/
BOOST_AUTO_TEST_CASE(testConvertApproximationType)
{
  UserInput::ParameterGridInput input;
  ControlType output;
  InputConverter inputConverter;
  //test case PIECEWISE_CONSTANT
  input.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
  output = inputConverter.convertApproximationType(input.type);
  BOOST_CHECK_EQUAL(output, PieceWiseConstant);
  //test case PIECEWISE_LINEAR not yet implemented
}

/** @test we are testing the functionality of the help function convertParamGridInput()
*/
BOOST_AUTO_TEST_CASE(testConvertParamGridInput)
{
  UserInput::ParameterGridInput input;
  FactoryInput::ParameterizationGridInput output;
  InputConverter inputConverter;
  input.duration = 6;
  input.hasFreeDuration = true;
  input.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
  const double lower = 0.0;
  const double upper = 10.0;
  //test case input.timePoints is empty && input.numIntervals == 0
  BOOST_CHECK_THROW(inputConverter.convertParamGridInput(input, lower, upper),
                    NoGridDefinedException);
  //test case input.timePoints is empty && input.numIntervals != 0
  input.numIntervals = 3;
  output = inputConverter.convertParamGridInput(input, lower, upper);
  for(unsigned i=0; i<input.numIntervals; i++)
    BOOST_CHECK_EQUAL(output.timePoints[i], double(i)/input.numIntervals);
  //test case input.values is not empty && input.values.size() != convertedInput.timePoints.size()
  input.values.resize(input.numIntervals+1);
  BOOST_CHECK_THROW(inputConverter.convertParamGridInput(input, lower, upper),
                    WrongNumberOfParameterValuesDefinedException);


  // test cases input.timePoints is not empty

  // timepoints always are numIntervals + time point 1.0
  std::vector<double> correctInput(input.numIntervals +1);
  const double intervalLength = 1.0/input.numIntervals;
  for(unsigned i=0; i<correctInput.size(); i++){
    correctInput[i] = intervalLength*i;
  }
  //test case input.timePoints do not begin with 0.0
  input.timePoints.assign(correctInput.begin(), correctInput.end());
  input.timePoints.front() = 0.1;
  BOOST_CHECK_THROW(inputConverter.convertParamGridInput(input, lower, upper),
                    WrongGridStructureException);
  
  //test case input.timePoints do not end with 1.0
  input.timePoints.assign(correctInput.begin(), correctInput.end());
  input.timePoints.back() = 1.1;  
  BOOST_CHECK_THROW(inputConverter.convertParamGridInput(input, lower, upper),
                    WrongGridStructureException);
  
  //test case input.timePoints is not in ascending order
  input.timePoints.assign(correctInput.begin(), correctInput.end());
  std::swap(input.timePoints[1], input.timePoints[2]);
  BOOST_CHECK_THROW(inputConverter.convertParamGridInput(input, lower, upper),
                    WrongGridStructureException);
  
  //test case input.timepoints have correct values
  // for piecewise constant values must be of same size as numIntervals
  input.values.resize(input.numIntervals);
  input.timePoints.assign(correctInput.begin(), correctInput.end());
  BOOST_CHECK_NO_THROW(output = inputConverter.convertParamGridInput(input,lower,upper));
  for(unsigned i=0; i<input.timePoints.size(); i++)
    BOOST_CHECK_EQUAL(output.timePoints[i], input.timePoints[i]);
}

/** @test we are testing the functionality of the help function convertParamSensType()
*/
BOOST_AUTO_TEST_CASE(testConvertParamSensType)
{
  UserInput::ParameterInput::ParameterSensitivityType input;
  FactoryInput::ParameterInput::ParameterSensitivityType output;
  InputConverter inputConverter;
  input = UserInput::ParameterInput::FULL;
  output = inputConverter.convertParamSensType(input);
  BOOST_CHECK_EQUAL(output, FactoryInput::ParameterInput::FULL);
}

/** @test we are testing the functionality of the help function convertParamType()
*/
BOOST_AUTO_TEST_CASE(testConvertParamType)
{
  UserInput::ParameterInput::ParameterType input;
  FactoryInput::ParameterInput::ParameterType output;
  InputConverter inputConverter;

  //test case UserInput::ParameterInput::INITIAL
  input = UserInput::ParameterInput::INITIAL;
  output = inputConverter.convertParamType(input);
  BOOST_CHECK_EQUAL(output, FactoryInput::ParameterInput::INITIAL);

  //test case UserInput::ParameterInput::TIME_INVARIANT
  input = UserInput::ParameterInput::TIME_INVARIANT;
  output = inputConverter.convertParamType(input);
  BOOST_CHECK_EQUAL(output, FactoryInput::ParameterInput::TIME_INVARIANT);

  //test case UserInput::ParameterInput::PROFILE
  input = UserInput::ParameterInput::PROFILE;
  output = inputConverter.convertParamType(input);
  BOOST_CHECK_EQUAL(output, FactoryInput::ParameterInput::CONTROL);

}

/** @test we are testing the functionality of the help function convertParameterInputForDuration()
*/
BOOST_AUTO_TEST_CASE(testConvertParameterInputForDuration)
{
  UserInput::ParameterInput input;
  input.paramType  = UserInput::ParameterInput::DURATION;
  FactoryInput::ParameterInput output;
  InputConverter inputConverter;
  output = inputConverter.convertParameterInputForDuration(input);
  BOOST_CHECK_EQUAL(output.type, FactoryInput::ParameterInput::DURATION);
}

/** @test we are testing the functionality of the help function convertParameterInput()
*/
BOOST_AUTO_TEST_CASE(testConvertParameterInput)
{
  UserInput::ParameterInput input;
  FactoryInput::ParameterInput output;
  InputConverter inputConverter;
  input.lowerBound = 0;
  input.upperBound = 10000;
  input.value = 10;
  input.paramType = UserInput::ParameterInput::INITIAL;
  input.sensType = UserInput::ParameterInput::FULL;
  input.grids.resize(3);
  for(unsigned i=0; i<input.grids.size(); i++)
  {
    input.grids[i].duration = i+1;
    input.grids[i].numIntervals = 2;
  }
  output = inputConverter.convertParameterInput(input);
  BOOST_CHECK_EQUAL(output.sensType, FactoryInput::ParameterInput::FULL);
  //removed test part where ParameterInput::NO is expected
  //now the user can directly set the sensType to FULL, FRACTIONAL or NO
}

/** @test we are testing the functionality of the help function convertMappingType()
*/
BOOST_AUTO_TEST_CASE(testConvertMappingType)
{
  UserInput::Input::RunningMode input;
  FactoryInput::MappingInput::MappingType output;
  InputConverter inputConverter;

  //test case SIMULATION
  input = UserInput::Input::SIMULATION;
  output = inputConverter.convertMappingType(input);
  BOOST_CHECK_EQUAL(output, FactoryInput::MappingInput::SINGLE_SHOOTING);

  //test case SINGLE_SHOOTING
  input = UserInput::Input::SINGLE_SHOOTING;
  output = inputConverter.convertMappingType(input);
  BOOST_CHECK_EQUAL(output, FactoryInput::MappingInput::SINGLE_SHOOTING);
}

/** @test we are testing the functionality of the help function convertMappingInput()
*/
BOOST_AUTO_TEST_CASE(testConvertMappingInput)
{
  FactoryInput::EsoInput eso1, eso2;
  eso1.model = TEST_OBJECT_NAME;
  eso1.type = FactoryInput::EsoInput::JADE;
  eso2.model = TEST_OBJECT_NAME;
  eso2.type = FactoryInput::EsoInput::JADE;
  InputConverter inputConverter;
  GenericEsoFactory genEsoFac;

  UserInput::Input::RunningMode mode = UserInput::Input::SINGLE_SHOOTING;
  UserInput::StageMapping mappingInput;
  UserInput::IntegratorStageInput isInput1, isInput2;
  isInput1.duration.value = 6;
  isInput2.duration.value = 7;
  isInput1.parameters.resize(3);
  isInput2.parameters.resize(3);
  isInput1.parameters[0].name = "s";
  isInput1.parameters[1].name = "t";
  isInput1.parameters[2].name = "deriv";
  isInput2.parameters[0].name = "s";
  isInput2.parameters[1].name = "deriv";
  isInput2.parameters[2].name = "a";
  isInput2.parameters[2].grids.resize(1);
  isInput2.parameters[2].grids[0].duration = 7;
  isInput2.parameters[2].grids[0].timePoints.resize(1);
  isInput2.parameters[2].grids[0].timePoints[0] = 6;
  isInput2.parameters[2].grids[0].numIntervals = 2;
  isInput2.parameters[2].grids[0].values.resize(1);
  isInput2.parameters[2].grids[0].values[0] = 3;
  FactoryInput::IntegratorMetaDataInput imdInputStage1;// = inputConverter.createIntegratorMetaDataInputStage(isInput1, eso1);
  FactoryInput::IntegratorMetaDataInput imdInputStage2;// = inputConverter.createIntegratorMetaDataInputStage(isInput2, eso2);

  imdInputStage1.esoPtr = genEsoFac.create(eso1);
  imdInputStage2.esoPtr = genEsoFac.create(eso2);

  FactoryInput::MappingInput output;
  mappingInput.fullStateMapping = true;
  BOOST_CHECK_NO_THROW(output = inputConverter.convertMappingInput(mode, mappingInput, imdInputStage1, imdInputStage2));
  mappingInput.fullStateMapping = false;
  BOOST_CHECK_NO_THROW(output = inputConverter.convertMappingInput(mode, mappingInput, imdInputStage1, imdInputStage2));
}

/** @test we are testing the functionality of the help function createMappingInputVector()
*/
BOOST_AUTO_TEST_CASE(testCreateMappingInputVector)
{
  UserInput::Input::RunningMode mode = UserInput::Input::SINGLE_SHOOTING;
  std::vector<FactoryInput::IntegratorMetaDataInput> convertedStages;
  std::vector<UserInput::StageInput> stages;
  std::vector<FactoryInput::MappingInput> output;
  InputConverter inputConverter;
  convertedStages.resize(1);
  convertedStages[0].controls.resize(1);
  stages.resize(1);
  BOOST_CHECK_NO_THROW(output = inputConverter.createMappingInputVector(mode, convertedStages, stages));
}

/** @test we are testing the functionality of the help function convertIntegrationOrder()
*/
BOOST_AUTO_TEST_CASE(testConvertIntegrationOrder)
{
  UserInput::IntegratorInput::IntegrationOrder order;
  InputConverter inputConverter;

  order = UserInput::IntegratorInput::ZEROTH;
  BOOST_CHECK_EQUAL(inputConverter.convertIntegrationOrder(order),
                    FactoryInput::IntegratorInput::ZEROTH);

  order = UserInput::IntegratorInput::FIRST_FORWARD;
  BOOST_CHECK_EQUAL(inputConverter.convertIntegrationOrder(order),
                    FactoryInput::IntegratorInput::FIRST_FORWARD);

  order = UserInput::IntegratorInput::FIRST_REVERSE;
  BOOST_CHECK_EQUAL(inputConverter.convertIntegrationOrder(order),
                    FactoryInput::IntegratorInput::FIRST_REVERSE);

  order = UserInput::IntegratorInput::SECOND_REVERSE;
  BOOST_CHECK_EQUAL(inputConverter.convertIntegrationOrder(order),
                    FactoryInput::IntegratorInput::SECOND_REVERSE);
}

/** @test we are testing the functionality of the help function convertIntegratorType()
*/
BOOST_AUTO_TEST_CASE(testConvertIntegratorType)
{
  UserInput::IntegratorInput::IntegratorType type;
  InputConverter inputConverter;

  type = UserInput::IntegratorInput::LIMEX;
  BOOST_CHECK_EQUAL(inputConverter.convertIntegratorType(type), FactoryInput::IntegratorInput::LIMEX);

  type = UserInput::IntegratorInput::NIXE;
  BOOST_CHECK_EQUAL(inputConverter.convertIntegratorType(type), FactoryInput::IntegratorInput::NIXE);
}

/** @test we are testing the functionality of the help function createIntegratorMetaDataInputStage()
*/
BOOST_AUTO_TEST_CASE(testCreateIntegratorMetaDataInputStage)
{
  UserInput::IntegratorStageInput input;
  UserInput::IntegratorInput::IntegrationOrder order = UserInput::IntegratorInput::IntegrationOrder::FIRST_FORWARD;
  FactoryInput::EsoInput eso;
  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  InputConverter inputConverter;

  GenericEsoFactory genEsoFac;
  GenericEso::Ptr esoPtr = genEsoFac.create(eso);

  input.duration.value = 7;
  input.duration.paramType = UserInput::ParameterInput::DURATION;
  input.parameters.resize(3);
  input.parameters[0].name = "s";
  input.parameters[1].name = "t";
  input.parameters[2].name = "a";
  
  for(unsigned i=0; i<5; i++){
    input.explicitPlotGrid.push_back(double(i)/10 + 0.3);
  }
  
  FactoryInput::IntegratorMetaDataInput output;
  // test case grids empty
  BOOST_CHECK_THROW(inputConverter.createIntegratorMetaDataInputStage(input, esoPtr), NoGridsDefinedOnControlException);
  
  
  // test case parameter type PROFILE
  for(unsigned i=0; i<input.parameters.size(); i++)
  {
    input.parameters[i].grids.resize(1);
    input.parameters[i].paramType = UserInput::ParameterInput::PROFILE;
    input.parameters[i].grids.front().numIntervals = 1;
    input.parameters[i].grids[0].duration = 7;
  }
  for(unsigned i=0; i<input.parameters.size(); i++)
  {
    BOOST_CHECK_NO_THROW(output = inputConverter.createIntegratorMetaDataInputStage(input, esoPtr));
    BOOST_CHECK_EQUAL(output.controls[i].type, FactoryInput::ParameterInput::CONTROL);//.grids[0].duration, input.parameters[i].grids[0].duration);
  }
  
  // test case parameter type INITIAL
  for(unsigned i=0; i<input.parameters.size(); i++)
  {
    input.parameters[i].grids.resize(1);
    input.parameters[i].paramType = UserInput::ParameterInput::INITIAL;
    input.parameters[i].grids.front().numIntervals = 1;
    input.parameters[i].grids[0].duration = 7;
  }
  for(unsigned i=0; i<input.parameters.size(); i++)
  {
    BOOST_CHECK_NO_THROW(output = inputConverter.createIntegratorMetaDataInputStage(input, esoPtr));
    BOOST_CHECK_EQUAL(output.initials[i].type, FactoryInput::ParameterInput::INITIAL);//.grids[0].duration, input.parameters[i].grids[0].duration);
  }
  
  //test of error handling if explicitPlotGrid has out of bounds values
  const double epsilon = 1e-6;
  double oldValue = input.explicitPlotGrid.front();
  input.explicitPlotGrid.front() = 0.0 - epsilon;
  BOOST_CHECK_THROW(inputConverter.createIntegratorMetaDataInputStage(input, esoPtr), ExplicitPlotGridNotInBoundsException);
  input.explicitPlotGrid.front() = oldValue;
  
  oldValue = input.explicitPlotGrid.back();
  input.explicitPlotGrid.back() = 1.0 + epsilon;
  BOOST_CHECK_THROW(inputConverter.createIntegratorMetaDataInputStage(input, esoPtr), ExplicitPlotGridNotInBoundsException);
  input.explicitPlotGrid.back() = oldValue;
  
  //test error handling if explicitPlotGrid is not ordered
  oldValue = input.explicitPlotGrid[2];
  input.explicitPlotGrid[2] = 0.25;
  BOOST_CHECK_THROW(inputConverter.createIntegratorMetaDataInputStage(input, esoPtr), ExplicitPlotGridNotOrderedException);
  input.explicitPlotGrid[2] = oldValue;
  
  //test error handling of the same parameter defined twice
  input.parameters.push_back(input.parameters.front());
  BOOST_CHECK_THROW(inputConverter.createIntegratorMetaDataInputStage(input, esoPtr), DefinedParameterTwiceException);
}

/** @test we are testing the functionality of the help function createIntegratorMetaDataInput()
*/
BOOST_AUTO_TEST_CASE(testCreateIntegratorMetaDataInput)
{
  UserInput::Input::RunningMode mode = UserInput::Input::SINGLE_SHOOTING;
  UserInput::IntegratorInput::IntegrationOrder order = UserInput::IntegratorInput::IntegrationOrder::FIRST_FORWARD;
  std::vector<UserInput::StageInput> stageInputVector;
  UserInput::EsoInput eso;
  eso.model = TEST_OBJECT_NAME;
  eso.type = UserInput::EsoInput::JADE;
  InputConverter inputConverter;
  stageInputVector.resize(3);
  for(unsigned i=0; i<stageInputVector.size(); i++)
  {
    stageInputVector[i].eso.model = eso.model;
    stageInputVector[i].eso.type = eso.type;
    stageInputVector[i].integrator.duration.value = 7;
    stageInputVector[i].integrator.duration.paramType = UserInput::ParameterInput::DURATION;
    stageInputVector[i].integrator.parameters.resize(1);
    stageInputVector[i].integrator.parameters[0].name = "s";
    stageInputVector[i].integrator.parameters[0].grids.resize(1);
    stageInputVector[i].integrator.parameters[0].grids[0].duration = 7;
    unsigned numIntervals = 5;
    stageInputVector[i].integrator.parameters[0].grids[0].hasFreeDuration = true;
    stageInputVector[i].integrator.parameters[0].grids[0].numIntervals = numIntervals;
    stageInputVector[i].integrator.parameters[0].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    stageInputVector[i].integrator.parameters[0].grids[0].values.resize(numIntervals);
    for(unsigned j=0; j<stageInputVector[i].integrator.parameters[0].grids[0].values.size(); j++)
      stageInputVector[i].integrator.parameters[0].grids[0].values[j] = i+j;
    stageInputVector[i].integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
    stageInputVector[i].integrator.parameters[0].lowerBound = 0;
    stageInputVector[i].integrator.parameters[0].upperBound = 9;
    stageInputVector[i].integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
    stageInputVector[i].integrator.parameters[0].value = 6;
  }
  FactoryInput::IntegratorMetaDataInput metaDataInput;
  BOOST_CHECK_NO_THROW(metaDataInput = inputConverter.createIntegratorMetaDataInput(mode, order, stageInputVector));
  BOOST_CHECK_EQUAL(metaDataInput.stages.size(), stageInputVector.size());
}

/** @test we are testing the functionality of the help function createIntegratorInput()
*/
BOOST_AUTO_TEST_CASE(testCreateIntegratorInput)
{
  IntegratorInputDummies iidObj;
  UserInput::Input::RunningMode mode;
  UserInput::IntegratorInput input;
  std::vector<UserInput::StageInput> stageInputVector;
  mode = UserInput::Input::SINGLE_SHOOTING;
  input.order = UserInput::IntegratorInput::ZEROTH;
  input.type = UserInput::IntegratorInput::LIMEX;
  UserInput::EsoInput eso;
  eso.model = TEST_OBJECT_NAME;
  eso.type = UserInput::EsoInput::JADE;
  stageInputVector.resize(3);
  for(unsigned i=0; i<stageInputVector.size(); i++)
  {
    stageInputVector[i].eso.model = eso.model;
    stageInputVector[i].eso.type = eso.type;
    stageInputVector[i].integrator.duration.value = 7;
    stageInputVector[i].integrator.duration.paramType = UserInput::ParameterInput::DURATION;
    stageInputVector[i].integrator.parameters.resize(1);
    stageInputVector[i].integrator.parameters[0].name = "s";
    stageInputVector[i].integrator.parameters[0].grids.resize(1);
    stageInputVector[i].integrator.parameters[0].grids[0].duration = 7;
    unsigned numIntervals = 5;
    stageInputVector[i].integrator.parameters[0].grids[0].hasFreeDuration = true;
    stageInputVector[i].integrator.parameters[0].grids[0].numIntervals = numIntervals;
    stageInputVector[i].integrator.parameters[0].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    stageInputVector[i].integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
    stageInputVector[i].integrator.parameters[0].lowerBound = 0;
    stageInputVector[i].integrator.parameters[0].upperBound = 9;
    stageInputVector[i].integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
    stageInputVector[i].integrator.parameters[0].value = 6;
  }
  BOOST_CHECK_NO_THROW(iidObj.createIntegratorInput(mode, input, stageInputVector));
}

/** @test we are testing the functionality of the help function createPathConstraint()
*/
BOOST_AUTO_TEST_CASE(testCreatePathConstraint)
{
  InputConverter inputConverter;
  std::vector<FactoryInput::ConstraintInput> output;
  FactoryInput::ConstraintInput constraint;
  std::vector<double> completeGrid;
  constraint.esoIndex = 3;
  constraint.lagrangeMultiplier = 0;
  constraint.lowerBound = 0;
  constraint.upperBound = 9;
  constraint.timePoint = 6;
  completeGrid.resize(3);
  for(unsigned i=0; i<completeGrid.size(); i++)
    completeGrid[0] = 2*(i+1);
  BOOST_CHECK_NO_THROW(output = inputConverter.createPathConstraint(constraint, completeGrid));
}

/** @test we are testing the functionality of the help function convertConstraintInput()
*/
BOOST_AUTO_TEST_CASE(testConvertConstraintInput)
{
  UserInput::ConstraintInput input;
  FactoryInput::IntegratorMetaDataInput imdInput;
  FactoryInput::EsoInput eso;
  InputConverter inputConverter;
  GenericEsoFactory genEsoFac;
  GenericEso::Ptr esoPtr;

  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  esoPtr = genEsoFac.create(eso);
  

  imdInput.controls.resize(4);
  imdInput.controls[0].esoIndex = 0;
  imdInput.controls[1].esoIndex = 1;
  imdInput.controls[2].esoIndex = 2;
  imdInput.controls[3].esoIndex = 3;

  input.type = UserInput::ConstraintInput::POINT;
  input.name = "s";
  input.lagrangeMultiplier = 1;
  input.lowerBound = 2;
  input.upperBound = 7;
  input.timePoint = 6;

  BOOST_CHECK_THROW(inputConverter.convertConstraintInput(input, imdInput, esoPtr), TimePointNotAllowedException);
  input.timePoint = 0.7654;
  FactoryInput::ConstraintInput outputConstraint = inputConverter.convertConstraintInput(input, imdInput, esoPtr);
  BOOST_CHECK_EQUAL(outputConstraint.lagrangeMultiplier, input.lagrangeMultiplier);
  BOOST_CHECK_EQUAL(outputConstraint.lowerBound, input.lowerBound);
  BOOST_CHECK_EQUAL(outputConstraint.upperBound, input.upperBound);
  BOOST_CHECK_EQUAL(outputConstraint.timePoint, input.timePoint);
}

/** @test we are testing the functionality of the help function convertConstraintInputVector()
*/
BOOST_AUTO_TEST_CASE(testConvertConstraintInputVector)
{
  std::vector<UserInput::ConstraintInput> inputVector;
  FactoryInput::IntegratorMetaDataInput imdInput;
  FactoryInput::EsoInput eso;
  GenericEsoFactory genEsoFac;
  GenericEso::Ptr esoPtr;
  std::vector<double> completeGrid;
  InputConverter inputConverter;

  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  esoPtr = genEsoFac.create(eso);

  imdInput.controls.resize(4);
  imdInput.controls[0].esoIndex = 0;
  imdInput.controls[1].esoIndex = 1;
  imdInput.controls[2].esoIndex = 2;
  imdInput.controls[3].esoIndex = 3;

  inputVector.resize(3);
  inputVector[0].type = UserInput::ConstraintInput::POINT;
  inputVector[1].type = UserInput::ConstraintInput::ENDPOINT;
  //inputVector[1].type = UserInput::ConstraintInput::POINT;
  inputVector[2].type = UserInput::ConstraintInput::PATH;
  inputVector[0].name = "s";
  inputVector[1].name = "v";
  inputVector[2].name = "t";

  for(unsigned i=0; i<inputVector.size(); i++)
  {
    inputVector[i].lagrangeMultiplier = 1.0;
    inputVector[i].lowerBound = 2.0;
    inputVector[i].upperBound = 7.0;
    inputVector[i].timePoint = 0.6;
  }
  std::vector<FactoryInput::ConstraintInput> outputConstraints;
  BOOST_CHECK_NO_THROW(outputConstraints = inputConverter.convertConstraintInputVector
                                                                              (inputVector,
                                                                               imdInput,
                                                                               esoPtr));
}

/** @test we are testing the functionality of the help function removeDoubleConstraints()
*/
BOOST_AUTO_TEST_CASE(testRemoveDoubleConstraints)
{
  std::vector<FactoryInput::ConstraintInput> constraints;
  InputConverter inputConverter;

  constraints.resize(4);
  for(unsigned i=0; i<constraints.size()-1; i++)
  {
    constraints[i].esoIndex = i;
    constraints[i].lagrangeMultiplier = 1;
    constraints[i].lowerBound = i+1;
    constraints[i].upperBound = 2*i+3;
    constraints[i].timePoint = 2*i+1;
  }
  constraints[3].esoIndex= 2;
  constraints[3].lagrangeMultiplier = 1;
  constraints[3].lowerBound = 3;
  constraints[3].timePoint = 5;
  inputConverter.removeDoubleConstraints(constraints);
  BOOST_CHECK_EQUAL(constraints.size(),3);
}

/** @test we are testing the functionality of the help function createOptimizerMetaDataInputStage()
*/
BOOST_AUTO_TEST_CASE(testCreateOptimizerMetaDataInputStage)
{
  OptimizerDummies odObj;
  FactoryInput::OptimizerMetaDataInput optMetaDataInput;
  UserInput::OptimizerStageInput input;
  FactoryInput::IntegratorMetaDataInput imdInput;
  input.constraints.resize(1);
  BOOST_CHECK_NO_THROW(odObj.createOptimizerMetaDataInputStage(input, imdInput));
}

/** @test we are testing the functionality of the help function createOptimizerMetaDataInput()
*/
BOOST_AUTO_TEST_CASE(testCreateOptimizerMetaDataInput)
{
  OptimizerDummies odObj;
  UserInput::OptimizerInput input;
  FactoryInput::IntegratorMetaDataInput imdInput;
  std::vector<UserInput::StageInput> stageInputVector;
  GenericEsoFactory genEsoFac;

  input.globalConstraints.resize(5);

  imdInput.stages.resize(3);
  FactoryInput::EsoInput eso;
  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  for(unsigned i=0; i<imdInput.stages.size(); i++)
  {
    imdInput.stages[i].esoPtr = genEsoFac.create(eso);
  }
  stageInputVector.resize(3);
  for(unsigned i=0; i<stageInputVector.size(); i++)
    stageInputVector[i].optimizer.constraints.resize(2);
  BOOST_CHECK_NO_THROW(odObj.createOptimizerMetaDataInput(input, imdInput, stageInputVector));
}

/** @test we are testing the functionality of the help function convertOptimizerType()
*/
BOOST_AUTO_TEST_CASE(testConvertOptimizerType)
{
  UserInput::OptimizerInput::OptimizerType type;
  InputConverter inputConverter;
  type = UserInput::OptimizerInput::SNOPT;
  BOOST_CHECK_EQUAL(inputConverter.convertOptimizerType(type), FactoryInput::OptimizerInput::SNOPT);
}

/** @test we are testing the functionality of the help function createOptimizerInput()
*/
BOOST_AUTO_TEST_CASE(testCreateOptimizerInput)
{
  OptimizerDummies odObj;
  UserInput::OptimizerInput optimizerInput;
  FactoryInput::IntegratorInput integratorInput;
  std::vector<UserInput::StageInput> stageInputVector;
  GenericEsoFactory genEsoFac;

  integratorInput.metaDataInput.stages.resize(3);
  FactoryInput::EsoInput eso;
  eso.model = TEST_OBJECT_NAME;
  eso.type = FactoryInput::EsoInput::JADE;
  for(unsigned i=0; i< integratorInput.metaDataInput.stages.size(); i++)
  {
    integratorInput.metaDataInput.stages[0].esoPtr = genEsoFac.create(eso);
  }
  stageInputVector.resize(3);
  BOOST_CHECK_NO_THROW(odObj.createOptimizerInput(optimizerInput, integratorInput, stageInputVector));
}

/** @test we are testing the functionality of the help function convertInput()
*/
BOOST_AUTO_TEST_CASE(testConvertInput)
{
  UserInput::Input input;
  
  InputConverter inputConverter;

  input.runningMode = UserInput::Input::SIMULATION;
  input.stages.resize(3);
  for(unsigned i=0; i<input.stages.size(); i++)
  {
    input.stages[i].eso.model = TEST_OBJECT_NAME;
    input.stages[i].eso.type = UserInput::EsoInput::JADE;
    input.stages[i].integrator.duration.name = "t";
    input.stages[i].integrator.duration.paramType = UserInput::ParameterInput::DURATION;
    input.stages[i].integrator.duration.lowerBound = 2;
    input.stages[i].integrator.duration.upperBound = 7;
    input.stages[i].integrator.duration.sensType =  UserInput::ParameterInput::FRACTIONAL;
    input.stages[i].integrator.duration.value = 0.0;
    input.stages[i].integrator.parameters.resize(1);
    input.stages[i].integrator.parameters[0].paramType = UserInput::ParameterInput::INITIAL;
    input.stages[i].integrator.parameters[0].name = "s";
    input.stages[i].integrator.parameters[0].value = 10;
    input.stages[i].integrator.parameters[0].lowerBound = 2;
    input.stages[i].integrator.parameters[0].upperBound = 7;
    input.stages[i].integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
    input.stages[i].integrator.parameters[0].grids.resize(1);
    input.stages[i].integrator.parameters[0].grids[0].duration = 0.0;
    input.stages[i].integrator.parameters[0].grids[0].hasFreeDuration = false;
    unsigned numIntervals = 1;
    input.stages[i].integrator.parameters[0].grids[0].numIntervals = numIntervals;
    input.stages[i].integrator.parameters[0].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    input.stages[i].integrator.parameters[0].grids[0].values.resize(numIntervals);
  }
  FactoryInput::IntegratorMetaDataInput imdInput;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  BOOST_CHECK_NO_THROW(ProblemInput outputProblem = inputConverter.convertInput(input));
}

/** 
* @test check if the correct exceptions are thrown
*/
BOOST_AUTO_TEST_CASE(testConvertInputExceptions)
{
  // check throwing of an exception if optimiztation without derivatives is attempted
  UserInput::Input input;
  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.integratorInput.order = UserInput::IntegratorInput::ZEROTH;
  TestConvertInputExceptionDummy inputConverter;
  ProblemInput convertedInput;
  BOOST_CHECK_NO_THROW(convertedInput = inputConverter.convertInput(input));
  BOOST_CHECK_EQUAL(convertedInput.integrator.order, FactoryInput::IntegratorInput::FIRST_FORWARD);
}

BOOST_AUTO_TEST_CASE(TestCheckConstraintForIndependency)
{
  UserInput::Input input;
  UserInput::StageInput stage;
  stage.eso.model = "carErweitert";
  stage.eso.type = UserInput::EsoInput::JADE;
  
  UserInput::IntegratorStageInput integratorStage;
  UserInput::ParameterInput control;
  control.paramType = UserInput::ParameterInput::TIME_INVARIANT;
  control.name = "a";
  integratorStage.parameters.push_back(control);
  
  integratorStage.duration.paramType = UserInput::ParameterInput::DURATION;
  
  stage.integrator = integratorStage;
  
  UserInput::OptimizerStageInput optimizerStage;
  optimizerStage.objective.name = "deriv";
  
  UserInput::ConstraintInput constraint;
  constraint.name = "v";
  constraint.type = UserInput::ConstraintInput::ENDPOINT;
  optimizerStage.constraints.push_back(constraint);
  
  
  stage.optimizer = optimizerStage;

  
  input.stages.push_back(stage);
  InputConverter ic;
  BOOST_CHECK_THROW(ic.convertInput(input), EndpointConstraintDirectlyDependsOnControlException);
  
  input.stages.front().optimizer.objective.name = "obj";
  input.stages.front().optimizer.constraints.front().name = "deriv";
  BOOST_CHECK_THROW(ic.convertInput(input), EndpointConstraintDirectlyDependsOnControlException);
  
}
BOOST_AUTO_TEST_SUITE_END()
