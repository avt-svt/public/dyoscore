/**
* @file TestInputDummies.hpp
*
* ==========================================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                                         \n
* ==========================================================================================\n
* ==========================================================================================\n
* Input  - part of dyos                                                                     \n
* ==========================================================================================\n
* This file contains dummy classes & functions for unit tests for the Input module          \n
* ==========================================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 11.06.2012
*/

#pragma once

#include "Input.hpp"

/********************************************************************* 
**    Class for overloading functions related to IntegratorInput      
**********************************************************************/
class IntegratorInputDummies: public InputConverter
{
public:
  FactoryInput::IntegratorInput::IntegrationOrder convertIntegrationOrder
                (const UserInput::IntegratorInput::IntegrationOrder order) const
             
 {
   return  FactoryInput::IntegratorInput::ZEROTH;
 }
  FactoryInput::IntegratorInput::IntegratorType convertIntegratorType
                (const UserInput::IntegratorInput::IntegratorType type) const
  {
    return FactoryInput::IntegratorInput::LIMEX;
  }
  struct FactoryInput::IntegratorMetaDataInput createIntegratorMetaDataInput
                             (const UserInput::Input::RunningMode &mode,
							const UserInput::IntegratorInput::IntegrationOrder order,
                            const std::vector<UserInput::StageInput> &stageInputVector)
  {
    FactoryInput::IntegratorMetaDataInput output;
    output = InputConverter::createIntegratorMetaDataInput(mode, order, stageInputVector);
    return output;
  }
  struct FactoryInput::IntegratorInput createIntegratorInput
                             (const UserInput::Input::RunningMode &mode,
                              const UserInput::IntegratorInput &input,
                              const std::vector<UserInput::StageInput> &stageInputVector)
  {
    FactoryInput::IntegratorInput output;
    output.order = convertIntegrationOrder(input.order);
    output.type = convertIntegratorType(input.type);
    output.metaDataInput = createIntegratorMetaDataInput(mode, input.order, stageInputVector);
    return output;
  }
};
/********************************************************************* 
**    Class for overloading functions related to UserGrid      
**********************************************************************/
class UserGridDummies: public InputConverter
{
public:
    std::vector<double> getUserGrid(const std::vector<UserInput::ConstraintInput> &constraints) const
    {
      std::vector<double> output;
      output.resize(5);
      for(unsigned i=0; i<output.size(); i++)
        output[i] = i+1;
      return output;
    }
    std::vector<double> getCompleteGrid(const FactoryInput::IntegratorMetaDataInput &input) const
    {
      std::vector<double> output;
      output.resize(9);
      for(unsigned i=0; i<output.size(); i++)
        output[i] = i+1;
      return output;
    }
};
/********************************************************************* 
**    Class for overloading functions related to ConstraintInput      
**********************************************************************/
class ConstraintInputDummies: public InputConverter
{
public:
  void checkConstraintNames(const std::vector<UserInput::ConstraintInput> &constraints,
                          const FactoryInput::IntegratorMetaDataInput &imdInput,
                          const GenericEso::Ptr &eso) const
  {
  }
  std::vector<FactoryInput::ConstraintInput> convertConstraintInputVector(
                             const std::vector<UserInput::ConstraintInput> &inputVector,
                             const FactoryInput::IntegratorMetaDataInput &imdInput,
                             const GenericEso::Ptr &eso,
                             const std::vector<double> &completeGrid) const
  {
    std::vector<FactoryInput::ConstraintInput> output;
    output.resize(5);
    for(unsigned i=0; i<output.size(); i++)
    {
      output[i].esoIndex = 2;
      output[i].lowerBound = 0.0;
      output[i].upperBound = 9.0;
      output[i].timePoint = 2*(i+1);
      output[i].lagrangeMultiplier = 0.0;
    }
    return output;

  }
  struct FactoryInput::ConstraintInput convertConstraintInput
                                             (const UserInput::ConstraintInput &input,
                                              const FactoryInput::IntegratorMetaDataInput &imdInput,
                                              const GenericEso::Ptr &eso)
  {
    FactoryInput::ConstraintInput constraint;
    constraint.esoIndex = 2;
    constraint.lowerBound = 0.0;
    constraint.upperBound = 9.0;
    constraint.timePoint = 6.0;
    constraint.lagrangeMultiplier = 0.0;
    return constraint;
  }
  std::vector<FactoryInput::ConstraintInput> createPathConstraint
                                              (const FactoryInput::ConstraintInput constraint,
                                               const std::vector<double> &completeGrid) const
  {
    std::vector<FactoryInput::ConstraintInput> output;
    output.resize(5);
    for(unsigned i=0; i<output.size(); i++)
    {
      output[i].esoIndex = 2;
      output[i].lowerBound = 0.0;
      output[i].upperBound = 9.0;
      output[i].timePoint = 2*(i+1);
      output[i].lagrangeMultiplier = 0.0;
    }
    return output;
  }
};

/********************************************************************* 
**    Class for overloading functions related to OptimizerDummies      
**********************************************************************/
class OptimizerDummies: public InputConverter
{
public:
  FactoryInput::OptimizerMetaDataInput createOptimizerMetaDataInputStage
                                                   (const UserInput::OptimizerStageInput &input,
                                                    FactoryInput::IntegratorMetaDataInput &imdInput) const
  {
    FactoryInput::OptimizerMetaDataInput output;
    ConstraintInputDummies cidObj;
    UserGridDummies ugdObj;

    std::vector<double> userGrid = ugdObj.getUserGrid(input.constraints);
    imdInput.userGrid = userGrid;
    std::vector<double> completeGrid = ugdObj.getCompleteGrid(imdInput);
    imdInput.userGrid = completeGrid;

    cidObj.checkConstraintNames(input.constraints, imdInput, imdInput.esoPtr);
    std::vector<FactoryInput::ConstraintInput> constraints = cidObj.convertConstraintInputVector(input.constraints,
                                                                            imdInput,
                                                                            imdInput.esoPtr,
                                                                            completeGrid);

    output.constraints = constraints;

    output.objective = cidObj.convertConstraintInput(input.objective, imdInput, imdInput.esoPtr);
    output.objective.timePoint = 1.0;

    return output;
  }
  FactoryInput::OptimizerMetaDataInput createOptimizerMetaDataInput(
                                    const UserInput::OptimizerInput &input,
                                          FactoryInput::IntegratorMetaDataInput &imdInput,
                                    const std::vector<UserInput::StageInput> &stageInputVector)
  {
    FactoryInput::OptimizerMetaDataInput output;
    ConstraintInputDummies cidObj;
    std::vector<double> completeGrid;
    std::vector<FactoryInput::ConstraintInput> constraints = cidObj.convertConstraintInputVector(input.globalConstraints,
                                                                            imdInput,
                                                                            imdInput.stages.front().esoPtr,
                                                                            completeGrid);
    output.constraints = constraints;

    unsigned numStages = stageInputVector.size();
    output.treatObjective.resize(numStages);
    output.stages.resize(numStages);
    for(unsigned i=0; i<numStages; i++){
      output.treatObjective[i] = stageInputVector[i].treatObjective;
      output.stages[i] = createOptimizerMetaDataInputStage(stageInputVector[i].optimizer,
                                                                     imdInput.stages[i]);
    }
    return output;
  }
  FactoryInput::OptimizerInput createOptimizerInput
                                   (const UserInput::OptimizerInput &optimizerInput,
                                          FactoryInput::IntegratorInput &integratorInput,
                                    const std::vector<UserInput::StageInput> &stageInputVector)
  {
    FactoryInput::OptimizerInput output;
    output.type = convertOptimizerType(UserInput::OptimizerInput::SNOPT);
    output.metaDataInput = createOptimizerMetaDataInput(optimizerInput, integratorInput.metaDataInput, stageInputVector);
    output.integratorInput = integratorInput;
    return output;
  }
};

/********************************************************************* 
**    Class for overloading functions related to GetIndexDummies      
**********************************************************************/
class GetIndexDummies: public InputConverter
{
public:
  void getEsoNames(std::vector<std::string> &esoNames, 
             const FactoryInput::EsoInput &eso);

  unsigned getEsoIndex(const std::vector<std::string> &esoNames, 
                       const std::string name)
  {
    return 2;
  }
  unsigned getEquationIndex(const unsigned esoIndex,
                          const FactoryInput::IntegratorMetaDataInput &imdInput,
                          const FactoryInput::EsoInput &eso)
  {
    return 3;
  }
};

/**
* @class TestConvertInputExceptionDummy
* @brief dummy class overloading all converion methods called by convertInput
*
* In order to have an easy test input for testing exceptions thrown by the 
* function convertInput, all subfunctions are overloaded with empty functions
*/
class TestConvertInputExceptionDummy : public InputConverter
{
public:
  virtual struct FactoryInput::IntegratorInput createIntegratorInput
                             (const UserInput::Input::RunningMode &mode,
                              const UserInput::IntegratorInput &input,
                              const std::vector<UserInput::StageInput> &stageInputVector)
  {
    FactoryInput::IntegratorInput dummy;
    return dummy;
  }
  
  virtual FactoryInput::OptimizerInput createOptimizerInput(const UserInput::OptimizerInput &optimizerInput,
                                      FactoryInput::IntegratorInput &integratorInput,
                                      const std::vector<UserInput::StageInput> &stageInputVector)
  {
    FactoryInput::OptimizerInput dummy;
    return dummy;
  }
};