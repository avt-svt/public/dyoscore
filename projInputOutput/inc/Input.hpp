/** 
* @file Input.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Input                                                                \n
* =====================================================================\n
* This file contains all declarations of functions to convert UserInput\n
* into module input needed to create the module objects. The main      \n
* program then uses the factories and the module input to build the    \n
* corresponding objects                                                \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 02.03.2012
* @sa UserInput.hpp EsoInput.hpp MetaDataInput.hpp IntegratorInput.hpp OptimizerInput.hpp
*/

#pragma once
#include "SolverConfig.hpp"
#include "InputExceptions.hpp"
#include "UserInput.hpp"

#include "EsoInput.hpp"
#include "MetaDataInput.hpp"
#include "IntegratorInput.hpp"
#include "OptimizerInput.hpp"
#include "DyosObject.hpp"




/**
* @brief struct containing the complete information for a dyos run
*/
struct ProblemInput
{
  /// type of the problem
  enum ProblemType{
    OPTIMIZATION, ///defining an optimization run (so a GenericOptimizer object is created)
    SIMULATION ///defining a simulation (so merely a GenericIntegrator object is created)
  };
  /// input for the Optimizer factory (not needed for problem type SIMULATION)
  FactoryInput::OptimizerInput optimizer;
  /// input for the Integrator factory (not needed for the problem type OPTIMIZATION)
  FactoryInput::IntegratorInput integrator;
  /// type of the dyos run
  ProblemType type;
};

class InputConverter : public DyosObject
{
protected:
  /// @brief currently used GenericEso pointer
  GenericEso::Ptr m_genEso;
  /// @brief name of the currently used model
  std::string m_model;
  
  GenericEso::Ptr getGenericEsoPtr(const FactoryInput::EsoInput &eso);
public:
  //check functions
  virtual void checkConstraintNames(const std::vector<UserInput::ConstraintInput> &constraints,
                                    const FactoryInput::IntegratorMetaDataInput &imdInput,
                                    const GenericEso::Ptr &eso);
  virtual void checkParameterNames(const std::vector<UserInput::ParameterInput> &parameters);
  virtual void checkObjectiveName(const std::vector<UserInput::ConstraintInput> &constraints,
                                  const UserInput::ConstraintInput &objective);
  virtual void checkConstraintForIndependency(const UserInput::ConstraintInput &constraint,
                                              const FactoryInput::IntegratorMetaDataInput &imdInput);
  virtual void checkExplicitPlotGrid(const std::vector<double> &explicitPlotGrid);
  //help functions
  
  virtual void getEsoNames(std::vector<std::string> &esoNames, 
                     const GenericEso::Ptr &esoPtr);
  virtual unsigned getEsoIndex(const std::vector<std::string> &esoNames,
                               const std::string &name);

  virtual std::vector<double> getUserGrid(const std::vector<UserInput::ConstraintInput> &constraints) const;
  virtual std::vector<double> getCompleteGrid(const FactoryInput::IntegratorMetaDataInput &input) const;

  //conversion functions
  virtual FactoryInput::EsoInput::EsoType convertEsoType(const UserInput::EsoInput::EsoType type) const;
  virtual struct FactoryInput::EsoInput convertEsoInput(const UserInput::EsoInput &input);

  virtual ControlType convertApproximationType(const UserInput::ParameterGridInput::ApproximationType type) const;
  virtual struct FactoryInput::ParameterizationGridInput convertParamGridInput(const UserInput::ParameterGridInput &input,
                                                                               const double lowerBound,
                                                                               const double upperBound);

  virtual FactoryInput::ParameterInput::ParameterSensitivityType convertParamSensType
                  (const UserInput::ParameterInput::ParameterSensitivityType type) const;
  virtual FactoryInput::ParameterInput::ParameterType convertParamType(const UserInput::ParameterInput::ParameterType type) const;
  virtual struct FactoryInput::ParameterInput convertParameterInputForDuration(const UserInput::ParameterInput &input);
  virtual struct FactoryInput::ParameterInput convertParameterInput(const UserInput::ParameterInput &inputo);

  virtual FactoryInput::MappingInput::MappingType convertMappingType(const UserInput::Input::RunningMode &mappingType) const;
  virtual FactoryInput::MappingInput convertMappingInput(const UserInput::Input::RunningMode &mode,
                                   const UserInput::StageMapping mappingInput,
                                   const FactoryInput::IntegratorMetaDataInput &imdInputStage1,
                                   const FactoryInput::IntegratorMetaDataInput &imdInputStage2);
  virtual std::vector<FactoryInput::MappingInput> createMappingInputVector
                                  (const UserInput::Input::RunningMode &mode,
                                   const std::vector<FactoryInput::IntegratorMetaDataInput> &convertedStages,
                                   const std::vector<UserInput::StageInput> &stages);

  virtual FactoryInput::IntegratorInput::IntegrationOrder convertIntegrationOrder
                  (const UserInput::IntegratorInput::IntegrationOrder order) const;
  virtual FactoryInput::IntegratorInput::IntegratorType convertIntegratorType
                  (const UserInput::IntegratorInput::IntegratorType type) const;
  virtual struct FactoryInput::IntegratorMetaDataInput createIntegratorMetaDataInputStage
                               (const UserInput::IntegratorStageInput &input,
								const GenericEso::Ptr esoPtr);
  virtual struct FactoryInput::IntegratorMetaDataInput createIntegratorMetaDataInput
                                (const UserInput::Input::RunningMode &mode,
								const UserInput::IntegratorInput::IntegrationOrder order,
                                const std::vector<UserInput::StageInput> &stageInputVector);
  virtual FactoryInput::LinearSolverInput::SolverType convertSolverType(
                                const UserInput::LinearSolverInput::SolverType &type) const;
  virtual FactoryInput::LinearSolverInput convertLinearSolverInput(
                                const UserInput::LinearSolverInput &input) const;
  virtual FactoryInput::NonLinearSolverInput::SolverType convertSolverType(
                                const UserInput::NonLinearSolverInput::SolverType &type) const;
  virtual FactoryInput::NonLinearSolverInput convertNonLinearSolverInput(
                                const UserInput::NonLinearSolverInput &input) const;
  virtual FactoryInput::DaeInitializationInput::DaeInitializationType 
              convertDaeInitializationType(
                const UserInput::DaeInitializationInput::DaeInitializationType &type) const;
  virtual FactoryInput::DaeInitializationInput convertDaeInitializationInput(
                                const UserInput::DaeInitializationInput &input) const;
  virtual struct FactoryInput::IntegratorInput createIntegratorInput
                             (const UserInput::Input::RunningMode &mode,
                              const UserInput::IntegratorInput &input,
                              const std::vector<UserInput::StageInput> &stageInputVector);


  virtual FactoryInput::ConstraintInput::ConstraintType convertConstraintType(
                                       const UserInput::ConstraintInput::ConstraintType type) const;
  virtual std::vector<FactoryInput::ConstraintInput> createPathConstraint(
                                                    const FactoryInput::ConstraintInput constraint,
                                                    const std::vector<double> &completeGrid) const;
  virtual struct FactoryInput::ConstraintInput convertConstraintInput(const UserInput::ConstraintInput &input,
                                                const FactoryInput::IntegratorMetaDataInput &imdInput,
                                                const GenericEso::Ptr &eso);
  virtual std::vector<FactoryInput::ConstraintInput> convertConstraintInputVector(
                               const std::vector<UserInput::ConstraintInput> &inputVector,
                               const FactoryInput::IntegratorMetaDataInput &imdInput,
                               const GenericEso::Ptr &eso);
  virtual void removeDoubleConstraints(std::vector<FactoryInput::ConstraintInput> &constraints) const;

  virtual FactoryInput::OptimizerMetaDataInput createOptimizerMetaDataInputStage
                                                   (const UserInput::OptimizerStageInput &input,
                                                    FactoryInput::IntegratorMetaDataInput &imdInput,
                                                    const UserInput::OptimizerStageInput &lastInput,
                                                    const bool treatObjective);
  virtual FactoryInput::OptimizerMetaDataInput createOptimizerMetaDataInput(const UserInput::OptimizerInput &input,
                                                      FactoryInput::IntegratorMetaDataInput &imdInput,
                                                      const std::vector<UserInput::StageInput> &stageInputVector);

  virtual FactoryInput::OptimizerInput::OptimizerType convertOptimizerType(const UserInput::OptimizerInput::OptimizerType &type) const;
  virtual FactoryInput::OptimizerInput createOptimizerInput(const UserInput::OptimizerInput &optimizerInput,
                                      FactoryInput::IntegratorInput &integratorInput,
                                      const std::vector<UserInput::StageInput> &stageInputVector);

  virtual FactoryInput::AdaptationInput convertAdaptationInput(
                                            const UserInput::AdaptationInput &adaptInput,
                                            const FactoryInput::ParameterizationGridInput gridInput,
                                            const double lowerBound,
                                            const double upperBound) const;
  virtual FactoryInput::OptionsWaveletRefinemet convertWaveletInput(
                                            const UserInput::WaveletAdaptationInput waveInput,
                                            const double lowerBound,
                                            const double upperBound) const;
  virtual FactoryInput::AdaptationOptions::AdaptiveStrategy convertAdaptiveStrategy
    (const UserInput::AdaptationOptions::AdaptiveStrategy input) const;
  virtual ProblemInput convertInput(const UserInput::Input &input);
  
  virtual void addShootingConstraints(UserInput::Input &input,
                                      std::vector<std::string> &diffVarNames);
  virtual void addInitialValueParameters(FactoryInput::IntegratorMetaDataInput &input,
                                         std::vector<std::string> &diffVarNames);
  
  virtual std::vector<std::string> getDifferentialVariableNames
                                    (const FactoryInput::IntegratorMetaDataInput &input);
};//class InputConverter
