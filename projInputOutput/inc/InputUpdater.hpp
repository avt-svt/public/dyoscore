/**
* @file InputUpdater.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the declaration of the InputUpdater class         \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 19.09.2012
* @sa Input.hpp EsoInput.hpp MetaDataInput.hpp IntegratorInput.hpp OptimizerInput.hpp
*     MetaDataOutput.hpp DyosOutput.hpp
*/
#pragma once
#include "boost/shared_ptr.hpp"
#include "Input.hpp"
#include "DyosOutput.hpp"
#include "MetaDataInput.hpp"
#include "MetaDataOutput.hpp"
#include "DyosObject.hpp"

/**
* @class InputUpdater
* @brief class providing functions to update input structs after an optimization
*
* In an adaptive dynamic optimization the output of an optimization is the
* input of the next optimization (with adapted grids).
* So all output must somehow be written into the corresponding input structs.
*/
class InputUpdater : public DyosObject
{
public:
  typedef boost::shared_ptr<InputUpdater> Ptr;
protected:
  map<double, vector<unsigned> > getDurationToIndex(
                                  const FactoryInput::IntegratorMetaDataInput &originalInput) const;
public:
  InputUpdater();
  virtual ~InputUpdater();
  
  std::vector<FactoryInput::ParameterizationGridInput> updateParameterizationGridInput(
                            const std::vector<FactoryInput::ParameterizationGridInput> &originalInput,
                            const std::vector<DyosOutput::ParameterGridOutput> &optimizationResult,
                            const std::map<double, vector<unsigned> > durationToIndex,
                            const unsigned esoIndex,
                            const bool keepRetardedParameters) const;
  FactoryInput::ParameterInput updateParameterInput(
                            const FactoryInput::ParameterInput &originalInput,
                            const DyosOutput::ParameterOutput &optimizationResult,
                            const std::map<double, vector<unsigned> > durationToIndex) const;
  FactoryInput::IntegratorMetaDataInput updateIntegratorMetaDataInputStage(
                            const FactoryInput::IntegratorMetaDataInput &originalInput,
                            const DyosOutput::IntegratorStageOutput &optimizationResult) const;
  FactoryInput::OptimizerMetaDataInput updateOptimizerMetaDataInputStage(
                            const FactoryInput::OptimizerMetaDataInput &originalInput,
                            const DyosOutput::OptimizerStageOutput &optimizationResult) const;
  FactoryInput::IntegratorMetaDataInput updateIntegratorMetaDataInput(
                            const FactoryInput::IntegratorMetaDataInput &originalInput,
                            const  std::vector<DyosOutput::StageOutput> &optimizationResult) const;
  FactoryInput::OptimizerMetaDataInput updateOptimizerMetaDataInput(
                            const FactoryInput::OptimizerMetaDataInput &originalInput,
                            const std::vector<DyosOutput::StageOutput> &optimizationResult) const;
  ProblemInput updateProblemInput(const ProblemInput &originalInput,
                                  const DyosOutput::Output &optimizationResult) const;
  ProblemInput updateMultipleShootingInitials(const ProblemInput &originalInput,
                                              const DyosOutput::Output &simulationResult) const;

  std::vector<double> getAbsoluteDurations(const vector<DyosOutput::ParameterGridOutput> 
                                                       &optimizationResult)const;
  void removePossibleRetardedParameters(const DyosOutput::ParameterGridOutput &optimizationResult,
    bool &isFirst,
    std::vector<double> &timePoints,
    std::vector<double> &values)const;

};