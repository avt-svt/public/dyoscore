/**
* @file InputOutputConversions.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the declaration of functions to convert           \n
input structures to output structures and vice versa.                  \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 26.06.2012
* @sa UserOutput.hpp Input.hpp EsoInput.hpp MetaDataInput.hpp IntegratorInput.hpp OptimizerInput.hpp
*/

#pragma once

#include "UserInput.hpp"
#include "UserOutput.hpp"
#include "GenericEso.hpp"
#include "MetaDataExtern.hpp"
#include "MetaDataOutput.hpp"
#include "ParameterizationGrid.hpp"
#include "Control.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include "OptimizerOutput.hpp"
#include "DyosObject.hpp"



class IOConversions : public DyosObject
{
protected:
  DyosOutput::ParameterOutput findMatchingInitial(const std::vector<DyosOutput::ParameterOutput> &initialOutputs,
                                                  const UserInput::ParameterInput initialInput) const;
public:
  //functions to convert input structures to comparable output structures
  UserOutput::EsoOutput i2oConvertEso(const UserInput::EsoInput &esoInput) const;
  UserOutput::Output::RunningMode i2oConvertRunningMode(const UserInput::Input &input) const;
  UserOutput::ParameterGridOutput::ApproximationType i2oConvertApproximationType
                                              (const UserInput::ParameterGridInput &input) const;
  UserOutput::Output i2oConvertInputToOutput(const UserInput::Input &input) const;
  UserOutput::MappingOutput i2oConvertMapping(const UserInput::StageMapping &input) const;
  void  i2oParameterInput(const UserInput::ParameterInput &input,
                                UserOutput::ParameterOutput &output) const;
  UserOutput::StageOutput  i2oConvertStageVector(const UserInput::StageInput &input) const;
  UserOutput::IntegratorOutput i2oConvertIntegrator(const UserInput::IntegratorInput &input) const;
  UserOutput::ConstraintOutput i2oConvertConstraint(
                                    const UserInput::ConstraintInput &input,
                                          std::vector<DyosOutput::ConstraintOutput> &mdOptVect) const;
  UserOutput::FirstSensitivityOutput i2oConvertFirstSensitivity(
                                          const DyosOutput::FirstSensitivityOutput &fSO) const;
  UserOutput::ParameterGridOutput  i2oConvertParameterGrid(
                                          const UserInput::ParameterGridInput &input,
                                          const DyosOutput::ParameterGridOutput &mdOpt) const;
  UserOutput::ParameterOutput  i2oConvertParameter(const UserInput::ParameterInput &input,
                                                   const DyosOutput::ParameterOutput &mdOpt) const;
  UserOutput::IntegratorStageOutput  i2oConvertIntegratorStage
                                              (const UserInput::IntegratorStageInput &input,
                                               const DyosOutput::IntegratorStageOutput &mdOpt) const;
  UserOutput::StateGridOutput  i2oConvertStateGrid(const DyosOutput::StateGridOutput &mdOpt) const;
  UserOutput::StateOutput  i2oConvertState(const DyosOutput::StateOutput &mdOpt) const;
  UserOutput::OptimizerStageOutput  i2oConvertOptimizerStage(
                                            const UserInput::OptimizerStageInput &input,
                                            const DyosOutput::OptimizerStageOutput &mdOpt) const;
  UserOutput::StageOutput  i2oConvertStage(const UserInput::StageInput &input,
                                           const DyosOutput::StageOutput &mdOpt) const;
  UserOutput::OptimizerOutput  i2oConvertOptimizer(
                                           const UserInput::OptimizerInput &input,
                                           const DyosOutput::Output &mdOpt) const;
  UserOutput::Output i2oConvertOutput(const UserInput::Input &input,
                                      const DyosOutput::Output &mdOpt) const;

  UserOutput::SecondOrderOutput i2oConvert2ndOrderOuput
                                     (const DyosOutput::SecondOrderOutput &secondOrderOut) const;

  //functions to convert UserOutput structures to comparable UserInput structures
  UserInput::Input o2iConvertOutputToInput(const UserOutput::Output &input) const;
 
  UserInput::IntegratorInput o2iConvertIntegratorInput(const UserOutput::Output & input) const;
  
  UserInput::OptimizerInput o2iConvertOptimizerInput(const UserOutput::Output & input) const;
  
  UserInput::Input::RunningMode o2iConvertRunningMode(const UserOutput::Output & input) const;
  
  UserInput::StageInput o2iConvertStage(const UserOutput::StageOutput &input) const; 
  
  UserInput::EsoInput o2iConvertEso(const UserOutput::EsoOutput &esoOutput) const;
  
  UserInput::StageMapping o2iConvertStageMapping(const UserOutput::StageOutput & input) const;
  
  UserInput::OptimizerStageInput o2iConvertOptimizer(const UserOutput::OptimizerStageOutput & input) const;
  
  UserInput::ConstraintInput::ConstraintType o2iConvertConstraintType(const UserOutput::ConstraintOutput::ConstraintType & input) const;
  
  UserInput::ConstraintInput o2iConvertConstraint(const UserOutput::ConstraintOutput & input) const;
  
  UserInput::ParameterInput o2iConvertParameter(const UserOutput::ParameterOutput & input) const;
  
  std::vector<UserInput::ParameterGridInput> o2iConvertParameterGridInput(const std::vector<UserOutput::ParameterGridOutput>& input) const;
  
  UserInput::ParameterGridInput::ApproximationType o2iConvertApproximationType(const UserOutput::ParameterGridOutput &input) const;
  
  UserInput::ParameterInput::ParameterSensitivityType o2iConvertParameterSensType(const UserOutput::ParameterOutput::ParameterSensitivityType & input) const;
  
  UserInput::ParameterInput::ParameterType o2iConvertParameterType(const UserOutput::ParameterOutput::ParameterType & input) const;
  


  // functions to convert DyosOutput to UserOutput
  UserOutput::ParameterGridOutput::ApproximationType o2oConvertApproximationType
                                              (const DyosOutput::ParameterGridOutput &input) const;

  
  
  //functions to extract data
  std::vector <UserOutput::StateOutput>  createStateOutputVector(const GenericEso::Ptr &genEso) const;
  UserOutput::ParameterOutput  createControlOutput(Control control,
                                                   UserInput::ParameterInput input,
                                                   GenericEso::Ptr &genPtr,
                                                   IntOptDataMap iODP) const;

  std::vector <UserOutput::FirstSensitivityOutput>  createSensitivityOutputVector(IntOptDataMap iODP,
                                                                                  GenericEso::Ptr &genPtr,
                                                                                  std::vector <unsigned> parameterIndices) const;
  UserOutput::ParameterGridOutput  createParameterGridOutput(IntOptDataMap iODP,
                                                              GenericEso::Ptr &genEso,
                                                              ParameterizationGrid &grid,
                                                              UserInput::ParameterGridInput &paramGridIn) const;
  UserOutput::ParameterOutput  createParameterOutput(UserInput::ParameterInput input,
                                                     GenericEso::Ptr eso,
                                                     IntOptDataMap iodm,
                                                     Parameter::Ptr p) const;
  UserOutput::ConstraintOutput  createConstraintOutput(OptimizerMetaData &omd,
                                                       DyosOutput::OptimizerOutput oo,
                                                       UserInput::ConstraintInput input) const;
  UserOutput::ConstraintOutput  createObjectiveOutput(OptimizerMetaData &omd,
                                                      UserInput::ConstraintInput input) const;
  std::vector<UserOutput::ConstraintOutput>  createLinearConstraintVector(OptimizerMetaData &omd,
                                                                          DyosOutput::OptimizerOutput oo) const;
  UserOutput::OptimizerOutput  createOptimizerOutput(UserInput::OptimizerInput input,
                                                     DyosOutput::OptimizerOutput oo,
                                                     OptimizerMetaData &omd) const;
  UserOutput::Output  createSimulationOutput(UserInput::Input input,IntegratorMetaData &imd) const;
  UserOutput::Output  createOptimizationOutput(UserInput::Input input,
                                               OptimizerMetaData &omd,
                                               DyosOutput::OptimizerOutput oo) const;

  //utility functions
  static bool compareConstraints(const DyosOutput::ConstraintOutput &mdOpt1, const DyosOutput::ConstraintOutput &mdOpt2);
  void sortConstraints(std::vector<DyosOutput::ConstraintOutput> &mdOptVect) const;
protected:

  //function to calculate state name
  std::string  getStateName(EsoIndex iStateIndex,const GenericEso::Ptr &genEso) const;
  //function to convert input parameter sensitivity type to comparable output
  void i2oConvertParameterSensitivityType(const UserInput::ParameterInput &input,
                                                UserOutput::ParameterOutput &output) const;
  //function to convert input parameter type to comparable output
  void i2oConvertParameterType(const UserInput::ParameterInput &input,
                                     UserOutput::ParameterOutput &output) const;
  //function to convert input optimization type to comparable output
  void i2oConvertOptimizationType(const UserInput::OptimizerInput &input,
                                        UserOutput::OptimizerOutput &output) const;
  //function to convert optimizer output flag type to output resultFlag
  void i2oConvertResultFlag(const DyosOutput::OptimizerOutput &oo,
                                  UserOutput::OptimizerOutput &output) const;
  //function to convert input constraint structure type to comparable output type
  void i2oConvertConstraintType(const UserInput::ConstraintInput &input,
                                      UserOutput::ConstraintOutput &output) const;
  //function to create constraint grid output from meta data constraint output vector
  void createConstraintGridOutputFromMDOotputVect(
                                   const std::vector<DyosOutput::ConstraintOutput> &mdOptVect,
                                         UserOutput::ConstraintGridOutput &output) const;
  
  //function to make sure, that all parameters are on all stage inputs
  UserInput::Input propagateParameters(const UserInput::Input &input)const;
}; //class IOConversions
