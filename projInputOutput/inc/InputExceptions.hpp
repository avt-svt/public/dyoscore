/**
* @file InputExceptions.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Input                                                                \n
* =====================================================================\n
* Definitions of all exceptions that are thrown by the input module    \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 02.03.2012
*/

#pragma once
#include <boost/lexical_cast.hpp>
#include <exception>
#include <string>
#include <iostream>

/**
* @class InputException
* @brief exception class for any exception of the Input module
*/
class InputException : public std::exception
{
protected:
  const std::string m_errorMessage;
public:
  /// @brief standard constructor setting a standard error message
  InputException()
    : m_errorMessage("An error occurred during user input conversion.")
  {

  }
  
  /**
  * @brief constructor
  * @param[in] message string containing a specific error message
  */ 
  InputException(const std::string &message)
    : m_errorMessage(message)
  {
  }

  /// @brief destructor
  virtual ~InputException() throw() {}

  /**
  * @brief get error message
  * @return error message of the exception
  */
  const char *what() const throw()
  {
    return m_errorMessage.c_str();
  }
};

class MissingModelNameException : public InputException
{
public:
  MissingModelNameException()
    : InputException("A model name has not been provided for an EsoInput struct.") {}
};



/**
* @class VariableNotFoundException
* @brief exception class thrown when a given variable name could not be found in the ESO
*/
class VariableNotFoundException : public InputException
{
public:
  VariableNotFoundException(std::string name)
    : InputException("The variable name '"+name+"' could not be found in the ESO.")
  {
  }
};

/**
* @class MissingVariableNameException
* @brief exception class thrown when a variable name was expected but none was given by the user
*/
class MissingVariableNameException : public InputException
{
public:
  MissingVariableNameException()
    : InputException("A variable name was expected, but none was given.")
  {
  }
  MissingVariableNameException(std::string st)
    : InputException("Name expected for variable "+st+".")
  {
  }
};

/**
* @class NoGridDefinedException
* @brief exception class thrown when the user has neither given time points or a number of grids
*        in the UserInput::ParameterGridInput struct
*/
class NoGridDefinedException : public InputException
{
public:
  NoGridDefinedException()
    : InputException("Neither time points or number of grids given in ParameterGridInput.")
  {

  }
};

/**
* @class NoGridsDefinedOnControlException
* @brief exception class thrown when the user has given no parameterization grid input
*         for a control in the UserInput::ParameterInput struct
*/
class NoGridsDefinedOnControlException : public InputException
{
public:
  NoGridsDefinedOnControlException()
    : InputException("No parameterization grid defined for a profile parameter.")
  {

  }
};

/**
* @class WrongNumberOfParameterValuesDefinedException
* @brief exception class thrown when number of values does not match the expected number of parameters
*/
class WrongNumberOfParameterValuesDefinedException : public InputException
{
public:
  WrongNumberOfParameterValuesDefinedException(std::string type, unsigned size)
    : InputException("For ParameterGridInput::ApproximationType: "+type+
                     ": values.size() == "+boost::lexical_cast<std::string>(size)+
                     "does not match convertedInput.timePoints.size().")
  {
  }
};

/**
* @class DefinedConstraintForParameterException
* @brief exception class thrown when the user has specified a constraint on a variable that has
*        already been declared as an optimization parameter
*/
class DefinedConstraintForParameterException : public InputException
{
public:
  DefinedConstraintForParameterException(unsigned constraintIndex)
    : InputException("At index: "+boost::lexical_cast<std::string>(constraintIndex)+
                     ": Constraint defined that is already an optimization variable.")
  {
  }
  DefinedConstraintForParameterException();
};

class DefinedParameterTwiceException : public InputException
{
public:
  DefinedParameterTwiceException(std::string &name) 
    : InputException("The parameter " + name + " has been defined twice on the same stage")
  {}
};

class DefinedConstraintForObjectiveException : public InputException
{
public:
  DefinedConstraintForObjectiveException()
    : InputException("Constraint defined that is already an objective")
  {}
};

/**
* @class AmbiguousEquationForConstraintException
* @brief exception class thrown when a constraint is set for a variable that cannot be assigned
*        unambiguously to a single equation
*/
class AmbiguousEquationForConstraintException : public InputException
{
public:
  AmbiguousEquationForConstraintException()
    : InputException("Constraint defined for which more than one equation could be determined.")
  {
  }
};

/**
* @class NoConstraintEquationFoundException
* @brief exception class thrown when a constraint is set for a variable that cannot be assigned to
*        any equation
* @todo check if this exception can be provoked by user input
*       - otherwise replace exception by an assert
*/
class NoConstraintEquationFoundException : public InputException
{
public:
  NoConstraintEquationFoundException(int res)
    : InputException("At res = "+boost::lexical_cast<std::string>(res)+
                     " : Constraint defined for which no equation can be determined.")
  {
  }
};

/**
* @class IncompatibleStagesForFullStateMappingException
* @brief exception class thrown when a full state mapping option is active,
*        but the stages do not match
*/
class IncompatibleStagesForFullStateMappingException : public InputException
{
public:
  IncompatibleStagesForFullStateMappingException(std::string st, unsigned numStates1, unsigned numStates2)
    : InputException("Full state mapping chosen, but ESOs to be mapped are not compatible:"+
                     st+" do not match ("+boost::lexical_cast<std::string>(numStates1)+" != "+
                     boost::lexical_cast<std::string>(numStates2)+")")
  {
  }
};

/**
* @class LastStageHasMappingDefinedException
* @brief exception class thrown when a mapping is defined of the last stage
*/
class LastStageHasMappingDefinedException : public InputException
{
public:
  LastStageHasMappingDefinedException()
    : InputException("Mapping data defined for last stage.")
  {
  }
};

/**
* @class TypeOfDurationParameterNotSetToDURATIONExeption
* @brief exception class thrown when DURATION has not been set for duration parameter
*/
class TypeOfDurationParameterNotSetToDURATIONExeption : public InputException
{
public:
  TypeOfDurationParameterNotSetToDURATIONExeption()
    : InputException("Type of UserInput::ParameterInput is not set to DURATION")
  {
  }
};

/**
* @class ZerothOrderForOptimizationUsedException
* @brief exception class thrown when ZEROTH is set as integration order for an optimization
*/
class ZerothOrderForOptimizationUsedException : public InputException
{
public:
  ZerothOrderForOptimizationUsedException()
    : InputException("Integration order set to ZEROTH for an optimization - this is only"
                     "allowed if UserInput::Input::RunningMode is set to SIMULATION.")
  {
  }
};

/**
* @class WrongGridStructureException
* @brief exception class thrown when grid point vector values do not make sense.
*/
class WrongGridStructureException : public InputException
{
public:
  WrongGridStructureException()
    : InputException("A user given grid had not the correct structure. The gridpoints "
                     "should include 0.0 and 1.0 and should be in ascending order.")
  {
  }
  WrongGridStructureException(std::string st)
    : InputException("Wrong grid structure: "+st)
  {
  }
};
/**
* @class EnumException
* @brief exception class thrown when user given Integer does not represent an enum.
*/
class EnumException : public InputException
{
public:
  EnumException(std::string st1, std::string st2)
    : InputException(st1+" is no member of enum "+st2)
  {
  }
};

class WrongNumberOfLagrangeMultipliersException : public InputException
{
public:
  WrongNumberOfLagrangeMultipliersException()
    : InputException("Number of Lagrange multipliers for a path constraint does not match "
                     "the number of total grid points of the stage.")
  {}
};

class LagrangeMultiplierVectorForNonPathConstraintDefinedException : public InputException
{
public:
  LagrangeMultiplierVectorForNonPathConstraintDefinedException()
   : InputException("Error: Lagrange multiplier vector has been defined"
                     " for a constraint that is no path constraint.")
  {}
};

class FalseOptionNameException : public InputException
{
public:
  FalseOptionNameException(std::string optionName, std::string optimizerName)
    : InputException("An option named "+optionName+" does not exist for "+optimizerName)
  {
  }
};

class FalseOptionInputException : public InputException
{
public:
  FalseOptionInputException(std::string input, std::string optionName)
    : InputException(input+" is no valid input for option "+optionName)
  {
  }
};

class NoObjectiveDefinedException : public InputException
{
public:
  NoObjectiveDefinedException()
    : InputException("No objecivte defined for optimization problem."){}
  NoObjectiveDefinedException(std::string message)
    : InputException(message)
  {
  }
};

class NoConstraintDefinedException : public InputException
{
public:
  NoConstraintDefinedException()
    : InputException("No constraint defined for optimization problem. "
                     "Some optimizers need at least 1 defined constraint to run. "
                     "If the problem is unconstrained, define an unbounded dummy constraint to optimize."){}
};

class SetZeroForPlotGridResolutionException : public InputException
{
public:
  SetZeroForPlotGridResolutionException()
    : InputException("Plot grid resolution must not be set to 0 (trivial plot grid is 1 and only includes"
                     " time points 0.0 and 1.0 which are always set for plot)."){}
};

class EndpointConstraintDirectlyDependsOnControlException : public InputException
{
public:
  EndpointConstraintDirectlyDependsOnControlException()
    : InputException("The objective or an endpoint constraint is algebraic and directly depends on a control.\n"
                     " That way the optimization problem does not make much sense."){}
};

class TimePointNotAllowedException : public InputException
{
public:
  TimePointNotAllowedException()
    : InputException("Time point for constraint must be between 0.0 and 1.0 (only scaled timepoints allowed)"){}
};

class ExplicitPlotGridNotInBoundsException : public InputException
{
public:
  ExplicitPlotGridNotInBoundsException()
    : InputException("All values of explicitPlotGrid must be within [0.0, 1.0]. At least one value was out of bounds."){}
};

class ExplicitPlotGridNotOrderedException : public InputException
{
public:
  ExplicitPlotGridNotOrderedException()
    : InputException("The values of explicitPlotGrid must be in ascending order."){}
};