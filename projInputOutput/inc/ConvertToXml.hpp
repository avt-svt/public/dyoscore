/**
* @file ConvertToXml.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertToXml                                                         \n
* =====================================================================\n
* This file contains the declaration of the function convertToXml which\n
* converts the UserInput::Input into an xml file                       \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski
* @date 28.11.2012
*/

#pragma once
#include "UserInput.hpp"

/**
* @def LINKDLL
* @brief defines whether a function is to be imported or exported

#if WIN32
#ifdef MAKE_DYOS_DLL
#define LINKDLL __declspec(dllexport)
#else
#define LINKDLL __declspec(dllimport)
#endif
#else
#define LINKDLL
#endi
*/

void convertToXml(UserInput::Input input);
