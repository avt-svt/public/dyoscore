/**
* @file InputOutputConversions.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the functions to convert input structures to      \n
* output structures and vice versa.                                    \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi, Johannes M.M. Faust, Adrian Caspari
* @date 22.06.2012, 26.03.2018,18.10.2018
* @sa UserOutput.hpp Input.hpp EsoInput.hpp MetaDataInput.hpp IntegratorInput.hpp OptimizerInput.hpp
*/

#define MAKE_DYOS_DLL
#include "InputOutputConversions.hpp"
#include <boost/lexical_cast.hpp>


  /**
  * @brief implementations of functions of class IOConversions
  */


/**
* @brief converts Input genericEso structure to comparable Output structure
*
* @param[in] pointer to the eso input structure
* @return pointer to the constructed eso output structure
*/
UserOutput::EsoOutput IOConversions::i2oConvertEso(const UserInput::EsoInput &esoInput) const
{
  UserOutput::EsoOutput output;
  switch(esoInput.type)
  {
    case UserInput::EsoInput::JADE:
      output.type = UserOutput::EsoOutput::JADE;
      break;
  #ifdef BUILD_WITH_FMU
	case UserInput::EsoInput::FMI:
      output.type = UserOutput::EsoOutput::FMI;
	  output.relativeFmuTolerance = esoInput.relativeFmuTolerance;
      break;
  #endif
    default:
      assert(false);
  }
  output.model = esoInput.model;
  return output;
}




/**
* @brief converts Input running mode to comparable Output
*
* @param[in] pointer to the input structure
* @return the output running mode
*/
UserOutput::Output::RunningMode IOConversions::i2oConvertRunningMode(const UserInput::Input &input) const
  {
    UserOutput::Output output;
    switch(input.runningMode)
    {
      case UserInput::Input::SIMULATION:
        output.runningMode = UserOutput::Output::SIMULATION;
        break;
      case UserInput::Input::SINGLE_SHOOTING:
        output.runningMode = UserOutput::Output::SINGLE_SHOOTING;
        break;
      case UserInput::Input::MULTIPLE_SHOOTING:
        output.runningMode = UserOutput::Output::MULTIPLE_SHOOTING;
        break;
      case UserInput::Input::SENSITIVITY_INTEGRATION:
        output.runningMode = UserOutput::Output::SENSITIVITY_INTEGRATION;
        break;
      default:
        assert(false);
    }
    return output.runningMode;
  }

/**
* @brief converts Input approximation type  to comparable Output
*
* @param[in] pointer to the input parameter grid structure
* @return the output approximation type
*/
UserOutput::ParameterGridOutput::ApproximationType  IOConversions::i2oConvertApproximationType(
                                                     const UserInput::ParameterGridInput &input) const
  {
    UserOutput::ParameterGridOutput output;
    switch(input.type)
    {
      case UserInput::ParameterGridInput::PIECEWISE_CONSTANT:
        output.type = UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT;
        break;
      case UserInput::ParameterGridInput::PIECEWISE_LINEAR:
        output.type = UserOutput::ParameterGridOutput::PIECEWISE_LINEAR;
        break;
      default:
        ;
    }
    return output.type;
  }


/**
* @brief converts entire Input structure to the Output structure
*
* @param[in] pointer to the input structure
* @return pointer to the output structure
*/
UserOutput::Output  IOConversions::i2oConvertInputToOutput(const UserInput::Input &input) const
{
  UserOutput::Output output;
  output.runningMode = i2oConvertRunningMode(input);
  output.stages.resize(input.stages.size());
  for(unsigned i=0; i<output.stages.size(); i++)
  {
    output.stages[i].eso = i2oConvertEso(input.stages[i].eso);
    output.stages[i].treatObjective = input.stages[i].treatObjective;
    output.stages[i].integrator.durations.resize(1);
    output.stages[i].integrator.durations.front().name = input.stages[i].integrator.duration.name;
    output.stages[i].integrator.durations.front().grids.resize(input.stages[i].integrator.duration.grids.size());
    for(unsigned j=0; j<output.stages[i].integrator.durations.front().grids.size(); j++)
    {
      output.stages[i].integrator.durations.front().grids[j].duration = input.stages[i].integrator.duration.grids[j].duration;
      output.stages[i].integrator.durations.front().grids[j].hasFreeDuration = input.stages[i].integrator.duration.grids[j].hasFreeDuration;
      output.stages[i].integrator.durations.front().grids[j].numIntervals = input.stages[i].integrator.duration.grids[j].numIntervals;
      output.stages[i].integrator.durations.front().grids[j].type = i2oConvertApproximationType(input.stages[i].integrator.duration.grids[j]);
      output.stages[i].integrator.durations.front().grids[j].values.resize(input.stages[i].integrator.duration.grids[j].values.size());
      output.stages[i].integrator.durations.front().grids[j].values.assign(input.stages[i].integrator.duration.grids[j].values.begin(),
                                                                           input.stages[i].integrator.duration.grids[j].values.end());
    }
  }
  return output;
}// function i2oConvertInputToOutput

/**
* @brief converts Mapping Input structure to the Mapping Output structure
*
* @param[in] pointer to the MappingInput structure
* @return pointer to the MappingOutput structure
*/
UserOutput::MappingOutput  IOConversions::i2oConvertMapping(const UserInput::StageMapping &input) const
{
  UserOutput::MappingOutput output;
  output.fullStateMapping = input.fullStateMapping;
  output.stateNameMapping = input.stateNameMapping;
  return output;
}

/**
* @brief converts Parameter Input structure to the Parameter Output structure
*
* @param[in] pointer to the ParameterInput structure
* @param[in] pointer to the ParameterOutput structure
*/
void  IOConversions::i2oParameterInput(const UserInput::ParameterInput &input,
                                             UserOutput::ParameterOutput &output) const
{
  output.name = input.name;
  output.value = input.value;
  output.lowerBound = input.lowerBound;
  output.upperBound = input.upperBound;
  i2oConvertParameterSensitivityType(input, output);
  i2oConvertParameterType(input, output);
}

/**
* @brief converts Parameter Input sensitivity type to comparable output type
*
* @param[in] pointer to the ParameterInput structure
* @return pointer to the ParameterOutput structure
*/
void IOConversions::i2oConvertParameterSensitivityType(const UserInput::ParameterInput &input,
                                                             UserOutput::ParameterOutput &output) const
{
  switch(input.sensType)
  {
  case UserInput::ParameterInput::FULL:
    output.sensType = UserOutput::ParameterOutput::FULL;
    break;
  case UserInput::ParameterInput::FRACTIONAL:
    output.sensType = UserOutput::ParameterOutput::FRACTIONAL;
    break;
  case UserInput::ParameterInput::NO:
    output.sensType = UserOutput::ParameterOutput::NO;
    break;
  }
}

/**
* @brief converts Parameter Input parametr type to comparable output type
*
* @param[in] pointer to the ParameterInput structure
* @return pointer to the ParameterOutput structure
*/
void IOConversions::i2oConvertParameterType(const UserInput::ParameterInput &input,
                                                  UserOutput::ParameterOutput &output) const
{
  switch(input.paramType)
  {
  case UserInput::ParameterInput::INITIAL:
    output.paramType = UserOutput::ParameterOutput::INITIAL;
    break;
  case UserInput::ParameterInput::TIME_INVARIANT:
    output.paramType = UserOutput::ParameterOutput::TIME_INVARIANT;
    break;
  case UserInput::ParameterInput::PROFILE:
    output.paramType = UserOutput::ParameterOutput::PROFILE;
    break;
  case UserInput::ParameterInput::DURATION:
    output.paramType = UserOutput::ParameterOutput::DURATION;
    break;
  }
}

/**
* @brief converts input stage vector to comparable output
*
* @param[in] pointer to the stage input structure
* @return pointer to the constructed stage output structure
*/
UserOutput::StageOutput  IOConversions::i2oConvertStageVector(const UserInput::StageInput &input) const
{
  UserOutput::StageOutput output;
  output.eso = i2oConvertEso(input.eso);
  output.mapping = i2oConvertMapping(input.mapping);
  output.treatObjective = input.treatObjective;
  return output;
}

/**
* @brief converts IntegratorInput structure to comparable output
*
* @param[in] pointer to the IntegratorInput structure
* @return the constructed IntegratorOutput structure
*/
UserOutput::IntegratorOutput  IOConversions::i2oConvertIntegrator(
                                             const UserInput::IntegratorInput &input) const
{
  UserOutput::IntegratorOutput output;
  switch(input.order)
  {
  case UserInput::IntegratorInput::ZEROTH:
    output.order = UserOutput::IntegratorOutput::ZEROTH;
    break;
  case UserInput::IntegratorInput::FIRST_FORWARD:
    output.order = UserOutput::IntegratorOutput::FIRST_FORWARD;
    break;
  case UserInput::IntegratorInput::FIRST_REVERSE:
    output.order = UserOutput::IntegratorOutput::FIRST_REVERSE;
    break;
  case UserInput::IntegratorInput::SECOND_REVERSE:
    output.order = UserOutput::IntegratorOutput::SECOND_REVERSE;
    break;
  }
  switch(input.type)
  {
  case UserInput::IntegratorInput::LIMEX:
    output.type = UserOutput::IntegratorOutput::LIMEX;
    break;
  case UserInput::IntegratorInput::NIXE:
    output.type = UserOutput::IntegratorOutput::NIXE;
    break;
  case UserInput::IntegratorInput::IDAS:
    output.type = UserOutput::IntegratorOutput::IDAS;
    break;
  default:
    assert(false);
  }
  return output;
}

/**
* @brief compares two  meta data ConstraintOutput structures regarding their timepoints value
*
* @param[in] pointer to the ConstraintOutput structure
* @param[in] pointer to the ConstraintOutput structure
* @return the comparison result
*/
bool IOConversions::compareConstraints(const DyosOutput::ConstraintOutput& mdOpt1, const DyosOutput::ConstraintOutput& mdOpt2)
{
  return mdOpt1.timePoint < mdOpt2.timePoint;
}

/**
* @brief sorts a vector of meta data constraint outputs
*
* @param[in] pointer to the meta data ConstraintOutput structure
*/
void IOConversions::sortConstraints(std::vector<DyosOutput::ConstraintOutput> &mdOptVect) const
{
  std::sort(mdOptVect.begin(),
            mdOptVect.end(),
            IOConversions::compareConstraints);
}

/**
* @brief converts the constraint type from input to output
*
* @param[in] pointer to the ConstraintInput structure
* @param[in] pointer to the ConstraintOutput structure
*/
void IOConversions::i2oConvertConstraintType(const UserInput::ConstraintInput &input,
                                                   UserOutput::ConstraintOutput &output) const
{
  switch(input.type)
  {
  case UserInput::ConstraintInput::ENDPOINT:
    output.type = UserOutput::ConstraintOutput::ENDPOINT;
    break;
  case UserInput::ConstraintInput::PATH:
    output.type = UserOutput::ConstraintOutput::PATH;
    break;
  case UserInput::ConstraintInput::POINT:
    output.type = UserOutput::ConstraintOutput::POINT;
    break;
  default:
    assert(false);
  }
}

/**
* @brief creates constraint grid output structure from meta data constraint output vector
*
* @param[in] meta data constraint output vector
* @return the constructed ConstraintGridOutput structure
*/
void IOConversions::createConstraintGridOutputFromMDOotputVect(
                                            const std::vector<DyosOutput::ConstraintOutput> &mdOptVect,
                                                  UserOutput::ConstraintGridOutput &output) const
{
  unsigned numMDConstraintGrids = mdOptVect.size();
  output.timePoints.resize(numMDConstraintGrids);
  output.lagrangeMultiplier.resize(numMDConstraintGrids);
  output.values.resize(numMDConstraintGrids);
  for(unsigned i=0; i<numMDConstraintGrids; i++)
  {
    output.timePoints[i] = mdOptVect[i].timePoint;
    output.lagrangeMultiplier[i] = mdOptVect[i].lagrangeMultiplier;
    output.values[i] = mdOptVect[i].value;
  }
}

UserInput::Input IOConversions::propagateParameters(const UserInput::Input &input)const
{
  UserInput::Input updatedInput = input;
  for(unsigned i=0; i<input.stages.size(); i++){
    for(unsigned j=i+1; j<updatedInput.stages.size(); j++){
      for(unsigned k=0; k<input.stages[i].integrator.parameters.size(); k++){
        UserInput::ParameterInput currentParameter = input.stages[i].integrator.parameters[k];
        //parameter propagation is for sensitivity output - so ignore parameters without sensitivity information
        if(currentParameter.sensType == UserInput::ParameterInput::NO){
          continue;
        }
        bool foundParameter = false;
        for(unsigned l=0; l<updatedInput.stages[j].integrator.parameters.size(); l++){
          UserInput::ParameterInput comparedParameter = updatedInput.stages[j].integrator.parameters[l];
          if(currentParameter.name == comparedParameter.name){
            bool currentParIsInitial = currentParameter.paramType == UserInput::ParameterInput::INITIAL;
            bool comparedParIsInitial = comparedParameter.paramType == UserInput::ParameterInput::INITIAL;
            if((currentParIsInitial && comparedParIsInitial)||(!currentParIsInitial && !comparedParIsInitial)){
              foundParameter = true;
              break;
            }
          }
        }
        if(!foundParameter){
          updatedInput.stages[j].integrator.parameters.push_back(currentParameter);
        }
      }
    }
  }
  return updatedInput;
}

/**
* @brief converts ConstraintInput structure to comparable output
*
* @param[in] pointer to the ConstraintInput structure
* @return the constructed ConstraintOutput structure
*/
UserOutput::ConstraintOutput  IOConversions::i2oConvertConstraint(
                                             const UserInput::ConstraintInput &input,
                                                   std::vector<DyosOutput::ConstraintOutput> &mdOptVect) const
{
  IOConversions::sortConstraints(mdOptVect);
  UserOutput::ConstraintOutput output;
  output.lowerBound = input.lowerBound;
  output.upperBound = input.upperBound;
  output.name = input.name;
  IOConversions::i2oConvertConstraintType(input, output);
  output.grids.resize(1);
  IOConversions::createConstraintGridOutputFromMDOotputVect(mdOptVect, output.grids[0]);
  //if two ore more constraints are merged into one, set Lagrange mutliplier to 0.0 for inactive constraints
  const double threshold = 1e-10;
  for(unsigned i=0; i<mdOptVect.size(); i++){
    if(fabs(output.lowerBound-mdOptVect[i].lowerBound)>threshold){
      if(fabs(mdOptVect[i].value - output.upperBound)>threshold){
        output.grids.front().lagrangeMultiplier[i] = 0.0;
      }
    }
    if(fabs(output.upperBound-mdOptVect[i].upperBound)>threshold){
      if(fabs(mdOptVect[i].value - output.lowerBound)>threshold){
        output.grids.front().lagrangeMultiplier[i] = 0.0;
      }
    }
  }
  return output;
}


/**
* @brief creates state Output structure
*
* @param[in] pointer to the GenericEso class
* @return pointer to the StateOutput vector
*/
std::vector<UserOutput::StateOutput> IOConversions::createStateOutputVector(
                                                  const GenericEso::Ptr &genEso) const
{
  std::vector<UserOutput::StateOutput> stateOutputVector;
  EsoIndex numEso = genEso->getNumStates();
  std::vector<EsoIndex> stateIndex(numEso);
  genEso->getStateIndex(numEso, &stateIndex[0]);
  for(unsigned i=0; i<stateIndex.size(); i++)
  {
    stateOutputVector[i].esoIndex = stateIndex[i];
    stateOutputVector[i].name = getStateName(i, genEso);
  }
  return stateOutputVector;
}

/**
* @brief creates state Output structure
*
* @param[in] pointer to the GenericEso class
* @return pointer to the StateOutput vector
*/
std::string  IOConversions::getStateName(EsoIndex iStateIndex,const GenericEso::Ptr &genEso) const
{
  std::vector <std::string> strNames;
  genEso->getVariableNames(strNames);
  return strNames[iStateIndex];
}

/**
* @brief creates FirstSensitivityOutput vector
*
* @param[in] SavedOptDataStruct structure
* @param[in] pointer to generic eso class
* @param[in] vector of parameter indices
* @return FirstSensitivityOutput vector
*/
std::vector <UserOutput::FirstSensitivityOutput>  IOConversions::createSensitivityOutputVector(IntOptDataMap iODP,
                                                                                  GenericEso::Ptr &genPtr,
                                                                                  std::vector <unsigned> parameterIndices) const
{
  std::vector <UserOutput::FirstSensitivityOutput> output;
  UserOutput::FirstSensitivityOutput out;
  IntOptDataMap::iterator iter;
  SavedOptDataStruct structSODS;
  for(iter = iODP.begin(); iter != iODP.end(); iter++)
  {
    structSODS = (*iter).second;
    const unsigned numParams = structSODS.activeParameterIndices.size();
    const unsigned numGradientParts = structSODS.nonLinConstVarIndices.size();
    for(unsigned i=0; i<numGradientParts; i++)
    {
      const int index = structSODS.nonLinConstVarIndices.at(i);
      std::string str = getStateName(index, genPtr);
      std::pair <std::string, int> tv;
      tv.first = str;
      tv.second = index;
      out.mapConstrRows.insert(tv);
    }
    std::vector<unsigned> parameterPositions;
    parameterPositions.reserve(parameterIndices.size());
    for(unsigned j=0; j<parameterIndices.size(); j++)
    {
      std::vector<unsigned>::iterator elementPtr;
      elementPtr = std::find(structSODS.activeParameterIndices.begin(),
                             structSODS.activeParameterIndices.end(),
                             parameterIndices[j]);
      if(elementPtr != structSODS.activeParameterIndices.end())
      {
        const unsigned elementPos = elementPtr - structSODS.activeParameterIndices.begin();
        parameterPositions.push_back(elementPos);
      }
    }
    out.values.reserve(structSODS.nonLinConstGradients.getSize());
    std::vector<double>parameterValues(parameterPositions.size());
    for(unsigned j=0; j<numGradientParts; j++)
    {
      //structSODS.nonLinConstGradients[j];
      structSODS.activeParameterIndices.size();
      for(unsigned k=0; k<numParams; k++)
      {
        if(parameterPositions[k] == j*numParams+k)
        {
          parameterValues.push_back(structSODS.nonLinConstGradients[j*numParams+k]);
        }
      }// for k
      out.values[j].assign(parameterValues.begin(), parameterValues.end());
    }// for j
  }
  output.push_back(out);
    // not yet completed!
  return output;
}

/**
* @brief creates FirstSensitivityOutput vector
*
* @param[in] IntOptDataMap vector
* @param[in] pointer to generic eso class
* @param[in] pointer to ParameterizationGrid class
* @param[in] pointer to ParameterGridInput structure
* @return ParameterGridOutput structure
*/
UserOutput::ParameterGridOutput  IOConversions::createParameterGridOutput(IntOptDataMap iODP,
                                                              GenericEso::Ptr &genEso,
                                                              ParameterizationGrid &grid,
                                                              UserInput::ParameterGridInput &paramGridIn) const
{
  UserOutput::ParameterGridOutput output;
  output.duration = grid.getDurationValue();
  output.timePoints = grid.getDiscretizationGrid();
  std::vector<ControlParameter::Ptr> ParameterPtr = grid.getAllControlParameter();
  std::vector<unsigned> ParameterIndices(ParameterPtr.size()+1);
  int index;
  for(unsigned i=0; i<ParameterPtr.size(); i++)
  {
    ParameterIndices[i] = ParameterPtr[i]->getParameterIndex();
    output.values[i] = ParameterPtr[i]->getParameterValue();
    index = i;
    if(!ParameterPtr[i]->getIsDecisionVariable()){
      output.isFixed = true;
    }
  }
  DurationParameter::Ptr FTPtr = grid.getDurationPointer();
  ParameterIndices[index+1] = FTPtr->getParameterIndex();
  output.firstSensitivities = IOConversions::createSensitivityOutputVector(iODP, genEso, ParameterIndices);// how to convert first parameter?
  output.type = IOConversions::i2oConvertApproximationType(paramGridIn);
  output.numIntervals = paramGridIn.numIntervals;
  output.hasFreeDuration = paramGridIn.hasFreeDuration;
  return output;
}

/**
* @brief creates control output
*
* @param[in] Control class
* @param[in] pointer to parameter input structure
* @param[in] pointer to generic eso class
* @param[in] pointer to IntOptDataMap mapping
* @return ParameterOutput structure
*/
UserOutput::ParameterOutput  IOConversions::createControlOutput(Control control,
                                                   UserInput::ParameterInput input,
                                                   GenericEso::Ptr &genPtr,
                                                   IntOptDataMap iODP) const
{
  UserOutput::ParameterOutput output;
  IOConversions::i2oParameterInput(input, output);
  ParameterizationGrid::Ptr paramGridPtr;
  output.grids.resize(control.getNumberOfGrids());
  for(unsigned i=0; i<control.getNumberOfGrids(); i++)
  {
    paramGridPtr = control.getParameterizationGrid(i);
    output.grids[i] = createParameterGridOutput(iODP, genPtr, *paramGridPtr,input.grids[i]);
  }
  return output;
}

/**
* @brief creates parameter output
*
* @param[in] parameter input structure
* @param[in] pointer to generic eso structure
* @param[in] IntOptDataMap
* @param[in] pointer to Parameter structure
* @return Parameter Output structure
*/
UserOutput::ParameterOutput  IOConversions::createParameterOutput(UserInput::ParameterInput input,
                                                     GenericEso::Ptr eso,
                                                     IntOptDataMap iodm,
                                                     Parameter::Ptr p) const
{
  UserOutput::ParameterOutput output;
  IOConversions::i2oParameterInput(input, output);

  output.value = p->getParameterValue();

  output.grids.resize(1);
  std::vector<unsigned> iv;
  iv[0] = p->getParameterIndex();
  output.grids.front().firstSensitivities = IOConversions::createSensitivityOutputVector(iodm, eso, iv);
  return output;
}

/**
* @brief creates constraint output
*
* @param[in] OptimizerSingleStageMetaData structure
* @param[in] generic optimizer OptimizerOutput structure
* @param[in] ConstraintInput structure
* @return Constraint Output structure
*/
UserOutput::ConstraintOutput  IOConversions::createConstraintOutput(OptimizerMetaData &omd,
                                                                    DyosOutput::OptimizerOutput oo,
                                                                    UserInput::ConstraintInput input) const
{
  UserOutput::ConstraintOutput  output;
  std::vector<DyosOutput::ConstraintOutput> mdOpt;
  IOConversions::i2oConvertConstraint(input, mdOpt);
  //will be extended in future
  return output;
}

/**
* @brief creates constraint output
*
* @param[in] pointer to OptimizerMetaData structure
* @param[in] ConstraintInput structure
* @return Constraint Output structure
*/
UserOutput::ConstraintOutput  IOConversions::createObjectiveOutput(OptimizerMetaData &omd, UserInput::ConstraintInput input) const
{
  UserOutput::ConstraintOutput  output;
  std::vector<DyosOutput::ConstraintOutput> mdOpt;
  IOConversions::i2oConvertConstraint(input, mdOpt);
  //will be extended in future
  return output;
}

/**
* @brief creates linear constraint output vector
*
* @param[in] pointer to OptimizerMetaData structure
* @param[in] OptimizerOutput structure
* @return vector of Constraint Output structure
*/
std::vector<UserOutput::ConstraintOutput>  IOConversions::createLinearConstraintVector
                                                          (OptimizerMetaData &omd,
                                                           DyosOutput::OptimizerOutput oo) const
{
  OptimizerProblemDimensions opd = omd.getOptProbDim();
  const unsigned numOutput = opd.numLinConstraints;
  std::vector<UserOutput::ConstraintOutput> output(numOutput);
  utils::ExtendableArray<double> lowerBounds, upperBounds;
  omd.getLinConstraintBounds(lowerBounds, upperBounds);
  for(unsigned i=0; i<numOutput; i++)
  {
    std::string str = boost::lexical_cast<string>(i);
    output[i].name = "Linear"+str;
    output[i].lowerBound = lowerBounds[i];
    output[i].upperBound = upperBounds[i];
    output[i].type = UserOutput::ConstraintOutput::POINT;
  }
  return output;
}

/**
* @brief creates OptimizerOutput output structure
*
* @param[in] OptimizerInput structure
* @param[in] OptimizerOutput structure
* @param[in] OptimizerMetaData structure
* @return created OptimizerOutput structure
*/
UserOutput::OptimizerOutput  IOConversions::createOptimizerOutput(UserInput::OptimizerInput input,
                                                                  DyosOutput::OptimizerOutput oo,
                                                                  OptimizerMetaData &omd) const
{
  UserOutput::OptimizerOutput output;
  IOConversions::i2oConvertOptimizationType(input, output);
  IOConversions::i2oConvertResultFlag(oo, output);
  const unsigned numGlobalConstrains = input.globalConstraints.size();
  for(unsigned i=0; i<numGlobalConstrains; i++)
  {
    IOConversions::createConstraintOutput(omd, oo, input.globalConstraints[i]);
  }
  IOConversions::createLinearConstraintVector(omd, oo);
  return output;
}

/**
* @brief creates OptimizerOutput output structure
*
* @param[in] OptimizerInput structure
* @return created OptimizerOutput structure
*/
void IOConversions::i2oConvertOptimizationType(const UserInput::OptimizerInput &input,
                                                     UserOutput::OptimizerOutput &output) const
{
  switch(input.type)
  {
  case UserInput::OptimizerInput::IPOPT:
    output.type = UserOutput::OptimizerOutput::IPOPT;
    break;
  case UserInput::OptimizerInput::SNOPT:
    output.type = UserOutput::OptimizerOutput::SNOPT;
    break;
  case UserInput::OptimizerInput::NPSOL:
    output.type = UserOutput::OptimizerOutput::NPSOL;
    break;
  case UserInput::OptimizerInput::FILTER_SQP:
    output.type = UserOutput::OptimizerOutput::FILTER_SQP;
    break;
  case UserInput::OptimizerInput::SENSITIVITY_INTEGRATION:
	output.type = UserOutput::OptimizerOutput::SENSITIVITY_INTEGRATION;
	break;
  default:
    assert(false);
  }
}

/**
* @brief creates simulation output structure
*
* @param[in] Input structure
* @param[in] pointer to IntegratorMetaData structure
* @return created Output structure
*/
UserOutput::Output  IOConversions::createSimulationOutput(UserInput::Input input,
                                                          IntegratorMetaData &imd) const
{
  UserOutput::Output output;
  output = IOConversions::i2oConvertInputToOutput(input);
  output.integratorOutput = IOConversions::i2oConvertIntegrator(input.integratorInput);
  //imd.getOutput(input.stages, output.stages);
  return output;
}

/**
* @brief creates optimization output structure
*
* @param[in] Input structure
* @param[in] pointer to OptimizerMetaData structure
* @param[in] OptimizerOutput structure
* @return created Output structure
*/
UserOutput::Output  IOConversions::createOptimizationOutput(UserInput::Input input,
                                                            OptimizerMetaData &omd,
                                                            DyosOutput::OptimizerOutput oo) const
{
  UserOutput::Output output;
  output = IOConversions::i2oConvertInputToOutput(input);
  output.integratorOutput = IOConversions::i2oConvertIntegrator(input.integratorInput);
  //omd.getOutput(input.stages, output.stages, oo);
  output.optimizerOutput = IOConversions::createOptimizerOutput(input.optimizerInput, oo, omd);
  return output;
}


/**
* @brief creates optimization output structure
*
* @param[in] pointer to OptimizerOutput structure
* @return created OptimizerOutput structure
*/
void IOConversions::i2oConvertResultFlag(const DyosOutput::OptimizerOutput &oo,
                                               UserOutput::OptimizerOutput &output) const
{
  switch(oo.m_informFlag)
  {
  case DyosOutput::OptimizerOutput::OK:
    output.resultFlag = UserOutput::OptimizerOutput::OK;
    break;
  case DyosOutput::OptimizerOutput::FAILED:
      output.resultFlag = UserOutput::OptimizerOutput::FAILED;
      break;
  case DyosOutput::OptimizerOutput::INFEASIBLE:
    output.resultFlag = UserOutput::OptimizerOutput::INFEASIBLE;
    break;
  case DyosOutput::OptimizerOutput::NOT_OPTIMAL:
    output.resultFlag = UserOutput::OptimizerOutput::NOT_OPTIMAL;
    break;
  case DyosOutput::OptimizerOutput::TOO_MANY_ITERATIONS:
      output.resultFlag = UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS;
    break;
  case DyosOutput::OptimizerOutput::WRONG_GRADIENTS:
    output.resultFlag = UserOutput::OptimizerOutput::WRONG_GRADIENTS;
    break;
  }
}

/**
* @brief converts first sensitivity structure from meta data to user output
*
* @param[in] mete data FirstSensitivityOutput structure
* @return created FirstSensitivityOutput structure
*/
UserOutput::FirstSensitivityOutput  IOConversions::i2oConvertFirstSensitivity(
                                            const DyosOutput::FirstSensitivityOutput &fSO) const
{
  UserOutput::FirstSensitivityOutput output;
  output.mapConstrRows = fSO.mapConstrRows;
  output.mapParamsCols = fSO.mapParamsCols;
  const unsigned numValues = fSO.values.size();
  output.values.resize(numValues);
  for(unsigned i=0; i<numValues; i++)
  {
    const unsigned numSubValues = fSO.values[i].size();
    output.values[i].resize(numSubValues);
    output.values[i].assign(fSO.values[i].begin(), fSO.values[i].end());
  }
  return output;
}

/**
* @brief creates user ParameterGridOutput from user ParameterGridInput and dyos(meta data) ParameterGridOutput
*
* @param[in]pointer to parameter grid input structure
* @param[in]pointer to dyos parameter grid output structure
* @return created ParameterGridOutput structure
*/
UserOutput::ParameterGridOutput  IOConversions::i2oConvertParameterGrid(
                                                     const UserInput::ParameterGridInput &input,
                                                     const DyosOutput::ParameterGridOutput &mdOpt) const
{
  UserOutput::ParameterGridOutput output;
  output.type = IOConversions::o2oConvertApproximationType(mdOpt);
  const unsigned numSensitivities = mdOpt.firstSensitivityOutputVector.size();
  output.firstSensitivities.reserve(numSensitivities);
  for(unsigned i=0; i<numSensitivities; i++)
  {
    output.firstSensitivities.push_back(IOConversions::i2oConvertFirstSensitivity(mdOpt.firstSensitivityOutputVector[i]));
  }
  output.duration = mdOpt.duration;
  output.timePoints.assign(mdOpt.gridPoints.begin(), mdOpt.gridPoints.end());
  output.lagrangeMultipliers.assign(mdOpt.lagrangeMultiplier.begin(), mdOpt.lagrangeMultiplier.end());
  output.hasFreeDuration = mdOpt.hasFreeDuration;
  output.numIntervals = input.numIntervals;
  output.isFixed = mdOpt.isFixed;
  output.values.assign(mdOpt.values.begin(), mdOpt.values.end());
  return output;
}

/**
* @brief creates user ParameterOutput from user ParameterInput and dyos(meta data) ParameterOutput
*
* @param[in]pointer to parameter input structure
* @param[in]pointer to dyos parameter output structure
* @return created ParameterOutput structure
*/
UserOutput::ParameterOutput  IOConversions::i2oConvertParameter(
                                                const UserInput::ParameterInput &input,
                                                const DyosOutput::ParameterOutput &mdOpt) const
{
  assert(input.paramType==UserInput::ParameterInput::DURATION || input.name == mdOpt.name);
  UserOutput::ParameterOutput output;
  const unsigned numGrids = mdOpt.grids.size();

  //create a dummy input for parameters without any grids (such as duration and initial)
  if(input.grids.empty() && !mdOpt.grids.empty()){
    UserInput::ParameterGridInput dummy;
    output.grids.resize(1);
    output.grids[0] = IOConversions::i2oConvertParameterGrid(dummy, mdOpt.grids.front());
  }
  else{
    output.grids.resize(numGrids);
    UserInput::ParameterGridInput dummy;
    for(unsigned i=0; i<numGrids; i++){
      output.grids[i] = IOConversions::i2oConvertParameterGrid(dummy, mdOpt.grids[i]);
      output.grids[i].numIntervals = output.grids[i].timePoints.size()-1;
    }
  }
  //changed from mdOpt to input as bounds for optimized initials were not correct
  //may be better to search for reason that bounds are not in mdOpt
  output.lowerBound = input.lowerBound;
  output.upperBound = input.upperBound;
  output.name = mdOpt.name;
  output.lagrangeMultiplier = mdOpt.lagrangeMultiplier;
  IOConversions::i2oConvertParameterType(input, output);
  IOConversions::i2oConvertParameterSensitivityType(input, output);
  output.value = mdOpt.value;
  return output;
}

/**
* @brief converts state grid structure from meta data to user output
*
* @param[in] mete data StateGridOutput structure
* @return created StateGridOutput structure
*/
UserOutput::StateGridOutput  IOConversions::i2oConvertStateGrid(
                                            const DyosOutput::StateGridOutput &mdOpt) const
{
  UserOutput::StateGridOutput output;
  output.timePoints.assign(mdOpt.gridPoints.begin(), mdOpt.gridPoints.end());
  output.values.assign(mdOpt.values.begin(), mdOpt.values.end());
  return output;
}

/**
* @brief converts state structure from meta data to user output
*
* @param[in] mete data StateOutput structure
* @return created StateOutput structure
*/
UserOutput::StateOutput  IOConversions::i2oConvertState(const DyosOutput::StateOutput &mdOpt) const
{
  UserOutput::StateOutput output;
  output.esoIndex = mdOpt.esoIndex;
  output.grid = IOConversions::i2oConvertStateGrid(mdOpt.grid);
  output.name = mdOpt.name;
  return output;
}

DyosOutput::ParameterOutput IOConversions::findMatchingInitial
                                  (const std::vector<DyosOutput::ParameterOutput> &initialOutputs,
                                                  const UserInput::ParameterInput initialInput) const
{
  std::string name = initialInput.name;
  
  for(unsigned i=0; i<initialOutputs.size(); i++){
    if(initialOutputs[i].name == name){
      return initialOutputs[i];
    }
  }
  
  assert(false);
  return initialOutputs.front();
}

/**
* @brief creates user IntegratorStageOutput from user IntegratorStageInput and dyos(meta data) IntegratorStageOutput
*
* @param[in]pointer to integrator stage input structure
* @param[in]pointer to dyos integrator stage output structure
* @return created IntegratorStageOutput structure
*/
UserOutput::IntegratorStageOutput  IOConversions::i2oConvertIntegratorStage(
                                                     const UserInput::IntegratorStageInput &input,
                                                     const DyosOutput::IntegratorStageOutput &mdOpt) const
{
  UserOutput::IntegratorStageOutput output;
  DyosOutput::IntegratorStageOutput mdOptCopy = mdOpt;
  output.durations.resize(mdOpt.durations.size());
  for(unsigned i=0; i<mdOpt.durations.size(); i++){
    output.durations[i] = IOConversions::i2oConvertParameter(input.duration, mdOpt.durations[i]);
  }
  const unsigned numStates = mdOpt.states.size();
  output.states.reserve(numStates);
  for(unsigned i=0; i<numStates; i++)
  {
    output.states.push_back(IOConversions::i2oConvertState(mdOpt.states[i]));
  }

  const unsigned numAdjoints = mdOpt.adjoints.size();
  output.adjoints.reserve(numAdjoints);
  for(unsigned i=0; i<numAdjoints; i++){
    output.adjoints.push_back(IOConversions::i2oConvertStateGrid(mdOpt.adjoints[i]));
  }
  output.adjoints2ndOrder = IOConversions::i2oConvertStateGrid(mdOpt.adjoints2ndOrder);
  
  const unsigned numParameters = input.parameters.size();
  output.parameters.reserve(numParameters);
  unsigned iCounter = 0;
  for(unsigned i=0; i<numParameters; i++)
  {
    
    if(input.parameters[i].paramType == UserInput::ParameterInput::INITIAL)
    {
      // initials are sorted using the EsoIndex, so the order of the initials might be different from the users
      // input order. This means, that we have to search the correct initial using the parameter name
      // (highly inefficient)
      DyosOutput::ParameterOutput initialOut = findMatchingInitial(mdOpt.initials, input.parameters[i]);
      output.parameters.push_back(IOConversions::i2oConvertParameter(input.parameters[i], initialOut));
    }
    else
    {
      assert(iCounter < mdOpt.parameters.size());
      //after parameter propagation parameters might be in the wrong order
      //so search for the right entry and then swap entries in output
      if(input.parameters[i].name != mdOptCopy.parameters[iCounter].name){
        for(unsigned j=iCounter+1; j<mdOptCopy.parameters.size(); j++){
          if(input.parameters[i].name == mdOptCopy.parameters[j].name){
            DyosOutput::ParameterOutput temp = mdOptCopy.parameters[iCounter];
            mdOptCopy.parameters[iCounter] = mdOptCopy.parameters[j];
            mdOptCopy.parameters[j] = temp;
            break;
          }
        }
      }
      output.parameters.push_back(IOConversions::i2oConvertParameter(input.parameters[i], mdOptCopy.parameters[iCounter]));
      iCounter++;
    }
  }
  return output;
}

/**
* @brief creates user OptimizerStageOutput from user OptimizerStageInput and dyos(meta data) OptimizerStageOutput
*
* @param[in]pointer to OptimizerStageInput structure
* @param[in]pointer to dyos OptimizerStageOutput structure
* @return created OptimizerStageOutput structure
*/
UserOutput::OptimizerStageOutput  IOConversions::i2oConvertOptimizerStage
                                                      (const UserInput::OptimizerStageInput &input,
                                                       const DyosOutput::OptimizerStageOutput &mdOpt) const
{
  UserOutput::OptimizerStageOutput output;
  std::vector<DyosOutput::ConstraintOutput> obVect(1);
  obVect[0] = mdOpt.objective;
  output.objective = IOConversions::i2oConvertConstraint(input.objective, obVect);
  const unsigned numInputConstraints = input.constraints.size();
  output.nonlinearConstraints.resize(numInputConstraints);
  for(unsigned i=0; i<numInputConstraints; i++)
  {
    std::vector<DyosOutput::ConstraintOutput> contraintOutputVector;
    output.nonlinearConstraints[i].name = input.constraints[i].name;
    for(unsigned j=0; j<mdOpt.nonLinearConstraints.size(); j++)
    {
      if(input.constraints[i].name == mdOpt.nonLinearConstraints[j].name){
        switch(input.constraints[i].type){
          case UserInput::ConstraintInput::ENDPOINT:
            if(mdOpt.nonLinearConstraints[j].timePoint == 1.0){
              contraintOutputVector.push_back(mdOpt.nonLinearConstraints[j]);
            }
            break;
          case UserInput::ConstraintInput::POINT:
            if(std::abs(mdOpt.nonLinearConstraints[j].timePoint - input.constraints[i].timePoint) < 1e-10){
              contraintOutputVector.push_back(mdOpt.nonLinearConstraints[j]);
            }
            break;
          case UserInput::ConstraintInput::PATH:
            contraintOutputVector.push_back(mdOpt.nonLinearConstraints[j]);
            break;
          default:
            assert(false);
        }
      }
    }
    
    output.nonlinearConstraints[i] = IOConversions::i2oConvertConstraint(input.constraints[i], contraintOutputVector);
  }
  return output;
}

/**
* @brief creates user StageOutput structure from user StageInput structure and dyos(meta data) StageOutput
*
* @param[in]pointer to StageInput structure
* @param[in]pointer to dyos StageOutput structure
* @return created StageOutput structure
*/
UserOutput::StageOutput  IOConversions::i2oConvertStage(const UserInput::StageInput &input,
                                                        const DyosOutput::StageOutput &mdOpt) const
{
  UserOutput::StageOutput output;
  output.eso = IOConversions::i2oConvertEso(input.eso);
  output.integrator = IOConversions::i2oConvertIntegratorStage(input.integrator,mdOpt.integrator);
  output.mapping = IOConversions::i2oConvertMapping(input.mapping);
  output.treatObjective = input.treatObjective;
  output.optimizer = IOConversions::i2oConvertOptimizerStage(input.optimizer, mdOpt.optimizer);
  return output;
}


/**
* @brief
*/
UserOutput::SecondOrderOutput IOConversions::i2oConvert2ndOrderOuput
                                     (const DyosOutput::SecondOrderOutput &secondOrderOut) const
{
  UserOutput::SecondOrderOutput out;
  out.compositeAdjoints = secondOrderOut.compositeAdjoints;
  out.values = secondOrderOut.hessian;
  out.constParamHessian = secondOrderOut.constParamHessian;
  out.indicesHessian = secondOrderOut.indicesHessian;

  return out;
}

/**
* @brief creates user OptimizerOutput structure from user OptimizerInput structure and dyos(meta data) OptimizerOutput
*
* @param[in]pointer to OptimizerInput structure
* @param[in]pointer to dyos Output structure
* @return created OptimizerOutput structure
*/
UserOutput::OptimizerOutput  IOConversions::i2oConvertOptimizer(
                                            const UserInput::OptimizerInput &input,
                                            const DyosOutput::Output &mdOpt) const
{
  UserOutput::OptimizerOutput output;
  IOConversions::i2oConvertResultFlag(mdOpt.optimizerOutput, output);
  IOConversions::i2oConvertOptimizationType(input, output);
  output.intermConstrVio = mdOpt.optimizerOutput.intermConstrVio;
  output.secondOrder = i2oConvert2ndOrderOuput(mdOpt.optimizerOutput.secondOrderOutput);
  output.objVal = mdOpt.optimizerOutput.optObjFunVal;

  return output;
}

/**
* @brief creates user Output structure from user Input structure and dyos(meta data) Output
*
* @param[in]pointer to user Input structure
* @param[in]pointer to dyos Output structure
* @return created user Output structure
*/
UserOutput::Output IOConversions::i2oConvertOutput(const UserInput::Input &input,
                                                   const DyosOutput::Output &mdOpt) const
{
  UserOutput::Output output;
  output.integratorOutput = IOConversions::i2oConvertIntegrator(input.integratorInput);
  output.optimizerOutput = IOConversions::i2oConvertOptimizer(input.optimizerInput, mdOpt);
  output.runningMode = IOConversions::i2oConvertRunningMode(input);
  const unsigned numStages = mdOpt.stageOutput.size();
  output.stages.reserve(numStages);
  //propagate all parameters to following stages, if not declared on that stage
  UserInput::Input updatedInput = propagateParameters(input);
  for(unsigned i=0; i<numStages; i++){
    output.stages.push_back(IOConversions::i2oConvertStage(updatedInput.stages[i],mdOpt.stageOutput[i]));
	output.stages[i].esoPtr = input.stages[i].esoPtr;
  }
  output.solutionHistory.resize(mdOpt.solutionHistory.size());
  for(unsigned i=0; i<mdOpt.solutionHistory.size(); i++){
    output.solutionHistory[i] = i2oConvertOutput(input, mdOpt.solutionHistory[i]);
    //avoid recursive solution histories
    output.solutionHistory[i].solutionHistory.clear();
  }
  output.totalEndTimeLowerBound = input.totalEndTimeLowerBound;
  output.totalEndTimeUpperBound = input.totalEndTimeUpperBound;
  output.solutionTime = mdOpt.solutionTime;
  output.finalIntegration = input.finalIntegration;
  return output;
}

/**
 *@brief converts approximation order of DyosOutput to UserOutput
 * @param[in] input pointer to the input to be converted
 * @return created user output
 */
UserOutput::ParameterGridOutput::ApproximationType IOConversions::o2oConvertApproximationType
                                              (const DyosOutput::ParameterGridOutput &input) const
{
  UserOutput::ParameterGridOutput output;
  switch(input.approximation)
  {
  case DyosOutput::ParameterGridOutput::PieceWiseConstant:
    output.type = UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT;
    break;
  case DyosOutput::ParameterGridOutput::PieceWiseLinear:
    output.type = UserOutput::ParameterGridOutput::PIECEWISE_LINEAR;
    break;
  default:
    ;
  }
  return output.type;
}


/**
* @brief converts entire UserOutput structure to the UserInput structure
*
* @param[in] pointer to the output structure
* @return pointer to the input structure
*/
UserInput::Input IOConversions::o2iConvertOutputToInput(const UserOutput::Output & input) const
{
	UserInput::Input output;
	output.integratorInput = o2iConvertIntegratorInput(input);
	output.optimizerInput  = o2iConvertOptimizerInput(input);
	output.runningMode = o2iConvertRunningMode(input);
	output.totalEndTimeLowerBound = input.totalEndTimeLowerBound; 
	output.totalEndTimeUpperBound = input.totalEndTimeUpperBound; 
	output.finalIntegration = input.finalIntegration;
	output.stages.resize(input.stages.size());
	for (unsigned i = 0; i<output.stages.size(); i++){
		output.stages[i] = o2iConvertStage(input.stages[i]);
	}
	return output;
}// function i2oConvertOutputToInput


 /**
 * @brief converts IntegratorInput from UserOutput to the UserInput 
 *
 * @param[in] pointer to the output structure
 * @return pointer to the input structure
 */
UserInput::IntegratorInput IOConversions::o2iConvertIntegratorInput(const UserOutput::Output & input) const
{
	UserInput::IntegratorInput output; 
	switch (input.integratorOutput.type)
	{
	case UserOutput::IntegratorOutput::IntegratorType::IDAS:
		output.type = UserInput::IntegratorInput::IDAS; 
		break; 
	case UserOutput::IntegratorOutput::IntegratorType::NIXE:
		output.type = UserInput::IntegratorInput::NIXE;
		break;
	case UserOutput::IntegratorOutput::IntegratorType::LIMEX:
		output.type = UserInput::IntegratorInput::LIMEX;
		break;
	default:
		break;
	}

	switch (input.integratorOutput.order)
	{
	case UserOutput::IntegratorOutput::IntegrationOrder::ZEROTH:
		output.order = UserInput::IntegratorInput::ZEROTH;
		break;
	case UserOutput::IntegratorOutput::IntegrationOrder::FIRST_FORWARD:
		output.order = UserInput::IntegratorInput::FIRST_FORWARD;
		break;
	case UserOutput::IntegratorOutput::IntegrationOrder::FIRST_REVERSE:
		output.order = UserInput::IntegratorInput::FIRST_REVERSE;
		break;
	case UserOutput::IntegratorOutput::IntegrationOrder::SECOND_REVERSE:
		output.order = UserInput::IntegratorInput::SECOND_REVERSE;
		break;
	default:
		break;
	}
	// options are currently NOT part of UserOutput
	// they must be specified explicitly or defaults are used.
	
	return output;
}

/**
* @brief converts OptimizerInput from UserOutput to the UserInput
*
* @param[in] pointer to the output structure
* @return pointer to the input structure
*/
UserInput::OptimizerInput IOConversions::o2iConvertOptimizerInput(const UserOutput::Output & input) const
{
	UserInput::OptimizerInput output;
	switch (input.optimizerOutput.type)
	{
	case UserOutput::OptimizerOutput::SNOPT: 
		output.type = UserInput::OptimizerInput::SNOPT;
		break;
	case UserOutput::OptimizerOutput::NPSOL:
		output.type = UserInput::OptimizerInput::NPSOL;
		break;
	case UserOutput::OptimizerOutput::IPOPT:
		output.type = UserInput::OptimizerInput::IPOPT;
		break;
	case UserOutput::OptimizerOutput::FILTER_SQP:
		output.type = UserInput::OptimizerInput::FILTER_SQP;
		break;
	default:
		break;
	}
	// options are currently NOT part of UserOutput
	// they must be specified explicitly or defaults are used.

	return output;
}

	
/**
 * @brief converts UserOutput running mode to comparable UserInput
 *
 * @param[in] pointer to the output structure
 * @return the input running mode
 */
	UserInput::Input::RunningMode IOConversions::o2iConvertRunningMode(const UserOutput::Output &input) const 
	{
		UserInput::Input::RunningMode output;
		
		switch (input.runningMode)
		{
		case UserOutput::Output::SIMULATION:
			output = UserInput::Input::SIMULATION;
			break;
		case UserOutput::Output::SINGLE_SHOOTING:
			output = UserInput::Input::SINGLE_SHOOTING;
			break;
		case UserOutput::Output::MULTIPLE_SHOOTING:
			output = UserInput::Input::MULTIPLE_SHOOTING;
			break;
		case UserOutput::Output::SENSITIVITY_INTEGRATION:
			output = UserInput::Input::SENSITIVITY_INTEGRATION;
			break; 
		default:;
		}
		return output;
	}

/**
 * @brief converts UserOutput Stage to comparable UserInput Stage
 *
 * @param[in] pointer to the output structure
 * @return the input running mode
 */
	UserInput::StageInput IOConversions::o2iConvertStage(const UserOutput::StageOutput &input) const
	{
		UserInput::StageInput stageInput;

		stageInput.eso = o2iConvertEso(input.eso);

		stageInput.mapping = o2iConvertStageMapping(input);
		stageInput.optimizer = o2iConvertOptimizer(input.optimizer);
		stageInput.treatObjective = input.treatObjective;

		//the duration of a stage should be only a single duration
		stageInput.integrator.duration = o2iConvertParameter(input.integrator.durations[0]);

		// explicitPlotGrid and plotGridResolution are not part of UserOutput
		stageInput.integrator.parameters.resize(input.integrator.parameters.size()); 
		//parameters
		for (size_t i = 0; i < input.integrator.parameters.size(); ++i) {
			stageInput.integrator.parameters[i] = o2iConvertParameter(input.integrator.parameters[i]);
		}
		return stageInput;
	}


/**
* @brief converts UserOutput genericEso structure to comparable UserInput structure
*
* @param[in] pointer to the eso output structure
* @return pointer to the constructed eso input structure
*/
	UserInput::EsoInput IOConversions::o2iConvertEso(const UserOutput::EsoOutput &esoOutput) const
{
	UserInput::EsoInput output;
	switch (esoOutput.type)
	{
	case UserOutput::EsoOutput::JADE:
		output.type = UserInput::EsoInput::JADE;
		break;
#ifdef BUILD_WITH_FMU
	case UserOutput::EsoOutput::FMI:
		output.type = UserInput::EsoInput::FMI;
#endif // BUILD_WITH_FMU

	default:;
	}
	output.model = esoOutput.model;
	return output;
}


/**
*@brief converts StageMapping from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
	UserInput::StageMapping IOConversions::o2iConvertStageMapping(const UserOutput::StageOutput &input) const
	{
		UserInput::StageMapping output;
		output.fullStateMapping = input.mapping.fullStateMapping;
		if (!output.fullStateMapping){
			// only if state mapping is not full, the stateNameMapping is needed
			output.stateNameMapping = input.mapping.stateNameMapping;
		}

		return output;
	}

/**
*@brief converts StageOptimizer from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
	UserInput::OptimizerStageInput IOConversions::o2iConvertOptimizer(const UserOutput::OptimizerStageOutput &input) const
	{
		UserInput::OptimizerStageInput output; 

		output.objective.name = input.objective.name; 
		output.objective.lowerBound = input.objective.lowerBound; 
		output.objective.upperBound = input.objective.upperBound; 
		output.objective.timePoint = input.objective.grids[0].timePoints[0]; 
		output.objective.lagrangeMultiplier = input.objective.grids[0].lagrangeMultiplier[0]; 
		//output.objective.lagrangeMultiplierVector; //causes problems //input.objective.grids[0].lagrangeMultiplier;
		output.objective.type = UserInput::ConstraintInput::POINT; 
		//output.objective.excludeZero; // Not part of UserOutput

		output.constraints.resize(input.nonlinearConstraints.size()); 
		// only nonlinear constraints
		for (size_t i=0; i < input.nonlinearConstraints.size(); ++i) {
			output.constraints[i] = o2iConvertConstraint(input.nonlinearConstraints[i]); 		
		}
		// No linear constraints encountered, as linear constraints are not defined in the 
		// UserInput, but in the DyosInput, e.g. for total duration
		//input.linearConstraints;

		return output; 
	}

/**
*@brief converts constraint type from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
	UserInput::ConstraintInput::ConstraintType IOConversions::o2iConvertConstraintType(const UserOutput::ConstraintOutput::ConstraintType &input) const
	{
		UserInput::ConstraintInput::ConstraintType output; 
		switch (input)
		{
		case UserOutput::ConstraintOutput::PATH:
			output = UserInput::ConstraintInput::ConstraintType::PATH; 
			break; 
		case UserOutput::ConstraintOutput::POINT:
			output = UserInput::ConstraintInput::ConstraintType::POINT; 
			break; 
		case UserOutput::ConstraintOutput::ENDPOINT: 
			output = UserInput::ConstraintInput::ConstraintType::ENDPOINT; 
			break; 
			// multiple shooting constraints should not be set by user?! 
		default:
			break;
		}

		return output; 
	}

/**
*@brief converts constraint from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
	UserInput::ConstraintInput IOConversions::o2iConvertConstraint(const UserOutput::ConstraintOutput &input) const
	{
		UserInput::ConstraintInput output;
		output.name = input.name;
		output.type = o2iConvertConstraintType(input.type);
		output.lowerBound = input.lowerBound;
		output.upperBound = input.upperBound;
		if (output.type == UserInput::ConstraintInput::PATH){
			//output.lagrangeMultiplierVector = input.grids[0].lagrangeMultiplier;
			//output.timePoint = input.grids[0].timePoints;
			// as path constraint resolution is not available, the lagrange Multipliers cannot be mapped, as the grid
			// does not match the inputs
		}
		if (output.type == UserInput::ConstraintInput::POINT) {
			output.lagrangeMultiplier = input.grids[0].lagrangeMultiplier[0]; 
			output.timePoint = input.grids[0].timePoints[0]; 
		}
		if (output.type == UserInput::ConstraintInput::ENDPOINT) {
			output.lagrangeMultiplier = input.grids[0].lagrangeMultiplier[0];
			output.timePoint = input.grids[0].timePoints[0];
		}
		
		return output;
	}


/**
*@brief converts Parameter from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
	UserInput::ParameterInput IOConversions::o2iConvertParameter(const UserOutput::ParameterOutput &input) const
	{
		UserInput::ParameterInput output;
		if (input.paramType == UserOutput::ParameterOutput::DURATION){
			// duration has no name and no grids
		}
		else if (input.paramType == UserOutput::ParameterOutput::INITIAL){
			// initial has no grids
			output.name = input.name;
		}
		else {
			output.grids = o2iConvertParameterGridInput(input.grids);
			output.name = input.name;
		}
		
		output.lowerBound = input.lowerBound;
		output.upperBound = input.upperBound;

		output.paramType = o2iConvertParameterType(input.paramType);
		output.sensType  = o2iConvertParameterSensType(input.sensType);
		
		output.value = input.value;

		return output;
	}

/**
*@brief converts ParameterGridInput from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
	std::vector<UserInput::ParameterGridInput> IOConversions::o2iConvertParameterGridInput(const std::vector<UserOutput::ParameterGridOutput> &input) const
	{
		std::vector<UserInput::ParameterGridInput> output(input.size());

		for (size_t i = 0; i<input.size(); ++i) {
			//output[i].adapt; //Not part of UserOutput
			output[i].duration = input[i].duration;
			output[i].hasFreeDuration = input[i].hasFreeDuration;
			output[i].numIntervals = input[i].numIntervals;
			//output[i].pcresolution; // Not part of UserOutput
			output[i].timePoints = input[i].timePoints;
			output[i].type = o2iConvertApproximationType(input[i]);
			output[i].values = input[i].values;
		}

		return output;
	}


/**
* @brief converts UserOutput approximation type  to comparable UserInput
*
* @param[in] pointer to the output parameter grid structure
* @return the input approximation type
*/
UserInput::ParameterGridInput::ApproximationType IOConversions::o2iConvertApproximationType(
	const UserOutput::ParameterGridOutput &input) const
{
	UserInput::ParameterGridInput output;
	switch (input.type)
	{
	case UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT:
		output.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
		break;
	case UserOutput::ParameterGridOutput::PIECEWISE_LINEAR:
		output.type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
		break;
	default:
		;
	}
	return output.type;
}



/*
*@brief converts ParameterSensType from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
UserInput::ParameterInput::ParameterSensitivityType IOConversions::o2iConvertParameterSensType(const UserOutput::ParameterOutput::ParameterSensitivityType &input) const
{
	UserInput::ParameterInput::ParameterSensitivityType output;
	switch (input)
	{
	case UserOutput::ParameterOutput::FRACTIONAL:
		output = UserInput::ParameterInput::FRACTIONAL;
		break; 
	case UserOutput::ParameterOutput::FULL:
		output = UserInput::ParameterInput::FULL;
		break; 
	case UserOutput::ParameterOutput::NO:
		output = UserInput::ParameterInput::NO;
		break; 
	default:
		break;
	}
	return output; 
}

/*
*@brief converts ParameterType from UserOutput to UserInput
* @param[in] input pointer to the input to be converted
* @return created user output
*/
UserInput::ParameterInput::ParameterType IOConversions::o2iConvertParameterType(const UserOutput::ParameterOutput::ParameterType &input) const
{
	UserInput::ParameterInput::ParameterType output; 
	switch (input)
	{
	case UserOutput::ParameterOutput::DURATION: 
		output = UserInput::ParameterInput::DURATION; 
		break;
	case UserOutput::ParameterOutput::INITIAL:
		output = UserInput::ParameterInput::INITIAL; 
		break;
	case UserOutput::ParameterOutput::PROFILE: 
		output = UserInput::ParameterInput::PROFILE; 
		break;
	case UserInput::ParameterInput::TIME_INVARIANT:
		output = UserInput::ParameterInput::TIME_INVARIANT; 
		break;
	default:
		break;
	}
	return output; 
}

