/**
* @file ConvertToXmlJson.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertToXmlJson                                                     \n
* =====================================================================\n
* This file contains the definitions of the function convertToXmlJson  \n
* and several subfunctions which convert the UserInput::Input into an  \n
* xml/json-file                                                        \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski, Adrian Caspari
* @date 18.10.2018
*/

#include "ConvertToXmlJson.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>

/**
* @brief create boost::propertyTree::ptree, call several subfunctions, write xml-file "convertedUserInput.xml"
*
* @param[in] std::string filename
* @param[in] UserInput::Input input
* @param[in] DataFormat dataFormat
*/
void createPropertyTree(std::string filename, const UserInput::Input &input, DataFormat dataFormat);
/**
* @brief put all components of optimizerInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to OptimizerInput
* @param[in] path of optimizerInput as string
*/
static void createPropertyTree_OptimizerInput(boost::property_tree::ptree &pTree,
                                              const UserInput::OptimizerInput &optimizerInput,
                                              std::string path);
/**
* @brief put all components of IntegratorInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to integratorInput
* @param[in] path of integratorInput as string
*/
static void createPropertyTree_IntegratorInput(boost::property_tree::ptree &pTree,
                                               const UserInput::IntegratorInput &integratorInput,
                                               std::string path);
/**
* @brief put all components of StageInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to stageInput
* @param[in] path of stageInput as string
*/
static void createPropertyTree_StageInput(boost::property_tree::ptree &pTree,
                                          const UserInput::StageInput &stageInput,
                                          std::string path);
/**
* @brief put all components of ParameterInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to parameterInput
* @param[in] path of parameterInput as string
*/
static void createPropertyTree_ParameterInput(boost::property_tree::ptree &pTree,
                                              const UserInput::ParameterInput &parameterInput,
                                              std::string path);
/**
* @brief put all components of ConstraintInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to constraintInput
* @param[in] path of constraintInput as string
*/
static void createPropertyTree_ConstraintInput(boost::property_tree::ptree &pTree,
                                               const UserInput::ConstraintInput &constraintInput,
                                               std::string path);
/**
* @brief gets the enum for AdaptiveStrategy
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getAdaptiveStrategy(UserInput::OptimizerInput::AdaptiveStrategy i);
/**
* @brief gets the enum for OptimizerType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getOptimizerType(UserInput::OptimizerInput::OptimizerType i);
/**
* @brief gets the enum for LinearSolverType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getLinearSolverType(UserInput::LinearSolverInput::SolverType i);
/**
* @brief gets the enum for NonLinearSolverType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getNonLinearSolverType(UserInput::NonLinearSolverInput::SolverType i);
/**
* @brief gets the enum for DaeInitializationType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getDaeInitializationType(UserInput::DaeInitializationInput::DaeInitializationType i);
/**
* @brief gets the enum for IntegrationOrder
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getIntegrationOrder(UserInput::IntegratorInput::IntegrationOrder i);
/**
* @brief gets the enum for IntegratorType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getIntegratorType(UserInput::IntegratorInput::IntegratorType i);
/**
* @brief gets the enum for RunningMode
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getRunningMode(UserInput::Input::RunningMode i);
/**
* @brief gets the enum for ApproximationType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getApproximationType(UserInput::ParameterGridInput::ApproximationType i);
/**
* @brief gets the enum for ParameterSensitivityType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getParameterSensitivityType(UserInput::ParameterInput::ParameterSensitivityType i);
/**
* @brief gets the enum for ParameterType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getParameterType(UserInput::ParameterInput::ParameterType i);
/**
* @brief gets the enum for EsoType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getEsoType(UserInput::EsoInput::EsoType i);
/**
* @brief gets the enum for AdaptationType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getAdaptationType(UserInput::AdaptationInput::AdaptationType i);
/**
* @brief gets the enum for ConstraintType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getConstraintType(UserInput::ConstraintInput::ConstraintType i);
/**
* @brief calls the function createPropertyTree
*
* @param[in] std::string filename
* @param[in] UserInput::Input input
* @param[in] DataFormat dataFormat
*/
void convertToXmlJson(std::string filename, UserInput::Input input, DataFormat dataFormat)
{
    createPropertyTree(filename, input, dataFormat);
}

void createPropertyTree(std::string filename, const UserInput::Input &input, DataFormat dataFormat)
{
  std::string path="debug.input";
  boost::property_tree::ptree pTree;
  pTree.put<unsigned>(path+".stages", input.stages.size());
  for (unsigned i=0; i<input.stages.size(); i++){
    createPropertyTree_StageInput(pTree, input.stages.at(i),
                                  path+".stages.stages_"+boost::lexical_cast<std::string>(i));
  }
  pTree.put<double>(path+".totalEndTimeLowerBound", input.totalEndTimeLowerBound);
  pTree.put<double>(path+".totalEndTimeUpperBound", input.totalEndTimeUpperBound);
  pTree.put<std::string>(path+".runningMode", getRunningMode(input.runningMode));

  createPropertyTree_IntegratorInput(pTree, input.integratorInput, path+".integratorInput");
  createPropertyTree_OptimizerInput(pTree, input.optimizerInput, path+".optimizerInput");

  if(dataFormat==XML)
    write_xml(filename, pTree);
  else if(dataFormat==JSON)
    write_json(filename, pTree);
}
static void createPropertyTree_OptimizerInput(boost::property_tree::ptree &pTree,
                                              const UserInput::OptimizerInput &optimizerInput,
                                              std::string path)
{
  pTree.put<std::string>(path+".optimizerType", getOptimizerType(optimizerInput.type));
  pTree.put<double>(path+".adaptationThreshold", optimizerInput.adaptationThreshold);
  pTree.put<std::string>(path+".adaptiveStrategy", getAdaptiveStrategy(optimizerInput.adaptStrategy));
  pTree.put<unsigned>(path+".globalConstraints", optimizerInput.globalConstraints.size());
  for (unsigned j=0; j<optimizerInput.globalConstraints.size(); j++){
    createPropertyTree_ConstraintInput(pTree, optimizerInput.globalConstraints.at(j),
      path+".globalConstraints.globalConstraints_"+boost::lexical_cast<std::string>(j) );
  }
}

static void createPropertyTree_IntegratorInput(boost::property_tree::ptree &pTree,
                                               const UserInput::IntegratorInput &integratorInput,
                                               std::string path)
{
  pTree.put<std::string>(path+".integratorType", getIntegratorType(integratorInput.type));
  pTree.put<std::string>(path+".integrationOrder", getIntegrationOrder(integratorInput.order));

  pTree.put<std::string>(path+".daeInit.daeInitializationType",
                         getDaeInitializationType(integratorInput.daeInit.type));
  pTree.put<std::string>(path+".daeInit.linSolver.solverType",
                         getLinearSolverType(integratorInput.daeInit.linSolver.type));
  pTree.put<std::string>(path+".daeInit.nonLinSolver.solverType",
                         getNonLinearSolverType(integratorInput.daeInit.nonLinSolver.type));
  pTree.put<double>(path+".daeInit.nonLinSolver.tolerance", integratorInput.daeInit.nonLinSolver.tolerance);
  pTree.put<double>(path+".daeInit.maximumErrorTolerance", integratorInput.daeInit.maximumErrorTolerance);
}

static void createPropertyTree_StageInput(boost::property_tree::ptree &pTree,
                                          const UserInput::StageInput &stageInput, std::string path)
{
  pTree.put<bool>(path+".treatObjective", stageInput.treatObjective);
  pTree.put<bool>(path+".mapping.fullStateMapping", stageInput.mapping.fullStateMapping);

    std::string path1;

  pTree.put<unsigned>(path+".mapping.stateNameMapping", stageInput.mapping.stateNameMapping.size());
  std::map<std::string,std::string>::const_iterator iter;
  for(iter=stageInput.mapping.stateNameMapping.begin(); iter!=stageInput.mapping.stateNameMapping.end(); iter++){
    path1=path+".mapping.stateNameMapping.stateNameMapping_"+boost::lexical_cast<std::string>
                           (std::distance(stageInput.mapping.stateNameMapping.begin() , iter));
    pTree.put<std::string>(path1, (*iter).first + "|" + (*iter).second);
  }

  pTree.put<std::string>(path+".eso.model", stageInput.eso.model);
  pTree.put<std::string>(path+".eso.process", stageInput.eso.process);
  pTree.put<std::string>(path+".eso.type", getEsoType(stageInput.eso.type));

  createPropertyTree_ParameterInput(pTree, stageInput.integrator.finaltime, path+".integrator.finaltime");
  pTree.put<unsigned>(path+".integrator.parameters", stageInput.integrator.parameters.size());
  for(unsigned j=0; j<stageInput.integrator.parameters.size(); j++) {//parameters is a vector of structs
    path1=path+".integrator.parameters.parameters_"+boost::lexical_cast<std::string>(j);
    createPropertyTree_ParameterInput(pTree, stageInput.integrator.parameters.at(j), path1);
  }
  createPropertyTree_ConstraintInput(pTree, stageInput.optimizer.objective, path+".optimizer.objective");
  pTree.put<unsigned>(path+".optimizer.constraints", stageInput.optimizer.constraints.size());
  for (unsigned j=0; j<stageInput.optimizer.constraints.size(); j++){
    path1=path+".optimizer.constraints.constraints_"+boost::lexical_cast<std::string>(j);
    createPropertyTree_ConstraintInput(pTree, stageInput.optimizer.constraints.at(j), path1);
  }
  pTree.put<unsigned>(path+".optimizer.structureDetection.maxStructureSteps", stageInput.optimizer.structureDetection.maxStructureSteps);
}

static void createPropertyTree_ConstraintInput(boost::property_tree::ptree &pTree,
                                               const UserInput::ConstraintInput &constraintInput,
                                               std::string path)
{
  pTree.put<std::string>(path+".name", constraintInput.name);
  pTree.put<double>(path+".lowerBound", constraintInput.lowerBound);
  pTree.put<double>(path+".upperBound", constraintInput.upperBound);
  pTree.put<double>(path+".timePoint", constraintInput.timePoint);
  pTree.put<double>(path+".lagrangeMultiplier", constraintInput.lagrangeMultiplier);
  pTree.put<std::string>(path+".constraintType", getConstraintType(constraintInput.type));
}


static void createPropertyTree_ParameterInput(boost::property_tree::ptree &pTree,
                                              const UserInput::ParameterInput &parameterInput,
                                              std::string path)
{
  pTree.put<std::string>(path+".name", parameterInput.name);
  pTree.put<double>(path+".lowerBound", parameterInput.lowerBound);
  pTree.put<double>(path+".upperBound", parameterInput.upperBound);
  pTree.put<double>(path+".value", parameterInput.value);
  pTree.put<std::string>(path+".parameterType", getParameterType(parameterInput.paramType));
  pTree.put<std::string>(path+".parameterSensitivityType", getParameterSensitivityType(parameterInput.sensType));

  std::string path1;
  std::string vectorString;
  pTree.put<unsigned>(path+".grids", parameterInput.grids.size());
  for (unsigned j=0; j<parameterInput.grids.size(); j++){//grids is a vector of structs
    path1=path+".grids.grids_"+boost::lexical_cast<std::string>( j );
    pTree.put<unsigned>(path1+".numIntervals", parameterInput.grids.at(j).numIntervals);
    pTree.put<double>(path1+".finalTime", parameterInput.grids.at(j).finalTime);
    pTree.put<bool>(path1+".hasFreeFinalTime", parameterInput.grids.at(j).hasFreeFinalTime);
    pTree.put<std::string>(path1+".approximationType", getApproximationType(parameterInput.grids.at(j).type));

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<parameterInput.grids.at(j).timePoints.size(); k++){

      vectorString+=boost::lexical_cast<std::string>(parameterInput.grids.at(j).timePoints.at(k))+",";
      //write all elements into vectorString
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".timePoints", vectorString); //put vectorString into pTree

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<parameterInput.grids.at(j).values.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(parameterInput.grids.at(j).values.at(k))+",";
      //write all elements into vectorString
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".values", vectorString);//put vectorString into pTree

    pTree.put(path1+".adapt.adaptType", getAdaptationType(parameterInput.grids.at(j).adapt.adaptType));
    pTree.put<unsigned>(path1+".adapt.maxAdaptSteps", parameterInput.grids.at(j).adapt.maxAdaptSteps);
    pTree.put<int>(path1+".adapt.adaptWave.maxRefinementLevel",
                        parameterInput.grids.at(j).adapt.adaptWave.maxRefinementLevel);
    pTree.put<int>(path1+".adapt.adaptWave.minRefinementLevel",
                        parameterInput.grids.at(j).adapt.adaptWave.minRefinementLevel);
    pTree.put<int>(path1+".adapt.adaptWave.horRefinementDepth",
                        parameterInput.grids.at(j).adapt.adaptWave.horRefinementDepth);
    pTree.put<int>(path1+".adapt.adaptWave.verRefinementDepth",
                        parameterInput.grids.at(j).adapt.adaptWave.verRefinementDepth);
    pTree.put<double>(path1+".adapt.adaptWave.etres", parameterInput.grids.at(j).adapt.adaptWave.etres);
    pTree.put<double>(path1+".adapt.adaptWave.epsilon", parameterInput.grids.at(j).adapt.adaptWave.epsilon);
  }
}

//functions to get enums

static std::string getAdaptiveStrategy(UserInput::OptimizerInput::AdaptiveStrategy i)
{
  switch (i)
  {
  case UserInput::OptimizerInput::NOADAPTATION:
      return "NOADAPTATION";
    case UserInput::OptimizerInput::ADAPTATION:
      return "ADAPTATION";
    case UserInput::OptimizerInput::STRUCTURE_DETECTION:
      return "STRUCTURE_DETECTION";
    default:
      throw EnumException();
      return "NO SUCH ENUM";
  }
}

static std::string getOptimizerType(UserInput::OptimizerInput::OptimizerType i)
{
  switch (i)
  {
    case UserInput::OptimizerInput::SNOPT:
      return "SNOPT";
    case UserInput::OptimizerInput::IPOPT:
      return "IPOPT";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getNonLinearSolverType(UserInput::NonLinearSolverInput::SolverType i)
{
  switch (i)
  {
  case UserInput::NonLinearSolverInput::NLEQ1S:
      return "NLEQ1S";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getLinearSolverType(UserInput::LinearSolverInput::SolverType i)
{
  switch (i)
  {
    case UserInput::LinearSolverInput::MA28:
      return "MA28";
	case UserInput::LinearSolverInput::KLU:
      return "KLU";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getDaeInitializationType(UserInput::DaeInitializationInput::DaeInitializationType i)
{
  switch (i)
  {
    case UserInput::DaeInitializationInput::NO:
      return "NO";
    case UserInput::DaeInitializationInput::FULL:
      return "FULL";
    case UserInput::DaeInitializationInput::BLOCK:
      return "BLOCK";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getIntegrationOrder(UserInput::IntegratorInput::IntegrationOrder i)
{
  switch (i)
  {
    case UserInput::IntegratorInput::ZEROTH:
      return "ZEROTH";
    case UserInput::IntegratorInput::FIRST_FORWARD:
      return "FIRST_FORWARD";
    case UserInput::IntegratorInput::FIRST_REVERSE:
      return "FIRST_REVERSE";
    case UserInput::IntegratorInput::SECOND_REVERSE:
      return "SECOND_REVERSE";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getIntegratorType(UserInput::IntegratorInput::IntegratorType i)
{
  switch (i)
  {
    case UserInput::IntegratorInput::NIXE:
      return "NIXE";
    case UserInput::IntegratorInput::LIMEX:
      return "LIMEX";
    case UserInput::IntegratorInput::IDAS:
      return "IDAS";
    default:
      throw EnumException();
      return "NO SUCH ENUM";
  }
}

static std::string getRunningMode(UserInput::Input::RunningMode i)
{
  switch (i)
  {
    case UserInput::Input::SIMULATION:
      return "SIMULATION";
    case UserInput::Input::SINGLE_SHOOTING:
      return "SINGLE_SHOOTING";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getApproximationType(UserInput::ParameterGridInput::ApproximationType i)
{
  switch (i)
  {
    case UserInput::ParameterGridInput::PIECEWISE_CONSTANT:
      return "PIECEWISE_CONSTANT";
    case UserInput::ParameterGridInput::PIECEWISE_LINEAR:
      return "PIECEWISE_LINEAR";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}
static std::string getParameterSensitivityType(UserInput::ParameterInput::ParameterSensitivityType i)
{
  switch (i)
  {
    case UserInput::ParameterInput::FULL:
      return "FULL";
    case UserInput::ParameterInput::FRACTIONAL:
      return "FRACTIONAL";
    case UserInput::ParameterInput::NO:
      return "NO";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getParameterType(UserInput::ParameterInput::ParameterType i)
{
  switch (i)
  {
    case UserInput::ParameterInput::INITIAL:
      return "INITIAL";
    case UserInput::ParameterInput::TIME_INVARIANT:
      return "TIME_INVARIANT";
    case UserInput::ParameterInput::PROFILE:
      return "PROFILE";
    case UserInput::ParameterInput::FINALTIME:
      return "FINALTIME";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getAdaptationType(UserInput::AdaptationInput::AdaptationType i)
{
  switch(i)
  {
    case UserInput::AdaptationInput::WAVELET:
      return "WAVELET";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getEsoType(UserInput::EsoInput::EsoType i)
{
  switch (i)
  {
    case UserInput::EsoInput::JADE:
      return "JADE";
    case UserInput::EsoInput::FMI:
      return "FMI";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}

static std::string getConstraintType(UserInput::ConstraintInput::ConstraintType i)
{
  switch (i)
  {
    case UserInput::ConstraintInput::PATH:
      return "PATH";
    case UserInput::ConstraintInput::POINT:
      return "POINT";
    case UserInput::ConstraintInput::ENDPOINT:
      return "ENDPOINT";
    default:
      throw EnumException();
      return "NO SUCH ENUM";  }
}
