/**
* @file ConvertUserOutputToXmlJson.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertUserOutputToXmlJson                                           \n
* =====================================================================\n
* This file contains the definitions of the function                   \n
* convertUserOutputToXmlJson and several subfunctions which convert    \n
* UserOutput::Output into an Xml/Json-file                             \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski, Adrian Caspari
* @date 18.10.2018
*/

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>
#include "ConvertToXmlJson.hpp"


/**
* @brief put all components of optimizerOutput into pTree
*
* @param[in] const reference to propertyTree
* @param[in] const reference to OptimizerOutput
* @param[in] path of optimizerOutput as string
*/
static void createPropertyTree_OptimizerOutput(boost::property_tree::ptree &pTree,
                                               const UserOutput::OptimizerOutput &optimizerOutput,
                                               std::string path);
/**
* @brief put all components of StageOutput into pTree
*
* @param[in] const reference to propertyTree
* @param[in] const reference to stageOutput
* @param[in] path of stageOutput as string
*/
static void createPropertyTree_StageOutput(boost::property_tree::ptree &pTree,
                                           const UserOutput::StageOutput &stageOutput,
                                           std::string path);
/**
* @brief put all components of ParameterOutput into pTree
*
* @param[in] const reference to propertyTree
* @param[in] const reference to parameterOutput
* @param[in] path of parameterOutput as string
*/
static void createPropertyTree_ParameterOutput(boost::property_tree::ptree &pTree,
                                               const UserOutput::ParameterOutput &parameterOutput,
                                               std::string path);
/**
* @brief put all components of ConstraintOutput into pTree
*
* @param[in] const reference to propertyTree
* @param[in] const reference to constraintOutput
* @param[in] path of constraintOutput as string
*/
static void createPropertyTree_ConstraintOutput(boost::property_tree::ptree &pTree,
                                                const UserOutput::ConstraintOutput &constraintOutput,
                                                std::string path);
/**
* @brief put all components of StateOutput into pTree
*
* @param[in] const reference to propertyTree
* @param[in] const reference to stateOutput
* @param[in] path of stateOutput as string
*/
static void createPropertyTree_StateOutput(boost::property_tree::ptree &pTree,
                                           const UserOutput::StateOutput &stateOutput,
                                           std::string path);
/**
* @brief put all components of FirstSensitivityOutput into pTree
*
* @param[in] const reference to propertyTree
* @param[in] const reference to firstSensitivityOutput
* @param[in] path of firstSensitivityOutput as string
*/
static void createPropertyTree_FirstSensitivityOutput(boost::property_tree::ptree &pTree,
                                                      const UserOutput::FirstSensitivityOutput &firstSensitivityOutput,
                                                      std::string path);
/**
* @brief gets the enum for ResultFlag
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getResultFlag(UserOutput::OptimizerOutput::ResultFlag i);
/**
* @brief gets the enum for OptimizerType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getOptimizerType(UserOutput::OptimizerOutput::OptimizerType i);
/**
* @brief gets the enum for IntegrationOrder
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getIntegrationOrder(UserOutput::IntegratorOutput::IntegrationOrder i);
/**
* @brief gets the enum for IntegratorType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getIntegratorType(UserOutput::IntegratorOutput::IntegratorType i);
/**
* @brief gets the enum for RunningMode
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getRunningMode(UserOutput::Output::RunningMode i);
/**
* @brief gets the enum for ApproximationType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getApproximationType(UserOutput::ParameterGridOutput::ApproximationType i);
/**
* @brief gets the enum for ParameterSensitivityType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getParameterSensitivityType(UserOutput::ParameterOutput::ParameterSensitivityType i);
/**
* @brief gets the enum for ParameterType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getParameterType(UserOutput::ParameterOutput::ParameterType i);
/**
* @brief gets the enum for EsoType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getEsoType(UserOutput::EsoOutput::EsoType i);
/**
* @brief gets the enum for ConstraintType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getConstraintType(UserOutput::ConstraintOutput::ConstraintType i);
/**
* @brief calls the function createPropertyTree
*
* @param[in] std::string filename
* @param[in] UserOutput::Output output
* @param[in] DataFormat dataFormat
*/
void convertUserOutputToXmlJson(std::string filename, UserOutput::Output output, DataFormat dataFormat)
{
  std::basic_ofstream<typename boost::property_tree::ptree::key_type::value_type> stream(filename.c_str());
  createPropertyTree_Output(stream, output, dataFormat);
}

void createPropertyTree_Output(boost::property_tree::ptree &pTree,
                         const UserOutput::Output &output, DataFormat dataFormat, std::string path)
{
  pTree.put<unsigned>(path+".stages.stagesSize", output.stages.size());
  for (unsigned i=0; i<output.stages.size(); i++){
    createPropertyTree_StageOutput(pTree, output.stages.at(i),
         path+".stages.stages_"+boost::lexical_cast<std::string>(i));
  }
  pTree.put<double>(path+".totalEndTimeLowerBound", output.totalEndTimeLowerBound);
  pTree.put<double>(path+".totalEndTimeUpperBound", output.totalEndTimeUpperBound);
  pTree.put<std::string>(path+".runningMode", getRunningMode(output.runningMode));

  pTree.put<std::string>(path+".integratorOutput.integratorType", getIntegratorType(output.integratorOutput.type));
  pTree.put<std::string>(path+".integratorOutput.integrationOrder", getIntegrationOrder(output.integratorOutput.order));
 // add solution time
  pTree.put<double>(path + ".solutionTime", output.solutionTime);
  
  createPropertyTree_OptimizerOutput(pTree, output.optimizerOutput, path+".optimizerOutput");

  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    createPropertyTree_Output(pTree, output.solutionHistory[i], dataFormat,
                              path+".solutionHistory_"+boost::lexical_cast<std::string>(i));
  }
}

void createPropertyTree_Output(
            std::basic_ostream<typename boost::property_tree::ptree::key_type::value_type> &stream,
      const UserOutput::Output &output, DataFormat dataFormat)
{
  std::string path="debug.output";
  boost::property_tree::ptree pTree;
  
  createPropertyTree_Output(pTree, output, dataFormat, path);
 

  if(dataFormat==XML)
    write_xml(stream, pTree);
  else if(dataFormat==JSON)
    write_json(stream, pTree);
}

static void createPropertyTree_OptimizerOutput(boost::property_tree::ptree &pTree,
                                               const UserOutput::OptimizerOutput &optimizerOutput,
                                               std::string path)
{
  pTree.put<std::string>(path+".optimizerType", getOptimizerType(optimizerOutput.type));
  pTree.put<unsigned>(path+".globalConstraints.globalConstraintsSize", optimizerOutput.globalConstraints.size());
  for (unsigned j=0; j<optimizerOutput.globalConstraints.size(); j++){
    createPropertyTree_ConstraintOutput(pTree, optimizerOutput.globalConstraints.at(j),
      path+".globalConstraints.globalConstraints_"+boost::lexical_cast<std::string>(j));
  }
  pTree.put<unsigned>(path+".linearConstraints.linearConstraintsSize", optimizerOutput.linearConstraints.size());
  for (unsigned j=0; j<optimizerOutput.linearConstraints.size(); j++){
    createPropertyTree_ConstraintOutput(pTree, optimizerOutput.linearConstraints.at(j),
      path+".linearConstraints.linearConstraints_"+boost::lexical_cast<std::string>(j) );
  }
  pTree.put<std::string>(path+".resultFlag", getResultFlag(optimizerOutput.resultFlag));
  pTree.put<double>(path+".intermConstrVio", optimizerOutput.intermConstrVio);

  std::string path1;
  pTree.put<unsigned>(path+".secondOrder.indicesHessian.indicesHessianSize", optimizerOutput.secondOrder.indicesHessian.size());
  std::map<std::string,int>::const_iterator iter;
  for(iter=optimizerOutput.secondOrder.indicesHessian.begin(); iter!=optimizerOutput.secondOrder.indicesHessian.end(); iter++){
    path1=path+".secondOrder.indicesHessian.indicesHessian_"+boost::lexical_cast<std::string>
      (std::distance(optimizerOutput.secondOrder.indicesHessian.begin() , iter));
    pTree.put<std::string>(path1, (*iter).first + "|" + boost::lexical_cast<std::string>((*iter).second));
  }
  std::string vectorString;
  pTree.put<unsigned>(path+".secondOrder.values.valuesSize", optimizerOutput.secondOrder.values.size());
  for (unsigned k=0; k<optimizerOutput.secondOrder.values.size(); k++){
    path1=path+".secondOrder.values.values_"+boost::lexical_cast<std::string>(k);//values is a vector of vectors of doubles
    vectorString.clear();
    for(unsigned l=0; l<optimizerOutput.secondOrder.values.at(k).size(); l++){
      vectorString+=boost::lexical_cast<std::string>((optimizerOutput.secondOrder.values.at(k)).at(l))+",";
      //write all elements into vectorString
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1, vectorString);
  }
  pTree.put<unsigned>(path+".secondOrder.compositeAdjoints.compositeAdjointsSize", optimizerOutput.secondOrder.compositeAdjoints.size());
  for (unsigned k=0; k<optimizerOutput.secondOrder.compositeAdjoints.size(); k++){
    path1=path+".secondOrder.compositeAdjoints.compositeAdjoints_"+boost::lexical_cast<std::string>(k);//compositeAdjoints is a vector of vectors of doubles
    vectorString.clear();
    for(unsigned l=0; l<optimizerOutput.secondOrder.compositeAdjoints.at(k).size(); l++){
      vectorString+=boost::lexical_cast<std::string>((optimizerOutput.secondOrder.compositeAdjoints.at(k)).at(l))+",";
      //write all elements into vectorString
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1, vectorString);
  }
}

static void createPropertyTree_StageOutput(boost::property_tree::ptree &pTree,
                                           const UserOutput::StageOutput &stageOutput, std::string path)
{
  pTree.put<bool>(path+".treatObjective", stageOutput.treatObjective);
  pTree.put<bool>(path+".mapping.fullStateMapping", stageOutput.mapping.fullStateMapping);

  std::string path1;

  pTree.put<unsigned>(path+".mapping.stateNameMapping.stateNameMappingSize", stageOutput.mapping.stateNameMapping.size());
  std::map<std::string,std::string>::const_iterator iter;
  for(iter=stageOutput.mapping.stateNameMapping.begin(); iter!=stageOutput.mapping.stateNameMapping.end(); iter++){
    path1=path+".mapping.stateNameMapping.stateNameMapping_"+boost::lexical_cast<std::string>
      (std::distance(stageOutput.mapping.stateNameMapping.begin() , iter));
    pTree.put<std::string>(path1, (*iter).first + "|" + (*iter).second);
  }

  pTree.put<std::string>(path+".eso.model", stageOutput.eso.model);
#ifdef BUILD_WITH_FMU
  if (stageOutput.eso.type == UserOutput::EsoOutput::FMI) {
	  pTree.put<std::string>(path + ".eso.relativeFmuTolerance", std::to_string(stageOutput.eso.relativeFmuTolerance));
  }
#endif
  pTree.put<std::string>(path+".eso.type", getEsoType(stageOutput.eso.type));

  pTree.put<unsigned>(path+".integrator.durations.durationsSize", stageOutput.integrator.durations.size());
  for(unsigned j=0; j<stageOutput.integrator.durations.size(); j++) {//durations is a vector of structs
    path1=path+".integrator.durations.durations_"+boost::lexical_cast<std::string>(j);
    createPropertyTree_ParameterOutput(pTree, stageOutput.integrator.durations.at(j), path1);
  }
  pTree.put<unsigned>(path+".integrator.parameters.parametersSize", stageOutput.integrator.parameters.size());
  for(unsigned j=0; j<stageOutput.integrator.parameters.size(); j++) {//parameters is a vector of structs
    path1=path+".integrator.parameters.parameters_"+boost::lexical_cast<std::string>(j);
    createPropertyTree_ParameterOutput(pTree, stageOutput.integrator.parameters.at(j), path1);
  }
  pTree.put<unsigned>(path+".integrator.states.statesSize", stageOutput.integrator.states.size());
  for(unsigned j=0; j<stageOutput.integrator.states.size(); j++) {//parameters is a vector of structs
    path1=path+".integrator.states.states_"+boost::lexical_cast<std::string>(j);
    createPropertyTree_StateOutput(pTree, stageOutput.integrator.states.at(j), path1);
  }
  pTree.put<unsigned>(path+".integrator.adjoints.adjointsSize", stageOutput.integrator.adjoints.size());
  for(unsigned j=0; j<stageOutput.integrator.adjoints.size(); j++) {//parameters is a vector of structs
    path1=path+".integrator.adjoints.adjoints_"+boost::lexical_cast<std::string>(j);
    std::string vectorString;
    vectorString.clear();
    for(unsigned k=0; k< stageOutput.integrator.adjoints[j].timePoints.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(stageOutput.integrator.adjoints[j].timePoints[k])+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".timePoints", vectorString);

    vectorString.clear();
    for(unsigned k=0; k<stageOutput.integrator.adjoints[j].values.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(stageOutput.integrator.adjoints[j].values[k])+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".values", vectorString);
  }

  createPropertyTree_ConstraintOutput(pTree, stageOutput.optimizer.objective, path+".optimizer.objective");
  pTree.put<unsigned>(path+".optimizer.nonlinearConstraints.nonlinearConstraintsSize", stageOutput.optimizer.nonlinearConstraints.size());
  for (unsigned j=0; j<stageOutput.optimizer.nonlinearConstraints.size(); j++){
    path1=path+".optimizer.nonlinearConstraints.nonlinearConstraints_"+boost::lexical_cast<std::string>(j);
    createPropertyTree_ConstraintOutput(pTree, stageOutput.optimizer.nonlinearConstraints.at(j), path1);
  }
  pTree.put<unsigned>(path+".optimizer.linearConstraints.linearConstraintsSize", stageOutput.optimizer.linearConstraints.size());
  for (unsigned j=0; j<stageOutput.optimizer.linearConstraints.size(); j++){
    path1=path+".optimizer.linearConstraints.linearConstraints_"+boost::lexical_cast<std::string>(j);
    createPropertyTree_ConstraintOutput(pTree, stageOutput.optimizer.linearConstraints.at(j), path1);
  }
}

static void createPropertyTree_ConstraintOutput(boost::property_tree::ptree &pTree,
                                                const UserOutput::ConstraintOutput &constraintOutput,
                                                std::string path)
{
  pTree.put<std::string>(path+".name", constraintOutput.name);
  pTree.put<double>(path+".lowerBound", constraintOutput.lowerBound);
  pTree.put<double>(path+".upperBound", constraintOutput.upperBound);
  pTree.put<std::string>(path+".constraintType", getConstraintType(constraintOutput.type));

  std::string vectorString;
  std::string path1;
  pTree.put<unsigned>(path+".grids.gridsSize", constraintOutput.grids.size());

  for(unsigned j=0; j<constraintOutput.grids.size(); j++){
    path1=path+".grids.grids_"+boost::lexical_cast<std::string>(j);
    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<constraintOutput.grids.at(j).timePoints.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(constraintOutput.grids.at(j).timePoints.at(k))+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".timePoints", vectorString);

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<constraintOutput.grids.at(j).values.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(constraintOutput.grids.at(j).values.at(k))+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".values", vectorString);

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<constraintOutput.grids.at(j).lagrangeMultiplier.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(constraintOutput.grids.at(j).lagrangeMultiplier.at(k))+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".lagrangeMultiplier", vectorString);
  }
}


static void createPropertyTree_ParameterOutput(boost::property_tree::ptree &pTree,
                                               const UserOutput::ParameterOutput &parameterOutput,
                                               std::string path)
{
  pTree.put<std::string>(path+".name", parameterOutput.name);
  pTree.put<bool>(path+".isOptimizationParameter", parameterOutput.isOptimizationParameter);
  pTree.put<double>(path+".lowerBound", parameterOutput.lowerBound);
  pTree.put<double>(path+".upperBound", parameterOutput.upperBound);
  pTree.put<double>(path+".value", parameterOutput.value);
  pTree.put<std::string>(path+".parameterType", getParameterType(parameterOutput.paramType));
  pTree.put<std::string>(path+".parameterSensitivityType", getParameterSensitivityType(parameterOutput.sensType));
  pTree.put<double>(path+".lagrangeMultiplier", parameterOutput.lagrangeMultiplier);

  std::string path1;
  std::string vectorString;
  pTree.put<unsigned>(path+".grids.gridsSize", parameterOutput.grids.size());
  for (unsigned j=0; j<parameterOutput.grids.size(); j++){//grids is a vector of structs
    path1=path+".grids.grids_"+boost::lexical_cast<std::string>(j);
    pTree.put<unsigned>(path1+".numIntervals", parameterOutput.grids.at(j).numIntervals);
    pTree.put<double>(path1+".duration", parameterOutput.grids.at(j).duration);
    pTree.put<bool>(path1+".hasFreeDuration", parameterOutput.grids.at(j).hasFreeDuration);
    pTree.put<bool>(path1+".isFixed", parameterOutput.grids.at(j).isFixed);
    pTree.put<std::string>(path1+".approximationType", getApproximationType(parameterOutput.grids.at(j).type));

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<parameterOutput.grids.at(j).timePoints.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(parameterOutput.grids.at(j).timePoints.at(k))+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".timePoints", vectorString); //put vectorString into pTree

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<parameterOutput.grids.at(j).values.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(parameterOutput.grids.at(j).values.at(k))+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".values", vectorString);//put vectorString into pTree

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<parameterOutput.grids.at(j).lagrangeMultipliers.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(parameterOutput.grids.at(j).lagrangeMultipliers.at(k))+",";
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1+".lagrangeMultiplier", vectorString);

    std::string path2;
    pTree.put<unsigned>(path1+".firstSensitivities.firstSensitivitiesSize", parameterOutput.grids.at(j).firstSensitivities.size());
    for(unsigned k=0; k<parameterOutput.grids.at(j).firstSensitivities.size(); k++){
      path2=path1+".firstSensitivities.firstSensitivities_"+boost::lexical_cast<std::string>(k);
      createPropertyTree_FirstSensitivityOutput(pTree, parameterOutput.grids.at(j).firstSensitivities.at(k), path2);
    }

  }
}
static void createPropertyTree_FirstSensitivityOutput(boost::property_tree::ptree &pTree,
                                                      const UserOutput::FirstSensitivityOutput &firstSensitivityOutput,
                                                      std::string path)
{
  std::map<int,int>::const_iterator iter;
  std::string path1;
  std::string vectorString;
  pTree.put<unsigned>(path+".mapParamsCols.mapParamsColsSize", firstSensitivityOutput.mapParamsCols.size());
  for(iter=firstSensitivityOutput.mapParamsCols.begin(); iter!=firstSensitivityOutput.mapParamsCols.end(); iter++){
    path1=path+".mapParamsCols.mapParamsCols_"+boost::lexical_cast<std::string>
      (std::distance(firstSensitivityOutput.mapParamsCols.begin(), iter));
    pTree.put<std::string>(path1, boost::lexical_cast<std::string>((*iter).first)
                          + "|" + boost::lexical_cast<std::string>((*iter).second));
  }

  std::map<std::string,int>::const_iterator iter1;
  pTree.put<unsigned>(path+".mapConstrRows.mapConstrRowsSize", firstSensitivityOutput.mapConstrRows.size());
  for(iter1=firstSensitivityOutput.mapConstrRows.begin(); iter1!=firstSensitivityOutput.mapConstrRows.end(); iter1++){
    path1=path+".mapConstrRows.mapConstrRows_"+boost::lexical_cast<std::string>
      (std::distance(firstSensitivityOutput.mapConstrRows.begin(), iter1));
    pTree.put<std::string>(path1, (*iter1).first + "|" + boost::lexical_cast<std::string>((*iter1).second));
  }

  pTree.put<unsigned>(path+".values.valuesSize", firstSensitivityOutput.values.size());
  for(unsigned i=0; i<firstSensitivityOutput.values.size(); i++){
    path1=path+".values.values_"+boost::lexical_cast<std::string>(i);
    vectorString.clear();
    for(unsigned j=0; j<firstSensitivityOutput.values.at(i).size(); j++){
      vectorString+=boost::lexical_cast<std::string>((firstSensitivityOutput.values.at(i)).at(j))+",";
      //firstSensitivityOutput.values is a vector of vectors of doubles "at(i)" returns a vector, "at(j)" returns a double
    }
    if(!vectorString.empty())
      vectorString.erase(vectorString.length()-1);//erase last ","
    pTree.put<std::string>(path1, vectorString);
  }
}
static void createPropertyTree_StateOutput(boost::property_tree::ptree &pTree,
                                           const UserOutput::StateOutput &stateOutput,
                                           std::string path)
{
  pTree.put<std::string>(path+".name", stateOutput.name);
  pTree.put<int>(path+".esoIndex", stateOutput.esoIndex);

  std::string vectorString;
  vectorString.clear();
  for(unsigned j=0; j<stateOutput.grid.timePoints.size(); j++){
    vectorString+=boost::lexical_cast<std::string>(stateOutput.grid.timePoints.at(j))+",";
  }
  if(!vectorString.empty())
    vectorString.erase(vectorString.length()-1);//erase last ","
  pTree.put<std::string>(path+".grid.timePoints", vectorString);

  vectorString.clear();
  for(unsigned j=0; j<stateOutput.grid.values.size(); j++){
    vectorString+=boost::lexical_cast<std::string>(stateOutput.grid.values.at(j))+",";
  }
  if(!vectorString.empty())
    vectorString.erase(vectorString.length()-1);//erase last ","
  pTree.put<std::string>(path+".grid.values", vectorString);
}
//functions to get enums

static std::string getResultFlag(UserOutput::OptimizerOutput::ResultFlag i)
{
  switch (i)
  {
  case UserOutput::OptimizerOutput::OK:
    return "OK";
  case UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS:
    return "TOO_MANY_ITERATIONS";
  case UserOutput::OptimizerOutput::INFEASIBLE:
    return "INFEASIBLE";
  case UserOutput::OptimizerOutput::WRONG_GRADIENTS:
    return "WRONG_GRADIENTS";
  case UserOutput::OptimizerOutput::NOT_OPTIMAL:
    return "NOT_OPTIMAL";
  case UserOutput::OptimizerOutput::FAILED:
    return "FAILED";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "ResultFlag");
  }
}

static std::string getOptimizerType(UserOutput::OptimizerOutput::OptimizerType i)
{
  switch (i)
  {
  case UserOutput::OptimizerOutput::SNOPT:
    return "SNOPT";
  case UserOutput::OptimizerOutput::NPSOL:
	  return "NPSOL";
  case UserOutput::OptimizerOutput::IPOPT:
    return "IPOPT";
  case UserOutput::OptimizerOutput::FILTER_SQP:
	  return "FILTER_SQP";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "OptimizerType in convert user output to xml json");
  }
}

static std::string getIntegrationOrder(UserOutput::IntegratorOutput::IntegrationOrder i)
{
  switch (i)
  {
  case UserOutput::IntegratorOutput::ZEROTH:
    return "ZEROTH";
  case UserOutput::IntegratorOutput::FIRST_FORWARD:
    return "FIRST_FORWARD";
  case UserOutput::IntegratorOutput::FIRST_REVERSE:
    return "FIRST_REVERSE";
  case UserOutput::IntegratorOutput::SECOND_REVERSE:
    return "SECOND_REVERSE";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "IntegrationOrder");
  }
}
static std::string getIntegratorType(UserOutput::IntegratorOutput::IntegratorType i)
{
  switch (i)
  {
  case UserOutput::IntegratorOutput::NIXE:
    return "NIXE";
  case UserOutput::IntegratorOutput::LIMEX:
    return "LIMEX";
  case UserOutput::IntegratorOutput::IDAS:
	  return "IDAS";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "IntegratorType");
  }
}

static std::string getRunningMode(UserOutput::Output::RunningMode i)
{
  switch (i)
  {
  case UserOutput::Output::SIMULATION:
    return "SIMULATION";
  case UserOutput::Output::SINGLE_SHOOTING:
    return "SINGLE_SHOOTING";
  case UserOutput::Output::MULTIPLE_SHOOTING:
    return "MULTIPLE_SHOOTING";
  case UserOutput::Output::SENSITIVITY_INTEGRATION:
    return "SENSITIVITY_INTEGRATION";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "RunningMode");
  }
}
static std::string getApproximationType(UserOutput::ParameterGridOutput::ApproximationType i)
{
  switch (i)
  {
  case UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT:
    return "PIECEWISE_CONSTANT";
  case UserOutput::ParameterGridOutput::PIECEWISE_LINEAR:
    return "PIECEWISE_LINEAR";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "ApproximationType");
  }
}
static std::string getParameterSensitivityType(UserOutput::ParameterOutput::ParameterSensitivityType i)
{
  switch (i)
  {
  case UserOutput::ParameterOutput::FULL:
    return "FULL";
  case UserOutput::ParameterOutput::FRACTIONAL:
    return "FRACTIONAL";
  case UserOutput::ParameterOutput::NO:
    return "NO";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "ParameterSensitivityType");
  }
}
static std::string getParameterType(UserOutput::ParameterOutput::ParameterType i)
{
  switch (i)
  {
  case UserOutput::ParameterOutput::INITIAL:
    return "INITIAL";
  case UserOutput::ParameterOutput::TIME_INVARIANT:
    return "TIME_INVARIANT";
  case UserOutput::ParameterOutput::PROFILE:
    return "PROFILE";
  case UserOutput::ParameterOutput::DURATION:
    return "DURATION";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "ParameterType");
  }
}
static std::string getEsoType(UserOutput::EsoOutput::EsoType i)
{
  switch (i)
  {
  case UserOutput::EsoOutput::JADE:
    return "JADE";
#ifdef BUILD_WITH_FMU
  case UserOutput::EsoOutput::FMI:
	  return "FMI";
#endif

  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "EsoType");
  }
}
static std::string getConstraintType(UserOutput::ConstraintOutput::ConstraintType i)
{
  switch (i)
  {
  case UserOutput::ConstraintOutput::PATH:
    return "PATH";
  case UserOutput::ConstraintOutput::POINT:
    return "POINT";
  case UserOutput::ConstraintOutput::ENDPOINT:
    return "ENDPOINT";
  default:
    throw EnumException(boost::lexical_cast<std::string>(i), "ConstraintType");
  }
}
