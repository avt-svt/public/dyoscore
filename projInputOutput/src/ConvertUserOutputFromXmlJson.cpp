/**
* @file ConvertUserOutputFromXmlJson.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertUserOutputFromXmlJson                                         \n
* =====================================================================\n
* This file contains the definition of the function                    \n
* convertUserOutputFromXmlJson and several subfunctions, that convert a\n
* given Xml/Json-file into UserOutput::Output                          \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski, Adrian Caspari
* @date 18.10.2018
*/
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>
#include "ConvertFromXmlJson.hpp"
#include <vector>
#include <sstream>



/**
* @brief read xml-file into boost::propertyTree::ptree, call several subfunctions
*
* @param[in] UserOutput::Output into which data from the given Xml/Json-file is written
* @param[in] name of the file from which data is read
* @param[in] format of the file from which data is read
*/
static void createOutput(std::string filename, UserOutput::Output &output, DataFormat dataFormat);
/**
* @brief create StageOutput for vector stages
*
* @param[in] reference to the propertyTree that contains all data
* @param[in] UserOutput::StageOutput &stageOutput
* @param[in] std::string path of object in propertyTree
*/
static void createStageOutput(boost::property_tree::ptree &pTree, UserOutput::StageOutput &stageOutput,
                              std::string path);
/**
* @brief create ParameterOutput
*
* @param[in] reference to the propertyTree that contains all data
* @param[in] UserOutput::ParameterOutput &parameterOutput
* @param[in] std::string path of object in propertyTree
*/
static void createParameterOutput(boost::property_tree::ptree &pTree,
                                  UserOutput::ParameterOutput &parameterOutput, std::string path);
/**
* @brief create OptimizerOutput
*
* @param[in] reference to the propertyTree that contains all data
* @param[in] UserOutput::OptimizerOutput &optimizerOutput
* @param[in] std::string path of object in propertyTree
*/
static void createOptimizerOutput(boost::property_tree::ptree &pTree,
                                  UserOutput::OptimizerOutput &optimizerOutput, std::string path);
/**
* @brief create ConstraintOutput
*
* @param[in] reference to the propertyTree that contains all data
* @param[in] UserOutput::ConstraintOutput &constraintOutput
* @param[in] std::string path of object in propertyTree
*/
static void createConstraintOutput(boost::property_tree::ptree &pTree,
                                   UserOutput::ConstraintOutput &constraintOutput, std::string path);
/**
* @brief create SecondOrderOutput
*
* @param[in] reference to the propertyTree that contains all data
* @param[in] UserOutput::SecondOrderOutput &secondOrderOutput
* @param[in] std::string path of object in propertyTree
*/
static void createSecondOrderOutput(boost::property_tree::ptree &pTree,
                                    UserOutput::SecondOrderOutput &secondOrderOutput, std::string path);
/**
* @brief create FirstSensitivityOutput
*
* @param[in] reference to the propertyTree that contains all data
* @param[in] UserOutput::FirstSensitivityOutput &firstSensitivityOutput
* @param[in] std::string path of object in propertyTree
*/
static void createFirstSensitivityOutput(boost::property_tree::ptree &pTree, UserOutput::FirstSensitivityOutput &firstSensitivityOutput,
                                         std::string path);
/**
* @brief create StateOutput
*
* @param[in] reference to the propertyTree that contains all data
* @param[in] UserOutput::StateOutput &stateOutput
* @param[in] std::string path of object in propertyTree
*/
static void createStateOutput(boost::property_tree::ptree &pTree, UserOutput::StateOutput &stateOutput, std::string path);
/**
* @brief vectors of standart data types are stored as strings in the xml-file
* toVector seperates the string again and saves the data in a vector
*
* @param[in] const std::string& s in which the data is stored
* @return std::vector<typename T>
*/

template<typename T> static std::vector<T> toVector(const std::string& s);
/**
* @brief maps of standart data types are stored as strings in the xml-file
* toPair seperates the string again and saves the data in a pair wich can then be used to create a map
*
* @param[in] const std::string& s in which the data is stored
* @return std::pair<T1, T2>
*/
template<typename T1, typename T2> static std::pair<T1, T2> toPair(const std::string& s);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return std::pair<typename T1, typename T2>
*/
static UserOutput::EsoOutput::EsoType getNumEsoType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::ParameterGridOutput::ApproximationType
*/
static UserOutput::ParameterGridOutput::ApproximationType getNumApproximationType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::ParameterOutput::ParameterTyp
*/
static UserOutput::ParameterOutput::ParameterType getNumParameterType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::ParameterOutput::ParameterSensitivityType
*/
static UserOutput::ParameterOutput::ParameterSensitivityType getNumParameterSensitivityType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::ConstraintOutput::ConstraintType
*/
static UserOutput::ConstraintOutput::ConstraintType getNumConstraintType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::IntegratorOutput::IntegratorType
*/
static UserOutput::IntegratorOutput::IntegratorType getNumIntegratorType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::IntegratorOutput::IntegrationOrder
*/
static UserOutput::IntegratorOutput::IntegrationOrder getNumIntegrationOrder(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::OptimizerOutput::OptimizerType
*/
static UserOutput::OptimizerOutput::OptimizerType getNumOptimizerType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::OptimizerOutput::ResultFlag
*/
static UserOutput::OptimizerOutput::ResultFlag getNumResultFlag(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st that represents the enum
* @return UserOutput::Output::RunningMode
*/
static UserOutput::Output::RunningMode getNumRunningMode(std::string st);
/**
* @brief visible function, calls createOutput which writes information from a xml-file into UserOutput::output
*
* @param[in] std::string filename
* @param[in] UserOutput::Output &output
* @param[in] DataFormat dataFormat
*/
void convertUserOutputFromXmlJson(std::string filename, UserOutput::Output &output, DataFormat dataFormat)
{
  createOutput(filename, output, dataFormat);
}


static void createOutput(std::string filename, UserOutput::Output &output, DataFormat dataFormat)
{
  using boost::property_tree::ptree;
  ptree pTree;
  if(dataFormat==XML)
    read_xml(filename, pTree);
  else if(dataFormat==JSON)
    read_json(filename, pTree);

  std::string path="debug.output";

  for (unsigned i=0; i<pTree.get<unsigned>(path+".stages.stagesSize"); i++){
    UserOutput::StageOutput stageOutput;
    createStageOutput(pTree, stageOutput, path+".stages.stages_"+boost::lexical_cast<std::string>(i));
    output.stages.push_back(stageOutput);//filled stageOutput is added to stages vector
  }
  output.totalEndTimeLowerBound=pTree.get<double>(path+".totalEndTimeLowerBound");
  output.totalEndTimeUpperBound=pTree.get<double>(path+".totalEndTimeUpperBound");
  output.runningMode=getNumRunningMode(pTree.get<std::string>(path+".runningMode"));
  createOptimizerOutput(pTree, output.optimizerOutput, path+".optimizerOutput");

  output.integratorOutput.type=getNumIntegratorType(pTree.get<std::string>(path+".integratorOutput.integratorType"));
  output.integratorOutput.order=getNumIntegrationOrder(pTree.get<std::string>(path+".integratorOutput.integrationOrder"));

  // add solution time
  output.solutionTime = pTree.get<double>(path + ".solutionTime");

}
static void createOptimizerOutput(boost::property_tree::ptree &pTree,
                                  UserOutput::OptimizerOutput &optimizerOutput, std::string path)
{
  optimizerOutput.type=getNumOptimizerType(pTree.get<std::string>(path+".optimizerType"));
  optimizerOutput.resultFlag=getNumResultFlag(pTree.get<std::string>(path+".resultFlag"));
  optimizerOutput.intermConstrVio=pTree.get<double>(path+".intermConstrVio");

  UserOutput::ConstraintOutput constraintOutput;
  for (unsigned j=0; j<pTree.get<unsigned>(path+".globalConstraints.globalConstraintsSize"); j++){
    createConstraintOutput(pTree, constraintOutput,
      path+".globalConstraints.globalConstraints_"+boost::lexical_cast<std::string>(j));
    optimizerOutput.globalConstraints.push_back(constraintOutput);
  }
  for (unsigned j=0; j<pTree.get<unsigned>(path+".linearConstraints.linearConstraintsSize"); j++){
    createConstraintOutput(pTree, constraintOutput,
      path+".linearConstraints.linearConstraints_"+boost::lexical_cast<std::string>(j));
    optimizerOutput.linearConstraints.push_back(constraintOutput);
  }
  createSecondOrderOutput(pTree, optimizerOutput.secondOrder, path+".secondOrder");

}
static void createSecondOrderOutput(boost::property_tree::ptree &pTree,
                                    UserOutput::SecondOrderOutput &secondOrderOutput, std::string path)
{
  std::string path1;
  secondOrderOutput.indicesHessian.clear();
  for (unsigned i=0; i<pTree.get<unsigned>(path+".indicesHessian.indicesHessianSize"); i++){
    path1=path+".indicesHessian.indicesHessian_"+boost::lexical_cast<std::string>(i);
    secondOrderOutput.indicesHessian.insert(toPair<std::string,int>(pTree.get<std::string>(path1)));
  }
  std::vector<double> helpVector;
  for(unsigned i=0; i<pTree.get<unsigned>(path+".values.valuesSize");i++){
    path1=path+".values.values_"+boost::lexical_cast<std::string>(i);
    helpVector=toVector<double>(pTree.get<std::string>(path1));
    secondOrderOutput.values.push_back(helpVector);
  }
  for(unsigned i=0; i<pTree.get<unsigned>(path+".compositeAdjoints.compositeAdjointsSize");i++){
    path1=path+".compositeAdjoints.compositeAdjoints_"+boost::lexical_cast<std::string>(i);
    helpVector=toVector<double>(pTree.get<std::string>(path1));
    secondOrderOutput.compositeAdjoints.push_back(helpVector);
  }
}
static void createConstraintOutput(boost::property_tree::ptree &pTree,
                                   UserOutput::ConstraintOutput &constraintOutput, std::string path)
{
  constraintOutput.name=pTree.get<std::string>(path+".name");
  constraintOutput.lowerBound=pTree.get<double>(path+".lowerBound");
  constraintOutput.upperBound=pTree.get<double>(path+".upperBound");
  constraintOutput.type=getNumConstraintType(pTree.get<std::string>(path+".constraintType"));
  constraintOutput.grids.clear();
  std::string path1;
  UserOutput::ConstraintGridOutput constraintGridOutput;
  for(unsigned i=0; i<pTree.get<unsigned>(path+".grids.gridsSize"); i++){
    path1=path+".grids.grids_"+boost::lexical_cast<std::string>(i);
    constraintGridOutput.timePoints=toVector<double>(pTree.get<std::string>(path1+".timePoints"));
    constraintGridOutput.values=toVector<double>(pTree.get<std::string>(path1+".values"));
    constraintGridOutput.lagrangeMultiplier=toVector<double>(pTree.get<std::string>(path1+".lagrangeMultiplier"));
    constraintOutput.grids.push_back(constraintGridOutput);
  }
}
template<typename T1, typename T2>
static std::pair<T1, T2> toPair(const std::string& s)
{
  std::pair <T1,T2> result;//pairs are stored as "first|second"
  std::stringstream ss(s);
  std::string item;
  std::getline(ss, item, '|');//reads string ss until "|" into item
  result.first=boost::lexical_cast<T1>(item);
  std::getline(ss, item, '|');
  result.second=boost::lexical_cast<T2>(item);
  return result;
}
template<typename T>
static std::vector<T> toVector(const std::string& s)
{
  std::vector<T> result;//vectors are stored as "vector.at(0),vector.at(1),..."
  std::stringstream ss(s);
  std::string item;
  while(std::getline(ss, item, ',')){
    result.push_back(boost::lexical_cast<T>(item));
  }
  //reads part of string ss until "," into item
  return result;
}
void createParameterOutput(boost::property_tree::ptree &pTree,
                           UserOutput::ParameterOutput &parameterOutput, std::string path)
{
  parameterOutput.name=pTree.get<std::string>(path+".name");
  parameterOutput.lowerBound=pTree.get<double>(path+".lowerBound");
  parameterOutput.upperBound=pTree.get<double>(path+".upperBound");
  parameterOutput.value=pTree.get<double>(path+".value");
  parameterOutput.isOptimizationParameter=pTree.get<bool>(path+".isOptimizationParameter");
  parameterOutput.paramType=getNumParameterType(pTree.get<std::string>(path+".parameterType"));
  parameterOutput.sensType=getNumParameterSensitivityType(pTree.get<std::string>
    (path+".parameterSensitivityType"));
  parameterOutput.lagrangeMultiplier = pTree.get<double>(path+".lagrangeMultiplier");

  std::string path1;
  std::string path2;
  for (unsigned j=0; j<pTree.get<unsigned>(path+".grids.gridsSize"); j++){
    UserOutput::ParameterGridOutput parameterGridOutput;
    path1=path+".grids.grids_"+boost::lexical_cast<std::string>(j);
    parameterGridOutput.numIntervals=pTree.get<unsigned>(path1+".numIntervals");
    parameterGridOutput.duration=pTree.get<double>(path1+".duration");
    parameterGridOutput.hasFreeDuration=pTree.get<bool>(path1+".hasFreeDuration");
    parameterGridOutput.isFixed=pTree.get<bool>(path1+".isFixed");
    parameterGridOutput.type=getNumApproximationType(pTree.get<std::string>(path1+".approximationType"));
    parameterGridOutput.timePoints=toVector<double>(pTree.get<std::string>(path1+".timePoints"));
    parameterGridOutput.values=toVector<double>(pTree.get<std::string>(path1+".values"));
    parameterGridOutput.lagrangeMultipliers=toVector<double>(pTree.get<std::string>(path1+".lagrangeMultiplier"));
    for(unsigned k=0; k<pTree.get<unsigned>(path1+".firstSensitivities.firstSensitivitiesSize"); k++){
      path2=path1+".firstSensitivities.firstSensitivities_"+boost::lexical_cast<std::string>(k);
      UserOutput::FirstSensitivityOutput firstSensitivityOutput;
      createFirstSensitivityOutput(pTree, firstSensitivityOutput, path2);
      parameterGridOutput.firstSensitivities.push_back(firstSensitivityOutput);
    }
    parameterOutput.grids.push_back(parameterGridOutput);//add filled parameterGridOutput to vector grids
  }
}
static void createFirstSensitivityOutput(boost::property_tree::ptree &pTree, UserOutput::FirstSensitivityOutput &firstSensitivityOutput, std::string path)
{
  std::string path1;
  std::string vectorString;
  for (unsigned i=0; i<pTree.get<unsigned>(path+".mapParamsCols.mapParamsColsSize"); i++){
    path1=path+".mapParamsCols.mapParamsCols_"+boost::lexical_cast<std::string>(i);
    firstSensitivityOutput.mapParamsCols.insert(toPair<int,int>
      (pTree.get<std::string>(path1)));
  }
  for(unsigned i=0; i<pTree.get<unsigned>(path+".mapConstrRows.mapConstrRowsSize"); i++){
    path1=path+".mapConstrRows.mapConstrRows_"+boost::lexical_cast<std::string>(i);
    firstSensitivityOutput.mapConstrRows.insert(toPair<std::string,int>
      (pTree.get<std::string>(path1)));
  }
  std::vector<double> helpVector;
  for(unsigned i=0; i<pTree.get<unsigned>(path+".values.valuesSize"); i++){
    path1=path+".values.values_"+boost::lexical_cast<std::string>(i);
    vectorString=pTree.get<std::string>(path1);
    helpVector=toVector<double>(vectorString);
    firstSensitivityOutput.values.push_back(helpVector);
  }
}
void createStageOutput(boost::property_tree::ptree &pTree, UserOutput::StageOutput &stageOutput, std::string path)
{
  stageOutput.treatObjective=pTree.get<bool>(path+".treatObjective");
  stageOutput.mapping.fullStateMapping=pTree.get<bool>(path+".mapping.fullStateMapping");
  std::string path1;
  for (unsigned i=0; i<pTree.get<unsigned>(path+".mapping.stateNameMapping.stateNameMappingSize"); i++){
    path1=path+".mapping.stateNameMapping.stateNameMapping_"+boost::lexical_cast<std::string>(i);
    stageOutput.mapping.stateNameMapping.insert(toPair<std::string,std::string>
      (pTree.get<std::string>(path1)));
  }

  stageOutput.eso.model=pTree.get<std::string>(path+".eso.model");
#ifdef BUILD_WITH_FMU
  if (stageOutput.eso.type == UserOutput::EsoOutput::FMI) {
	  stageOutput.eso.relativeFmuTolerance = std::stod(pTree.get<std::string>(path + ".eso.relativeFmuTolerance"));
  }
#endif
  stageOutput.eso.type=getNumEsoType(pTree.get<std::string>(path+".eso.type"));

  for (unsigned j=0; j<pTree.get<unsigned>(path+".integrator.durations.durationsSize"); j++){
    UserOutput::ParameterOutput parameterOutput;
    path1=path+".integrator.durations.durations_"+boost::lexical_cast<std::string>(j);
    createParameterOutput(pTree, parameterOutput, path1);
    stageOutput.integrator.durations.push_back(parameterOutput);
  }

  for (unsigned j=0; j<pTree.get<unsigned>(path+".integrator.parameters.parametersSize"); j++){
    UserOutput::ParameterOutput parameterOutput;
    path1=path+".integrator.parameters.parameters_"+boost::lexical_cast<std::string>(j);
    createParameterOutput(pTree, parameterOutput, path1);
    stageOutput.integrator.parameters.push_back(parameterOutput);
  }
  for (unsigned j=0; j<pTree.get<unsigned>(path+".integrator.states.statesSize"); j++){
    UserOutput::StateOutput stateOutput;
    path1=path+".integrator.states.states_"+boost::lexical_cast<std::string>(j);
    createStateOutput(pTree, stateOutput, path1);
    stageOutput.integrator.states.push_back(stateOutput);
  }

  createConstraintOutput(pTree, stageOutput.optimizer.objective, path+".optimizer.objective");

  UserOutput::ConstraintOutput constraintOutput;
  for (unsigned j=0; j<pTree.get<unsigned>(path+".optimizer.nonlinearConstraints.nonlinearConstraintsSize"); j++){
    path1=path+".optimizer.nonlinearConstraints.nonlinearConstraints_"+boost::lexical_cast<std::string>(j);
    createConstraintOutput(pTree, constraintOutput, path1);
    stageOutput.optimizer.nonlinearConstraints.push_back(constraintOutput);
  }
  for (unsigned j=0; j<pTree.get<unsigned>(path+".optimizer.linearConstraints.linearConstraintsSize"); j++){
    path1=path+".optimizer.linearConstraints.linearConstraints_"+boost::lexical_cast<std::string>(j);
    createConstraintOutput(pTree, constraintOutput, path1);
    stageOutput.optimizer.linearConstraints.push_back(constraintOutput);
  }
}
static void createStateOutput(boost::property_tree::ptree &pTree, UserOutput::StateOutput &stateOutput, std::string path)
{
  stateOutput.name=pTree.get<std::string>(path+".name");
  stateOutput.esoIndex=pTree.get<int>(path+".esoIndex");

  stateOutput.grid.timePoints=toVector<double>(pTree.get<std::string>(path+".grid.timePoints"));
  stateOutput.grid.values=toVector<double>(pTree.get<std::string>(path+".grid.values"));
}
//get types for strings representing enums
static UserOutput::EsoOutput::EsoType getNumEsoType(std::string st)
{
	if (st == "JADE")
		return UserOutput::EsoOutput::JADE;
#ifdef BUILD_WITH_FMU
	else if (st == "FMI")
		return UserOutput::EsoOutput::FMI;
#endif

  else
    throw EnumException(st, "EsoType");
}
static UserOutput::ParameterGridOutput::ApproximationType getNumApproximationType(std::string st)
{
  if(st=="PIECEWISE_CONSTANT")
    return UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT;
  else if(st=="PIECEWISE_LINEAR")
    return UserOutput::ParameterGridOutput::PIECEWISE_LINEAR;
  else
    throw EnumException(st, "ApproximationType");
}
static UserOutput::ParameterOutput::ParameterType getNumParameterType(std::string st)
{
  if(st=="INITIAL")
    return UserOutput::ParameterOutput::INITIAL;
  else if(st=="TIME_INVARIANT")
    return UserOutput::ParameterOutput::TIME_INVARIANT;
  else if(st=="PROFILE")
    return UserOutput::ParameterOutput::PROFILE;
  else if(st=="DURATION")
    return UserOutput::ParameterOutput::DURATION;
  else
    throw EnumException(st, "ParameterType");

}
static UserOutput::ParameterOutput::ParameterSensitivityType getNumParameterSensitivityType(std::string st)
{
  if(st=="FULL")
    return UserOutput::ParameterOutput::FULL;
  else if(st=="FRACTIONAL")
    return UserOutput::ParameterOutput::FRACTIONAL;
  else if(st=="NO")
    return UserOutput::ParameterOutput::NO;
  else
    throw EnumException(st, "ParameterSensitivityType");
}
static UserOutput::ConstraintOutput::ConstraintType getNumConstraintType(std::string st)
{
  if(st=="PATH")
    return UserOutput::ConstraintOutput::PATH;
  else if(st=="POINT")
    return UserOutput::ConstraintOutput::POINT;
  else if(st=="ENDPOINT")
    return UserOutput::ConstraintOutput::ENDPOINT;
  else
    throw EnumException(st, "ConstraintType");
}
static UserOutput::IntegratorOutput::IntegratorType getNumIntegratorType(std::string st)
{
  if(st=="NIXE")
    return UserOutput::IntegratorOutput::NIXE;
  else if(st=="LIMEX")
    return UserOutput::IntegratorOutput::LIMEX;
  else if (st == "IDAS")
	  return UserOutput::IntegratorOutput::IDAS;
  else
    throw EnumException(st, "IntegratorType");
}
static UserOutput::IntegratorOutput::IntegrationOrder getNumIntegrationOrder(std::string st)
{
  if(st=="ZEROTH")
    return UserOutput::IntegratorOutput::ZEROTH;
  else if(st=="FIRST_FORWARD")
    return UserOutput::IntegratorOutput::FIRST_FORWARD;
  else if(st=="FIRST_REVERSE")
    return UserOutput::IntegratorOutput::FIRST_REVERSE;
  else if(st=="SECOND_REVERSE")
    return UserOutput::IntegratorOutput::SECOND_REVERSE;
  else
    throw EnumException(st, "IntegrationOrder");
}
static UserOutput::OptimizerOutput::OptimizerType getNumOptimizerType(std::string st)
{
  if(st=="SNOPT")
    return UserOutput::OptimizerOutput::SNOPT;
  else if(st=="IPOPT")
    return UserOutput::OptimizerOutput::IPOPT;
  else
    throw EnumException(st, "OptimizerType");
}
static UserOutput::OptimizerOutput::ResultFlag getNumResultFlag(std::string st)
{
  if(st=="OK")
    return UserOutput::OptimizerOutput::OK;
  else if(st=="TOO_MANY_ITERATIONS")
    return UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS;
  else if(st=="INFEASIBLE")
    return UserOutput::OptimizerOutput::INFEASIBLE;
  else if(st=="WRONG_GRADIENTS")
    return UserOutput::OptimizerOutput::WRONG_GRADIENTS;
  else if(st=="NOT_OPTIMAL")
    return UserOutput::OptimizerOutput::NOT_OPTIMAL;
  else if(st=="FAILED")
    return UserOutput::OptimizerOutput::FAILED;
  else
    throw EnumException(st, "ResultFlag");
}
static UserOutput::Output::RunningMode getNumRunningMode(std::string st)
{
  if(st=="SIMULATION")
    return UserOutput::Output::SIMULATION;
  else if(st=="SINGLE_SHOOTING")
    return UserOutput::Output::SINGLE_SHOOTING;
  else if(st=="MULTIPLE_SHOOTING")
    return UserOutput::Output::MULTIPLE_SHOOTING;
  else if(st=="SENSITIVITY_INTEGRATION")
    return UserOutput::Output::SENSITIVITY_INTEGRATION;
  else
    throw EnumException(st, "RunningMode");
}
