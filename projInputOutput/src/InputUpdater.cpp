/**
* @file InputUpdater.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the definition of member functions of the         \n
* InputUpdater class                                                   \n
* =====================================================================\n
* @author Tjalf Hoffmann, Fady Assassa, Adrian Caspari
* @date 18.10.2018
* @sa Input.hpp EsoInput.hpp MetaDataInput.hpp IntegratorInput.hpp OptimizerInput.hpp
*     MetaDataOutput.hpp DyosOutput.hpp
*/

#include "InputUpdater.hpp"
#include "InputOutputConversions.hpp"
#include <set>

/**
* @brief standard constructor
*/
InputUpdater::InputUpdater()
{
}

/**
* @brief destructor
*/
InputUpdater::~InputUpdater()
{
}

/**
* @brief write gridpoints, duration and values into new ParameterizationGridInput
*
* @param[in] originalInput input struct used for the previous dyos run
* @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous doys run
* @param[in] durationToIndex has has a mapping of final times to control indices, which help to determine
* the correct grid series.
* @param[in] esoIndex of the control
* @return input struct updated by the output
* @todo this function needs restructuring
*/
vector<FactoryInput::ParameterizationGridInput> InputUpdater::updateParameterizationGridInput(
                           const vector<FactoryInput::ParameterizationGridInput> &originalInput,
                           const vector<DyosOutput::ParameterGridOutput> &optimizationResult,
                           const std::map<double, vector<unsigned> > durationToIndex,
                           const unsigned esoIndex,
                           const bool keepRetardedParameters) const
{
  vector<FactoryInput::ParameterizationGridInput> updatedInput = originalInput;
  // delete entries
  for(unsigned i=0; i<updatedInput.size(); i++){
    updatedInput[i].timePoints.clear();
    updatedInput[i].values.clear();
  }

  vector<double> absFTimes = getAbsoluteDurations(optimizationResult);

  double gridDuration = 0; // variable that calculates the grid duration of the specific control
  // without retarded grid coming from structure detection.
  unsigned updatedGridIndex = 0;
  map<double, vector<unsigned> > durationToIndexCopy = durationToIndex;
  map<double, vector<unsigned> >::iterator it = durationToIndexCopy.begin();
  bool isFirst = true;
  //optimizationResult and durationToIndex have the same size
  // so iterator it corresponds with loop index i
  // assuming a correct sorting of durationToIndex
  for(unsigned i=0; i<optimizationResult.size(); i++){
    vector<double> value, timePoint;
    if(keepRetardedParameters){
      //if isFirst is set to true, then grids containing retarded parameters will not be merged.
      isFirst = true;
    }
    
    removePossibleRetardedParameters(optimizationResult[i], isFirst, timePoint, value);
    updatedInput[updatedGridIndex].values.insert(updatedInput[updatedGridIndex].values.end(),
      value.begin(),
      value.end());

    for(unsigned j=0; j<timePoint.size(); j++){
      timePoint[j] = timePoint[j] * optimizationResult[i].duration + gridDuration;
    }
    updatedInput[updatedGridIndex].timePoints.insert(updatedInput[updatedGridIndex].timePoints.end(),
      timePoint.begin(),
      timePoint.end());

    gridDuration += optimizationResult[i].duration;
    updatedInput[updatedGridIndex].duration = gridDuration;

    if(it != durationToIndexCopy.end()){
      if( find(it->second.begin(), it->second.end(), esoIndex) != it->second.end()){
        // scale with correct end time
        for(unsigned j=0; j<updatedInput[updatedGridIndex].timePoints.size(); j++){
          updatedInput[updatedGridIndex].timePoints[j] /= gridDuration;
        }
        // we have to reinsert the last point, because we always delete it.
        updatedInput[updatedGridIndex].timePoints.push_back(1.0);
        // in case of linear profiles we also need to include the last value
        if(optimizationResult[i].gridPoints.size() == optimizationResult[i].values.size()){
          updatedInput[updatedGridIndex].values.push_back(optimizationResult[i].values.back());
        }
        gridDuration = 0;
        updatedGridIndex++;
        isFirst = true;
      }//if (it!=durationToIndexCopy.end())
    }//if(find(...
    it++; // we iterate through durationToIndex map until we find the esoIndex of the control
  }

  return updatedInput;
}

/**
* @brief update a ParameterInput
*
* @param[in] originalInput input struct used for the previous dyos run
* @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous doys run
* @param[in] durationToIndex has has a mapping of final times to control indices, which help to determine
* the correct grid series.
* @return input struct updated by the output
*/
FactoryInput::ParameterInput InputUpdater::updateParameterInput(
                           const FactoryInput::ParameterInput &originalInput,
                           const DyosOutput::ParameterOutput &optimizationResult,
                           const std::map<double, vector<unsigned> > durationToIndex) const
{
  FactoryInput::ParameterInput updatedInput = originalInput;

  if(updatedInput.type != FactoryInput::ParameterInput::DURATION){
    assert(updatedInput.esoIndex == optimizationResult.esoIndex);
  }
  updatedInput.value = optimizationResult.value;
  
  const unsigned numGrids = updatedInput.grids.size();
  //optimizationResult.grids is never empty, and so may have more elements than input.grids
  assert(optimizationResult.grids.size() >= numGrids);
  if(originalInput.grids.size() > 0){
    std::vector<FactoryInput::ParameterizationGridInput> grids = originalInput.grids;
    bool keepRetardedParameters = false;
    if(originalInput.type == FactoryInput::ParameterInput::CONTINUOUS_CONTROL){
      keepRetardedParameters = true;
    }
    updatedInput.grids = updateParameterizationGridInput(grids,
                                                         optimizationResult.grids,
                                                         durationToIndex,
                                                         updatedInput.esoIndex,
                                                         keepRetardedParameters);
  }
  return updatedInput;
}

/**
* @brief update an OptimizerMetaDataInputStage
*
* @param[in] originalInput input struct used for the previous dyos run
* @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous doys run
* @return input struct updated by the output
*/
FactoryInput::OptimizerMetaDataInput InputUpdater::updateOptimizerMetaDataInputStage(
                            const FactoryInput::OptimizerMetaDataInput &originalInput,
                            const DyosOutput::OptimizerStageOutput &optimizationResult) const
{
  FactoryInput::OptimizerMetaDataInput updatedInput = originalInput;
  updatedInput.objective.lagrangeMultiplier = optimizationResult.objective.lagrangeMultiplier;
  for(unsigned i=0; i<updatedInput.constraints.size(); i++){
    FactoryInput::ConstraintInput &updatedConstraint = updatedInput.constraints[i];
    updatedConstraint.lagrangeMultiplierVector.clear();
    for(unsigned j=0; j<optimizationResult.nonLinearConstraints.size(); j++)
    {
      DyosOutput::ConstraintOutput constraintResult = optimizationResult.nonLinearConstraints[j];
      if(updatedConstraint.esoIndex == constraintResult.esoIndex){
        switch(updatedConstraint.type){
          case FactoryInput::ConstraintInput::ENDPOINT:
            if(constraintResult.timePoint == 1.0){
              updatedConstraint.lagrangeMultiplier = constraintResult.lagrangeMultiplier;
            }
            break;
          case FactoryInput::ConstraintInput::POINT:
            if(fabs(updatedConstraint.timePoint - constraintResult.timePoint) < 1e-10){
              updatedConstraint.lagrangeMultiplier = constraintResult.lagrangeMultiplier;
            }
            break;
          case FactoryInput::ConstraintInput::PATH:
            updatedConstraint.lagrangeMultiplierVector.push_back(constraintResult.lagrangeMultiplier);
            break;
          default:
            assert(false);
        }
      }
    }
  }
  return updatedInput;
}

map<double, vector<unsigned> > InputUpdater::getDurationToIndex(
                                  const FactoryInput::IntegratorMetaDataInput &originalInput) const
{
  map<double, vector<unsigned> > durationToIndex;
  
  const unsigned numControls = originalInput.controls.size();
  // determine the original structure such that we remove retarded parameters.
  // the grids have a fixed order. Thus, we can assign for each absolute final time
  // a certain control variable.
  // this can go into a function
  for(unsigned i=0; i<numControls; i++){
    const unsigned numGrids = originalInput.controls[i].grids.size();
    const unsigned esoIndex =  originalInput.controls[i].esoIndex;
    double elapsedTime = 0;
    for(unsigned j=0; j<numGrids; j++){
      const double fTime = originalInput.controls[i].grids[j].duration;
      // accessing map with a double might be problematic. solve this.
      // first iterate through map an check if there is a close value
      map<double, vector<unsigned> >::iterator it;
      bool isSmallValue = false;
      for(it = durationToIndex.begin(); it != durationToIndex.end(); it++){
        double compare = fabs(it->first - fTime - elapsedTime);
        if(compare < 1e-6){
          isSmallValue = true;
          break;
        }
      }
      if(isSmallValue){
        it->second.push_back(esoIndex);
      }else{
        durationToIndex[fTime + elapsedTime ].push_back(esoIndex);
      }
      elapsedTime += fTime;
    }
  }
  
  return durationToIndex;
}

/**
* @brief update an IntegratorMetaDataInput for a single stage
*
* Update all initials, controls and the final time respectively
* @param[in] originalInput input struct used for the previous dyos run
* @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous doys run
* @return input struct updated by the output
*/
FactoryInput::IntegratorMetaDataInput InputUpdater::updateIntegratorMetaDataInputStage(
                            const FactoryInput::IntegratorMetaDataInput &originalInput,
                            const DyosOutput::IntegratorStageOutput &optimizationResult) const
{
  assert(originalInput.stages.empty());
  FactoryInput::IntegratorMetaDataInput updatedStage = originalInput;

  map<double, vector<unsigned> > empty;
  updatedStage.duration = updateParameterInput(originalInput.duration,
                                                optimizationResult.durations.front(), empty);

  const unsigned numInitials = originalInput.initials.size();
  assert(numInitials <= optimizationResult.initials.size());
  for(unsigned i=0; i<numInitials; i++){
    const unsigned esoIndex = originalInput.initials[i].esoIndex;
    //search for parameter in output
    for(unsigned j=0; j<numInitials; j++){
      if(esoIndex == optimizationResult.initials[j].esoIndex){
        updatedStage.initials[i] = updateParameterInput(originalInput.initials[i],
                                                        optimizationResult.initials[j],
                                                        empty);
        break;
      }
    }
  }
  

  map<double, vector<unsigned> > durationToIndex = getDurationToIndex(originalInput);
  
  const unsigned numControls = originalInput.controls.size();
  assert(numControls <= optimizationResult.parameters.size());
  for(unsigned i=0; i<numControls; i++){
    const unsigned esoIndex = originalInput.controls[i].esoIndex;
    for(unsigned j=0; j<optimizationResult.parameters.size(); j++){
      if(esoIndex == optimizationResult.parameters[j].esoIndex){
        updatedStage.controls[i] = updateParameterInput(originalInput.controls[i],
                                                        optimizationResult.parameters[j],
                                                        durationToIndex);
      }
    }
  }

  // map the user grid of the previous solution to the user grid of the current solution
  for(unsigned i=0; i<originalInput.userGrid.size(); i++){
    updatedStage.userGrid[i] *= originalInput.duration.value;
  }
  map<double, vector<unsigned> >::iterator it;
  vector< vector<double> > separatedUserGrid;
  int sUGiterator = -1;
  unsigned wki = 0;
  double prevFTime = 0;
  for(it = durationToIndex.begin(); it != durationToIndex.end(); ++it){
    separatedUserGrid.resize(separatedUserGrid.size() + 1);
    sUGiterator++;
    const double fTime = it->first;
    const double duration = fTime - prevFTime;
    while(wki<originalInput.userGrid.size()){
      if(updatedStage.userGrid[wki] <= fTime){
        separatedUserGrid[sUGiterator].push_back((updatedStage.userGrid[wki] - prevFTime)/duration);
      }else{
        break;
      }
      wki++;
    }
    prevFTime = fTime;
  }

  updatedStage.userGrid.clear();
  vector<double> absFtimes;
  if(!optimizationResult.parameters.empty()){
    absFtimes = getAbsoluteDurations(optimizationResult.parameters[0].grids);
  }
  prevFTime = 0;
  for(unsigned i=0; i<absFtimes.size(); i++){
    for(unsigned j=0;j<separatedUserGrid[i].size(); j++){
      double durs = absFtimes[i] - prevFTime;
      separatedUserGrid[i][j] = separatedUserGrid[i][j] * durs + prevFTime;
      separatedUserGrid[i][j] /= optimizationResult.durations[0].value;
    }
    updatedStage.userGrid.insert(updatedStage.userGrid.end(),separatedUserGrid[i].begin(),separatedUserGrid[i].end());
    prevFTime = absFtimes[i];
  }


  return updatedStage;
}

/**
* @brief update an IntegratorMetaDataInput for either single stage or multi stage structs
* @param[in] originalInput input struct used for the previous dyos run
* @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous doys run
* @return input struct updated by the output
*/
FactoryInput::IntegratorMetaDataInput InputUpdater::updateIntegratorMetaDataInput(
                        const FactoryInput::IntegratorMetaDataInput &originalInput,
                        const std::vector<DyosOutput::StageOutput> &optimizationResult)const
{
  FactoryInput::IntegratorMetaDataInput updatedInput = originalInput;
  if(originalInput.stages.empty()){
    assert(optimizationResult.size() == 1);
    updatedInput = updateIntegratorMetaDataInputStage(originalInput,
                                                      optimizationResult.front().integrator);
  }
  else{
    const unsigned numStages = originalInput.stages.size();
    assert(numStages == optimizationResult.size());
    for(unsigned i=0; i<numStages; i++){
      updatedInput.stages[i] = updateIntegratorMetaDataInputStage(originalInput.stages[i],
                                                                  optimizationResult[i].integrator);
    }
  }
  
  return updatedInput;
}

/**
* @brief update an OptimizerMetaDataInput for either single stage or multi stage structs
* @param[in] originalInput input struct used for the previous dyos run
* @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous doys run
* @return input struct updated by the output
*/
FactoryInput::OptimizerMetaDataInput InputUpdater::updateOptimizerMetaDataInput(
                         const FactoryInput::OptimizerMetaDataInput &originalInput,
                         const std::vector<DyosOutput::StageOutput> &optimizationResult) const
{
  FactoryInput::OptimizerMetaDataInput updatedInput = originalInput;
  if(originalInput.stages.empty()){
    assert(optimizationResult.size() == 1);
    updatedInput = updateOptimizerMetaDataInputStage(originalInput,
                                                     optimizationResult.front().optimizer);
  }
  else{
    const unsigned numStages = originalInput.stages.size();
    assert(numStages == optimizationResult.size());
    for(unsigned i=0; i<numStages; i++){
      updatedInput.stages[i] = updateOptimizerMetaDataInputStage(originalInput.stages[i],
                                                                 optimizationResult[i].optimizer);
    }
  }
  return updatedInput;
}

ProblemInput InputUpdater::updateMultipleShootingInitials(const ProblemInput &originalInput,
                                                          const DyosOutput::Output &simulationResult) const
{
  assert(originalInput.type == ProblemInput::OPTIMIZATION);
  assert(simulationResult.stageOutput.size() == originalInput.optimizer.metaDataInput.stages.size());
  ProblemInput updatedInput = originalInput;
  for(unsigned i=1; i<simulationResult.stageOutput.size(); i++){
    std::vector<FactoryInput::ParameterInput> currentStageInitials;
    currentStageInitials = originalInput.optimizer.integratorInput.metaDataInput.stages[i].initials;
    std::vector<DyosOutput::StateOutput> currentStageOutStates;
    currentStageOutStates = simulationResult.stageOutput[i].integrator.states;
    for(unsigned j=0; j<currentStageInitials.size(); j++){
      //search corresponding state and set the value
      bool found = false;
      for(unsigned k=0; k<currentStageOutStates.size(); k++){
        if(currentStageInitials[j].esoIndex == currentStageOutStates[k].esoIndex){
          currentStageInitials[j].value = currentStageOutStates[k].grid.values.front();
          found = true;
          break;
        }//if
      }// for k
      assert(found);
    }// for j
    updatedInput.optimizer.integratorInput.metaDataInput.stages[i].initials = currentStageInitials;
  }// for i
  return updatedInput;
}
/**
* @brief update the ProblemInput of the previous dyos run using the result struct 
*
* Update all initials, controls and the final time respectively
* @param[in] originalInput input struct used for the previous dyos run
* @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous dyos run
* @return input struct updated by the output
*/
ProblemInput InputUpdater::updateProblemInput(const ProblemInput &originalInput,
                                              const DyosOutput::Output &optimizationResult) const
{
  ProblemInput updatedInput = originalInput;
  switch(updatedInput.type){
    case ProblemInput::SIMULATION:
      updatedInput.integrator.metaDataInput = updateIntegratorMetaDataInput(
                                                          originalInput.integrator.metaDataInput,
                                                          optimizationResult.stageOutput);
      break;
    case ProblemInput::OPTIMIZATION:
      {//block to make variable integratorMetaDataInput local within the case
        FactoryInput::IntegratorMetaDataInput integratorMetaDataInput 
                    = originalInput.optimizer.integratorInput.metaDataInput;
        //convert all controls to continuous controls
        for(unsigned i=0; i<integratorMetaDataInput.stages.size(); i++){
          if(originalInput.optimizer.metaDataInput.stages[i].structureDetection.createContinuousGrids == true){
            for(unsigned j=0; j<integratorMetaDataInput.stages[i].controls.size(); j++){
              if(integratorMetaDataInput.stages[i].controls[j].type 
                                == FactoryInput::ParameterInput::CONTROL){
                integratorMetaDataInput.stages[i].controls[j].type  = FactoryInput::ParameterInput::CONTINUOUS_CONTROL;
              }
            }
          }
        }
        updatedInput.optimizer.integratorInput.metaDataInput = updateIntegratorMetaDataInput(
                                                     integratorMetaDataInput,
                                                     optimizationResult.stageOutput);
        updatedInput.optimizer.metaDataInput = updateOptimizerMetaDataInput(
                                                     originalInput.optimizer.metaDataInput,
                                                     optimizationResult.stageOutput);
      }
      break;
    default:
      assert(false);
  }
  
  return updatedInput;
}

/** @brief calculates a vector of absolute final times based on the grid durations 
*
*  @param[in] optimizationResult output for the corresponding input produced 
*                               by the previous doys run
*  @retun vector of all final times as absolute value
*/
vector<double> InputUpdater::getAbsoluteDurations(const vector<DyosOutput::ParameterGridOutput> 
                                                       &optimizationResult)const
{
  assert(!optimizationResult.empty());
  vector<double> absFTimes(optimizationResult.size());
  absFTimes[0] = optimizationResult[0].duration;
  for(unsigned i=1; i<absFTimes.size(); i++){
    absFTimes[i] = absFTimes[i-1] + optimizationResult[i].duration;
  }
  return absFTimes;
}
/** @brief removes the "retarded paramters" introduced from the structure detection
 *  @param[in] optimizationResult
 *  @param[in/out] isFirst indicates wether we are in the beginning of the grid of that specific control.
               It indicates wethere we delete the first point or not.
 *  @param[out] timePoints
 *  @param[out] values
 *
 * for the first grid we have to remove the last grid point and value
 * for all other elements, we have to remove both the first and the last
 * the last element has also to be taken special care, but this is done in the function
 * updateParameterizationGridInput()
 */
void InputUpdater::removePossibleRetardedParameters(
  const DyosOutput::ParameterGridOutput &optimizationResult,
        bool &isFirst,
        std::vector<double> &timePoint,
        std::vector<double> &value)const
{
  value = optimizationResult.values;
  timePoint = optimizationResult.gridPoints;
  if(value.size() == timePoint.size()){
    if(!value.empty()){
      value.pop_back();
    }
  }
  if(!timePoint.empty()){
    timePoint.pop_back();
  }
  if(isFirst){
    isFirst = false;
  }else{
    // last element only exists for piecewise linear approximations
    value.erase(value.begin());
    timePoint.erase(timePoint.begin());
  }

}

