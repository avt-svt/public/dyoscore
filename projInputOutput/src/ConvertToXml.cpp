/**
* @file ConvertToXml.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertToXml                                                         \n
* =====================================================================\n
* This file contains the definitions of the function convertToXml and  \n
* several subfunctions which convert the UserInput::Input into an xml  \n
* file                                                                 \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski, Adrian Caspari
* @date 18.10.2018
*/

#include "ConvertToXml.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/lexical_cast.hpp>

/**
* @brief create boost::propertyTree::ptree, call several subfunctions, write xml-file "convertedUserInput.xml"
*
* @param[in] UserInput::Input input
*/
static void createPropertyTree(const UserInput::Input &input);
/**
* @brief put all components of optimizerInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to OptimizerInput
* @param[in] path of optimizerInput as string
*/
static void createPropertyTree_OptimizerInput(boost::property_tree::ptree &pTree,
                                              const UserInput::OptimizerInput &optimizerInput, std::string path);
/**
* @brief put all components of IntegratorInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to integratorInput
* @param[in] path of integratorInput as string
*/
static void createPropertyTree_IntegratorInput(boost::property_tree::ptree &pTree,
                                               const UserInput::IntegratorInput &integratorInput, std::string path);
/**
* @brief put all components of StageInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to stageInput
* @param[in] path of stageInput as string
*/
static void createPropertyTree_StageInput(boost::property_tree::ptree &pTree, const UserInput::StageInput &stageInput,
                                          std::string path);
/**
* @brief put all components of ParameterInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to parameterInput
* @param[in] path of parameterInput as string
*/
static void createPropertyTree_ParameterInput(boost::property_tree::ptree &pTree,
                                              const UserInput::ParameterInput &parameterInput, std::string path);
/**
* @brief put all components of ConstraintInput into pTree
*
* @param[in] reference to pTree
* @param[in] const reference to constraintInput
* @param[in] path of constraintInput as string
*/
static void createPropertyTree_ConstraintInput(boost::property_tree::ptree &pTree,
                                               const UserInput::ConstraintInput &constraintInput, std::string path);
/**
* @brief gets the enum for AdaptiveStrategy
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getAdaptiveStrategy(unsigned i);
/**
* @brief gets the enum for OptimizerType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getOptimizerType(unsigned i);
/**
* @brief gets the enum for LinearSolverType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getLinearSolverType(unsigned i);
/**
* @brief gets the enum for NonLinearSolverType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getNonLinearSolverType(unsigned i);
/**
* @brief gets the enum for DaeInitializationType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getDaeInitializationType(unsigned i);
/**
* @brief gets the enum for IntegrationOrder
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getIntegrationOrder(unsigned i);
/**
* @brief gets the enum for IntegratorType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getIntegratorType(unsigned i);
/**
* @brief gets the enum for RunningMode
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getRunningMode(unsigned i);
/**
* @brief gets the enum for ApproximationType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getApproximationType(unsigned i);
/**
* @brief gets the enum for ParameterSensitivityType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getParameterSensitivityType(unsigned i);
/**
* @brief gets the enum for ParameterType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getParameterType(unsigned i);
/**
* @brief gets the enum for EsoType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getEsoType(unsigned i);
/**
* @brief gets the enum for AdaptationType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getAdaptationType(unsigned i);
/**
* @brief gets the enum for ConstraintType
*
* @param[in] number that represents the enum
* @return the enum as a string
*/
static std::string getConstraintType(unsigned i);
/**
* @brief calls the function createPropertyTree
*
* @param[in] UserInput::Input input
* @return integer
*/
void convertToXml(UserInput::Input input)
{
  createPropertyTree(input);
}

void createPropertyTree(const UserInput::Input &input)
{
  std::string path="debug.input";
  boost::property_tree::ptree pTree;
  for (unsigned i=0; i<input.stages.size(); i++){
    createPropertyTree_StageInput(pTree, input.stages.at(i), path+".stages.stages_"+boost::lexical_cast<std::string>(i));
  }
  pTree.put<double>(path+".totalEndTimeLowerBound", input.totalEndTimeLowerBound);
  pTree.put<double>(path+".totalEndTimeUpperBound", input.totalEndTimeUpperBound);
  pTree.put<std::string>(path+".runningMode", getRunningMode(input.runningMode));

  createPropertyTree_IntegratorInput(pTree, input.integratorInput, path+".integratorInput");
  createPropertyTree_OptimizerInput(pTree, input.optimizerInput, path+".optimizerInput");

  write_xml("convertedUserInput.xml", pTree);
}

static void createPropertyTree_OptimizerInput(boost::property_tree::ptree &pTree,
                                              const UserInput::OptimizerInput &optimizerInput, std::string path)
{
  pTree.put<std::string>(path+".optimizerType", getOptimizerType(optimizerInput.type));
  pTree.put<double>(path+".adaptationThreshold", optimizerInput.adaptationThreshold);
  pTree.put<std::string>(path+".adaptiveStrategy", getAdaptiveStrategy(optimizerInput.adaptStrategy));
  for (unsigned j=0; j<optimizerInput.globalConstraints.size(); j++){
    createPropertyTree_ConstraintInput(pTree, optimizerInput.globalConstraints.at(j),
      path+".globalConstraints.globalConstraints_"+boost::lexical_cast<std::string>(j) );
  }
}

static void createPropertyTree_IntegratorInput(boost::property_tree::ptree &pTree,
                                               const UserInput::IntegratorInput &integratorInput, std::string path)
{
  pTree.put<std::string>(path+".integratorType", getIntegratorType(integratorInput.type));
  pTree.put<std::string>(path+".integrationOrder", getIntegrationOrder(integratorInput.order));

  pTree.put<std::string>(path+".daeInit.daeInitializationType", getDaeInitializationType(integratorInput.daeInit.type));
  pTree.put<std::string>(path+".daeInit.linSolver.solverType", getLinearSolverType(integratorInput.daeInit.linSolver.type));
  pTree.put<std::string>(path+".daeInit.nonLinSolver.solverType",
                         getNonLinearSolverType(integratorInput.daeInit.nonLinSolver.type));
  pTree.put<double>(path+".daeInit.nonLinSolver.tolerance", integratorInput.daeInit.nonLinSolver.tolerance);
  pTree.put<double>(path+".daeInit.maximumErrorTolerance", integratorInput.daeInit.maximumErrorTolerance);
}

static void createPropertyTree_StageInput(boost::property_tree::ptree &pTree, const UserInput::StageInput &stageInput,
                                          std::string path)
{
  pTree.put<bool>(path+".treatObjective", stageInput.treatObjective);
  pTree.put<bool>(path+".mapping.fullStateMapping", stageInput.mapping.fullStateMapping);

  std::map<std::string,std::string>::const_iterator iter;
  for ( iter=stageInput.mapping.stateNameMapping.begin() ; iter != stageInput.mapping.stateNameMapping.end(); iter++ ){
    pTree.put<std::string>(path+".mapping.stateNameMapping_"+boost::lexical_cast<std::string>
                           (std::distance(stageInput.mapping.stateNameMapping.begin() , iter)),
                           (*iter).first + " => " + (*iter).second);
  }

  pTree.put<std::string>(path+".eso.model", stageInput.eso.model);
  pTree.put<std::string>(path+".eso.process", stageInput.eso.process);
  pTree.put<std::string>(path+".eso.type", getEsoType(stageInput.eso.type));

  createPropertyTree_ParameterInput(pTree, stageInput.integrator.finaltime, path+".integrator.finaltime");
  for (unsigned j=0; j<stageInput.integrator.parameters.size(); j++) {//parameters is a vector of structs
    createPropertyTree_ParameterInput(pTree, stageInput.integrator.parameters.at(j),
                                      path+".integrator.parameters.parameters_"+boost::lexical_cast<std::string>( j ));
  }
  createPropertyTree_ConstraintInput(pTree, stageInput.optimizer.objective, path+".optimizer.objective");
  for (unsigned j=0; j<stageInput.optimizer.constraints.size(); j++){
    createPropertyTree_ConstraintInput(pTree, stageInput.optimizer.constraints.at(j),
                                       path+".optimizer.constraints.constraints_"+boost::lexical_cast<std::string>( j ));
  }
}

static void createPropertyTree_ConstraintInput(boost::property_tree::ptree &pTree,
                                               const UserInput::ConstraintInput &constraintInput, std::string path)
{
  pTree.put<std::string>(path+".name", constraintInput.name);
  pTree.put<double>(path+".lowerBound", constraintInput.lowerBound);
  pTree.put<double>(path+".upperBound", constraintInput.upperBound);
  pTree.put<double>(path+".timePoint", constraintInput.timePoint);
  pTree.put<double>(path+".lagrangeMultiplier", constraintInput.lagrangeMultiplier);
  pTree.put<std::string>(path+".constraintType", getConstraintType(constraintInput.type));
}


static void createPropertyTree_ParameterInput(boost::property_tree::ptree &pTree,
                                              const UserInput::ParameterInput &parameterInput,
                                              std::string path)
{
  pTree.put(path+".name", parameterInput.name);
  pTree.put<double>(path+".lowerBound", parameterInput.lowerBound);
  pTree.put<double>(path+".upperBound", parameterInput.upperBound);
  pTree.put<double>(path+".value", parameterInput.value);
  pTree.put<std::string>(path+".parameterType", getParameterType(parameterInput.paramType));
  pTree.put<std::string>(path+".parameterSensitivityType", getParameterSensitivityType(parameterInput.sensType));

  std::string path1;
  std::string vectorString;
  for (unsigned j=0; j<parameterInput.grids.size(); j++){//grids is a vector of structs
    path1=path+".grids.grids_"+boost::lexical_cast<std::string>( j );
    pTree.put<unsigned>(path1+".numIntervals", parameterInput.grids.at(j).numIntervals);
    pTree.put<double>(path1+".finalTime", parameterInput.grids.at(j).finalTime);
    pTree.put<bool>(path1+".hasFreeFinalTime", parameterInput.grids.at(j).hasFreeFinalTime);
    pTree.put<std::string>(path1+".approximationType", getApproximationType(parameterInput.grids.at(j).type));

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<parameterInput.grids.at(j).timePoints.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(parameterInput.grids.at(j).timePoints.at(k))+", ";
      //write all elements into vectorString
    }
    pTree.put<std::string>(path1+".timePoints", vectorString); //put vectorString into pTree

    vectorString.clear();//empty vectorString to use it again for new vector
    for (unsigned k=0; k<parameterInput.grids.at(j).values.size(); k++){
      vectorString+=boost::lexical_cast<std::string>(parameterInput.grids.at(j).values.at(k))+", ";
      //write all elements into vectorString
    }
    pTree.put<std::string>(path1+".values", vectorString);//put vectorString into pTree

    pTree.put(path1+".adapt.adaptType", getAdaptationType(parameterInput.grids.at(j).adapt.adaptType));
    pTree.put<unsigned>(path1+".adapt.maxAdaptSteps", parameterInput.grids.at(j).adapt.maxAdaptSteps);
    pTree.put<unsigned>(path1+".adapt.adaptWave.maxRefinementLevel", parameterInput.grids.at(j).adapt.adaptWave.maxRefinementLevel);
    pTree.put<unsigned>(path1+".adapt.adaptWave.minRefinementLevel", parameterInput.grids.at(j).adapt.adaptWave.minRefinementLevel);
    pTree.put<unsigned>(path1+".adapt.adaptWave.horRefinementDepth", parameterInput.grids.at(j).adapt.adaptWave.horRefinementDepth);
    pTree.put<unsigned>(path1+".adapt.adaptWave.verRefinementDepth", parameterInput.grids.at(j).adapt.adaptWave.verRefinementDepth);
    pTree.put<double>(path1+".adapt.adaptWave.etres", parameterInput.grids.at(j).adapt.adaptWave.etres);
    pTree.put<double>(path1+".adapt.adaptWave.epsilon", parameterInput.grids.at(j).adapt.adaptWave.epsilon);
  }
}

//functions to get enums

static std::string getAdaptiveStrategy(unsigned i)
{
  switch (i)
  {
  case 0:
      return "NOADAPTATION";
    case 1:
      return "ADAPTATION";
    case 2:
      return "STRUCTURE_DETECTION";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getOptimizerType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "SNOPT";
    case 1:
      return "IPOPT";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getNonLinearSolverType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "NLEQ1S";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getLinearSolverType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "MA28";
	case 1:
      return "KLU";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getDaeInitializationType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "NO";
    case 1:
      return "FULL";
    case 2:
      return "BLOCK";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getIntegrationOrder(unsigned i)
{
  switch (i)
  {
    case 0:
      return "ZEROTH";
    case 1:
      return "FIRST_FORWARD";
    case 2:
      return "FIRST_REVERSE";
    case 3:
      return "SECOND_REVERSE";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getIntegratorType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "NIXE";
    case 1:
      return "LIMEX";
    case 2:
      return "IDAS";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getRunningMode(unsigned i)
{
  switch (i)
  {
    case 0:
      return "SIMULATION";
    case 1:
      return "SINGLE_SHOOTING";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getApproximationType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "PIECEWISE_CONSTANT";
    case 1:
      return "PIECEWISE_LINEAR";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}
static std::string getParameterSensitivityType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "FULL";
    case 1:
      return "FRACTIONAL";
    case 3:
      return "NO";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getParameterType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "INITIAL";
    case 1:
      return "TIME_INVARIANT";
    case 2:
      return "PROFILE";
    case 3:
      return "FINALTIME";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getAdaptationType(unsigned i)
{
  switch(i)
  {
    case 0:
      return "WAVELET";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getEsoType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "JADE";
    case 1:
      return "FMI";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}

static std::string getConstraintType(unsigned i)
{
  switch (i)
  {
    case 0:
      return "PATH";
    case 1:
      return "POINT";
    case 2:
      return "ENDPOINT";
    default:
      return boost::lexical_cast<std::string>(i);
  }
}
