/**
* @file ConvertFromXmlJson.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertFromXmlJson                                                   \n
* =====================================================================\n
* This file contains the definition of the function convertFromXmlJson \n
* and several subfunctions, that convert a given xml/json-file into    \n
* UserInput::Input                                                     \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski, Adrian Caspari
* @date 18.10.2018
*/
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>
#include "ConvertFromXmlJson.hpp"
#include <vector>
#include <sstream>

/**
* @brief read xml-file into boost::propertyTree::ptree, call several subfunctions
*
* @param[in] UserInput::Input input
* @param[in] std::string filename
* @param[in] DataFormat dataFormat
*/
static void createInput(std::string filename, UserInput::Input &input, DataFormat dataFormat);
/**
* @brief create StageInput for vector stages
*
* @param[in] boost::property_tree::ptree &pTree
* @param[in] UserInput::StageInput &stageInput
* @param[in] std::string path
*/
static void createStageInput(boost::property_tree::ptree &pTree, UserInput::StageInput &stageInput,
                             std::string path);
/**
* @brief create ParameterInput
*
* @param[in] boost::property_tree::ptree &pTree
* @param[in] UserInput::ParameterInput &parameterInput
* @param[in] std::string path
*/
static void createParameterInput(boost::property_tree::ptree &pTree,
                                 UserInput::ParameterInput &parameterInput, std::string path);
/**
* @brief create IntegratorInput
*
* @param[in] boost::property_tree::ptree &pTree
* @param[in] UserInput::IntegratorInput &integratorInput
* @param[in] std::string path
*/
static void createIntegratorInput(boost::property_tree::ptree &pTree,
                                  UserInput::IntegratorInput &integratorInput, std::string path);
/**
* @brief create OptimizerInput
*
* @param[in] boost::property_tree::ptree &pTree
* @param[in] UserInput::OptimizerInput &optimizerInput
* @param[in] std::string path
*/
static void createOptimizerInput(boost::property_tree::ptree &pTree,
                                 UserInput::OptimizerInput &optimizerInput, std::string path);
/**
* @brief create ConstraintInput
*
* @param[in] boost::property_tree::ptree &pTree
* @param[in] UserInput::ConstraintInput &constraintInput
* @param[in] std::string path
*/
static void createConstraintInput(boost::property_tree::ptree &pTree,
                                  UserInput::ConstraintInput &constraintInput, std::string path);
/**
* @brief vectors of standart data types are stored as strings in the xml-file
* toVector seperates the string again and saves the data in a vector
*
* @param[in] const std::string& s
*/
template<typename T> static std::vector<T> toVector(const std::string& s);
/**
* @brief maps of standart data types are stored as strings in the xml-file
* toPair seperates the string again and saves the data in a pair wich can then be used to create a map
*
* @param[in] const std::string& s
* @return std::vector<typename T>
*/
template<typename T1, typename T2> static std::pair<T1, T2> toPair(const std::string& s);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return std::pair<typename T1, typename T2>
*/
static UserInput::EsoInput::EsoType getNumEsoType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::EsoInput::EsoType
*/
static UserInput::AdaptationInput::AdaptationType getNumAdaptationType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::ParameterGridInput::ApproximationType
*/
static UserInput::ParameterGridInput::ApproximationType getNumApproximationType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::ParameterInput::ParameterTyp
*/
static UserInput::ParameterInput::ParameterType getNumParameterType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::ParameterInput::ParameterSensitivityType
*/
static UserInput::ParameterInput::ParameterSensitivityType getNumParameterSensitivityType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::ConstraintInput::ConstraintType
*/
static UserInput::ConstraintInput::ConstraintType getNumConstraintType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::LinearSolverInput::SolverType
*/
static UserInput::LinearSolverInput::SolverType getNumLinearSolverType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::NonLinearSolverInput::SolverType
*/
static UserInput::NonLinearSolverInput::SolverType getNumNonLinearSolverType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::DaeInitializationInput::DaeInitializationType
*/
static UserInput::DaeInitializationInput::DaeInitializationType getNumDaeInitializationType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::IntegratorInput::IntegratorType
*/
static UserInput::IntegratorInput::IntegratorType getNumIntegratorType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::IntegratorInput::IntegrationOrder
*/
static UserInput::IntegratorInput::IntegrationOrder getNumIntegrationOrder(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::OptimizerInput::OptimizerType
*/
static UserInput::OptimizerInput::OptimizerType getNumOptimizerType(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::OptimizerInput::AdaptiveStrategy
*/
static UserInput::OptimizerInput::AdaptiveStrategy getNumAdaptiveStrategy(std::string st);
/**
* @brief converts string(typename) to specific type
*
* @param[in] std::string st
* @return UserInput::Input::RunningMode
*/
static UserInput::Input::RunningMode getNumRunningMode(std::string st);
/**
* @brief visible function, calls createInput which writes information from a xml-file into UserInput::input
*
* @param[in] std::string filename
* @param[in] UserInput::Input &input
* @param[in] DataFormat dataFormat
*/
void convertFromXmlJson(std::string filename, UserInput::Input &input, DataFormat dataFormat)
{
  createInput(filename, input, dataFormat);
}


static void createInput(std::string filename, UserInput::Input &input, DataFormat dataFormat)
{
  using boost::property_tree::ptree;
  ptree pTree;
  if(dataFormat==XML)
    read_xml(filename, pTree);
  else if(dataFormat==JSON)
    read_json(filename, pTree);

  std::string path="debug.input";

  for (unsigned i=0; i<pTree.get<unsigned>(path+".stages"); i++){
    UserInput::StageInput stageInput;
    createStageInput(pTree, stageInput, path+".stages.stages_"+boost::lexical_cast<std::string>(i));
    input.stages.push_back(stageInput);//filled stageInput is added to stages vector
  }
  input.totalEndTimeLowerBound=pTree.get<double>(path+".totalEndTimeLowerBound");
  input.totalEndTimeUpperBound=pTree.get<double>(path+".totalEndTimeUpperBound");
  input.runningMode=getNumRunningMode(pTree.get<std::string>(path+".runningMode"));
  createIntegratorInput(pTree, input.integratorInput, path+".integratorInput");
  createOptimizerInput(pTree, input.optimizerInput, path+".optimizerInput");

}
static void createOptimizerInput(boost::property_tree::ptree &pTree,
                                 UserInput::OptimizerInput &optimizerInput, std::string path)
{
  optimizerInput.type=getNumOptimizerType(pTree.get<std::string>(path+".optimizerType"));
  optimizerInput.adaptationThreshold=pTree.get<double>(path+".adaptationThreshold");
  optimizerInput.adaptStrategy=getNumAdaptiveStrategy(pTree.get<std::string>(path+".adaptiveStrategy"));

  for (unsigned j=0; j<pTree.get<unsigned>(path+".globalConstraints"); j++){
    UserInput::ConstraintInput constraintInput;
    createConstraintInput(pTree, constraintInput,
                          path+".globalConstraints.globalConstraints_"+boost::lexical_cast<std::string>(j));
    optimizerInput.globalConstraints.push_back(constraintInput);
  }
}
static void createConstraintInput(boost::property_tree::ptree &pTree,
                                  UserInput::ConstraintInput &constraintInput, std::string path)
{
  constraintInput.name=pTree.get<std::string>(path+".name");
  constraintInput.lowerBound=pTree.get<double>(path+".lowerBound");
  constraintInput.upperBound=pTree.get<double>(path+".upperBound");
  constraintInput.timePoint=pTree.get<double>(path+".timePoint");
  constraintInput.lagrangeMultiplier=pTree.get<double>(path+".lagrangeMultiplier");
  constraintInput.type=getNumConstraintType(pTree.get<std::string>(path+".constraintType"));
}
static void createIntegratorInput(boost::property_tree::ptree &pTree,
                                  UserInput::IntegratorInput &integratorInput, std::string path)
{
  integratorInput.type=getNumIntegratorType(pTree.get<std::string>(path+".integratorType"));
  integratorInput.order=getNumIntegrationOrder(pTree.get<std::string>(path+".integrationOrder"));

  integratorInput.daeInit.type=getNumDaeInitializationType(pTree.get<std::string>
                                                           (path+".daeInit.daeInitializationType"));
  integratorInput.daeInit.linSolver.type=getNumLinearSolverType(pTree.get<std::string>
                                                                (path+".daeInit.linSolver.solverType"));
  integratorInput.daeInit.nonLinSolver.type=getNumNonLinearSolverType(pTree.get<std::string>
                                                                      (path+".daeInit.nonLinSolver.solverType"));
  integratorInput.daeInit.nonLinSolver.tolerance=pTree.get<double>(path+".daeInit.nonLinSolver.tolerance");
  integratorInput.daeInit.maximumErrorTolerance=pTree.get<double>(path+".daeInit.maximumErrorTolerance");
}
template<typename T1, typename T2>
static std::pair<T1, T2> toPair(const std::string& s)
{
  std::pair <T1,T2> result;//pairs are stored as "first|second"
  std::stringstream ss(s);
  std::string item;
  std::getline(ss, item, '|');//reads string ss until "|" into item
  result.first=boost::lexical_cast<T1>(item);
  std::getline(ss, item, '|');
  result.second=boost::lexical_cast<T2>(item);
  return result;
}
template<typename T>
static std::vector<T> toVector(const std::string& s)
{
  std::vector<T> result;//vectors are stored as "vector.at(0),vector.at(1),..."
  std::stringstream ss(s);
  std::string item;
  while(std::getline(ss, item, ',')){
      result.push_back(boost::lexical_cast<T>(item));
  }
  //reads part of string ss until "," into item
  return result;
}
void createParameterInput(boost::property_tree::ptree &pTree,
                          UserInput::ParameterInput &parameterInput, std::string path)
{
  parameterInput.name=pTree.get<std::string>(path+".name");
  parameterInput.lowerBound=pTree.get<double>(path+".lowerBound");
  parameterInput.upperBound=pTree.get<double>(path+".upperBound");
  parameterInput.value=pTree.get<double>(path+".value");
  parameterInput.paramType=getNumParameterType(pTree.get<std::string>(path+".parameterType"));
  parameterInput.sensType=getNumParameterSensitivityType(pTree.get<std::string>
                                                         (path+".parameterSensitivityType"));

  std::string path1;
  for (unsigned j=0; j<pTree.get<unsigned>(path+".grids"); j++){
    UserInput::ParameterGridInput parameterGridInput;
    path1=path+".grids.grids_"+boost::lexical_cast<std::string>(j);
    parameterGridInput.numIntervals=pTree.get<unsigned>(path1+".numIntervals");
    parameterGridInput.finalTime=pTree.get<double>(path1+".finalTime");
    parameterGridInput.hasFreeFinalTime=pTree.get<bool>(path1+".hasFreeFinalTime");
    parameterGridInput.type=getNumApproximationType(pTree.get<std::string>(path1+".approximationType"));
    parameterGridInput.timePoints=toVector<double>(pTree.get<std::string>(path1+".timePoints"));
    parameterGridInput.values=toVector<double>(pTree.get<std::string>(path1+".values"));

    parameterGridInput.adapt.adaptType=getNumAdaptationType(pTree.get<std::string>(path1+".adapt.adaptType"));
    parameterGridInput.adapt.maxAdaptSteps=pTree.get<unsigned>(path1+".adapt.maxAdaptSteps");
    parameterGridInput.adapt.adaptWave.maxRefinementLevel=pTree.get<int>
                                                          (path1+".adapt.adaptWave.maxRefinementLevel");
    parameterGridInput.adapt.adaptWave.minRefinementLevel=pTree.get<int>
                                                          (path1+".adapt.adaptWave.minRefinementLevel");
    parameterGridInput.adapt.adaptWave.horRefinementDepth=pTree.get<int>
                                                          (path1+".adapt.adaptWave.horRefinementDepth");
    parameterGridInput.adapt.adaptWave.verRefinementDepth=pTree.get<int>
                                                          (path1+".adapt.adaptWave.verRefinementDepth");
    parameterGridInput.adapt.adaptWave.etres=pTree.get<double>(path1+".adapt.adaptWave.etres");

    parameterGridInput.adapt.adaptWave.epsilon=pTree.get<double>(path1+".adapt.adaptWave.epsilon");
    parameterInput.grids.push_back(parameterGridInput);//add filled parameterGridInput to vector grids

  }
}

void createStageInput(boost::property_tree::ptree &pTree, UserInput::StageInput &stageInput, std::string path)
{
  stageInput.treatObjective=pTree.get<bool>(path+".treatObjective");
  stageInput.mapping.fullStateMapping=pTree.get<bool>(path+".mapping.fullStateMapping");
  std::string path1;
  for (unsigned i=0; i<pTree.get<unsigned>(path+".mapping.stateNameMapping"); i++){
    path1=path+".mapping.stateNameMapping.stateNameMapping_"+boost::lexical_cast<std::string>(i);
    stageInput.mapping.stateNameMapping.insert(toPair<std::string,std::string>
                                               (pTree.get<std::string>(path1)));
  }

  stageInput.eso.model=pTree.get<std::string>(path+".eso.model");
  stageInput.eso.process=pTree.get<std::string>(path+".eso.process");
  stageInput.eso.type=getNumEsoType(pTree.get<std::string>(path+".eso.type"));

  createParameterInput(pTree, stageInput.integrator.finaltime, path+".integrator.finaltime");

  for (unsigned j=0; j<pTree.get<unsigned>(path+".integrator.parameters"); j++){
    UserInput::ParameterInput parameterInput;
    path1=path+".integrator.parameters.parameters_"+boost::lexical_cast<std::string>(j);
    createParameterInput(pTree, parameterInput, path1);
    stageInput.integrator.parameters.push_back(parameterInput);
  }
  stageInput.optimizer.structureDetection.maxStructureSteps=pTree.get<unsigned>(path+".optimizer.structureDetection.maxStructureSteps");

  createConstraintInput(pTree, stageInput.optimizer.objective, path+".optimizer.objective");

  for (unsigned j=0; j<pTree.get<unsigned>(path+".optimizer.constraints"); j++){
    UserInput::ConstraintInput constraintInput;
    path1=path+".optimizer.constraints.constraints_"+boost::lexical_cast<std::string>(j);
    createConstraintInput(pTree, constraintInput, path1);
    stageInput.optimizer.constraints.push_back(constraintInput);
  }
}
//get types for strings representing enums
static UserInput::EsoInput::EsoType getNumEsoType(std::string st)
{
  if(st=="JADE")
    return UserInput::EsoInput::JADE;
  else if(st=="FMI")
    return UserInput::EsoInput::FMI;
  else	
    throw EnumException();
}
static UserInput::AdaptationInput::AdaptationType getNumAdaptationType(std::string st)
{
  if(st=="WAVELET")
    return UserInput::AdaptationInput::WAVELET;
  else
    throw EnumException();
}
static UserInput::ParameterGridInput::ApproximationType getNumApproximationType(std::string st)
{
  if(st=="PIECEWISE_CONSTANT")
    return UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
  else if(st=="PIECEWISE_LINEAR")
    return UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  else
    throw EnumException();
}
static UserInput::ParameterInput::ParameterType getNumParameterType(std::string st)
{
  if(st=="INITIAL")
    return UserInput::ParameterInput::INITIAL;
  else if(st=="TIME_INVARIANT")
    return UserInput::ParameterInput::TIME_INVARIANT;
  else if(st=="PROFILE")
    return UserInput::ParameterInput::PROFILE;
  else if(st=="FINALTIME")
    return UserInput::ParameterInput::FINALTIME;
  else
    throw EnumException();
}
static UserInput::ParameterInput::ParameterSensitivityType getNumParameterSensitivityType(std::string st)
{
  if(st=="FULL")
    return UserInput::ParameterInput::FULL;
  else if(st=="FRACTIONAL")
    return UserInput::ParameterInput::FRACTIONAL;
  else if(st=="NO")
    return UserInput::ParameterInput::NO;
  else
    throw EnumException();
}
static UserInput::ConstraintInput::ConstraintType getNumConstraintType(std::string st)
{
  if(st=="PATH")
    return UserInput::ConstraintInput::PATH;
  else if(st=="POINT")
    return UserInput::ConstraintInput::POINT;
  else if(st=="ENDPOINT")
    return UserInput::ConstraintInput::ENDPOINT;
  else
    throw EnumException();
}
static UserInput::LinearSolverInput::SolverType getNumLinearSolverType(std::string st)
{
  if(st=="MA28")
    return UserInput::LinearSolverInput::MA28;
  if(st=="KLU")
    return UserInput::LinearSolverInput::KLU;
  else
    throw EnumException();
}
static UserInput::NonLinearSolverInput::SolverType getNumNonLinearSolverType(std::string st)
{
    if(st=="NLEQ1S")
        return UserInput::NonLinearSolverInput::NLEQ1S;
    else
        throw EnumException();
}
static UserInput::DaeInitializationInput::DaeInitializationType getNumDaeInitializationType(std::string st)
{
  if(st=="NO")
    return UserInput::DaeInitializationInput::NO;
  else if(st=="FULL")
    return UserInput::DaeInitializationInput::FULL;
  else if(st=="BLOCK")
    return UserInput::DaeInitializationInput::BLOCK;
  else
    throw EnumException();
}
static UserInput::IntegratorInput::IntegratorType getNumIntegratorType(std::string st)
{
  if(st=="NIXE")
    return UserInput::IntegratorInput::NIXE;
  else if(st=="LIMEX")
    return UserInput::IntegratorInput::LIMEX;
  else if(st=="IDAS")
    return UserInput::IntegratorInput::IDAS;
  else
    throw EnumException();
}
static UserInput::IntegratorInput::IntegrationOrder getNumIntegrationOrder(std::string st)
{
  if(st=="ZEROTH")
    return UserInput::IntegratorInput::ZEROTH;
  else if(st=="FIRST_FORWARD")
    return UserInput::IntegratorInput::FIRST_FORWARD;
  else if(st=="FIRST_REVERSE")
    return UserInput::IntegratorInput::FIRST_REVERSE;
  else if(st=="SECOND_REVERSE")
    return UserInput::IntegratorInput::SECOND_REVERSE;
  else
    throw EnumException();
}
static UserInput::OptimizerInput::OptimizerType getNumOptimizerType(std::string st)
{
    if(st=="SNOPT")
        return UserInput::OptimizerInput::SNOPT;
    else if(st=="IPOPT")
        return UserInput::OptimizerInput::IPOPT;
    else
        throw EnumException();
}
static UserInput::OptimizerInput::AdaptiveStrategy getNumAdaptiveStrategy(std::string st)
{
  if(st=="NOADAPTATION")
    return UserInput::OptimizerInput::NOADAPTATION;
  else if(st=="ADAPTATION")
    return UserInput::OptimizerInput::ADAPTATION;
  else if(st=="STRUCTURE_DETECTION")
    return UserInput::OptimizerInput::STRUCTURE_DETECTION;
  else
    throw EnumException();
}
static UserInput::Input::RunningMode getNumRunningMode(std::string st)
{
  if(st=="SIMULATION")
    return UserInput::Input::SIMULATION;
  else if(st=="SINGLE_SHOOTING")
    return UserInput::Input::SINGLE_SHOOTING;
  else
    throw EnumException();
}
