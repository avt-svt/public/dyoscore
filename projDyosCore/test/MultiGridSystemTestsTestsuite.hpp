BOOST_AUTO_TEST_SUITE(MultiGridSystemTests)

#ifdef BUILD_SNOPT
/**
* @test TestDyos - system test of runDyos with the old car example
*/
BOOST_AUTO_TEST_CASE(OldCarExampleMultiGrid)
{
  StageInput stageInput;

  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 10.0;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 35.429799679758318;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned numGrids = 3;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  unsigned numIntervals = 3;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 2.0);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
  stageInput.integrator.parameters[0].grids[0].duration = 4.4287249599697898;
  stageInput.integrator.parameters[0].grids[0].hasFreeDuration = true;

  numIntervals = 4;
  stageInput.integrator.parameters[0].grids[1].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[1].type = ParameterGridInput::PIECEWISE_CONSTANT;
  if (stageInput.integrator.parameters[0].grids[1].type == ParameterGridInput::PIECEWISE_LINEAR){
    numIntervals++;
  }
  stageInput.integrator.parameters[0].grids[1].values.resize(numIntervals, 0.0);
  stageInput.integrator.parameters[0].grids[1].duration = 26.5723497598187372;
  stageInput.integrator.parameters[0].grids[1].hasFreeDuration = true;

  numIntervals = 3;
  stageInput.integrator.parameters[0].grids[2].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[2].values.resize(numIntervals, -2.0);
  stageInput.integrator.parameters[0].grids[2].type = ParameterGridInput::PIECEWISE_CONSTANT;
  stageInput.integrator.parameters[0].grids[2].hasFreeDuration = true;



  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 10.0;
  stageInput.optimizer.objective.upperBound = 100.0;

  stageInput.optimizer.constraints.resize(3);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  stageInput.optimizer.constraints[1].name = "velo";
  stageInput.optimizer.constraints[1].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[1].lowerBound = 0.0;
  stageInput.optimizer.constraints[1].upperBound = 0.0;

  stageInput.optimizer.constraints[2].name = "dist";
  stageInput.optimizer.constraints[2].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[2].lowerBound = 300.0;
  stageInput.optimizer.constraints[2].upperBound = 300.0;


  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;

  UserOutput::Output output;
  
  Logger::Ptr logger(new Logger());
  OutputChannel::Ptr dyosOut(new FileChannel("Dyos.out"));
  logger->setLoggingChannel(Logger::FINAL_OUT_JSON, dyosOut);
  OutputChannel::Ptr finalstatesOut(new FileChannel("Finalstates.out"));
  logger->setLoggingChannel(Logger::FINALSTATES_OUT, finalstatesOut);
  OutputChannel::Ptr stateNamesOut(new FileChannel("stateNames.out"));
  logger->setLoggingChannel(Logger::STATENAMES_OUT, stateNamesOut);
  OutputChannel::Ptr finalOut(new FileChannel("final.out"));
  logger->setLoggingChannel(Logger::FINAL_OUT, finalOut);
  output = runDyos(input, logger);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif

#ifdef BUILD_IPOPT
BOOST_AUTO_TEST_CASE(WOBatchMultiGridTwoControls)
{
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0;
      stageDuration.upperBound = 1000.0;
      stageDuration.value = 1000.0;
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(2);
        const int numIntervals = 2;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.resize(numIntervals, 5.784);
        grids[0].hasFreeDuration = false;
        grids[0].duration = 500;

        grids[1].numIntervals = numIntervals;
        grids[1].type  = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[1].values.resize(numIntervals, 0.0);
        grids[1].hasFreeDuration = false;
        parameters[0].grids = grids;
      }
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 6;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.clear();
        grids[0].values.resize(numIntervals, 100e-3);
        grids[0].hasFreeDuration = false;
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }

  input.stages.push_back(stageInput);
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::IPOPT;

  UserOutput::Output output;
  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif

#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(WOBatchMultiGridProblemWithSubParameters)
{
  UserInput::StageInput stageInput2;

  stageInput2.eso.type = UserInput::EsoInput::JADE;

  stageInput2.eso.model = "woreactormultistage";

  stageInput2.integrator.duration.lowerBound = 0.0001;
  stageInput2.integrator.duration.upperBound = 1000.0;
  stageInput2.integrator.duration.value = 819.16119622096789;
  stageInput2.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput2.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput2.integrator.parameters.resize(4);
  stageInput2.integrator.parameters[0].name = "yconti";
  stageInput2.integrator.parameters[0].value= 1.0;
  stageInput2.integrator.parameters[0].lowerBound = 1.0;
  stageInput2.integrator.parameters[0].upperBound = 1.0;
  stageInput2.integrator.parameters[0].paramType = UserInput::ParameterInput::TIME_INVARIANT;
  stageInput2.integrator.parameters[0].sensType = UserInput::ParameterInput::NO;

  stageInput2.integrator.parameters[1].name = "tbin";
  stageInput2.integrator.parameters[1].value= 25.0;
  stageInput2.integrator.parameters[1].lowerBound = 25.0;
  stageInput2.integrator.parameters[1].upperBound = 25.0;
  stageInput2.integrator.parameters[1].paramType = UserInput::ParameterInput::TIME_INVARIANT;
  stageInput2.integrator.parameters[1].sensType = UserInput::ParameterInput::NO;

  stageInput2.integrator.parameters[2].name = "fbin";
  stageInput2.integrator.parameters[2].lowerBound = 0.0001;
  stageInput2.integrator.parameters[2].upperBound = 10;
  stageInput2.integrator.parameters[2].paramType = UserInput::ParameterInput::PROFILE;
  stageInput2.integrator.parameters[2].sensType = UserInput::ParameterInput::FULL;
  stageInput2.integrator.parameters[2].grids.resize(1);

  unsigned numIntervals = 2;
  stageInput2.integrator.parameters[2].grids[0].numIntervals = numIntervals;
  stageInput2.integrator.parameters[2].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput2.integrator.parameters[2].grids[0].pcresolution = 1;
  stageInput2.integrator.parameters[2].grids[0].duration = 819.16119622096789;
  stageInput2.integrator.parameters[2].grids[0].hasFreeDuration = true;
  stageInput2.integrator.parameters[2].grids[0].values.resize(numIntervals + 1, 5.784);
  stageInput2.integrator.parameters[2].grids[0].values[0] = 7.9446814728773889;
  stageInput2.integrator.parameters[2].grids[0].values[1] = 4.3336885393927753;
  stageInput2.integrator.parameters[2].grids[0].values[2] = 7.4345408540643003;

  stageInput2.integrator.parameters[2].grids[0].timePoints.resize(numIntervals + 1, 0.0);
  stageInput2.integrator.parameters[2].grids[0].timePoints[0] = 0.00000000000000000;
  stageInput2.integrator.parameters[2].grids[0].timePoints[1] = 0.300000000000000;
  stageInput2.integrator.parameters[2].grids[0].timePoints[2] = 1.000000000000000;



  stageInput2.integrator.parameters[3].name = "tw";
  stageInput2.integrator.parameters[3].lowerBound = 20.0;
  stageInput2.integrator.parameters[3].upperBound = 100.0;
  stageInput2.integrator.parameters[3].paramType = UserInput::ParameterInput::PROFILE;
  stageInput2.integrator.parameters[3].sensType = UserInput::ParameterInput::FULL;

  stageInput2.integrator.parameters[3].grids.resize(3);
  stageInput2.integrator.parameters[3].grids[0].numIntervals = 1;
  stageInput2.integrator.parameters[3].grids[0].duration = 204.79029905524197;
  stageInput2.integrator.parameters[3].grids[0].hasFreeDuration = true;
  stageInput2.integrator.parameters[3].grids[0].values.resize(1,100);
  stageInput2.integrator.parameters[3].grids[0].values[0] = 50.0;
  stageInput2.integrator.parameters[3].grids[0].timePoints.resize(2,0.0);
  stageInput2.integrator.parameters[3].grids[0].timePoints[0] = 0.00000000000000000;
  stageInput2.integrator.parameters[3].grids[0].timePoints[1] = 1.00000000000000;

  stageInput2.integrator.parameters[3].grids[1].numIntervals = 2;
  stageInput2.integrator.parameters[3].grids[1].duration = 563.17332240191536;
  stageInput2.integrator.parameters[3].grids[1].pcresolution = 1;
  stageInput2.integrator.parameters[3].grids[1].hasFreeDuration = true;
  stageInput2.integrator.parameters[3].grids[1].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput2.integrator.parameters[3].grids[1].values.resize(3,100);
  stageInput2.integrator.parameters[3].grids[1].values[0] = 97.282521487702866;
  stageInput2.integrator.parameters[3].grids[1].values[1] = 95.703557294503838;
  stageInput2.integrator.parameters[3].grids[1].values[2] = 100.00000000000000;
  stageInput2.integrator.parameters[3].grids[1].timePoints.resize(3,0.0);
  stageInput2.integrator.parameters[3].grids[1].timePoints[0] = 0.00000000000000000;
  stageInput2.integrator.parameters[3].grids[1].timePoints[1] = 0.50000000000000;
  stageInput2.integrator.parameters[3].grids[1].timePoints[2] = 1.0000000000000;


  stageInput2.integrator.parameters[3].grids[2].numIntervals = 1;
  stageInput2.integrator.parameters[3].grids[2].duration = 51.197574763810508;
  stageInput2.integrator.parameters[3].grids[2].values.resize(1,50);
  stageInput2.integrator.parameters[3].grids[2].hasFreeDuration = true;
  stageInput2.integrator.parameters[3].grids[2].values[0] = 100.00000000000000;
  stageInput2.integrator.parameters[3].grids[2].timePoints.resize(2,0.0);
  stageInput2.integrator.parameters[3].grids[2].timePoints[0] = 0.00000000000000000;
  stageInput2.integrator.parameters[3].grids[2].timePoints[1] = 1.00000000000000;

  stageInput2.optimizer.objective.name = "obj";
  stageInput2.treatObjective = true;


  UserInput::ConstraintInput constraint21;
  constraint21.name="tr";
  constraint21.type=UserInput::ConstraintInput::PATH;
  constraint21.lowerBound=60.0;
  constraint21.upperBound=90.0;
  stageInput2.optimizer.constraints.push_back(constraint21);

  UserInput::ConstraintInput constraint22;
  constraint22.name="wp";
  constraint22.type=UserInput::ConstraintInput::PATH;
  constraint22.lowerBound=0.105;
  constraint22.upperBound=0.2;
  constraint22.excludeZero = true;
  stageInput2.optimizer.constraints.push_back(constraint22);


  UserInput::Input input;

  input.stages.push_back(stageInput2);


  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1e-5";
  input.integratorInput.integratorOptions["relative tolerance"] = "1e-5";
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = UserInput::AdaptationOptions::NOADAPTATION;
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-5";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["major iterations limit"] = "150";
  input.optimizerInput.optimizerOptions["verify level"] = "-1";



  UserOutput::Output output;

  Logger::Ptr logger(new StandardLogger());
  output = runDyos(input, logger);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif
BOOST_AUTO_TEST_SUITE_END()