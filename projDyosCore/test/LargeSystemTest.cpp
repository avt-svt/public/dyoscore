/**
* @file DyosTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos testsuite                                                       \n
* =====================================================================\n
* This file contains dyos integration tests                            \n
* =====================================================================\n
* @author Fady Assassa
* @date 01.02.2013
*/

#include <iostream>

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "ConvertToXmlJson.hpp"

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE Dyos
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>
using namespace UserInput;
BOOST_AUTO_TEST_SUITE(LargeSystemTests)


BOOST_AUTO_TEST_CASE(SecondOrderStoppingCriterion)
{
  unsigned numStages = 1;
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0 / double(numStages);
      stageDuration.upperBound = 1000.0 / double(numStages);
      stageDuration.value = 1000.0 / double(numStages);
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.resize(numIntervals+1 , 5.784);
        grids[0].adapt.maxAdaptSteps = 8;
        grids[0].adapt.adaptWave.minRefinementLevel = 1;
        grids[0].adapt.adaptWave.horRefinementDepth = 0;
        grids[0].pcresolution = 2;
        parameters[0].grids = grids;

        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals+1, 100e-3);
        grids[0].adapt.maxAdaptSteps = 8;
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 8;

  for(unsigned i=0; i<numStages; i++){
    input.stages.push_back(stageInput);
    input.stages[i].treatObjective = false;
    if(i<numStages-1)
      input.stages[i].mapping.fullStateMapping = true;
  }
  input.stages.back().treatObjective = true;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-8;
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::IPOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPTATION;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-6";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-6";
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-6";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "250";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  input.optimizerInput.optimizerOptions["function precision"] = "1.e-6";

  UserOutput::Output output;
  output = runDyos(input);
  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }
}


BOOST_AUTO_TEST_CASE(WOBatchMultipleShooting)
{
  unsigned numStages = 4;
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0 / double(numStages);
      stageDuration.upperBound = 1000.0 / double(numStages);
      stageDuration.value = 1000.0 / double(numStages);
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 1;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.resize(numIntervals+1 , 5.784);
        grids[0].adapt.maxAdaptSteps = 8;
        grids[0].adapt.adaptWave.minRefinementLevel = 1;
        grids[0].adapt.adaptWave.horRefinementDepth = 0;
        grids[0].pcresolution = 2;
        grids[0].adapt.adaptWave.etres = 1e-8;
        grids[0].adapt.adaptWave.epsilon = 0.90;
        parameters[0].grids = grids;

        grids[0].numIntervals = 1;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals + 1, 100e-3);
        grids[0].adapt.maxAdaptSteps = 8;
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 8;
  
  for(unsigned i=0; i<numStages; i++){
    input.stages.push_back(stageInput);
    input.stages[i].treatObjective = false;
    if(i<numStages-1)
      input.stages[i].mapping.fullStateMapping = true;
  }
  input.stages.back().treatObjective = true;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-8;
  input.runningMode = Input::MULTIPLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::IPOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPT_STRUCTURE;
  input.optimizerInput.adaptationOptions.intermConstraintViolationTolerance = 1e-2;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-10";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-10";
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-7";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "250";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  input.optimizerInput.optimizerOptions["function precision"] = "1.e-10";

  UserOutput::Output output;
  output = runDyos(input);

  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}

BOOST_AUTO_TEST_CASE(WOMultiStageAdaptStructPaper)
{
  Input input;
  StageInput stageInput1, stageInput2;

  stageInput1.eso.type = UserInput::EsoInput::JADE;
  stageInput1.eso.model = "wobatch2Feeds";

  stageInput1.integrator.duration.lowerBound = 0.0001;
  stageInput1.integrator.duration.upperBound = 1000.0;
  stageInput1.integrator.duration.value = 100;
  stageInput1.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput1.integrator.duration.paramType = UserInput::ParameterInput::DURATION;


  stageInput1.integrator.parameters.resize(2);

  unsigned numIntervals = 16;

  stageInput1.integrator.parameters[0].name = "FainCur";
  stageInput1.integrator.parameters[0].lowerBound = 0;
  stageInput1.integrator.parameters[0].upperBound = 5.784;
  stageInput1.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput1.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
  stageInput1.integrator.parameters[0].grids.resize(1);

  stageInput1.integrator.parameters[0].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput1.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput1.integrator.parameters[0].grids[0].values.resize(numIntervals + 1, 5.784);


  stageInput1.integrator.parameters[1].name = "TwCur";
  stageInput1.integrator.parameters[1].lowerBound = 20.0;
  stageInput1.integrator.parameters[1].upperBound = 100.0;
  stageInput1.integrator.parameters[1].paramType = UserInput::ParameterInput::PROFILE;
  stageInput1.integrator.parameters[1].sensType = UserInput::ParameterInput::FULL;
  stageInput1.integrator.parameters[1].grids.resize(1);
  stageInput1.integrator.parameters[1].grids[0].numIntervals = numIntervals;
  stageInput1.integrator.parameters[1].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput1.integrator.parameters[1].grids[0].values.resize(numIntervals + 1, 60);

  stageInput1.optimizer.objective.name = "phi";
  stageInput1.treatObjective = false;

  UserInput::ConstraintInput constraint1;
  constraint1.name = "TR";
  constraint1.type = UserInput::ConstraintInput::PATH;
  constraint1.lowerBound = 60.0;
  constraint1.upperBound = 70.0;
  stageInput1.optimizer.constraints.push_back(constraint1);

  UserInput::ConstraintInput constraint12;
  constraint12.name = "TR";
  constraint12.type = UserInput::ConstraintInput::ENDPOINT;
  constraint12.lowerBound = 70.0;
  constraint12.upperBound = 70.0;
  stageInput1.optimizer.constraints.push_back(constraint12);

//########################################################
//###################STAGE 2##############################
//########################################################


  stageInput2.eso.type = UserInput::EsoInput::JADE;
  stageInput2.eso.model = "wobatch2Feeds";

  stageInput2.integrator.duration.lowerBound = 0.0001;
  stageInput2.integrator.duration.upperBound = 1000.0;
  stageInput2.integrator.duration.value = 900;
  stageInput2.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput2.integrator.duration.paramType = UserInput::ParameterInput::DURATION;


  stageInput2.integrator.parameters.resize(2);

  stageInput2.integrator.parameters[0].name = "FbinCur";
  stageInput2.integrator.parameters[0].lowerBound = 0;
  stageInput2.integrator.parameters[0].upperBound = 5.784;
  stageInput2.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput2.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
  stageInput2.integrator.parameters[0].grids.resize(1);

  stageInput2.integrator.parameters[0].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput2.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput2.integrator.parameters[0].grids[0].values.resize(numIntervals + 1, 5.784);


  stageInput2.integrator.parameters[1].name = "TwCur";
  stageInput2.integrator.parameters[1].lowerBound = 20.0;
  stageInput2.integrator.parameters[1].upperBound = 100.0;
  stageInput2.integrator.parameters[1].paramType = UserInput::ParameterInput::PROFILE;
  stageInput2.integrator.parameters[1].sensType = UserInput::ParameterInput::FULL;
  stageInput2.integrator.parameters[1].grids.resize(1);
  stageInput2.integrator.parameters[1].grids[0].numIntervals = numIntervals;
  stageInput2.integrator.parameters[1].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput2.integrator.parameters[1].grids[0].values.resize(numIntervals + 1, 60);

  stageInput2.optimizer.objective.name = "phi";
  stageInput2.treatObjective = true;

  UserInput::ConstraintInput constraint2;
  constraint2.name = "TR";
  constraint2.type = UserInput::ConstraintInput::PATH;
  constraint2.lowerBound = 70.0;
  constraint2.upperBound = 90.0;
  stageInput2.optimizer.constraints.push_back(constraint2);

  UserInput::ConstraintInput constraint3;
  constraint3.name = "V";
  constraint3.type = UserInput::ConstraintInput::ENDPOINT;
  constraint3.lowerBound = 5.0;
  constraint3.upperBound = 5.0;
  stageInput2.optimizer.constraints.push_back(constraint3);

  stageInput1.integrator.parameters[0].grids[0].adapt.maxAdaptSteps = 8;
  stageInput1.integrator.parameters[1].grids[0].adapt.maxAdaptSteps = 8;
  stageInput1.integrator.parameters[0].grids[0].adapt.adaptWave.etres = 1e-8;
  stageInput1.integrator.parameters[1].grids[0].adapt.adaptWave.etres = 1e-8;
  stageInput1.integrator.parameters[0].grids[0].adapt.adaptWave.epsilon = 0.9;
  stageInput1.integrator.parameters[1].grids[0].adapt.adaptWave.epsilon = 0.9;
  stageInput1.integrator.parameters[0].grids[0].adapt.adaptWave.maxRefinementLevel = 10;
  stageInput1.integrator.parameters[0].grids[0].adapt.adaptWave.minRefinementLevel = 1;
  stageInput1.integrator.parameters[0].grids[0].adapt.adaptWave.horRefinementDepth = 0;
  stageInput1.integrator.parameters[1].grids[0].adapt.adaptWave.maxRefinementLevel = 10;
  stageInput1.integrator.parameters[1].grids[0].adapt.adaptWave.minRefinementLevel = 1;
  stageInput1.integrator.parameters[1].grids[0].adapt.adaptWave.horRefinementDepth = 0;
  stageInput1.integrator.parameters[0].grids[0].pcresolution = 2;
  stageInput1.integrator.parameters[1].grids[0].pcresolution = 2;

  stageInput2.integrator.parameters[0].grids[0].adapt.maxAdaptSteps = 8;
  stageInput2.integrator.parameters[1].grids[0].adapt.maxAdaptSteps = 8;
  stageInput2.integrator.parameters[0].grids[0].adapt.adaptWave.etres = 1e-8;
  stageInput2.integrator.parameters[1].grids[0].adapt.adaptWave.etres = 1e-8;
  stageInput2.integrator.parameters[0].grids[0].adapt.adaptWave.epsilon = 0.9;
  stageInput2.integrator.parameters[1].grids[0].adapt.adaptWave.epsilon = 0.9;
  stageInput2.integrator.parameters[0].grids[0].adapt.adaptWave.maxRefinementLevel = 10;
  stageInput2.integrator.parameters[0].grids[0].adapt.adaptWave.minRefinementLevel = 1;
  stageInput2.integrator.parameters[0].grids[0].adapt.adaptWave.horRefinementDepth = 0;
  stageInput2.integrator.parameters[1].grids[0].adapt.adaptWave.maxRefinementLevel = 10;
  stageInput2.integrator.parameters[1].grids[0].adapt.adaptWave.minRefinementLevel = 1;
  stageInput2.integrator.parameters[1].grids[0].adapt.adaptWave.horRefinementDepth = 0;

  stageInput2.integrator.parameters[0].grids[0].pcresolution = 2;
  stageInput2.integrator.parameters[1].grids[0].pcresolution = 2;

  stageInput1.optimizer.structureDetection.maxStructureSteps = 8;
  stageInput2.optimizer.structureDetection.maxStructureSteps = 8;

  input.stages.push_back(stageInput1);
  input.stages.push_back(stageInput2);
  input.stages[0].mapping.fullStateMapping = true;
  input.totalEndTimeLowerBound = 200.0;
  input.totalEndTimeUpperBound = 2000.0;

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = UserInput::AdaptationOptions::ADAPT_STRUCTURE;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-6;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-10";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-10";
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-7";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "1250";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  input.optimizerInput.optimizerOptions["function precision"] = "1.e-10";
  input.optimizerInput.optimizerOptions["maximize"];


  UserOutput::Output output = runDyos(input);
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  //std::string filename = "output_64.dat";
  //convertUserOutputToXmlJson(filename, output, JSON);

  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }


}

BOOST_AUTO_TEST_SUITE_END()
