#include "ConvertFromXmlJson.hpp"
#include "ConvertToXmlJson.hpp"
#include "DyosWithLogger.hpp"

BOOST_AUTO_TEST_SUITE(SecondOrderSystemTests)

/**
* @test TestDyos - system test of runDyos with the old car example
*                  checking 2nd order derivatives
*/
BOOST_AUTO_TEST_CASE(OldCarExampleSecondOrder)
{
  UserInput::StageInput stageInput;

  stageInput.eso.type = UserInput::EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 10.0;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 1.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(2);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned numGrids = 1;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  unsigned numIntervals = 10;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 0.25);
  stageInput.integrator.parameters[0].grids[0].values.front() = 2.0;
  stageInput.integrator.parameters[0].grids[0].values.back() = -2.0;
  stageInput.integrator.parameters[0].grids[0].values[1] = 1.0332278615358494;
  stageInput.integrator.parameters[0].grids[0].values[8] = -0.57844314190697121;

  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;

  stageInput.integrator.parameters[1].name = "tf";
  stageInput.integrator.parameters[1].lowerBound = 10.0;
  stageInput.integrator.parameters[1].upperBound = 100.0;
  stageInput.integrator.parameters[1].paramType = UserInput::ParameterInput::TIME_INVARIANT;
  stageInput.integrator.parameters[1].sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.parameters[1].value = 35.607643626501620;

  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 10.0;
  stageInput.optimizer.objective.upperBound = 100.0;

  stageInput.optimizer.constraints.resize(3);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = UserInput::ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  stageInput.optimizer.constraints[1].name = "velo";
  stageInput.optimizer.constraints[1].type = UserInput::ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[1].lowerBound = 0.0;
  stageInput.optimizer.constraints[1].upperBound = 0.0;

  stageInput.optimizer.constraints[2].name = "dist";
  stageInput.optimizer.constraints[2].type = UserInput::ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[2].lowerBound = 300.0;
  stageInput.optimizer.constraints[2].upperBound = 300.0;


  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::SECOND_REVERSE;
  input.integratorInput.integratorOptions["initial stepsize"] = "1e-2";

  UserOutput::Output output;
  Logger::Ptr logger(new StandardLogger());

  unsigned numParameters = numIntervals + 1; //+1 because of the time invariant parameter tf
  std::vector<std::vector<double> > expectedHessian(numParameters);
  for (unsigned i = 0; i<numParameters; i++) {
	  expectedHessian[i].resize(numParameters);
  }
  BOOST_REQUIRE_EQUAL(numParameters, 11);
  //declare upper triangle matrix
  expectedHessian[0][0] = 0.019820856013357355;
  expectedHessian[0][1] = -0.0059386492683559224;
  expectedHessian[0][2] = -0.0066159531043471376;
  expectedHessian[0][3] = -0.0078861092985287474;
  expectedHessian[0][4] = -0.0094035157619219260;
  expectedHessian[0][5] = -0.011216366248031346;
  expectedHessian[0][6] = -0.013382237992442416;
  expectedHessian[0][7] = -0.015969920400892432;
  expectedHessian[0][8] = -0.019030699809591302;
  expectedHessian[0][9] = -0.011197896428272564;
  expectedHessian[0][10] = -0.20954362281045294;

  expectedHessian[1][1] = -0.0067999283239989139;
  expectedHessian[1][2] = -0.0074143224249854341;
  expectedHessian[1][3] = -0.0088377526405906490;
  expectedHessian[1][4] = -0.010538269647272792;
  expectedHessian[1][5] = -0.012569882900921040;
  expectedHessian[1][6] = -0.014997117675858866;
  expectedHessian[1][7] = -0.017897064426857547;
  expectedHessian[1][8] = -0.021327198384871501;
  expectedHessian[1][9] = -0.012549184265869663;
  expectedHessian[1][10] = -0.16029905839414421;

  expectedHessian[2][2] = -0.0088244975410482537;
  expectedHessian[2][3] = -0.010469172455849270;
  expectedHessian[2][4] = -0.012483599259932124;
  expectedHessian[2][5] = -0.014890241579648713;
  expectedHessian[2][6] = -0.017765535840878487;
  expectedHessian[2][7] = -0.021200803140570199;
  expectedHessian[2][8] = -0.025264128446619218;
  expectedHessian[2][9] = -0.014865722045241492;
  expectedHessian[2][10] = -0.15019721996183616;

  expectedHessian[3][3] = -0.012549293381331265;
  expectedHessian[3][4] = -0.014915422762470664;
  expectedHessian[3][5] = -0.017790882546880712;
  expectedHessian[3][6] = -0.021226288360522712;
  expectedHessian[3][7] = -0.025330750784388706;
  expectedHessian[3][8] = -0.030185617838291313;
  expectedHessian[3][9] = -0.017761586571096152;
  expectedHessian[3][10] = -0.13991270755386351;

  expectedHessian[4][4] = -0.017860717954610281;
  expectedHessian[4][5] = -0.021256572716729993;
  expectedHessian[4][6] = -0.025361200651669701;
  expectedHessian[4][7] = -0.030265218411670663;
  expectedHessian[4][8] = -0.036065820730830124;
  expectedHessian[4][9] = -0.021221569841637544;
  expectedHessian[4][10] = -0.12762430346083392;

  expectedHessian[5][5] = -0.025435984073529323;
  expectedHessian[5][6] = -0.030301600047010376;
  expectedHessian[5][7] = -0.036160927719543122;
  expectedHessian[5][8] = -0.043091495949378736;
  expectedHessian[5][9] = -0.025355563032804191;
  expectedHessian[5][10] = -0.11294156320811648;

  expectedHessian[6][6] = -0.036241623035277171;
  expectedHessian[6][7] = -0.043205130026346232;
  expectedHessian[6][8] = -0.051485783217792562;
  expectedHessian[6][9] = -0.030294864286113499;
  expectedHessian[6][10] = -0.095397966108687421;

  expectedHessian[7][7] = -0.051657139229818232;
  expectedHessian[7][8] = -0.061515290212169706;
  expectedHessian[7][9] = -0.036196348817600157;
  expectedHessian[7][10] = -0.074436092313731259;

  expectedHessian[8][8] = -0.073022653264680340;
  expectedHessian[8][9] = -0.042886296624966999;
  expectedHessian[8][10] = -0.049080146179738417;

  expectedHessian[9][9] = -0.034016906728372112;
  expectedHessian[9][10] = 0.021219463596082830;

  expectedHessian[10][10] = -0.022826496866359174;

  //declare lower triangle matrix (thus providing a symmetric matrix)
  for (unsigned i = 0; i<numParameters; i++) {
	  for (unsigned j = i + 1; j<numParameters; j++) {
		  expectedHessian[j][i] = expectedHessian[i][j];
	  }
  }
#ifdef BUILD_SNOPT
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;
  output = optimizeFirstForwardWithSecondOrderOutput(input, logger);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values.size(), numParameters);
  //now check the output
  for(unsigned i=0; i<numParameters; i++){
    BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values[i].size(), numParameters);
    for(unsigned j=0; j<numParameters; j++){
      BOOST_CHECK_CLOSE(output.optimizerOutput.secondOrder.values[i][j],
                        expectedHessian[i][j],
                        THRESHOLD);
    }
  }
#endif
#ifdef BUILD_NPSOL
  input.optimizerInput.type = UserInput::OptimizerInput::NPSOL;
  output = optimizeFirstForwardWithSecondOrderOutput(input, logger);
  // NPSOL produces apparently a similar solution like SNOPT. However the solution is not
  // close enough to grand exact matches with the standard threshold.
  double npsolThreshold = 1e-4;
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values.size(), numParameters);
  for(unsigned i=0; i<numParameters; i++){
    BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values[i].size(), numParameters);
    for(unsigned j=0; j<numParameters; j++){
      BOOST_CHECK_CLOSE(output.optimizerOutput.secondOrder.values[i][j],
                        expectedHessian[i][j],
                        npsolThreshold);
    }
  }
#endif

  double bigThreshold = 1.0;
  #ifdef BUILD_IPOPT
  input.optimizerInput.type = UserInput::OptimizerInput::IPOPT;
  output = runDyos(input);
  
  // having probably slightly differentoptimal results from the optimization,
  // the second derivatives are most likely slightly different from the SNOPT solution.
  // set the threshold to 1%
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values.size(), numParameters);
  //now check the output
  for(unsigned i=0; i<numParameters; i++){
    BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values[i].size(), numParameters);
    for(unsigned j=0; j<numParameters; j++){
      BOOST_CHECK_CLOSE(output.optimizerOutput.secondOrder.values[i][j],
                        expectedHessian[i][j],
                        bigThreshold);
    }
  }
  #endif
  
  #ifdef BUILD_FILTERSQP //DO_FILTER_SQP_TEST
  input.optimizerInput.type = UserInput::OptimizerInput::FILTER_SQP;
  output = runDyos(input);
  
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values.size(), numParameters);
  //now check the output
  for(unsigned i=0; i<numParameters; i++){
    BOOST_REQUIRE_EQUAL(output.optimizerOutput.secondOrder.values[i].size(), numParameters);
    for(unsigned j=0; j<numParameters; j++){
      BOOST_CHECK_CLOSE(output.optimizerOutput.secondOrder.values[i][j],
                        expectedHessian[i][j],
                        bigThreshold);
    }
  }
  #endif
}

#ifdef DO_FILTER_SQP_TEST
/**
* @test TestDyos - system test of runDyos with the old car example
*                  checking 2nd order derivatives
*/
BOOST_AUTO_TEST_CASE(OldCarExampleSecondOrderOptimization)
{
  StageInput stageInput;

  stageInput.eso.type = UserInput::EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 10.0;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 40.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned numGrids = 1;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  unsigned numIntervals = 10;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 2.0);
  stageInput.integrator.parameters[0].grids[0].values.front() = 2.0;
  stageInput.integrator.parameters[0].grids[0].values.back() = -2.0;

  stageInput.integrator.parameters[0].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;

  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 10.0;
  stageInput.optimizer.objective.upperBound = 100.0;

  stageInput.optimizer.constraints.resize(3);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = UserInput::ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  stageInput.optimizer.constraints[1].name = "velo";
  stageInput.optimizer.constraints[1].type = UserInput::ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[1].lowerBound = 0.0;
  stageInput.optimizer.constraints[1].upperBound = 0.0;

  stageInput.optimizer.constraints[2].name = "dist";
  stageInput.optimizer.constraints[2].type = UserInput::ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[2].lowerBound = 300.0;
  stageInput.optimizer.constraints[2].upperBound = 300.0;


  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::SECOND_REVERSE;
  input.optimizerInput.type = UserInput::OptimizerInput::FILTER_SQP;

  UserOutput::Output output;
  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  // check optimal solution (compared with SNOPT solution
  // we have to relax the THRESHOLD at some places
  std::vector<double> expectedOptimalControl(numIntervals, 0.25);
  expectedOptimalControl.front() = 2.0;
  expectedOptimalControl.back() = -2.0;
  expectedOptimalControl[1] = 1.0332278615358494;
  expectedOptimalControl[numIntervals -2] = -0.57844314190697121;
  std::vector<double> outputValues = output.stages[0].integrator.parameters[0].grids[0].values;
  BOOST_REQUIRE_EQUAL(numIntervals, outputValues.size());
  double relaxedThreshold = 6e-5;
  for(unsigned i=0; i<numIntervals; i++){
    double threshold = THRESHOLD;
    if(i==1 || i==numIntervals-2){
      threshold = relaxedThreshold;
    }
    BOOST_CHECK_CLOSE(expectedOptimalControl[i], outputValues[i], relaxedThreshold);
  }
  double expectedObjective = 35.607643626501620;
  double outputObjective = output.stages[0].optimizer.objective.grids[0].values[0];
  BOOST_CHECK_CLOSE(expectedObjective, outputObjective, relaxedThreshold);
}
#endif

#ifdef BUILD_IPOPT
/**
* @test TestDyos - system test of runDyos with the old car example
*                  checking 2nd order derivatives with Ipopt
*/
BOOST_AUTO_TEST_CASE(OldCarExampleSecondOrderOptimizationIpopt)
{
  StageInput stageInput;

  stageInput.eso.type = UserInput::EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 10.0;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 40.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned numGrids = 1;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  unsigned numIntervals = 10;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 2.0);
  stageInput.integrator.parameters[0].grids[0].values.front() = 2.0;
  stageInput.integrator.parameters[0].grids[0].values.back() = -2.0;

  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;

  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 10.0;
  stageInput.optimizer.objective.upperBound = 100.0;

  stageInput.optimizer.constraints.resize(3);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = UserInput::ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  stageInput.optimizer.constraints[1].name = "velo";
  stageInput.optimizer.constraints[1].type = UserInput::ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[1].lowerBound = 0.0;
  stageInput.optimizer.constraints[1].upperBound = 0.0;

  stageInput.optimizer.constraints[2].name = "dist";
  stageInput.optimizer.constraints[2].type = UserInput::ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[2].lowerBound = 300.0;
  stageInput.optimizer.constraints[2].upperBound = 300.0;


  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::SECOND_REVERSE;
  input.optimizerInput.type = UserInput::OptimizerInput::IPOPT;

  UserOutput::Output output;
  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  // check optimal solution
  std::vector<double> expectedOptimalControl(numIntervals);

  expectedOptimalControl[0] = 2;
  expectedOptimalControl[1] = 1.0332284792104074;
  expectedOptimalControl[2] = 0.25000000465463346;
  expectedOptimalControl[3] = 0.25000000465287492;
  expectedOptimalControl[4] = 0.25000000465287386;
  expectedOptimalControl[5] = 0.25000000465287570;
  expectedOptimalControl[6] = 0.25000000465287697;
  expectedOptimalControl[7] = 0.25000000465774869;
  expectedOptimalControl[8] = -0.57844327545455920;
  expectedOptimalControl[9] = -2;


  std::vector<double> outputValues = output.stages[0].integrator.parameters[0].grids[0].values;
  BOOST_REQUIRE_EQUAL(numIntervals, outputValues.size());
  double relaxedThreshold = 6e-5;
  for(unsigned i=0; i<numIntervals; i++){
    double threshold = THRESHOLD;
    //if(i==1 || i==numIntervals-2){
    //  threshold = relaxedThreshold;
    //}
    BOOST_CHECK_CLOSE(expectedOptimalControl[i], outputValues[i], threshold);
  }
  double expectedObjective = 35.607641344861534;
  double outputObjective = output.stages[0].optimizer.objective.grids[0].values[0];
  BOOST_CHECK_CLOSE(expectedObjective, outputObjective, relaxedThreshold);
}
#endif

BOOST_AUTO_TEST_SUITE_END()
