/**
* @file FMILoader.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic FMILoader - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the FMILoader module	                             \n
* =====================================================================\n
* @author Adrian Caspari
* @date 28.11.2017
*/

#ifndef _FMILARGESYSTEMTESTCONFIG_H_
#define _FMILARGESYSTEMTESTCONFIG_H_


// Path to fmi.fmu for FmiLoaderTest
#cmakedefine PATH_FMI_LARGE_SYSTEM_TEST_FMI "@PATH_FMI_LARGE_SYSTEM_TEST_FMI@"



#endif