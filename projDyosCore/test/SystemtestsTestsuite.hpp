
#include "ConvertToXmlJson.hpp"
using namespace UserInput;

#include "ConvertFromXmlJson.hpp"
#include "ConvertToXmlJson.hpp"
#include "DyosWithLogger.hpp"

BOOST_AUTO_TEST_SUITE(DyosSystemTests)

BOOST_AUTO_TEST_CASE(SimulateOldCarExampleWithoutParameters)
{
  StageInput stageInput;

  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 1.0;
  stageInput.integrator.duration.upperBound = 1.0;
  stageInput.integrator.duration.value = 1.0;
  stageInput.integrator.duration.paramType = ParameterInput::DURATION;
  stageInput.integrator.duration.sensType = ParameterInput::NO;
  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SIMULATION;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  
  
  OutputChannel::Ptr out(new FileChannel("myJson.jsn"));
  Logger::Ptr logger(new StandardLogger());
  logger->setLoggingChannel(Logger::INPUT_OUT_JSON, out);

  BOOST_CHECK_NO_THROW(runDyos(input, logger));
}


/**
* @test TestDyos - system test of runDyos with the expanded car example
*/
BOOST_AUTO_TEST_CASE(ExpandedCarExample)
{
  StageInput stageInput;

  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "carErweitert";

  stageInput.integrator.duration.lowerBound = 1e-3;
  stageInput.integrator.duration.upperBound = 10.0;
  stageInput.integrator.duration.value = 4.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "a";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
  const unsigned numIntervals = 4;
  stageInput.integrator.parameters[0].grids.resize(3);
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 2.0);
  stageInput.integrator.parameters[0].grids[0].values.front() = 2.0;
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
  stageInput.integrator.parameters[0].grids[0].duration = 1.0;
  stageInput.integrator.parameters[0].grids[0].hasFreeDuration = true;

  stageInput.integrator.parameters[0].grids[1].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[1].values.resize(numIntervals, 0.0);
  stageInput.integrator.parameters[0].grids[1].type = ParameterGridInput::PIECEWISE_CONSTANT;
  stageInput.integrator.parameters[0].grids[1].duration = 1.0;
  stageInput.integrator.parameters[0].grids[1].hasFreeDuration = true;

  stageInput.integrator.parameters[0].grids[2].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[2].values.resize(numIntervals, -2.0);
  stageInput.integrator.parameters[0].grids[2].values.back() = -2.0;
  stageInput.integrator.parameters[0].grids[2].hasFreeDuration = true;
  stageInput.integrator.parameters[0].grids[2].type = ParameterGridInput::PIECEWISE_CONSTANT;

  stageInput.optimizer.objective.name = "obj";

  stageInput.optimizer.constraints.resize(1);
  stageInput.optimizer.constraints[0].name = "v";
  stageInput.optimizer.constraints[0].type = ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  Input input;
  UserOutput::Output output;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;

#ifdef BUILD_SNOPT
  input.optimizerInput.type = OptimizerInput::SNOPT;

  output = runDyos(input);
  output = runDyos(input);


  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
#endif

#ifdef BUILD_NPSOL
  input.optimizerInput.type = OptimizerInput::NPSOL;
  output = runDyos(input);
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
#ifdef BUILD_LIMEX
  input.integratorInput.type = IntegratorInput::NIXE;
  output = runDyos(input);
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
#endif
#endif

#ifdef BUILD_IPOPT
  input.optimizerInput.type = OptimizerInput::IPOPT; // FILTER_SQP;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.order = IntegratorInput::SECOND_REVERSE;

  // test for second order end

  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
#endif
}


#ifdef BUILD_SNOPT
/**
* @test TestDyos - system test of runDyos with the old car example
*/
BOOST_AUTO_TEST_CASE(OldCarExample)
{
  StageInput stageInput;

  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 10.0;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 40.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned int numIntervals = 10;
  stageInput.integrator.parameters[0].grids.resize(1);
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals+1, 0.0);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;

  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 10.0;
  stageInput.optimizer.objective.upperBound = 100.0;

  stageInput.optimizer.constraints.resize(3);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  stageInput.optimizer.constraints[1].name = "velo";
  stageInput.optimizer.constraints[1].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[1].lowerBound = 0.0;
  stageInput.optimizer.constraints[1].upperBound = 0.0;

  stageInput.optimizer.constraints[2].name = "dist";
  stageInput.optimizer.constraints[2].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[2].lowerBound = 300.0;
  stageInput.optimizer.constraints[2].upperBound = 300.0;

  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;
  //switch gradients check on for SNOPT
  //this option should be used, if one suspects bugs in DyOS gradient calculation
  //switch it off for normal testing behavior
  //input.optimizerInput.optimizerOptions["verify level"] = "3";
  //never use verify level 1 - that will lead to errors because
  //then the problem will not be reevaluated if decision variables are changed

  UserOutput::Output output;
  output = runDyos(input);

  BOOST_REQUIRE_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  //now check maximize option
  input.optimizerInput.optimizationMode = UserInput::OptimizerInput::MAXIMIZE;
  input.stages[0].eso.model = "carFreeFinalTimeMaximize";
  input.stages[0].optimizer.objective.name = "obj";
  input.stages[0].optimizer.objective.lowerBound = -10.0;
  input.stages[0].optimizer.objective.upperBound = -100.0;

  UserOutput::Output output2;
  output2 = runDyos(input);
  BOOST_REQUIRE_EQUAL(output2.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  std::vector<double> outputValues1 = output.stages[0].integrator.parameters[0].grids[0].values;
  std::vector<double> outputValues2 = output2.stages[0].integrator.parameters[0].grids[0].values;
  BOOST_REQUIRE_EQUAL(outputValues1.size(), outputValues2.size());
  //is this threshold sufficient?
  double threshold = 1e-5;
  for(unsigned i=0; i<numIntervals; i++){
    BOOST_CHECK_CLOSE(outputValues1[i], outputValues2[i], threshold);
  }
  double outputObjective1 = output.stages[0].optimizer.objective.grids[0].values[0];
  double outputObjective2 = output2.stages[0].optimizer.objective.grids[0].values[0];
  BOOST_CHECK_CLOSE(outputObjective1, -outputObjective2, threshold);
}
#endif

/**
* @test TestDyos - system test of sensitivity integration using the old car example
*/
BOOST_AUTO_TEST_CASE(OldCarExampleSensIntegration)
{
  StageInput stageInput;

  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 10.0;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 40.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned int numIntervals = 10;
  stageInput.integrator.parameters[0].grids.resize(1);
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals+1, 0.0);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;

  stageInput.optimizer.constraints.resize(3);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  stageInput.optimizer.constraints[1].name = "velo";
  stageInput.optimizer.constraints[1].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[1].lowerBound = 0.0;
  stageInput.optimizer.constraints[1].upperBound = 0.0;

  stageInput.optimizer.constraints[2].name = "dist";
  stageInput.optimizer.constraints[2].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[2].lowerBound = 300.0;
  stageInput.optimizer.constraints[2].upperBound = 300.0;

  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SENSITIVITY_INTEGRATION;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;

  UserOutput::Output output;
  output = runDyos(input);
  
  //todo check result of the sensitivity integration
}


#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(PenicillinExample)
{

  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "penicillin";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 150.0;
      stageDuration.upperBound = 150.0;
      stageDuration.value = 150.0;
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(1);
      parameters[0].name = "u";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 1.0;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;
      //set grid of first parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 10;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        parameters[0].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(1);
      constraints[0].name = "P";
      constraints[0].upperBound = 1.0;
      constraints[0].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }

  input.stages.push_back(stageInput);
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;


  UserOutput::Output output;
  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif

#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(WOBatch)
{
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0;
      stageDuration.upperBound = 1000.0;
      stageDuration.value = 1000.0;
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.resize(numIntervals , 5.784);
        grids[0].adapt.maxAdaptSteps = 8;
        parameters[0].grids = grids;

        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals + 1, 100e-3);
        grids[0].adapt.maxAdaptSteps = 8;

        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 4;
  input.stages.push_back(stageInput);
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-7;
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::STRUCTURE_DETECTION;
  input.optimizerInput.optimizerOptions["major iterations limit"] = "150";

  UserOutput::Output output;
  OutputChannel::Ptr jsonFile(new FileChannel("WOBatch_output.dat"));
  Logger::Ptr logger(new StandardLogger());
  logger->setLoggingChannel(Logger::FINAL_OUT_JSON, jsonFile);
  output = runDyos(input, logger);
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif

#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(WOBatchNixeWithPWL)
{
  Input input;
  StageInput stageInput;

  //set eso input
  {
    UserInput::EsoInput esoInput;
    esoInput.type = UserInput::EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      UserInput::ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0;
      stageDuration.upperBound = 1000.0;
      stageDuration.value = 1000.0;
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<UserInput::ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = UserInput::ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = UserInput::ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.resize(numIntervals +1 , 5.784);
        grids[0].adapt.maxAdaptSteps = 8;
        parameters[0].grids = grids;

        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals + 1, 100e-3);
        grids[0].adapt.maxAdaptSteps = 8;

        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      UserInput::ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<UserInput::ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = UserInput::ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = UserInput::ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 4;
  input.stages.push_back(stageInput);
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-7;
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = UserInput::AdaptationOptions::NOADAPTATION;
  input.optimizerInput.optimizerOptions["major iterations limit"] = "150";

  UserOutput::Output output;

  output = runDyos(input);

  
  //if not implemented correctly Nixe can be very slow. It should be calculated as fast as NIXE
  BOOST_CHECK(output.solutionTime < 100);


  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif

// same problem as previous one, only here we test Ipopt
#ifdef BUILD_IPOPT
BOOST_AUTO_TEST_CASE(WOBatchIpopt)
{
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0;
      stageDuration.upperBound = 1000.0;
      stageDuration.value = 1000.0;
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.resize(numIntervals , 5.784);
        parameters[0].grids = grids;

        grids[0].numIntervals = 8;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals , 100e-3);
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 2;
  input.stages.push_back(stageInput);
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::IPOPT;
  //just set some options to test the setOptions of the IpoptWrapper
  input.optimizerInput.optimizerOptions["tol"] = "1e-5";
  //input.optimizerInput.optimizerOptions["print_level"] = "1";
  input.optimizerInput.optimizerOptions["max_iter"]="100";
  input.optimizerInput.optimizerOptions["linear_scaling_on_demand"] = "no";

  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::STRUCTURE_DETECTION;


  UserOutput::Output output;
  output = runDyos(input);
  // this should be c hanged to optimal
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif


#ifdef BUILD_SNOPT
/**
* @test TestDyos - system test of runDyos with the old car example
*/
BOOST_AUTO_TEST_CASE(OldCarExampleMultipleShooting)
{
  StageInput stageInput;

  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 0.01;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 4.4287249599697898;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned numGrids = 1;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  unsigned numIntervals = 3;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 2.0);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;


  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 10.0;
  stageInput.optimizer.objective.upperBound = 100.0;

  stageInput.optimizer.constraints.resize(1);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  StageInput input2 = stageInput;

  input2.integrator.duration.value = 26.5723497598187372;
  numIntervals = 4;
  input2.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  input2.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
  if (stageInput.integrator.parameters[0].grids[0].type == ParameterGridInput::PIECEWISE_LINEAR){
    numIntervals++;
  }
  input2.integrator.parameters[0].grids[0].values.assign(numIntervals, 0.0);

  StageInput input3 = stageInput;
  input3.integrator.duration.value = 35.429799679758318 - 26.5723497598187372 - 4.4287249599697898;
  numIntervals = 3;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.assign(numIntervals, -2.0);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;


  input3.optimizer.constraints.resize(3);

  input3.optimizer.constraints[1].name = "velo";
  input3.optimizer.constraints[1].type = ConstraintInput::ENDPOINT;
  input3.optimizer.constraints[1].lowerBound = 0.0;
  input3.optimizer.constraints[1].upperBound = 0.0;

  input3.optimizer.constraints[2].name = "dist";
  input3.optimizer.constraints[2].type = ConstraintInput::ENDPOINT;
  input3.optimizer.constraints[2].lowerBound = 300.0;
  input3.optimizer.constraints[2].upperBound = 300.0;


  Input input;

  input.stages.push_back(stageInput);
  input.stages.push_back(input2);
  input.stages.push_back(input3);
  input.stages[0].mapping.fullStateMapping = true;
  input.stages[1].mapping.fullStateMapping = true;

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-5";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["major iterations limit"] = "500";
  input.optimizerInput.optimizerOptions["verify level"] = "-1";

  UserOutput::Output output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

    //run MultipleShootingProblem

  input.runningMode = Input::MULTIPLE_SHOOTING;


  UserOutput::Output output2 = runDyos(input);
  BOOST_CHECK_EQUAL(output2.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  BOOST_CHECK_CLOSE(output.stages.back().optimizer.objective.grids.front().values.front(),
                    output2.stages.back().optimizer.objective.grids.front().values.front(),
                    1.5e-5);
}
#endif

#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(Parkram)
{
  StageInput stageInput;


  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "parkram";

  stageInput.integrator.duration.lowerBound = 15.0;
  stageInput.integrator.duration.upperBound = 15.0;
  stageInput.integrator.duration.value = 15.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "u";
  stageInput.integrator.parameters[0].lowerBound = 0.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
  const unsigned numGrids = 1;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  unsigned numIntervals = 8;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 0.5);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
  stageInput.integrator.parameters[0].grids[0].adapt.maxAdaptSteps = 4;
  stageInput.integrator.parameters[0].grids[0].adapt.adaptType = AdaptationInput::SWITCHING_FUNCTION;
  stageInput.integrator.parameters[0].grids[0].adapt.swAdapt.includeTol = 1e-3;
  stageInput.integrator.parameters[0].grids[0].adapt.swAdapt.maxRefinementLevel = 5;


  stageInput.optimizer.objective.name = "obj";
  stageInput.optimizer.constraints.resize(1);
  stageInput.optimizer.constraints[0].type = ConstraintInput::POINT;
  stageInput.optimizer.constraints[0].name = "g1";
  stageInput.optimizer.constraints[0].lowerBound = -1e30;
  stageInput.optimizer.constraints[0].upperBound = 1e30;
  stageInput.optimizer.constraints[0].timePoint = 0.0;

  Input input;
  input.stages.push_back(stageInput);

  input.integratorInput.daeInit.nonLinSolver.type = UserInput::NonLinearSolverInput::CMINPACK;

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPTATION;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-5;

  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-4";
  input.optimizerInput.optimizerOptions["Function Precision"] = "1.e-5";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["major iterations limit"] = "150";
  input.optimizerInput.optimizerOptions["verify level"] = "-1";
  input.integratorInput.integratorOptions["relative tolerance"] = "1e-5";
  input.integratorInput.integratorOptions["absolute tolerance"] = "1e-5";

  UserOutput::Output output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  /*for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }*/
  unsigned numSteps = 4;
  BOOST_CHECK_EQUAL(output.solutionHistory.size(), numSteps);
  double refVals[] = {-31.7682255098822,
                      -32.3026906134775,
                      -32.49506629879457,
                      -32.495155138691};
  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    BOOST_CHECK_CLOSE(
      output.solutionHistory[i].stages[0].optimizer.objective.grids[0].values[0],
      refVals[i],
      THRESHOLD);
  }

}
#endif

BOOST_AUTO_TEST_SUITE_END()
