/**
* @file RunDyosWithJsonFile
* @brief For debugging purpose this file starts Dyos with mexDyosInput.json in the path
*
* @author Tjalf Hoffmann
* @date 18.09.2015
*/

#include "ConvertFromXmlJson.hpp"
#include "ConvertToXmlJson.hpp"
#include "Dyos.hpp"

int main()
{
  UserInput::Input input;
  convertUserInputFromXmlJson("mexDyosInput.json",input, JSON);
  UserOutput::Output out;
  out = runDyos(input);
  return 0;
}