#include "ConvertToXmlJson.hpp"
using namespace UserInput;

#include "ConvertFromXmlJson.hpp"
#include "ConvertToXmlJson.hpp"
#include "DyosWithLogger.hpp"

BOOST_AUTO_TEST_SUITE(AdaptationSystemTests)


#ifdef BUILD_IPOPT
/**
* @test TestDyos - system test of runDyos with the old car example
*/
BOOST_AUTO_TEST_CASE(OldCarExampleStructureDetection)
{
  StageInput stageInput;

  stageInput.eso.type = EsoInput::JADE;
  stageInput.eso.model = "carFreeFinalTime";

  stageInput.integrator.duration.lowerBound = 10.0;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 40.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned numGrids = 1;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  const unsigned numIntervals = 8;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 0.0);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
  stageInput.integrator.parameters[0].grids[0].adapt.maxAdaptSteps = 2;

  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 10.0;
  stageInput.optimizer.objective.upperBound = 100.0;

  stageInput.optimizer.constraints.resize(3);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;

  stageInput.optimizer.constraints[1].name = "velo";
  stageInput.optimizer.constraints[1].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[1].lowerBound = 0.0;
  stageInput.optimizer.constraints[1].upperBound = 0.0;

  stageInput.optimizer.constraints[2].name = "dist";
  stageInput.optimizer.constraints[2].type = ConstraintInput::ENDPOINT;
  stageInput.optimizer.constraints[2].lowerBound = 300.0;
  stageInput.optimizer.constraints[2].upperBound = 300.0;
  stageInput.optimizer.structureDetection.maxStructureSteps = 1;

  Input input;

  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::IPOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPTATION;


  UserOutput::Output output;
  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
}
#endif

#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(WOBatchAdaptStruct)
{
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0;
      stageDuration.upperBound = 1000.0;
      stageDuration.value = 1000.0;
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 2;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.resize(numIntervals , 5.784);
        grids[0].adapt.maxAdaptSteps = 8;
        grids[0].adapt.adaptWave.minRefinementLevel = 1;
        grids[0].adapt.adaptWave.horRefinementDepth = 0;
        parameters[0].grids = grids;

        grids[0].numIntervals = 4;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals, 100e-3);
        grids[0].adapt.maxAdaptSteps = 8;
        grids[0].pcresolution = 2;
        grids[0].adapt.adaptWave.minRefinementLevel = 3;

        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 8;
  //if this option is activated Nixe is caught in an endless loop
  //stageInput.optimizer.structureDetection.createContinuousGrids = true;
  input.stages.push_back(stageInput);
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPT_STRUCTURE;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-3;
  input.optimizerInput.adaptationOptions.intermConstraintViolationTolerance = 0.068;

  input.integratorInput.integratorOptions["relative tolerance"] = "1e-6";
  input.integratorInput.integratorOptions["absolute tolerance"] = "1e-6";

  input.optimizerInput.optimizerOptions["Solution"] = "Yes";
  input.optimizerInput.optimizerOptions["System information"] = "Yes";
  input.optimizerInput.optimizerOptions["Iterations Limit"] = "10000";
  input.optimizerInput.optimizerOptions["Major optimality tolerance"] = "1.0E-6";
  input.optimizerInput.optimizerOptions["Major feasibility tolerance"] = "1.E-6";
  input.optimizerInput.optimizerOptions["Function Precision"] = "1.E-6";
  input.optimizerInput.optimizerOptions["Verify Level"] = "-1";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "150";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "150";
  input.optimizerInput.optimizerOptions["Jacobian"] = "sparse";
  input.optimizerInput.optimizerOptions["Derivative Level"] = "3";
  input.optimizerInput.optimizerOptions["Linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["Major step limit"] = "2.0";


  UserOutput::Output output;
  
  OutputChannel::Ptr jsonFile(new FileChannel("WOBatchAdaptStruct_output.dat"));
  Logger::Ptr logger(new StandardLogger());
  logger->setLoggingChannel(Logger::FINAL_OUT_JSON, jsonFile);

  output = runDyos(input, logger);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  int numSteps = 7;
  const double threshold = THRESHOLD;
  BOOST_CHECK_EQUAL(output.solutionHistory.size(), numSteps);
  BOOST_CHECK_CLOSE(output.solutionHistory[0].stages[0].optimizer.objective.grids[0].values[0], -4709.6262900885304, threshold);
  BOOST_CHECK_CLOSE(output.solutionHistory[1].stages[0].optimizer.objective.grids[0].values[0], -4747.4668332272076, threshold);
  BOOST_CHECK_CLOSE(output.solutionHistory[2].stages[0].optimizer.objective.grids[0].values[0], -4760.7717696661575, threshold);
  BOOST_CHECK_CLOSE(output.solutionHistory[3].stages[0].optimizer.objective.grids[0].values[0], -4765.6177331297931, threshold);
  BOOST_CHECK_CLOSE(output.solutionHistory[4].stages[0].optimizer.objective.grids[0].values[0], -4767.5428745914915, threshold);
  BOOST_CHECK_CLOSE(output.solutionHistory[5].stages[0].optimizer.objective.grids[0].values[0], -4768.3043818185306, threshold);
  BOOST_CHECK_CLOSE(output.solutionHistory[6].stages[0].optimizer.objective.grids[0].values[0], -4768.3128926577365, threshold);
}
#endif

#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(NISBRAdaptive)
{
  UserInput::StageInput stageInput;

  stageInput.eso.type = UserInput::EsoInput::JADE;
  stageInput.eso.model = "nisbr";

  stageInput.integrator.duration.lowerBound = 0.5;
  stageInput.integrator.duration.upperBound = 0.5;
  stageInput.integrator.duration.value = 0.5;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;


  stageInput.integrator.parameters.resize(2);
  stageInput.integrator.parameters[0].name = "F";
  stageInput.integrator.parameters[0].lowerBound = 0;
  stageInput.integrator.parameters[0].upperBound = 1;
  stageInput.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.parameters[0].grids.resize(1);

  int numIntervals = 8;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals + 1, 0.5);


  stageInput.integrator.parameters[1].name = "T";
  stageInput.integrator.parameters[1].lowerBound = 20;
  stageInput.integrator.parameters[1].upperBound = 50;
  stageInput.integrator.parameters[1].paramType = UserInput::ParameterInput::PROFILE;
  stageInput.integrator.parameters[1].sensType = UserInput::ParameterInput::FULL;
  stageInput.integrator.parameters[1].grids.resize(1);

  stageInput.integrator.parameters[1].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[1].grids[0].type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  stageInput.integrator.parameters[1].grids[0].values.resize(numIntervals + 1, 35);

  stageInput.optimizer.objective.name = "Obj";
  stageInput.treatObjective = true;


  UserInput::ConstraintInput constraint11;
  constraint11.name="Q";
  constraint11.type = UserInput::ConstraintInput::PATH;
  constraint11.lowerBound = 0.0;
  constraint11.upperBound = 1.5;
  stageInput.optimizer.constraints.push_back(constraint11);

  UserInput::ConstraintInput constraint12;
  constraint12.name="V";
  constraint12.type=UserInput::ConstraintInput::ENDPOINT;
  constraint12.lowerBound=0.0;
  constraint12.upperBound=1.1;
  stageInput.optimizer.constraints.push_back(constraint12);

  stageInput.integrator.parameters[0].grids[0].adapt.maxAdaptSteps = 4;
  stageInput.integrator.parameters[1].grids[0].adapt.maxAdaptSteps = 4;
  stageInput.integrator.parameters[0].grids[0].adapt.adaptWave.etres = 1e-7;
  stageInput.integrator.parameters[1].grids[0].adapt.adaptWave.etres = 1e-7;
  stageInput.integrator.parameters[0].grids[0].adapt.adaptWave.epsilon = 0.9;
  stageInput.integrator.parameters[1].grids[0].adapt.adaptWave.epsilon = 0.9;
  stageInput.integrator.parameters[0].grids[0].adapt.adaptWave.maxRefinementLevel = 10;
  stageInput.integrator.parameters[1].grids[0].adapt.adaptWave.maxRefinementLevel = 10;
  stageInput.integrator.parameters[0].grids[0].pcresolution = 2;
  stageInput.integrator.parameters[1].grids[0].pcresolution = 2;


  UserInput::Input input;
  input.stages.push_back(stageInput);


  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = UserInput::AdaptationOptions::ADAPTATION;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-4;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-8";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-8";
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-6";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.9";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "250";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  input.optimizerInput.optimizerOptions["function precision"] = "1.e-8";


  UserOutput::Output output;

  Logger::Ptr logger(new StandardLogger());
  output = runDyos(input, logger);

  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }


  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  unsigned numSteps = 4;
  BOOST_CHECK_EQUAL(output.solutionHistory.size(), numSteps);
  double refVals[] = {-2.0476967304878575, -2.0513969836643775, -2.0524648818482896, -2.0527372719046921};
  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    BOOST_CHECK_CLOSE(
      output.solutionHistory[i].stages[0].optimizer.objective.grids[0].values[0],
      refVals[i],
      5e-8);
  }
}
#endif

#ifdef BUILD_SNOPT
BOOST_AUTO_TEST_CASE(WOBatchSWAdpatStruct)
{
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::JADE;
    esoInput.model = "wobatchinactive";
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0 ;
      stageDuration.upperBound = 1000.0 ;
      stageDuration.value = 1000.0 ;
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.resize(numIntervals, 5.784);
        grids[0].adapt.maxAdaptSteps = 8;
        grids[0].pcresolution = 2;
        grids[0].adapt.adaptType = AdaptationInput::SWITCHING_FUNCTION;
        parameters[0].grids = grids;

        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals , 100e-3);
        grids[0].adapt.maxAdaptSteps = 8;
        grids[0].adapt.adaptType = AdaptationInput::SWITCHING_FUNCTION;
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 8;
  //with this option set the problem still runs through, but the result is slightly different (difference up to 0.3%)
  //stageInput.optimizer.structureDetection.createContinuousGrids = true;
  
  input.stages.push_back(stageInput);

  input.stages.back().treatObjective = true;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-8;
  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPT_STRUCTURE;
  input.optimizerInput.adaptationOptions.intermConstraintViolationTolerance = 1e-2;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-7";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-7";
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-6";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "250";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  input.optimizerInput.optimizerOptions["function precision"] = "1.e-7";

  UserOutput::Output output;
  output = runDyos(input);

  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }
  //apparently the result is platform dependent (even server dependent).
  //could be an initialization error, but if so extremely hard to detect.
  // maybe also dependent on optimizer
  //deactivate until a good solution can be found (or the error)
  //BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
/*#ifdef WIN32
  const unsigned numSteps = 11;
  double refVals[numSteps] = {-4.76008245009854e+003,
                       -4.76550304825432e+003,
                       -4.76754525628602e+003,
                       -4767.580370132423,
                       -4768.309468558552,
                       -4768.1927279942465,
                       -4768.3053085435558,
                       -4768.3125145571403,
                       -4768.3131950302313, -4768.3133893966788,-4768.3133661474649};
#else
  const unsigned numSteps = 12;
  double refVals[numSteps] = {-4.76008245009854e+003,
                              -4.76550304825432e+003,
                              -4.76754525628602e+003,
                              -4767.580370132423,
                              -4768.309468558552,
                              -4768.2978438180817,
                              -4768.3001319556597,
                              -4768.3115451679969,
                              -4768.3131950302313,
                              -4768.3016486491961,
                              -4768.3133661474649,
                              -4768.3132};
#endif*/
  //apparently the result is platform dependent (even server dependent).
  //could be an initialization error, but if so extremely hard to detect.
  // maybe also dependent on optimizer
  //deactivate until a good solution can be found (or the error)
  //BOOST_CHECK_EQUAL(output.solutionHistory.size(), numSteps);
  /*for(unsigned i=0; i<numSteps; i++){
    BOOST_CHECK_CLOSE(output.solutionHistory[i].stages[0].optimizer.objective.grids[0].values[0],
                      refVals[i],
                      1e-5);
  }*/

}
#endif

BOOST_AUTO_TEST_SUITE_END()
