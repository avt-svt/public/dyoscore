#include "InputUpdater.hpp"
#include "InputOutputDummies.hpp"
using namespace DyosOutput;
using namespace FactoryInput;
using namespace std;

BOOST_AUTO_TEST_SUITE(InputUpdaterTests)

BOOST_AUTO_TEST_CASE(testConstraintUpdate)
{
  OptimizerMetaDataInput in = createOptimizerMetaDataStageInput();
  OptimizerStageOutput out = createOptimizerStageOutput();

  InputUpdater updater;
  in = updater.updateOptimizerMetaDataInputStage(in, out);

  for(unsigned i=0; i<in.constraints[0].lagrangeMultiplierVector.size(); i++){
    BOOST_CHECK_CLOSE(in.constraints[0].lagrangeMultiplierVector[i],
                      out.nonLinearConstraints[i].lagrangeMultiplier,
                      1e-10);
  }
}

BOOST_AUTO_TEST_CASE(testConstraintStages)
{
  const unsigned numStages = 3;
  OptimizerMetaDataInput in = createOptimizerMetaDataInput();
  vector<StageOutput> out = createOptimizerOutput();

  InputUpdater updater;
  in = updater.updateOptimizerMetaDataInput(in,out);

  for(unsigned j=0; j<numStages; j++){
    for(unsigned i=0; i<in.stages[j].constraints[0].lagrangeMultiplierVector.size(); i++){
      BOOST_CHECK_CLOSE(in.stages[j].constraints[0].lagrangeMultiplierVector[i],
        out[j].optimizer.nonLinearConstraints[i].lagrangeMultiplier,
        1e-10);
    }
  }
}


BOOST_AUTO_TEST_CASE(testIntegratorMetaDataStage)
{
  IntegratorMetaDataInput in = createIntegratorMetaDataStageInput();
  IntegratorStageOutput out = createIntegratorStageOutput();

  InputUpdater updater;
  in = updater.updateIntegratorMetaDataInputStage(in, out);

  BOOST_CHECK_CLOSE(in.duration.value, out.durations[0].value, 1e-10);
  for(unsigned i=0; i<in.controls[0].grids[0].timePoints.size(); i++){
    BOOST_CHECK_CLOSE(in.controls[0].grids[0].timePoints[i], out.parameters[0].grids[0].gridPoints[i],1e-10);
    BOOST_CHECK_CLOSE(in.controls[0].grids[0].values[i], out.parameters[0].grids[0].values[i],1e-10);
  }
  double totalDuration = 0.0;
  for(unsigned i=0; i<in.controls[0].grids.size(); i++){
    BOOST_CHECK_CLOSE(in.controls[0].grids[i].duration, out.parameters[0].grids[i].duration, 1e-10);
    totalDuration += out.parameters[0].grids[i].duration;
  }
  BOOST_CHECK_CLOSE(out.durations[0].value, totalDuration, 1e-10);
}

BOOST_AUTO_TEST_CASE(testupdateIntegratorMetaDataInput)
{
  IntegratorMetaDataInput in = createIntegratorMetaDataInput();
  vector<StageOutput> out = createOptimizerOutput();
  
  InputUpdater updater;
  in = updater.updateIntegratorMetaDataInput(in,out);

  const unsigned numStages = 3;
  for(unsigned j=0; j<numStages; j++){
    for(unsigned k=0; k<in.stages[j].controls[0].grids.size(); k++){
      for(unsigned i=0; i<in.stages[j].controls[0].grids[k].timePoints.size(); i++){
        BOOST_CHECK_CLOSE(in.stages[j].controls[0].grids[k].timePoints[i], out[j].integrator.parameters[0].grids[k].gridPoints[i],1e-10);
        BOOST_CHECK_CLOSE(in.stages[j].controls[0].grids[k].values[i], out[j].integrator.parameters[0].grids[k].values[i],1e-10);
      }
      BOOST_CHECK_CLOSE(in.stages[j].controls[0].grids[k].duration, out[j].integrator.parameters[0].grids[k].duration, 1e-10);
    }
    for(unsigned i=0; i<in.stages[j].userGrid.size(); i++){
      double val= 0;
      if (i<9){
        val = double(i+1)*5.0/81.0;
      }else{
        val = double(i-8)* 4.0/81. + 5.0/9.0;
      }
      BOOST_CHECK_CLOSE(in.stages[j].userGrid[i], val,1e-10);
    }
  }

}


BOOST_AUTO_TEST_SUITE_END()
