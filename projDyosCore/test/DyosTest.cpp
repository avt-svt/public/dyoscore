/**
* @file DyosTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos testsuite                                                       \n
* =====================================================================\n
* This file contains dyos integration tests                            \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 08.03.2012
*/


/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE Dyos
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>

#include <iostream>

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "ConvertToXmlJson.hpp"
#include <utility>
#include <string>
#include <sstream>

#define THRESHOLD 1e-10

#include "UnitTestsTestsuite.hpp"
#include "SystemtestsTestsuite.hpp" //namespace UserInput etc used in following testsuites is defined here
#include "MultiStageSystemTestsTestsuite.hpp"
#include "MultiGridSystemTestsTestsuite.hpp"
#include "AdaptationSystemTestsTestsuite.hpp"
#include "SecondOrderSystemTestsTestsuite.hpp"
#include "InputUpdaterTestsTestsuite.hpp"
