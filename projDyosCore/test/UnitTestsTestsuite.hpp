#include <iostream>
#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include <utility>
#include <string>
#include "DyosStructureDetectionDummies.hpp"
#include "DyosAdaptationDummies.hpp"
#include "GridRefinementFactory.hpp"

BOOST_AUTO_TEST_SUITE(DyosUnitTests)

BOOST_AUTO_TEST_CASE(testPrintStatus)
{
  AdaptiveDynOptimizationDummies::Ptr adaptDummy(new AdaptiveDynOptimizationDummies());
  adaptDummy->printStatus();
}

/** checks the termination criterion of the adaptation */
BOOST_AUTO_TEST_CASE(testTerminationCondition)
{
  AdaptiveDynOptimizationDummies::Ptr adaptDummy(new AdaptiveDynOptimizationDummies());
  bool criterion = adaptDummy->stoppingCriterionAdaptation();
  BOOST_CHECK_EQUAL(criterion, false);

  adaptDummy->setRefStep(1);
  vector<DyosOutput::Output> solHistory(2);
  solHistory[0].optimizerOutput.optObjFunVal = -1.0;
  solHistory[1].optimizerOutput.optObjFunVal = -2.0;
  adaptDummy->setSolutionHistory(solHistory);
  criterion = adaptDummy->stoppingCriterionAdaptation();
  BOOST_CHECK_EQUAL(criterion, false);

  solHistory[1].optimizerOutput.optObjFunVal = -1.1;
  adaptDummy->setSolutionHistory(solHistory);
  criterion = adaptDummy->stoppingCriterionAdaptation();
  BOOST_CHECK_EQUAL(criterion, false);

  solHistory[1].optimizerOutput.optObjFunVal = -1.009;
  adaptDummy->setSolutionHistory(solHistory);
  criterion = adaptDummy->stoppingCriterionAdaptation();
  BOOST_CHECK_EQUAL(criterion, true);

  solHistory[0].optimizerOutput.optObjFunVal = 0.0001;
  solHistory[1].optimizerOutput.optObjFunVal = 0.0;
  adaptDummy->setSolutionHistory(solHistory);
  criterion = adaptDummy->stoppingCriterionAdaptation();
  BOOST_CHECK_EQUAL(criterion, true);
}

BOOST_AUTO_TEST_CASE(testSolutionHistory)
{
  AdaptiveDynOptimizationDummies::Ptr adaptDummy(new AdaptiveDynOptimizationDummies());
  std::vector<DyosOutput::Output> solHistory = adaptDummy->getSolutionHistory();
  unsigned size = 0;
  BOOST_CHECK_EQUAL(solHistory.size(), size);
}

/* checks directly the adaptation framework like in the signal analysis */
BOOST_AUTO_TEST_CASE(testAdaptGrid)
{
  AdaptiveDynOptimizationDummies::Ptr adaptDummy(new AdaptiveDynOptimizationDummies());
  FactoryInput::ParameterizationGridInput grid;
  grid.hasFreeDuration = false;
  grid.approxType = PieceWiseLinear;
  grid.duration = 30.0;
  grid.adapt.maxAdaptSteps = 3;
  grid.adapt.adaptWave.type = 2; // 2 for linear
  grid.adapt.adaptWave.upperBound = 1000.0;
  grid.adapt.adaptWave.lowerBound = 0.0;
  unsigned numPoints = 5;
  grid.timePoints.resize(numPoints);
  grid.values.resize(numPoints);
  for(unsigned i=0; i<numPoints; i++){
    grid.timePoints[i] = double(i)/double(numPoints);
    grid.values[i] = (i+1) * 3.2;
  }
  grid.timePoints.push_back(1.0);
  grid.values.push_back(1.3);
  FactoryInput::IntegratorMetaDataInput metaData;
  metaData.controls.resize(1);
  metaData.controls[0].type = FactoryInput::ParameterInput::CONTROL;
  metaData.controls[0].grids.push_back(grid);
  vector<vector<GridRefinementInterface::Ptr> > refInterface;
  const unsigned numControls = 1;
  refInterface.resize(numControls);
  for(unsigned j=0; j<numControls; j++){
    const unsigned numGrids = metaData.controls[j].grids.size();
    refInterface[j].resize(numGrids);
    for(unsigned k=0; k<numGrids; k++){
      GridRefinementFactory refineFac;
      refInterface[j][k] = refineFac.createRefinementInterface(metaData.controls[j].grids[k].adapt);
    }
  }
  adaptDummy->adaptControlGridStage(metaData,refInterface);

  grid = metaData.controls[0].grids[0];
  const unsigned num = 10;
  BOOST_CHECK_EQUAL(grid.timePoints.size(), num);
  BOOST_CHECK_EQUAL(grid.values.size(), num);

  const double vals[num] = {3.2, 5.2, 7.2, 9.2, 11.2, 13.2, 15.199999999999, 15.9968749999, 10.47658536585, 1.3};
  const double tPoints[num] = {0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 1.0/2.0+1.0/4.0+1.0/32.0+1.0/64.0+3.0/1024.0 , 0.875 , 1.0};
  for(unsigned i=0; i<num; i++){
    BOOST_CHECK_CLOSE(vals[i], grid.values[i], 1E-7);
    BOOST_CHECK_CLOSE(tPoints[i], grid.timePoints[i], 1E-7);
  }

}

/** test adapt stage function */
BOOST_AUTO_TEST_CASE(testAdaptStage)
{
  AdaptiveDynOptimizationDummies::Ptr adaptDummy(new AdaptiveDynOptimizationDummies());

  adaptDummy->adaptControlGrid();

  ProblemInput input = adaptDummy->getProblemInput();

  const unsigned num = 10;
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[0].controls[0].grids[0].timePoints.size(), num);
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[0].controls[0].grids[0].values.size(), num);

  const double vals[num] = {3.2, 5.2, 7.2, 9.2, 11.2, 13.2, 15.199999999999, 15.9968749999, 10.47658536585, 1.3};
  const double tPoints[num] = {0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 1.0/2.0+1.0/4.0+1.0/32.0+1.0/64.0+3.0/1024.0 , 0.875 , 1.0};


  for(unsigned i=0; i<num; i++){
    BOOST_CHECK_CLOSE(vals[i], input.optimizer.integratorInput.metaDataInput.stages[0].controls[0].grids[0].values[i], 1E-7);
    BOOST_CHECK_CLOSE(tPoints[i], input.optimizer.integratorInput.metaDataInput.stages[0].controls[0].grids[0].timePoints[i], 1E-7);
  }

  const unsigned num2 = 16;
  for(unsigned i=0; i<=num2; i++){
    BOOST_CHECK_CLOSE(double(i)/16.0, input.optimizer.integratorInput.metaDataInput.stages[0].controls[0].grids[1].timePoints[i], 1E-7);
  }
  unsigned mul = 0;
  for(unsigned i=0; i<num2; i++){
    if( i % 2 == 0)
      mul ++;
    BOOST_CHECK_CLOSE(7.2 * mul , input.optimizer.integratorInput.metaDataInput.stages[0].controls[0].grids[1].values[i], 1E-7);
  }

  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[0].controls[1].grids.size(), 0);

  const double vals2[num] = {-3.2, -5.2, -7.2, -9.2, -11.2, -13.2, -15.199999999999, -15.9968749999, -10.47658536585, -1.3};
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[0].controls[2].grids[0].timePoints.size(), num);
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[0].controls[2].grids[0].values.size(), num);

  for(unsigned i=0; i<num; i++){
    BOOST_CHECK_CLOSE(vals2[i], input.optimizer.integratorInput.metaDataInput.stages[0].controls[2].grids[0].values[i], 1E-7);
    BOOST_CHECK_CLOSE(tPoints[i], input.optimizer.integratorInput.metaDataInput.stages[0].controls[2].grids[0].timePoints[i], 1E-7);
  }

  //stage 2
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[1].controls[0].grids[0].timePoints.size(), num);
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[1].controls[0].grids[0].values.size(), num);

  for(unsigned i=0; i<num; i++){
    BOOST_CHECK_CLOSE(vals[i], input.optimizer.integratorInput.metaDataInput.stages[1].controls[0].grids[0].values[i], 1E-7);
    BOOST_CHECK_CLOSE(tPoints[i], input.optimizer.integratorInput.metaDataInput.stages[1].controls[0].grids[0].timePoints[i], 1E-7);
  }

  for(unsigned i=0; i<=num2; i++){
    BOOST_CHECK_CLOSE(double(i)/16.0, input.optimizer.integratorInput.metaDataInput.stages[1].controls[0].grids[1].timePoints[i], 1E-7);
  }
  mul = 0;
  for(unsigned i=0; i<num2; i++){
    if( i % 2 == 0)
      mul ++;
    BOOST_CHECK_CLOSE(7.2 * mul , input.optimizer.integratorInput.metaDataInput.stages[1].controls[0].grids[1].values[i], 1E-7);
  }

  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[1].controls[1].grids.size(), 0);
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[1].controls[2].grids[0].timePoints.size(), num);
  BOOST_CHECK_EQUAL(input.optimizer.integratorInput.metaDataInput.stages[1].controls[2].grids[0].values.size(), num);
  for(unsigned i=0; i<num; i++){
    BOOST_CHECK_CLOSE(vals2[i], input.optimizer.integratorInput.metaDataInput.stages[1].controls[2].grids[0].values[i], 1E-7);
    BOOST_CHECK_CLOSE(tPoints[i], input.optimizer.integratorInput.metaDataInput.stages[1].controls[2].grids[0].timePoints[i], 1E-7);
  }
}



BOOST_AUTO_TEST_SUITE_END()