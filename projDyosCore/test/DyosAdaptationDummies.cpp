#include "DyosAdaptationDummies.hpp"

AdaptiveDynOptimizationDummies::AdaptiveDynOptimizationDummies()
{
  m_maxNumSteps = 0;
  m_curRefStep = 0;
  m_calculateAdjoints = false;
  m_rigorousStoppingCriterion = false;
  m_problemInput.type = ProblemInput::OPTIMIZATION;
  m_problemInput.optimizer.adaptationOptions.adaptStrategy = FactoryInput::AdaptationOptions::ADAPTATION;
  m_problemInput.optimizer.integratorInput.order = FactoryInput::IntegratorInput::FIRST_FORWARD;
  m_problemInput.optimizer.integratorInput.type = FactoryInput::IntegratorInput::NIXE;
  m_problemInput.optimizer.integratorInput.metaDataInput.stages.resize(2);
  m_problemInput.optimizer.integratorInput.metaDataInput.stages[0].controls = prepareControlStage1();
  m_problemInput.optimizer.integratorInput.metaDataInput.stages[1].controls = prepareControlStage1();
  m_problemInput.optimizer.metaDataInput.stages.resize(2);
  resetRefinementInterface();
}


std::vector<FactoryInput::ParameterInput> AdaptiveDynOptimizationDummies::prepareControlStage1(void)
{
  std::vector<FactoryInput::ParameterInput> controls;
  unsigned numControls = 3;
  controls.resize(numControls);

  // control 0
  controls[0].esoIndex = 1;
  controls[0].type = FactoryInput::ParameterInput::CONTROL;
  controls[0].upperBound = 100.0;
  controls[0].lowerBound = 1.0;
  controls[0].grids.resize(2);
  controls[0].grids[0].hasFreeDuration = false;
  controls[0].grids[0].approxType = PieceWiseLinear;
  controls[0].grids[0].duration = 30.0;
  controls[0].grids[0].adapt.maxAdaptSteps = 3;
  controls[0].grids[0].adapt.adaptWave.type = 2; // 2 for linear
  controls[0].grids[0].adapt.adaptWave.lowerBound = controls[0].lowerBound;
  controls[0].grids[0].adapt.adaptWave.upperBound = controls[0].upperBound;
  unsigned numPoints = 5;
  controls[0].grids[0].timePoints.resize(numPoints);
  controls[0].grids[0].values.resize(numPoints);
  for(unsigned i=0; i<numPoints; i++){
    controls[0].grids[0].timePoints[i] = double(i)/double(numPoints);
    controls[0].grids[0].values[i] = double(i+1) * 3.2;
  }
  controls[0].grids[0].timePoints.push_back(1.0);
  controls[0].grids[0].values.push_back(1.3);

  controls[0].grids[1].hasFreeDuration = false;
  controls[0].grids[1].approxType = PieceWiseConstant;
  controls[0].grids[1].duration = 80.0;
  controls[0].grids[1].adapt.maxAdaptSteps = 2;
  controls[0].grids[1].adapt.adaptWave.type = 1; // 1 for constant
  controls[0].grids[1].adapt.adaptWave.lowerBound = controls[0].lowerBound;
  controls[0].grids[1].adapt.adaptWave.upperBound = controls[0].upperBound;
  numPoints = 8;
  controls[0].grids[1].timePoints.resize(numPoints);
  controls[0].grids[1].values.resize(numPoints);
  for(unsigned i=0; i<numPoints; i++){
    controls[0].grids[1].timePoints[i] = double(i)/double(numPoints);
    controls[0].grids[1].values[i] = double(i+1) * 7.2;
  }
  controls[0].grids[1].timePoints.push_back(1.0);
  //controls[0].grids[1].values.push_back(1.8);


  // control 1
  controls[1].esoIndex = 3;
  controls[1].type = FactoryInput::ParameterInput::TIME_INVARIANT;

  // control 2
  controls[2].esoIndex = 4;
  controls[2].type = FactoryInput::ParameterInput::CONTROL;
  controls[2].upperBound = 0.0;
  controls[2].lowerBound = -100.0;
  controls[2].grids.resize(1);
  controls[2].grids[0].hasFreeDuration = false;
  controls[2].grids[0].approxType = PieceWiseLinear;
  controls[2].grids[0].duration = 110.0;
  controls[2].grids[0].adapt.maxAdaptSteps = 1;
  controls[2].grids[0].adapt.adaptWave.type = 2; // 2 for linear
  controls[2].grids[0].adapt.adaptWave.lowerBound = controls[2].lowerBound;
  controls[2].grids[0].adapt.adaptWave.upperBound = controls[2].upperBound;
  numPoints = 5;
  controls[2].grids[0].timePoints.resize(numPoints);
  controls[2].grids[0].values.resize(numPoints);
  for(unsigned i=0; i<numPoints; i++){
    controls[2].grids[0].timePoints[i] = double(i)/double(numPoints);
    controls[2].grids[0].values[i] =  - double(i+1) * 3.2;
  }
  controls[2].grids[0].timePoints.push_back(1.0);
  controls[2].grids[0].values.push_back(- 1.3);

  return controls;
}
