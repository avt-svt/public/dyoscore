/**
* @file DyosAdaptationDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file creates dummies for input and output to test the input     \n
* updater                                                              \n
* =====================================================================\n
* @author Fady Assassa
* @date 05.11.2013
*/

#pragma once
#include "Input.hpp"
#include "GenericEsoFactory.hpp"

using namespace DyosOutput;
using namespace FactoryInput;


OptimizerMetaDataInput createOptimizerMetaDataStageInput()
{
  const int numPoints = 4;
  const int numConstraints = 1;
  const unsigned eIndexConstr(10);
  const double ubound(10.2), lbound(-10.1);
  
  OptimizerMetaDataInput in;
  in.constraints.resize(numConstraints);
  in.constraints[0].esoIndex = eIndexConstr;
  in.constraints[0].lagrangeMultiplierVector.resize(numPoints);
  for(int i=0; i<numPoints; i++)
    in.constraints[0].lagrangeMultiplierVector[i] = 1.2 * (double(i) + 1.0);
  in.constraints[0].lowerBound = lbound;
  in.constraints[0].upperBound = ubound;
  in.constraints[0].type = FactoryInput::ConstraintInput::PATH;
  in.objective.esoIndex = 9;
  in.objective.lagrangeMultiplier = 2.1;
  return in;
}

OptimizerMetaDataInput createOptimizerMetaDataInput()
{
  const unsigned numStages = 3;
  OptimizerMetaDataInput in;
  in.stages.resize(numStages);
  for(unsigned i=0; i<numStages; i++){
    in.stages[i] = createOptimizerMetaDataStageInput();
  }
  return in;
}


OptimizerStageOutput createOptimizerStageOutput()
{
  const int numPoints = 4;
  const int numConstraints = 1;
  const unsigned eIndexConstr(10), eIndexObj(9);
  const double ubound(10.2), lbound(-10.1);
  
  
  OptimizerStageOutput out;
  out.objective.esoIndex = eIndexObj;
  out.objective.lagrangeMultiplier = 3.9;
  out.objective.value = 3.11;

  out.nonLinearConstraints.resize(numConstraints*numPoints);
  for(int i=0; i<numPoints; i++){
    out.nonLinearConstraints[i].esoIndex = eIndexConstr;
    out.nonLinearConstraints[i].lagrangeMultiplier = 0.95 * (double(i) + 1.2);
    out.nonLinearConstraints[i].name = "var";
    out.nonLinearConstraints[i].upperBound = ubound;
    out.nonLinearConstraints[i].lowerBound = lbound;
    out.nonLinearConstraints[i].value = 3.44 * (double(i) + 0.2);
    out.nonLinearConstraints[i].timePoint = double(i+1)/double(numPoints);
  }
  return out;
}


IntegratorMetaDataInput createIntegratorMetaDataStageInput()
{
  IntegratorMetaDataInput in;
  // controls
  const unsigned numControls = 1;
  const unsigned eIndex = 3;
  const unsigned numPoints = 9;
  in.controls.resize(numControls);
  in.controls[0].esoIndex = eIndex;
  const unsigned numGrids = 2;
  in.controls[0].grids.resize(numGrids);
  for(unsigned j=0; j<numGrids; j++){
    in.controls[0].grids[j].timePoints.resize(numPoints);
    in.controls[0].grids[j].values.resize(numPoints);
    for(unsigned i=0; i<numPoints; i++){
      in.controls[0].grids[j].values[i] = double(i+2.1) * 3.9;
      in.controls[0].grids[j].timePoints[i] = double(i) / double(numPoints);
    }
    in.controls[0].grids[j].duration = 23.1;
  }
  // duration
  in.duration.type = FactoryInput::ParameterInput::DURATION;
  in.duration.value = in.controls[0].grids[0].duration + in.controls[0].grids[1].duration;

  // user grid
  const unsigned numPointsUG = numPoints*2;
  in.userGrid.resize(numPointsUG);
  for(unsigned i=0; i<numPointsUG; i++)
    in.userGrid[i] = double(i+1) / double(numPointsUG);

  // eso
  in.esoPtr = NULL;
  return in;
}

IntegratorMetaDataInput createIntegratorMetaDataInput()
{
  const unsigned numStages = 3;
  IntegratorMetaDataInput in;
  in.stages.resize(numStages);
  for(unsigned i=0; i<numStages; i++)
    in.stages[i] = createIntegratorMetaDataStageInput();
  return in;
}

IntegratorStageOutput createIntegratorStageOutput()
{
  IntegratorStageOutput out;
  const unsigned numDurations(1), numParams(1);
  // duration
  out.durations.resize(numDurations);
  out.durations[0].lagrangeMultiplier = 3.1;
  out.durations[0].isTimeInvariant = true;
  out.durations[0].value = 220.1;

  // control
  out.parameters.resize(numParams);
  out.parameters[0].esoIndex = 3;
  out.parameters[0].name = "controlvar";
  const unsigned numGrids = 2;
  vector<double> durs(numGrids);
  durs[0] = out.durations[0].value / 1.8;
  durs[1] = out.durations[0].value - durs[0];
  out.parameters[0].grids.resize(numGrids);
  for(unsigned j=0; j<numGrids; j++){
    out.parameters[0].grids[j].approximation = DyosOutput::ParameterGridOutput::PieceWiseConstant;
    out.parameters[0].grids[j].duration = durs[j];
    const unsigned numPoints = 9;
    out.parameters[0].grids[j].gridPoints.resize(numPoints);
    out.parameters[0].grids[j].values.resize(numPoints);
    out.parameters[0].grids[j].lagrangeMultiplier.resize(numPoints);
    for(unsigned i=0; i<numPoints; i++){
      out.parameters[0].grids[j].gridPoints[i] = double(i) / double(numPoints);
      out.parameters[0].grids[j].values[i] = double(i*i+2.1) + double(i*2);
      out.parameters[0].grids[j].lagrangeMultiplier[i] = double(i)/1.7 + 0.2;
    }
    out.parameters[0].grids[j].gridPoints.push_back(1.0);
    out.parameters[0].grids[j].values.push_back(out.parameters[0].grids[0].values.back());
  }
  return out;
}



std::vector<StageOutput> createOptimizerOutput()
{
  const unsigned numStages = 3;
  std::vector<StageOutput> out(numStages);
  for(unsigned i=0; i<numStages; i++){
    out[i].optimizer = createOptimizerStageOutput();
    out[i].integrator = createIntegratorStageOutput();
  }
  return out;
}
