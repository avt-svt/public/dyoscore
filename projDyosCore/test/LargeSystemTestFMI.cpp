/**
* @file DyosTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Dyos testsuite                                                       \n
* =====================================================================\n
* This file contains dyos large system fmi tests - williams otto       \n
* =====================================================================\n
* @author Adrian Caspari
* @date 19.05.2018
*/

#include <iostream>

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "ConvertToXmlJson.hpp"



#include "FMILargeSystemTestConfig.hpp"

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE Dyos
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>
using namespace UserInput;
BOOST_AUTO_TEST_SUITE(LargeSystemTestsFMI)


BOOST_AUTO_TEST_CASE(LargeSystemFmiAdaptationWo)
{
  unsigned numStages = 1;
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoInput::FMI;
    esoInput.model = PATH_FMI_LARGE_SYSTEM_TEST_FMI;
	esoInput.relativeFmuTolerance = 1e-6;
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0 / double(numStages);
      stageDuration.upperBound = 1000.0 / double(numStages);
      stageDuration.value = 1000.0 / double(numStages);
      stageDuration.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.resize(numIntervals+1 , 5.784);
        grids[0].adapt.maxAdaptSteps = 4;
        grids[0].adapt.adaptWave.minRefinementLevel = 1;
        grids[0].adapt.adaptWave.horRefinementDepth = 0;
        grids[0].pcresolution = 2;
        parameters[0].grids = grids;

        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals+1, 100e-3);
        grids[0].adapt.maxAdaptSteps = 4;
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;
	  
      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 1;

  for(unsigned i=0; i<numStages; i++){
    input.stages.push_back(stageInput);
    input.stages[i].treatObjective = false;
    if(i<numStages-1)
      input.stages[i].mapping.fullStateMapping = true;
  }
  input.stages.back().treatObjective = true;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-8;
  input.runningMode = Input::SINGLE_SHOOTING; //SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.daeInit.linSolver.type = LinearSolverInput::KLU;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::IPOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPTATION;    //ADAPTATION;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-4";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-4";
  input.integratorInput.integratorOptions["forward sensitivity method"] = "staggered"; // staggered , simultaneous
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-6";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "250";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  input.optimizerInput.optimizerOptions["function precision"] = "1.e-6";
  input.optimizerInput.optimizerOptions["elastic weight"] = "1.e+6";
  input.optimizerInput.optimizerOptions["warm start"];

  //input.integratorInput.daeInit.type = UserInput::DaeInitializationInput::FMI;

  UserOutput::Output output;
  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }
 
 }
 
BOOST_AUTO_TEST_SUITE_END()
