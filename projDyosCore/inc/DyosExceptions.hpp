/**
* @file MetaDataException.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData Exceptions - Part of DyOS                                   \n
* =====================================================================\n
* This file contains the Exceptions used in MetaData classes           \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 2012-10-16
*/

#include <exception>

class DyosException : public std::exception {
protected:
  std::string m_msg;
public:
  DyosException(const std::string& msg) : m_msg(msg) {}
  virtual ~DyosException() throw() {}
  virtual const char* what() const throw() {
    return m_msg.c_str();
  }
};

class NoOutputException : public DyosException
{
public:
  NoOutputException() : DyosException("No dyos output producable") {}
  NoOutputException(std::exception &e) :  
    DyosException("No dyos output producable because of following error:\n")
  {
    m_msg.append(e.what());
  }
};
