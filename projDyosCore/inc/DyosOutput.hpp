/**
* @file DyosOutput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the output struct containing the output data of a \n
* DyOS run                                                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 06.09.2012
* @sa MetaDataOutput.hpp
*/
#pragma once

#include "MetaDataOutput.hpp"
#include "OptimizerOutput.hpp"

//must not include "Input.hpp" (leads to cirlular includes and produces lots of errors)
struct ProblemInput;

namespace DyosOutput{
  struct Output
  {
    std::vector<Output> solutionHistory;
    std::vector<ProblemInput> inputHistory;
    std::vector<StageOutput> stageOutput;
    OptimizerOutput optimizerOutput;
    /// solution time
    double solutionTime;
  };
  

}