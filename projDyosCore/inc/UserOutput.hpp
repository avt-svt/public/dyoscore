/**
* @file UserOutput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the output structs and enums that show the        \n
* problem output constructed from the user inputs.                     \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 22.06.2012
* @sa Input.hpp EsoInput.hpp MetaDataInput.hpp IntegratorInput.hpp OptimizerInput.hpp
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <cfloat>
#include "GenericEso.hpp"

#include "HaveFmuConfig.hpp"

#ifndef DYOS_DBL_MAX
#define DYOS_DBL_MAX 1e300
#endif

#ifdef WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT 
#endif

namespace UserOutput
{
//Using DYOS_DBL_MAX instead of DBL_MAX avoids errors due to rounding
  /**
  * @struct EsoOutput
  * @brief
  */
  //struct DLLEXPORT EsoOutput
  struct EsoOutput
  {
    /// @brief type of the Eso
    enum EsoType{
		JADE ///Esotype for Modelica/JADE ESO
	#ifdef BUILD_WITH_FMU
	 ,
	FMI // Esotype for FMI ESO
	#endif   
	};

    std::string model; ///< name of the model to be loaded
    EsoType type; ///< type of the GenericEso to be created (default: JADE)
    /// @brief standard constructor presetting type

	// in case of FMU we need to give the relative tolerance of the fmu.
#ifdef BUILD_WITH_FMU
	double relativeFmuTolerance = 0;
#endif

    EsoOutput(): model(""), type(JADE){}
    DLLEXPORT bool operator==(const EsoOutput &other)const;
  };

  /**
  * @struct MappingOutput
  * @brief
  */
  struct MappingOutput
  {
    bool fullStateMapping;
    std::map<std::string, std::string> stateNameMapping; ///< mapping of one stage to another
    /// standard constructor predefining fullStateMapping
    DLLEXPORT bool operator==(const MappingOutput &other)const;
    MappingOutput():fullStateMapping(false){};
  };

  /**
  * @struct StateGridOutput
  * @brief
  */
  struct StateGridOutput
  {
    std::vector<double> timePoints;
    std::vector<double> values;
    DLLEXPORT bool operator==(const StateGridOutput &other)const;
  };

  /**
  * @struct StateOutput
  * @brief
  */
  struct StateOutput
  {
    std::string name; ///<variable name of the state output
    int esoIndex;
    StateGridOutput grid;
    DLLEXPORT bool operator==(const StateOutput &other)const;
    StateOutput():name(""),esoIndex(0){};
  };

  /**
  * @struct FirstSensitivityOutput
  * @brief
  */
  struct  FirstSensitivityOutput
  {
    std::map<int, int> mapParamsCols;
    std::map<std::string, int> mapConstrRows;
    std::vector<std::vector<double> >  values;
    DLLEXPORT bool operator==(const FirstSensitivityOutput &other)const;
  };

  /**
  * @struct ParameterGridOutput
  * @brief information provided by user concerning the grid of a parameter
  */
  struct ParameterGridOutput
  {
    /// @brief approximation type
    enum ApproximationType{
      PIECEWISE_CONSTANT, /// grid is piecewise constant
      PIECEWISE_LINEAR /// grid is piecewise linear
    };

    unsigned numIntervals; ///< number of intervals for the grid
    std::vector<double> timePoints; ///< explicit grid
    std::vector<double> values; ///< final values of the parameters
    std::vector<double> lagrangeMultipliers; ///< lagrange multipliers of the control
    bool isFixed; /// is set by structure detection. e.g. if control is on bound
    double duration; /// duration of the parameterGrid
    bool hasFreeDuration; ///< flag defining whether duration is fixed or not
    ApproximationType type; ///< approximation type of the grid (default: PIECEWISE_CONSTANT)
    std::vector <FirstSensitivityOutput>  firstSensitivities;
    DLLEXPORT bool operator==(const ParameterGridOutput &other)const;
    /// standard constructor presetting type
    ParameterGridOutput():
    numIntervals(0),isFixed(false),duration(0.0), hasFreeDuration(false), type(PIECEWISE_CONSTANT){}
  };

  /**
  * @struct ParameterOutput
  * @brief
  */
  struct ParameterOutput
  {
    /// @brief type of the parameter
    enum ParameterType{
      INITIAL, /// defines an initial value parameter
      TIME_INVARIANT, /// define a time invariant control
      PROFILE, ///define a control parameter with adapted grid
      DURATION ///define a final time parameter(set only for duration in IntegratorStageInput
    };

    /// @brief type of the parameter sensitivity information
    enum ParameterSensitivityType{
      /// full sensitivity information collected (sensitivities and decision variable, full Hessian)
      FULL,
      /// only partly sensitivity information collected (only mixed Lagrange derivatives, constraint derivatives)
      FRACTIONAL,
      /// no sensitivity information calculated
      NO
    };

    std::string name; ///< variable name of the parameter
    /// flag stating whether a parameter (time invariant, duration) is an optimization parameter
    bool isOptimizationParameter;
    double lowerBound; ///< lower bound of the parameter
    double upperBound; ///< upper bound of the parameter
    double value; ///< starting value of the parameter (for duration, time_invariant, initial)
    std::vector<ParameterGridOutput> grids; ///< parameterization grid of the parameter (profile only)
    ParameterType paramType;///< type of the parameter (default: PROFILE)
    ParameterSensitivityType sensType; ///< type of the sensitivity information (default: FULL)
    double lagrangeMultiplier;

    DLLEXPORT bool operator==(const ParameterOutput &other)const;
    /// @brief standard constructor presetting paramType and sensType
    ParameterOutput(): name(""), isOptimizationParameter(true), lowerBound(-DYOS_DBL_MAX),
      upperBound(DYOS_DBL_MAX), value(0.0), paramType(PROFILE), sensType(FULL), lagrangeMultiplier(0.0){}
  };

  /**
  * @struct IntegratorStageOutput
  * @brief
  */
  struct IntegratorStageOutput
  {
    std::vector<ParameterOutput> durations; ///< grid duration parameters
    std::vector<ParameterOutput> parameters; ///< initial value and control parameters
    std::vector<StateOutput>  states;
    std::vector<StateGridOutput> adjoints;
    StateGridOutput adjoints2ndOrder;
    DLLEXPORT bool operator==(const IntegratorStageOutput &other)const;
  };

  /**
  * @struct ConstraintGridOutput
  * @brief
  */
  struct ConstraintGridOutput
  {
    enum  ConstraintOutputType{
      UPPER_BOUND,
      LOWER_BOUND,
      INFEASIBLE,
      ACTIVE
    };
    std::vector<double> timePoints;
    std::vector<double> values;
    std::vector<double> lagrangeMultiplier;
    DLLEXPORT bool operator==(const ConstraintGridOutput &other)const;
  };

  /**
  * @struct ConstraintOutput
  * @brief
  */
  struct ConstraintOutput
  {
     /// @brief type of the constraint
    enum ConstraintType{
      PATH, /// define a path constraint
      POINT, /// define a point constraint
      ENDPOINT /// define an endpoint constraint
    };

    std::string name; ///< variable name of the constraint
    double lowerBound; ///< lower bound of the constraint
    double upperBound; ///< upper bound of the constraint
    std::vector<ConstraintGridOutput> grids;///////////////////////////////////////////stoppthere!!!!
    ConstraintType type; ///< type of the constraint (default: POINT)
    DLLEXPORT bool operator==(const ConstraintOutput &other)const;
    ConstraintOutput(): name(""), lowerBound(-DYOS_DBL_MAX), upperBound(DYOS_DBL_MAX), type(PATH)
    {}
  };

  /**
  * @struct OptimizerStageOutput
  * @brief
  */
  struct OptimizerStageOutput
  {
    ConstraintOutput objective; ///< objective information (interpreted as a constraint)
    std::vector<ConstraintOutput> nonlinearConstraints;
    std::vector<ConstraintOutput> linearConstraints;
    DLLEXPORT bool operator==(const OptimizerStageOutput &other)const;
  };

  /**
  * @struct StageOutput
  * @brief
  */
  struct StageOutput
  {
    bool treatObjective; ///< flag whether the objective (last stage is expected to be true)
    MappingOutput mapping;
    EsoOutput eso; ///< eso information
	GenericEso::Ptr esoPtr;
    IntegratorStageOutput integrator; ///< integrator information
    OptimizerStageOutput optimizer; ///< optimizer information
    std::vector<double> stageGrid; ///< global grid for states and constraints
    DLLEXPORT bool operator==(const StageOutput &other)const;
    StageOutput():treatObjective(true){}
  };

  /**
  * @struct IntegratorOutput
  * @brief
  */
  struct IntegratorOutput
  {
    ///@brief type of the integrator
    enum IntegratorType{
      NIXE, /// select integrator NIXE
      LIMEX, /// select integrator Limex
      IDAS /// select integrator IDAS
    };
    ///@brief integration order of the problem (so far needed only by NIXE)
    enum IntegrationOrder{
      ZEROTH, ///zeroth order (no sensitivities)
      FIRST_FORWARD, /// forward integration including sensitivities
      FIRST_REVERSE, /// reverse integration with first order derivatives only
      SECOND_REVERSE /// reverse integration including second order derivatives
    };
    IntegratorType type;
    IntegrationOrder order;
    DLLEXPORT bool operator==(const IntegratorOutput &other)const;
    ///@brief standard constructor presetting type and order
    IntegratorOutput():type(NIXE), order(ZEROTH){}
  };

  /**
  * @struct SecondOrderOutput
  */
  struct SecondOrderOutput
  {
    std::map<std::string, int> indicesHessian;
    std::vector<std::vector<double> > values;
    std::vector<std::vector<double> > constParamHessian;
    std::vector<std::vector<double> > compositeAdjoints;
    DLLEXPORT bool operator==(const SecondOrderOutput &other)const;
  };

  /**
  * @struct OptimizerOutput
  * @brief
  */
  struct OptimizerOutput
  {
    ///@brief type of the optimizer
    enum OptimizerType{
      SNOPT, /// select optimizer SNOPT
      IPOPT, /// select optimizer IPOPT
      NPSOL, /// select optimizer NPSOL
      FILTER_SQP, /// select optimizer FILTER_SQP
	  SENSITIVITY_INTEGRATION ///select sensitivity integration
    };

    enum ResultFlag{
      OK,
      TOO_MANY_ITERATIONS,
      INFEASIBLE,
      WRONG_GRADIENTS,
      NOT_OPTIMAL,
      FAILED
    };

    OptimizerType type; ///< type of the optimizer (default: SNOPT)
    ResultFlag resultFlag;
    std::vector<ConstraintOutput> globalConstraints;
    std::vector<ConstraintOutput> linearConstraints;
    double objVal;
     //! intermediate constraint violation
    double intermConstrVio;

    SecondOrderOutput secondOrder;
    DLLEXPORT bool operator==(const OptimizerOutput &other)const;
    OptimizerOutput():type(SNOPT), resultFlag(FAILED),objVal(0.0), intermConstrVio(0.0){}
  };

  /**
  * @struct Output
  * @brief
  */
  struct Output
  {
    //use only one enum of that sort?
    ///@brief mode in which dyos is run
    enum RunningMode{
      SIMULATION, /// integrate only
      SINGLE_SHOOTING, /// do an optimization using single shooting
      MULTIPLE_SHOOTING, ///< do an optimization using multiple shooting
      SENSITIVITY_INTEGRATION ///< integrate with sensitivy calculation (optimizer data needed)
    };
    std::vector<Output> solutionHistory;
    std::vector<StageOutput> stages;
    double totalEndTimeLowerBound;
    double totalEndTimeUpperBound;
    double solutionTime;
	bool finalIntegration;
    RunningMode runningMode;
    IntegratorOutput  integratorOutput;
    OptimizerOutput optimizerOutput;
    ///@brief standard constructor presetting runningMode and total endtime bounds
    Output():totalEndTimeLowerBound(-DYOS_DBL_MAX), totalEndTimeUpperBound(DYOS_DBL_MAX),
      runningMode(SINGLE_SHOOTING), finalIntegration(true)
    {}
    DLLEXPORT bool operator==(const Output &other)const;
  };




  struct TestVector
  {
    std::string str;
    int a;
  };
}
