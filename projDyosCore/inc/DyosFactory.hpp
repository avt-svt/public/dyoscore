/**
* @file DyosFactory.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosFactory                                                          \n
* =====================================================================\n
* This file creates the DyOS user interface for simulation and         \n
* optimization.                                                        \n
* =====================================================================\n
* @author Fady Assassa
* @date 17.09.2012
*/

#pragma once
#include "DyosInterface.hpp"
#include "Input.hpp"
#include "DyosObject.hpp"

/** @class DyOSFactory
 * @brief creates the user interface of DyOS
 */
class DyOSFactory : public DyosObject
{
public:
  DyOS::Ptr createDyOSproblem(const ProblemInput &problemInput);
};
