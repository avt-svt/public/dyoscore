/**
* @file DyosStructureDetection.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the dyos algorithms for     structure detection   \n
* =====================================================================\n
* @author Fady Assassa
* @date 03.12.2012
*/

#pragma once
#include "DyosInterface.hpp"


/** @class StrucDetectDynOpt
  * @brief solve the structure detection described by Martin Schlegel
  */
class DyosStructureDetection : public IterativeDynOpt
{
protected:
  DyosStructureDetection(){};
  bool stoppingCriterionAdaptation();
  bool stoppingCriterionStructureDetection();
  void printStatus()const;
public:
  typedef boost::shared_ptr<DyosStructureDetection> Ptr; ///! boost shared pointer
  virtual void run(DyosOutput::Output &out);
  DyosStructureDetection(const GenericOptimizer::Ptr &optimizer,
                         const ProblemInput &problemInput);
};
