/**
* @file DyosStructureDetection.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the algorithms for structure detection            \n
* =====================================================================\n
* @author Fady Assassa
* @date 03.12.2012
*/

//#include <cassert>
#include "DyosStructureDetection.hpp"

//#include "UtilityFunctions.hpp"

/** @brief creates the Structure detection class and sets the maximum number of structure
  * detection steps
  */
DyosStructureDetection::DyosStructureDetection(const GenericOptimizer::Ptr &optimizer,
                                               const ProblemInput &problemInput)
                         : IterativeDynOpt(optimizer, problemInput)
{
  // this only determines the largest maxSteps in all control and all grids
  const unsigned numStages = problemInput.optimizer.integratorInput.metaDataInput.stages.size();
  for(unsigned i=0; i<numStages; i++){
    FactoryInput::OptimizerMetaDataInput iStage;
    iStage =  problemInput.optimizer.metaDataInput.stages[i];
    unsigned maxSteps = iStage.structureDetection.maxStructureSteps;
    if(maxSteps > m_maxNumSteps){
          m_maxNumSteps = maxSteps;
    }
  }
}

/** @brief runs the structure detection loop with adaptation (Step 2)
 */
void DyosStructureDetection::run(DyosOutput::Output &out)
{
  structureDetectionLoop(out);
}

void DyosStructureDetection::printStatus()const
{
  std::stringstream ss;
  ss << "    Structure detection step "<< m_curRefStep + 1 <<"   \n";
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "\n\n\n--------------------------------\n" );
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, ss.str() );
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "--------------------------------\n" );
}

/** @brief this function does nothing and should never be called*/
bool DyosStructureDetection::stoppingCriterionAdaptation()
{
  // you should never get here.
  assert(false); 
  return false;
}

bool DyosStructureDetection::stoppingCriterionStructureDetection()
{
  bool criterionMet = false;
  if(m_rigorousStoppingCriterion){
    criterionMet = determineAdjointError();
  }else{
    criterionMet = determineObjectiveError();
    criterionMet = m_isICVfulfilled && criterionMet;
  }
  return criterionMet;
}
