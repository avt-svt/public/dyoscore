/**
* @file DyosAdaptation.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the algorithms for wavelet based                  \n
* =====================================================================\n
* @author Fady Assassa
* @date 03.12.2012
*/


#include <cassert>
#include "DyosAdaptation.hpp"


/** @brief constructur sets the maximum number of refinement steps
  * @param[in] optimizer object to generic optimizer
  * @param[in] problemInput contains the full FactoryInput required to build a problem
  */
DyosGridAdaptation::DyosGridAdaptation(const GenericOptimizer::Ptr &optimizer,
                                       const ProblemInput &problemInput) 
                                       : IterativeDynOpt(optimizer, problemInput)
{
  // this only determines the largest maxSteps in all control and all grids
  const unsigned numStages = problemInput.optimizer.integratorInput.metaDataInput.stages.size();
  for(unsigned i=0; i<numStages; i++){
    FactoryInput::IntegratorMetaDataInput iStage;
    iStage =  problemInput.optimizer.integratorInput.metaDataInput.stages[i];
    const unsigned numControls = iStage.controls.size();
    for(unsigned j=0; j<numControls; j++){
      const unsigned numGrids = iStage.controls[j].grids.size();
      for(unsigned k=0; k<numGrids; k++){
        unsigned maxSteps = iStage.controls[j].grids[k].adapt.maxAdaptSteps;
        if(maxSteps > m_maxNumSteps){
          m_maxNumSteps = maxSteps;
        }
      }
    }
  }
}

/** @brief runs the dynamic optimization problem with grid adaptation
 *  @param[out] output of DyOS
 */
void DyosGridAdaptation::run(DyosOutput::Output &out)
{
  IterativeDynOpt::controlGridAdaptationLoop(out);
}

bool DyosGridAdaptation::stoppingCriterionAdaptation()
{
  bool criterionMet = false;
  if(m_rigorousStoppingCriterion){
    criterionMet = determineAdjointError();
  }else{
    criterionMet = determineObjectiveError() ;
    criterionMet = m_isICVfulfilled && criterionMet;
  }
  return criterionMet;
}

/** @brief prints some information on screen
  * @param[in] iter iteration of the adaptation
  */
void DyosGridAdaptation::printStatus()const
{
  std::stringstream ss;
  ss << "     Grid refinement step " << m_curRefStep + 1 <<"  \n" ;
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "\n\n\n--------------------------------\n" );
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, ss.str() );
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "--------------------------------\n" );
}



bool DyosGridAdaptation::stoppingCriterionStructureDetection()
{
  // you should never get here.
  assert(false); 
  return false;
}
