/**
* @file Dyos.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the definition of the dyos main function          \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 02.03.2012
*/


#define MAKE_DYOS_DLL
#include <cassert>

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "DyosFactory.hpp"
#include "InputOutputConversions.hpp"
#include "DyosFormatter.hpp"
#include "InputUpdater.hpp"

// modules subfunctions
ProblemInput createProblemInput(const struct UserInput::Input &input,
                                const Logger::Ptr &logger);
DyosOutput::Output runProblem(const struct ProblemInput &problemInput,
                              const Logger::Ptr &logger);
UserOutput::Output convertDyosOutput(const struct UserInput::Input input,
                                     DyosOutput::Output dyosOut,
                                     const Logger::Ptr &logger);
void printUserOutput(UserOutput::Output out, const Logger::Ptr &logger);

DyosOutput::Output updateDyosOutput(const DyosOutput::Output formerDyosOut,
                                    const DyosOutput::Output latterDyosOut);

LINKDLL UserOutput::Output runDyos(const struct UserInput::Input &input)
{
  Logger::Ptr logger(new StandardLogger());
  return runDyos(input, logger);
}

/**
* @brief dyos main function
*
* @param[in] input UserInput struct containing all plroblem information
* @param[out] output UserOutputDummy struct containing the results of the function
*/
LINKDLL UserOutput::Output runDyos(const struct UserInput::Input &input,
                                   const Logger::Ptr &logger)
{
  JsonInputFormatter jsonInputFormatter(input);
  logger->sendMessage(Logger::INPUT_OUT_JSON, OutputChannel::STANDARD_VERBOSITY,
                      jsonInputFormatter);
  XmlInputFormatter xmlInputFormatter(input);
  logger->sendMessage(Logger::INPUT_OUT_XML, OutputChannel::STANDARD_VERBOSITY,
                      xmlInputFormatter);
  logger->flushAll();
  DyosOutput::Output dyosOut;
  dyosOut.solutionTime = 0.0;

  try{

    ProblemInput problemInput = createProblemInput(input, logger);
    
    InputUpdater updater;
    if(input.runningMode == UserInput::Input::MULTIPLE_SHOOTING){
      //run simulation first and adjust ProblemInput of multiple shooting problem
      UserInput::Input simulationCopy = input;
      simulationCopy.runningMode = UserInput::Input::SIMULATION;
      simulationCopy.integratorInput.order = UserInput::IntegratorInput::ZEROTH;
      ProblemInput simulationInput = createProblemInput(simulationCopy, logger);
      dyosOut = runProblem(simulationInput, logger);
      
      //update original problemInput by writing trajectories into initial value parameters
      problemInput = updater.updateMultipleShootingInitials(problemInput, dyosOut);
    }
    dyosOut = runProblem(problemInput, logger);
    
    DyosOutput::Output firstDyosOut = dyosOut;
    
    //write results to problem input
    updater.setLogger(logger);
    if(!dyosOut.inputHistory.empty()){
      problemInput = dyosOut.inputHistory.back();
    }
    
    //no final integration needed if only sensitivity integration or simulation is done
	//no finla integration is done of finalIntegration is set to false
    if(problemInput.optimizer.type != FactoryInput::OptimizerInput::FINAL_INTEGRATION && input.finalIntegration == true && !(input.runningMode == input.SIMULATION || input.runningMode == input.SENSITIVITY_INTEGRATION)){
      // the final integration overrides the solution time. So we add it after the final integration.

      problemInput = updater.updateProblemInput(problemInput, dyosOut);

      //simulate with optimization result
      problemInput.optimizer.type = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
      problemInput.optimizer.adaptationOptions.adaptStrategy = FactoryInput::AdaptationOptions::NOADAPTATION;
      
      dyosOut = runProblem(problemInput, logger);
      dyosOut = updateDyosOutput(firstDyosOut, dyosOut);

    }
  }
  catch(...){} //no further exception handling needed


  UserOutput::Output output = convertDyosOutput(input, dyosOut, logger);
  if(!output.solutionHistory.empty()){
    output.solutionTime = 0.0;
    for(unsigned i=0; i<output.solutionHistory.size(); i++){
      output.solutionTime += output.solutionHistory[i].solutionTime;
    }
  }
  
  
  std::stringstream ss;
  ss <<" Solution time: \t " << output.solutionTime << " seconds" << std::endl;
  logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, ss.str());
  printUserOutput(output, logger);
  return output;
}

ProblemInput createProblemInput(const struct UserInput::Input &input,
                                const Logger::Ptr &logger)
{
  ProblemInput problemInput;
  try{
    InputConverter inputConverter;
    inputConverter.setLogger(logger);
    problemInput = inputConverter.convertInput(input);
  }
  catch(InputException &e){
    std::string errorMessage("exception thrown in input section\n");
    errorMessage.append(e.what());
    logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, errorMessage);
    logger->newlineAndFlush(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
    throw;
  }
  catch(std::exception &e){
    std::string errorMessage(e.what());
    logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, errorMessage);
    logger->newlineAndFlush(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
    logger->flushAll();
    throw;
  }
  catch(...){
    logger->sendMessage(Logger::DYOS_OUT,
                        OutputChannel::ERROR_VERBOSITY,
                        "unknown error occurred");
    logger->newlineAndFlush(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
    logger->flushAll();
    throw;
  }
  return problemInput;
}

DyosOutput::Output runProblem(const struct ProblemInput &problemInput,
                              const Logger::Ptr &logger)
{
  DyosOutput::Output dyosOut;
  dyosOut.solutionTime = 0.0;
  try{
    DyOSFactory dyosFactory;
    dyosFactory.setLogger(logger);
    const DyOS::Ptr dyos = dyosFactory.createDyOSproblem(problemInput);


    dyos->run(dyosOut);
  }
  catch(std::exception &e){
    std::string errorMessage(e.what());
    logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, errorMessage);
    logger->newlineAndFlush(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
    logger->flushAll();
    throw;
  }
  catch(...){
    logger->sendMessage(Logger::DYOS_OUT,
                        OutputChannel::ERROR_VERBOSITY,
                        "unknown error occurred");
    logger->newlineAndFlush(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
    logger->flushAll();
    throw;
  }
  return dyosOut;
}

UserOutput::Output convertDyosOutput(const struct UserInput::Input input,
                                     DyosOutput::Output dyosOut,
                                     const Logger::Ptr &logger)
{
  IOConversions ioConverter;
  ioConverter.setLogger(logger);
  return ioConverter.i2oConvertOutput(input, dyosOut);
}

void printUserOutput(UserOutput::Output output, const Logger::Ptr &logger)
{
  logger->newline(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);
  UserOutputFormatter userOutputFormatter(output);
  logger->sendMessage(Logger::FINAL_OUT,
                      OutputChannel::STANDARD_VERBOSITY,
                      userOutputFormatter);
  StateNamesFormatter stateNamesFormatter(output);
  logger->sendMessage(Logger::STATENAMES_OUT,
                      OutputChannel::STANDARD_VERBOSITY,
                      stateNamesFormatter);
  FinalStatesFormatter finalStateFormatter(output);
  logger->sendMessage(Logger::FINALSTATES_OUT,
                      OutputChannel::STANDARD_VERBOSITY,
                      finalStateFormatter);
  JsonFormatter jsonFormatter(output);
  logger->sendMessage(Logger::FINAL_OUT_JSON,
                      OutputChannel::STANDARD_VERBOSITY,
                      jsonFormatter);
  XmlFormatter xmlFormatter(output);
  logger->sendMessage(Logger::FINAL_OUT_XML,
                      OutputChannel::STANDARD_VERBOSITY,
                      xmlFormatter);
  OptimizationOverviewFormatter overviewFormatter(output);
  logger->sendMessage(Logger::DYOS_OUT,
                      OutputChannel::STANDARD_VERBOSITY,
                      overviewFormatter);
  logger->flushAll();
}


LINKDLL UserOutput::Output optimizeFirstForwardWithSecondOrderOutput
                                  (const struct UserInput::Input &input,
                                   const Logger::Ptr &logger)
{
  JsonInputFormatter jsonInputFormatter(input);
  logger->sendMessage(Logger::INPUT_OUT_JSON, OutputChannel::STANDARD_VERBOSITY,
                      jsonInputFormatter);
  XmlInputFormatter xmlInputFormatter(input);
  logger->sendMessage(Logger::INPUT_OUT_XML, OutputChannel::STANDARD_VERBOSITY,
                      xmlInputFormatter);
  DyosOutput::Output dyosOut;
  dyosOut.solutionTime = 0.0;
  try{
     if( input.integratorInput.order != UserInput::IntegratorInput::SECOND_REVERSE
     || input.runningMode == UserInput::Input::SIMULATION){
      logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY,
                          "Error: function was not used for 2nd order optimization.");
     throw;
    }

    //optimize first forward
    UserInput::Input inputCopy = input;
    inputCopy.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
    ProblemInput problemIn = createProblemInput(inputCopy, logger);
    DyosOutput::Output firstDyosOut;
    firstDyosOut = runProblem(problemIn, logger);

    //write results to problem input
    InputUpdater updater;
    updater.setLogger(logger);
    problemIn = updater.updateProblemInput(problemIn, firstDyosOut);

    //simulate second reverse
    problemIn.optimizer.type = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
    problemIn.optimizer.integratorInput.order = FactoryInput::IntegratorInput::SECOND_REVERSE;
    dyosOut = runProblem(problemIn, logger);
    dyosOut = updateDyosOutput(firstDyosOut, dyosOut);
  }
  // use this try-catch block to skip following code,
  // if anything goes wrong
  catch(...){}
  UserOutput::Output output = convertDyosOutput(input, dyosOut, logger);
  printUserOutput(output, logger);
  return output;
}

DyosOutput::Output updateDyosOutput(const DyosOutput::Output formerDyosOut,
                                    const DyosOutput::Output latterDyosOut)
{
  DyosOutput::Output dyosOut = latterDyosOut;
  dyosOut.optimizerOutput.m_informFlag = formerDyosOut.optimizerOutput.m_informFlag;
  dyosOut.solutionHistory = formerDyosOut.solutionHistory;
  dyosOut.inputHistory = formerDyosOut.inputHistory;
  assert(formerDyosOut.stageOutput.size() == latterDyosOut.stageOutput.size());
  for(unsigned i=0; i<dyosOut.stageOutput.size(); i++){
    DyosOutput::StageOutput currentStage = dyosOut.stageOutput[i];
    DyosOutput::StageOutput formerStage = formerDyosOut.stageOutput[i];
    assert(currentStage.integrator.parameters.size()
           == formerStage.integrator.parameters.size());
    for(unsigned j=0; j<currentStage.integrator.parameters.size(); j++){
      DyosOutput::ParameterOutput formerParameter = formerStage.integrator.parameters[j];
      DyosOutput::ParameterOutput currentParameter = currentStage.integrator.parameters[j];
      currentParameter.lagrangeMultiplier = formerParameter.lagrangeMultiplier;
      assert(currentParameter.grids.size() == formerParameter.grids.size());
      for(unsigned k=0; k<formerParameter.grids.size(); k++){
        currentParameter.grids[k].lagrangeMultiplier = formerParameter.grids[k].lagrangeMultiplier;
      }
      dyosOut.stageOutput[i].integrator.parameters[j] = currentParameter;
    }
  }
  return dyosOut;
}
