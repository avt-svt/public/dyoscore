/**
* @file DyosFormatter.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the definition of all DyosFormatter classes.      \n
* These classes manage the formatting of all structs used on to Dyos   \n
* level such as UserInput and UserOutput.                              \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 30.11.2012
*/

#include "DyosFormatter.hpp"
#include "FieldFormatter.hpp"
#include <iomanip>

#include "ConvertToXmlJson.hpp"

DyosFormatter::DyosFormatter()
{
  m_indentationLength = 0;
}

DyosFormatter::DyosFormatter(const unsigned indentationLength)
{
  m_indentationLength = indentationLength;
}


DoubleVectorMatrixFormatter::DoubleVectorMatrixFormatter()
{}

DoubleVectorMatrixFormatter::DoubleVectorMatrixFormatter(
                              const std::vector<std::vector<double> > &vecMatrix,
                              const unsigned indentationLength)
                            : DyosFormatter(indentationLength)
{
  m_vecMatrix = vecMatrix;
}


void DoubleVectorMatrixFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  for(unsigned i=0; i<m_vecMatrix.size(); i++){
    FieldFormatter<double> rowFormatter(m_vecMatrix[i]);
    m_sstream<<indentation<<rowFormatter.toString()<<std::endl;
  }
}


EsoOutputFormatter::EsoOutputFormatter()
{}

EsoOutputFormatter::EsoOutputFormatter(const UserOutput::EsoOutput esoOut,
                                       const unsigned indentationLength)
                                      : DyosFormatter(indentationLength)
{
  m_esoOut = esoOut;
}

void EsoOutputFormatter::printEsoType()
{
  m_sstream<<"Eso type :";
  switch(m_esoOut.type){
    case UserOutput::EsoOutput::JADE:
      m_sstream<<"JADE";
      break;
    default:
      assert(false);
  }
}

void EsoOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Model: "<<m_esoOut.model<<std::endl;
  m_sstream<<indentation;
  printEsoType();
}


MappingOutputFormatter::MappingOutputFormatter()
{}

MappingOutputFormatter::MappingOutputFormatter(const UserOutput::MappingOutput mappingOut,
                                               const unsigned indentationLength)
                                             : DyosFormatter(indentationLength)
{
  m_mappingOut = mappingOut;
}

void MappingOutputFormatter::printStateNameMapping()
{
  MapFormatter<std::string, std::string> mapFormatter(m_mappingOut.stateNameMapping,
                                                      m_indentationLength +2);
  m_sstream<<mapFormatter.toString();
}

void MappingOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  if(m_mappingOut.fullStateMapping){
    m_sstream<<indentation<<"Full state mapping"<<std::endl;
  }
  else{
    m_sstream<<indentation<<"State name mapping: "<<std::endl;
    printStateNameMapping();
  }
}


StateGridOutputFormatter::StateGridOutputFormatter()
{}

StateGridOutputFormatter::StateGridOutputFormatter(const UserOutput::StateGridOutput stateGridOut,
                                                   const unsigned indentationLength)
                                                 : DyosFormatter(indentationLength)
{
  m_stateGridOut = stateGridOut;
}


void StateGridOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  FieldFormatter<double> timePointsFormatter(m_stateGridOut.timePoints);
  FieldFormatter<double> valuesFormatter(m_stateGridOut.values);
  m_sstream<<indentation<<"Time points: "<<timePointsFormatter.toString()<<std::endl;
  m_sstream<<indentation<<"Values: "<<valuesFormatter.toString()<<std::endl;
}

StateOutputFormatter::StateOutputFormatter()
{}


StateOutputFormatter::StateOutputFormatter(const UserOutput::StateOutput stateOut,
                                           const unsigned indentationLength)
                                         : DyosFormatter(indentationLength)
{
  m_stateOut = stateOut;
}

void StateOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"State name: "<<m_stateOut.name<<std::endl;
  m_sstream<<indentation<<"Eso index: "<<m_stateOut.esoIndex<<std::endl;
  StateGridOutputFormatter stateGridOutFormatter(m_stateOut.grid, m_indentationLength + 2);
  m_sstream<<indentation<<"Grid: "<<std::endl;
  m_sstream<<stateGridOutFormatter.toString()<<std::endl;
}


FirstSensitivityOutputFormatter::FirstSensitivityOutputFormatter()
{}

FirstSensitivityOutputFormatter::FirstSensitivityOutputFormatter(
                                  const UserOutput::FirstSensitivityOutput firstSensOut,
                                  const unsigned indentationLength)
                                : DyosFormatter(indentationLength)
{
  m_firstSensOut = firstSensOut;
}


void FirstSensitivityOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  MapFormatter<std::string,int> rowMapFormatter(m_firstSensOut.mapConstrRows,
                                                m_indentationLength + 2);
  MapFormatter<int,int> colMapFomatter(m_firstSensOut.mapParamsCols,
                                               m_indentationLength + 2);
  DoubleVectorMatrixFormatter valueFormatter(m_firstSensOut.values, m_indentationLength + 2);
  m_sstream<<indentation<<"RowMap :"<<std::endl;
  m_sstream<<rowMapFormatter.toString();
  m_sstream<<indentation<<"ColMap :"<<std::endl;
  m_sstream<<colMapFomatter.toString();
  m_sstream<<indentation<<"Sensitivity matrix: "<<std::endl;
  m_sstream<<valueFormatter.toString()<<std::endl;
}



ParameterGridOutputFormatter::ParameterGridOutputFormatter()
{}

ParameterGridOutputFormatter::ParameterGridOutputFormatter(
                               const UserOutput::ParameterGridOutput paramGridOut,
                               const unsigned indentationLength)
                               : DyosFormatter(indentationLength)
{
  m_paramGridOut = paramGridOut;
}

void ParameterGridOutputFormatter::printApproximationType()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Approximation type: ";
  switch(m_paramGridOut.type){
    case UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT:
      m_sstream<<"piecewise constant";
      break;
    case UserOutput::ParameterGridOutput::PIECEWISE_LINEAR:
      m_sstream<<"piecewise linear";
      break;
    default:
      assert(false);
  }
  m_sstream<<std::endl;
}

void ParameterGridOutputFormatter::putDataIntoStream()
{
  std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"number of intervals: "<<m_paramGridOut.numIntervals<<std::endl;
  FieldFormatter<double> timePointsFormatter(m_paramGridOut.timePoints);
  m_sstream<<indentation<<"Time points: "<<timePointsFormatter.toString()<<std::endl;
  FieldFormatter<double> valuesFormatter(m_paramGridOut.values);
  m_sstream<<indentation<<"Values: "<<valuesFormatter.toString()<<std::endl;
  if(!m_paramGridOut.lagrangeMultipliers.empty()){
    FieldFormatter<double> lagrangeMultFormatter(m_paramGridOut.lagrangeMultipliers);
    m_sstream<<indentation<<"Lagrange multiplier: "<<lagrangeMultFormatter.toString()<<std::endl;
  }
  m_sstream<<indentation<<"Duration "<<m_paramGridOut.duration;
  if(m_paramGridOut.hasFreeDuration){
    m_sstream<<" (with sensitivity information)";
  }
  m_sstream<<std::endl;
  printApproximationType();
  m_sstream<<indentation<<"First order sensitivities:"<<std::endl;
  indentation.resize(m_indentationLength + 2, ' ');
  for(unsigned i=0; i<m_paramGridOut.firstSensitivities.size(); i++){
    m_sstream<<indentation<<"Sensitivities on interval "<<(i+1)<<std::endl;
    FirstSensitivityOutputFormatter firstSensFormatter(m_paramGridOut.firstSensitivities[i],
                                                       m_indentationLength + 2);
    m_sstream<<firstSensFormatter.toString();
  }
}


ParameterOutputFormatter::ParameterOutputFormatter()
{}

ParameterOutputFormatter::ParameterOutputFormatter(
                           const UserOutput::ParameterOutput paramOut,
                           const unsigned indentationLength)
                          : DyosFormatter(indentationLength)
{
  m_paramOut = paramOut;
}

void ParameterOutputFormatter::printParameterType()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Parameter type: ";
  switch(m_paramOut.paramType){
    case UserOutput::ParameterOutput::DURATION:
      m_sstream<<"duration";
      break;
    case UserOutput::ParameterOutput::INITIAL:
      m_sstream<<"initial value parameter";
      break;
    case UserOutput::ParameterOutput::PROFILE:
      m_sstream<<"control parameter";
      break;
    case UserOutput::ParameterOutput::TIME_INVARIANT:
      m_sstream<<"time invariant control parameter";
      break;
    default:
      assert(false);
  }
  m_sstream<<std::endl;
}

void ParameterOutputFormatter::printParameterSensitivityType()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation;
  switch(m_paramOut.sensType){
    case UserOutput::ParameterOutput::FULL:
      m_sstream<<"all sensitivity information collected";
      break;
    case UserOutput::ParameterOutput::FRACTIONAL:
      m_sstream<<"only mixed Lagrange derivatives and constraint derivatives collected";
      break;
    case UserOutput::ParameterOutput::NO:
      m_sstream<<"no sensitivity information collected";
  }
  m_sstream<<std::endl;
}


void ParameterOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  if(m_paramOut.name != ""){
    m_sstream<<indentation<<"Parameter name: "<<m_paramOut.name;
  }
  else{
    m_sstream<<indentation<<"Duration parameter";
  }
  if(m_paramOut.isOptimizationParameter){
    m_sstream<<" (with sensitivity information)";
  }
  m_sstream<<std::endl;
  m_sstream<<indentation<<"Bounds: [";
  if(m_paramOut.lowerBound <= -DYOS_DBL_MAX){
    m_sstream<<"-inf";
  }
  else{
    m_sstream<<m_paramOut.lowerBound;
  }
  m_sstream<<",";
  if(m_paramOut.upperBound >= DYOS_DBL_MAX){
    m_sstream<<"inf";
  }
  else{
    m_sstream<<m_paramOut.upperBound;
  }
  m_sstream<<"]"<<std::endl;
  printParameterType();
  printParameterSensitivityType();
  if(m_paramOut.paramType != UserOutput::ParameterOutput::PROFILE){
    m_sstream<<indentation<<"Value: "<<m_paramOut.value<<std::endl;
  }
  if(m_paramOut.isOptimizationParameter && (m_paramOut.grids.empty() || m_paramOut.name == "")){
    m_sstream<<indentation<<"Lagrange multiplier: "<<m_paramOut.lagrangeMultiplier<<std::endl;
  }
  else if(m_paramOut.sensType != UserOutput::ParameterOutput::NO){
    for(unsigned i=0; i<m_paramOut.grids.size(); i++){
      m_sstream<<indentation<<"Grid "<<(i+1)<<std::endl;
      ParameterGridOutputFormatter gridFormatter(m_paramOut.grids[i], m_indentationLength + 2);
      m_sstream<<gridFormatter.toString();
    }
  }
}

IntegratorStageOutputFormatter::IntegratorStageOutputFormatter()
{}

IntegratorStageOutputFormatter::IntegratorStageOutputFormatter(
                                 const UserOutput::IntegratorStageOutput intStageOut,
                                 const unsigned indentationLength)
                               : DyosFormatter(indentationLength)
{
  m_intStageOut = intStageOut;
}

void IntegratorStageOutputFormatter::putDataIntoStream()
{
  std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<m_intStageOut.parameters.size()-1;
  m_sstream<<" Stage duration parameters:"<<std::endl;
  for(unsigned i=0; i<m_intStageOut.durations.size(); i++){
    if(i==0){
     m_sstream<<indentation<<"Global final time "<<std::endl;
    }
    else{
      m_sstream<<indentation<<"Stage duration "<<i<<std::endl;
    }
    ParameterOutputFormatter durationFormatter(m_intStageOut.durations[i],
                                                m_indentationLength + 2);
    m_sstream<<durationFormatter.toString();
  }
  m_sstream<<indentation<<m_intStageOut.parameters.size()<<" Parameters:"<<std::endl;
  indentation.resize(m_indentationLength + 2, ' ');
  for(unsigned i=0; i<m_intStageOut.parameters.size(); i++){
    m_sstream<<indentation<<"Parameter "<<(i+1)<<std::endl;
    ParameterOutputFormatter parameterFormatter(m_intStageOut.parameters[i],
                                                m_indentationLength + 4);
    m_sstream<<parameterFormatter.toString();
  }
  indentation.resize(m_indentationLength);
  m_sstream<<indentation<<m_intStageOut.states.size()<<" States:"<<std::endl;
  indentation.resize(m_indentationLength + 2, ' ');
  for(unsigned i=0; i<m_intStageOut.states.size(); i++){
    m_sstream<<indentation<<"State "<<(i+1)<<std::endl;
    StateOutputFormatter stateFormatter(m_intStageOut.states[i], m_indentationLength + 4);
    m_sstream<<stateFormatter.toString();
  }
}

ConstraintGridOutputFormatter::ConstraintGridOutputFormatter()
{}

ConstraintGridOutputFormatter::ConstraintGridOutputFormatter
                               (const UserOutput::ConstraintGridOutput constraintGridOut,
                                const unsigned indentationLength)
                             : DyosFormatter(indentationLength)
{
  m_constraintGridOut = constraintGridOut;
}

void ConstraintGridOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  FieldFormatter<double> timeFormatter(m_constraintGridOut.timePoints);
  m_sstream<<indentation<<"Time points: "<<timeFormatter.toString()<<std::endl;
  FieldFormatter<double> valueFormatter(m_constraintGridOut.values);
  m_sstream<<indentation<<"Values: "<<valueFormatter.toString()<<std::endl;
  FieldFormatter<double> lagrangeMultFormatter(m_constraintGridOut.lagrangeMultiplier);
  m_sstream<<indentation<<"Lagrange multipliers: "<<lagrangeMultFormatter.toString()<<std::endl;
}


ConstraintOutputFormatter::ConstraintOutputFormatter()
{}

ConstraintOutputFormatter::ConstraintOutputFormatter(
                            const UserOutput::ConstraintOutput constraintOut,
                            const unsigned indentationLength)
                          : DyosFormatter(indentationLength)
{
  m_constraintOut = constraintOut;
}

void ConstraintOutputFormatter::printConstraintType()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation;
  switch(m_constraintOut.type){
    case UserOutput::ConstraintOutput::ENDPOINT:
      m_sstream<<"Endpoint constraint"<<std::endl;
      break;
    case UserOutput::ConstraintOutput::PATH:
      m_sstream<<"Path constraint"<<std::endl;
      break;
    case UserOutput::ConstraintOutput::POINT:
      m_sstream<<"Point constraint"<<std::endl;
      break;
    default:
      assert(false);
  }
  
}
void ConstraintOutputFormatter::putDataIntoStream()
{
  std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Constraint "<<m_constraintOut.name<<std::endl;
  printConstraintType();
  m_sstream<<indentation<<"Bounds: [";
  if(m_constraintOut.lowerBound <= -DYOS_DBL_MAX){
    m_sstream<<"-inf";
  }
  else{
    m_sstream<<m_constraintOut.lowerBound;
  }
  m_sstream<<",";
  if(m_constraintOut.upperBound >= DYOS_DBL_MAX){
    m_sstream<<"inf";
  }
  else{
    m_sstream<<m_constraintOut.upperBound;
  }
  m_sstream<<"]"<<std::endl;
  
  m_sstream<<indentation<<m_constraintOut.grids.size()<<" Grids:"<<std::endl;
  indentation.resize(m_indentationLength + 2, ' ');
  for(unsigned i=0; i<m_constraintOut.grids.size(); i++){
    m_sstream<<indentation<<"Grid "<<(i+1)<<std::endl;
    ConstraintGridOutputFormatter gridFormatter(m_constraintOut.grids[i], m_indentationLength + 4);
    m_sstream<<gridFormatter.toString()<<std::endl;
  }
}

OptimizerStageOutputFormatter::OptimizerStageOutputFormatter()
{}

OptimizerStageOutputFormatter::OptimizerStageOutputFormatter(
                                const UserOutput::OptimizerStageOutput optStageOut,
                                const unsigned indentationLength)
                             : DyosFormatter(indentationLength)
{
  m_optStageOut = optStageOut;
}

void OptimizerStageOutputFormatter::putDataIntoStream()
{
  std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Objective:"<<std::endl;
  ConstraintOutputFormatter objectiveFormatter(m_optStageOut.objective, m_indentationLength + 2);
  m_sstream<<objectiveFormatter.toString();
  if(!m_optStageOut.nonlinearConstraints.empty()){
    m_sstream<<indentation<<m_optStageOut.nonlinearConstraints.size();
    m_sstream<<" nonlinear constraints:"<<std::endl;
  }
  indentation.resize(m_indentationLength + 2, ' ');
  for(unsigned i=0; i<m_optStageOut.nonlinearConstraints.size(); i++){
    m_sstream<<indentation<<"Constraint "<<(i+1)<<std::endl;
    ConstraintOutputFormatter constraintFormatter(m_optStageOut.nonlinearConstraints[i],
                                                  m_indentationLength + 4);
    m_sstream<<constraintFormatter.toString();
  }
  if(!m_optStageOut.linearConstraints.empty()){
    indentation.resize(m_indentationLength);
    m_sstream<<indentation<<m_optStageOut.linearConstraints.size();
    m_sstream<<indentation<<" linear constraints"<<std::endl;
  }
  indentation.resize(m_indentationLength + 2, ' ');
  for(unsigned i=0; i<m_optStageOut.linearConstraints.size(); i++){
    m_sstream<<indentation<<"Constraint "<<(i+1)<<std::endl;
    ConstraintOutputFormatter constraintFormatter(m_optStageOut.linearConstraints[i],
                                                  m_indentationLength + 4);
    m_sstream<<constraintFormatter.toString();
  }
}

StageOutputFormatter::StageOutputFormatter()
{}

StageOutputFormatter::StageOutputFormatter(const UserOutput::StageOutput stageOut,
                                           const unsigned indentationLength)
                                       : DyosFormatter(indentationLength)
{
  m_stageOut = stageOut;
}

void StageOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  if(!m_stageOut.treatObjective){
    m_sstream<<"Stage objective is not taken into account for global objective!"<<std::endl;
  }
  m_sstream<<indentation<<"Eso:"<<std::endl;
  EsoOutputFormatter esoFormatter(m_stageOut.eso, m_indentationLength + 2);
  m_sstream<<esoFormatter.toString()<<std::endl;
  if(m_stageOut.mapping.fullStateMapping || !m_stageOut.mapping.stateNameMapping.empty()){
    m_sstream<<indentation<<"Mapping:"<<std::endl;
    MappingOutputFormatter mappingFormatter(m_stageOut.mapping, m_indentationLength + 2);
    m_sstream<<mappingFormatter.toString()<<std::endl;
  }
  m_sstream<<indentation<<"Integrator data:"<<std::endl;
  IntegratorStageOutputFormatter integratorFormatter(m_stageOut.integrator,
                                                     m_indentationLength +2);
  m_sstream<<integratorFormatter.toString()<<std::endl;
  
  m_sstream<<indentation<<"Optimizer data:"<<std::endl;
  OptimizerStageOutputFormatter optimizerFormatter(m_stageOut.optimizer,
                                                   m_indentationLength + 2);
  m_sstream<<optimizerFormatter.toString()<<std::endl;
}

IntegratorOutputFormatter::IntegratorOutputFormatter()
{}

IntegratorOutputFormatter::IntegratorOutputFormatter(const UserOutput::IntegratorOutput intOut,
                                                           unsigned indentationLength)
                                                  : DyosFormatter(indentationLength)
{
  m_intOut = intOut;
}

void IntegratorOutputFormatter::printIntegratorType()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Integrator: ";
  switch(m_intOut.type){
    case UserOutput::IntegratorOutput::LIMEX:
      m_sstream<<"LIMEX"<<std::endl;
      break;
    case UserOutput::IntegratorOutput::NIXE:
      m_sstream<<"Nixe"<<std::endl;
      break;
    default:
      assert(false);
  }
}

void IntegratorOutputFormatter::printIntegrationOrder()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Order: ";
  switch(m_intOut.order){
    case UserOutput::IntegratorOutput::ZEROTH:
      m_sstream<<"no sensitivities"<<std::endl;
      break;
    case UserOutput::IntegratorOutput::FIRST_FORWARD:
      m_sstream<<"first order forward"<<std::endl;
      break;
    case UserOutput::IntegratorOutput::FIRST_REVERSE:
      m_sstream<<"first order reverse"<<std::endl;
      break;
    case UserOutput::IntegratorOutput::SECOND_REVERSE:
      m_sstream<<"second order reverse"<<std::endl;
      break;
    default:
      assert(false);
  }
}

void IntegratorOutputFormatter::putDataIntoStream()
{
  printIntegratorType();
  printIntegrationOrder();
}

SecondOrderOutputFormatter::SecondOrderOutputFormatter()
{}

SecondOrderOutputFormatter::SecondOrderOutputFormatter(
                                          const UserOutput::SecondOrderOutput secOrdOut,
                                          const unsigned indentationLength)
                                              : DyosFormatter(indentationLength)
{
  m_secOrdOut = secOrdOut;
}

void SecondOrderOutputFormatter::putDataIntoStream()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Hessian indices map:"<<std::endl;
  MapFormatter<std::string, int> hessianMapFormatter(m_secOrdOut.indicesHessian,
                                                     m_indentationLength + 2);
  m_sstream<<hessianMapFormatter.toString()<<std::endl;
  
  m_sstream<<indentation<<"Second order derivatives:"<<std::endl;
  DoubleVectorMatrixFormatter valuesFormatter(m_secOrdOut.values, m_indentationLength + 2);
  m_sstream<<valuesFormatter.toString()<<std::endl;
  
  m_sstream<<indentation<<"Composite adjoints:"<<std::endl;
  DoubleVectorMatrixFormatter compositeAdjointsFormatter(m_secOrdOut.compositeAdjoints,
                                                         m_indentationLength + 2);
  m_sstream<<compositeAdjointsFormatter.toString()<<std::endl;
}


OptimizerOutputFormatter::OptimizerOutputFormatter()
{}

OptimizerOutputFormatter::OptimizerOutputFormatter(const UserOutput::OptimizerOutput optOut,
                                                   const unsigned indentationLength)
                                                : DyosFormatter(indentationLength)
{
  m_optOut = optOut;
}


void OptimizerOutputFormatter::printOptimizerType()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Optimizer: ";
  switch(m_optOut.type){
    case UserOutput::OptimizerOutput::IPOPT:
      m_sstream<<"IPOPT"<<std::endl;
      break;
    case UserOutput::OptimizerOutput::NPSOL:
      m_sstream<<"NPSOL"<<std::endl;
      break;
    case UserOutput::OptimizerOutput::SNOPT:
      m_sstream<<"SNOPT"<<std::endl;
      break;
    default:
      assert(false);
  }
}
void OptimizerOutputFormatter::printResultFlag()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation<<"Optimization result: ";
  switch(m_optOut.resultFlag){
    case UserOutput::OptimizerOutput::FAILED:
      m_sstream<<"optimization failed"<<std::endl;
      break;
    case UserOutput::OptimizerOutput::INFEASIBLE:
      m_sstream<<"solution infeasible"<<std::endl;
      break;
    case UserOutput::OptimizerOutput::NOT_OPTIMAL:
      m_sstream<<"solution not optimal"<<std::endl;
      break;
    case UserOutput::OptimizerOutput::OK:
      m_sstream<<"optimal solution found"<<std::endl;
      break;
    case UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS:
      m_sstream<<"not optimal because of too many iterations"<<std::endl;
      break;
    case UserOutput::OptimizerOutput::WRONG_GRADIENTS:
      m_sstream<<"wrong gradients"<<std::endl;
      break;
    default:
      assert(false);
  }
}

void OptimizerOutputFormatter::putDataIntoStream()
{
  printOptimizerType();
  printResultFlag();
  const std::string indentation(m_indentationLength, ' ');
  SecondOrderOutputFormatter secOrdOutFormatter(m_optOut.secondOrder, m_indentationLength);
  if(!m_optOut.secondOrder.values.empty()){
    m_sstream<<indentation<<"Second order output:"<<std::endl;
    m_sstream<<secOrdOutFormatter.toString()<<std::endl;
  }
}

UserOutputFormatter::UserOutputFormatter()
{}

UserOutputFormatter::UserOutputFormatter(const UserOutput::Output out,
                                         const unsigned indentationLength)
{
  m_out = out;
}

void UserOutputFormatter::printRunningMode()
{
  const std::string indentation(m_indentationLength, ' ');
  m_sstream<<indentation;
  switch(m_out.runningMode){
    case UserOutput::Output::SIMULATION:
      m_sstream<<"Simulated problem"<<std::endl;
      break;
    case UserOutput::Output::SINGLE_SHOOTING:
      m_sstream<<"Optimized problem using single shooting"<<std::endl;
      break;
    default:
      assert(false);
  }
}


void UserOutputFormatter::putDataIntoStream()
{
  std::string indentation(m_indentationLength, ' ');
  printRunningMode();
  if(m_out.totalEndTimeLowerBound > -DYOS_DBL_MAX || 
     m_out.totalEndTimeUpperBound < DYOS_DBL_MAX){
    m_sstream<<indentation<<"Total end time constraint: [";
    if(m_out.totalEndTimeLowerBound <= -DYOS_DBL_MAX){
      m_sstream<<"-inf,";
    }
    else{
      m_sstream<<m_out.totalEndTimeLowerBound<<",";
    }
    if(m_out.totalEndTimeUpperBound >= DYOS_DBL_MAX){
      m_sstream<<"inf]"<<std::endl;
    }
    else{
      m_sstream<<m_out.totalEndTimeUpperBound<<"]"<<std::endl;
    }
  }
  m_sstream<<indentation<<"Integrator information:"<<std::endl;
  IntegratorOutputFormatter integratorFormatter(m_out.integratorOutput, m_indentationLength + 2);
  m_sstream<<integratorFormatter.toString()<<std::endl;
  
  m_sstream<<indentation<<"Optimizer information:"<<std::endl;
  OptimizerOutputFormatter optimizerFormatter(m_out.optimizerOutput, m_indentationLength + 2);
  m_sstream<<optimizerFormatter.toString()<<std::endl;
  
  m_sstream<<indentation<<m_out.stages.size()<<" Stages"<<std::endl;
  indentation.resize(m_indentationLength + 2, ' ');
  for(unsigned i=0; i< m_out.stages.size(); i++){
    m_sstream<<indentation<<"Stage "<<(i+1)<<std::endl;
    StageOutputFormatter stageFormatter(m_out.stages[i], m_indentationLength + 4);
    m_sstream<<stageFormatter.toString()<<std::endl;
  }
}

StateNamesFormatter::StateNamesFormatter()
{}

StateNamesFormatter::StateNamesFormatter(const UserOutput::Output out)
{
  m_out = out;
}

void StateNamesFormatter::putDataIntoStream()
{
  unsigned stateIndex = 1;
  m_sstream<<stateIndex<<"\tTIME"<<std::endl;
  if(m_out.stages.empty()){
    return; 
  }
  UserOutput::IntegratorStageOutput integratorStage = m_out.stages.front().integrator;
  for(unsigned i=0; i<integratorStage.parameters.size(); i++){
      UserOutput::ParameterOutput currentParameter = m_out.stages.front().integrator.parameters[i];
    if( currentParameter.paramType == UserOutput::ParameterOutput::PROFILE){
      stateIndex++;
      m_sstream<<stateIndex<<"\t"<<currentParameter.name<<"\tCONTROL";
      if(currentParameter.grids.front().type == UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT){
        m_sstream<<"\tPiecewiseConstant";
      }
      else{
        m_sstream<<"\tPiecewiseLinear";
      }
      m_sstream<<"\t"<<currentParameter.grids.front().numIntervals<<std::endl;
    }
    else if(currentParameter.paramType == UserOutput::ParameterOutput::TIME_INVARIANT){
      stateIndex++;
      m_sstream<<stateIndex<<"\t"<<currentParameter.name<<"\tCONSTANT"<<std::endl;
    }
  }
  for(unsigned i=0; i<integratorStage.states.size(); i++){
    UserOutput::StateOutput currentState = integratorStage.states[i];
    stateIndex++;
    m_sstream<<stateIndex<<"\t"<<currentState.name<<"\tSTATE"<<std::endl;
  }
}

FinalStatesFormatter::FinalStatesFormatter()
{}

FinalStatesFormatter::FinalStatesFormatter(const UserOutput::Output out)
{
  m_out = out;
}

void FinalStatesFormatter::putDataIntoStream()
{
  if(m_out.stages.empty()){
    return;
  }
  const int double_precision = 12;
  m_sstream.precision(double_precision);
  double lastStageTotalDuration = 0.0;
  for(unsigned i=0; i<m_out.stages.size(); i++){
    UserOutput::IntegratorStageOutput integratorStage = m_out.stages[i].integrator;
    assert(!integratorStage.states.empty());
    //collect value vectors
    std::vector<std::vector<double> > values;
    values.push_back(integratorStage.states.front().grid.timePoints);
    const unsigned numTotalGridPoints = values.front().size();
    for(unsigned j=0; j<integratorStage.parameters.size(); j++){
      UserOutput::ParameterOutput currentParameter = integratorStage.parameters[j];
      std::vector<double> parameterVector;
      if(currentParameter.paramType == UserOutput::ParameterOutput::TIME_INVARIANT || 
        currentParameter.paramType == UserOutput::ParameterOutput::INITIAL){
        parameterVector.resize(numTotalGridPoints, currentParameter.value);
      }
      else{
        std::vector<double> parameterGrid (1, 0.0);
        double cumDurations = 0.0;
        for(unsigned k=0; k<currentParameter.grids.size(); k++){
          for(unsigned l=1; l<currentParameter.grids[k].timePoints.size(); l++){
            double point = currentParameter.grids[k].timePoints[l];
            point = point*currentParameter.grids[k].duration + cumDurations;
            parameterGrid.push_back(point);
          }
          cumDurations += currentParameter.grids[k].duration;
          if(k>0 && currentParameter.grids[k].type == UserOutput::ParameterGridOutput::PIECEWISE_LINEAR){
            parameterVector.pop_back();
          }
          parameterVector.insert(parameterVector.end(),
                                 currentParameter.grids[k].values.begin(),
                                 currentParameter.grids[k].values.end());
          
        }
        if(currentParameter.grids.front().type == UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT){
            parameterVector.push_back(currentParameter.grids.back().values.back());
        }
        unsigned parameterGridIndex = 1;
        double epsilon = 1e-10;
        for(unsigned k=1; k<values.front().size(); k++){
          if(values.front()[k] + epsilon < parameterGrid[parameterGridIndex]){
            parameterVector.insert(parameterVector.begin() + k, parameterVector[k-1]);
          }
          else{
            parameterGridIndex ++;
          }
        }
      }
      assert(parameterVector.size() == values.front().size());
      values.push_back(parameterVector);
    }
    
    for(unsigned j=0; j<integratorStage.states.size(); j++){
      assert(integratorStage.states[j].grid.values.size() == numTotalGridPoints);
      values.push_back(integratorStage.states[j].grid.values);
    }
    
    for(unsigned j=0; j<values.front().size(); j++){
      //first column is always the time - so update total time for current stage
      values.front()[j] += lastStageTotalDuration;
      m_sstream<< std::scientific<<values.front()[j];
      for(unsigned k=1; k<values.size(); k++){
        m_sstream<<"\t"<< std::scientific<<values[k][j];
      }
      m_sstream<<std::endl;
      
    }
    lastStageTotalDuration = values.front().back();
  }
}

JsonFormatter::JsonFormatter()
{}

JsonFormatter::JsonFormatter(const UserOutput::Output out)
{
  m_out = out;
}

void JsonFormatter::putDataIntoStream()
{
  std::basic_stringstream<typename boost::property_tree::ptree::key_type::value_type> jsonSstream;
  createPropertyTree_Output(jsonSstream, m_out, JSON);
  m_sstream<<jsonSstream.str();
}

XmlFormatter::XmlFormatter()
{}

XmlFormatter::XmlFormatter(const UserOutput::Output out)
{
  m_out = out;
}

void XmlFormatter::putDataIntoStream()
{
  std::basic_stringstream<typename boost::property_tree::ptree::key_type::value_type> xmlSstream;
  createPropertyTree_Output(xmlSstream, m_out, XML);
  m_sstream<<xmlSstream.str();
}

JsonInputFormatter::JsonInputFormatter()
{}

JsonInputFormatter::JsonInputFormatter(const UserInput::Input in)
{
  m_in = in;
}

void JsonInputFormatter::putDataIntoStream()
{
  std::basic_stringstream<typename boost::property_tree::ptree::key_type::value_type> jsonSstream;
  createPropertyTree_Input(jsonSstream, m_in, JSON);
  m_sstream<<jsonSstream.str();
}

XmlInputFormatter::XmlInputFormatter()
{}

XmlInputFormatter::XmlInputFormatter(const UserInput::Input in)
{
  m_in = in;
}

void XmlInputFormatter::putDataIntoStream()
{
  std::basic_stringstream<typename boost::property_tree::ptree::key_type::value_type> xmlSstream;
  createPropertyTree_Input(xmlSstream, m_in, XML);
  m_sstream<<xmlSstream.str();
}

OptimizationOverviewFormatter::OptimizationOverviewFormatter()
{}

OptimizationOverviewFormatter::OptimizationOverviewFormatter(const UserOutput::Output out)
{
  m_out = out;
}


std::string OptimizationOverviewFormatter::convertOptInformFlag(const UserOutput::OptimizerOutput::ResultFlag flag)
{
  std::string convertedFlag = "";
  switch(flag){
      case UserOutput::OptimizerOutput::OK:
        convertedFlag = "OK";
        break;
      case UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS:
        convertedFlag = "TOO_MANY_ITERATIONS";
        break;
      case UserOutput::OptimizerOutput::INFEASIBLE:
        convertedFlag = "INFEASIBLE";
        break;
      case UserOutput::OptimizerOutput::WRONG_GRADIENTS:
        convertedFlag = "WRONG_GRADIENTS";
        break;
      case UserOutput::OptimizerOutput::NOT_OPTIMAL:
        convertedFlag = "NOT_OPTIMAL";
        break;
      case UserOutput::OptimizerOutput::FAILED:
        convertedFlag = "FAILED";
        break;
      default:
        assert(false);
  }
  return convertedFlag;
}


unsigned OptimizationOverviewFormatter::getNumDecVars(const std::vector<UserOutput::ParameterOutput> &out)
{
  unsigned numDecVars = 0;
  for(unsigned i=0; i<out.size(); i++){
    if(out[i].sensType == UserOutput::ParameterOutput::FULL){
      if(out[i].grids.empty()){
        numDecVars++;
      }
      else{
        for(unsigned j=0; j<out[i].grids.size(); j++){
          if(!out[i].grids[j].isFixed)
            numDecVars += out[i].grids[j].values.size();
        }
      }
    }
  }
  return numDecVars;
}

unsigned OptimizationOverviewFormatter::getNumDecVars(const UserOutput::Output &out)
{
  unsigned numDecVars = 0;
  for(unsigned i=0; i<out.stages.size(); i++){
    numDecVars += getNumDecVars(out.stages[i].integrator.durations);
    numDecVars += getNumDecVars(out.stages[i].integrator.parameters);
  }
  return numDecVars;
}

double OptimizationOverviewFormatter::getObjValue(const UserOutput::Output &out)
{
  double objValue = 0.0;
  for(unsigned i=0; i<out.stages.size(); i++){
    if(out.stages[i].treatObjective){
      objValue += out.stages[i].optimizer.objective.grids.front().values.front();
    }
  }
  return objValue;
}

void OptimizationOverviewFormatter::putDataIntoStream()
{
  const std::string stepNr = "step";
  const unsigned stpNrLength = stepNr.size();
  m_sstream<<stepNr<<"  ";
  
  const std::string cpuTimeString = "CPU time";
  const unsigned cpuTimeLength = cpuTimeString.size()+2;
  m_sstream<<cpuTimeString<<"  ";
  
  const std::string accCpuTimeString = "acc. CPU time";
  const unsigned accCpuTimeLength = accCpuTimeString.size()+2;
  m_sstream<<accCpuTimeString<<" ";
  
  const std::string optFlag = "optimizer flag";
  //this length must be maximal length of all flags and column name
  const unsigned optFlagLength = 20;
  m_sstream<<std::setw(optFlagLength-1)<<optFlag<<"  ";
  m_sstream<<std::setw(0);
  
  const std::string decVars = "dec. vars";
  const unsigned decVarsLength = decVars.size()+2;
  m_sstream<<decVars<<"  ";
  
  const std::string objFuncVal = "obj. func. val.";
  const unsigned objFuncValLength = objFuncVal.size()+2;
  m_sstream<<objFuncVal<<"  ";

  const std::string intermConstrVio = "interm. constr. vio.";
  const unsigned intermConstrVioLength = intermConstrVio.size()+2;
  m_sstream<<intermConstrVio<<std::endl;

  double accCpuTime = 0.0;
  std::vector<UserOutput::Output> solutionHistory = m_out.solutionHistory;
  if(solutionHistory.empty()){
    solutionHistory.push_back(m_out);
  }
  for(unsigned i=0; i<solutionHistory.size(); i++){
    m_sstream<<std::setw(stpNrLength)<<i;
    const double cpuTime = solutionHistory[i].solutionTime;
    m_sstream<<std::setw(cpuTimeLength)<<cpuTime;
    accCpuTime += cpuTime;
    m_sstream<<std::setw(accCpuTimeLength)<<accCpuTime;
    m_sstream<<std::setw(optFlagLength)<<convertOptInformFlag(solutionHistory[i].optimizerOutput.resultFlag);
    
    m_sstream<<std::setw(decVarsLength)<<getNumDecVars(solutionHistory[i]);
    m_sstream<<std::setw(objFuncValLength)<<std::setprecision(8)<<getObjValue(solutionHistory[i]);
    m_sstream<<std::setw(intermConstrVioLength)<<std::setprecision(8)<<solutionHistory[i].optimizerOutput.intermConstrVio;
    m_sstream<<std::endl;
  }
}