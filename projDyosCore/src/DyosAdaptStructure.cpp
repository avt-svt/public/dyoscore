/**
* @file DyosAdaptStructure.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the implementation belonging to the class that    \n
* combines structure detection and adpatation                          \n
* =====================================================================\n
* @author Fady Assassa
* @date 18.01.2013
*/

#include "DyosAdaptStructure.hpp"
#include "OptimizerFactory.hpp"


DyosAdaptStructure::DyosAdaptStructure(const GenericOptimizer::Ptr &optimizer,
                                       const ProblemInput &problemInput)
                                       : IterativeDynOpt(optimizer, problemInput), m_printStructureSteps(false)
{
  // this only determines the largest maxSteps in all control and all grids
  const unsigned numStages = problemInput.optimizer.integratorInput.metaDataInput.stages.size();
  for(unsigned i=0; i<numStages; i++){
    FactoryInput::IntegratorMetaDataInput iStage;
    iStage =  problemInput.optimizer.integratorInput.metaDataInput.stages[i];
    const unsigned numControls = iStage.controls.size();
    for(unsigned j=0; j<numControls; j++){
      const unsigned numGrids = iStage.controls[j].grids.size();
      for(unsigned k=0; k<numGrids; k++){
        unsigned maxSteps = iStage.controls[j].grids[k].adapt.maxAdaptSteps;
        if(maxSteps > m_maxNumSteps){
          m_maxNumSteps = maxSteps;
        }
      }
    }
  }
}

void DyosAdaptStructure::run(DyosOutput::Output &out)
{
  // Step 1: control grid adaptation loop
  controlGridAdaptationLoop(out);

  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "\nBeginning structure detection loop\n" );
  {
    const unsigned numStages = m_problemInput.optimizer.integratorInput.metaDataInput.stages.size();
    for(unsigned i=0; i<numStages; i++){
      const FactoryInput::OptimizerMetaDataInput iStage = m_problemInput.optimizer.metaDataInput.stages[i];
      const unsigned maxSteps = iStage.structureDetection.maxStructureSteps;
      if(maxSteps > m_maxNumSteps){
        m_maxNumSteps = maxSteps;
      }
    }
    // reformulate first time as multi grid problem

    m_prevArcStructure = identifiyControlStructure(out, m_problemInput);
    resetRefinementInterface();
    OptimizerFactory optFac;
    const GenericOptimizer::Ptr optimizer(optFac.createGenericOptimizer(m_problemInput.optimizer));
    m_optimizer = optimizer;
    m_printStructureSteps = true;
  }
  // Step 2: structure detection loop with adaptation
  structureDetectionLoop(out);
}

bool DyosAdaptStructure::stoppingCriterionAdaptation()
{
  bool isEqual = IterativeDynOpt::isArcStructureEqual();
  // run atleast one adaptation step
  isEqual = isEqual && (m_curRefStep > 0);
  return isEqual;
}


bool DyosAdaptStructure::stoppingCriterionStructureDetection()
{
  bool criterionMet = false;
  if(m_rigorousStoppingCriterion){
    criterionMet = determineAdjointError();
  }else{
    criterionMet = determineObjectiveError();
    criterionMet = m_isICVfulfilled && criterionMet;
  }
  return criterionMet;
}


void DyosAdaptStructure::printStatus()const
{
  std::stringstream ss;
  if(!m_printStructureSteps)
    ss << "     Adaptive Grid Refinement step " << m_curRefStep + 1 <<"  \n" ;
  else
    ss << "     Structure detection step " << m_curRefStep + 1 <<"  \n" ;

  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "\n\n\n--------------------------------\n" );
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, ss.str() );
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "--------------------------------\n" );
}
