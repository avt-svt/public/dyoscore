/**
* @file DyosFactory.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosFactory                                                          \n
* =====================================================================\n
* This file creates the DyOS user interface for simulation and         \n
* optimization.                                                        \n
* =====================================================================\n
* @author Fady Assassa
* @date 17.09.2012
*/

#include "DyosFactory.hpp"
#include "DyosAdaptation.hpp"
#include "DyosStructureDetection.hpp"
#include "DyosAdaptStructure.hpp"
#include "OptimizerFactory.hpp"
#include "IntegratorFactory.hpp"


DyOS::Ptr DyOSFactory::createDyOSproblem(const ProblemInput &problemInput)
{
  switch(problemInput.type){
      case ProblemInput::OPTIMIZATION:
        {
          OptimizerFactory optFac;
          optFac.setLogger(m_logger);
          GenericOptimizer::Ptr optimizer(optFac.createGenericOptimizer(problemInput.optimizer));
          optimizer->setLogger(m_logger);
          DyOS::Ptr dynOpt;
          switch(problemInput.optimizer.adaptationOptions.adaptStrategy)
          {
          case FactoryInput::AdaptationOptions::NOADAPTATION:
            {
              const DynamicOptimization::Ptr dummy(new DynamicOptimization(optimizer));
              dynOpt = dummy;
              break;
            }
          case FactoryInput::AdaptationOptions::ADAPTATION:
            {
              const DyosGridAdaptation::Ptr dummy(new DyosGridAdaptation(optimizer, problemInput));
              dynOpt = dummy;
              break;
            }
          case FactoryInput::AdaptationOptions::STRUCTURE_DETECTION:
            {
              const DyosStructureDetection::Ptr dummy(new DyosStructureDetection(optimizer, problemInput));
              dynOpt = dummy;
              break;
            }
          case FactoryInput::AdaptationOptions::ADAPT_STRUCTURE:
            {
              const DyosAdaptStructure::Ptr dummy(new DyosAdaptStructure(optimizer, problemInput));
              dynOpt = dummy;
              break;
            }
          default:
            assert(false);
            break;
          }
          dynOpt->setLogger(m_logger);
          return dynOpt;
        }
        break;
      case ProblemInput::SIMULATION:
        {
          IntegratorFactory intFac;
          intFac.setLogger(m_logger);
          GenericIntegrator::Ptr integrator(intFac.createGenericIntegrator(problemInput.integrator));
          integrator->setLogger(m_logger);
          Simulation::Ptr simRun (new Simulation(integrator));
          simRun->setLogger(m_logger);
          return simRun;
        }
        break;
      default:
        assert(false);
        DyOS::Ptr empty;
        return empty;
  }
}
