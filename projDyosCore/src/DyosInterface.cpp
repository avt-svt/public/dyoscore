/**
* @file DyosInterface.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the definition classes belonging to Dyos          \n
* =====================================================================\n
* @author Fady Assassa
* @date 04.10.2012
*/


#include <cassert>
#include "Dyos.hpp"
#include "DyosInterface.hpp"
#include "Input.hpp"
#include "DyosOutput.hpp"
#include "InputUpdater.hpp"
#include "DyosExceptions.hpp"
#include "OptimizerFactory.hpp"
#include "MetaDataFactories.hpp"
#include "DenseMatrix.hpp"
#include "EsoUtilities.hpp"
#include "UtilityFunctions.hpp"
#include <iomanip>
#include <numeric>
#include <iterator>


#include "boost/date_time/posix_time/posix_time.hpp"

using namespace DyosOutput;
using namespace FactoryInput;
using namespace utils;

/** @brief constructor assigning the only member m_integrator
 * @param[in] integrator should include a complete simulation problem coming from the DyOS
 * Factory
 */
Simulation::Simulation(const GenericIntegrator::Ptr &integrator)
: m_integrator(integrator)
{
}

/** @brief runs the simulation specified by the m_integrator and returns the results.
*   @param[out] out contains the simulation results
*/
void Simulation::run(Output &out)
{
  try{
    m_integrator->activatePlotGrid();

	// measure time
	boost::posix_time::ptime startTime = boost::posix_time::microsec_clock::universal_time();



    m_integrator->solve();


	boost::posix_time::ptime endTime = boost::posix_time::microsec_clock::universal_time();
	boost::posix_time::time_duration solutionTime = endTime - startTime;
	out.solutionTime = solutionTime.total_seconds();
	int numFractionalPlaces = solutionTime.num_fractional_digits();
	double divisor = std::pow(10.0, double(numFractionalPlaces));
	out.solutionTime += (solutionTime.fractional_seconds() / divisor);



    out.stageOutput = m_integrator->getOutput();
  }
  catch(std::exception &e){
    try{
      out.stageOutput = m_integrator->getOutput();
    }
    catch(...){
      m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY,
                            "Could not get proper output");
      m_logger->newlineAndFlush(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
      throw NoOutputException(e);
    }
    throw e;
  }
  catch(...){
    try{
      out.stageOutput = m_integrator->getOutput();
    }
    catch(...){
      throw NoOutputException();
    }
    throw;
  }
}

/** @brief constructor assigning the only member m_optimizer
 * @param[in] optimizer should include a complete optimization problem coming from the DyOS
 * Factory
 */
DynamicOptimization::DynamicOptimization(const GenericOptimizer::Ptr &optimizer)
: m_optimizer(optimizer)
{
}
/** @brief runs the optimizer specified by the m_optimizer and returns the optimization output.
*   @param[out] out contains the optimizer output
*/
void DynamicOptimization::run(Output &out)
{
  try{
    // measure time
    boost::posix_time::ptime startTime = boost::posix_time::microsec_clock::universal_time();
    
    out.optimizerOutput = m_optimizer->solve();
    
    boost::posix_time::ptime endTime = boost::posix_time::microsec_clock::universal_time();
    boost::posix_time::time_duration solutionTime = endTime - startTime;
    out.solutionTime = solutionTime.total_seconds();
    int numFractionalPlaces = solutionTime.num_fractional_digits();
    double divisor = std::pow(10.0, double(numFractionalPlaces));
    out.solutionTime += (solutionTime.fractional_seconds()/divisor);
    
    out.stageOutput = m_optimizer->getOutput();
    
  }
  catch(exception &e){
    try{
      out.stageOutput = m_optimizer->getOutput();
    }
    catch(...){
      m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY,
                            "Could not get proper output");
      m_logger->newlineAndFlush(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
      throw NoOutputException(e);
    }
    throw e;
  }
  catch(...){
    try{
      out.stageOutput = m_optimizer->getOutput();
    }
    catch(...){
      throw NoOutputException();
    }
    throw;
  }
}
  /** @brief general constructor required for sub classes */
IterativeDynOpt::IterativeDynOpt(const GenericOptimizer::Ptr &optimizer,
                                const ProblemInput &problemInput) :
  DynamicOptimization(optimizer),m_maxNumSteps(1), m_curRefStep(0), m_calculateAdjoints(false), m_rigorousStoppingCriterion(false), m_isICVfulfilled(false), m_problemInput(problemInput)
{
  resetRefinementInterface();
}


/** @brief runs a dynamic optimization problem and updates the input with the obtained solution
*   @param[out] out contains the optimizer output
*/
void IterativeDynOpt::run(Output &out)
{
  DynamicOptimization::run(out);
  InputUpdater updater;
  m_inputHistory.push_back(m_problemInput);
  m_problemInput = updater.updateProblemInput(m_problemInput, out);
  m_solutionHistory.push_back(out);
  m_isICVfulfilled = evaluateIntermediateConstraintViolation();
  if (m_calculateAdjoints){
    calculateAdjointVariables();
  }
  //avoid recursive solution histories (can happen with AdaptStructure)
  m_solutionHistory.back().solutionHistory.clear();
}
/** @brief resets the lagrange multipliers after adaptation or structure detection
  * @param[in/out] input is manipulated by reseting the lagrange multipliers
  */
void IterativeDynOpt::resetLagrangeMultipliers(ProblemInput &input)const
{
  vector<ConstraintInput> constraints;
  const unsigned numStages = input.optimizer.integratorInput.metaDataInput.stages.size();
  for(unsigned i=0; i<numStages; i++){
    constraints = input.optimizer.metaDataInput.stages[i].constraints;
    for(unsigned j=0; j<constraints.size(); j++){
      constraints[j].lagrangeMultiplier = 0.0;
      constraints[j].lagrangeMultiplierVector.clear();
    }
    input.optimizer.metaDataInput.stages[i].constraints = constraints;
  }
}
/** @brief creates a second order integration object and solves the adjoints at the current solution
  * the adjoints are saved in the current solution history. The adjoints are only calculated if they
  * dont already exist.
  */
void IterativeDynOpt::calculateAdjointVariables()
{
  ProblemInput input2ndOrder = m_problemInput;
  input2ndOrder.optimizer.type = OptimizerInput::FINAL_INTEGRATION;
  input2ndOrder.optimizer.integratorInput.type = IntegratorInput::NIXE;
  input2ndOrder.optimizer.integratorInput.order = IntegratorInput::FIRST_REVERSE;
  std::stringstream absKey, relKey;
  absKey << input2ndOrder.optimizer.integratorInput.integratorOptions["absolute tolerance"];
  relKey << input2ndOrder.optimizer.integratorInput.integratorOptions["relative tolerance"];

  double absVal, relVal;
  absKey >> absVal;
  relKey >> relVal;

  if(absVal < 1e-8){
    input2ndOrder.optimizer.integratorInput.integratorOptions["absolute tolerance"] = "1e-8";
    input2ndOrder.integrator.integratorOptions["absolute tolerance"] = "1e-8";
  }
  if(relVal < 1e-8){
    input2ndOrder.optimizer.integratorInput.integratorOptions["relative tolerance"] = "1e-8";
    input2ndOrder.integrator.integratorOptions["relative tolerance"] = "1e-8";
  }

  for(unsigned i=0; i<input2ndOrder.optimizer.integratorInput.metaDataInput.stages.size(); i++){
    std::set<double> plotGrid;
    double prevDurs = 0.0;
    for(unsigned j=0; j<input2ndOrder.optimizer.integratorInput.metaDataInput.stages[i].controls.size(); j++){
      prevDurs = 0.0;
      for(unsigned k=0; k<input2ndOrder.optimizer.integratorInput.metaDataInput.stages[i].controls[j].grids.size(); k++){
        vector<double> tPoints = input2ndOrder.optimizer.integratorInput.metaDataInput.stages[i].controls[j].grids[k].timePoints;
        double dur = input2ndOrder.optimizer.integratorInput.metaDataInput.stages[i].controls[j].grids[k].duration;
        for(unsigned l=0; l<tPoints.size()-1; l++){
          double newPoint = (tPoints[l] + tPoints[l+1]) * dur / 2 + prevDurs;
          plotGrid.insert(newPoint);
        } // l
        prevDurs += dur;
      } // k - grid
    } // j - control
    vector<double> plotGridVec(plotGrid.begin(), plotGrid.end());
    for(unsigned ik=0; ik<plotGridVec.size(); ik++){
      plotGridVec[ik] = plotGridVec[ik] / prevDurs;
    }
    input2ndOrder.optimizer.integratorInput.metaDataInput.stages[i].explicitPlotGrid.assign(plotGridVec.begin(), plotGridVec.end());
  } // i
  
  OptimizerFactory optFac;
  const GenericOptimizer::Ptr optimizer(optFac.createGenericOptimizer(input2ndOrder.optimizer));
  optimizer->solve();
  vector<StageOutput> adjOutput = optimizer->getOutput();
  for(unsigned i=0; i<adjOutput.size(); i++){
    m_solutionHistory.back().stageOutput[i].integrator.states = adjOutput[i].integrator.states;
    m_solutionHistory.back().stageOutput[i].integrator.adjoints = adjOutput[i].integrator.adjoints;
  }
  //std::cout << "adjoints succesful" << std::endl;
}

/** @brief creates an eso and evaluates the jacobian df/du for all control parameters.
  * Information required to evaluate the switching function.
  */
vector<map<unsigned, vector<map<double, double> > > > IterativeDynOpt::getDfDu(){
  IntegratorMetaDataInput imdIn = m_problemInput.optimizer.integratorInput.metaDataInput;
  for(unsigned i=0; i<imdIn.stages.size(); i++){
    double endVal = m_solutionHistory.back().stageOutput[i].integrator.states[0].grid.gridPoints.back();
    imdIn.stages[i].userGrid = m_solutionHistory.back().stageOutput[i].integrator.states[0].grid.gridPoints;
    for(unsigned j=0; j<imdIn.stages[i].userGrid.size(); j++){
      imdIn.stages[i].userGrid[j] /= endVal;
    }
  }
  IntegratorMetaDataFactory imdFac;
  IntegratorMetaData::Ptr imd(imdFac.createIntegratorMetaData(imdIn));
  const unsigned numStages = imd->getNumStages();
  /*Iteration over all physical stages*/
  vector<map<unsigned, map<double,double> > > swFunction;
  swFunction.resize(numStages);
  for(unsigned i=0; i<numStages; i++){
    vector<StateOutput> states = m_solutionHistory.back().stageOutput[i].integrator.states;
    vector<StateGridOutput> adjoints = m_solutionHistory.back().stageOutput[i].integrator.adjoints;
    vector<unsigned> esoIndex;
    for(unsigned k=0; k<imdIn.stages[i].controls.size();k++){
      if(imdIn.stages[i].controls[k].type == ParameterInput::CONTROL && 
        imdIn.stages[i].controls[k].sensType == ParameterInput::FULL){
          esoIndex.push_back(imdIn.stages[i].controls[k].esoIndex);
      }
    }
    utils::Array<double> stateVals(states.size()), adjVals(states.size());
    int timeIndexStates(0), timeIndexAdjoints(0);
    imd->initializeForFirstIntegration();
    unsigned numIntervals = imd->getNumIntegrationIntervals();
    double time = 0.0;
    for(unsigned j=0; j<numIntervals; j++){
      do{
        time = imd->getStepEndTime();
        imd->setStepCurrentTime(time);
        if(time != 1.0)
          imd->initializeForNewIntegrationStep();
        imd->setControlsToEso();
        const double stageTime = imd->getStageCurrentTime();
        timeIndexStates = findEntry(stageTime, states[0].grid.gridPoints);
        timeIndexAdjoints = findEntry(stageTime, adjoints[0].gridPoints);
        // we make sure that we use the same grid for both the states and the adjoints.
        if(timeIndexStates != -1 && timeIndexAdjoints != -1){
          //if(time!=0.0){
          //  timeIndexStates--;
          //  timeIndexAdjoints--;
          //}
          for(unsigned k=0; k<states.size(); k++){
            stateVals[k] = states[k].grid.values[timeIndexStates];
            adjVals[k] = adjoints[k].values[timeIndexAdjoints];
          }

          imd->getGenericEso()->setStateValues(stateVals.getSize(),stateVals.getData());
          imd->setControlsToEso();

          if(imd->getGenericEso()->getNumAlgebraicVariables() > 0){
            solveLinearSystemForAlgebraicAdjoints(imd, adjVals);
          }
          const CsCscMatrix::Ptr jacFu = imd->getJacobianFup();
          utils::Array<double> dfdp(stateVals.getSize());
          for(unsigned l=0; l<esoIndex.size();l++){
            jacFu->extractColumn(esoIndex[l], dfdp.getData(), int(stateVals.getSize()));
            ////////////////////////////////////////////////////////////////////////////
            //std::stringstream fname;
            //ofstream myfile;
            //fname << "Fu_" << esoIndex[l] << "_refStep_" << m_solutionHistory.size() << ".dat";
            //myfile.open(fname.str().c_str(), std::ofstream::out | std::ofstream::app);
            //for(unsigned ff = 0; ff<dfdp.getSize(); ff++){
            //  myfile << dfdp[ff] << std::endl;
            //}
            //myfile.close();
            ////////////////////////////////////////////////////////////////////////////
            double val = 0;
            for(unsigned ll=0; ll<stateVals.getSize(); ll++){
              val += adjVals[ll] * dfdp[ll];
            }
            swFunction[i][esoIndex[l]][states[0].grid.gridPoints[timeIndexStates]]= val;
          }
        }
      }while(time != 1.0);
      if (j<numIntervals-1) { // otherwise assert in initializeForNextIntegration() will fail
        imd->initializeForNextIntegration();
      }
    }
  }

  //std::cout<< "integrations routine fur adjoints: OK" << std::endl;

  ///// DEBUG BEGIN
  //unsigned nStages = swFunction.size();
  //std::stringstream fname;
  //ofstream myfile;
  //fname << "sw_" << m_solutionHistory.size() <<  ".m";
  //myfile.open(fname.str().c_str());
  //set<unsigned> ind;
  //double totalDuration = 0;
  //for(unsigned i=0; i<nStages; i++){
  //  totalDuration += imdIn.stages[i].duration.value;
  //}

  //for(unsigned i=0; i<nStages; i++){
  //  for(unsigned k=0; k<imdIn.stages[i].controls.size();k++){
  //    unsigned eIndex = imdIn.stages[i].controls[k].esoIndex;
  //    if(imdIn.stages[i].controls[k].type == ParameterInput::CONTROL){
  //      if(i==0){
  //        myfile << "s" << eIndex << "=[];" ;
  //        ind.insert(eIndex);
  //      }
  //      map<double, double>::iterator it;
  //      myfile << "s" << eIndex << "=[ s" << eIndex << ";" ;
  //      for(it= swFunction[i][eIndex].begin(); it!=swFunction[i][eIndex].end(); ++it){
  //        myfile << it->first + (totalDuration/nStages * i) << "\t" << std::setprecision(12) << it->second << "\n";
  //      }
  //      myfile << "];\n\n";
  //    }
  //  }
  //}
  //myfile << "figure;\n";
  //set<unsigned>::iterator it = ind.begin();
  //for(unsigned i=0; i<ind.size(); i++){
  //  myfile << "subplot(3,1," << i+1 << ");\n";
  //  myfile << "plot(s" << *it <<"(:,1), s" << *it << "(:,2));\n";
  //  myfile << "ylabel('"<< *it << "')\n";
  //  if(i<ind.size()-1)
  //    std::advance(it,1);
  //}
  //myfile.close();
  ///// DEBUG END

  vector<map<unsigned, vector<map<double, double> > > > convSW(numStages);
  for(unsigned i=0; i<numStages; i++){
    map<unsigned, vector<map<double, double> > >::iterator it;
    for(unsigned j=0; j<imdIn.stages[i].controls.size(); j++){
      if(imdIn.stages[i].controls[j].type == ParameterInput::CONTROL){
        unsigned eIndex = imdIn.stages[i].controls[j].esoIndex;
        unsigned nGrids = imdIn.stages[i].controls[j].grids.size();
        vector<double> durs(nGrids), elpsdDurs(nGrids+1,0.0);
        for(unsigned k=0; k<nGrids; k++){
          durs[k] = imdIn.stages[i].controls[j].grids[k].duration;
          elpsdDurs[k+1] = durs[k] + elpsdDurs[k];
        }
        //vector<map<unsigned, map<double,double> > > swFunction;
        convSW[i][eIndex].resize(nGrids);
        std::map<double, double>::const_iterator frstPos, lstPos;
        for(unsigned k=0; k<nGrids; k++){
          frstPos = findMapIter(swFunction[i][eIndex], elpsdDurs[k]);
          lstPos = findMapIter(swFunction[i][eIndex], elpsdDurs[k+1]);
          std::advance(lstPos,1); // so we get the last element in the new vector
          std::map<double, double>::const_iterator it;
          std::map<double, double> scldTime;
          // scale to grid between 0 and 1
          for(it  = frstPos; it != lstPos; it++){
            scldTime[ (it->first - elpsdDurs[k])/durs[k]] = it->second;
          }
          convSW[i][eIndex][k] = scldTime;
        }
      }
    }
  }
  //std::cout<< "Konvertierung auf Gitter: OK" << std::endl;
  return convSW;
}

/** @brief we need to solve a linear system for the algebraic first order adjoints.
  * The system to be solved is:  lam^y^T g_y = -lam^x^T * inv(M) * f_y.
  * Here, f refers to the diff. eqns. and g to the algebraic eqns.
  * lam^y are the adjoints of the algebraic equations.
  * @param[in] imd integrator meta data
  * @param[in/out] adjVals the array of differential and algebraic adjoints. On output it contains the 
  * the the solution of the algebraic adjoints. The differential adjoints are not modified.
  * @todo use a linear solver to solve the linear system rather than inverting matrix g_y
  */
void IterativeDynOpt::solveLinearSystemForAlgebraicAdjoints(const IntegratorMetaData::Ptr &imd,
                                                                  utils::Array<double> &adjVals)const
{
  const CsTripletMatrix::Ptr jacStates(new CsTripletMatrix(imd->getJacobianFstates()->get_matrix_ptr(),false));

  // extract the algebraic part of the jacobian g_y
  EsoIndex numAlgVars = imd->getGenericEso()->getNumAlgebraicVariables();
  utils::Array<EsoIndex> algVarIndex(numAlgVars), algEqnIndex(numAlgVars);
  imd->getGenericEso()->getAlgebraicIndex(numAlgVars, algVarIndex.getData());
  imd->getGenericEso()->getAlgEquationIndex(numAlgVars, algEqnIndex.getData());
  CsTripletMatrix::Ptr gysparse = jacStates->get_matrix_slices(numAlgVars, algEqnIndex.getData(), numAlgVars, algVarIndex.getData());

  // we invert the matrix so we will end up in a full matrix. This should be replaced by a linear solver
  vector<vector<double> > gy, gyinvers;
  convertSparseMatrixToFull(gysparse, gy);
  gyinvers = invertMatrix(gy);

  // extract the differntial part of the jacobian f_y
  EsoIndex numDiffVars = imd->getGenericEso()->getNumDifferentialVariables();
  utils::Array<EsoIndex> diffEqnIndex(numDiffVars), diffVarIndex(numDiffVars);
  imd->getGenericEso()->getDiffEquationIndex(numDiffVars, diffEqnIndex.getData());
  imd->getGenericEso()->getDifferentialIndex(numDiffVars, diffVarIndex.getData());
  CsTripletMatrix::Ptr fysparse = jacStates->get_matrix_slices(numDiffVars, diffEqnIndex.getData(), numAlgVars, algVarIndex.getData());

  vector<vector<double> > fy; 
  convertSparseMatrixToFull(fysparse, fy);

  // get the mass matrix of the DAE system
  CsTripletMatrix::Ptr massparse = imd->getMMatrixCoo();
  CsTripletMatrix::Ptr massX = massparse->get_matrix_slices(numDiffVars, diffEqnIndex.getData(), numDiffVars, diffVarIndex.getData());

  vector<vector<double> > mass, massinv;
  convertSparseMatrixToFull(massX, mass);
  massinv = invertMatrix(mass); // should be avoided

  // algAdjoitns will contain the solution of the algbraic adjoints, which is a vector
  // however, we will introduce both adjoint vectors as matrices to use the matrix multiply function
  vector<vector<double> > algAdjoints, diffAdjoints;
  // solution of the linear system
  algAdjoints = multiply(fy, gyinvers);
  algAdjoints = multiply(massinv, algAdjoints);
  diffAdjoints.resize(1);
  diffAdjoints[0].resize(numDiffVars);
  for(EsoIndex k=0; k<numDiffVars; k++){
    diffAdjoints[0][k] = - adjVals[k];
  }
  algAdjoints =  multiply(diffAdjoints, algAdjoints);

  // the algebraic adjoints are sorted according to their equation index. However, we need to 
  // sort them according to the variable index.
  map<EsoIndex,EsoIndex> eqIndex;
  for(EsoIndex k=numDiffVars; k<numAlgVars+numDiffVars; k++){
    EsoIndex temp = getAlgebraicEquationIndex(algVarIndex[k-numDiffVars], imd->getGenericEso());
    eqIndex[temp] = algVarIndex[k-numDiffVars];
    adjVals[k] = algAdjoints[0][k-numDiffVars];
  }
  map<EsoIndex,EsoIndex>::iterator it = eqIndex.begin();
  utils::Array<double> adjValCopy(adjVals);
  for(unsigned k=0; k<algVarIndex.getSize(); k++){
    adjVals[it->second] = adjValCopy[k+numDiffVars];
    if(k < algVarIndex.getSize()-1)
      std::advance(it,1);
  }

}
//void IterativeDynOpt::solveLinearSystemForAlgebraicAdjoints(const IntegratorMetaData::Ptr &imd,
//                                                                  utils::Array<double> &adjVals)const
//{
//  const CsTripletMatrix::Ptr jacStates(new CsTripletMatrix(imd->getJacobianFstates()->get_matrix_ptr(),false));
//
//  // extract the algebraic part of the jacobian g_y
//  EsoIndex numAlgVars = imd->getGenericEso()->getNumAlgebraicVariables();
//  utils::Array<EsoIndex> algVarIndex(numAlgVars), algEqnIndex(numAlgVars);
//  imd->getGenericEso()->getAlgebraicIndex(numAlgVars, algVarIndex.getData());
//  imd->getGenericEso()->getAlgEquationIndex(numAlgVars, algEqnIndex.getData());
//  utils::Array<EsoIndex> algVarIndexCopy = algVarIndex;
//  for(EsoIndex i=0; i<numAlgVars; i++){
//    algVarIndex[i] = imd->getGenericEso()->getStateIndexOfVariable(algVarIndex[i]);
//  }
//
//  CsTripletMatrix::Ptr gysparse = jacStates->get_matrix_slices(numAlgVars, algEqnIndex.getData(), numAlgVars, algVarIndex.getData());
//
//  // extract the differntial part of the jacobian f_y
//  EsoIndex numDiffVars = imd->getGenericEso()->getNumDifferentialVariables();
//  utils::Array<EsoIndex> diffEqnIndex(numDiffVars), diffVarIndex(numDiffVars);
//  imd->getGenericEso()->getDiffEquationIndex(numDiffVars, diffEqnIndex.getData());
//  imd->getGenericEso()->getDifferentialIndex(numDiffVars, diffVarIndex.getData());
//  utils::Array<EsoIndex> diffVarIndexCopy = diffVarIndex;
//  for(EsoIndex i=0; i<numDiffVars; i++){
//    diffVarIndex[i] = imd->getGenericEso()->getStateIndexOfVariable(diffVarIndex[i]);
//  }
//
//  CsTripletMatrix::Ptr fysparse = jacStates->get_matrix_slices(numDiffVars, diffEqnIndex.getData(), numAlgVars, algVarIndex.getData());
//
//  // get the mass matrix of the DAE system
//  CsTripletMatrix::Ptr massparse = imd->getMMatrixCoo();
//  CsTripletMatrix::Ptr massX = massparse->get_matrix_slices(numDiffVars, diffEqnIndex.getData(), numDiffVars, diffVarIndex.getData());
//
//  vector<vector<double> > fy, gy, gyTrans, mass, massinv; 
//  convertSparseMatrixToFull(fysparse, fy);
//  convertSparseMatrixToFull(gysparse, gy);
//  convertSparseMatrixToFull(massX, mass);
//
//  // transposed matrix required to solve the system Ax=b. We have to convert 
//  // lam^y^T g_y = -lam^x^T * inv(M) * f_y
//  // g_y^T * lam^y = -(inv(M) * f_y)^T * lam^x
//  gysparse->transpose_in_place();
//  convertSparseMatrixToFull(gysparse,gyTrans);
//
//  // required to reorder the rhs of the differential states
//  massinv = invertMatrix(mass); 
//  // evaluate the rhs of the system Ax=b -> lam_x * inv(M) * f_y
//  vector<vector<double> > rhs, diffAdjoints(1);
//  rhs = multiply(massinv,fy);
//  diffAdjoints[0].resize(diffVarIndex.getSize(),0.0);
//  for(unsigned i=0; i<diffVarIndex.getSize(); i++){
//    diffAdjoints[0][i] = adjVals[diffVarIndex[i]];
//  }
//  rhs = multiply(diffAdjoints,rhs);
//  for(unsigned i=0; i<rhs[0].size(); i++){
//    rhs[0][i] *= -1;
//  }
//  vector<vector<double> > gyInv;
//  gyInv = invertMatrix(gy);
//  rhs = multiply(rhs,gyInv);
//
//  // vector contains the algebraic adjoints at the solution
//  // it is initialized with the rhs
//  vector<double> algAdjoints(rhs[0].begin(), rhs[0].end());
//
//  /*vector<int> permutation;
//  double outd;
//  luDecomposition(gyTrans, permutation, outd);
//  luBackSubstitution(gyTrans, permutation, algAdjoints);*/
//
//  for(unsigned i=0; i<algAdjoints.size(); i++){
//    adjVals[algVarIndex[i]] = algAdjoints[i];
//  }
//
//
//
//  // the algebraic adjoints are sorted according to their equation index. However, we need to 
//  // sort them according to the variable index.
//  
//  map<EsoIndex,EsoIndex> eqIndex;
//  for(EsoIndex k=0; k<numAlgVars; k++){
//    EsoIndex temp = getEquationIndexOfVariable(algVarIndexCopy[k], imd->getGenericEso());
//    assert(temp >= 0);
//    eqIndex[temp] = algVarIndex[k];
//  }
//  for(EsoIndex k=0; k<numDiffVars; k++){
//    EsoIndex temp = getEquationIndexOfVariable(diffVarIndexCopy[k], imd->getGenericEso());
//    assert(temp >= 0);
//    eqIndex[temp] = diffVarIndex[k];
//  }
//
//
//  map<EsoIndex,EsoIndex>::iterator it = eqIndex.begin();
//  utils::Array<double> adjValCopy(adjVals);
//  for(EsoIndex k=0; k<numAlgVars+numDiffVars; k++){
//    adjVals[it->first] = adjValCopy[it->second];
//    if(k < numAlgVars+numDiffVars-1)
//      std::advance(it,1);
//  }
//}
/** @brief checks the stopping criterion based on the objective function
  * @return true if stopping criterion is fulfilled.
  */
bool IterativeDynOpt::determineObjectiveError()const
{
  bool criterionMet = false;
  if(m_curRefStep > 0){
    const unsigned numSolutions = m_solutionHistory.size();
    double currentObjVal = m_solutionHistory.back().optimizerOutput.optObjFunVal;
    double previousObjVal = m_solutionHistory[numSolutions - 2].optimizerOutput.optObjFunVal;
    double diff = currentObjVal - previousObjVal;
    double relChange = 0.0;
    if(currentObjVal != 0.0){
      relChange = fabs(diff/currentObjVal);
    }else{
      // we then take the absolut change
      relChange = fabs(diff);
    }
    criterionMet = (relChange < m_problemInput.optimizer.adaptationOptions.adaptationThreshold);
  }
  return criterionMet;
}

/** @brief checks the stopping criterion based on the intermediate constraint violation
  * @return true if stopping criterion is fulfilled.
  * the intermediate constraint violation is determined by integrating a refined state grid.
  */
bool IterativeDynOpt::evaluateIntermediateConstraintViolation()
{
  vector<StageOutput> output = calculateRefinedStateTrajectory();
  unsigned numStages = m_problemInput.optimizer.integratorInput.metaDataInput.stages.size();
  double violation = 0;
  vector<vector<double> > uGridVio;
  vector<double> uPointDistLB(numStages), uPointDistUB(numStages);
  uGridVio.resize(numStages);
  for(unsigned i=0; i<numStages; i++){
    // we override the states with the refined solution
    m_solutionHistory.back().stageOutput[i].integrator.states = output[i].integrator.states;

    std::vector<ConstraintInput> constraints = m_problemInput.optimizer.metaDataInput.stages[i].constraints;
    vector<unsigned> pConstr = getPathConstraintIndices(constraints);
    map<unsigned, vector<double> > bounds =  getPathConstraintBounds(constraints);
    double maxValVio(0), maxValDistLB (0), maxValDistUB(0);
    // go through constraints and analyze the constraint violation
    for(unsigned j=0; j<pConstr.size(); j++){
      StateGridOutput grid = getPathConstraintOutput(output[i].integrator.states, pConstr[j]);
      // go through grid
      for(unsigned k=1; k<grid.gridPoints.size(); k++){
        unsigned lb(0), ub(1);
        if(grid.values[k]<bounds[pConstr[j]][lb]){
          double vio = (bounds[pConstr[j]][lb] - grid.values[k]) * (grid.gridPoints[k] - grid.gridPoints[k-1]);
          violation += vio;
          if(maxValVio < vio){
            maxValVio = vio;
            uGridVio[i].clear();
            uGridVio[i].push_back(grid.gridPoints[k-1]/grid.gridPoints.back());
            uGridVio[i].push_back(grid.gridPoints[k]/grid.gridPoints.back());
          }
          if(maxValDistLB < (bounds[pConstr[j]][lb] - grid.values[k])){
            maxValDistLB = (bounds[pConstr[j]][lb] - grid.values[k]);
            uPointDistLB[i] = grid.gridPoints[k]/grid.gridPoints.back();
          }
        }
        if(grid.values[k]>bounds[pConstr[j]][ub]){
          double vio = (grid.values[k] - bounds[pConstr[j]][ub]) * (grid.gridPoints[k] - grid.gridPoints[k-1]);
          violation += vio;
          if(maxValVio < vio){
            maxValVio = vio;
            uGridVio[i].clear();
            uGridVio[i].push_back(grid.gridPoints[k-1]/grid.gridPoints.back());
            uGridVio[i].push_back(grid.gridPoints[k]/grid.gridPoints.back());
          }
          if(maxValDistUB < (grid.values[k] - bounds[pConstr[j]][ub])){
            maxValDistUB = (grid.values[k] - bounds[pConstr[j]][ub]);
            uPointDistUB[i] = grid.gridPoints[k]/grid.gridPoints.back();
          }
        }
      }
    }
  }
  m_maxIntermConstrVioGrid.clear();
  m_maxIntermConstrVioGrid.resize(numStages);
  for(unsigned i=0; i<numStages; i++){
    //m_maxIntermConstrVioGrid[i].insert(uGridVio[i].begin(), uGridVio[i].end());
    if(uPointDistUB[i] != 0.0)
      m_maxIntermConstrVioGrid[i].insert(uPointDistUB[i]);
    if(uPointDistLB[i] != 0.0)
      m_maxIntermConstrVioGrid[i].insert(uPointDistLB[i]);
  }
  
  std::cout << "intermediate constraint violation: " << std::setprecision (15) << violation << std::endl;
  m_solutionHistory.back().optimizerOutput.intermConstrVio = violation;
  return violation < m_problemInput.optimizer.adaptationOptions.intermConstraintViolationTolerance;
}

/** @brief this function calculates a state trajectory which is finer than the path constraint
  * resolution. However, it always includes the same number of points between two path constraint
  * time points.
  */
vector<StageOutput> IterativeDynOpt::calculateRefinedStateTrajectory()const
{
  // create a simulation output that is used for the refined integration
  IntegratorInput simInput = m_problemInput.optimizer.integratorInput;
  unsigned numStages = simInput.metaDataInput.stages.size();

  vector< set<double> > contrPoints, midPoints;
  // controls the number of points included between two path constraint points
  unsigned midRes = m_problemInput.optimizer.adaptationOptions.numOfIntermPoints;
  midPoints.resize(numStages);
  contrPoints.resize(numStages);
  for(unsigned i=0; i<numStages; i++){
    vector<unsigned> pConstr = 
      getPathConstraintIndices(m_problemInput.optimizer.metaDataInput.stages[i].constraints);
    vector<ConstraintOutput> cOutput = m_solutionHistory.back().stageOutput[i].optimizer.nonLinearConstraints;
    for(unsigned j=0; j<cOutput.size(); j++){
      if(find(pConstr.begin(), pConstr.end(), cOutput[j].esoIndex) != pConstr.end()){
        contrPoints[i].insert(cOutput[j].timePoint);
      }
    }
    // check if any path constraint goes beyond two stages. Then we have to include time point 0
    if (i>0){ 
      vector<unsigned> prevPConstr = 
        getPathConstraintIndices(m_problemInput.optimizer.metaDataInput.stages[i-1].constraints);
      for(unsigned j=0; j<cOutput.size();j++){
        if(find(prevPConstr.begin(), prevPConstr.end(), cOutput[j].esoIndex) != prevPConstr.end()){
          contrPoints[i].insert(0.0);
          break;
        }
      }
    }
    vector<double> constrPointsVec(contrPoints[i].begin(),contrPoints[i].end());
    for(int j=0; j<int(constrPointsVec.size()-1); j++){
      double interval = constrPointsVec[j+1] - constrPointsVec[j];
      interval /= midRes;
      for(unsigned k=0; k<midRes-1; k++){
        midPoints[i].insert(constrPointsVec[j] + interval * (k+1));
      }
    }
    contrPoints[i].insert(midPoints[i].begin(), midPoints[i].end());
  }

  for(unsigned i=0; i<numStages;i++){
    vector<double> userGrid(contrPoints[i].begin(), contrPoints[i].end());
    simInput.metaDataInput.stages[i].userGrid = userGrid;
  }
  IntegratorFactory intFac;
  simInput.order = FactoryInput::IntegratorInput::ZEROTH;
  const GenericIntegrator::Ptr integrator(intFac.createGenericIntegrator(simInput));
  integrator->solve();

  return integrator->getOutput();
}
/** @brief extracts lower and upper bounds of all constraints
  * @param[in] constraints vector containing the constraint information
  * @return a map of esoIndex with two entries (first lower bound, second upper bound) 
  *         of the corresponding path constraint
  */
map<unsigned, vector<double> > IterativeDynOpt::getPathConstraintBounds(
                                                const vector<ConstraintInput> &constraints)const
{
  map<unsigned, vector<double> > bounds;
  for(unsigned i=0; i<constraints.size(); i++){
    if(constraints[i].type == ConstraintInput::PATH){
      vector<double> tempBound;
      tempBound.push_back(constraints[i].lowerBound);
      tempBound.push_back(constraints[i].upperBound);
      bounds[constraints[i].esoIndex] = tempBound;
    }
  }
  return bounds;
}
/** @brief returns the state grid output to the corresponding esoIndex
  * @param[in] output state output provided by IterativeDynOpt::calculateRefinedStateTrajectory()
  * @param[in] esoIndex is an index of a path constraint
  */
StateGridOutput IterativeDynOpt::getPathConstraintOutput(const vector<StateOutput> output,
                                                         const unsigned esoIndex)const
{
  unsigned numStates = output.size();
  for(unsigned i=0; i<numStates; i++){
    if(output[i].esoIndex == esoIndex){
      return output[i].grid;
      break;
    }
  }
  assert(false);
  return output.back().grid;
}
/** @brief checks the stopping criterion based on the adjoint function
  * @return true if stopping criterion is fulfilled.
  */
bool IterativeDynOpt::determineAdjointError()
{
  bool criterionMet = false;
  double absTol=0.01;
  double relTol=0.01;
  if(m_curRefStep > 0){
    double maxError = 0;
    unsigned blockSize = 32;
    vector<vector<vector<double> > > oldAdjoints, newAdjoints;
    interpolateAdjoints(oldAdjoints, newAdjoints);
    const unsigned numStages = oldAdjoints.size();
    for(unsigned i=0; i<numStages; i++){
      vector<vector<double> > oldBlock, newBlock;
      const unsigned numStates = oldAdjoints[i].size();
      oldBlock.resize(numStates);
      newBlock.resize(numStates);
      for(unsigned j=0; j<numStates; j++){
        const unsigned numEntries = oldAdjoints[i][j].size() / blockSize;
        oldBlock[j].resize(numEntries);
        newBlock[j].resize(numEntries);
        for(unsigned k=0; k<numEntries; k++){
          oldBlock[j][k] =  accumulate(oldAdjoints[i][j].begin() + (k*blockSize), oldAdjoints[i][j].begin() + ((k+1)*blockSize), 0.0);
          oldBlock[j][k] /= double(blockSize);
          newBlock[j][k] =  accumulate(newAdjoints[i][j].begin() + (k*blockSize), newAdjoints[i][j].begin() + ((k+1)*blockSize), 0.0);
          newBlock[j][k] /= double(blockSize);
        }
      }
      for(unsigned j=0; j<numStates; j++){
        const unsigned numEntries = oldAdjoints[i][j].size() / blockSize;
        for(unsigned k=0; k<numEntries; k++){
          double currentError = fabs(newBlock[j][k] - oldBlock[j][k]) / (fabs(newBlock[j][k]) * relTol + absTol);
          maxError = max(currentError, maxError);
        }
      }
    }
    criterionMet = maxError < 1.0;
  }
  return criterionMet;
}

/** @brief interpolates (first order) state adjoints  on an equidistant fine grid
  * @param[out] matrix with first order adjoints overall stages of the previous solution
  * @param[out] matrix with first order adjoints overall stages of the current solution
  */
void IterativeDynOpt::interpolateAdjoints(std::vector<std::vector<std::vector<double> > > &oldAdjointsInterp,
                                          std::vector<std::vector<std::vector<double> > > &newAdjointsInterp)const
{
  const int resolution = 1023; // resolution of interpolation
  const unsigned numStages = m_solutionHistory.back().stageOutput.size();
  oldAdjointsInterp.resize(numStages);
  newAdjointsInterp.resize(numStages);
  for (unsigned i=0; i<numStages; i++){
    const unsigned numStates = m_solutionHistory.back().stageOutput[i].integrator.states.size();
    // we only interpolate the adjoints belonging to the states
    for(unsigned j=0; j<numStates; j++){
      vector<double> newGridPoints, oldGridPoints, newAdjoints, oldAdjoints;
      const unsigned numSolutions = m_solutionHistory.size();
      newGridPoints  = m_solutionHistory.back().stageOutput[i].integrator.adjoints[j].gridPoints;
      oldGridPoints  = m_solutionHistory[numSolutions-2].stageOutput[i].integrator.adjoints[j].gridPoints;
      newAdjoints  = m_solutionHistory.back().stageOutput[i].integrator.adjoints[j].values;
      oldAdjoints  = m_solutionHistory[numSolutions-2].stageOutput[i].integrator.adjoints[j].values;
      vector<double> newInterpTime, oldInterpTime, newInterpValues, oldInterpValues;
      interpGrid(resolution,
                  newGridPoints,
                  newAdjoints,
                  newInterpTime,
                  newInterpValues);
      interpGrid(resolution,
                  oldGridPoints,
                  oldAdjoints,
                  oldInterpTime,
                  oldInterpValues);
      newAdjointsInterp[i].push_back(newInterpValues);
      oldAdjointsInterp[i].push_back(oldInterpValues);
    }
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////
////////// Begin Step 1: Control grid adaptation loops and subfunction ///////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

/** @brief here the control grid adaptation loop is defined. It is used alone or together with
  * the structure detection.
  */
void IterativeDynOpt::controlGridAdaptationLoop(Output &out)
{
  m_prevArcStructure = firstControlStructure();
  for(m_curRefStep = 0; m_curRefStep<m_maxNumSteps; m_curRefStep++){
    printStatus();
    IterativeDynOpt::run(out); // problem input already updated
    ProblemInput suggestedInput = m_problemInput;
    m_newArcStructure = identifiyControlStructure(out, suggestedInput);
    if(stoppingCriterionAdaptation()){
      break;
    }else{
      m_prevArcStructure = m_newArcStructure;
      adaptControlGrid();
      //addAdditionalConstraintGrid();
      //evaluateIntermediateConstraintViolation();
      OptimizerFactory optFac;
      const GenericOptimizer::Ptr optimizer(optFac.createGenericOptimizer(m_problemInput.optimizer));
      m_optimizer = optimizer;
    }
  }
  out.solutionHistory = m_solutionHistory;
  out.inputHistory = m_inputHistory;
}

/** @brief runs the control grid adaptation over all stages
  */
void IterativeDynOpt::adaptControlGrid()
{
  ProblemInput refinedInput = m_problemInput; // input should be up-to-date
  const unsigned numStages = refinedInput.optimizer.integratorInput.metaDataInput.stages.size();
  if(m_calculateAdjoints)
    m_swFunction = getDfDu();

  for(unsigned i=0; i<numStages; i++){
    adaptControlGridStage(refinedInput.optimizer.integratorInput.metaDataInput.stages[i], i);
  }
  resetLagrangeMultipliers(refinedInput);
  m_problemInput = refinedInput;
}
/** @brief adapts the control grid of a certain stage. Loop through all controls.
  * @param[in] iStage information of a certain stage that should be adapted
  */
void IterativeDynOpt::adaptControlGridStage(IntegratorMetaDataInput &stageData,
                                            const unsigned iStage)
{
  //std::cout<< "Start Adaptation: OK" << std::endl;
  const unsigned numControls = stageData.controls.size();
  for(unsigned i=0; i<numControls; i++){
    if(stageData.controls[i].type == ParameterInput::CONTROL && 
      stageData.controls[i].sensType == ParameterInput::FULL){
      const unsigned numControlGrids = stageData.controls[i].grids.size();
      for(unsigned j=0; j<numControlGrids; j++){
//        if(m_curRefStep < iStage.controls[i].grids[j].adapt.maxAdaptSteps){
        if(!stageData.controls[i].grids[j].parametersFixed){
          ParameterizationGridInput iGrid = stageData.controls[i].grids[j];

          if(m_refInterface[iStage][i][j]->getType() == GridRefinementInterface::SWITCHINGFUNCTION){
            m_refInterface[iStage][i][j]->addData(m_swFunction[iStage][stageData.controls[i].esoIndex][j]);
          }
          m_refInterface[iStage][i][j]->obtainNewMesh(iGrid.timePoints, iGrid.values);
          // produces the output of the wavelet coefficient as in the old dyos
          //std::stringstream fileName;
          //fileName << "refStep_" << m_curRefStep << "_stage_"<< iStage << "_control_" << i << "_grid_" << j << ".dat";
          //m_refInterface[iStage][i][j]->writeOutput(fileName.str());
          //std::cout<< "Obtain new mesh: OK" << std::endl;
          iGrid.timePoints = m_refInterface[iStage][i][j]->getRefinedGrid();
          iGrid.values = m_refInterface[iStage][i][j]->getRefinedValues();
          stageData.controls[i].grids[j] = iGrid;

        }
      }
    }
  }
  //std::cout<< "Ende Adaptation: OK" << std::endl;
}


void IterativeDynOpt::addAdditionalConstraintGrid()
{
  unsigned stageSize = m_problemInput.optimizer.integratorInput.metaDataInput.stages.size();
  for(unsigned i=0; i<stageSize; i++){
    set<double> temp(m_problemInput.optimizer.integratorInput.metaDataInput.stages[i].userGrid.begin(),
      m_problemInput.optimizer.integratorInput.metaDataInput.stages[i].userGrid.end());
    temp.insert(m_maxIntermConstrVioGrid[i].begin(), m_maxIntermConstrVioGrid[i].end());
    m_problemInput.optimizer.integratorInput.metaDataInput.stages[i].userGrid.assign(temp.begin(), temp.end());
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////
////////// End Step 1: Control grid adaptation loop and subfunctions /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////
////////// Begin Step 2: Structure detection loop and subfunctions ///////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

/** @brief control grid is reformulated as a multi grid problem with subsequent control grid adaptation
  */
void IterativeDynOpt::structureDetectionLoop(Output &out)
{
  if(m_newArcStructure.size() == 0)
    m_prevArcStructure = firstControlStructure();

  for(m_curRefStep = 0; m_curRefStep<m_maxNumSteps; m_curRefStep++){
    printStatus();
    IterativeDynOpt::run(out); // problem input already updated

    ProblemInput suggestedInput = m_problemInput;
    m_newArcStructure = identifiyControlStructure(out, suggestedInput);
    if(m_curRefStep < m_maxNumSteps - 1){
      if(isArcStructureEqual()){
        if(stoppingCriterionStructureDetection()){
          break;
        }else{
          adaptControlGrid();
          //addAdditionalConstraintGrid();
          OptimizerFactory optFac;
          const GenericOptimizer::Ptr optimizer(optFac.createGenericOptimizer(m_problemInput.optimizer));
          m_optimizer = optimizer;
        }
      }else{
        m_problemInput = suggestedInput;
        addAdditionalConstraintGrid();
        m_prevArcStructure = m_newArcStructure;
        resetRefinementInterface();
        OptimizerFactory optFac;
        const GenericOptimizer::Ptr optimizer(optFac.createGenericOptimizer(m_problemInput.optimizer));
        m_optimizer = optimizer;
      }
    }
  }
  out.solutionHistory = m_solutionHistory;
  out.inputHistory = m_inputHistory;
}

/** @brief resetes the refinement interface if a new structure was detected
  */
void IterativeDynOpt::resetRefinementInterface()
{
  m_refInterface.clear();
  vector<IntegratorMetaDataInput> stages = m_problemInput.optimizer.integratorInput.metaDataInput.stages;
  const unsigned numStages = stages.size();
  m_refInterface.resize(numStages);
  for(unsigned i=0; i<numStages; i++){
    const unsigned numControls = stages[i].controls.size();
    m_refInterface[i].resize(numControls);
    for(unsigned j=0; j<numControls; j++){
      const unsigned numGrids = stages[i].controls[j].grids.size();
      m_refInterface[i][j].resize(numGrids);
      for(unsigned k=0; k<numGrids; k++){
        GridRefinementFactory refineFac;
        m_refInterface[i][j][k] = refineFac.createRefinementInterface(stages[i].controls[j].grids[k].adapt);
        if(!m_calculateAdjoints && stages[i].controls[j].grids[k].adapt.adaptType == AdaptationInput::SwitchingFunction)
          m_calculateAdjoints = true;
      }
    }
  }
}
/** @brief identifies the arc sequence that is required to reformulate the problem and to check
  * if the sequence changes over the optimization problem.
  * @param[in] output of previous optimization. Required to evaluate path constraint and their multipliers
  * @param[out] suggestedInput suggested problem input with the new arc structure
  */
vector<ArcPattern> IterativeDynOpt::identifiyControlStructure(const Output &out,
                                                              ProblemInput &suggestedInput)const
{
  vector<ArcPattern> suggestedArcs;
  const unsigned numStages = m_problemInput.optimizer.integratorInput.metaDataInput.stages.size();
  for(unsigned i=0; i<numStages; i++){
    // check where nonlinear path constraints are active
    const PathConstraintMap pathConstr = 
      getPathConstraintStage(out.stageOutput[i].optimizer.nonLinearConstraints,
                              m_problemInput.optimizer.metaDataInput.stages[i].constraints);
    const ActivityMap pathActivity = getPathConstraintActivity(pathConstr);
    //only use Parameters from suggested input, since in out there can be also dummy parameters
    // containing just sensitivity output
    std::vector<DyosOutput::ParameterOutput> stageParameters;
    std::vector<FactoryInput::ParameterInput> controls;
    if(!suggestedInput.integrator.metaDataInput.stages.empty()){
      assert(i < suggestedInput.integrator.metaDataInput.stages.size());
      controls = suggestedInput.integrator.metaDataInput.stages[i].controls;
    }
    else{
      assert(i< suggestedInput.optimizer.integratorInput.metaDataInput.stages.size());
      controls = suggestedInput.optimizer.integratorInput.metaDataInput.stages[i].controls;
    }
    
    for(unsigned j=0; j<controls.size(); j++){
      for(unsigned k=0; k<out.stageOutput[i].integrator.parameters.size(); k++){
        if(out.stageOutput[i].integrator.parameters[k].esoIndex == controls[j].esoIndex){
          stageParameters.push_back(out.stageOutput[i].integrator.parameters[k]);
        }
      }
    }
    const ActivityMap controlActivity = getControlActivity(stageParameters,
                                                           pathActivity);

    IntegratorMetaDataInput iStage = m_problemInput.optimizer.integratorInput.metaDataInput.stages[i];
    suggestedArcs.push_back(getArcStructure(controlActivity,
                                            pathActivity,
                                            iStage.controls));
    // suggest problem Input
    iStage = suggestedInput.optimizer.integratorInput.metaDataInput.stages[i];
    iStage.controls = formulateMultiGridProblem(controlActivity,
                                                pathActivity,
                                                iStage.controls);
    suggestedInput.optimizer.integratorInput.metaDataInput.stages[i] = iStage;
    resetLagrangeMultipliers(suggestedInput);
  }
  return suggestedArcs;
}
/** @brief accumulates path constraints such that it can be accessed easily for structure detection
  * @param[in] constraintOut are the nonlinear constraints at the optimal solution
  * @param[in] constraintInput are the nonlinear constraints from the input
  * @return a map of all path constraints in the stage
  */
PathConstraintMap IterativeDynOpt::getPathConstraintStage(const vector<ConstraintOutput> &constraintOut,
                                                          const vector<ConstraintInput> &constraintInput)
                                                          const
{
  const vector<unsigned> indices = getPathConstraintIndices(constraintInput);
  PathConstraintMap pathConstraints;

  // path constraints can only be nonlinear
  const unsigned numNonLinConst = constraintOut.size();
  for(unsigned i=0; i<numNonLinConst; i++){
    if(find(indices.begin(), indices.end(), constraintOut[i].esoIndex) != indices.end()){
      pathConstraints[constraintOut[i].esoIndex].push_back(constraintOut[i]);
    }
  }

  // we need to check wether the path-constraint at the end-point of the stage is part of the
  // path constraint or an endpoint constraint that is overriding the path.
  PathConstraintMap::iterator iter;
  for(iter = pathConstraints.begin(); iter != pathConstraints.end(); ++iter){
    // we want to compare the bound to the second last entry
    if(iter->second.size() > 1){
      const ConstraintOutput prevEntry = iter->second.end()[-2];
      const ConstraintOutput lastEntry = iter->second.back();
      if(lastEntry.lowerBound != prevEntry.lowerBound || lastEntry.upperBound != prevEntry.upperBound  ){
        iter->second.pop_back();
      }
    }
  }
  return pathConstraints;
}
/** @brief gets the esoIndices of the pathconstraints, so that we can identify them in the output
  * @param[in] input are the nonlinear constraints from the input
  * @return a list of esoIndices of path constraints.
  */
vector<unsigned> IterativeDynOpt::getPathConstraintIndices(const vector<ConstraintInput> &input)const
{
  const unsigned numConstraints = input.size();
  vector<unsigned> pathConstraintIndices;
  pathConstraintIndices.reserve(numConstraints);
  for(unsigned i = 0; i<numConstraints; i++){
    if(input[i].type == ConstraintInput::PATH){
      pathConstraintIndices.push_back(input[i].esoIndex);
    }
  }
  return pathConstraintIndices;
}
/** @brief determines the activity pattern of all path constraints
  * @param[in] pathConstraints has the point information of each path constraint
  * @return a pattern vector of activity for each path constraint activity
  */
ActivityMap IterativeDynOpt::getPathConstraintActivity(const PathConstraintMap &pathConstraints)const
{
  ActivityMap constrActivity;
  PathConstraintMap pCCopy = pathConstraints;
  PathConstraintMap::iterator it;

  for(it = pCCopy.begin(); it != pCCopy.end(); ++it){
    vector<ConstraintActivity> activity = checkConstraintAcitivity(it->second);
    vector<IntervalActivity> intervals = activePathIntervals(activity, it->second);
    constrActivity[it->first] = intervals;
  }
  return constrActivity;
}

vector<double> IterativeDynOpt::getActiveConstraintTimePoints(const PathConstraintMap &pcMap)const
{
  PathConstraintMap::const_iterator it;
  std::set<double> tp;
  for(it = pcMap.begin(); it != pcMap.end(); ++it){
    vector<ConstraintActivity> activity = checkConstraintAcitivity(it->second);
    assert(activity.size() == it->second.size());
    for(unsigned i=0; i<it->second.size(); i++){
      if(activity[i] != NOTACTIVE){
        tp.insert(it->second[i].timePoint);
      }
    }
  }
  vector<double> retVec(tp.begin(), tp.end());
  return retVec;
}

/** @brief determines the activity pattern for point constraints
  * @param[in] pConstr has the information of each point constraint
  * @return a vector of ConstraintActivity distinguishing between upperbound, lowerbound, notactive
  */
vector<ConstraintActivity> IterativeDynOpt::checkConstraintAcitivity(
  const vector<ConstraintOutput> &pConstr) const
{
  const unsigned numPoints = pConstr.size();
  const double EPSILON = 1e-7;
  vector<ConstraintActivity> activity(numPoints);
  for(unsigned i=0; i<numPoints; i++){
    if(pConstr[i].lagrangeMultiplier < 0){
      activity[i] = ACTIVELOWERBOUND;
    }
    else if (pConstr[i].lagrangeMultiplier > 0){
      activity[i] = ACTIVEUPPERBOUND;
    }
    else{
      // sometimes the lagrange multiplier is zero although the constraint is active
      if(fabs( pConstr[i].value - pConstr[i].upperBound) < EPSILON){
        activity[i] = ACTIVEUPPERBOUND;
      }
      else if(fabs( pConstr[i].value - pConstr[i].lowerBound) < EPSILON){
        activity[i] = ACTIVELOWERBOUND;
      }
      else{
        activity[i] = NOTACTIVE;
      }
    }
  }
  return activity;
}
/** @brief determines the activity pattern of the path constraints and groups them
  * @param[in] activity pattern of the point constraints
  * @param[in] pConstr has the information of each point constraint
  * @return a vector of path constraint activity for a certain path constraint
  */
vector<IntervalActivity> IterativeDynOpt::activePathIntervals(
     const vector<ConstraintActivity> &activity,
     const vector<ConstraintOutput> &pConstr) const
{

  vector<IntervalActivity> intervals;
  IntervalActivity tia = createTimeActivity(pConstr[0].timePoint,
                                                pConstr[0].timePoint,
                                                activity[0],
                                                false);

  for(unsigned i=0; i<activity.size() - 1; i++){
    if(activity[i] == activity[i+1]){
      tia.exit = pConstr[i+1].timePoint;
      tia.isSuccessive = true;
    }else{
      intervals.push_back(tia);
      tia = createTimeActivity(pConstr[i+1].timePoint,
                               pConstr[i+1].timePoint,
                               activity[i+1],
                               false);
      
    }
  }
  intervals.push_back(tia);
  return intervals;
}

/** @brief determines for all controls where they are active or inactive
  * @param[in] params grids and solutions of all controls
  * @return activity pattern grouped in intervals of same acitiviy for all controls
  */
ActivityMap IterativeDynOpt::getControlActivity(const std::vector<ParameterOutput> &params,
                                                const ActivityMap &pPattern)const
{
  ActivityMap activityControlPattern;
  const unsigned numParams = params.size();
  for(unsigned i=0; i<numParams; i++){
    if(!params[i].isTimeInvariant){
      // we first need the data to check where the control constraints are active in order
      // to group them in intervals
      ActivityPattern activityPattern = checkControlAcitivity(params[i].grids,
                                                              params[i].lowerBound,
                                                              params[i].upperBound);

      vector<IntervalActivity> intervals = activeControlIntervals(params[i], activityPattern);

      activityControlPattern[params[i].esoIndex] = intervals;
    }
  }
  activityControlPattern = splitControlActivity(activityControlPattern);
  return activityControlPattern;
}

/** @brief introduce for all controls all grids (required to have always equistant grids).
  * @param[in] intervals provided by getControlAcitivity
  * @return activity pattern grouped in intervals of same acitiviy for all controls
  * This is according to Schlegel's implementation. Leaving this block out will lead to fewer 
  * grids but these grids will be not-equidistant. Thus, the adaptation will be inefficient.
  * However, this should not be too diffuclt to solve.
  */
ActivityMap IterativeDynOpt::splitControlActivity(const ActivityMap &pattern)const
{
  set<double> pointSet;
  ActivityMap cPattern = pattern;
  ActivityMap::iterator iC;
  for(iC = cPattern.begin(); iC != cPattern.end(); ++iC){
    unsigned nGrid = iC->second.size();
    for(unsigned j=1; j<nGrid; j++){ // skip the zero at the beginning
      pointSet.insert(iC->second[j].entry);
    }
  }
  vector<double> point(pointSet.begin(), pointSet.end());

  for(iC = cPattern.begin(); iC != cPattern.end(); ++iC){
    for(unsigned j=0; j<point.size(); j++){
      if(findEntryOrExit(iC->second, point[j])){
        unsigned pos = findEntryPosition(iC->second, point[j]);

        IntervalActivity tia = iC->second[pos-1];
        iC->second[pos-1].exit = point[j];
        tia.entry = iC->second[pos-1].exit;
        iC->second.insert(iC->second.begin() + pos, tia);
      }
    }
  }
  return cPattern;
}

/** @brief goes through every grid point and checks wether the control is at the bounds or inactive.
  * @param[in] paramGrid contain the grids of the previous solution.
  * @param[in] lowerBound to check values for activity
  * @param[in] upperBound to check values for activity
  * @return
  */
ActivityPattern IterativeDynOpt::checkControlAcitivity(const vector<ParameterGridOutput> &paramGrid,
                                                       const double &lowerBound,
                                                       const double &upperBound) const
{
  const double EPSILON = 1e-7;
  const unsigned numGrids = paramGrid.size();
  ActivityPattern activityPattern(numGrids);

  for(unsigned i=0; i<numGrids; i++){
    const vector<double> localVals = paramGrid[i].values;
    const vector<double> localMultipliers = paramGrid[i].lagrangeMultiplier;

    const unsigned gridSize = localMultipliers.size();
    for(unsigned j=0; j<gridSize; j++){
/*      if(localMultipliers[j] < 0){
        activityPattern[i].push_back(ACTIVEUPPERBOUND);
      }else if(localMultipliers[j] > 0){
        activityPattern[i].push_back(ACTIVELOWERBOUND);
      }else*/{
        // sometimes the lagrange multiplier is zero although the constraint is active
        if(fabs( localVals[j] - lowerBound) < EPSILON){
          activityPattern[i].push_back(ACTIVELOWERBOUND);
        }
        else if(fabs( localVals[j] - upperBound) < EPSILON){
          activityPattern[i].push_back(ACTIVEUPPERBOUND);
        }
        else{
          activityPattern[i].push_back(NOTACTIVE);
        }
      }
    }
  }
  return activityPattern;
}


/** @brief  goes through the activity pattern and groups neighboring intervals of the same type
  * @param[in] param contains the grids of the previous solution.
  * @param[in] activity contains the solution obtained with StrucDetectDynOpt::checkControlConstraintAcitivity
  * @return
  */
vector<IntervalActivity> IterativeDynOpt::activeControlIntervals(const ParameterOutput &param,
                                                                     const ActivityPattern &activity)const
{
  vector<IntervalActivity> intervals;
  double elapsedTime = 0;
  double totalDur = 0;
  for(unsigned i=0; i<param.grids.size(); i++)
    totalDur += param.grids[i].duration;


  for(unsigned i=0; i<param.grids.size(); i++){
    const double scaleFactor = param.grids[i].duration;
    const vector<double> gridPoints = rescaleGrid(param.grids[i].gridPoints, scaleFactor, elapsedTime);
    bool isSuccessive = false;
    // always include the first activity
    IntervalActivity tia;
    if(param.grids[i].approximation == DyosOutput::ParameterGridOutput::PieceWiseConstant){
      tia = createTimeActivity(gridPoints[0],
                               gridPoints[1],
                               activity[i][0],
                               isSuccessive);
    }else{
      tia = createTimeActivity(gridPoints[0],
                               gridPoints[0],
                               activity[i][0],
                               isSuccessive);
    }
    intervals.push_back(tia);

    for(unsigned j=1; j<activity[i].size(); j++){
      if(activity[i][j-1] == activity[i][j]){
        isSuccessive = true;
        if(param.grids[i].approximation ==DyosOutput::ParameterGridOutput::PieceWiseConstant){
          intervals.back().exit = gridPoints[j+1];
        }else{
          intervals.back().exit = gridPoints[j];
        }
        double dur = intervals.back().exit - intervals.back().entry;
        double deleteTol = 1e-2; // grids are deleted which are smaller than 1% of the total stage duration
        intervals.back().isSuccessive = (isSuccessive) && (dur > totalDur*deleteTol);
      }else{
        isSuccessive = false;
        if(param.grids[i].approximation == DyosOutput::ParameterGridOutput::PieceWiseConstant){
          tia = createTimeActivity(gridPoints[j],
                                   gridPoints[j+1],
                                   activity[i][j],
                                   isSuccessive);
        }else{
          tia =  createTimeActivity(gridPoints[j],
                                   gridPoints[j],
                                   activity[i][j],
                                   isSuccessive);
        }
        intervals.push_back(tia);
      }
    }
    elapsedTime += param.grids[i].duration;
    if(param.grids[i].approximation ==DyosOutput::ParameterGridOutput::PieceWiseLinear){
      intervals = rearrangeIntervals(intervals);
    }
  }
  intervals = mergeIntervals(intervals);
  return intervals;
}

/** @brief rearranges the intervals for piecewise linear controls that only touch the upper or lower bound
  * @param[in] intervals provided by function activeControlIntervals for linear approximations
  * @return    modified intervals according to description.
  * Incase of constant controls everything is perfect, because they are defined on an interval but
  * for linear we have to rearrange the arcs.
  * e.g. What do you do when the control only hits the upper bound at one point and at the next point
  * the lower bound? Then we don't have an interval. 
  * Thus, we destinguish following cases occur:
  * UB - LB; we will introduce 2 grids: UB - LB; splitted equally
  * NA - UB,LB; we introduce 2 stages: NA - UB; splitted equally
  */
vector<IntervalActivity> IterativeDynOpt::rearrangeIntervals(const vector<IntervalActivity> &intervals)const
{
  vector<IntervalActivity> rearranged = intervals;
  for(unsigned i=0; i<rearranged.size(); i++){
    // exapnd forward
    if(i<rearranged.size() -1){
      //assert(rearranged[i].activity != rearranged[i+1].activity);
      double diff = (rearranged[i+1].entry - rearranged[i].exit)/2.0;
      rearranged[i+1].entry -= diff;
      rearranged[i].exit = rearranged[i+1].entry;
    }
    // expand backward
    if(i>0){
      //assert(rearranged[i-1].activity != rearranged[i].activity);
      double diff = rearranged[i].entry - rearranged[i-1].exit;
      rearranged[i].entry -= diff;
      rearranged[i-1].exit = rearranged[i].entry; 
    }
  }
  return rearranged;
}
/** @brief we need to merge unnecessary intervals which have the same activity 
  * @param[in] intervals that should be merged
  * @return merged intervals
  * e.g. in the second step of structure detection, we have too many grids. This comes from introducing
  * a grid for all controls "equally" (according to Schlegel's Algorithm). Thus, we might end up
  * with a control with multiple grids that have the same activity. In order to remove unnecessary grids
  * and to split the grids later again, we have to merge them first.
  */
vector<IntervalActivity> IterativeDynOpt::mergeIntervals(const vector<IntervalActivity> &intervals)const
{
  unsigned numInt = intervals.size();
  vector<IntervalActivity> mergedIntervals;
  mergedIntervals.push_back(intervals[0]);
  unsigned mergedIndex = 0;
  for(unsigned i=0; i<numInt-1; i++){
    if(intervals[i].activity == intervals[i+1].activity){
      mergedIntervals[mergedIndex].exit = intervals[i+1].exit;
      mergedIntervals[mergedIndex].isSuccessive = true;
    }else{
      mergedIntervals.push_back(intervals[i+1]);
      mergedIndex++;
    }
  }

  // not active intervals only if they are between two intervals with active bounds
  vector<IntervalActivity> withoutSmallIntervals;
  // we always include the first interval.
  // it is not followed by or preceded by an active bound. Thus, it is a valid arc.
  withoutSmallIntervals.push_back(mergedIntervals[0]);
  unsigned smallIndex = 1;
  for(unsigned i=1; i<mergedIntervals.size() - 1; i++){
    if( !mergedIntervals[i].isSuccessive &&  mergedIntervals[i].activity == NOTACTIVE){
      withoutSmallIntervals[smallIndex-1].exit = mergedIntervals[i].exit;
    }else{
      smallIndex++;
      withoutSmallIntervals.push_back(mergedIntervals[i]);
    }
  }
  // we always include the last interval.
  // it is not followed by or preceded by an active bound. Thus, it is a valid arc.
  if(mergedIntervals.size() > 1)
    withoutSmallIntervals.push_back(mergedIntervals.back());

  return withoutSmallIntervals;
}
/** @brief updates the controls according to the acitivity patterns of the path contraints
 *  and the control bounds. Grids are introduced for each identified active interval
 *  @param[in] controlActivity obtained by AdaptStructure::getControlActivity
 *  @param[in] pathActivity -> this variable is not used. We assume everything to be "Path" constrained,
 *  if it is not at active bounds. We ignore sensitivity seeking arcs.
 *  @param[in] controls of the previous solution.
 *  @param[out] arcType stores the activity of the grid for the structure detection
 *  @return updated controls which correspond to the new structure.
 */
ArcPattern IterativeDynOpt::getArcStructure(const ActivityMap &controlActivity,
                                            const ActivityMap &pathActivity,
                                            const vector<ParameterInput> &controls)const
{
  // this function is necessary in case we want to destinuish between path constraint and sensitivity
  // seeking arcs. But here, we don't do it. So it appears that the function is pointless.
  ActivityMap cActivityCopy =controlActivity;
  ActivityMap pActivityCopy = pathActivity;
  ArcPattern arcType;

  ActivityMap::iterator it;
  for(it = cActivityCopy.begin(); it != cActivityCopy.end(); ++it){
    const int pos = findControlIndex(controls, it->first);
    const unsigned numGrids = it->second.size();
    for(unsigned j=0; j<numGrids; j++){
      if(j != 0){
        if(arcType[controls[pos].esoIndex].back() != it->second[j].activity)
          arcType[controls[pos].esoIndex].push_back(it->second[j].activity);
      }else{
        arcType[controls[pos].esoIndex].push_back(it->second[j].activity);
      }
    }
  }
  return arcType;
}

/** @brief first structure is assumed to be all not active and has the same number of grids as specified
  * by the user
  * @return the arc pattern given by the user.
  */
vector<ArcPattern> IterativeDynOpt::firstControlStructure(void)const
{
  const unsigned numStages = m_problemInput.optimizer.integratorInput.metaDataInput.stages.size();
  vector<ArcPattern> firstArcStructure(numStages);
  const vector<FactoryInput::IntegratorMetaDataInput> stages = 
    m_problemInput.optimizer.integratorInput.metaDataInput.stages;
  for(unsigned i=0; i<numStages; i++){
    const unsigned numControls = stages[i].controls.size();
    for(unsigned j=0; j<numControls; j++){
      if(stages[i].controls[j].type == FactoryInput::ParameterInput::CONTROL){
        const unsigned esoIndex = stages[i].controls[j].esoIndex;
        const unsigned numGrids = stages[i].controls[j].grids.size();
        for(unsigned k=0; k<numGrids; k++){
          firstArcStructure[i][esoIndex].push_back(NOTACTIVE);
        }// grid loop
      }
    }// control loop
  }// stage loop
  return firstArcStructure;
}
/** @brief compares the previous arc structure stored in the class with the new structure stored in class
  * @return true if the structure is equal
  */
bool IterativeDynOpt::isArcStructureEqual()const
{
  bool isEqual = true;
  for(unsigned i = 0; i<m_newArcStructure.size(); i++){
    ArcPattern::const_iterator itNew, itOld;
    itOld = m_prevArcStructure[i].begin();
    assert(m_prevArcStructure[i].size() == m_newArcStructure[i].size());
    for(itNew = m_newArcStructure[i].begin(); itNew != m_newArcStructure[i].end(); ++itNew, ++itOld){
        assert(itNew->first == itOld->first);
        if(itNew->second.size() != itOld->second.size()){
          isEqual = false;
          break;
        }else{
          for(unsigned k=0; k<itNew->second.size(); k++){
            if(itNew->second[k] != itOld->second[k]){
              isEqual = false;
              break;
            }
          }
        }
    }
  }
  return isEqual;
}
/** @brief updates the controls according to the acitivity patterns of the path contraints
 *  and the control bounds. Grids are introduced for each identified active interval
 *  @param[in] controlActivity intervals of active and inactive bounds
 *  @param[in] pathActivity -> this variable is not used. currently everything "sensitivity" seeking
 *  @param[in] controls of the current solution.
 *  @return updated controls which correspond to the new structure.
 */
vector<ParameterInput> IterativeDynOpt::formulateMultiGridProblem(const ActivityMap &cActivity,
                                                                  const ActivityMap &pActivity,
                                                                  const vector<ParameterInput> &controls)const
{
  vector<ParameterInput> suggControls = controls;
  // first we merge the old control grids such that we can use the previous optimal solution
  // to initialize the new grids. This is only interesting for the path and sensitivity seeking arcs.
  map<unsigned, vector<double> > mergedPnts, mergedVals, interpPnts, interpVals;

  const int interpRes = 1024;
  for(unsigned i=0; i<controls.size(); i++){
    if(controls[i].type == FactoryInput::ParameterInput::CONTROL 
       || controls[i].type == FactoryInput::ParameterInput::CONTINUOUS_CONTROL){
      unsigned eInd = controls[i].esoIndex;
      mergeGrids(controls[i].grids, mergedPnts[eInd], mergedVals[eInd]);
      interpGrid(interpRes, mergedPnts[eInd], mergedVals[eInd], interpPnts[eInd], interpVals[eInd]);
    }
  }

  ActivityMap::const_iterator iter;
  for(iter = cActivity.begin(); iter != cActivity.end(); ++iter){
    const int pos = findControlIndex(suggControls, iter->first);
    // set correct grid size and take settings of first grid for all new grids
    suggControls[pos].grids.resize(iter->second.size());
    for(unsigned i=1; i<iter->second.size(); i++){
      suggControls[pos].grids[i] = suggControls[pos].grids[0];
    }
    unsigned eInd = suggControls[pos].esoIndex;

    double elpasedTime = 0;
    for(unsigned j=0; j<iter->second.size(); j++){
      // we set the final time and keep it free for each grid
      suggControls[pos].grids[j].duration = iter->second[j].exit - iter->second[j].entry;
      suggControls[pos].grids[j].hasFreeDuration = true;
      ConstraintActivity activity = iter->second[j].activity;

      if(activity == ACTIVEUPPERBOUND || activity == ACTIVELOWERBOUND){
        setToBounds(activity,
                    suggControls[pos].upperBound,
                    suggControls[pos].lowerBound,
                    suggControls[pos].grids[j]);
      }else{
        setSensSeeking(interpPnts[eInd],
                       interpVals[eInd],
                       elpasedTime,
                       suggControls[pos].upperBound,
                       suggControls[pos].lowerBound,
                       suggControls[pos].grids[j]);
      }
      elpasedTime += suggControls[pos].grids[j].duration;
    }
  }
  return suggControls;
}
/** @brief sets the control to upper or lower bounds according to the activity
  * @param[in] interval activity of current control
  * @param[in] upperBound of the control
  * @param[in] lowerBound of the control
  * @param[in/out] grids are reformulated directly in this function.
  */
void IterativeDynOpt::setToBounds(const ConstraintActivity &activity,
                                  const double upperBound,
                                  const double lowerBound,
                                        ParameterizationGridInput &grids)const
{
  grids.approxType = PieceWiseConstant;
  grids.parametersFixed = true;
  const unsigned resolution = 1;
  grids.values.clear();

  if(activity == ACTIVEUPPERBOUND){
    grids.values.resize(resolution, upperBound);
  }else{
    grids.values.resize(resolution, lowerBound);
  }
  grids.timePoints.resize(resolution);
  for(unsigned iRes = 0; iRes < resolution; iRes++){
    grids.timePoints[iRes] = double(iRes) / double(resolution);
  }
  grids.adapt.adaptWave.minRefinementLevel = 1;
  grids.pcresolution = 2;
  grids.adapt.maxAdaptSteps = 0;
  grids.adapt.adaptWave.type = PieceWiseConstant;
  grids.approxType = PieceWiseConstant;

  grids.adapt.swAdapt.type = 1;
  // last point has to be set manually.
  grids.timePoints.push_back(1.0);
}

void IterativeDynOpt::setSensSeeking(const vector<double> &interpPoints,
                                     const vector<double> &interpValues,
                                     const double elpasedTime,
                                     const double upperBound,
                                     const double lowerBound,
                                           ParameterizationGridInput &grids)const
{
  int numIntervals = 2;
  grids.approxType = PieceWiseLinear;
  numIntervals++;
  grids.timePoints.resize(numIntervals);
  grids.values.resize(numIntervals);


  for(int k=0; k<numIntervals; k++){
    if(grids.approxType == PieceWiseConstant){
      grids.timePoints[k] = double(k)/double(numIntervals);
    }else{
      grids.timePoints[k] = double(k)/double(numIntervals-1);
    }
  }
  if(grids.approxType == PieceWiseConstant){
    grids.timePoints.push_back(1.0);
  }
  vector<double> unscaledPoints = grids.timePoints;
  for(unsigned i=0; i<unscaledPoints.size(); i++){
    unscaledPoints[i] *= grids.duration;
    unscaledPoints[i] += elpasedTime;
  }
  std::vector<double>::iterator low,up;
  vector<double> interpPointsCopy = interpPoints;
  for(int k=0; k<numIntervals; k++){
    low = std::lower_bound (interpPointsCopy.begin(), interpPointsCopy.end(), unscaledPoints[k]);
    up = std::upper_bound (interpPointsCopy.begin(), interpPointsCopy.end(), unscaledPoints[k]);
    if(up != interpPointsCopy.end()){
      if( fabs(*low - unscaledPoints[k]) > fabs(*up - unscaledPoints[k]) ){
        grids.values[k] = interpValues[int(up-interpPointsCopy.begin())];
      }else{
        grids.values[k] = interpValues[int(low-interpPointsCopy.begin())];
      }
    }else if(up == interpPointsCopy.end() && low != interpPointsCopy.end()){
      grids.values[k] = interpValues[int(low-interpPointsCopy.begin())];
    }else{
      grids.values[k] = interpValues.back();
    }
  }

  grids.pcresolution = 2;
  grids.parametersFixed = false;
  grids.adapt.adaptWave.minRefinementLevel = 1;
  grids.adapt.adaptWave.type = PieceWiseLinear;
  grids.adapt.swAdapt.type = 2;
  grids.adapt.swAdapt.includeTol = 1e-2;
}

//////////////////////////////////////////////////////////////////////////////////////////////
////////// End Step 2: Structure detection loop and subfunctions /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

/** @brief merges all grids of a control within a stage into one grid
  * @param[in] grids control grid structure 
  * @param[out] gridPoints of the combined grids
  * @param[out] values of the combined grids
  */
void IterativeDynOpt::mergeGrids(const  vector<ParameterizationGridInput> &grids,
                                        vector<double> &gridPoints,
                                        vector<double> &values)const
{
  double elpasedTime = 0;
  for(unsigned i=0; i<grids.size(); i++){
    for(unsigned j=0; j<grids[i].values.size(); j++){
      gridPoints.push_back( grids[i].timePoints[j] * grids[i].duration + elpasedTime);
      values.push_back(grids[i].values[j]);
    }
    if(grids[i].values.size() != grids[i].timePoints.size()){
      gridPoints.push_back(grids[i].timePoints.back() * grids[i].duration + elpasedTime);
      values.push_back(grids[i].values.back());
    }
    elpasedTime += grids[i].duration;
  }
}

/** @brief interpolates grid with given interpResolution to a finer resolution. 
  * @param[in] interpResolution resolution of the interpolated grid
  * @param[in] gridPoints of the original grid
  * @param[in] values of the original grid
  * @param[out] interpGridPoints grid points of the interpolated grid with interpResolution points
  * @param[out] interpValues valuesof the interpolated grid with interpResolution values
  */
void IterativeDynOpt::interpGrid(const  int interpResolution,
                                 const  std::vector<double> &gridPoints,
                                 const  std::vector<double> &values,
                                        std::vector<double> &interpGridPoints,
                                        std::vector<double> &interpValues)const
{
  interpGridPoints.resize(interpResolution);
  for(int i=0; i<interpResolution; i++){
    interpGridPoints[i] = double(i) * (1.0 / double(interpResolution));
  }
  interpGridPoints.push_back(1.0);

  vector<double> gridPointsScaled = gridPoints;
  double scaleVal = gridPointsScaled.back();
  for(unsigned i=0; i<gridPointsScaled.size(); i++){
    gridPointsScaled[i] = gridPointsScaled[i]/scaleVal;
  }

  int order = 2; // we interpolate as pwl
  interpValues = utils::interp(&gridPointsScaled[0],
                               &values[0],
                               values.size(),
                               &interpGridPoints[0],
                               interpGridPoints.size(),
                               order);

  for(unsigned i=0; i<interpGridPoints.size(); i++){
    interpGridPoints[i] *= scaleVal;
  }

}

/** @brief help function that fills the TimeIntervalAcitivitiy struct 
  *
  * @param[in] in time point of entry to interval
  * @param[in] out exit point of interval
  * @param[in] act activity of interval: ACTIVEUPPERBOUND; ACTIVELOWERBOUND; NOTACTIVE
  * @param[in] isSucc says if the interval has mulitple points included.
  */
IntervalActivity IterativeDynOpt::createTimeActivity(const double in,
                                                         const double out,
                                                         const ConstraintActivity act,
                                                         const bool isSucc)const
{
  IntervalActivity tia;
  tia.activity = act;
  tia.entry = in;
  tia.exit = out;
  tia.isSuccessive = isSucc;
  return tia;
}

/** @brief rescales the time grid 
  *
  */
vector<double> IterativeDynOpt::rescaleGrid(const std::vector<double> grid,
                                            const double gridDuration,
                                            const double previousGridTimes)const
{
  vector<double> scaledGrid = grid;
  // scale with stage duration
  std::transform(scaledGrid.begin(), scaledGrid.end(), scaledGrid.begin(), 
                 std::bind1st(std::multiplies<double>(), gridDuration));
  std::transform(scaledGrid.begin(), scaledGrid.end(), scaledGrid.begin(), 
                 std::bind1st(std::plus<double>(), previousGridTimes));

  return scaledGrid;
}

/** @brief finds the esoIndex within a control 
  * @param[in] controls
  * @param[in] searchIndex, esoIndex we want to look for
  * @return always returns a valid int. Or it will fail with an assert.
  */
int IterativeDynOpt::findControlIndex(const vector<ParameterInput> &controls,
                                      const unsigned searchIndex)const
{
  int pos = -1;
  for(unsigned l=0; l<controls.size(); l++){
    if(controls[l].esoIndex == searchIndex){
      pos = l;
    }
  }
  assert(pos != -1);
  return pos;
}

/** @brief checks if a double value is either an entry or exit point of IntervalAcitivity
  * @param[in] tia the vector to be searched
  * @param[in] value the value to be searched
  * @return not found = true; found = false;
  */
bool IterativeDynOpt::findEntryOrExit(const vector<IntervalActivity> &tia,
                                      const double &value)const
{
  bool notFound = true;
  unsigned nGrid = tia.size();
  // we skip the first entry because it is always 0
  for(unsigned k=1; k<nGrid; k++){
    if(tia[k].entry == value || tia[k].exit == value){
      notFound = false;
      break;
    }
  }
  return notFound;
}

/** @brief searches for first entry value that is larger then value in the vector
  * @param[in] tia the vector to be searched
  * @param[in] value the value to be searched
  * @return position of the first entry value that is larger than the searched value
  */
unsigned IterativeDynOpt::findEntryPosition(const vector<IntervalActivity> &tia,
                                            const double &value)const
{
  unsigned nGrid = tia.size();
  unsigned pos = nGrid; // if nothing was found, then we point to the end
  // we skip the first entry because it is always 0
  for(unsigned k=1; k<nGrid; k++){ 
    if(tia[k].entry > value){
      pos = k;
      break;
    }
  }
  return pos;
}

