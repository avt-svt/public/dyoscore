
#include "UserOutput.hpp"
using namespace UserOutput;

template <typename T>
bool operator == (const std::vector<T>& lhs, const std::vector<T>& rhs)
{
  return lhs.size() == rhs.size() && std::equal(lhs.begin(), lhs.end(), rhs.begin());
}
bool EsoOutput::operator==(const EsoOutput &other)const
{
    if(!(this->model==other.model))
        return false;
    if(!(this->type==other.type))
        return false;

    return true;
}
bool MappingOutput::operator==(const MappingOutput &other)const
{
    if(!(this->fullStateMapping==other.fullStateMapping))
        return false;
    if(!(this->stateNameMapping==other.stateNameMapping))
        return false;

    return true;
}
bool StateGridOutput::operator==(const StateGridOutput &other)const
{
    if(!(this->timePoints==other.timePoints))
        return false;
    if(!(this->values==other.values))
        return false;

    return true;
}
bool StateOutput::operator==(const StateOutput &other)const
{
    if(!(this->name==other.name))
        return false;
    if(!(this->esoIndex==other.esoIndex))
        return false;
    if(!(this->grid==other.grid))
        return false;

    return true;
}
bool FirstSensitivityOutput::operator==(const FirstSensitivityOutput &other)const
{
    if(!(this->mapParamsCols==other.mapParamsCols))
        return false;
    if(!(this->mapConstrRows==other.mapConstrRows))
        return false;
    if(!(this->values==other.values))
        return false;

    return true;
}
bool ParameterGridOutput::operator==(const ParameterGridOutput &other)const
{
    if(!(this->numIntervals==other.numIntervals))
        return false;
    if(!(this->timePoints==other.timePoints))
        return false;
    if(!(this->values==other.values))
        return false;
    if(!(this->duration==other.duration))
        return false;
    if(!(this->hasFreeDuration==other.hasFreeDuration))
        return false;
    if(!(this->type==other.type))
        return false;
    if(!(this->firstSensitivities==other.firstSensitivities))
        return false;
    if(!(this->isFixed == other.isFixed))
        return false;


    return true;
}
bool ParameterOutput::operator==(const ParameterOutput &other)const
{
    if(!(this->name==other.name))
        return false;
    if(!(this->isOptimizationParameter==other.isOptimizationParameter))
        return false;
    if(!(this->lowerBound==other.lowerBound))
        return false;
    if(!(this->upperBound==other.upperBound))
        return false;
    if(!(this->value==other.value))
        return false;
    if(!(this->grids==other.grids))
        return false;
    if(!(this->paramType==other.paramType))
        return false;
    if(!(this->sensType==other.sensType))
        return false;

    return true;
}
bool IntegratorStageOutput::operator==(const IntegratorStageOutput &other)const
{
    if(!(this->durations==other.durations))
        return false;
    if(!(this->parameters==other.parameters))
        return false;
    if(!(this->states==other.states))
        return false;
    if(!(this->adjoints == other.adjoints)){
      return false;
    }
    if(!(this->adjoints2ndOrder == other.adjoints2ndOrder)){
      return false;
    }

    return true;
}
bool ConstraintGridOutput::operator==(const ConstraintGridOutput &other)const
{
    if(!(this->timePoints==other.timePoints))
        return false;
    if(!(this->values==other.values))
        return false;
    if(!(this->lagrangeMultiplier==other.lagrangeMultiplier))
        return false;

    return true;
}
bool ConstraintOutput::operator==(const ConstraintOutput &other)const
{
    if(!(this->name==other.name))
        return false;
    if(!(this->lowerBound==other.lowerBound))
        return false;
    if(!(this->upperBound==other.upperBound))
        return false;
    if(!(this->grids==other.grids))
        return false;
    if(!(this->type==other.type))
        return false;

    return true;
}
bool OptimizerStageOutput::operator==(const OptimizerStageOutput &other)const
{
    if(!(this->objective==other.objective))
        return false;
    if(!(this->nonlinearConstraints==other.nonlinearConstraints))
        return false;
    if(!(this->linearConstraints==other.linearConstraints))
        return false;

    return true;
}
bool StageOutput::operator==(const StageOutput &other)const
{
    if(!(this->treatObjective==other.treatObjective))
        return false;
    if(!(this->mapping==other.mapping))
        return false;
    if(!(this->eso==other.eso))
        return false;
    if(!(this->integrator==other.integrator))
        return false;
    if(!(this->optimizer==other.optimizer))
        return false;

    return true;
}
bool IntegratorOutput::operator==(const IntegratorOutput &other)const
{
    if(!(this->type==other.type))
        return false;
    if(!(this->order==other.order))
        return false;

    return true;
}
bool SecondOrderOutput::operator==(const SecondOrderOutput &other)const
{
    if(!(this->indicesHessian==other.indicesHessian))
        return false;
    if(!(this->values==other.values))
        return false;
    if(!(this->compositeAdjoints==other.compositeAdjoints))
        return false;

    return true;
}
bool OptimizerOutput::operator==(const OptimizerOutput &other)const
{
    if(!(this->type==other.type))
        return false;
    if(!(this->resultFlag==other.resultFlag))
        return false;
    if(!(this->globalConstraints==other.globalConstraints))
        return false;
    if(!(this->linearConstraints==other.linearConstraints))
        return false;
    if(!(this->secondOrder==other.secondOrder))
        return false;

    return true;
}

bool UserOutput::Output::operator==(const Output &other)const
{
    if(!(this->stages==other.stages))
        return false;
    if(!(this->totalEndTimeLowerBound==other.totalEndTimeLowerBound))
        return false;
    if(!(this->totalEndTimeUpperBound==other.totalEndTimeUpperBound))
        return false;
    if(!(this->runningMode==other.runningMode))
        return false;
    if(!(this->integratorOutput==other.integratorOutput))
        return false;
    if(!(this->optimizerOutput==other.optimizerOutput))
        return false;
    if(!(this->solutionHistory == other.solutionHistory))
        return false;

    return true;
}

