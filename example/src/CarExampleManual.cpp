#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include <utility>
#include <string>

//comprehensive car example
//to compile and run the problem, the correct header and dlls must be available
//it has to be linked against dyos.lib and the bin folder must be in path while
//executing
int main(){
	// variable names have to be equal to those in .mof file!
	const std::string varNameA = "accel";
	const std::string varNameT = "ttime";
	const std::string varNameV = "velo";
	const std::string varNameS = "dist";
	const std::string varNameObj = "obj";
	const std::string varNamePar = "k";


	const int numStages = 1;
  //define first stage 1
  UserInput::StageInput stage1;
  {
    //define esoInput
    UserInput::EsoInput eso;
    eso.type = UserInput::EsoInput::JADE; //either JADE or FMI
    eso.model = "CarExampleManualJade"; //name of the dll
	//eso.type = UserInput::EsoInput::FMI; //either JADE or FMI
	//eso.model = "CarExampleManualFMU"; //name of the dll

    stage1.eso = eso;
    //define integrator stage input
    UserInput::IntegratorStageInput integratorStage;
    {
      //define time horizon of optimization problem
      UserInput::ParameterInput duration;
      duration.lowerBound = 1e-1 / numStages;
      duration.upperBound = 100 / numStages;
      duration.value = 10.0;
      duration.sensType = UserInput::ParameterInput::FRACTIONAL;
	  /* FULL: duration is optimization variable, 
		 FRACTIONAL: sensitivities shall be calculated, although it is a constant (parameter)
		 NO: constant, do not calculate sensitivities of duration
		*/
      duration.paramType = UserInput::ParameterInput::DURATION;
      
      integratorStage.duration = duration;

      //define control k
      UserInput::ParameterInput par_k;
      par_k.name = varNamePar;
	  par_k.lowerBound = -2.0;
	  par_k.upperBound = 2.0;
	  //par_k.value = 1;
	  // set to TIME_INVARIANT for introductory car example
	  par_k.paramType = UserInput::ParameterInput::PROFILE;
	  /* PROFILE: time dependent values, define grid as below
	     TIME_INVARIANT: for constants
		 DURATION: for times like t_f
	  */
	  par_k.sensType = UserInput::ParameterInput::FULL;
	  /* FULL: parameter is decision variable
		 FRACTIONAL: parameter is no decision variable
		 NO: parameter bounds are ignored
	  */

      //define control grid for singlestage problem
      UserInput::ParameterGridInput grid; 
	  const unsigned numIntervals = 3;
	  grid.numIntervals = numIntervals;
      grid.values.resize(numIntervals, 1);
	  grid.adapt.maxAdaptSteps = 10;
	  grid.pcresolution = 1;
      grid.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;

	  grid.hasFreeDuration = true;

	  par_k.grids.push_back(grid);
      
      integratorStage.parameters.push_back(par_k);
    }
    
	integratorStage.plotGridResolution = 100;

	stage1.integrator = integratorStage;

    //define optimizer stage input
    UserInput::OptimizerStageInput optimizerStage;
    {
      // define objective (must always be defined somehow, but never use an actual constraint as
      // dummy objective
      UserInput::ConstraintInput objective;
      objective.name = varNameObj;
      objective.lowerBound = -100000.0;
      objective.upperBound =  100000.0;
      optimizerStage.objective = objective;

      //define constraints
      UserInput::ConstraintInput vPathConstr;
	  vPathConstr.name = varNameV;
	  vPathConstr.type = UserInput::ConstraintInput::PATH;
	  vPathConstr.lowerBound = -1.6;
	  vPathConstr.upperBound = 1.6;

	  // Comment vEndConstr for introductory car example (v(t_f) = 0)
	  UserInput::ConstraintInput vEndConstr;
	  vEndConstr.name = varNameV;
	  vEndConstr.type = UserInput::ConstraintInput::ENDPOINT;
	  vEndConstr.lowerBound = 0;
	  vEndConstr.upperBound = 0;
	  
      optimizerStage.constraints.push_back(vPathConstr);
	  optimizerStage.constraints.push_back(vEndConstr);
    }
    stage1.optimizer = optimizerStage;

    //in multistage always set full state mapping true, in single stage to false
    stage1.mapping.fullStateMapping = false;
    //objective of the first stage is no taken into account
    //stage1.treatObjective = true;
  }

  UserInput::AdaptationOptions adaptation;
  adaptation.adaptStrategy = UserInput::AdaptationOptions::ADAPT_STRUCTURE;

  UserInput::Input input;

  input.stages.push_back(stage1);

  //define integrator and optimizer
  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::IPOPT;
  
  //define some optimizer options
  std::map<std::string, std::string> optimizerOptions;
  optimizerOptions.insert(std::make_pair<std::string, std::string>("Major Iterations Limit", "250"));
  optimizerOptions.insert(std::make_pair<std::string, std::string>("Minor Iterations Limit", "500"));
  input.optimizerInput.optimizerOptions = optimizerOptions;
  input.optimizerInput.adaptationOptions = adaptation;

  //define logger for output (otherwise standard logger is used)
  Logger::Ptr logger(new StandardLogger());
  //create logger writing into statenames.dat
  OutputChannel::Ptr statenamesDat(new FileChannel("statenames.dat"));
  logger->setLoggingChannel(Logger::STATENAMES_OUT, statenamesDat);
  //create logger writing into finalstates.dat
  OutputChannel::Ptr finalstateDat(new FileChannel("finalstates.dat"));
  logger->setLoggingChannel(Logger::FINALSTATES_OUT, finalstateDat);
  
  UserOutput::Output output;
  output = runDyos(input,logger);
 
  return 0;
}