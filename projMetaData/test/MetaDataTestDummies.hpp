/**
* @file MetaDataTestDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Single Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* Dummyclasses for MetaSingleStageEsoTest                              \n
* =====================================================================\n
* @author Tjalf Hoffmann, Klaus Stockmann, Kathrin Frankl, Fady Assassa
* @date 26.01.2012
*/

#pragma once

#include "IntegratorSingleStageMetaData.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include "Parameter.hpp"
#include "Array.hpp"
#include <vector>
#include "Constraints.hpp"
#include "Mapping.hpp"
#include <memory>

/**
* @class DummyGenericEso
* @brief dummy implementation of abstract class GenericEso; used for test modules
*/
class DummyGenericEso:public GenericEso
{
public:
  virtual EsoIndex getNumVariables() const {return 0;};
  virtual EsoIndex getNumDifferentialVariables() const {return 0;};
  virtual EsoIndex getNumParameters() const {return 0;};
  virtual EsoIndex getNumAlgebraicVariables() const {return 0;};
  virtual EsoIndex getNumStates() const {return 0;};
  virtual EsoIndex getNumEquations() const {return 0;};
  virtual EsoIndex getNumDiffEquations() const {return 0;};
  virtual EsoIndex getNumAlgEquations() const {return 0;};
  virtual EsoIndex getNumNonZeroes() const {return 0;};
  virtual EsoIndex getNumDifferentialNonZeroes() const {return 0;};
  virtual EsoIndex getNumInitialNonZeroes() const{return 0;};
  virtual ModelName getModel() const{return " ";}

  virtual void setIndependentVariable(const EsoDouble var){}
  virtual EsoDouble getIndependentVariable() const {return 0.0;}
  virtual void getVariableNames(std::vector<std::string> &names){}
  virtual void setAllVariableValues(const EsoIndex n_var, const EsoDouble *variables){}
  virtual void setParameterValues(const EsoIndex n_pars, const EsoDouble *parameters){}
  virtual void setAlgebraicVariableValues(const EsoIndex n_alg_var, const EsoDouble *algebraicVariables){}
  virtual void setDifferentialVariableValues(const EsoIndex n_diff_var, const EsoDouble *differentialVariables){}
  virtual void setStateValues(const EsoIndex n_states, const EsoDouble *states){}
  virtual void setVariableValues(const EsoIndex n_idx, const EsoDouble *variables, const EsoIndex *indices){}
  virtual void getAllVariableValues(const EsoIndex n_var, EsoDouble *variables) const {}
  virtual void getParameterValues(const EsoIndex n_params, EsoDouble *parameters){}
  virtual void getAlgebraicVariableValues(const EsoIndex n_alg_var, EsoDouble *algebraicVariables){}
  virtual void getDifferentialVariableValues(const EsoIndex n_diff_var, EsoDouble *differentialVariables){}
  virtual void getStateValues(const EsoIndex n_states, EsoDouble *states){}
  virtual void getInitialStateValues(const EsoIndex n_states, EsoDouble *states){}
  virtual GenericEso::RetFlag setInitialValues(const EsoIndex,EsoIndex *,EsoDouble *,const EsoIndex,EsoIndex *){return GenericEso::OK;}
  virtual void getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices){}
  virtual void setDerivativeValues(const EsoIndex n_diff_var, const EsoDouble *derivatives){}
  virtual void getDerivativeValues(const EsoIndex n_diff_var, EsoDouble *derivatives){}
  virtual void getAllBounds(const EsoIndex n_var, EsoDouble *lowerBounds, EsoDouble *upperBounds){}
  virtual GenericEso::RetFlag getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals){return GenericEso::OK;}
  virtual GenericEso::RetFlag getDifferentialResiduals(const EsoIndex n_diff_eq, EsoDouble *differentialResiduals){return GenericEso::OK;}
  virtual GenericEso::RetFlag getAlgebraicResiduals(const EsoIndex n_alg_eq, EsoDouble *algebraicResiduals){return GenericEso::OK;}
  virtual GenericEso::RetFlag  getResiduals(const EsoIndex n_eq, EsoDouble *residuals, const EsoIndex *equationIndex){return GenericEso::OK;}
  virtual void getJacobianStruct(const EsoIndex n_nz, EsoIndex *rowIndices, EsoIndex *colIndices){}
  virtual GenericEso::RetFlag  getJacobianValues(const EsoIndex n_nz, EsoDouble *values){return GenericEso::OK;}
  virtual GenericEso::RetFlag  getJacobianValues(const EsoIndex n_idx, EsoDouble *values, const EsoIndex *indices){return GenericEso::OK;}

  virtual void getDiffJacobianStruct(const EsoIndex n_diff_nz, EsoIndex *rowIndices, EsoIndex *colIndices){}
  virtual GenericEso::RetFlag  getDiffJacobianValues(const EsoIndex n_diff_nz, EsoDouble *values){return GenericEso::OK;}
  virtual GenericEso::RetFlag getInitialJacobian(const EsoIndex n_nz, EsoIndex *rowIndices,
                                                 EsoIndex *colIndices, EsoDouble *values){return GenericEso::OK;}
  virtual GenericEso::RetFlag  getJacobianMultVector(const bool transpose, const EsoIndex n_states, EsoDouble *seedStates,
                                     const EsoIndex n_params, EsoDouble *seedParameters, const EsoIndex n_y,
                                     EsoDouble *y){return GenericEso::OK;}

  virtual void getParameterIndex(const EsoIndex n_para, EsoIndex *parameterIndex){}
  virtual void getDifferentialIndex(const EsoIndex n_diff_var, EsoIndex *differentialIndex){}
  virtual void getAlgebraicIndex(const EsoIndex n_alg_var, EsoIndex *algebraicIndex){}
  virtual void getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex){}
  virtual void getDiffEquationIndex(const EsoIndex n_diff_eq, EsoIndex *diffEqIndex){}
  virtual void getAlgEquationIndex(const EsoIndex n_alg_eq, EsoIndex *algEqIndex){}
  virtual EsoType getType() const { return EsoType::JADE; }
};

/**
* @class DummyMapping
* @brief dummy implementation of abstract class Mapping; used for test modules
*/
class DummyMapping : public Mapping
{
  /// @copydoc Mapping::applyStageMapping
  virtual void applyStageMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                                       IntegratorSingleStageMetaData::Ptr currentStage,
                                       vector<double> &multiStageSens){};
  /// @copydoc Mapping::calculateMappedSensitivitiesRhs
  void calculateMappedSensitiviesRhs(const IntegratorSingleStageMetaData::Ptr currentStage,
                                           utils::Array<double> &inputSensitivities,
                                           utils::Array<double> &rhsSensitivities){};
  /// @copydoc Mapping::setIndexMaps
  void setIndexMaps(const iiMap varIndexMap, const iiMap eqnIndexMap){};
  iiMap virtual getEquationsVectorMap(const iiMap &equationsMap)
  { return iiMap();}
  iiMap virtual getReverseEquationsVectorMap(const iiMap &equationsMap)
  {return iiMap();}
};

class IntegratorSingleStageMetaDataTestClass : public IntegratorSingleStageMetaData
{
public:
  IntegratorSingleStageMetaDataTestClass(const GenericEso::Ptr genEso){
    m_genericEso = genEso;
    createMassMatrix();
  }
};

/**
* @class SensitivityTestGenericEso
* @brief test class used by SensitivityTestMSSE object
*/
class SensitivityTestGenericEso : public DummyGenericEso
{
public:
  /**
  * @brief override the function GenericEso::getNumEquations
  * @return 5 (constant testdata)
  */
  virtual EsoIndex getNumEquations() const {return 5;};
  /**
  * @brief override the function GenericEso::getNumStates
  * @return 5 (constant testdata)
  */
  virtual EsoIndex getNumStates() const {return 5;};

};

/**
* @class SensitivityTestMSSE
* @brief test class for testing function calculateRhsSensitivities1stOrder
*
* Since this function is dependent on the parameter class this test class allows
* overriding the parametersWithSensitivity vector to replace the parameter with
* dummy objects.
*/
class SensitivityTestMSSE : public IntegratorSingleStageMetaData
{
public:
  //! @brief standard constructor using a dummy genericEso
  SensitivityTestMSSE()
  {
    GenericEso::Ptr genEso(new SensitivityTestGenericEso());
    m_genericEso=genEso;
  }

  /**
  * @brief returns a fixed number as number of equations
  *
  * Since this class does not hold a genericEso, the original function may not
  * be used. getNumEquations is called in the function to be tested though.
  * @return 5
  */
  int getNumEquations() const {return 5;}
  /**
  * @brief returns a fixed number as number of states
  *
  * Since this class does not hold a genericEso, the original function may not
  * be used. getNumEquations is called in the function to be tested though.
  * @return 5
  */
  int getNumStates() const {return 5;}

  /**
  * @brief get JacobianFstates (hard-coded)
  *
  * @return JacobianFstates
  */
  CsCscMatrix::Ptr getJacobianFstates()
  {
    const int nnz = 2;
    const int m = 5;
    const int n = 5;

    const int rowdata[] = {0, 2};
    const int coldata[] = {2, 2};
    const double valdata[] = {1.0, 2.0};

    CsTripletMatrix::Ptr tempTriplet(new CsTripletMatrix(m, n, nnz, &rowdata[0], &coldata[0], &valdata[0]));
    CsCscMatrix::Ptr jacFstates = tempTriplet->compress();
    return jacFstates;
  }

  /**
  * @brief setter for the attribute m_currentParamsWithSens
  *
  * This function is not in the original class. It is used to override the said
  * attribute for testing purpose.
  * @param params vector of Parameter pointer to be used for calculating RHS
  *         sensitivities
  */
  void setParameters(const vector<Parameter::Ptr> &params)
  {
    m_currentParamsWithSens.assign(params.begin(), params.end());
  }

  /**
  * @brief getter for the attribute m_currentParamsWithSens
  *
  * This function is not in the original class. It is used to check the said
  * attribute for testing purpose.
  * @param params vector receiving the Parameters
  */
  void getParameters(vector<Parameter::Ptr> &params) const
  {
    params.assign(m_currentParamsWithSens.begin(),
                  m_currentParamsWithSens.end());
  }

};

/**
* @class CarGenericEsoDummy
* @brief test class used by CarIntegratorMetaData
*/
class CarGenericEsoDummy : public DummyGenericEso
{
public:
  /**
  * @brief get the test residual vector ([11,12,13])
  *
  * overrides getAllResiduals from class GenericEso
  * @param[in] n_eq size of
  * @param[out] residuals vector receiving the residuals
  */
  GenericEso::RetFlag getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals)
  {
    assert(n_eq == 3);
    residuals[0] = -11.0;
    residuals[1] = 12E3;
    residuals[2] = 13.0;
    return GenericEso::OK;
  }

  /**
  * @copydoc GenericEso::getNumEquations
  * returns always 3
  */
  EsoIndex getNumEquations() const {return 3;}
};

/**
* @brief synthetic meta single stage eso which hard-coded data otherwise retrieved from a generic
*        eso
*
* this class is derived from MetaSingleStageEso and implemented for testing purposes
* @author Klaus Stockmann
*/
class CarIntegratorMetaData : public IntegratorSingleStageMetaData
{
public:
  CarIntegratorMetaData()
  {
    GenericEso::Ptr genEso(new CarGenericEsoDummy());
    m_genericEso = genEso;
  };

  /**
  * @brief get final time (hard-coded)
  *
  * @return final time
  */
  double getIntegrationGridDuration() const
  {
    return 40.0;
  }

  /**
  * @brief get JacobianFstates (hard-coded)
  *
  * @return JacobianFstates
  */
  CsCscMatrix::Ptr getJacobianFstates()
  {
    const int rowdata[] = {0, 2};
    const int coldata[] = {2, 2};
    const double valdata[] = {1.0, 2.0};

    const int nnz = 2;
    const int m = 3;
    const int n = 3;

    CsTripletMatrix::Ptr tempTriplet(new CsTripletMatrix(m, n, nnz, &rowdata[0], &coldata[0], &valdata[0]));
    CsCscMatrix::Ptr jacFstates = tempTriplet->compress();
    return jacFstates;
  }

  /**
  * @brief get JacobianFup (hard-coded)
  *
  * @return JacobianFup
  */
  CsCscMatrix::Ptr getJacobianFup()
  {
    const int rowdata[] = {2, 2};
    const int coldata[] = {0, 1};
    const double valdata[] = {3.0, 4.0};

    const int nnz = 2;
    const int m = 3;
    const int n = 5;

    CsTripletMatrix::Ptr tempTriplet(new CsTripletMatrix(m, n, nnz, &rowdata[0], &coldata[0], &valdata[0]));
    CsCscMatrix::Ptr jacFstates = tempTriplet->compress();
    return jacFstates;
  }
};

/**
* @class DummyParameter
* @brief dummy class to provide testdata when calling the function
*        getRhsSensForward1stOrder
*/
class DummyParameter : public Parameter
{
private:
  //! double value used to shift the fake sensitivity values
  double m_bias;
public:
  /**
  * @brief constructor setting the bias
  *
  * @param bias value of the bias to be set
  */
  DummyParameter(const double bias)
  {
    this->m_bias=bias;
  }

  /**
  * @brief unused function, being a pure virtual function it needs to be implemented
  *
  * @param initialValues values receiving the initial values
  */
  void getInitialValuesForIntegration(utils::Array<double> &initialValues,
                                      GenericEso::Ptr &genEso)const
  {}

  /**
  * @brief dummy function simply returning a consecutive set of integer values
  *
  * @param currentSens dummy input vector ignored by the function
  * @param jacobianFstates dummy variable that can be empty
  * @param imd dummy input object ignored by the function
  * @param rhsSens double vector receiving the integer values in the range
  *                (bias..bias+l) while l is the length of the vector and
  *                bias is the value of the attribute set in the constructor.
  */
  void  getRhsSensForward1stOrder(const utils::Array<double> &currentSens,
                                  const cs *jacobianFstates,
                                  IntegratorMetaData *imd,
                                  utils::Array<double> &rhsSens)
  {
    for(unsigned i=0; i<rhsSens.getSize(); i++){
      rhsSens[i] = m_bias+i;
    }
  }

  /**
  * @copydoc Parameter addDfDp
  * dummy function, that is not used, but has to be implemented
  */
  void addDfDp(utils::Array<double> &y, IntegratorMetaData *imd){}
  
  

  /**
  * @copydoc Parameter getDuDpIndex
  * dummy function, that is not used, but has to be implemented
  */
  int getDuDpIndex() const {return 0;}

  /**
  * @copydoc Parameter getDuDp
  * dummy function, that is not used, but has to be implemented
  */
  double getDuDp(const IntegratorMetaData *imd) const {return 0.0;}

  bool isOriginal(){return true;}
};

/**
* @class GenericEsoDummyForOptimizerMetaData
* @brief test class used by IntegratorMetaDataDummyForOptimizerMetaData
*/
class GenericEsoDummyForOptimizerMetaData : public DummyGenericEso
{
public:
  EsoIndex m_numStates;
  EsoIndex m_numEquations;
  EsoIndex m_numDiffVars;

  GenericEsoDummyForOptimizerMetaData()
  {
    m_numStates = 3;
    m_numEquations = m_numStates;
    m_numDiffVars = 1;
  }
  virtual EsoIndex getNumEquations() const {return m_numEquations;};
  virtual EsoIndex getNumStates() const {return m_numStates;}
  virtual void getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex)
  {
    assert(n_states == m_numStates);
    for(int i=0; i < m_numStates; i++){
      stateIndex[i] = i;
    }
  }
  /**
  * @brief get the test objective value (constant at 4.76)
  *
  * overrides getVariableValues from class MetaSingleStageEso
  * @param[out] objValue objValue vector receiving the objective value
  * @param[in] objIndex dummy vector (will not be used)
  */
  void getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices)
  {
    assert(n_idx == 1);
    variables[0] = 4.76;
  }
};

/**
* @class IntegratorMetaDataDummyForOptimizerMetaData
* @brief test class used by OptimizerInterface.testsuite
*/
class IntegratorMetaDataDummyForOptimizerMetaData:public IntegratorSingleStageMetaData
{
public:
  /**
  * @brief standard constructor
  *
  * standard constructor puts six parameters into the parameters vector
  */
  IntegratorMetaDataDummyForOptimizerMetaData()
  {
    for(unsigned i=0; i<6; i++){
      m_currentParamsWithSens.push_back(Parameter::Ptr(new ControlParameter()));
      m_currentParamsWithSens[i]->setParameterIndex(i);
      m_currentParamsWithSens[i]->setWithSensitivity(true);
      m_currentParamsWithSens[i]->setIsDecisionVariable(true);
      m_currentParamsWithSens[i]->setParameterLowerBound(4.1*(i+1));
      m_currentParamsWithSens[i]->setParameterUpperBound(7.3*(i+1));
      m_currentParamsWithSens[i]->setParameterValue(5.2*(i+1));
    }

    GenericEso::Ptr genEso(new GenericEsoDummyForOptimizerMetaData());
    m_genericEso = genEso;
    m_completeGridUnscaled.resize(1, 1.0);
    m_currentIndexCompleteGridUnscaled = 0;
    m_globalDurationParameter = DurationParameter::Ptr(new DurationParameter());
    m_globalDurationParameter->setParameterValue(1.0);
    m_globalDurationParameter->setParameterIndex(6);
    m_gridDurations.push_back(m_globalDurationParameter);
  }

  /**
  * @brief destructor deleting attribute m_currentParamsWithSens
  */
  ~IntegratorMetaDataDummyForOptimizerMetaData()
  {
  }

  /**
  * @brief return size of the constant parameters with sensitivity vector
  */
  unsigned getNumDecisionVariables() const
  {
    return m_currentParamsWithSens.size();
  }

  /**
  * @copydoc IntegratorMetaDataDummyForOptimizerMetaData::getNumSensitivityParameters
  */
  unsigned getNumSensitivityParameters() const
  {
    return m_currentParamsWithSens.size();
  }

  /**
  * @brief rerturn vector of constrant sensitivity parameters
  */
  void getDecisionVariables(std::vector<Parameter::Ptr> &decisionVariables) const
  {
    decisionVariables.assign(m_currentParamsWithSens.begin(),
                             m_currentParamsWithSens.end());
  }

  void getSensitivityParameters(std::vector<Parameter::Ptr> &sensParam) const
  {
    sensParam.assign(m_currentParamsWithSens.begin(),
                     m_currentParamsWithSens.end());
  }

  /**
  * @brief get the current sensitivity vector
  *
  * The output vector will simply be filled with values {0.0, 1.0, ...}.
  * So getNonLinObjDerivative from class OptimizerMetaData can be properly tested
  * @param curSens vector receiving the current sensitivities
  */
  void getCurrentSensitivities(vector<double> &curSens) const
  {
    curSens.resize(m_genericEso->getNumEquations() * getNumCurrentSensitivityParameters());
    for(unsigned i = 0; i < curSens.size(); i++){
      curSens[i] = double(i);
    }
  }
  /**
  * @brief constructs and returns some dummy data in the m_intOptData struct
  *
  * We include 3 constraints. The indices of the equation and the variablese are
  * sorted as 1, 2 and 3, respectivley. There are 6 active parameters, whith the parameter
  * indices are 1, 2, 3, 4, 5 and 6, respectivley. Thus, we have 18 nonlinear constraint
  * gradients.
  * @return a copy of m_intOptData is returned containing information constructed in the function
  */
  vector<IntOptDataMap> getIntOptData() const
  {
    // we include some dummy data
    vector<IntOptDataMap> m_intOptData(1);

    SavedOptDataStruct newTimePoint;
    EsoIndex numConstraints = 3;
    for (EsoIndex i=0; i<numConstraints; i++)
    {
      newTimePoint.nonLinConstVarIndices.push_back(i);
      newTimePoint.nonLinConstIndices.push_back(i);
      newTimePoint.nonLinConstValues.append(1.1*(i+1));
      newTimePoint.numSensParam = 6;
    }
    for (unsigned i=0; i<newTimePoint.numSensParam; i++)
    {
      newTimePoint.activeParameterIndices.push_back(i);
      newTimePoint.activeDecVarIndices.push_back(i);
    }
    for (unsigned i=0; i<numConstraints*newTimePoint.numSensParam; i++)
    {
      newTimePoint.nonLinConstGradients.append(1.1*(i+1));
    }
    m_intOptData[0].insert(pair<unsigned,SavedOptDataStruct>(0, newTimePoint));

    return m_intOptData;
  }
  /**
  * @brief we override the original function to avoid a crash in the test testGetOptProbDim
  *
  * In e.g. testGetOptProbDim we don't want to create consistent data for the constraints.
  * Thus, we overload the function such that it doesn't do anything.
  */
  virtual unsigned addNonlinearConstraint(const unsigned esoIndex,
                                          const unsigned eqnIndex,
                                          const unsigned constIndex,
                                          const double timePoint,
                                          const double lagrangeMultiplier){return 0;}

  void setIntOptData()
  {}
};


/**
* @class IntegratorMetaDataDummyForAddConstraintTest
* @brief test class for test case addConstraintTest
*/
class IntegratorMetaDataDummyForAddConstraintTest : public IntegratorSingleStageMetaData
{
public:
  /**
  * @brief creates a dummy eso for the test of optimizer meta data
  **/
  IntegratorMetaDataDummyForAddConstraintTest()
  {
    GenericEsoDummyForOptimizerMetaData *eso = new GenericEsoDummyForOptimizerMetaData();
    eso->m_numStates = 6;
    m_genericEso = GenericEso::Ptr(eso);

  }

  void setGlobalDurationParameter(const DurationParameter::Ptr &ft)
  {
    m_globalDurationParameter = ft;
    m_gridDurations.push_back(ft);
  }

  void setUserGrid(const std::vector<double> &userGrid)
  {
    m_additionalGrid.assign(userGrid.begin(), userGrid.end());
  }


  /**
  * @brief set constraint values for test purposes
  * @param timePoint time point at which the constraints are tested
  * @param values vector containing the values of the constraints at the given timepoint
  */
  void setConstraintTestValues(const double timePoint, const vector<double> &values)
  {
    const unsigned timeIndex = findTimeIndex(m_intOptData[0], timePoint);
    assert(m_intOptData[0][timeIndex].nonLinConstValues.getSize() == values.size());
    for(unsigned i=0; i<values.size(); i++){
      m_intOptData[0][timeIndex].nonLinConstValues[i] = values[i];
    }
  }
  void createGlobalGrid()
  {
    m_completeGridUnscaled.assign(m_additionalGrid.begin(), m_additionalGrid.end());
  }
};
/**
* @class GenericEsoDummyForMapping
* @brief test class used by IntegratorMetaDataDummyForMapping
*/
class GenericEsoDummyForMapping : public GenericEsoDummyForOptimizerMetaData
{
protected:
  // this is simulating an artificial eso
  map<EsoIndex, double> m_esoVals;
public:
  /** @brief fill up artificial eso with data
  **/
  GenericEsoDummyForMapping()
  {
    m_esoVals.insert(pair<EsoIndex, double>(0, 2.0));
    m_esoVals.insert(pair<EsoIndex, double>(1, 3.0));
    m_esoVals.insert(pair<EsoIndex, double>(2, 4.0));
  }
  /** @brief acts as artificial eso with burned in values
    * @return number of equations. */
  virtual EsoIndex getNumEquations() const {return m_esoVals.size();};
  /** @brief acts as artificial eso with burned in values
    * @return number of algebraic variables.*/
  virtual EsoIndex getNumAlgebraicVariables() const {return 1;};
  /** @brief acts as artificial eso with burned in values
    * @return number of differential variables.*/
  virtual EsoIndex getNumDifferentialVariables() const {return m_esoVals.size() - getNumAlgebraicVariables();};
  /** @brief acts as artificial eso with burned in values
    * @return number of variables.*/
  virtual EsoIndex getNumVariables() const {return m_esoVals.size();};

  /** @brief sets the variable values of the fake eso according to the parameters
    * @param[in] values the values that should be written in the fake eso
    * @param[in] indices the indices where the values should be written
    **/
  virtual void setVariableValues(const EsoIndex n_idx, const EsoDouble *variables, const EsoIndex *indices)
  {
    assert((unsigned)n_idx == m_esoVals.size());
    for(int i = 0; i < n_idx; i ++){
      m_esoVals[indices[i]] = variables[i];
    }
  };
  /** @brief gets the variable values of the fake eso according to the parameters
  *
  *
  * @param[in] values where the values are written from the fake eso. needs prior allocation
  * @param[in] indices where the indices are written from the fake eso. needs prior allocation
  **/
  void getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices)
  {
    assert((unsigned)n_idx == m_esoVals.size());
    for(unsigned i = 0; i < m_esoVals.size(); i ++){
      variables[i] = m_esoVals[indices[i]];
    }
  };
};

/**
* @class IntegratorMetaDataDummyForMapping
* @brief test class used by Mapping.testsuite
*/
class IntegratorMetaDataDummyForMapping:public IntegratorSingleStageMetaData
{
public:
  /**
  * @brief standard constructor
  *
  * standard constructor puts six parameters into the parameters vector
  */
  IntegratorMetaDataDummyForMapping()
  {
    for(unsigned i=0; i<6; i++){
      m_currentParamsWithSens.push_back(Parameter::Ptr(new ControlParameter()));
      m_currentParamsWithSens[i]->setParameterIndex(i);
      m_currentParamsWithSens[i]->setWithSensitivity(true);
      m_currentParamsWithSens[i]->setParameterLowerBound(4.1*(i+1));
      m_currentParamsWithSens[i]->setParameterUpperBound(7.3*(i+1));
      m_currentParamsWithSens[i]->setParameterValue(5.2*(i+1));
    }
    m_parametersWithSensitivity.assign(m_currentParamsWithSens.begin(),
                                       m_currentParamsWithSens.end());

    GenericEso::Ptr genEso(new GenericEsoDummyForMapping());
    m_genericEso = genEso;

    int  rowIndex = 0;
    int  colIndex = 0;
    double  values = 0.0;
    CsTripletMatrix::Ptr tempTriplet(new CsTripletMatrix(3,
                                                         3,
                                                         1,
                                                         &rowIndex,
                                                         &colIndex,
                                                         &values));

    m_jacobianFstates = tempTriplet->compress();

    const unsigned numDiffVars = m_genericEso->getNumDifferentialVariables();
    m_initialValues.resize(numDiffVars);
    for(unsigned i=0; i<numDiffVars; i++){
      m_initialValues[i] = InitialValueParameter::Ptr(new InitialValueParameter());
    }

    m_initialValues.front()->setEsoIndex(0);
    //skip algebraic indices (set to index 1 ff)
    const EsoIndex offset = m_genericEso->getNumAlgebraicVariables();
    for(unsigned i=1; i<m_initialValues.size(); i++){
      m_initialValues[i]->setEsoIndex(i+offset);
    }
  }

  /**
  * @brief destructor deleting attribute m_currentParamsWithSens
  */
  ~IntegratorMetaDataDummyForMapping()
  {
  }

  /**
  * @brief get the current sensitivity vector
  *
  * The output vector will simply be filled with values {0.0, 1.0, ...}.
  * @param curSens vector receiving the current sensitivities
  */
  void getCurrentSensitivities(vector<double> &curSens) const
  {
    curSens.resize(m_genericEso->getNumEquations() * getNumCurrentSensitivityParameters());
    for(unsigned i=0; i<curSens.size(); i++){
      curSens[i] = double(i);
    }
  }

  /**
  * @copydoc IntegratorMetaData::getJacobianFstates
  *
  * construct a simple state Jacobian as Diagonal matrix
  * with vector [1.0,2.0,3.0] on the diagonal
  */
  CsCscMatrix::Ptr getJacobianFstates()
  {
    const int nnz = 3;
    double vals[] = {1.0, 2.0, 3.0};
    int rows[] = {0, 1, 2};
    int cols[] = {0, 1, 2, 3};

    for(int i=0; i<nnz; i++){
      //set value vector
      m_jacobianFstates->get_matrix_ptr()->x[i] = vals[i];
      //set row vector
      m_jacobianFstates->get_matrix_ptr()->i[i] = rows[i];
      //set column vector
      m_jacobianFstates->get_matrix_ptr()->p[i] = cols[i];
    }
    // I am not sure why this is added. (faas)
    m_jacobianFstates->get_matrix_ptr()->p[nnz] = cols[nnz];
    return m_jacobianFstates;
  }

  /**
  * @copydoc IntegratorMetaData::getDuration
  *
  * returns 4.0
  */
  double getDuration() const
  {
    return 4.0;
  }
  /**
  * @brief gets copy of initials from the initials attribute of the class
  * @param[out] containts the copy of the initials
  */
  virtual void getInitials(vector<InitialValueParameter::Ptr> &initials) const
  {
    initials.assign(m_initialValues.begin(), m_initialValues.end());
  }
};


/**
* @class NonLinDerTest
* @brief test class used for the test of the nonlinear derivatives
*/
class NonLinDerTest : public IntegratorSingleStageMetaData
{
public:
  /** @brief constructor that generates an eso from a dummy class
  */
  NonLinDerTest()
  {
    m_genericEso = GenericEso::Ptr(new GenericEsoDummyForOptimizerMetaData());
    m_globalDurationParameter = DurationParameter::Ptr(new DurationParameter(1.0));
    m_gridDurations.push_back(m_globalDurationParameter);
    m_plotGrid.push_back(0.0);
    m_plotGrid.push_back(1.0);
  }
  /** @brief sets the IntOptDataMap with sensitivity values
  *
  *   This function inserts values in the IntOptDataMap, in order to check if we can retrieve them
  *   in the test for nonlinear derivatives.
  *   @param[in] contains the values that need to be set into the IntOptDataMap
  */
  void setSensitivities(const utils::Array<double> & vals){
    unsigned numParams = 1;
    IntOptDataMap::iterator timeIt;
    for(timeIt = m_intOptData[0].begin() ; timeIt != m_intOptData[0].end(); timeIt++ ){
      numParams += numParams;
      (*timeIt).second.numSensParam = numParams;
      (*timeIt).second.activeParameterIndices.resize(numParams);
      (*timeIt).second.activeDecVarIndices.resize(numParams);
      for(unsigned j=0;j<numParams;j++){
        (*timeIt).second.activeParameterIndices[j] = j;
        //first parameter is not a decision variable
        (*timeIt).second.activeDecVarIndices[j] = j-1;
      }
      const unsigned numConstraints = (*timeIt).second.nonLinConstStateIndices.size();
      const unsigned numGrads = numConstraints * numParams;
      (*timeIt).second.nonLinConstGradients.resize(numGrads, 0.0);
      for(unsigned j=0; j<numGrads; j++){
        (*timeIt).second.nonLinConstGradients[j] = vals[0]*(j+numParams+1) + numParams;
      }
    }
    m_currentParamsWithSens.resize(numParams);
    //provide actual parameters (needed in function calculateOptProbDim
    for(unsigned i=0; i<numParams; i++){
      m_currentParamsWithSens[i] = Parameter::Ptr(new ControlParameter());
      m_currentParamsWithSens[i]->setParameterIndex(i);
    }
    m_gridDurations.front()->setParameterIndex(numParams);

  }
  /**
  * @brief returns the number of decision variables
  *
  * In this case decision variables are all parameters with sensitivities but one
  * @return number of decision variables
  */
  unsigned getNumDecisionVariables() const
  {
    return m_currentParamsWithSens.size()-1;
  }

  /**
  * In this testclass the number of current sensitivity parameter is equal to the
  * total number of sensitivity parameters.
  * @return number of current sensitivity parameters
  */
  unsigned getNumSensitivityParameters() const
  {
    return m_currentParamsWithSens.size();
  }

  void setGlobalDurationParameter(const DurationParameter::Ptr &ft)
  {
    m_globalDurationParameter = ft;
  }

  void setUserGrid(const std::vector<double> &userGrid)
  {
    m_additionalGrid.assign(userGrid.begin(), userGrid.end());
  }

  void getSensitivityParameters(std::vector<Parameter::Ptr> &sensParam) const
  {
    sensParam.assign(m_currentParamsWithSens.begin(), m_currentParamsWithSens.end());
  }
  
  void setIntOptData()
  {}
};

class SetInitialTestEsoDummy : public DummyGenericEso
{
protected:
  EsoIndex m_numDiffVars;
public:
  SetInitialTestEsoDummy(EsoIndex numDiffVars)
  {
    m_numDiffVars = numDiffVars;
  }
  
  EsoIndex getNumDifferentialVariables() const
  {
    return m_numDiffVars;
  }
};

/**
* @class NonLinDerTest
* @brief test class in which initialValueParameters may be set directly
*/
class SetInititalTestClass : public IntegratorSingleStageMetaData
{
public:
  /**
  * @brief standard constructor
  */
  SetInititalTestClass()
  {
    m_genericEso = GenericEso::Ptr(new SetInitialTestEsoDummy(10));
  }

  void setInitialValueParameter(std::vector<InitialValueParameter::Ptr> &initials){
    m_initialValues.assign(initials.begin(), initials.end());
  }

};

/**
* @class OptimizerSingleStageMetaDataDummy
* @brief test class used for following testcases in the optimizerinterface.testsuite:
*        testGetNonLinConDerivative
*        testSetLagrangeMultipliers
*/
class OptimizerSingleStageMetaDataTestClass : public OptimizerSingleStageMetaData
{
public:
  /** @brief creates a dummy class with some integrator meta data
  *
  *   function also sets the eso index of the objective
  *   @param[in] the integrator meta data required for the optimizer
  */
  OptimizerSingleStageMetaDataTestClass(IntegratorSingleStageMetaData::Ptr &integratorMetaData)
  {
    assert(integratorMetaData.get() != NULL);
    m_integratorMetaData = integratorMetaData;
  }

  /**
  * @brief set the number of constraints in m_optProbDim
  *
  * @param numNonLinConstraints number of nonlinear constraints
  * @param numLinConstraints number of linear constraints
  */
  void setNumConstraints(unsigned numNonLinConstraints, unsigned numLinConstraints)
  {
    m_optProbDim.numNonLinConstraints = numNonLinConstraints;
    m_optProbDim.numLinConstraints = numLinConstraints;
    m_linConLagrangeMultiplier.resize(numLinConstraints);
  }

  /**
  * @copydoc OptimizerSingleStageMetaData::getDecisionVariableIndices
  *
  * exclude the first parameter with sensitivities  - all others should be decision variables
  */
  void getDecisionVariableIndices(std::vector<unsigned> &decVarInd) const
  {
    const unsigned numDecVars = m_integratorMetaData->getNumDecisionVariables();
    decVarInd.resize(numDecVars);
    for(unsigned i=0; i<numDecVars; i++){
      decVarInd[i] = i+1;
    }
  }
};

class PlotGridTestClass : public IntegratorSingleStageMetaData
{
public:
  typedef std::shared_ptr<PlotGridTestClass> Ptr;
protected:
  PlotGridTestClass(){}
public:
  PlotGridTestClass(GenericEso::Ptr &eso,
                    std::vector<Control> &controls,
                    std::vector<InitialValueParameter::Ptr> &initials,
                    DurationParameter::Ptr &duration,
                    std::vector<double> &userGrid)
  : IntegratorSingleStageMetaData(eso, controls, initials, duration, userGrid) {}
  
  std::vector<double> getCompleteStageGrid()
  {
    return m_completeGridUnscaled;
  }
  
  void triggerCreateGlobalGrid()
  {
    createGlobalGrid();
  }
};
