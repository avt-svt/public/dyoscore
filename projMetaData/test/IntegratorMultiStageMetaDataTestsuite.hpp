/** 
* @file IntegratorMultiStageMetaData.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData - Part of DyOS                                              \n
* =====================================================================\n
* Unittest for IntegratorMultiStageMetaData                            \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.3.2012
*/

#include "boost/test/floating_point_comparison.hpp"
#include "IntegratorMultiStageMetaData.hpp"
#include "IntegratorMultiStageMetaDataTestDummies.hpp"

#define TEST_MODEL_NAME "carErweitert"

//! threshold for double comparison testmacro BOOST_CHECK_CLOSE to pass
#define BOOST_TOL 1.0e-10

BOOST_AUTO_TEST_SUITE(MetaMultiStageEsoTest)


/**
* @test MetaMultiStageEsoTest - test constructor of class IntegratorMultiStageMetaData 
*/ 
BOOST_AUTO_TEST_CASE(TestConstruction)
{
  IntegratorMultiStageMetaData *imsmd; 
  BOOST_CHECK_NO_THROW(imsmd = createIntegratorMultiStageMetaDataTestObject(););
  delete imsmd;
}

/**
* @test MetaMultiStageEsoTest - test initialization of a new integration block 
*       using a different integration grid than the first. In case of multi stage problem
*
* checks also function getNumCurrentSensitivityParameters
* and function getMsDurationVal
* @sa createIntegratorMultiStageMetaDataTestObject for properties of the test object
*/
BOOST_AUTO_TEST_CASE(TestInitializeForFirstIntegration)
{
  TestIntegratorMultiStageMetaData *imsmd = createIntegratorMultiStageMetaDataTestObject(); 
  
  // Initialization for the first integration step - global initialization
  imsmd->initializeForFirstIntegration();
  const int firstStage = 0;
  BOOST_CHECK_EQUAL(imsmd->getCurrentStageIndex(), firstStage);
  double parameterizationTf = 0;
  const double stageTf = 1.2;
  const double relativeTf = 1.0;
  const int numStages = imsmd->getNumStages();
  BOOST_CHECK_CLOSE(imsmd->getMsDurationVal(), numStages*stageTf, BOOST_TOL);
  
  // we don't perform a test for the first stage
  // but we have to integrate over the whole first stage, so that the states have reasonable values
  while(parameterizationTf < stageTf){
    parameterizationTf += imsmd->getIntegrationGridDuration();
  // we integrate over the parameterization blocks in the first stage
    while(imsmd->getStepEndTime() != relativeTf){
      imsmd->initializeForNewIntegrationStep();
    }
    if (parameterizationTf < stageTf){
      imsmd->initializeForNextIntegration();
    }
  }

  // initialize the second stage
  imsmd->initializeForFirstIntegration();
  const int secondStage = 1;
  BOOST_CHECK_EQUAL(imsmd->getCurrentStageIndex(), secondStage);

  // one parameter from each control and one parameter for the end time
  //plus 10 parameters from the first stage
  int numSensStage1 = 3 + 10; 
  BOOST_CHECK_EQUAL(imsmd->getNumCurrentSensitivityParameters(), numSensStage1);
  const double integrationBlocktf = 0.7;
  BOOST_CHECK_CLOSE(imsmd->getIntegrationGridDuration(), integrationBlocktf, BOOST_TOL);
  const int intIntervals = 2;
  BOOST_CHECK_EQUAL(imsmd->getNumIntegrationIntervals(), intIntervals);
  const double startTime = 0.0;
  BOOST_CHECK_CLOSE(imsmd->getStepStartTime(), startTime, BOOST_TOL);

  // time interval [0 , 0.3333] of the second stage
  imsmd->initializeForNewIntegrationStep();
  BOOST_CHECK_CLOSE(startTime, imsmd->getStepStartTime(), BOOST_TOL);
  BOOST_CHECK_CLOSE(0.33333333333333326, imsmd->getStepEndTime(), BOOST_TOL);
  // no new sensitivity parameter added on the first integration step
  BOOST_CHECK_EQUAL(imsmd->getNumCurrentSensitivityParameters(), numSensStage1);

  // time interval [0.3333 , 0.6666] of the second stage
  imsmd->initializeForNewIntegrationStep();
  BOOST_CHECK_CLOSE(0.33333333333333326, imsmd->getStepStartTime(), BOOST_TOL);
  BOOST_CHECK_CLOSE(0.66666666666666652, imsmd->getStepEndTime(), BOOST_TOL);
  numSensStage1++;
  BOOST_CHECK_EQUAL(imsmd->getNumCurrentSensitivityParameters(), numSensStage1);

  // time interval [0.6666 , 0.6/0.7] of the second stage
  // 0.6 is the time vaue of the gridpoint of the first control with respect to the global grid.
  // 0.7 is duration on the first grid of the second control. Since all gridpoints on this first
  // grid are scaled, the gridpoint 0.6 has to be scaled with the duration 0.7 as well.
  imsmd->initializeForNewIntegrationStep();
  BOOST_CHECK_CLOSE(0.66666666666666652, imsmd->getStepStartTime(), BOOST_TOL);
  const double endp1stCon1stPara = 0.6 / 0.7; // end point of first control and first parameter
  BOOST_CHECK_CLOSE(endp1stCon1stPara, imsmd->getStepEndTime(), BOOST_TOL);
  numSensStage1++;
  BOOST_CHECK_EQUAL(imsmd->getNumCurrentSensitivityParameters(), numSensStage1);

  // time interval [0.6/0.7 , 1.0] of the second stage
  // 1.0 is the scaled endpoint of the first grid of the second control (0.7/0.7)
  imsmd->initializeForNewIntegrationStep();
  BOOST_CHECK_CLOSE(endp1stCon1stPara,  imsmd->getStepStartTime(), BOOST_TOL);
  BOOST_CHECK_CLOSE(1.0, imsmd->getStepEndTime(), BOOST_TOL);
  numSensStage1++;
  BOOST_CHECK_EQUAL(imsmd->getNumCurrentSensitivityParameters(), numSensStage1);
  
  //initialize third stage (last stage)
  imsmd->initializeForFirstIntegration();
  const unsigned thirdStageIndex = 2;
  BOOST_CHECK_EQUAL(thirdStageIndex, imsmd->getCurrentStageIndex());
  
  //test if the initialize function resets the stage index 
  //after a complete iteration through all stages
  //initialize the first stage again
  imsmd->initializeForFirstIntegration();
  const unsigned firstStageIndex = 0;
  BOOST_CHECK_EQUAL(firstStageIndex, imsmd->getCurrentStageIndex());

  delete imsmd;
}


/**
* @test MetaMultistageEsoTest - check function calculateSensitivitiesForward1stOrder
*
* simply checks whether sensitivities are build together correctly assuming that used 
* functions of classes Mapping and IntegratorSingleStageEso work correctly
* For that purpose Mapping and SingleStage dummies are used that provide constant data.
* This function checks whether that data is combined correctly.
* The multipliers are used to identify the object (stage or mapping) that produced the output
*/
BOOST_AUTO_TEST_CASE(TestCalculateSensitivitiesForward1stOrderWithFullMapping)
{
  
  MultiStageDummyGenericEso::Ptr esoPtr(new MultiStageDummyGenericEso());
  //set 4 equations for all stages
  const unsigned numEquations = 4;
  esoPtr->m_numEquations = numEquations;
  GenericEso::Ptr eso(esoPtr);
  
  Mapping::Ptr mapping(new TestDummyMapping());
  std::vector<Mapping::Ptr> mappingVec(1);
  mappingVec[0] = mapping; // we have only two stages
  
  
  const unsigned numParametersStage1 = 5;
  const double multiplierStage1 = 2.0; // this is not a lagrange multiplier, but a multiplier for testing
  IntegratorSingleStageMetaDataDummyForMultiStageTest *stage1Ptr, *stage2Ptr;
  stage1Ptr = new IntegratorSingleStageMetaDataDummyForMultiStageTest(eso);
  stage1Ptr->m_testNumSensitivityParameters = numParametersStage1;
  stage1Ptr->m_testMultiplier = multiplierStage1;
  
  const unsigned numParametersStage2 = 8;
  const double multiplierStage2 = 4.0;
  const double durationStage2 = 3.0; //duration is used by mapping to calculate rhsSens
  stage2Ptr = new IntegratorSingleStageMetaDataDummyForMultiStageTest(eso);
  stage2Ptr->m_testNumSensitivityParameters = numParametersStage2;
  stage2Ptr->m_testMultiplier = multiplierStage2;
  stage2Ptr->m_testDuration = durationStage2;

  IntegratorSingleStageMetaData::Ptr stage1(stage1Ptr);
  IntegratorSingleStageMetaData::Ptr stage2(stage2Ptr);
  
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(2);
  stages[0] = stage1;
  stages[1] = stage2;
  
  TestIntegratorMultiStageMetaData imsMetaData(mappingVec, stages);

  utils::Array<double> sensInputStage1(numParametersStage1, 1.0); // test input vector
  utils::Array<double> sensRhsStage1(numParametersStage1); // test output vector
  // expected output vector = multiplier * test input vector
  utils::Array<double> expectedOutputStage1(numParametersStage1, multiplierStage1); 
  
  imsMetaData.setCurrentStageIndex(0);
  imsMetaData.calculateSensitivitiesForward1stOrder(sensInputStage1, sensRhsStage1);
  
  //in the first stage the function should simply call the function on the first stage only
  //i.e. multiply the test input vector with the  multiplier of the first stage
  for(unsigned i=0; i<numParametersStage1; i++){
    BOOST_CHECK_CLOSE(sensRhsStage1[i], expectedOutputStage1[i], THRESHOLD);
  }

  //test a full mapping
  const unsigned numMappingSens = numParametersStage1 * numEquations;
  const unsigned numSensStage2 = numMappingSens + numParametersStage2*numEquations;
  utils::Array<double> sensInputStage2(numSensStage2, 1.0);
  utils::Array<double> sensRhsStage2(numSensStage2);
  utils::Array<double> expectedOutputStage2(numSensStage2);
  
  for(unsigned i=0; i<numMappingSens; i++){
    // multiplication with jacobianFstates results in multiplier
    // and afterwards it is scaled with duration
    expectedOutputStage2[i] = multiplierStage2 * durationStage2;
  }
  for(unsigned i=numMappingSens; i<numSensStage2; i++){
    expectedOutputStage2[i] = multiplierStage2;
  }
  

  imsMetaData.setCurrentStageIndex(1);
  imsMetaData.resizeMappingSensVector(numMappingSens);
  
  
  imsMetaData.calculateSensitivitiesForward1stOrder(sensInputStage2, sensRhsStage2);

  for(unsigned i=0; i<numSensStage2; i++){
    BOOST_CHECK_CLOSE(sensRhsStage2[i], expectedOutputStage2[i], THRESHOLD);
  }
}

/// @brief typedef for ForwardingTestDummy class (just for convenience)
typedef IntegratorSSMetaDataForwardingTestDummy ForwardDummy;

/**
* @brief checks whether the right function on the right stage was called
*
* All other stages should have the state NoFunctionCalled
* @param stages ForwardDummy objects tested in Multi stage forward test
* @param stageIndex index of the stage on which the function is expected to be called
* @param type of the function that has been called
* @return true, if all stages have the correct flag
*/
bool checkAndResetType(std::vector<ForwardDummy*> stages, 
               unsigned stageIndex,
               ForwardDummy::CalledFunction calledFunction)
{
  bool isOK = true;
  isOK = stages[stageIndex]->m_calledFunction == calledFunction;
  
  stages[stageIndex]->m_calledFunction = ForwardDummy::NoFunctionCalled;
  
  for(unsigned i=0; i<stages.size(); i++){
    isOK = isOK && (stages[i]->m_calledFunction == ForwardDummy::NoFunctionCalled);
  }
  return isOK;
}

/**
* @test MetaMultiStageEsoTest - check the functions of the IntegratorMultiStageMetaData which
                              simply forwards the information to the respective single stage
*
* To do so a dummy single stage class is used that stores whether a function has been visited or not.
* 
*/
BOOST_AUTO_TEST_CASE(TestForwardingFunctions)
{
  std::vector<Mapping::Ptr> mappingVec;
  
  const unsigned numStages = 3;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  std::vector<ForwardDummy*> dummyStages(numStages);

  
  for(unsigned i=0; i<numStages; i++){
    dummyStages[i] = new ForwardDummy();
    dummyStages[i]->m_calledFunction = ForwardDummy::NoFunctionCalled;
    dummyStages[i]->setSignature(i);
    IntegratorSingleStageMetaData::Ptr stage (dummyStages[i]);
    stages[i] = stage;
  }
  
  TestIntegratorMultiStageMetaData imsMetaData(mappingVec, stages);
  

  for(unsigned i=0; i<numStages; i++){
    imsMetaData.setCurrentStageIndex(i);
    
    //check non constant functions
    
    imsMetaData.setStepStartTime(0.0);
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::SetStepStartTime));
    
    imsMetaData.setStepEndTime(0.0);
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::SetStepEndTime));
    
    imsMetaData.setStepCurrentTime(0.0);
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::SetStepCurrentTime));
    
    utils::Array<double> dummy(0);
    imsMetaData.calculateRhsStates(dummy,dummy);
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::CalculateRhsStates));
    
    imsMetaData.setStates(dummy);
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::SetStates));
    
    imsMetaData.initializeForNewIntegrationStep();
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::InitializeForNewIntegrationStep));
    
    imsMetaData.initializeForNextIntegration();
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::InitializeForNextIntegration));
    
    imsMetaData.setAllInitialValuesFree();
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::SetAllInitialValuesFree));
    
    imsMetaData.getGenericEso();
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::GetGenericEso));
    
    imsMetaData.getNumStateNonZeroes();
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::GetNumStateNonZeroes));
    
    //imsMetaData.getGlobalDurationParameter();
    //BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::GetGlobalDurationParameter));
    
    imsMetaData.getJacobianFup();
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::GetJacobianFup));
    
    imsMetaData.getJacobianFstates();
    BOOST_CHECK(checkAndResetType(dummyStages, i, ForwardDummy::GetJacobianFstates));
    
    //check constant functions
    //return values are related with the stage signature which is the stage index
    
    BOOST_CHECK_CLOSE(imsMetaData.getStepStartTime(), double(i), THRESHOLD);
    
    BOOST_CHECK_CLOSE(imsMetaData.getStepEndTime(), double(i), THRESHOLD);
    
    BOOST_CHECK_CLOSE(imsMetaData.getStepCurrentTime(), double(i), THRESHOLD);
    
    BOOST_CHECK_CLOSE(imsMetaData.getIntegrationGridDuration(), double(i), THRESHOLD);
    
    CsCscMatrix::Ptr ret = imsMetaData.getMMatrix();
    BOOST_CHECK_EQUAL(ret->get_matrix_ptr()->nz, i);
    
    BOOST_CHECK_EQUAL(imsMetaData.getNumIntegrationIntervals(), i);
  }
}


/////////////////////////////////////////////////////////////////////////////////////////
/** @test MetaMultiStageTest - checks the behavior of getSensitivities and setSensitivities in the multi
*                              stage case
*/
BOOST_AUTO_TEST_CASE(TestSetAndGetSensitivities)
{

  // we need to create a function here!
  MultiStageDummyGenericEso::Ptr esoPtr (new MultiStageDummyGenericEso());
  //set 4 equations for all stages
  const unsigned numEquations = 4;
  esoPtr->m_numEquations = numEquations;
  GenericEso::Ptr eso(esoPtr);
  
  Mapping::Ptr mapping(new TestDummyMapping());
  std::vector<Mapping::Ptr> mappingVec(1);
  mappingVec[0] = mapping;
  
  
  const unsigned numParametersStage1 = 5;
  IntegratorSingleStageMetaDataDummyForMultiStageTest *stage1Ptr, *stage2Ptr;
  stage1Ptr = new IntegratorSingleStageMetaDataDummyForMultiStageTest(eso);
  stage1Ptr->m_testNumSensitivityParameters = numParametersStage1;
  for(unsigned i=0;i<numParametersStage1;i++){
    stage1Ptr->addParamWithSens(Parameter::Ptr(new InitialValueParameter()));
  }
  stage1Ptr->setStepStartTime(0.0);
  stage1Ptr->setStepEndTime(1.0);

  
  const unsigned numParametersStage2 = 8;
  stage2Ptr = new IntegratorSingleStageMetaDataDummyForMultiStageTest(eso);
  stage2Ptr->m_testNumSensitivityParameters = numParametersStage2;
  for(unsigned i=0;i<numParametersStage2;i++){
    stage2Ptr->addParamWithSens(Parameter::Ptr(new InitialValueParameter()));
  }
  stage2Ptr->setStepStartTime(0.0);
  stage2Ptr->setStepEndTime(1.0);

  IntegratorSingleStageMetaData::Ptr stage1(stage1Ptr);
  IntegratorSingleStageMetaData::Ptr stage2(stage2Ptr);
  
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(2);
  stages[0] = stage1;
  stages[1] = stage2;
  ///////////////////////////////////////

  TestIntegratorMultiStageMetaData imsmd(mappingVec, stages);

  const int numSens1 = numParametersStage1*numEquations;
  utils::Array<double> sens1(numSens1);
  for(int i=0;i<numSens1;i++){
    sens1[i] = 4.45*(i+1)+(i*i);
  }
  imsmd.setStepCurrentTime(1.0);
  imsmd.setSensitivities(sens1);
  const int numCurrSens1 = imsmd.getNumCurrentSensitivityParameters();
  BOOST_CHECK_EQUAL(numCurrSens1,numParametersStage1);

  std::vector<double> sensCheckMS0;
  // we will only get the sensitivity information of the multi stage problem.
  imsmd.getCurrentSensitivities(sensCheckMS0);
  BOOST_CHECK_EQUAL(sensCheckMS0.size(),unsigned(0));

  // we will get the sensitivitiy information of the local single stage
  std::vector<double> sensCheckSS0;
  stage1->getCurrentSensitivities(sensCheckSS0);
  BOOST_CHECK_EQUAL(sensCheckSS0.size(), unsigned(numSens1));
  for(int i=0; i<numSens1; i++){
    BOOST_CHECK_CLOSE(sensCheckSS0[i], sens1[i], THRESHOLD);
  }

  // go to the next stage
  const int nextStage = 1;
  imsmd.setCurrentStageIndex(nextStage);
  const int numCurrSens2 = imsmd.getNumCurrentSensitivityParameters();
  const int numSens2 = numSens1 + numParametersStage2*numEquations;
  BOOST_CHECK_EQUAL(numCurrSens2, numParametersStage2 + numParametersStage1);
  utils::Array<double> sens2(numSens2);
  for(int i=0; i<numSens2; i++){
    sens2[i] = 3.28*(i+1) + (i*i);
  }
  imsmd.setStepCurrentTime(1.0);
  imsmd.setSensitivities(sens2);

  std::vector<double> sensCheckMS1;
  // we will only get the sensitivity information of the multi stage problem.
  imsmd.getCurrentSensitivities(sensCheckMS1);
  BOOST_CHECK_EQUAL(sensCheckMS1.size(), unsigned(numSens1));
  for(int i=0; i<numSens1; i++){
    BOOST_CHECK_CLOSE(sensCheckMS1[i], sens2[i], THRESHOLD);
  }

  // we now obtain the single stage information
  std::vector<double> sensCheckSS1;
  stage2->getCurrentSensitivities(sensCheckSS1);
  BOOST_CHECK_EQUAL(sensCheckSS1.size(), unsigned(numParametersStage2 * numEquations));
  for(unsigned i=0; i<numParametersStage2*numEquations; i++){
    BOOST_CHECK_CLOSE(sensCheckSS1[i], sens2[i+numSens1], THRESHOLD);
  }

}

/**
* @test MetaMultiStageEsoTest - check function addIntOptDataPoint
*
* In this test a multistage problem with three stages is created and the function is
* tested on the first stage (no information of previous stages) and the third stage
* (information of more than one previous stage is used). The function is tested 
* for two different timepoints on the third stage.
* @note This test implicitly also tests the function getConstraintSensitivities.
*/
BOOST_AUTO_TEST_CASE(TestAddIntOptDataPoint)
{
  //set up test data
  const unsigned numStages = 3;
  std::vector<Mapping::Ptr> mappings;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  std::vector<IntegratorSingleStageMetaDataDummyForMultiStageTest*> stagesPointer(numStages);
  
  GenericEso::Ptr eso(new MultiStageDummyGenericEso());
  const double startPoint = 0.0;
  const double midPoint = 0.5;
  const double endPoint = 1.0;
  const unsigned midPointIndex = 1;
  const unsigned endPointIndex = 2;
  std::vector<double> grid;
  grid.push_back(startPoint);
  grid.push_back(midPoint);
  grid.push_back(endPoint);
  for(unsigned i=0; i<numStages; i++){
    stagesPointer[i] = new IntegratorSingleStageMetaDataDummyForMultiStageTest(eso);
    stagesPointer[i]->setIntegrationGrid(grid);
    stagesPointer[i]->m_testDuration = 1.0;
    IntegratorSingleStageMetaData::Ptr stage(stagesPointer[i]);
    stages[i] = stage;
    
  }
  
  
  TestIntegratorMultiStageMetaData imsMetaData(mappings, stages);
  
  //build up sensitivity data for the endpoints
  SavedOptDataStruct endDataPoint;
  
  //stage1
  const unsigned numParamsStage1 = 4;
  endDataPoint.numSensParam = numParamsStage1;
  stagesPointer[0]->m_testNumSensitivityParameters = numParamsStage1;
  endDataPoint.activeParameterIndices.resize(numParamsStage1);
  for(unsigned i=0; i<numParamsStage1; i++){
    endDataPoint.activeParameterIndices[i] = i;
  }
  //define a constraint for the first stage
  endDataPoint.nonLinConstIndices.push_back(0);
  endDataPoint.nonLinConstVarIndices.push_back(0);
  endDataPoint.timePoint = 1.0;
  
  IntOptDataMap mapStage1;
  mapStage1[endPointIndex] = endDataPoint;
  stagesPointer[0]->setIntOptDataMap(mapStage1);
  
  //stage2
  const unsigned numParamsStage2 = 6;
  endDataPoint.numSensParam = numParamsStage2;
  endDataPoint.activeParameterIndices.resize(numParamsStage2);
  stagesPointer[1]->m_testNumSensitivityParameters = numParamsStage2;
  for(unsigned i=0; i<numParamsStage2; i++){
    endDataPoint.activeParameterIndices[i] = i;
  }
  IntOptDataMap mapStage2;
  mapStage2[endPointIndex] = endDataPoint;
  stagesPointer[1]->setIntOptDataMap(mapStage2);
  

  //build up constraints for the third stage
  BOOST_REQUIRE_EQUAL(midPointIndex, stages[2]->addNonlinearConstraint(0, 0, midPoint, 0.0));
  BOOST_REQUIRE_EQUAL(midPointIndex, stages[2]->addNonlinearConstraint(2, 1, midPoint, 0.0));
  BOOST_REQUIRE_EQUAL(endPointIndex, stages[2]->addNonlinearConstraint(0, 2, endPoint, 0.0));
  BOOST_REQUIRE_EQUAL(endPointIndex, stages[2]->addNonlinearConstraint(1, 3, endPoint, 0.0));
  BOOST_REQUIRE_EQUAL(endPointIndex, stages[2]->addNonlinearConstraint(3, 4, endPoint, 0.0));
  BOOST_REQUIRE_EQUAL(endPointIndex, stages[2]->addNonlinearConstraint(2, 5, endPoint, 0.0));
  
  
  //add int opt data for the first stage
  //should do nothing
  imsMetaData.setCurrentStageIndex(0);
  stagesPointer[0]->m_testCurrentTime = endPoint;
  stagesPointer[0]->m_testTimeIndex = endPointIndex;
  imsMetaData.testAddIntOptDataPoint();
  
  //add int opt data for the third stage at time point 0.5
  imsMetaData.setCurrentStageIndex(2);
  
  // set mapping sensitivity vector for third stage
  // the vector is resized during call to setCurrentStageIndex, 
  // so the mappingSensVector has to be set afterwards.
  const unsigned numEquationsStage3 = stages[2]->getGenericEso()->getNumEquations();
  const unsigned numMappingSens((numParamsStage1 + numParamsStage2) * numEquationsStage3);
  std::vector<double> mappingSensVector(numMappingSens);
  unsigned mappingSensCounter = 0;
  // store sensitivity coding equation and parameter such that 
  // eq sensitivity 13.0 means sensitivity for second row and third parameter
  // matrix is stored columnwise in the vector
  for(unsigned i=0; i< numParamsStage1+numParamsStage2; i++){
    for(unsigned j=0; j<numEquationsStage3; j++){
      mappingSensVector[mappingSensCounter] = 10*j +i;
      mappingSensCounter++;
    }
  }
  imsMetaData.setMappingSensVector(mappingSensVector);
  
  stagesPointer[2]->m_testCurrentTime = midPoint;
  stagesPointer[2]->m_testTimeIndex = midPointIndex;
  imsMetaData.testAddIntOptDataPoint();
  stagesPointer[2]->m_testTimeIndex = endPointIndex;
  stagesPointer[2]->m_testCurrentTime = endPoint;
  imsMetaData.testAddIntOptDataPoint();
  
  
  //check function
  std::vector<IntOptDataMap> multiStageIODMaps = imsMetaData.getIntOptData();
  
  /////////////////////////////check first stage///////////////////////////////
  
  BOOST_REQUIRE(multiStageIODMaps[0].count(endPointIndex) > 0);
  BOOST_CHECK_EQUAL(multiStageIODMaps[0][endPointIndex].numSensParam, 0);
  BOOST_CHECK(multiStageIODMaps[0][endPointIndex].activeParameterIndices.empty());
  // no values, since no values have been stored on single stage object in this test
  BOOST_CHECK_EQUAL(multiStageIODMaps[0][endPointIndex].nonLinConstValues.getSize(), 0);
  
  // constraint with esoIndex 0 and constraint index 0 has been set
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[0][endPointIndex].nonLinConstVarIndices.size(),1);
  BOOST_CHECK_EQUAL(multiStageIODMaps[0][endPointIndex].nonLinConstVarIndices[0], 0);
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[0][endPointIndex].nonLinConstIndices.size(), 1);
  BOOST_CHECK_EQUAL(multiStageIODMaps[0][endPointIndex].nonLinConstIndices[0], 0);
  
  // gradients are empty, since no mapping sensitivities exist in first stage
  BOOST_CHECK_EQUAL(multiStageIODMaps[0][endPointIndex].nonLinConstGradients.getSize() , 0);
  
  ////////////////////////////check third stage///////////////////////////////
  
  BOOST_REQUIRE(multiStageIODMaps[2].count(midPointIndex) > 0);
  const unsigned numTotalParams = numParamsStage1 + numParamsStage2;
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][midPointIndex].numSensParam, numTotalParams);
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][midPointIndex].activeParameterIndices.size(), numTotalParams);
  for(unsigned i=0; i<numTotalParams; i++){
    BOOST_CHECK_EQUAL(multiStageIODMaps[2][midPointIndex].activeParameterIndices[i] , i);
  }
  
  const unsigned numMidpointConstraints = 2;
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstValues.getSize(), numMidpointConstraints);
  // values are not checked, since values have never been set in this test
  
  // 2 constraints have been set:
  // first constraint - esoIndex 0, constraint index 0
  // second constraint - esoIndex 2, constraint index 1
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstVarIndices.size(), numMidpointConstraints);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstVarIndices[0], 0);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstVarIndices[1], 2);
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstIndices.size(), numMidpointConstraints);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstIndices[0], 0);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstIndices[1], 1);
  
  // check constraint gradients
  // The first and third line should be extracted from the mapping sensitivity matrix
  // and they are stored rowwise (first all sensitivities of the first constraint,
  // then of the second etc)
  {
    const unsigned numStateIndices = multiStageIODMaps[2][midPointIndex].nonLinConstStateIndices.size();
    const unsigned numGradients = numTotalParams * numStateIndices;
    BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][midPointIndex].nonLinConstGradients.getSize(), numGradients);
    unsigned gradientsIndex = 0;
    
    for(unsigned i=0; i<numStateIndices; i++){
      for(unsigned j=0; j< numTotalParams; j++){
        double gradient = multiStageIODMaps[2][midPointIndex].nonLinConstGradients[gradientsIndex];
        const unsigned stateIndex = multiStageIODMaps[2][midPointIndex].nonLinConstStateIndices[i];
        const unsigned parameterIndex = j;
        BOOST_CHECK_CLOSE(gradient, double(10*stateIndex + parameterIndex), THRESHOLD);
        gradientsIndex++;
      }
    }
  }
  
  BOOST_REQUIRE(multiStageIODMaps[2].count(endPointIndex) > 0);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].numSensParam, numTotalParams);
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][endPointIndex].activeParameterIndices.size(), numTotalParams);
  for(unsigned i=0; i<numTotalParams; i++){
    BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].activeParameterIndices[i] , i);
  }
  
  const unsigned numEndpointConstraints = 4;
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstValues.getSize(), numEndpointConstraints);
  // values are not checked, since values have never been set in this test
  
  // 2 constraints have been set:
  // first constraint - esoIndex 0, constraint index 2
  // second constraint - esoIndex 1, constraint index 3
  // third constraint - esoIndex 3, constraint index 4
  // fourth constraint - esoIndex 2, constraint index 5
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstVarIndices.size(),numEndpointConstraints);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstVarIndices[0], 0);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstVarIndices[1], 1);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstVarIndices[2], 3);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstVarIndices[3], 2);
  BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstIndices.size(), numEndpointConstraints);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstIndices[0], 2);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstIndices[1], 3);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstIndices[2], 4);
  BOOST_CHECK_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstIndices[3], 5);
  
  // check constraint gradients
  // The first and third line should be extracted from the mapping sensitivity matrix
  // and they are stored rowwise (first all sensitivities of the first constraint,
  // then of the second etc)
  {
    const unsigned numStateIndices = multiStageIODMaps[2][endPointIndex].nonLinConstStateIndices.size();
    const unsigned numGradients = numTotalParams * numStateIndices;
    BOOST_REQUIRE_EQUAL(multiStageIODMaps[2][endPointIndex].nonLinConstGradients.getSize(), numGradients);
    unsigned gradientsIndex = 0;
    
    for(unsigned i=0; i<numStateIndices; i++){
      for(unsigned j=0; j< numTotalParams; j++){
        double gradient = multiStageIODMaps[2][endPointIndex].nonLinConstGradients[gradientsIndex];
        const unsigned stateIndex = multiStageIODMaps[2][endPointIndex].nonLinConstStateIndices[i];
        const unsigned parameterIndex = j;
        BOOST_CHECK_CLOSE(gradient, double(10*stateIndex + parameterIndex), THRESHOLD);
        gradientsIndex++;
      }
    }
  }
  
}

/**
* @test MetaMultiStageEsoTest - check function getNumStages
*/
BOOST_AUTO_TEST_CASE(TestGetNumStages)
{
  const unsigned numStages = 5;
  std::vector<Mapping::Ptr> mappings;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  
  IntegratorMultiStageMetaData imsMetaData(mappings, stages);
  
  BOOST_CHECK_EQUAL(imsMetaData.getNumStages(),numStages);
}

/**
* @test MetaMultiStageEsoTest - check function evaluateInitialValues
*/
BOOST_AUTO_TEST_CASE(TestEvaluateInitialValues)
{
  // set up test data
  const unsigned numStages = 2;
  const unsigned numTestSens = 1;
  const unsigned numTestStates = 1;
  std::vector<Mapping::Ptr> mappings;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  for(unsigned i=0; i<numStages; i++){
    MultiStageDummyGenericEso::Ptr genEsoPtr (new MultiStageDummyGenericEso());
    genEsoPtr->m_numEquations = 1;
    GenericEso::Ptr eso(genEsoPtr);
    IntegratorSingleStageMetaDataDummyForMultiStageTest *stagePtr;
    stagePtr = new IntegratorSingleStageMetaDataDummyForMultiStageTest(eso);
    stagePtr->m_testMultiplier = double(i+1);
    stagePtr->m_testNumSensitivityParameters = numTestSens;
    IntegratorSingleStageMetaData::Ptr stage(stagePtr);
    stages[i] = stage;
  }
  
  TestIntegratorMultiStageMetaData imsMetaData(mappings, stages);
  
  // check function
  imsMetaData.setCurrentStageIndex(0);
  const double stageMultiplier1 = 1.0; // identifier for data produced in the first stage
  // at the first stage the mappingSens vector is empty, so the initials should be
  // evaluated completely by the first stage (dummy function inserting 1.0 in all entries)
  utils::Array<double> initialStatesStage1(numTestStates), initialSensitivitiesStage1(numTestSens);
  imsMetaData.evaluateInitialValues(initialStatesStage1, initialSensitivitiesStage1);
  BOOST_CHECK_CLOSE(initialStatesStage1[0], stageMultiplier1, THRESHOLD);
  BOOST_CHECK_CLOSE(initialSensitivitiesStage1[0], stageMultiplier1, THRESHOLD);
  
  imsMetaData.setCurrentStageIndex(1);
  const double stageMultiplier2 = 2.0; // identifier for data produced in the second stage
  // set a custom mapping sens vector
  const unsigned numMappingSens = 2;
  const double mappingMultiplier = 10.0; // identifier for data produced by mapping
  std::vector<double> mappingSens(numMappingSens, mappingMultiplier);
  imsMetaData.setMappingSensVector(mappingSens);
  utils::Array<double> initialStatesStage2(numTestStates);
  utils::Array<double> initialSensitivitiesStage2(numTestSens + numMappingSens);
  imsMetaData.evaluateInitialValues(initialStatesStage2, initialSensitivitiesStage2);
  BOOST_CHECK_CLOSE(initialStatesStage2[0], stageMultiplier2, THRESHOLD);
  // first sens values are mappings sensitivities
  // the last sens value is evaluated by the stage
  for(unsigned i=0; i<numMappingSens; i++){
    BOOST_CHECK_CLOSE(initialSensitivitiesStage2[i], mappingMultiplier, THRESHOLD);
  }
  BOOST_CHECK_CLOSE(initialSensitivitiesStage2[numMappingSens], stageMultiplier2, THRESHOLD);
  
}

/**
* @test MetaMultiStageEsoTest - check function getIntOptDataStages
*/
BOOST_AUTO_TEST_CASE(testGetIntOptDataStages)
{
  //  set up test data
  const unsigned numStages = 3;
  std::vector<Mapping::Ptr> mappings;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  for(unsigned i=0; i<numStages; i++){
    GenericEso::Ptr eso(new MultiStageDummyGenericEso());
    IntegratorSingleStageMetaDataDummyForMultiStageTest::Ptr stagePtr(new IntegratorSingleStageMetaDataDummyForMultiStageTest(eso));
    IntOptDataMap map;
    SavedOptDataStruct dummy;
    //time point used to identify map of the stage;
    map[i] = dummy;
    stagePtr->setIntOptDataMap(map);
    IntegratorSingleStageMetaData::Ptr stage(stagePtr);
    stages[i] = stage;
  }
  
  IntegratorMultiStageMetaData imsMetaData(mappings, stages);
  
  //check function
  std::vector<IntOptDataMap> maps = imsMetaData.getIntOptDataStages();
  BOOST_CHECK_EQUAL(maps.size(), numStages);
  // check if every stage has a map with one entry and the right timepoint
  // as has been set in each stage (see loop above) 
  for(unsigned i=0; i<numStages; i++){
    BOOST_CHECK_EQUAL(maps[i].size(), 1);
    BOOST_CHECK(maps[i].count(i) > 0);
  }
}


/**
* @test MetaMultiStageEsoTest - check function getMsDurationData
*/
BOOST_AUTO_TEST_CASE(testgetMsDurationData)
{
  //  set up test data
  const unsigned numStages = 3;
  std::vector<Mapping::Ptr> mappings;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  for(unsigned i=0; i<numStages; i++){
    IntegratorSingleStageMetaDataDummyForMultiStageTestWithDuration *stagePtr;
    DurationParameter::Ptr globalDurationParameter(new DurationParameter());
    globalDurationParameter->setParameterValue(double(i));
    stagePtr = new IntegratorSingleStageMetaDataDummyForMultiStageTestWithDuration(globalDurationParameter);

    IntegratorSingleStageMetaData::Ptr stage(stagePtr);
    stages[i] = stage;
  }
  
  IntegratorMultiStageMetaData imsMetaData(mappings, stages);
  
  //check function
  const double sumDurations = imsMetaData.getMsDurationVal();
  BOOST_CHECK_CLOSE(sumDurations, 3.0, THRESHOLD);
  
}

/**
* @test MetaMultiStageEsoTest - test functions getAdjoints, getLagrangeDers
                                         and setAdjointsAndLagrangeDers
*
* This test checks the overloaded functions concerning adjoints and lagrangeDers
* The test problem has three stages each with one control and 5,7, and 9 gridpoints
* The first stage has initial value parameters and all stages have global duration
* parameters with sensitivity
*/
BOOST_AUTO_TEST_CASE(TestSetAndGetLagrangeDersAndAdjointsMultiStage)
{
  const unsigned numStages = 3;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(numStages);
  std::vector<double> additionalGrid;
  GenericEsoFactory genEsoFac;

  ////////////////////
  // set up stage 1 //
  ////////////////////
  {
    // set up global final time
    DurationParameter::Ptr duration(new DurationParameter(1.0));
    duration->setWithSensitivity(true);
    
    std::vector<Control> controls(1);
    ParameterizationGrid::Ptr grid(new ParameterizationGrid(5, PieceWiseConstant));
    grid->setDurationPointer(duration);
    std::vector<ParameterizationGrid::Ptr> gridV1;
    gridV1.push_back(grid);
    controls[0].setControlGrid(gridV1);    
    
    //set up 3 initials with sensitivity information
    const unsigned numInitials = 3;
    std::vector<InitialValueParameter::Ptr> initials(numInitials);
    for(unsigned i=0; i<numInitials; i++){
      initials[i] = InitialValueParameter::Ptr(new InitialValueParameter());
      initials[i]->setWithSensitivity(true);
      initials[i]->setEsoIndex(i);
      initials[i]->setEquationIndex(i);
    }
    
  
    
    GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    stages[0] = IntegratorSingleStageMetaData::Ptr(new IntegratorSingleStageMetaData(genEso,
                                                                                     controls,
                                                                                     initials,
                                                                                     duration,
                                                                                     additionalGrid));
  }
  
  ////////////////////
  // set up stage 2 //
  ////////////////////
  {// set up global final time
    DurationParameter::Ptr duration(new DurationParameter(1.0));
    duration->setWithSensitivity(true);    
    
    std::vector<Control> controls(1);
    ParameterizationGrid::Ptr grid(new ParameterizationGrid(7, PieceWiseConstant));
    grid->setDurationPointer(duration);
    std::vector<ParameterizationGrid::Ptr> gridV1;
    gridV1.push_back(grid);
    controls[0].setControlGrid(gridV1);    
    
    std::vector<InitialValueParameter::Ptr> initials;
    
    
    
    GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    stages[1] = IntegratorSingleStageMetaData::Ptr(new IntegratorSingleStageMetaData(genEso,
                                                                                     controls,
                                                                                     initials,
                                                                                     duration,
                                                                                     additionalGrid));
  }
  
  ////////////////////
  // set up stage 3 //
  ////////////////////
  {
    // set up global final time
    DurationParameter::Ptr duration(new DurationParameter(1.0));
    duration->setWithSensitivity(true);
    
    std::vector<Control> controls(1);
    ParameterizationGrid::Ptr grid(new ParameterizationGrid(9, PieceWiseConstant));
    grid->setDurationPointer(duration);
    std::vector<ParameterizationGrid::Ptr> gridV1;
    gridV1.push_back(grid);
    controls[0].setControlGrid(gridV1);    
    
    std::vector<InitialValueParameter::Ptr> initials;
    

    
    GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    stages[2] = IntegratorSingleStageMetaData::Ptr(new IntegratorSingleStageMetaData(genEso,
                                                                                     controls,
                                                                                     initials,
                                                                                     duration,
                                                                                     additionalGrid));
  }
  
  //apply full state mapping for all stages
  std::vector<Mapping::Ptr> mappings;
  
  TestIntegratorMultiStageMetaData imsmd(mappings, stages);
  
  imsmd.initializeAtStageDuration();
  
  //check initialized lagrangeDers
  const unsigned numAdjoints = imsmd.getGenericEso()->getNumStates();
  const unsigned numLagrangeDers = imsmd.getNumSensitivityParameters();
  
   // since lagrangeDers are simply added collect the current lagrangeDers first
  // usually these should be initialized with 0.0;
  std::vector<double> lagrangeDersStart;
  imsmd.getLagrangeDers(lagrangeDersStart);
  BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersStart.size());
  for(unsigned i=0; i<lagrangeDersStart.size(); i++){
    BOOST_CHECK_CLOSE(lagrangeDersStart[i], 0.0, THRESHOLD);
  }
  std::vector<double> adjointsStart;
  imsmd.getAdjoints(adjointsStart);
  BOOST_CHECK_EQUAL(numAdjoints, adjointsStart.size());
  
  std::vector<double> lagrangeDersOut = lagrangeDersStart;
  //check set and get functions for each stage
  for(unsigned i=0; i<numStages; i++){
    imsmd.setCurrentStageIndex(i);
    
     utils::Array<double> adjoints(numAdjoints);
    for(unsigned j=0; j<numAdjoints; j++){
      adjoints[j] = 1.1*j + 0.01*i;
    }
    utils::Array<double> lagrangeDerivatives(numLagrangeDers);
    std::vector<double> lagrangeDersCompare = lagrangeDersOut;
    for(unsigned j=0; j<numLagrangeDers; j++){
      lagrangeDerivatives[j] = 0.02*i + 0.005 + 0.2*j;
      lagrangeDersCompare[j] += lagrangeDerivatives[j];
    }
    imsmd.setAdjointsAndLagrangeDerivatives(adjoints, lagrangeDerivatives);
    

    
    imsmd.getLagrangeDers(lagrangeDersOut);
    BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersOut.size());
    for(unsigned j=0; j<numLagrangeDers; j++){
      BOOST_CHECK_CLOSE(lagrangeDersOut[j], lagrangeDersCompare[j], THRESHOLD);
    }
    
    std::vector<double> adjointsOut;
    imsmd.getAdjoints(adjointsOut);
    BOOST_CHECK_EQUAL(numAdjoints, adjointsOut.size());
    for(unsigned j=0; j<numAdjoints; j++){
      BOOST_CHECK_CLOSE(adjointsOut[j], adjoints[j], THRESHOLD);
    }
  }
}

BOOST_AUTO_TEST_SUITE_END()