/** 
* @file TimeInvariantParameter.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Control class test - Part MetaSingleStageESO in DyOS                 \n
* =====================================================================\n
* Test for time invariant parameter                                    \n
* =====================================================================\n
* @author Fady Assassa
* @date 12.12.2011
*/

#include "Control.hpp"
#include "MetaDataFactories.hpp"


#include <vector>

BOOST_AUTO_TEST_SUITE(TimeInvariantParameter)

/**
* @test time invariant parameter factory
*/
BOOST_AUTO_TEST_CASE(createTimeInvariantParameter)
{
  ControlFactory cFac;
  FactoryInput::ParameterInput input;
  const unsigned esoIndex = 3;
  const double lowerBound = -1.2;
  const double upperBound = 9.7;
  const double defaultValue = 10.54;
  input.esoIndex = esoIndex;
  input.lowerBound = lowerBound;
  input.upperBound = upperBound;
  input.value = defaultValue;
  input.type = FactoryInput::ParameterInput::TIME_INVARIANT;
  input.grids.resize(1);
  const double durationValue = 12.1;
  input.grids[0].duration = durationValue;
  DurationParameter::Ptr globalDuration(new DurationParameter(durationValue));

  
  Control timeInvariantParameter = cFac.createTimeInvariantParameter(input);

  timeInvariantParameter.getParameterizationGrid(0)->setDurationPointer(globalDuration);
  // check if the simple setters worked
  BOOST_CHECK_EQUAL(timeInvariantParameter.getUpperBound(),upperBound);
  BOOST_CHECK_EQUAL(timeInvariantParameter.getLowerBound(),lowerBound);
  BOOST_CHECK_EQUAL(timeInvariantParameter.getEsoIndex(),esoIndex);
  BOOST_CHECK_EQUAL(timeInvariantParameter.getIsInvariant(), true);
  // prepare for integration such that we can check the correctness of the active grid indices
  timeInvariantParameter.initializeForFirstIntegration();
  const unsigned int interval = 0;
  const ParameterizationGrid::Ptr checkGrid = timeInvariantParameter.getParameterizationGrid(interval);
  const DurationParameter::Ptr checkDuration = checkGrid->getDurationPointer();
  BOOST_CHECK_EQUAL(checkGrid->getActiveParameterIndex()[0], interval);
  BOOST_CHECK_EQUAL(timeInvariantParameter.getActiveGridIndex(), interval);
    // check if we have the same pointer adress
  BOOST_CHECK_EQUAL(checkDuration, globalDuration);
  // the evaluation of the parameter is time invariant. We check this here
  BOOST_CHECK_EQUAL(timeInvariantParameter.calculateCurrentControlValue(1.0), defaultValue);
}

BOOST_AUTO_TEST_SUITE_END()
