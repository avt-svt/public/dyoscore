/** 
* @file OptimizerInterface.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Single Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* Test for OptimizerMetaData                                           \n
* =====================================================================\n
* @author Fady Assassa, Tjalf Hoffmann
* @date 13.12.2011
*/

#include "OptimizerSingleStageMetaData.hpp"
#include "MetaDataTestDummies.hpp"
#include "Constraints.hpp"

#include "Array.hpp"

BOOST_AUTO_TEST_SUITE(TestOptimizerSingleStageMetaData)

/**
* @test TestOptimizerSingleStageMetaData - test member function calculateOptimizerProblemDimensions 
*                                          and getOptProbDim
*/
BOOST_AUTO_TEST_CASE(testGetOptProbDim)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData(new IntegratorMetaDataDummyForOptimizerMetaData());
  // full parameterization of the optimization 
  // (that is all control parameters, duration parameters and initial value parameters)
  std::vector<Parameter::Ptr> parameterization;
  integratorMetaData->getDecisionVariables(parameterization);
  const unsigned numParameters = parameterization.size();
  OptimizerSingleStageMetaData optMetaData(integratorMetaData);
  
  const unsigned numConstraints = 5;
  std::vector<StatePointConstraint> constraints(numConstraints);
  for(unsigned i=0; i<numConstraints; i++){
    constraints[i].setEsoIndex(0);
    constraints[i].setTime(1.0);
  }
  optMetaData.setConstraints(constraints);
  
  StatePointConstraint objective;
  const unsigned objectiveIndex = 2;
  objective.setEsoIndex(objectiveIndex);
  objective.setTime(1.0);
  optMetaData.setObjective(objective);
  
  optMetaData.calculateOptimizerProblemDimensions();
  
  const OptimizerProblemDimensions optProbDim = optMetaData.getOptProbDim();
  
  CsTripletMatrix::Ptr nonLinJac = optProbDim.nonLinJacDecVar; 
  
  BOOST_CHECK_EQUAL(nonLinJac->get_n_rows(), numConstraints);
  BOOST_CHECK_EQUAL(nonLinJac->get_n_cols(), numParameters);
  unsigned numConstraintsIntOptData = optMetaData.getIntegrationOptData()[0][0].nonLinConstIndices.size();
  //one of the int opt data constraints is the objective and the objective is filtered out
  BOOST_CHECK_EQUAL(nonLinJac->get_nnz(), numParameters * (numConstraintsIntOptData-1));
  
  //linJac contains constraint for global final time parameter
  CsTripletMatrix::Ptr linJac = optProbDim.linJac;
  BOOST_CHECK_EQUAL(linJac->get_n_rows(), 1);
  BOOST_CHECK_EQUAL(linJac->get_n_cols(), numParameters);
  BOOST_REQUIRE_EQUAL(linJac->get_nnz(), 1);
  BOOST_CHECK_EQUAL(linJac->get_matrix_ptr()->i[0], 0);
  BOOST_CHECK_EQUAL(linJac->get_matrix_ptr()->p[0], 6);
  BOOST_CHECK_CLOSE(linJac->get_matrix_ptr()->x[0], 1.0, THRESHOLD);
  
  BOOST_CHECK_EQUAL(optProbDim.nonLinObjIndex, objectiveIndex);
  
  
  BOOST_CHECK_EQUAL(optProbDim.numNonLinConstraints, numConstraints);
  BOOST_CHECK_EQUAL(optProbDim.numOptVars, numParameters);
}

/**
* @test TestOptimizerSingleStageMetaData - test function getDecVarBounds
*/
BOOST_AUTO_TEST_CASE(testGetDecVarBounds)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData(new IntegratorMetaDataDummyForOptimizerMetaData());
  
  const unsigned numParameters = integratorMetaData->getNumDecisionVariables();
  OptimizerSingleStageMetaData optMetaData(integratorMetaData);
  
  utils::Array<double> upperBounds(numParameters), lowerBounds(numParameters);
  optMetaData.getDecVarBounds(lowerBounds,upperBounds);
  
  // result defined in constructor of
  // class IntegratorMetaDataDummyForOptimizerMetaData
  for (unsigned i=0; i<numParameters;i++) {
      BOOST_CHECK_CLOSE(upperBounds[i], 7.3*(i+1), THRESHOLD);
      BOOST_CHECK_CLOSE(lowerBounds[i], 4.1*(i+1), THRESHOLD);
  }
}

/**
* @test TestOptimizerSingleStageMetaData - test function getDecVarValues
*/
BOOST_AUTO_TEST_CASE(testGetDecVarValues)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData(new IntegratorMetaDataDummyForOptimizerMetaData());
  
  const unsigned numParameters = integratorMetaData->getNumDecisionVariables();
  OptimizerSingleStageMetaData optMetaData(integratorMetaData);
  
  utils::Array<double> decVals(numParameters);
  optMetaData.getDecVarValues(decVals);
  
  // result defined in constructor of
  // class IntegratorMetaDataDummyForOptimizerMetaData
  for (unsigned i=0; i<numParameters;i++) {
      BOOST_CHECK_CLOSE(decVals[i], 5.2*(i+1), THRESHOLD);
  }
}

/**
* @test TestOptimizerSingleStageMetaData - test function setDecVarValues
*/
BOOST_AUTO_TEST_CASE(testSetDecVarValues)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData(new IntegratorMetaDataDummyForOptimizerMetaData());
  
  const unsigned numParameters = integratorMetaData->getNumDecisionVariables();
  OptimizerSingleStageMetaData optMetaData(integratorMetaData);
  
  utils::Array<double> newVals(numParameters);
  // result defined in constructor of
  // class IntegratorMetaDataDummyForOptimizerMetaData
  for (unsigned i=0; i<numParameters;i++) {
      newVals[i] = -6.5*(i+1);
  }
  
  utils::WrappingArray<const double> constValues(numParameters, newVals.getData());
  optMetaData.setDecVarValues(constValues);
  
  // result defined in constructor of
  // class IntegratorMetaDataDummyForOptimizerMetaData
  std::vector<Parameter::Ptr> decisionVariables;
  integratorMetaData->getDecisionVariables(decisionVariables);
  BOOST_REQUIRE_EQUAL(decisionVariables.size(), numParameters);
  for (unsigned i=0; i<numParameters;i++) {
      BOOST_CHECK_CLOSE(decisionVariables[i]->getParameterValue(), -6.5*(i+1), THRESHOLD);
  }
}

/**
* @test TestOptimizerSingleStageMetaData - test function getConstraintBounds
*/
BOOST_AUTO_TEST_CASE(testGetConstraintBounds)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData(new IntegratorMetaDataDummyForOptimizerMetaData());
  OptimizerSingleStageMetaData optMetaData(integratorMetaData);
  
  std::vector<StatePointConstraint> constr(2);
  constr[0].setLowerBound(9.8);
  constr[0].setUpperBound(10.2);
  constr[0].setEsoIndex(0);
  constr[0].setTime(1.0);
  constr[1].setLowerBound(-5.3);
  constr[1].setUpperBound(5.2);
  constr[1].setEsoIndex(0);
  constr[1].setTime(1.0);
  
  optMetaData.setConstraints(constr);
  utils::Array<double> lowerBounds(2),upperBounds(2);
  optMetaData.getNonLinConstraintBounds(lowerBounds,upperBounds);
  
  BOOST_CHECK_CLOSE(lowerBounds[0], 9.8, THRESHOLD);
  BOOST_CHECK_CLOSE(lowerBounds[1], -5.3, THRESHOLD);
  BOOST_CHECK_CLOSE(upperBounds[0], 10.2, THRESHOLD);
  BOOST_CHECK_CLOSE(upperBounds[1], 5.2, THRESHOLD);
}

/**
* @test OptimizerSingleStageMetaData - test function addNonLinConstraints
*
* This test first sets some constraints and then simulates an integratrion
* by directly setting the values at the given time points. Afterwards the
* values are extracted and compared.
* @note the right order is not properly tested here - that would be more suited for a
*       test including an integrator doing a complete integration
*/
BOOST_AUTO_TEST_CASE(AddNonLinConstraints)
{
  IntegratorMetaDataDummyForAddConstraintTest metaEso;
  const unsigned numConstraints = 6;
  // add some random constraint testdata
  // input parameters are (esoIndex, equationIndex, constraintIndex, timePoint)
  const unsigned numTimePoints = 3;
  DurationParameter::Ptr ft(new DurationParameter(1.0));
  metaEso.setGlobalDurationParameter(ft);
  
  const unsigned gridSize=10;
  std::vector<double> userGrid(gridSize +1);
  for(unsigned i=0; i<userGrid.size(); i++){
    userGrid[i] = double(i)/gridSize;
  }
  metaEso.setUserGrid(userGrid);
  
  std::vector<double> timePoints(numTimePoints);
  timePoints[0] = 0.5;
  timePoints[1] = 0.7;
  timePoints[2] = 0.8;
  // data for testing
  const unsigned varIndex[] = {4, 1, 5, 3, 1, 3};
  const unsigned constrIndex[] = {0, 1, 2, 3, 4, 5};
  unsigned timeIndex[] = {0, 1, 2, 1, 2, 0};
  const double lagrangeMultipliers[] = {5.5, 4.4, 3.3, 2.2, 1.1, 0.123};
  for(unsigned i=0; i<numConstraints; i++){
      timeIndex[i] = metaEso.addNonlinearConstraint(varIndex[i],
                                                    constrIndex[i],
                                                    timePoints[timeIndex[i]],
                                                    lagrangeMultipliers[i]);
  }

  //set testdata of the constraints
  std::vector<double> testData(numConstraints);
  for(unsigned i=0; i<numConstraints; i++){
    testData[i] = double(i);
  }
  
  //for every timepoint set some values of testData
  const unsigned numValues = numConstraints/numTimePoints;
  std::vector<double> values(numValues);
  unsigned testDataIndex = 0;
  for(unsigned i=0; i<numTimePoints; i++){
    for(unsigned j=0; j<numValues; j++){
      values[j] = testData[testDataIndex];
      testDataIndex++;
    }
    metaEso.setConstraintTestValues(timePoints[i], values);
  }
  
  //call function to be tested
  utils::Array<double> constrVals(numConstraints);
  std::vector<IntOptDataMap> intOptData = metaEso.getIntOptData();
  
  // get constraint data out of intOptData
  IntOptDataMap::iterator timeIt;
  int constrCounter = 0;
  // iterate trough time points
  for(timeIt = intOptData[0].begin() ; timeIt != intOptData[0].end(); timeIt++ ){
    // iterate through eso indices
    const unsigned numConstr = (*timeIt).second.nonLinConstVarIndices.size();
    for(unsigned i = 0; i < numConstr; i++){
      // insert value in the vector
      constrVals[constrCounter] = (*timeIt).second.nonLinConstValues[i];
      constrCounter++;
    }
  }

  for(unsigned i=0; i<numConstraints; i++){
    BOOST_CHECK_CLOSE(testData[i], constrVals[i], THRESHOLD);
  }
}

/**
* @test OptimizerSingleStageMetaData - test function getNonLinObjValue
*/
BOOST_AUTO_TEST_CASE(testgetNonLinObjValue)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData(new IntegratorMetaDataDummyForOptimizerMetaData());
  OptimizerSingleStageMetaData optMetaData(integratorMetaData);
  
  StatePointConstraint obj;
  obj.setEsoIndex(2);
  obj.setTime(1.0);
  optMetaData.setObjective(obj);
    
  const double value = optMetaData.getNonLinObjValue();
  // result defined in function getVariableValues of
  // class IntegratorMetaDataDummyForOptimizerMetaData
  BOOST_CHECK_CLOSE(value, 3.3000000000000003, THRESHOLD);
} 

/**
* @test OptimizerSingleStageMetaData - test function setLagrangeMult
*/
BOOST_AUTO_TEST_CASE(testSetLagrangeMult)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData (new IntegratorMetaDataDummyForOptimizerMetaData());
  //this is 
  OptimizerSingleStageMetaDataTestClass optMetaData(integratorMetaData);
  
  const unsigned numNonLinearConstraints = 2;
  const unsigned numLinearConstraints = 1;
  std::vector<StatePointConstraint> constr(2);
  for(unsigned i=0; i<constr.size(); i++){
    constr[i].setEsoIndex(0);
    constr[i].setTime(1.0);
  }
  optMetaData.setConstraints(constr);
  optMetaData.setNumConstraints(numNonLinearConstraints,
                                numLinearConstraints);
  
  utils::Array<double> nonLinearMultiplier(numNonLinearConstraints);
  nonLinearMultiplier[0] = -5.1;
  nonLinearMultiplier[1] = 9.7;
  utils::Array<double> linearMultiplier(numLinearConstraints);
  linearMultiplier[0] = 2.44;
  optMetaData.setLagrangeMultipliers(nonLinearMultiplier,
                                     linearMultiplier);
  
  utils::Array<double> nonLinMultiplierOut(numNonLinearConstraints);
  utils::Array<double> linMultiplierOut(numLinearConstraints);
  optMetaData.getLagrangeMultipliers(nonLinMultiplierOut,
                                     linMultiplierOut);
  
  for(unsigned i=0; i<numNonLinearConstraints; i++){
    BOOST_CHECK_CLOSE(nonLinearMultiplier[i], nonLinMultiplierOut[i], THRESHOLD);
  }
  for(unsigned i=0; i<numLinearConstraints; i++){
    BOOST_CHECK_CLOSE(linearMultiplier[i], linMultiplierOut[i], THRESHOLD);
  }
}

/**
* @test OptimizerSingleStageMetaData - test function getLagrangeMult
*/
BOOST_AUTO_TEST_CASE(testGetLagrangeMult)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData (new IntegratorMetaDataDummyForOptimizerMetaData());
  OptimizerSingleStageMetaDataTestClass optMetaData(integratorMetaData);
  
  const unsigned numNonLinearConstraints = 2;
  const unsigned numLinearConstraints = 0;
  optMetaData.setNumConstraints(numNonLinearConstraints,
                                numLinearConstraints);
  
  std::vector<double> nonLinConstraintValues(numNonLinearConstraints);
  nonLinConstraintValues[0] = 2.3;
  nonLinConstraintValues[1] = -3.4;
  std::vector<StatePointConstraint> constr(numNonLinearConstraints);
  for(unsigned i=0; i<numNonLinearConstraints; i++){
    constr[i].setLagrangeMultiplier(nonLinConstraintValues[i]);
    constr[i].setEsoIndex(0);
    constr[i].setTime(1.0);
  }
  
  optMetaData.setConstraints(constr);
  utils::Array<double> nonLinearMultipliers(numNonLinearConstraints);
  utils::Array<double> linearMultipliers(numLinearConstraints);
  optMetaData.getLagrangeMultipliers(nonLinearMultipliers,
                                     linearMultipliers);
  for(unsigned i=0; i<numNonLinearConstraints; i++){
    BOOST_CHECK_CLOSE(nonLinearMultipliers[i], nonLinConstraintValues[i], THRESHOLD);
  }
}

/**
* @test TestOptimizerSingleStageMetaData - test function getNonLinObjDerivative
*/
BOOST_AUTO_TEST_CASE(testGetNonLinObjDerivative)
{
  IntegratorSingleStageMetaData::Ptr integratorMetaData (new IntegratorMetaDataDummyForOptimizerMetaData());
  // integrator is set with sensitivity data such that we can verify it.
  const int numParams = integratorMetaData->getNumCurrentSensitivityParameters();
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();
  utils::Array<double> sens(numParams*numStates);
  for (int i=0; i<numParams*numStates; i++) {
    sens[i] = 1.1*(i+1);
  }
  integratorMetaData->setSensitivities(sens);

  OptimizerSingleStageMetaData optMetaData(integratorMetaData);

  StatePointConstraint objective;
  // the integratorMetaData has 3 equations (hardcoded), so pick any
  // valid equation index as the objective equation index
  const unsigned esoIndex = 1;
  objective.setEsoIndex(esoIndex);
  optMetaData.setObjective(objective);
  optMetaData.calculateOptimizerProblemDimensions();


  const unsigned numParameters = integratorMetaData->getNumCurrentSensitivityParameters();
  utils::Array<double> values(numParameters);
  optMetaData.getNonLinObjDerivative(values);
  
  // result defined in function getCurrentSensitivities of
  // class IntegratorMetaDataDummyForOptimizerMetaData
  for(unsigned i=0; i<values.getSize(); i++){
    unsigned sensIndex = numParams*esoIndex + i;
    BOOST_CHECK_CLOSE(values[i],sens[sensIndex], THRESHOLD);
  }
}


/**
* @test testGetNonLinConDerivative - tests function getNonLinConstraintDerivatives
*/
BOOST_AUTO_TEST_CASE(testGetNonLinConDerivative)
{
  NonLinDerTest *imdPtr = new NonLinDerTest();
  IntegratorSingleStageMetaData::Ptr integratorMetaData (imdPtr);
  DurationParameter::Ptr ft(new DurationParameter(1.0));
  imdPtr->setGlobalDurationParameter(ft);
  // integrator is set with sensitivity data such that we can verify it.
  OptimizerSingleStageMetaDataTestClass optMetaData(integratorMetaData);

  const unsigned numConstr = 2;
  const unsigned numTimePoints = 2;
  const unsigned esoIndex = 1;
  std::vector<StatePointConstraint> constr(numConstr * numTimePoints);
  std::vector<double> userGrid;
  // we include for every time point two constraints
  for(unsigned i=0; i<numConstr * numTimePoints; i=i + numTimePoints){
    constr[i].setEsoIndex(esoIndex);
    constr[i+1].setEsoIndex(esoIndex+1);

    const double time = (double(i) / numTimePoints) / double(numConstr-1);
    userGrid.push_back(time);
    constr[i].setTime(time);
    constr[i+1].setTime(time);
  }
  imdPtr->setUserGrid(userGrid);
  optMetaData.setConstraints(constr);
  utils::Array<double> vals(1);
  vals[0] = 2.3;
  integratorMetaData->setSensitivities(vals);

  optMetaData.calculateOptimizerProblemDimensions();

  OptimizerProblemDimensions optDim =  optMetaData.getOptProbDim();
  const unsigned numNonLinConstr = optDim.numNonLinConstraints;
  BOOST_REQUIRE(numNonLinConstr == numTimePoints * numConstr);

  const int numParams = integratorMetaData->getNumCurrentSensitivityParameters();
  const int numNonZeros = numConstr * numTimePoints * numParams;
  CsTripletMatrix::Ptr jacobianMatrixStruct = optDim.nonLinJacDecVar;
  double *values = new double[numNonZeros];
  CsTripletMatrix::Ptr derMatrix(new CsTripletMatrix(jacobianMatrixStruct->get_n_rows(),
                                                     jacobianMatrixStruct->get_n_cols(),
                                                     jacobianMatrixStruct->get_nnz(),
                                                     jacobianMatrixStruct->get_matrix_ptr()->i,
                                                     jacobianMatrixStruct->get_matrix_ptr()->p,
                                                     values));
  delete []values;
  // the first point has 4 gradients. coming from two parameters and two constraints
  // the second point has 8 gradients coming from four parameters and two constraints
  // however, we have all gradients in the vectors so we get 16 instead of 12 entries.
  // the number of zero entries are counted here.
  // the test example excludes the first parameter as a decision variable, so the
  // 4 sensitivities of the first parameter fall out.
  const int rows[] = {0, 1, 2, 2, 2, 3, 3 ,3};
  const int cols[] = {0, 0, 0, 1, 2, 0, 1, 2};
  const double checkVals[] = {11.2, 15.8, 17.8, 20.1, 22.4, 27, 29.3, 31.6};
  const int numNzz = 8;
  optMetaData.getNonLinConstraintDerivativesDecVar(derMatrix);

  BOOST_CHECK_EQUAL(derMatrix->get_nnz(), numNzz);
  for(int i=0; i<numNzz; i++){
    BOOST_CHECK_EQUAL(rows[i], derMatrix->get_matrix_ptr()->i[i]);
    BOOST_CHECK_EQUAL(cols[i], derMatrix->get_matrix_ptr()->p[i]);
    BOOST_CHECK_CLOSE(checkVals[i], derMatrix->get_matrix_ptr()->x[i], THRESHOLD);
  }
}


BOOST_AUTO_TEST_SUITE_END()

