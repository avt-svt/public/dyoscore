/** 
* @file IntegratorSingleStageMetaData.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Single Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* Test for MetaSingleStageEso                                          \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 25.8.2011
*/
#pragma once
#include "boost/test/floating_point_comparison.hpp"
#include "IntegratorSingleStageMetaData.hpp"
#include "Constraints.hpp"
#include "Control.hpp"
#include "ParameterizationGrid.hpp"
#include "Array.hpp"
#include "MetaDataTestDummies.hpp"
#include "MetaDataTestFactories.hpp"

#include <vector>

/**
* @def TEST_ACSAMMM_MODEL_NAME 
* @brief model used in IntegratorSingleStageMetaDataTest test
*/
#define TEST_ISSMD_MODEL_NAME "GenericEsoTest"


//! threshold for double comparison testmacro BOOST_CHECK_CLOSE to pass
#define THRESHOLD 1e-10

BOOST_AUTO_TEST_SUITE(IntegratorSingleStageMetaDataTest)


/**
* @test IntegratorSingleStageMetaDataTest - test constructor of class MetaSingleStageEso 
*
* case use an InitialValue object for construction and control variables
*/ 
BOOST_AUTO_TEST_CASE(TestConstruction)
{
  IntegratorSingleStageMetaData *integratorMetaData;
  BOOST_CHECK_NO_THROW(integratorMetaData = createIntegratorSingleStageMetaDataTestObject());
  delete integratorMetaData;
}

/** @brief this test checks if the union of the all grid in complete grids is successfull. */
BOOST_AUTO_TEST_CASE(testCompleteGrid)
{
  bool includeUserGrid = true;
  IntegratorSingleStageMetaData *integratorMetaData = 
        createIntegratorSingleStageMetaDataTestObject(includeUserGrid);
  // integratorMetaData must be properly initialized before being able to calculate anything on the data
  // we collect the entire grid data in the same fashion as the integrator
  integratorMetaData->initializeForFirstIntegration();
  const int numSteps = integratorMetaData->getNumIntegrationIntervals();
  std::vector<double> timePoints;
  for(int i=0; i<numSteps; i++){
    while(integratorMetaData->getStepEndTime() < 1.0){
        integratorMetaData->initializeForNewIntegrationStep();
        timePoints.push_back(integratorMetaData->getStepEndTime());
    }
    if (i < numSteps-1)
      integratorMetaData->initializeForNextIntegration();
  }

  // expected grid scaled by final times
  const int expectedSize = 9;
  const double checkTimePoints[] = {0.33333333333333331, 0.40114285714285713, 0.66666666666666663,
                              0.85714285714285721, 1.00000000000000000, 0.33333333333333326,
                              0.36159999999999992, 0.66666666666666652, 1.0000000000000000};

  BOOST_CHECK_EQUAL(expectedSize, timePoints.size());
  for(int i=0; i<expectedSize; i++){
    BOOST_CHECK_CLOSE(timePoints[i], checkTimePoints[i], THRESHOLD);
  }
  delete integratorMetaData;
}

/** @brief this test checks if we have the correct number of decision variables */
BOOST_AUTO_TEST_CASE(testGetNumDecisionVariables)
{
  // we need to set the global time as free to trigger also the last line of the function
  const bool additionalGrid = true;
  const bool freeGlobalTF = true;
  IntegratorSingleStageMetaData *integratorMetaData = 
        createIntegratorSingleStageMetaDataTestObject(!additionalGrid, freeGlobalTF);
  // integratorMetaData must be properly initialized before being able to calculate anything on the data
  // in order to trigger all lines of the function we have to free all initial values
  integratorMetaData->setAllInitialValuesFree();


  // we have 2 + 3 + 3 control parameter and 4 free initial values. Additionally, we have 2 free
  // final times. Thus, we should have 14 decision variables
  const unsigned expectedNumDecVars = 14;
  const unsigned numDecVars = integratorMetaData->getNumDecisionVariables();
  BOOST_CHECK_EQUAL(numDecVars, expectedNumDecVars);
  delete integratorMetaData;
}

/** @brief tests the function getNumStages */
BOOST_AUTO_TEST_CASE(testGetNumStages)
{
  IntegratorSingleStageMetaData *integratorMetaData = 
        createIntegratorSingleStageMetaDataTestObject();
  // this is a single stage test. Thus, the solution is obviously 1!
  const unsigned expectedNumStages = 1;
  BOOST_CHECK_EQUAL(integratorMetaData->getNumStages(), expectedNumStages);
  delete integratorMetaData;
}
/**
* @test IntegratorSingleStageMetaDataTest - test initialization of a new integration step
*/
BOOST_AUTO_TEST_CASE(TestInitializeForNewIntegrationStep)
{
  // integratorMetaData should have the following grid as the first integration grid
  // [0.00000000000000000,0.33333333333333331,0.66666666666666663,0.85714285714285721,1.0000000000000000]
  // so the first start time should be 0 and the first endtime should be 0.33333333333333331
  // after the second initialization the start time should be 0.33333333333333331 
  // and the endtime should be 0.66666666666666663
  // and so on
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  integratorMetaData->initializeForFirstIntegration();
  // check state of the integratorMetaData after first initialization
  const double assumedDuration = 0.7;
  const int assumedNumIntegrationIntervals = 2;
  // there should be three parameters after first initialization:
  // each of the two controls provides an active parameter
  // and the second control provides an active durationParameter
  int numberOfSensitivityParameters = 3;
  BOOST_CHECK_CLOSE(integratorMetaData->getIntegrationGridDuration(), assumedDuration, THRESHOLD);
  BOOST_CHECK_EQUAL(integratorMetaData->getNumCurrentSensitivityParameters(), 
                    numberOfSensitivityParameters);
  BOOST_CHECK_EQUAL(integratorMetaData->getNumIntegrationIntervals(), 
                    assumedNumIntegrationIntervals);

  integratorMetaData->initializeForNewIntegrationStep();
  // check the state of the integratorMetaData after initialization of first integration step
  BOOST_CHECK_CLOSE(0.0, integratorMetaData->getStepStartTime(), THRESHOLD);
  BOOST_CHECK_CLOSE(0.33333333333333331, integratorMetaData->getStepEndTime(), THRESHOLD);
  // there should be three active parameters (no new parameters added at first initialization)
  BOOST_CHECK_EQUAL(numberOfSensitivityParameters, integratorMetaData->getNumCurrentSensitivityParameters());
 

  integratorMetaData->initializeForNewIntegrationStep();
  // check the state of the integratorMetaData after initialization of the second integration step
  BOOST_CHECK_CLOSE(0.33333333333333331, integratorMetaData->getStepStartTime(), THRESHOLD);
  BOOST_CHECK_CLOSE(0.66666666666666663, integratorMetaData->getStepEndTime(), THRESHOLD);
  // a new active parameter should have been added to the parameter list
  numberOfSensitivityParameters++;
  BOOST_CHECK_EQUAL(numberOfSensitivityParameters, integratorMetaData->getNumCurrentSensitivityParameters());

  integratorMetaData->initializeForNewIntegrationStep();
  // check the state of the integratorMetaData after initialization of the third integration step
  BOOST_CHECK_CLOSE(0.66666666666666663, integratorMetaData->getStepStartTime(), THRESHOLD);
  BOOST_CHECK_CLOSE(0.85714285714285721, integratorMetaData->getStepEndTime(), THRESHOLD);
  // a new active parameter should have been added to the parameter list
  numberOfSensitivityParameters++;
  BOOST_CHECK_EQUAL(numberOfSensitivityParameters, integratorMetaData->getNumCurrentSensitivityParameters());

  integratorMetaData->initializeForNewIntegrationStep();
  // check the state of the integratorMetaData after initialization of the last integration step
  BOOST_CHECK_CLOSE(0.85714285714285721, integratorMetaData->getStepStartTime(), THRESHOLD);
  BOOST_CHECK_CLOSE(1.0000000000000000, integratorMetaData->getStepEndTime(), THRESHOLD);
  // a new active parameter should have been added to the parameter list
  numberOfSensitivityParameters++;
  BOOST_CHECK_EQUAL(numberOfSensitivityParameters, integratorMetaData->getNumCurrentSensitivityParameters());
  delete integratorMetaData;
}

/**
* @test IntegratorSingleStageMetaDataTest - test initialization of a new integration block 
*       using a different integration grid than the first
*/
BOOST_AUTO_TEST_CASE(TestInitializeForNextIntegration)
{
  // integratorMetaData has a total number of 2 integration grids
  // the second grid should have the following values:
  // [0.00000000000000000,0.33333333333333326,0.66666666666666652,1.0000000000000000]
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  integratorMetaData->initializeForFirstIntegration();
  // first integration block already tested in previous test case
  // calls to initializeForNextIntegrationStep are neccessary to get a consistent state
  while(integratorMetaData->getStepEndTime() != 1.0){
    integratorMetaData->initializeForNewIntegrationStep();
  }
  integratorMetaData->initializeForNextIntegration();
  const double assumedRelativeDuration = 1.2 - 0.7;
  // check state of the integratorMetaData at the beginning of the second integration block
  // there should be 6 sensitivity parameters from the first block
  // plus the first active parameters of the second block. 
  // One control still has an active parameter from the last integration block,
  // so only one acvtive control parameter is added.
  // The second final time parameter is no decision variable, so it should not be added here
  int numberOfSensitivityParameters = 7;
  BOOST_CHECK_CLOSE(integratorMetaData->getIntegrationGridDuration(), assumedRelativeDuration, THRESHOLD);
  BOOST_CHECK_EQUAL(integratorMetaData->getNumCurrentSensitivityParameters(), numberOfSensitivityParameters);

  integratorMetaData->initializeForNewIntegrationStep();
  BOOST_CHECK_CLOSE(0.0, integratorMetaData->getStepStartTime(), THRESHOLD);
  BOOST_CHECK_CLOSE(0.33333333333333326, integratorMetaData->getStepEndTime(), THRESHOLD);
  // no new sensitivity parameter added on the first integration step
  BOOST_CHECK_EQUAL(integratorMetaData->getNumCurrentSensitivityParameters(), numberOfSensitivityParameters);

  integratorMetaData->initializeForNewIntegrationStep();
  BOOST_CHECK_CLOSE(0.33333333333333326, integratorMetaData->getStepStartTime(), THRESHOLD);
  BOOST_CHECK_CLOSE(0.66666666666666652, integratorMetaData->getStepEndTime(), THRESHOLD);
  numberOfSensitivityParameters++;
  BOOST_CHECK_EQUAL(integratorMetaData->getNumCurrentSensitivityParameters(), numberOfSensitivityParameters);

  integratorMetaData->initializeForNewIntegrationStep();
  BOOST_CHECK_CLOSE(0.66666666666666652, integratorMetaData->getStepStartTime(), THRESHOLD);
  BOOST_CHECK_CLOSE(1.0000000000000000, integratorMetaData->getStepEndTime(), THRESHOLD);
  numberOfSensitivityParameters++;
  BOOST_CHECK_EQUAL(integratorMetaData->getNumCurrentSensitivityParameters(), numberOfSensitivityParameters);

  delete integratorMetaData;
}

/**
* @test IntegratorSingleStageMetaDataTest - test functions getJacobianFup and getJacobianFstates
*
* The test model has two assigned variables Para and Cont both declared as controls in the 
* MetaSingleStage creation. All other variables are states.
* The test model has the following equations:
* 2*$tim=1*2;
* $x = -2*tim + 4 + Para^2;
* 3*$y = (2*y + z + testvar1^3)*3;
* 4*$z = z*tim*4;
*
* testvar1 = x +y + cont;
* testvar2 = x -y;
*/
BOOST_AUTO_TEST_CASE(TestGetJabobianFstatesAndFup)
{
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  /*Para is assigned to 1.3, so the expected output of getJacobianFup is:
  * rowIndices: 1, 4
  * colIndices: 0, 0, 0, 0, 0, 0, 0, 1, 2 (in compressed sparse column format)
  *                                        (column indices of para and cont are expected to be 6 and 7)
  * values: -2.6, -1.0 (evaluated by lhs-rhs)*/
  //cs *Fup;
  CsCscMatrix::Ptr Fup;
  Fup = (*integratorMetaData).getJacobianFup();
  // test structure
  const int numColumnsFup = Fup->get_n_cols();
  BOOST_CHECK_EQUAL(Fup->get_matrix_ptr()->nzmax, 2);
  BOOST_CHECK_EQUAL(Fup->get_n_rows(), 6);
  BOOST_CHECK_EQUAL(Fup->get_n_cols(), 8);
  // test rows
  BOOST_CHECK_EQUAL(Fup->get_matrix_ptr()->i[0], 1);
  BOOST_CHECK_EQUAL(Fup->get_matrix_ptr()->i[1], 4);
  // testColums
  for(int i=0; i<numColumnsFup - 1 ; i++){
    BOOST_CHECK_EQUAL(Fup->get_matrix_ptr()->p[i], 0);
  }
  BOOST_CHECK_EQUAL(Fup->get_matrix_ptr()->p[numColumnsFup-1], 1);
  BOOST_CHECK_EQUAL(Fup->get_matrix_ptr()->p[numColumnsFup], 2);
  // test values
  BOOST_CHECK_CLOSE(Fup->get_matrix_ptr()->x[0], -2.6, THRESHOLD);
  BOOST_CHECK_CLOSE(Fup->get_matrix_ptr()->x[1], -1.0, THRESHOLD);

  
  // order of the states is tim = 0.0, x=1.0, y=2.0, z=3.0, testvar1=testvar2=0.0
  // there are 12 state nonzeroes
  // rowIndices: 1, 3, 4, 5, 2, 4, 5, 2, 3, 2, 4, 5
  // colIndices: 0, 2, 4, 7, 9, 11, 12 (here the columns 6 and 7 are cut out)
//  cs *Fstates;
  CsCscMatrix::Ptr Fstates;
  Fstates = integratorMetaData->getJacobianFstates();
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->nzmax, 12);
  BOOST_CHECK_EQUAL(Fstates->get_n_cols(), 6);
  BOOST_CHECK_EQUAL(Fstates->get_n_rows(), 6);
  // test rows
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[0], 1);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[1], 3);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[2], 4);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[3], 5);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[4], 2);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[5], 4);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[6], 5);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[7], 2);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[8], 3);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[9], 2);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[10], 4);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->i[11], 5);
  // test colums
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->p[0], 0);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->p[1], 2);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->p[2], 4);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->p[3], 7);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->p[4], 9);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->p[5], 11);
  BOOST_CHECK_EQUAL(Fstates->get_matrix_ptr()->p[6], 12);
  // test values,
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[0], 2.0, THRESHOLD);  //-2*tim + 4 + Para^2/dtim
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[1], -12.0, THRESHOLD);//z*tim*4/dtim
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[2], -1.0, THRESHOLD); //testvar1 = x +y + cont/dx
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[3], -1.0, THRESHOLD); //testvar2 = x -y/dx
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[4], -6.0, THRESHOLD); //(2*y + z + testvar1^3)*3/dy
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[5], -1.0, THRESHOLD); //testvar1 = x +y + cont/dy
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[6], 1.0, THRESHOLD);  //testvar2 = x -y/dy
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[7], -3.0, THRESHOLD); //(2*y + z + testvar1^3)*3/dz
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[8], -0.0, THRESHOLD); //z*tim*4/dz
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[9], -0.0, THRESHOLD);//(2*y + z + testvar1^3)*3/dtestvar1
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[10], 1.0, THRESHOLD); //testvar1 = x +y + cont/dtestvar1
  BOOST_CHECK_CLOSE(Fstates->get_matrix_ptr()->x[11], 1.0, THRESHOLD); //testvar2 = x -y/dtestvar2

	delete integratorMetaData;
}

/**
* @test IntegratorSingleStageMetaDataTest - test calculateRhsStates function
*/
BOOST_AUTO_TEST_CASE(testCalculateRhsStates)
{
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  // integratorMetaData must be properly initialized before being able to calculate anything on the data
  integratorMetaData->initializeForFirstIntegration();
  integratorMetaData->initializeForNewIntegrationStep();
  // current time must be set as well
  integratorMetaData->setStepCurrentTime(0.0);
  GenericEso::Ptr genEso = integratorMetaData->getGenericEso();
  utils::Array<double> rhsStates(genEso->getNumEquations());
  utils::Array<double> inputStates(genEso->getNumEquations(), 1);
  integratorMetaData->calculateRhsStates(inputStates, rhsStates);

  // evaluate equation 2*$tim = 1*2;
  BOOST_CHECK_CLOSE(rhsStates[0], -2*integratorMetaData->getIntegrationGridDuration(), THRESHOLD);
  // evaluate equation     $x = -2*tim + 4 + Para^2;
  // Para has been set to value 1 (see createMetaSingleStageEsoTestObject()->parameterizationValues1)
  BOOST_CHECK_CLOSE(rhsStates[1], -3*integratorMetaData->getIntegrationGridDuration(), THRESHOLD);
  // 3*$y = (2*y + z + testvar1^3)*3;
  BOOST_CHECK_CLOSE(rhsStates[2], -12*integratorMetaData->getIntegrationGridDuration(), THRESHOLD);
  // 4*$z = z*tim*4
  BOOST_CHECK_CLOSE(rhsStates[3], -4*integratorMetaData->getIntegrationGridDuration(), THRESHOLD);
  // testvar1 = x +y + cont; 
  // cont has been set to value 2 (see createMetaSingleStageEsoTestObject()->parameterizationValues2)
  BOOST_CHECK_CLOSE(rhsStates[4], -3*integratorMetaData->getIntegrationGridDuration(), THRESHOLD);
  // testvar2 = x -y;
  BOOST_CHECK_CLOSE(rhsStates[5], 1*integratorMetaData->getIntegrationGridDuration(), THRESHOLD);
  delete integratorMetaData;
}

/**
* @test IntegratorSingleStageMetaDataTest - test calculateRhsSensitivities1stOrder function
*
* This function simply tests if the rhsSens vector is correctly constructed. For this purpose
* some DummyParameters are set into the MSSE which return simply consecutive numbers.
*/ 
BOOST_AUTO_TEST_CASE(testCalculateRhsSensitivities1stOrder)
{
  SensitivityTestMSSE sensTestMSSE;
  
  const unsigned numParameters = 3;
  const unsigned numEquations = sensTestMSSE.getNumEquations();
  std::vector<Parameter::Ptr> dummyParameters(numParameters);
  for(unsigned i=0; i<numParameters; i++){
    dummyParameters[i] = Parameter::Ptr(new DummyParameter(i*numEquations));
  }
  sensTestMSSE.setParameters(dummyParameters);

  utils::Array<double> inputSens(numParameters*numEquations);
  utils::Array<double> rhsSens(numParameters*numEquations);
  sensTestMSSE.calculateSensitivitiesForward1stOrder(inputSens, rhsSens);

  for(unsigned i=0; i<rhsSens.getSize(); i++){
    BOOST_CHECK_CLOSE(rhsSens[i], (double)i, THRESHOLD);
  }
}

/**
* @test IntegratorSingleStageMetaDataTest - test sortParameters function
*/
BOOST_AUTO_TEST_CASE(testSortParameters)
{
  SensitivityTestMSSE sensTestMSSE;
  const unsigned numParameters = 4;
  std::vector<Parameter::Ptr> dummyParameters(numParameters);
  for(unsigned i=0; i<numParameters; i++){
    dummyParameters[i] = Parameter::Ptr(new DummyParameter(i));
    dummyParameters[i]->setParameterIndex(i);
  }

  std::vector<Parameter::Ptr> compareParameters;
  compareParameters.assign(dummyParameters.begin(), dummyParameters.end());

  //swap the first two parameters
  Parameter::Ptr temp = dummyParameters[0];
  dummyParameters[0] = dummyParameters[1];
  dummyParameters[1] = temp;

  sensTestMSSE.setParameters(dummyParameters);

  sensTestMSSE.sortControlParameters();
  
  sensTestMSSE.getParameters(dummyParameters);

  BOOST_CHECK_EQUAL_COLLECTIONS(dummyParameters.begin(), 
                                dummyParameters.end(), 
                                compareParameters.begin(), 
                                compareParameters.end());
}

/**
* @test IntegratorSingleStageMetaDataTest - test function getParameterization
* 
* This test simply checks the correct number of the parameters. It is not
* checked if the parameters are in the right order or if the pointers do
* actually point to the right parameter at all.
* @todo enhance the test for checking the content of the parameterization vector.
*/
BOOST_AUTO_TEST_CASE(TestGetParameterization)
{
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  
  std::vector<Parameter::Ptr> parameterization;
  integratorMetaData->getDecisionVariables(parameterization);

  // values derived from function createMetaSingleStageEsoTestObject
  const unsigned numIntervalsFirstControl = 2;
  const unsigned numGridsFirstControl = 1;
  const unsigned numDurationsFirstControl = 0;
  const unsigned numIntervalsSecondControl = 3;
  const unsigned numGridsSecondControl = 2;
  const unsigned numDurationsSecondControl = 1;
  const unsigned numGlobalDurationParameters = 0;
  unsigned numTotalParameters = 0;
  
  numTotalParameters += numIntervalsFirstControl*numGridsFirstControl;
  numTotalParameters += numDurationsFirstControl;
  numTotalParameters += numIntervalsSecondControl*numGridsSecondControl;
  numTotalParameters += numDurationsSecondControl;
  numTotalParameters += numGlobalDurationParameters;
  
  BOOST_CHECK_EQUAL(parameterization.size(), numTotalParameters);
  
  delete integratorMetaData;
}

/**
* @test IntegratorSingleStageMetaDataTest - test function getConstraintSensitivities
* 
* get the sensitivities of some equations (constraints) at the current time point
* The tested function is extracting a subset of sensitivity information.
*/
BOOST_AUTO_TEST_CASE(TestGetConstraintSensitivities)
{
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  integratorMetaData->initializeForFirstIntegration();
  const unsigned numParameters = integratorMetaData->getNumCurrentSensitivityParameters();
  const unsigned numEquations = integratorMetaData->getGenericEso()->getNumEquations();
  // arbitrary number of equation indices - must be in range (0..numEquations)
  // 
  const unsigned numEquationIndices = 2;
  BOOST_REQUIRE(numEquations > numEquationIndices);
  
  //set sensitivities to ascending integers (0, 1, 2, 3, ...)
  utils::Array<double> allSensitivities(numParameters*numEquations);
  for(unsigned i=0; i<allSensitivities.getSize(); i++){
    allSensitivities[i]=double(i);
  }
  vector<unsigned> constIndex(numEquationIndices), esoIndex(numEquationIndices);
  vector<double> lagrangeMultipliers(numEquationIndices);
  double timePoint = 0.0;
  for (unsigned i = 0; i< numEquationIndices; i++){
    esoIndex[i] = i;
    constIndex[i] = i;
    lagrangeMultipliers[i] = double(i)/2.0;
    integratorMetaData->addNonlinearConstraint(esoIndex[i],
                                               constIndex[i],
                                               timePoint,
                                               lagrangeMultipliers[i]);
  }

  integratorMetaData->setStepCurrentTime(timePoint);
  integratorMetaData->setSensitivities(allSensitivities);
  
  //set some arbitrary equation indices
  std::vector<unsigned> stateIndices(numEquationIndices);
  stateIndices[0] = 0;
  stateIndices[1] = 1;
  
  //utils::Array<double> constraintSensitivities(numEquationIndices*numParameters);
  vector<IntOptDataMap> optDataMap = integratorMetaData->getIntOptData();
  IntOptDataMap::iterator it = optDataMap[0].end();
  it --;
  utils::ExtendableArray<double> constraintSensitivities((*it).second.nonLinConstGradients);
  //integratorMetaData->getConstraintSensitivities(equationIndices, constraintSensitivities);
  
  // check sensitivity block for each equation index
  // assuming that numEquations is 5 and the first state index is 1,
  // the first block should contain (1, 6, 11, ...)
  for(unsigned i=0; i<stateIndices.size(); i++){
    for(unsigned j=0; j<numParameters; j++){
      unsigned sensIndex = i*numParameters+j;
      double compareValue = double(numEquations*j+stateIndices[i]);
      BOOST_CHECK_CLOSE(constraintSensitivities[sensIndex], compareValue, THRESHOLD);
    }
  }
  delete integratorMetaData;
}


/**
* @test IntegratorSingleStageMetaDataTest - test functions getAdjoints, getLagrangeDers
                                 and setAdjointsAndLagrangeDers
*
* This test first sets test data using function setAdjointsAndLagrangeDers
* and afterwards compares the output of the get functions to the set data.
*/
BOOST_AUTO_TEST_CASE(TestSetAndGetLagrangeDersAndAdjoints)
{
  //set up single stage object
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  
  const unsigned numAdjoints = integratorMetaData->getGenericEso()->getNumStates();
  const unsigned numLagrangeDers = integratorMetaData->getNumDecisionVariables();
  //use this function to resize lagrange ders and adjoints
  integratorMetaData->initializeAtStageDuration();
  //since lagrangeDers are simply added collect the current lagrangeDers first
  //usually these should be initialized with 0.0;
  std::vector<double> lagrangeDersStart;
  integratorMetaData->getLagrangeDers(lagrangeDersStart);
  BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersStart.size());
  for(unsigned i=0; i<lagrangeDersStart.size(); i++){
    BOOST_CHECK_CLOSE(lagrangeDersStart[i], 0.0, THRESHOLD);
  }
  
  utils::Array<double> adjoints(numAdjoints);
  for(unsigned i=0; i<numAdjoints; i++){
    adjoints[i] = 1.1*i;
  }
  utils::Array<double> lagrangeDerivatives(numLagrangeDers);
  for(unsigned i=0; i<numLagrangeDers; i++){
    lagrangeDerivatives[i] = 0.05 + 0.2*i;
  }
  integratorMetaData->setAdjointsAndLagrangeDerivatives(adjoints, lagrangeDerivatives);
  
  std::vector<double> lagrangeDersOut;
  integratorMetaData->getLagrangeDers(lagrangeDersOut);
  BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersOut.size());
  for(unsigned i=0; i<numLagrangeDers; i++){
    BOOST_CHECK_CLOSE(lagrangeDersOut[i], lagrangeDerivatives[i], THRESHOLD);
  }
  
  std::vector<double> adjointsOut;
  integratorMetaData->getAdjoints(adjointsOut);
  BOOST_CHECK_EQUAL(numAdjoints, adjointsOut.size());
  for(unsigned i=0; i<numAdjoints; i++){
    BOOST_CHECK_CLOSE(adjointsOut[i], adjoints[i], THRESHOLD);
  }
}

/** 
* @brief test the creation of Mass matrix 
*
* @todo set up test that has a different order of variables such that assigned variables
*       are not all at the end of the variables vector. Otherwise the cutting out of the
*       assigned from the mass matrix cannot be properly tested
*/
BOOST_AUTO_TEST_CASE(TestMassMatrix)
{
  GenericEso::Ptr eso(new JadeGenericEso(TEST_ISSMD_MODEL_NAME));
  
  IntegratorSingleStageMetaDataTestClass integratorMetaData(eso);
  
  GenericEso::Ptr genEso = integratorMetaData.getGenericEso();
  const unsigned numNonZeros = genEso->getNumDifferentialNonZeroes();
  const int numDiffVars = genEso->getNumDifferentialVariables();
  const int numStates = genEso->getNumStates();
  const int numEquations = genEso->getNumEquations();

  // The test model has the following equations:
  // 2*$tim=1*2;
  // $x = -2*tim + 4 + Para^2;
  // 3*$y = (2*y + z + testvar1^3)*3;
  // 4*$z = z*tim*4;
  //
  // testvar1 = x +y + cont;
  // testvar2 = x -y;
  // The first four rows of the M-matrix have nonzero entries.
  // Order of variables are tim, x, y, z, so the row indices are (0,1,2,3).
//  cs massMatrix = integratorMetaData.getMMatrix();
  CsCscMatrix::Ptr massMatrix = integratorMetaData.getMMatrix();
  std::vector<double> compareVals(numNonZeros), compareP(numDiffVars + 1), compareI(numNonZeros);
  compareVals[0] = 2; compareVals[1] = 1; compareVals[2] = 3;  compareVals[3] = 4;
  compareP[0] = 0; compareP[1] = 1; compareP[2] = 2;  compareP[3] = 3;  compareP[4] = numNonZeros;
  compareI[0] = 0; compareI[1] = 1; compareI[2] = 2;  compareI[3] = 3;
  BOOST_CHECK_EQUAL(massMatrix->get_n_rows(), numEquations);
  BOOST_CHECK_EQUAL(massMatrix->get_n_cols(), numStates);
  for(unsigned i=0; i<numNonZeros; i++){
    BOOST_CHECK_CLOSE(massMatrix->get_matrix_ptr()->x[i], compareVals[i], THRESHOLD);
    BOOST_CHECK_EQUAL(massMatrix->get_matrix_ptr()->i[i], compareI[i]);
  }
  for(int i=0; i<numDiffVars + 1; i++)
    BOOST_CHECK_EQUAL(massMatrix->get_matrix_ptr()->p[i], compareP[i]);
}

/**
* @test IntegratorSingleStageMetaDataTest - test function getDurationParameters
*/
BOOST_AUTO_TEST_CASE(TestGetDurationParameters)
{
  //set up single stage object
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  std::vector<DurationParameter::Ptr> durationParameters;
  
  integratorMetaData->getDurationParameters(durationParameters);
  
  // durationParameters contains all duration parameters of all control grids
  // here the first control has only the global final time, the second control an
  // additional final time parameter - so the function should return two final time parameters
  BOOST_CHECK_EQUAL(durationParameters.size(), 2);
  //intermediate duration was set to 0.7 (see createIntegratorSingleStageMetaDataTestObject)
  BOOST_CHECK_CLOSE(durationParameters.front()->getParameterValue(), 0.7, THRESHOLD);
  //global final time was set to 1.2 - so the value of the second grid duration should be 0.5
  BOOST_CHECK_CLOSE(durationParameters.back()->getParameterValue(), 0.5, THRESHOLD);
  
}

/**
* @test IntegratorSingleStageMetaDataTest - test function getNumStateNonZeroes
*/
BOOST_AUTO_TEST_CASE(TestGetNumStateNonZeroes)
{
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  // Equations of funtion:
  // 2*$tim=1*2;

  // $x = -2*tim + 4 + Para^2;
  // 3*$y = (2*y + z + testvar1^3)*3;
  // 4*$z = z*tim*4;
  // testvar1 = x +y + cont;
  // testvar2 = x -y;
  // states of the problem are: tim, x, y, z, testvar1, testvar2
  // therefore the state nonzeroes should be 12
  BOOST_CHECK_EQUAL(integratorMetaData->getNumStateNonZeroes(), 12);
}

/**
* @test IntegratorSingleStageMetaDataTest - test function setStates
*
* This function also tests subfunction setNonlinearConstraints
* and indirectly addNonlinearConstraint
*/
BOOST_AUTO_TEST_CASE(TestSetStates)
{
  IntegratorSingleStageMetaData *integratorMetaData = createIntegratorSingleStageMetaDataTestObject();
  Observer::Ptr observer(new IntegrationObserver());
  integratorMetaData->setObserver(observer);
  
  integratorMetaData->initializeForFirstIntegration();
  integratorMetaData->initializeForNewIntegrationStep();
  
  const unsigned numStates = integratorMetaData->getGenericEso()->getNumStates();
  utils::Array<double> states(numStates);
  for(unsigned i=0; i<numStates; i++){
    states[i] = 3.4 + i*7.4;
  }
  
  //set artificial constraint to check subfunction setNonlinearConstraints
  const unsigned esoIndex = 0;
  const unsigned constraintIndex = 0;
  double timePoint = integratorMetaData->getStepEndTime();
  //scale timePoint to global grid
  timePoint *= integratorMetaData->getIntegrationGridDuration();
  timePoint /= integratorMetaData->getStageDuration();
  double lagrangeMultiplier = 0.0;
  unsigned timeIndex = integratorMetaData->addNonlinearConstraint(esoIndex,
                                                                  constraintIndex,
                                                                  timePoint,
                                                                  lagrangeMultiplier);
  
  integratorMetaData->setStates(states);
  
  utils::Array<double> setStates(numStates);
  integratorMetaData->getGenericEso()->getStateValues(setStates.getSize(), setStates.getData());
  
  for(unsigned i=0; i<numStates; i++){
    BOOST_CHECK_CLOSE(states[i], setStates[i], THRESHOLD);
  }
  
  std::vector<IntOptDataMap> intOptData = integratorMetaData->getIntOptData();
  
  BOOST_REQUIRE_EQUAL(intOptData.size(), 1);
  BOOST_REQUIRE(intOptData[0].count(timeIndex) > 0);
  
  const unsigned numConstraints = 1;
  SavedOptDataStruct sods = intOptData[0][timeIndex];
  BOOST_REQUIRE_EQUAL(sods.lagrangeMultipliers.size(), numConstraints);
  BOOST_CHECK_CLOSE(sods.lagrangeMultipliers[0], lagrangeMultiplier, THRESHOLD);
  BOOST_REQUIRE_EQUAL(sods.nonLinConstIndices.size(), numConstraints);
  BOOST_CHECK_EQUAL(sods.nonLinConstIndices[0], constraintIndex);
  BOOST_REQUIRE_EQUAL(sods.nonLinConstVarIndices.size(), numConstraints);
  BOOST_CHECK_EQUAL(sods.nonLinConstVarIndices[0], esoIndex);
  
  utils::Array<double> constraintValue(numConstraints);
  integratorMetaData->getGenericEso()->getVariableValues(constraintValue.getSize(),
                                                         constraintValue.getData(),
                                                         &sods.nonLinConstVarIndices[0]);
  BOOST_REQUIRE_EQUAL(sods.nonLinConstValues.getSize(), numConstraints);
  BOOST_CHECK_CLOSE(sods.nonLinConstValues[0], constraintValue[0], THRESHOLD);
 
}

/**
* @test IntegratorSingleStageMetaDataTest - test function setMappingValues
*/
BOOST_AUTO_TEST_CASE(TestSetMappingValues)
{
  SetInititalTestClass imdTest;
  const unsigned numInitials = 10;
  std::vector<InitialValueParameter::Ptr> initials(numInitials);
  std::vector<double> originalValues(numInitials);
  for(unsigned i=0; i<numInitials; i++){
    originalValues[i] = 1.1*i;
    initials[i] = InitialValueParameter::Ptr(new InitialValueParameter());
    initials[i]->setParameterValue(originalValues[i]);
    initials[i]->setEsoIndex(i);
  }
  
  imdTest.setInitialValueParameter(initials);
  const unsigned numMappedValues = 4;
  const unsigned startingIndex = 2;
  utils::Array<double> mappedValues(numMappedValues);
  std::vector<int> mappedIndices(numMappedValues);
  for(unsigned i=0; i<numMappedValues; i++){
    mappedValues[i] = -2.4 - i*0.1;
    mappedIndices[i] = startingIndex+i;
  }
  imdTest.setMappingValues(mappedValues, mappedIndices);
  
  //check if mapped parameters have been changed
  for(unsigned i=0; i<numMappedValues; i++){
    BOOST_CHECK_CLOSE(initials[mappedIndices[i]]->getParameterValue(), mappedValues[i], THRESHOLD);
  }
  
  //check if all other initials remain their value
  for(unsigned i=0; i<startingIndex; i++){
    BOOST_CHECK_CLOSE(initials[i]->getParameterValue(), originalValues[i], THRESHOLD);
  }
  for(unsigned i=startingIndex + numMappedValues; i<numInitials; i++){
    BOOST_CHECK_CLOSE(initials[i]->getParameterValue(), originalValues[i], THRESHOLD);
  }
}

BOOST_AUTO_TEST_SUITE_END()//IntegratorSingleStageMetaDataTest

BOOST_AUTO_TEST_SUITE(ConstraintTest)

/**
* @test ConstraintTest - test setter and getter of class StatePointConstraint
*/ 
BOOST_AUTO_TEST_CASE(TestStatePathConstraint)
{
  StatePointConstraint pointConstraint;
  const double lowerBound = 0.0;
  const double upperBound = 10.0;
  const double time = 0.32;
  const unsigned esoIndex = 19;
  const double lagrangeMultiplier = -12.456;
  pointConstraint.setLowerBound(lowerBound);
  pointConstraint.setUpperBound(upperBound);
  pointConstraint.setEsoIndex(esoIndex);
  pointConstraint.setTime(time);
  pointConstraint.setLagrangeMultiplier(lagrangeMultiplier);

  // check for equality since values are set and not calculated
  // there should not be any difference
  BOOST_CHECK_EQUAL(pointConstraint.getLowerBound(), lowerBound);
  BOOST_CHECK_EQUAL(pointConstraint.getUpperBound(), upperBound);
  BOOST_CHECK_EQUAL(pointConstraint.getEsoIndex(), esoIndex);
  BOOST_CHECK_EQUAL(pointConstraint.getTime(), time);
  BOOST_CHECK_EQUAL(pointConstraint.getLagrangeMultiplier(), lagrangeMultiplier);
  
}

/**
* @test ConstraintTest - test copy constructor
*/
BOOST_AUTO_TEST_CASE(CopyConstructorTest)
{
  StatePointConstraint pointConstraint;
  pointConstraint.setLowerBound(2.4);
  pointConstraint.setUpperBound(7.8);
  pointConstraint.setEsoIndex(3);
  pointConstraint.setTime(0.85);
  pointConstraint.setLagrangeMultiplier(-17.4);

  StatePointConstraint copy(pointConstraint);
  
  BOOST_CHECK_EQUAL(copy.getEsoIndex(), pointConstraint.getEsoIndex());
  BOOST_CHECK_EQUAL(copy.getTime(), pointConstraint.getTime());
  BOOST_CHECK_EQUAL(copy.getLagrangeMultiplier(), pointConstraint.getLagrangeMultiplier());
  BOOST_CHECK_EQUAL(copy.getLowerBound(), pointConstraint.getLowerBound());
  BOOST_CHECK_EQUAL(copy.getUpperBound(), pointConstraint.getUpperBound());
}

/**
* @test IntegratorSingleStageMetaDataTest - check if IntegratorMetaData can set plot grid correctly
*
* Following things should be tested:
* 1. Insertion of the plotgrid (especially in multigrid)
* 2. Correct deletion of close gridpoints
* 
* To check, whether the states are stored correctly must be tested in TestGenericIntegrator testsuite
*/
BOOST_AUTO_TEST_CASE(TestPlotGrid)
{
  GenericEsoTestFactory genEsoFac;
  GenericEso::Ptr eso(genEsoFac.createJadeGenericEsoTestExample());
  
  // initialize controls
  const int numIntervals1 = 2, numIntervals2 = 3;
  std::vector<double> parameterizationValues1, parameterizationValues2;
  parameterizationValues1.push_back(1.0);
  parameterizationValues1.push_back(2.0);
  parameterizationValues1.push_back(4.0);
  parameterizationValues2.push_back(4.0);
  parameterizationValues2.push_back(3.0);
  parameterizationValues2.push_back(1.5);
  parameterizationValues2.push_back(1.0);
  
  ParameterizationGrid::Ptr Grid1(new ParameterizationGrid(numIntervals1, PieceWiseLinear));
  ParameterizationGrid::Ptr Grid2(new ParameterizationGrid(numIntervals2, PieceWiseLinear));

  Grid1->setAllParameterizationValues(parameterizationValues1);
  Grid2->setAllParameterizationValues(parameterizationValues2);
  std::vector<ParameterizationGrid::Ptr> pg;
  pg.push_back(Grid1);
  pg.push_back(Grid2);
  
  const double epsilon = 1e-10;
  const double intermediateDuration = 200 + epsilon;
  const double globalDuration = 1000.0;
  Control c1;
  
  double upperBoundc1 = 4.0; double lowerBoundc1 = 1.0;
  c1.setLowerBound(lowerBoundc1);
  c1.setUpperBound(upperBoundc1);
  pg[0]->setDurationValue(intermediateDuration);
  pg[1]->setDurationValue(globalDuration - intermediateDuration);
  c1.setControlGrid(pg);
  DurationParameter::Ptr durationGrid1 = c1.getParameterizationGrid(0)->getDurationPointer();
  durationGrid1->setWithSensitivity(true);
  std::vector<double> duration2(1);
  duration2[0] = intermediateDuration;
  pg[0]->setDurationValue(intermediateDuration);
  
  // set control Eso indices to the first two assigned indices
  const EsoIndex numParameters = eso->getNumParameters();
  std::vector<EsoIndex> parameterIndices(numParameters);
  eso->getParameterIndex(numParameters, &parameterIndices[0]);
  BOOST_REQUIRE(numParameters >= 2);
  //choose parameter cont as control (should be 2nd parameter index)
  c1.setEsoIndex(parameterIndices[1]);
  std::vector<Control> controls;
  controls.push_back(c1) ;
  
  std::vector<InitialValueParameter::Ptr> initials;
  
  DurationParameter::Ptr durationParameter(new DurationParameter());
  durationParameter->setParameterValue(globalDuration);
  durationParameter->setWithSensitivity(true);
  
  std::vector<double> userGrid;
  
  // construct output MetaSingleStageEso
  PlotGridTestClass::Ptr issmd( new PlotGridTestClass(eso,
                                                      controls,
                                                      initials,
                                                      durationParameter,
                                                      userGrid));
  
  issmd->activatePlotGrid();
  unsigned numPlotGridPoints = 97;
  std::vector<double> explicitPlotGrid;
  explicitPlotGrid.push_back(0.25001347);
  explicitPlotGrid.push_back(0.499937216);
  explicitPlotGrid.push_back(0.750012390);
  issmd->setPlotGrid(numPlotGridPoints, explicitPlotGrid);
  issmd->triggerCreateGlobalGrid();
  std::vector<double> globalGrid = issmd->getCompleteStageGrid();
  BOOST_CHECK_EQUAL(numPlotGridPoints + explicitPlotGrid.size() + 5, globalGrid.size());
  std::vector<double> parameterGrid;
  parameterGrid.push_back(1.0);
  parameterGrid.push_back(1.0);
  parameterGrid.push_back(intermediateDuration + 2.0/3.0*(globalDuration - intermediateDuration));
  parameterGrid.push_back(intermediateDuration + 1.0/3.0*(globalDuration - intermediateDuration));
  parameterGrid.push_back(intermediateDuration);
  double currentParameterGridValue = intermediateDuration/2;
  unsigned plotGridFactor = 0;
  for(unsigned i=0; i<globalGrid.size(); i++){
    if(fabs(globalGrid[i] - currentParameterGridValue) < THRESHOLD){
      BOOST_REQUIRE(!parameterGrid.empty());
      currentParameterGridValue = parameterGrid.back();
      parameterGrid.pop_back();
    }
    else if(i== 27){
      BOOST_CHECK_CLOSE(globalGrid[i], explicitPlotGrid[0]*globalDuration, THRESHOLD);
    }
    else if(i==53){
      BOOST_CHECK_CLOSE(globalGrid[i], explicitPlotGrid[1]*globalDuration, THRESHOLD);
    }
    else if(i==79){
      BOOST_CHECK_CLOSE(globalGrid[i], explicitPlotGrid[2]*globalDuration, THRESHOLD);
    }
    else{
      BOOST_CHECK_CLOSE(globalGrid[i], double(globalDuration*plotGridFactor)/numPlotGridPoints, THRESHOLD);
      plotGridFactor++;
    }
  }
  
  // check if close points are merged - by splitting the integration interval into two grids (1/5 : 4/5)
  // and splitting the first grid into 2 and the second grid into 3 we need multiples of 30 to cover
  // all timepoints of the optimization grid with the plotgrid. Since the grids are not exactly split
  // 1/5 to 4/5, the points do not fit 100% - but should be so close, that they are merged.
  numPlotGridPoints = 300;
  explicitPlotGrid.clear();
  issmd->setPlotGrid(numPlotGridPoints, explicitPlotGrid);
  issmd->triggerCreateGlobalGrid();
  globalGrid = issmd->getCompleteStageGrid();
  BOOST_CHECK_EQUAL(numPlotGridPoints + 1, globalGrid.size()); // a grid with n intervals has n+1 timepoints
  for(unsigned i=0; i<globalGrid.size(); i++){
    BOOST_CHECK_CLOSE(globalGrid[i], double(globalDuration*i)/numPlotGridPoints, THRESHOLD);
  }
}

BOOST_AUTO_TEST_SUITE_END()

/**
* @test IntegratorSingleStageMetaDataTest - proof of concept for uniting the control grids
* 
* this test checks the correctness of the implementation for calculating the complete grid 
* in the MetaSingleStageEso constructor
*/
BOOST_AUTO_TEST_CASE(TestVectorUnion)
{
  const int firstA[] = {0, 5, 10};
  const int secondA[] = {0, 3, 6, 10};
  std::vector<int> first, second;
  std::vector<int>::iterator it;

  first.assign(firstA, firstA + 3);
  second.assign(secondA, secondA + 4);
  std::vector<int> firstCopy = first;

  first.resize(first.size() + second.size());
  it = set_union (firstCopy.begin(), firstCopy.end(), second.begin(), second.end(), first.begin());
  first.resize(it - first.begin());
  BOOST_REQUIRE_EQUAL(first.size(), 5);
  BOOST_CHECK_EQUAL(first[0], 0);
  BOOST_CHECK_EQUAL(first[1], 3);
  BOOST_CHECK_EQUAL(first[2], 5);
  BOOST_CHECK_EQUAL(first[3], 6);
  BOOST_CHECK_EQUAL(first[4], 10);
}

