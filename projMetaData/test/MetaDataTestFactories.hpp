/** 
* @file MetaDataTestFactories.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* factory for Generic Eso test objects                                 \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 21.12.2011
*/

#pragma once
#include "IntegratorSingleStageMetaData.hpp"
#include "Control.hpp"
#include "ParameterizationGrid.hpp"
#include "GenericEsoTestFactories.hpp"

/**
* @brief construct testobject of type MetaSingleStageEso
*
* The genericEso used is GenericEsoTest in jade/Example/DyOS.
* The MetaSingleStageEso has two controls (for the two assigned values of the model).
* The first control has one parameterization grid with two equidistant intervals.
* The second control has two parameterization grids each containing three equidistant intervals.
* The global duration is 1.2, the duration of the first parameterization grid of the 
* second control is 0.7. 
* @return pointer to created MetaSingleStageEso object
*/
IntegratorSingleStageMetaData *createIntegratorSingleStageMetaDataTestObject(const bool includeUserGrid = false,
                                                                             const bool freeGlobalTF = false)
{
  GenericEsoTestFactory genEsoFac;
  // since this object is deleted within the MetaSingleStageEso class,
  // it must not be deleted within this function
  GenericEso::Ptr acsammmEso(genEsoFac.createJadeGenericEsoTestExample());
  // initialize controls
  const int numIntervals1 = 2, numIntervals2 = 3;
  const double arbitraryValue1 = 1.0, arbitraryValue2 = 2.0;
  std::vector<double> parameterizationValues1(numIntervals1, arbitraryValue1);
  std::vector<double> parameterizationValues2(numIntervals2, arbitraryValue2);
  const ControlType PWC = PieceWiseConstant;
  
  ParameterizationGrid::Ptr Grid1(new ParameterizationGrid(numIntervals1, PWC));
  ParameterizationGrid::Ptr Grid2(new ParameterizationGrid(numIntervals2, PWC));
  ParameterizationGrid::Ptr Grid3(new ParameterizationGrid(numIntervals2, PWC));

  Grid1->setAllParameterizationValues(parameterizationValues1);
  Grid2->setAllParameterizationValues(parameterizationValues2);
  Grid3->setAllParameterizationValues(parameterizationValues2);
  std::vector<ParameterizationGrid::Ptr> pg1, pg2;
  pg1.push_back(Grid1);
  // set two grids for control 2
  pg2.push_back(Grid2);
  pg2.push_back(Grid3);
  
  // the second control has two parameterization grids adding an intermediate duration
  const double intermediateDuration = 0.7;
  const double globalDuration = 1.2;
  const double durationGrid2 = globalDuration - intermediateDuration;
  Control c1, c2;
  
  double upperBoundc1 = 4.1; double lowerBoundc1 = 2.3;
  double upperBoundc2 = 8.7; double lowerBoundc2 = 0;
  c1.setLowerBound(lowerBoundc1);
  c1.setUpperBound(upperBoundc1);
  c2.setLowerBound(lowerBoundc2);
  c2.setUpperBound(upperBoundc2);
  pg1[0]->setDurationValue(globalDuration);
  c1.setControlGrid(pg1);
  DurationParameter::Ptr durationGrid1 = c1.getParameterizationGrid(0)->getDurationPointer();
  durationGrid1->setIsDecisionVariable(freeGlobalTF);
  durationGrid1->setWithSensitivity(freeGlobalTF);
  std::vector<double> duration2(1);
  duration2[0] = intermediateDuration;
  pg2[0]->setDurationValue(intermediateDuration);
  pg2[1]->setDurationValue(durationGrid2);
  c2.setControlGrid(pg2);
  DurationParameter::Ptr durationnGrid2 = c2.getParameterizationGrid(1)->getDurationPointer();
  durationnGrid2->setIsDecisionVariable(freeGlobalTF);
  durationnGrid2->setWithSensitivity(freeGlobalTF);
  
  // set control Eso indices to the first two assigned indices
  const EsoIndex numParameters = acsammmEso->getNumParameters();
  std::vector<EsoIndex> parameterIndices(numParameters);
  acsammmEso->getParameterIndex(numParameters, &parameterIndices[0]);
  BOOST_REQUIRE(numParameters >= 2);
  c1.setEsoIndex(parameterIndices[0]);
  c2.setEsoIndex(parameterIndices[1]);
  std::vector<Control> controls;
  controls.push_back(c1) ;
  controls.push_back(c2) ;
  
  std::vector<InitialValueParameter::Ptr> initials;
  
  DurationParameter::Ptr durationParameter(new DurationParameter());
  durationParameter->setParameterValue(globalDuration);
  durationParameter->setWithSensitivity(freeGlobalTF);
  
  std::vector<double> userGrid;
  if(includeUserGrid){
    // has to be between 0 and 1
    userGrid.push_back(0.234);
    userGrid.push_back(0.734);
  }
  
  // construct output MetaSingleStageEso
  IntegratorSingleStageMetaData *issmd = new IntegratorSingleStageMetaData(acsammmEso, 
                                                                           controls,
                                                                           initials,
                                                                           durationParameter,
                                                                           userGrid);
  return issmd; 
  
}
