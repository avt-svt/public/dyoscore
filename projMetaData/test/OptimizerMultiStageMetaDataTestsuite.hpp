/** 
* @file OptimizerMultiStageMetaData.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Multi Stage Optimizer - Part of DyOS                            \n
* =====================================================================\n
* Test for OptimizerMultiStageMetaData                                 \n
* =====================================================================\n
* @author Kathrin Frankl
* @date 12.04.2012
*/

#pragma once
#include "boost/test/floating_point_comparison.hpp"
#include "OptimizerMultiStageMetaData.hpp"
#include "OptimizerMultiStageMetaDataTestDummies.hpp"

//! threshold for double comparison testmacro BOOST_CHECK_CLOSE to pass
#define BOOST_TOL 1.0e-10

BOOST_AUTO_TEST_SUITE(OptimizerMultiStageMetaDataTest)

/**
* @test OptimizerMultiStageMetaDataTest - test assembly of nonlinear constraint sensitivities
*/ 
BOOST_AUTO_TEST_CASE(TestGetNonLinConstraintDerivatives)
{

  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
                              
  //first stage has 18 parameters
  //second stage has
  int *indicesSpace = new int[14];
  double *valuesSpace = new double[14];
  CsTripletMatrix::Ptr P(new CsTripletMatrix(5, 4, 14, indicesSpace, indicesSpace, valuesSpace));
  delete []valuesSpace;
  delete []indicesSpace;
  P->get_matrix_ptr()->i[0] = 0;
  P->get_matrix_ptr()->i[1] = 0;
  P->get_matrix_ptr()->i[2] = 1;
  P->get_matrix_ptr()->i[3] = 1;
  P->get_matrix_ptr()->i[4] = 2;
  P->get_matrix_ptr()->i[5] = 2;
  P->get_matrix_ptr()->i[6] = 2;
  P->get_matrix_ptr()->i[7] = 3;
  P->get_matrix_ptr()->i[8] = 3;
  P->get_matrix_ptr()->i[9] = 3;
  P->get_matrix_ptr()->i[10] = 4;
  P->get_matrix_ptr()->i[11] = 4;
  P->get_matrix_ptr()->i[12] = 4;
  P->get_matrix_ptr()->i[13] = 4;
  P->get_matrix_ptr()->p[0] = 0;
  P->get_matrix_ptr()->p[1] = 1;
  P->get_matrix_ptr()->p[2] = 0;
  P->get_matrix_ptr()->p[3] = 1;
  P->get_matrix_ptr()->p[4] = 0;
  P->get_matrix_ptr()->p[5] = 1;
  P->get_matrix_ptr()->p[6] = 2;
  P->get_matrix_ptr()->p[7] = 0;
  P->get_matrix_ptr()->p[8] = 1;
  P->get_matrix_ptr()->p[9] = 2;
  P->get_matrix_ptr()->p[10] = 0;
  P->get_matrix_ptr()->p[11] = 1;
  P->get_matrix_ptr()->p[12] = 2;
  P->get_matrix_ptr()->p[13] = 3;
  
  optmsmd.getNonLinConstraintDerivativesDecVar(P);
  
  const double con_result[14] = { 1.5,  2.5, 
                           1.7,  2.7, 
                           2.2,  2.3,  4.1,
                           2.4,  2.5,  4.2, 
                           3.1,  3.2,  3.3,  5.1 };

  for (int z=0; z<P->get_nnz(); z++)
  {
    BOOST_CHECK_CLOSE(P->get_matrix_ptr()->x[z], con_result[z], THRESHOLD);
  }
}

/**
* @test OptimizerMultiStageMetaDataTest - test assembly of nonlinear objective sensitivities
*/ 
BOOST_AUTO_TEST_CASE(TestGetNonLinObjDerivative)
{

  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
  
  utils::Array<double> objDer(4);
  optmsmd.getNonLinObjDerivative(objDer);
  
  const double obj_result[4] = {5.11, 6.22, 3.33, 5.2};
  
  for (unsigned z=0; z<objDer.getSize(); z++)
  {
    BOOST_CHECK_CLOSE(objDer[z], obj_result[z], THRESHOLD);
  }
}

/**
* @test OptimizerMultiStageMetaDataTest - test value of the nonlinear objective function
*/ 
BOOST_AUTO_TEST_CASE(TestGetNonLinObjValue)
{
  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
                              
  const double objValue = optmsmd.getNonLinObjValue();
  
  BOOST_CHECK_CLOSE(objValue, 4.4, THRESHOLD);
}

/**
* @test OptimizerMultiStageMetaDataTest - test values of the nonlinear constraints
*/ 
BOOST_AUTO_TEST_CASE(TestGetNonLinConstraintValues)
{
  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
  const unsigned numValues = 5;
  utils::Array<double> values(numValues);
  optmsmd.getNonLinConstraintValues(values);
  
  const double con_result[numValues] = {1.1, 1.2, 2.1, 2.2, 3.1};
  
  for (unsigned z=0; z<5; z++)
  {
    BOOST_CHECK_CLOSE(values[z], con_result[z], THRESHOLD);
  }
}

/**
* @test OptimizerMultiStageMetaDataTest - test lower and upper bounds of the constraints
*/ 
BOOST_AUTO_TEST_CASE(TestGetConstraintBounds)
{
  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
  
  utils::Array<double> lowerBounds(5), upperBounds(5);
  
  optmsmd.getNonLinConstraintBounds(lowerBounds, upperBounds);
  
  const double lb_result[5] = {1.1, 1.2, 2.1, 2.2, 3.1};
  const double ub_result[5] = {11.1, 11.2, 21.1, 21.2, 31.1};
  
  for (unsigned z=0; z<5; z++)
  {
    BOOST_CHECK_CLOSE(lowerBounds[z], lb_result[z], THRESHOLD);
    BOOST_CHECK_CLOSE(upperBounds[z], ub_result[z], THRESHOLD);
  }
}

/**
* @test OptimizerMultiStageMetaDataTest - test values of the lagrange multipliers
*/ 
BOOST_AUTO_TEST_CASE(TestGetLagrangeMultiplier)
{
  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
  
  const unsigned numNonLinMult = 5;
  const unsigned numLinMult = 2;
  utils::Array<double> nonLinMultiplier(numNonLinMult);
  utils::Array<double> linMultiplier(numLinMult);
  
  optmsmd.getLagrangeMultipliers(nonLinMultiplier,
                                 linMultiplier);
  
  const double nonLinMultiplier_res[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
  const double linMultiplier_res[2] = {6.6, 7.7};
  
  for (unsigned i=0; i<numNonLinMult; i++){
    BOOST_CHECK_CLOSE(nonLinMultiplier[i], nonLinMultiplier_res[i], THRESHOLD);
  }
  for (unsigned i=0; i<numLinMult; i++){
    BOOST_CHECK_CLOSE(linMultiplier[i], linMultiplier_res[i], THRESHOLD);
  }
}

/**
* @test OptimizerMultiStageMetaDataTest - test setting of the lagrange multipliers
*/ 
BOOST_AUTO_TEST_CASE(TestSetLagrangeMultiplier)
{
  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
  
  const unsigned numNonLinMult = 5;
  utils::Array<double> nonLinMultiplier(numNonLinMult);
  nonLinMultiplier[0] = 4.5;
  nonLinMultiplier[1] = 5.5;
  nonLinMultiplier[2] = 6.5;
  nonLinMultiplier[3] = 7.5;
  nonLinMultiplier[4] = 8.5;
  const unsigned numLinMult = 2;
  utils::Array<double> linMultiplier(numLinMult);
  linMultiplier[0] = 1.5;
  linMultiplier[1] = 2.5;
  
  optmsmd.setLagrangeMultipliers(nonLinMultiplier,
                                 linMultiplier);
  
  utils::Array<double> nonLinMultiplier_res(numNonLinMult);
  utils::Array<double> linMultiplier_res(numLinMult);
  
  optmsmd.getLagrangeMultipliers(nonLinMultiplier_res,
                                 linMultiplier_res);
  
  for (unsigned i=0; i<numNonLinMult; i++){
    BOOST_CHECK_CLOSE(nonLinMultiplier[i], nonLinMultiplier_res[i], THRESHOLD);
  }
  for (unsigned i=0; i<numLinMult; i++){
    BOOST_CHECK_CLOSE(linMultiplier[i], linMultiplier_res[i], THRESHOLD);
  }
}

/**
* @test OptimizerMultiStageMetaDataTest - test lower and upper bounds of the decision variables
*/ 
BOOST_AUTO_TEST_CASE(TestGetDecVarBounds)
{
  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
  
  utils::Array<double> lowerBounds(4), upperBounds(4);
  
  optmsmd.getDecVarBounds(lowerBounds, upperBounds);
  
  const double lb_result[4] = {-11.1, -22.2, -33.3, -44.4};
  const double ub_result[4] = {11.1, 22.2, 33.3, 44.4};
  
  for (unsigned z=0; z<4; z++)
  {
    BOOST_CHECK_CLOSE(lowerBounds[z], lb_result[z], THRESHOLD);
    BOOST_CHECK_CLOSE(upperBounds[z], ub_result[z], THRESHOLD);
  }
}

/**
* @test OptimizerMultiStageMetaDataTest - test setting lower and upper bounds of the decision variables
*/ 
BOOST_AUTO_TEST_CASE(TestSetGetDecVarValues)
{
  OptimizerMultiStageMetaData optmsmd = createMSMDdummy();
  
  utils::Array<double> values(4);
  values[0] = 6.6;
  values[1] = 7.7;
  values[2] = 8.8;
  values[3] = 9.9;
  
  utils::WrappingArray<const double> constValues(values.getSize(), values.getData());
  optmsmd.setDecVarValues(constValues);
  
  utils::Array<double> values_res(4);
  
  optmsmd.getDecVarValues(values_res);
  
  for (unsigned z=0; z<4; z++)
  {
    BOOST_CHECK_CLOSE(values[z], values_res[z], THRESHOLD);
  }

}

BOOST_AUTO_TEST_SUITE_END()