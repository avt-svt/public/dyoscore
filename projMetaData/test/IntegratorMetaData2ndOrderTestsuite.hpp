/** 
* @file MetaData2ndOrder.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* 2nd order MetaData class test - Part MetaData in DyOS                \n
* =====================================================================\n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 30.07.2012
*/

#include "boost/test/floating_point_comparison.hpp"

#include "IntegratorSingleStageMetaData.hpp"
#include "IntegratorMultiStageMetaData.hpp"
#include "IntegratorMetaData2ndOrderDummies.hpp"
#include "GenericEsoFactory.hpp"

//! threshold for double comparison testmacro BOOST_CHECK_CLOSE to pass
#define THRESHOLD 1e-10

/**
* @def TEST_MODEL_NAME 
* @brief model used in MetaSingleStageEsoTest test
*/
#define TEST_MODEL_NAME "carErweitert"

BOOST_AUTO_TEST_SUITE(IntegratorMetaData2ndOrderTest)

/**
* @test IntegratorMetaData2ndOrderTest - check function getGenericEso2ndOrder
                                         returns the correct object
*
* This test checks the function getGenericEso2ndOrder for single stage and
* multi stage objects. The objects are created with different GenericEso
* pointers. The address of the returned GenericEso is then compared to the
* address of the GenericEso that has originally been used to create the object.
*/
BOOST_AUTO_TEST_CASE(TestGetGenericEso2ndOrder)
{
  //declare dummies for instantiation of single stage objects
  std::vector<Control> controls;
  std::vector<InitialValueParameter::Ptr> initials;
  DurationParameter::Ptr duration(new DurationParameter(1.0));
  std::vector<double> additionalGrid;
  GenericEsoFactory genEsoFac;
  
  //single stage test
  GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
  IntegratorSSMetaData2ndOrder issmd2nd(genEso, controls, initials, duration, additionalGrid);
  
  BOOST_CHECK_EQUAL(genEso.get(), issmd2nd.getGenericEso2ndOrder().get());
  
  //multi stage test
  const unsigned numStages = 4;
  std::vector<GenericEso2ndOrder::Ptr> esoVec(numStages);
  std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages(numStages);
  for(unsigned i=0; i<numStages; i++){
    esoVec[i] = GenericEso2ndOrder::Ptr(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    stages[i] = IntegratorSSMetaData2ndOrder::Ptr(new IntegratorSSMetaData2ndOrder(esoVec[i],
                                                                                   controls,
                                                                                   initials,
                                                                                   duration,
                                                                                   additionalGrid));
  }
  
  std::vector<Mapping::Ptr> mappings;
  TestIntegratorMSMD2ndOrder imsmd2nd(mappings, stages);
  for(unsigned i=0; i<numStages; i++){
    imsmd2nd.setStage(i);
    BOOST_CHECK_EQUAL(esoVec[i].get(), imsmd2nd.getGenericEso2ndOrder().get());
  }
}

/**
* @test IntegratorMetaData2ndOrderTest - test functions getAdjoints, getLagrangeDers
                                         and setAdjointsAndLagrangeDers on a 
                                         IntegratorSSMetaData2ndOrder object
*
* This test checks the overloaded functions concerning adjoints and lagrangeDers
* The test problem contains initial value parameters, duration parameters (with sensitivity)
* and two controls from which one is a multi grid control.
*/
BOOST_AUTO_TEST_CASE(TestSetAndGetLagrangeDersAndAdjointsSingleStage)
{
  
  // set up global final time
  DurationParameter::Ptr duration(new DurationParameter(4.0));
  duration->setWithSensitivity(true);
  
  //set up 2 controls - one with 10 gridpoints and the other with 2 grids of 3 and 4 grid points
  std::vector<Control> controls(2);
  ParameterizationGrid::Ptr grid1(new ParameterizationGrid(10, PieceWiseConstant));
  grid1->setDurationPointer(duration);
  std::vector<ParameterizationGrid::Ptr> gridV1;
  gridV1.push_back(grid1);
  controls[0].setControlGrid(gridV1);
  
  ParameterizationGrid::Ptr grid2(new ParameterizationGrid(3, PieceWiseConstant));
  DurationParameter::Ptr f1(new DurationParameter(1.0));
  f1->setWithSensitivity(true);
  grid2->setDurationPointer(f1);
  ParameterizationGrid::Ptr grid3(new ParameterizationGrid(4, PieceWiseConstant));
  DurationParameter::Ptr f2(new DurationParameter(3.0));
  f2->setWithSensitivity(true);
  grid3->setDurationPointer(f2);
  std::vector<ParameterizationGrid::Ptr> gridV2;
  gridV2.push_back(grid2);
  gridV2.push_back(grid3);
  controls[1].setControlGrid(gridV2);
  
  
  //set up 3 initials with sensitivity information
  const unsigned numInitials = 3;
  std::vector<InitialValueParameter::Ptr> initials(numInitials);
  for(unsigned i=0; i<numInitials; i++){
    initials[i] = InitialValueParameter::Ptr(new InitialValueParameter());
    initials[i]->setWithSensitivity(true);
    initials[i]->setEsoIndex(i);
    initials[i]->setEquationIndex(i);
  }
  

  std::vector<double> additionalGrid;
  GenericEsoFactory genEsoFac;
  
  // single stage test
  GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
  IntegratorSSMetaData2ndOrder issmd2nd(genEso, controls, initials, duration, additionalGrid);
  
  issmd2nd.initializeAtStageDuration();
  
  const unsigned numStates = issmd2nd.getGenericEso()->getNumStates();
  const unsigned numDecVars = issmd2nd.getNumDecisionVariables();
  const unsigned numAdjoints = numStates*(1+numDecVars);
  const unsigned numLagrangeDers = numDecVars*(1+numDecVars);
  
  // since lagrangeDers are simply added collect the current lagrangeDers first
  // usually these should be initialized with 0.0;
  std::vector<double> lagrangeDersStart;
  issmd2nd.getLagrangeDers(lagrangeDersStart);
  BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersStart.size());
  for(unsigned i=0; i<lagrangeDersStart.size(); i++){
    BOOST_CHECK_CLOSE(lagrangeDersStart[i], 0.0, THRESHOLD);
  }
  
  std::vector<double> adjointsStart;
  issmd2nd.getAdjoints(adjointsStart);
  BOOST_CHECK_EQUAL(numAdjoints, adjointsStart.size());
  utils::Array<double> adjoints(numAdjoints);
  for(unsigned i=0; i<numAdjoints; i++){
    adjoints[i] = 1.1*i;
  }
  utils::Array<double> lagrangeDers(numLagrangeDers);
  for(unsigned i=0; i<numLagrangeDers; i++){
    lagrangeDers[i] = 0.05 + 0.2*i;
  }
  issmd2nd.setAdjointsAndLagrangeDerivatives(adjoints, lagrangeDers);
  
  std::vector<double> lagrangeDersOut;
  issmd2nd.getLagrangeDers(lagrangeDersOut);
  BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersOut.size());
  for(unsigned i=0; i<numLagrangeDers; i++){
    BOOST_CHECK_CLOSE(lagrangeDersOut[i], lagrangeDers[i], THRESHOLD);
  }
  
  std::vector<double> adjointsOut;
  issmd2nd.getAdjoints(adjointsOut);
  BOOST_CHECK_EQUAL(numAdjoints, adjointsOut.size());
  for(unsigned i=0; i<numAdjoints; i++){
    BOOST_CHECK_CLOSE(adjointsOut[i], adjoints[i], THRESHOLD);
  }
}

/**
* @test IntegratorMetaData2ndOrderTest - test functions getAdjoints, getLagrangeDers
                                         and setAdjointsAndLagrangeDers on a
                                         IntegratorMSMetaData2ndOrder object
*
* This test checks the overloaded functions concerning adjoints and lagrangeDers
* The test problem has three stages each with one control and 5,7, and 9 gridpoints
* The first stage has initial value parameters and all stages have global duration
* parameters with sensitivity
*/
BOOST_AUTO_TEST_CASE(TestSetAndGetLagrangeDersAndAdjointsMultiStage)
{
  const unsigned numStages = 3;
  std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages(numStages);
  std::vector<double> additionalGrid;
  GenericEsoFactory genEsoFac;

  ////////////////////
  // set up stage 1 //
  ////////////////////
  {
    // set up global final time
    DurationParameter::Ptr duration(new DurationParameter(1.0));
    duration->setWithSensitivity(true);
    
    std::vector<Control> controls(1);
    ParameterizationGrid::Ptr grid(new ParameterizationGrid(5, PieceWiseConstant));
    grid->setDurationPointer(duration);
    std::vector<ParameterizationGrid::Ptr> gridV1;
    gridV1.push_back(grid);
    controls[0].setControlGrid(gridV1);    
    
    //set up 3 initials with sensitivity information
    const unsigned numInitials = 3;
    std::vector<InitialValueParameter::Ptr> initials(numInitials);
    for(unsigned i=0; i<numInitials; i++){
      initials[i] = InitialValueParameter::Ptr(new InitialValueParameter());
      initials[i]->setWithSensitivity(true);
      initials[i]->setEsoIndex(i);
      initials[i]->setEquationIndex(i);
    }
    

    
    GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    stages[0] = IntegratorSSMetaData2ndOrder::Ptr(new IntegratorSSMetaData2ndOrder(genEso,
                                                                                   controls,
                                                                                   initials,
                                                                                   duration,
                                                                                   additionalGrid));
  }
  
  ////////////////////
  // set up stage 2 //
  ////////////////////
  {
    // set up global final time
    DurationParameter::Ptr duration(new DurationParameter(1.0));
    duration->setWithSensitivity(true);
    
    std::vector<Control> controls(1);
    ParameterizationGrid::Ptr grid(new ParameterizationGrid(7, PieceWiseConstant));
    grid->setDurationPointer(duration);
    std::vector<ParameterizationGrid::Ptr> gridV1;
    gridV1.push_back(grid);
    controls[0].setControlGrid(gridV1);
    
    std::vector<InitialValueParameter::Ptr> initials;
    

    
    GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    stages[1] = IntegratorSSMetaData2ndOrder::Ptr(new IntegratorSSMetaData2ndOrder(genEso,
                                                                                 controls,
                                                                                 initials,
                                                                                 duration,
                                                                                 additionalGrid));
  }
  
  ////////////////////
  // set up stage 3 //
  ////////////////////
  {
    // set up global final time
    DurationParameter::Ptr duration(new DurationParameter(1.0));
    duration->setWithSensitivity(true);
    
    std::vector<Control> controls(1);
    ParameterizationGrid::Ptr grid(new ParameterizationGrid(9, PieceWiseConstant));
    grid->setDurationPointer(duration);
    std::vector<ParameterizationGrid::Ptr> gridV1;
    gridV1.push_back(grid);
    controls[0].setControlGrid(gridV1);    
    
    std::vector<InitialValueParameter::Ptr> initials;
    
    
    GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    stages[2] = IntegratorSSMetaData2ndOrder::Ptr(new IntegratorSSMetaData2ndOrder(genEso,
                                                                                 controls,
                                                                                 initials,
                                                                                 duration,
                                                                                 additionalGrid));
  }
  
  //apply full state mapping for all stages
  std::vector<Mapping::Ptr> mappings(numStages-1);
  for(unsigned i=0; i<numStages-1; i++){
    mappings[i] = Mapping::Ptr(new SingleShootingMapping());
    GenericEso2ndOrder::Ptr genEso(genEsoFac.createJadeGenericEso(TEST_MODEL_NAME));
    const unsigned numStates = genEso->getNumStates();
    iiMap varIndexMap;
    for(unsigned j=0; j<numStates; j++){
      varIndexMap[j]=j;
    }
    iiMap eqnIndexMap;
    for(unsigned j=0; j<numStates; j++){
      eqnIndexMap[j]=j;
    }
    mappings[i]->setIndexMaps(varIndexMap, eqnIndexMap);
  }
  
  TestIntegratorMSMD2ndOrder imsmd2nd(mappings, stages);
  
  imsmd2nd.initializeAtStageDuration();
  
  //check initialized lagrangeDers
  const unsigned numStates = imsmd2nd.getGenericEso()->getNumStates();
  const unsigned numDecVars = imsmd2nd.getNumSensitivityParameters();
  const unsigned numAdjoints = numStates*(1+numDecVars);
  const unsigned numLagrangeDers = numDecVars*(1+numDecVars);
  
   // since lagrangeDers are simply added collect the current lagrangeDers first
  // usually these should be initialized with 0.0;
  std::vector<double> lagrangeDersStart;
  imsmd2nd.getLagrangeDers(lagrangeDersStart);
  BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersStart.size());
  for(unsigned i=0; i<lagrangeDersStart.size(); i++){
    BOOST_CHECK_CLOSE(lagrangeDersStart[i], 0.0, THRESHOLD);
  }
  std::vector<double> adjointsStart;
  imsmd2nd.getAdjoints(adjointsStart);
  BOOST_CHECK_EQUAL(numAdjoints, adjointsStart.size());
  
  std::vector<double> lagrangeDersOut = lagrangeDersStart;
  //check set and get functions for each stage
  for(unsigned i=0; i<numStages; i++){
    imsmd2nd.setStage(i);
    
     utils::Array<double> adjoints(numAdjoints);
    for(unsigned j=0; j<numAdjoints; j++){
      adjoints[j] = 1.1*j + 0.01*i;
    }
    utils::Array<double> lagrangeDers(numLagrangeDers);
    std::vector<double> lagrangeDersCompare = lagrangeDersOut;
    for(unsigned j=0; j<numLagrangeDers; j++){
      lagrangeDers[j] = 0.02*i + 0.005 + 0.2*j;
      lagrangeDersCompare[j] += lagrangeDers[j];
    }
    imsmd2nd.setAdjointsAndLagrangeDerivatives(adjoints, lagrangeDers);
    

    
    imsmd2nd.getLagrangeDers(lagrangeDersOut);
    BOOST_CHECK_EQUAL(numLagrangeDers, lagrangeDersOut.size());
    for(unsigned j=0; j<numLagrangeDers; j++){
      BOOST_CHECK_CLOSE(lagrangeDersOut[j], lagrangeDersCompare[j], THRESHOLD);
    }
    
    std::vector<double> adjointsOut;
    imsmd2nd.getAdjoints(adjointsOut);
    BOOST_CHECK_EQUAL(numAdjoints, adjointsOut.size());
    for(unsigned j=0; j<numAdjoints; j++){
      BOOST_CHECK_CLOSE(adjointsOut[j], adjoints[j], THRESHOLD);
    }
  }
}

/**
* @test IntegratorMetaData2ndOrderTest - test function clearActiveParameters
*/
BOOST_AUTO_TEST_CASE(TestClearActiveParameters)
{
  //use SensitivityTestMSSE because of functions setParameters adn getParameters
  TestIntegratorSSMD2nd imd;
  
  const unsigned numParameters = 5;
  std::vector<Parameter::Ptr> currentParameters(numParameters);
  
  imd.setParameters(currentParameters);
  
  std::vector<Parameter::Ptr> storedParameters;
  imd.getParameters(storedParameters);
  BOOST_CHECK_EQUAL(storedParameters.size(), numParameters);
  
  //now delete the current parameters vector
  imd.clearActiveParameters();
  
  imd.getParameters(storedParameters);
  BOOST_CHECK(storedParameters.empty());
}

BOOST_AUTO_TEST_SUITE_END()
