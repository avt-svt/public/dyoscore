/** 
* @file IntegratorMetaData2ndOrderDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData - Part of DyOS                                              \n
* =====================================================================\n
* Dummyclasses for IntegratorMetaData2ndOrder testsuite                \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 30.07.2012
*/
#pragma once
#include "MetaDataTestDummies.hpp"
#include "IntegratorMultiStageMetaData.hpp"

class TestIntegratorMSMD2ndOrder : public virtual IntegratorMSMetaData2ndOrder
{
public:
  TestIntegratorMSMD2ndOrder(std::vector<Mapping::Ptr> mappings,
                             std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages)
                             :IntegratorMSMetaData2ndOrder(mappings, stages){}
  
  void setStage(const unsigned stageIndex)
  {
    m_currentStageIndex = stageIndex;
  }
};

class TestIntegratorSSMD2nd : public virtual IntegratorSSMetaData2ndOrder
{
public:
  TestIntegratorSSMD2nd(const GenericEso2ndOrder::Ptr &genericEso2ndOrder,
                        const std::vector<Control> &controls,
                        const std::vector<InitialValueParameter::Ptr> &initials,
                        const DurationParameter::Ptr &globalDuration,
                        const std::vector<double> &additionalGrid)
                         :IntegratorSSMetaData2ndOrder(genericEso2ndOrder, controls, initials,
                                                       globalDuration, additionalGrid){}
  
  TestIntegratorSSMD2nd(){}
  
  /**
  * @brief setter for the attribute m_currentParamsWithSens
  *
  * This function is not in the original class. It is used to override the said
  * attribute for testing purpose.
  * @param params vector of Parameter pointer to be used for calculating RHS 
  *         sensitivities
  */
  void setParameters(const vector<Parameter::Ptr> &params)
  {
    m_currentParamsWithSens.assign(params.begin(), params.end());
  }

  /**
  * @brief getter for the attribute m_currentParamsWithSens
  *
  * This function is not in the original class. It is used to check the said
  * attribute for testing purpose.
  * @param params vector receiving the Parameters
  */
  void getParameters(vector<Parameter::Ptr> &params)
  {
    params.assign(m_currentParamsWithSens.begin(), 
                  m_currentParamsWithSens.end());
  }
};