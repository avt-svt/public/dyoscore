/** 
* @file OptimizerMultiStageMetaDataTestDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData - Part of DyOS                                              \n
* =====================================================================\n
* Dummyclasses for OptimizerMultiStageMetaData testsuite               \n
* =====================================================================\n
* @author Kathrin Frankl
* @date 04.04.2012
*/
#pragma once


class LagrangeTestIntegratorMetaData : public IntegratorSingleStageMetaData
{
protected:
  std::vector<double> m_lagrangeMultipliers;
public:
  void setLagrangeMultipliers(const utils::Array<double> &lagrangeMultipliers){
    m_lagrangeMultipliers.clear();
    for(unsigned i=0; i<lagrangeMultipliers.getSize(); i++){
      m_lagrangeMultipliers.push_back(lagrangeMultipliers[i]);
    }
  }
};

/**
* @class MSTest1_OptimizerSingleStageMetaData
* @brief dummy implementation of abstract class OptimizerSingleStageMetaData; used for test modules 
*
* this class is used to create an array of OptimizerSingleStageMetaData of length 3. This class represents stage 1.
*/
class MSTest1_OptimizerSingleStageMetaData : public OptimizerSingleStageMetaData
{
public:

  std::vector<double> decVarValues;

  /**
  * @brief generate a new constructor
  * @param optProbDim OptimizerProblemDimensions used for the construction
  */
  MSTest1_OptimizerSingleStageMetaData(const OptimizerProblemDimensions &optProbDim){
      m_optProbDim = optProbDim;
      m_integratorMetaData = IntegratorSingleStageMetaData::Ptr(new LagrangeTestIntegratorMetaData());
    }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setConstraints
  * @param constraints vector of StatePointConstraint to set the constraints of the optimization problem
  */
  virtual void setConstraints(const std::vector<StatePointConstraint> &constraints){
        m_constraints = constraints;
  };
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setObjective
  * @param objective StatePointConstraint to set the objective of the optimization problem
  */
  virtual void setObjective(const StatePointConstraint &objective){
        m_objective = objective;
  };
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinObjDerivative
  * @param[out] values vector of length 2 containing two double values
  */
  virtual void getNonLinObjDerivative(utils::Array<double> &values) const
{
      values[0] = 2.0;
      values[1] = 3.0;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinObjValue
  * @return double value of the nonlinear objective value
  */
  virtual double getNonLinObjValue() const
  {
    return 2.1;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinConstraintValues
  * @param[out] values vector of length 2 containing two double values
  */
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const
  {
    values[0] = 1.1;
    values[1] = 1.2;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getDecVarBounds
  * @param[out] upperBounds vector of length 2 containing the values of the upper bound of the decision variables
  * @param[out] lowerBounds vector of length 2 containing the values of the lower bound of the decision variables
  */
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const
  {
    upperBounds[0] = 11.1;
    upperBounds[1] = 22.2;
    lowerBounds[0] = -11.1;
    lowerBounds[1] = -22.2;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getDecVarValues
  * @param[out] values vector containing the values of the local variable decVarValues
  */
  virtual void getDecVarValues(utils::Array<double> &values) const
  {
    for (unsigned z=0; z<values.getSize(); z++)
    {
      values[z] = decVarValues[z];
    }
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setDecVarValues
  * @param[out] decVars vector containing the values for the local variable decVarValues
  */
  virtual void setDecVarValues(const utils::Array<const double> &decVars)
  {
    decVarValues.resize(decVars.getSize());
    for(unsigned i=0; i<decVars.getSize(); i++)
    {
      decVarValues[i] = decVars[i];
    }
  }

  virtual void setLinLagrangeMultipliers(const std::vector<double> &mult)
  {
    assert(mult.size() == m_optProbDim.numLinConstraints);
    m_linConLagrangeMultiplier.assign(mult.begin(), mult.end());
  }
  
  virtual void getDecisionVariableIndices(std::vector<unsigned> &decVarInd) const
  {
    decVarInd.resize(decVarValues.size());
    for(unsigned i=0; i<decVarValues.size(); i++){
      decVarInd[i] = i;
    }
  }
};

/**
* @class MSTest2_OptimizerSingleStageMetaData
* @brief dummy implementation of abstract class OptimizerSingleStageMetaData; used for test modules 
*
* this class is used to create an array of OptimizerSingleStageMetaData of length 3. This class represents stage 2.
*/
class MSTest2_OptimizerSingleStageMetaData : public OptimizerSingleStageMetaData
{
public:

  std::vector<double> decVarValues;

  /**
  * @brief generate a new constructor
  * @param optProbDim OptimizerProblemDimensions used for the construction
  */
  MSTest2_OptimizerSingleStageMetaData(const OptimizerProblemDimensions &optProbDim){
      m_optProbDim = optProbDim;
      m_integratorMetaData = IntegratorSingleStageMetaData::Ptr(new LagrangeTestIntegratorMetaData());
    }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setConstraints
  * @param constraints vector of StatePointConstraint to set the constraints of the optimization problem
  */
  virtual void setConstraints(const std::vector<StatePointConstraint> &constraints){
        m_constraints = constraints;
  };
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setObjective
  * @param objective StatePointConstraint to set the objective of the optimization problem
  */
  virtual void setObjective(const StatePointConstraint &objective){
        m_objective = objective;
  };
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinObjValue
  * @return double value of the nonlinear objective value
  */
  virtual double getNonLinObjValue() const
  {
    return 2.2;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinConstraintValues
  * @param[out] values vector of length 2 containing two double values
  */
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const
  {
    values[0] = 2.1;
    values[1] = 2.2;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getDecVarBounds
  * @param[out] upperBounds vector of length 1 containing the value of the upper bound of the decision variable
  * @param[out] lowerBounds vector of length 1 containing the value of the lower bound of the decision variable
  */
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const
  {
    upperBounds[0] = 33.3;
    lowerBounds[0] = -33.3;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getDecVarValues
  * @param[out] values vector containing the values of the local variable decVarValues
  */
  virtual void getDecVarValues(utils::Array<double> &values) const
  {
    for (unsigned z=0; z<values.getSize(); z++)
    {
      values[z] = decVarValues[z];
    }
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setDecVarValues
  * @param[out] decVars vector containing the values for the local variable decVarValues
  */
  virtual void setDecVarValues(const utils::Array<const double> &decVars)
  {
    decVarValues.resize(decVars.getSize());
    for(unsigned i=0; i<decVars.getSize(); i++)
    {
      decVarValues[i] = decVars[i];
    }
  }
  
  virtual void setLinLagrangeMultipliers(const std::vector<double> &mult)
  {
    assert(mult.size() == m_optProbDim.numLinConstraints);
    m_linConLagrangeMultiplier.assign(mult.begin(), mult.end());
  }
  
  virtual void getDecisionVariableIndices(std::vector<unsigned> &decVarInd) const
  {
    decVarInd.resize(decVarValues.size());
    for(unsigned i=0; i<decVarValues.size(); i++){
      decVarInd[i] = i;
    }
  }
};

/**
* @class MSTest3_OptimizerSingleStageMetaData
* @brief dummy implementation of abstract class OptimizerSingleStageMetaData; used for test modules 
*
* this class is used to create an array of OptimizerSingleStageMetaData of length 3. This class represents stage 3.
*/
class MSTest3_OptimizerSingleStageMetaData : public OptimizerSingleStageMetaData
{
public:

  std::vector<double> decVarValues;

  /**
  * @brief generate a new constructor
  * @param optProbDim OptimizerProblemDimensions used for the construction
  */
  MSTest3_OptimizerSingleStageMetaData(const OptimizerProblemDimensions &optProbDim){
      m_optProbDim = optProbDim;
      m_integratorMetaData = IntegratorSingleStageMetaData::Ptr(new LagrangeTestIntegratorMetaData());
    }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setConstraints
  * @param constraints vector of StatePointConstraint to set the constraints of the optimization problem
  */
  virtual void setConstraints(const std::vector<StatePointConstraint> &constraints){
        m_constraints = constraints;
  };
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setObjective
  * @param objective StatePointConstraint to set the objective of the optimization problem
  */
  virtual void setObjective(const StatePointConstraint &objective){
        m_objective = objective;
  };
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinObjDerivative
  * @param[out] values vector of length 1 containing one double values
  */
  virtual void getNonLinObjDerivative(utils::Array<double> &values) const
{
      values[0] = 5.2;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinObjValue
  * @return double value of the nonlinear objective value
  */
  virtual double getNonLinObjValue() const
  {
    return 2.3;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getNonLinConstraintValues
  * @param[out] values vector of length 2 containing two double values
  */
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const
  {
    values[0] = 3.1;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getDecVarBounds
  * @param[out] upperBounds vector of length 1 containing the value of the upper bound of the decision variable
  * @param[out] lowerBounds vector of length 1 containing the value of the lower bound of the decision variable
  */
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const
  {
    upperBounds[0] = 44.4;
    lowerBounds[0] = -44.4;
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::getDecVarValues
  * @param[out] values vector containing the values of the local variable decVarValues
  */
  virtual void getDecVarValues(utils::Array<double> &values) const
  {
    for (unsigned z=0; z<values.getSize(); z++)
    {
      values[z] = decVarValues[z];
    }
  }
  /**
  * @brief overwrite the function OptimizerSingleStageMetaData::setDecVarValues
  * @param[out] decVars vector containing the values for the local variable decVarValues
  */
  virtual void setDecVarValues(const utils::Array<const double> &decVars)
  {
    decVarValues.resize(decVars.getSize());
    for(unsigned i=0; i<decVars.getSize(); i++)
    {
      decVarValues[i] = decVars[i];
    }
  }
  
  virtual void setLinLagrangeMultipliers(const std::vector<double> &mult)
  {
    assert(mult.size() == m_optProbDim.numLinConstraints);
    m_linConLagrangeMultiplier.assign(mult.begin(), mult.end());
  }
  
  virtual void getDecisionVariableIndices(std::vector<unsigned> &decVarInd) const
  {
    decVarInd.resize(decVarValues.size());
    for(unsigned i=0; i<decVarValues.size(); i++){
      decVarInd[i] = i;
    }
  }
};

class MSTest_Mapping : public DummyMapping
{
public:
  virtual bool isDecoupled(){return false;}
};

/**
* @class MSTest_IntegratorMultiStageMetaData
* @brief dummy implementation of abstract class IntegratorMultiStageMetaData; used for test modules 
*/
class MSTest_IntegratorMultiStageMetaData : public IntegratorMultiStageMetaData
{
protected:
  
public:
  MSTest_IntegratorMultiStageMetaData()
  {
    unsigned numStages = getNumStages();
    Mapping::Ptr dummyMapping(new MSTest_Mapping());
    m_mapping.resize(numStages-1);
    for(unsigned i=0; i<m_mapping.size(); i++){
      m_mapping[i] = dummyMapping;
    }
  }

  /**
  * @brief overwrite the function IntegratorMultiStageMetaData::getNumStages
  * @return positive integer value defining number of stages
  */
  virtual unsigned getNumStages() const{
     return 3;
  }
  
  /**
  * @brief overwrite the function IntegratorMultiStageMetaData::getIntOptDataStages
  * @return vector of IntOptDataMap of length 3 refering to IntegratorSingleStageMetaData
  */
  std::vector<IntOptDataMap> getIntOptDataStages()
  {
    std::vector<IntOptDataMap> ssOptData(3);

    // insert entries in ssOptData[0]
    SavedOptDataStruct newTimePoint1;
    newTimePoint1.nonLinConstVarIndices.push_back(0);
    newTimePoint1.activeParameterIndices.push_back(0);
    newTimePoint1.activeParameterIndices.push_back(1);
    newTimePoint1.activeDecVarIndices.push_back(0);
    newTimePoint1.activeDecVarIndices.push_back(1);
    newTimePoint1.nonLinConstIndices.push_back(0);
    newTimePoint1.nonLinConstGradients.append(1.5);
    newTimePoint1.nonLinConstGradients.append(2.5);
    newTimePoint1.numSensParam = 2;
    ssOptData[0].insert(pair<unsigned,SavedOptDataStruct>(0, newTimePoint1));
    
    SavedOptDataStruct newTimePoint2;
    newTimePoint2.nonLinConstVarIndices.push_back(1);
    newTimePoint2.nonLinConstVarIndices.push_back(2);
    newTimePoint2.activeParameterIndices.push_back(0);
    newTimePoint2.activeParameterIndices.push_back(1);
    newTimePoint2.activeDecVarIndices.push_back(0);
    newTimePoint2.activeDecVarIndices.push_back(1);
    newTimePoint2.nonLinConstIndices.push_back(1);
    newTimePoint2.nonLinConstIndices.push_back(2);
    newTimePoint2.nonLinConstGradients.append(1.7);
    newTimePoint2.nonLinConstGradients.append(2.7);
    newTimePoint2.nonLinConstGradients.append(2.0);
    newTimePoint2.nonLinConstGradients.append(3.0);
    newTimePoint2.numSensParam = 2;
    ssOptData[0].insert(pair<unsigned,SavedOptDataStruct>(1, newTimePoint2));
    
    // insert entries in ssOptData[1]
    SavedOptDataStruct newTimePoint3;
    newTimePoint3.nonLinConstVarIndices.push_back(0);
    newTimePoint3.nonLinConstVarIndices.push_back(1);
    newTimePoint3.activeParameterIndices.push_back(0);
    newTimePoint3.activeDecVarIndices.push_back(0);
    newTimePoint3.nonLinConstIndices.push_back(0);
    newTimePoint3.nonLinConstIndices.push_back(1);
    newTimePoint3.nonLinConstGradients.append(4.1);
    newTimePoint3.nonLinConstGradients.append(4.2);
    newTimePoint3.numSensParam = 1;
    ssOptData[1].insert(pair<unsigned,SavedOptDataStruct>(0, newTimePoint3));
    
    // insert entries in ssOptData[2]
    SavedOptDataStruct newTimePoint4;
    newTimePoint4.nonLinConstVarIndices.push_back(0);
    newTimePoint4.activeParameterIndices.push_back(0);
    newTimePoint4.activeDecVarIndices.push_back(0);
    newTimePoint4.nonLinConstIndices.push_back(0);
    newTimePoint4.nonLinConstGradients.append(5.1);
    newTimePoint4.numSensParam = 1;
    ssOptData[2].insert(pair<unsigned,SavedOptDataStruct>(0, newTimePoint4));
    
    SavedOptDataStruct newTimePoint5;
    newTimePoint5.nonLinConstVarIndices.push_back(1);
    newTimePoint5.activeParameterIndices.push_back(0);
    newTimePoint5.activeDecVarIndices.push_back(0);
    newTimePoint5.nonLinConstIndices.push_back(1);
    newTimePoint5.nonLinConstGradients.append(5.2);
    newTimePoint5.numSensParam = 1;
    ssOptData[2].insert(pair<unsigned,SavedOptDataStruct>(1, newTimePoint5));
    
    return ssOptData;
    
  };
  
  /**
  * @brief overwrite the function IntegratorMultiStageMetaData::getIntOptData
  * @return vector of IntOptDataMap of length 3 containing multistage information
  */
  std::vector<IntOptDataMap> getIntOptData() const
  {
    std::vector<IntOptDataMap> msOptData(3);
    
    // insert entries in msOptData[1]
    SavedOptDataStruct newTimePoint6;
    newTimePoint6.nonLinConstVarIndices.push_back(0);
    newTimePoint6.nonLinConstVarIndices.push_back(1);
    newTimePoint6.activeParameterIndices.push_back(0);
    newTimePoint6.activeParameterIndices.push_back(1);
    newTimePoint6.activeDecVarIndices.push_back(0);
    newTimePoint6.activeDecVarIndices.push_back(1);
    newTimePoint6.nonLinConstIndices.push_back(0);
    newTimePoint6.nonLinConstIndices.push_back(1);
    newTimePoint6.nonLinConstGradients.append(2.2);
    newTimePoint6.nonLinConstGradients.append(2.3);
    newTimePoint6.nonLinConstGradients.append(2.4);
    newTimePoint6.nonLinConstGradients.append(2.5);
    newTimePoint6.numSensParam = 2;
    msOptData[1].insert(pair<unsigned,SavedOptDataStruct>(2, newTimePoint6));
        
    // insert entries in msOptData[2]
    SavedOptDataStruct newTimePoint7;
    newTimePoint7.nonLinConstVarIndices.push_back(0);
    //two points of the first stage (indices 0 and 1)
    newTimePoint7.activeParameterIndices.push_back(0);
    newTimePoint7.activeParameterIndices.push_back(1);
    //point of the second stage (index 0 + numParameters of first stage)
    newTimePoint7.activeParameterIndices.push_back(2);
    newTimePoint7.activeDecVarIndices.push_back(0);
    newTimePoint7.activeDecVarIndices.push_back(1);
    //point of the second stage (index 0 + numParameters of first stage)
    newTimePoint7.activeDecVarIndices.push_back(2);
    newTimePoint7.nonLinConstIndices.push_back(0);
    newTimePoint7.nonLinConstGradients.append(3.1);
    newTimePoint7.nonLinConstGradients.append(3.2);
    newTimePoint7.nonLinConstGradients.append(3.3);
    newTimePoint7.numSensParam = 3;
    msOptData[2].insert(pair<unsigned,SavedOptDataStruct>(2, newTimePoint7));
    
    SavedOptDataStruct newTimePoint8;
    newTimePoint8.nonLinConstVarIndices.push_back(1);
    //two points of the first stage (indices 0 and 1)
    newTimePoint8.activeParameterIndices.push_back(0);
    newTimePoint8.activeParameterIndices.push_back(1);
    //point of the second stage (index 0 + numParameters of first stage)
    newTimePoint8.activeParameterIndices.push_back(2);
    newTimePoint8.activeDecVarIndices.push_back(0);
    newTimePoint8.activeDecVarIndices.push_back(1);
    //point of the second stage (index 0 + numParameters of first stage)
    newTimePoint8.activeDecVarIndices.push_back(2);
    newTimePoint8.nonLinConstIndices.push_back(1);
    newTimePoint8.nonLinConstGradients.append(3.11);
    newTimePoint8.nonLinConstGradients.append(3.22);
    newTimePoint8.nonLinConstGradients.append(3.33);
    newTimePoint8.numSensParam = 3;
    msOptData[2].insert(pair<unsigned,SavedOptDataStruct>(3, newTimePoint8));
    
    return msOptData;
    
  };
};

/** @brief construct a test OptimizerMultiStageMetaData object 
*   @return a OptimizerMultiStageMetaData object consisting of 3 stages
*/
OptimizerMultiStageMetaData createMSMDdummy()
{
  OptimizerProblemDimensions optProbDim_s1, optProbDim_s2, optProbDim_s3;
  optProbDim_s1.numOptVars = 2;
  optProbDim_s2.numOptVars = 1;
  optProbDim_s3.numOptVars = 1;
  optProbDim_s1.numConstParams = 0;
  optProbDim_s2.numConstParams = 0;
  optProbDim_s3.numConstParams = 0;
  optProbDim_s1.numLinConstraints = 0;
  optProbDim_s2.numLinConstraints = 1;
  optProbDim_s3.numLinConstraints = 1;
  optProbDim_s1.numNonLinConstraints = 2;
  optProbDim_s2.numNonLinConstraints = 2;
  optProbDim_s3.numNonLinConstraints = 1;
  optProbDim_s1.nonLinObjIndex = 2;
  optProbDim_s2.nonLinObjIndex = 2;
  optProbDim_s3.nonLinObjIndex = 1;
  
  CsTripletMatrix::Ptr A, B, C;
  OptimizerSingleStageMetaData::constructFullCooMatrix(A, 2, 2);
  OptimizerSingleStageMetaData::constructFullCooMatrix(B, 2, 1);
  OptimizerSingleStageMetaData::constructFullCooMatrix(C, 1, 1);
  
  optProbDim_s1.nonLinJacDecVar = A;
  optProbDim_s2.nonLinJacDecVar = B;
  optProbDim_s3.nonLinJacDecVar = C;
  int * indicesSpace;
  double * valuesSpace;
  indicesSpace = new int;
  valuesSpace = new double;
  CsTripletMatrix::Ptr linJacStage1(new CsTripletMatrix(0, 1, 0, indicesSpace, indicesSpace, valuesSpace));
  optProbDim_s1.linJac = linJacStage1;
  

  CsTripletMatrix::Ptr linJacStage2 (new CsTripletMatrix(1, 1, 1, indicesSpace, indicesSpace, valuesSpace));
  optProbDim_s2.linJac = linJacStage2;
  
  CsTripletMatrix::Ptr linJacStage3 (new CsTripletMatrix(1, 1, 1, indicesSpace, indicesSpace, valuesSpace));
  optProbDim_s3.linJac = linJacStage3;
                                  
  delete indicesSpace;
  delete valuesSpace;

  MSTest_IntegratorMultiStageMetaData *msmdIntegrator = new MSTest_IntegratorMultiStageMetaData();
  IntegratorMultiStageMetaData::Ptr msmdIntPtr(msmdIntegrator);
    
  std::vector<StatePointConstraint> constraints_s1(2), constraints_s2(2), constraints_s3(1);
  StatePointConstraint objective_s1, objective_s2, objective_s3;
  
  constraints_s1[0].setEsoIndex(0);
  constraints_s1[1].setEsoIndex(1);
  constraints_s1[0].setTime(0.0);
  constraints_s1[1].setTime(1.0);
  constraints_s2[0].setEsoIndex(0);
  constraints_s2[1].setEsoIndex(1);
  constraints_s2[0].setTime(0.0);
  constraints_s2[1].setTime(1.0);
  constraints_s3[0].setEsoIndex(0);
  constraints_s3[0].setTime(0.0);
  
  constraints_s1[0].setLowerBound(1.1);
  constraints_s1[1].setLowerBound(1.2);
  constraints_s2[0].setLowerBound(2.1);
  constraints_s2[1].setLowerBound(2.2);
  constraints_s3[0].setLowerBound(3.1);
  constraints_s1[0].setUpperBound(11.1);
  constraints_s1[1].setUpperBound(11.2);
  constraints_s2[0].setUpperBound(21.1);
  constraints_s2[1].setUpperBound(21.2);
  constraints_s3[0].setUpperBound(31.1);
  
  constraints_s1[0].setLagrangeMultiplier(1.1);
  constraints_s1[1].setLagrangeMultiplier(2.2);
  constraints_s2[0].setLagrangeMultiplier(3.3);
  constraints_s2[1].setLagrangeMultiplier(4.4);
  constraints_s3[0].setLagrangeMultiplier(5.5);
  
  objective_s1.setEsoIndex(2);
  objective_s1.setTime(1.0);
  objective_s2.setEsoIndex(2);
  objective_s2.setTime(1.0);
  objective_s3.setEsoIndex(1);
  objective_s3.setTime(1.0);
  
  MSTest1_OptimizerSingleStageMetaData *optSS_s1 = new MSTest1_OptimizerSingleStageMetaData(optProbDim_s1);
  OptimizerSingleStageMetaData::Ptr optSSMDVec_s1(optSS_s1);
  optSSMDVec_s1->setConstraints(constraints_s1);
  optSSMDVec_s1->setObjective(objective_s1);
  optSS_s1->decVarValues.resize(2);
  optSS_s1->decVarValues[0] = 3;
  optSS_s1->decVarValues[1] = 3;
  
  MSTest2_OptimizerSingleStageMetaData *optSS_s2 = new MSTest2_OptimizerSingleStageMetaData(optProbDim_s2);
  //set lagrange multiplier for linear constraint
  std::vector<double> linMultS2(1, 6.6);
  optSS_s2->setLinLagrangeMultipliers(linMultS2);
  OptimizerSingleStageMetaData::Ptr optSSMDVec_s2(optSS_s2);
  optSSMDVec_s2->setConstraints(constraints_s2);
  optSSMDVec_s2->setObjective(objective_s2);
  optSS_s2->decVarValues.resize(1);
  optSS_s2->decVarValues[0] = 3;
  
  MSTest3_OptimizerSingleStageMetaData *optSS_s3 = new MSTest3_OptimizerSingleStageMetaData(optProbDim_s3);
  //set lagrange multiplier for linear constraint
  std::vector<double> linMultS3(1, 7.7);
  optSS_s3->setLinLagrangeMultipliers(linMultS3);
  OptimizerSingleStageMetaData::Ptr optSSMDVec_s3(optSS_s3);
  optSSMDVec_s3->setConstraints(constraints_s3);
  optSSMDVec_s3->setObjective(objective_s3);
  optSS_s3->decVarValues.resize(1);
  optSS_s3->decVarValues[0] = 3;
  
  
  
  std::vector<OptimizerSingleStageMetaData::Ptr> optSSMDVec(3);
  optSSMDVec[0] = optSSMDVec_s1;
  optSSMDVec[1] = optSSMDVec_s2;
  optSSMDVec[2] = optSSMDVec_s3;
  
  std::vector<bool> treatObjective(3);
  treatObjective[0] = true;
  treatObjective[1] = false;
  treatObjective[2] = true;
  
  OptimizerMultiStageMetaData optmsmd(optSSMDVec, 
                              msmdIntPtr,
                              treatObjective);

  return optmsmd;
}