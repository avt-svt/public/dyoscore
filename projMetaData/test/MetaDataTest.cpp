/** 
* @file MetaDataTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Single Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* contains testsuites for module MetaSingleStageEso                    \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 25.8.2011
*/

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE MetaSingleStageEso

#include "boost/test/unit_test.hpp"
#include "boost/test/floating_point_comparison.hpp"


#include "IntegratorSingleStageMetaDataTestsuite.hpp"
#include "IntegratorMultiStageMetaDataTestsuite.hpp"
#include "ParameterTestsuite.hpp"
#include "ControlTestsuite.hpp"
#include "TimeInvariantParameterTestsuite.hpp"
#include "OptimizerInterfaceTestsuite.hpp"
#include "OptimizerMultiStageMetaDataTestsuite.hpp"
#include "MappingTestsuite.hpp"
#include "IntegratorMetaData2ndOrderTestsuite.hpp"



