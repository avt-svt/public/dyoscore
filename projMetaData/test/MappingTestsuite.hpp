/** 
* @file Mapping.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mapping class test - Part MetaSingleStageESO in DyOS                 \n
* =====================================================================\n
* =====================================================================\n
* @author Fady Assassa, Tjalf Hoffmann
* @date 27.03.2012
*/
#include "boost/test/floating_point_comparison.hpp"

#include <vector>
#include "Mapping.hpp"
#include "MetaDataTestDummies.hpp"
#include "IntegratorSingleStageMetaData.hpp"

//! @brief shared pointer to an IntegratorMetaData object
//typedef boost::shared_ptr<IntegratorSingleStageMetaData> IntegratorSSMetaDataPtr;

//! threshold for double comparison testmacro BOOST_CHECK_CLOSE to pass
#define BOOST_TOL 1.0e-10

BOOST_AUTO_TEST_SUITE(MappingClassTest)

/**
* @test MappingTest - tests the functionality of applyStageMapping for single shooting
*/
BOOST_AUTO_TEST_CASE(TestFullStateMapping)
{
  // first we map the stage 1 to stage 2. Next, we map stage 2 to stage 3.
  IntegratorMetaDataDummyForMapping* stage1 = new IntegratorMetaDataDummyForMapping();
  IntegratorMetaDataDummyForMapping* stage2 = new IntegratorMetaDataDummyForMapping();
  IntegratorMetaDataDummyForMapping* stage3 = new IntegratorMetaDataDummyForMapping();
  IntegratorSingleStageMetaData::Ptr MetaDataStage1(stage1);
  IntegratorSingleStageMetaData::Ptr MetaDataStage2(stage2);
  IntegratorSingleStageMetaData::Ptr MetaDataStage3(stage3);

  // mapping class employing single shooting between stage 1 and stage 2
  SingleShootingMapping mapStages12;
  // mapping class employing single shooting between stage 2 and stage 3
  SingleShootingMapping mapStages23;

  // number of variables for all models is the same
  const EsoIndex numVars = MetaDataStage1->getGenericEso()->getNumVariables();
  // vector that contains the sensitivity information of the multi stage. This should be empty, 
  // since we are switching from stage 1 to stage 2. In stage 2 it should be filled.
  std::vector<double> multiSens;
  utils::Array<double> varVals(numVars);

  // vectors containing the mapping information from stage 1 to stage 2.
  const unsigned numMaps = numVars;
  iiMap esoIndicesMap, eqnIndicesMap;
  vector<int> esoIndices(numVars);
  for(unsigned i=0; i<numMaps; i++){
    // this is full state mapping.
    esoIndices[i] = i;
    esoIndicesMap.insert(pair<unsigned, unsigned> (esoIndices[i],i));
    eqnIndicesMap.insert(pair<unsigned, unsigned> (i,i));
    varVals[i] = (i+1)*(i+1);
  }
  // set the mapping in the class
  mapStages12.setIndexMaps(esoIndicesMap, eqnIndicesMap);

  // change the values in the eso of the first stage since the values by default are already the same
  stage1->getGenericEso()->setVariableValues(esoIndices.size(), varVals.getData(), &esoIndices[0]);

  // apply stage mapping. We assume we are in Stage 2. Thus Stage 1 needs to be mapped to Stage 2.
  // mapped values should have been set into initial value parameters
  mapStages12.applyStageMapping(MetaDataStage1, MetaDataStage2, multiSens);

  int numParams = MetaDataStage1->getNumCurrentSensitivityParameters();
  int numSens = numVars * numParams;
  // check if the sensitivity vector has the correct size and values
  BOOST_CHECK_EQUAL(numSens, multiSens.size());
  for(int i=0; i<numSens; i++){
    BOOST_CHECK_CLOSE(multiSens[i], double(i), BOOST_TOL);
  }
  
  // check if the variables in the eso carry the correct values
  std::vector<InitialValueParameter::Ptr> initials;
  // ignore algebraic variables from varVals, since those are ignored during stage mapping
  const unsigned numAlgVars = stage2->getGenericEso()->getNumAlgebraicVariables();
  const unsigned numDiffVars = stage2->getGenericEso()->getNumDifferentialVariables();
  std::vector<double> result(numDiffVars);
  result.front() = varVals[0];
  for(unsigned i=1; i<numDiffVars; i++){
    result[i] = varVals[i+numAlgVars];
  }
  
  stage2->getInitials(initials);
  for(int i=0; i<int(initials.size()); i++){
    BOOST_CHECK_CLOSE(initials[i]->getParameterValue(), result[i], BOOST_TOL);
  }
  
  // now we assume we are in stage 3 and want to map stage 2 into stage 3. In contrast to the
  // the previous case, we have multi stage sensitivities
  // set the mapping in the class
  mapStages23.setIndexMaps(esoIndicesMap, eqnIndicesMap);
  mapStages23.applyStageMapping(MetaDataStage2, MetaDataStage3, multiSens);

  // the amount of sensitivities should have doubled. we have twice the same set of values.
  numParams += MetaDataStage2->getNumCurrentSensitivityParameters();
  numSens = numVars * numParams;
  BOOST_CHECK_EQUAL(numSens, multiSens.size());
  for(int i=0; i<numSens; i++){
    if(i<numSens/2)
      BOOST_CHECK_CLOSE(multiSens[i], double(i), BOOST_TOL);
    else
      BOOST_CHECK_CLOSE(multiSens[i], double(i-numSens/2), BOOST_TOL);
  }
}

BOOST_AUTO_TEST_SUITE_END()
