/** 
* @file Control.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Control class test - Part MetaSingleStageESO in DyOS                 \n
* =====================================================================\n
* Test for Control and ParameterizationGrid                            \n
* =====================================================================\n
* @author Fady Assassa
* @date 26.10.2011
*/

#include "Control.hpp"



#include <vector>

BOOST_AUTO_TEST_SUITE(ControlTest)

/** @brief construct a test control object 
*   @param lower is the lower bound of the control
*   @param upper is the upper bound of the control
*   @param esoIndex is the ESO index of the control
*   @return a control with the bounds and index set by the user
*/
Control createControlObject(const double lower, const double upper, const int esoIndex)
{
  Control con1;
  const double lowerBound = lower;
  const double upperBound = upper;
  con1.setLowerBound(lowerBound); con1.setUpperBound(upperBound); 
  con1.setEsoIndex(esoIndex);
  return con1;
}
/** @brief prepares controls and fills reasonable entries in the grid
*   @param numIntervals number of intervals of Grid1
*   @param numIntervals2 number of intervals of Grid2 and Grid3
    @param numControlStructure is the number of Grids that are assigend to con1. it has to be 2
*   @param Grid1 is already initialized with a Grid of size numIntervals
*   @param Grid2 is already initialized with a Grid of size numIntervals2
*   @param Grid3 is already initialized with a Grid of size numIntervals2
*   @param values1 is already initialized with a size numIntervals
*   @param values2 is already initialized with a size numIntervals2
*   @param durations is already initialized with a size of numControlStructure
*   @param con1 is already created using createControlObject. It will have 2 Grids.
*   @param con2 is already created using createControlObject. It will have 1 Grid.
*   @param globalDuration is the pointer to the last final time
*/
void prepareControls(const int numIntervals,
                     const int numIntervals2,
                     const int numControlStructure,
                     ParameterizationGrid::Ptr &Grid1,
                     ParameterizationGrid::Ptr &Grid2,
                     ParameterizationGrid::Ptr &Grid3,
                     vector<double>& values1,
                     vector<double>& values2,
                     vector<double>& durations,
                     Control& con1,
                     Control& con2,
                     DurationParameter::Ptr &globalDuration)
{
  assert(numControlStructure == 2);
  assert(values1.size() == (unsigned)numIntervals);
  assert(values2.size() == (unsigned)numIntervals2);

  for(int i=0; i<numIntervals; i++){
    values1[i] = i+i+1; // random numbers
  }
  for(int i=0; i<numIntervals2; i++){
    values2[i] = i*5+1; // random numbers
  }
  if(Grid1->getControlType() == PieceWiseLinear){
    values1.push_back(numIntervals*2+1);
  }

  if(Grid2->getControlType() == PieceWiseLinear){
    values2.push_back(numIntervals2*5+1);
  }
  // assign values to the grids
  Grid1->setAllParameterizationValues(values1);
  Grid2->setAllParameterizationValues(values2);


  vector<ParameterizationGrid::Ptr> completeGrid(numControlStructure);
  // merge grid to one Grid
  completeGrid[0] = Grid1;
  completeGrid[1] = Grid2;

  durations[0] = 1;
  durations[1] = 3;
  globalDuration->setParameterValue(durations[0] + durations[1]);
  // prepare first control
  Grid1->setDurationValue(durations[0]);
  Grid2->setDurationValue(durations[1]);
  con1.setControlGrid(completeGrid);

  // prepare second control
  vector<ParameterizationGrid::Ptr> completeGrid2(1);
  completeGrid2[0] = Grid3;
  Grid3->setDurationPointer(globalDuration);
  // get last final time pointer and override the one in completegrid 2
  con2.setControlGrid(completeGrid2);
}
/** @test ParameterizationGrid - Functionality test */
BOOST_AUTO_TEST_CASE(TestParameterizationGrid)
{
  const int numIntervals = 5;
  ParameterizationGrid* Grid = new ParameterizationGrid(numIntervals, PieceWiseConstant);
  // check control type
  BOOST_CHECK_EQUAL(Grid->getControlType(), PieceWiseConstant);
  BOOST_CHECK(Grid->getDurationPointer() == NULL);
  // initialize values
  
  std::vector<double> values(numIntervals);
  for(int i=0; i<numIntervals; i++){
    values[i] = i+i+1; // random values
  }
  // check if the values are the same
  Grid->setAllParameterizationValues(values);
  std::vector<ControlParameter::Ptr> conPara = Grid->getAllControlParameter();
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK_EQUAL(conPara[i]->getParameterValue(), values[i]);
  }
  // check if the grid is equidistant
  std::vector<double> eqGrid = Grid->getDiscretizationGrid();
  for(int i=0; i<(numIntervals+1); i++){
    double j = double(i);
    BOOST_CHECK_EQUAL(eqGrid[i], j/numIntervals);
  }
  // check functionality active parameter Index
  vector<unsigned> activeGridIndex;
  activeGridIndex.push_back(2);
  Grid->setActiveParameterIndex(activeGridIndex);
  BOOST_CHECK_EQUAL(Grid->getActiveParameterIndex()[0], activeGridIndex[0]);
  // get pointer of controls and check for activity and value
  std::vector<ControlParameter::Ptr> conParaPointer1 = Grid->getAllControlParameter();
  for(int i=0; i<numIntervals; i++){
    BOOST_CHECK(conParaPointer1[i]->getWithSensitivity());
    BOOST_CHECK_EQUAL(conParaPointer1[i]->getParameterValue(), values[i]);
  }
  Grid->setAllParametersWithoutSens();
  std::vector<ControlParameter::Ptr> conParaPointer2 = Grid->getAllControlParameter();
  for(int i=0; i<numIntervals; i++){
    bool sensActivity = conParaPointer2[i]->getWithSensitivity();
    BOOST_CHECK(!sensActivity);
  }
  // set final time. check if the pointer is created with the correct value
  const double durationValue = 1.3;
  Grid->setDurationValue(durationValue);
  DurationParameter::Ptr finTim1 = Grid->getDurationPointer();
  BOOST_CHECK_EQUAL(finTim1->getParameterValue(), durationValue);
  BOOST_CHECK_EQUAL(Grid->getDurationValue(), durationValue);
  // check if we really get the same pointer
  DurationParameter::Ptr finTim2 = Grid->getDurationPointer();
  BOOST_CHECK_EQUAL(finTim1.get(), finTim2.get());
 // create a copy of the Grid
  ParameterizationGrid::Ptr copyGrid(new ParameterizationGrid(Grid));
  // make sure that the new Grid is a deep copy of the old. This is done by checking the pointer of the Duration
  // A true copy has a different pointer address.
  const bool checkAddress = copyGrid->getDurationPointer() != Grid->getDurationPointer();
  BOOST_CHECK(checkAddress);
  delete Grid; 
  
}
/**
* @test ControlTest - test for originalParameterGrid hier
*/ 
BOOST_AUTO_TEST_CASE(originalParameterGrid)
{


}
/**
* @test ControlTest - test setter and getter of class Control
*/ 
BOOST_AUTO_TEST_CASE(SetterAndGetterOfControls)
{
  double lb, ub; int esoIndex;
  lb = -0.5; ub = 0.7; esoIndex = 3;
  Control con1 = createControlObject(lb, ub, esoIndex);
  BOOST_CHECK_EQUAL(con1.getLowerBound(), lb);
  BOOST_CHECK_EQUAL(con1.getUpperBound(), ub);
  BOOST_CHECK_EQUAL(con1.getEsoIndex(), esoIndex);

  //changed behaviour of function getParameterizationGrid - should throw an exception instead
  ParameterizationGrid::Ptr currentGrid = con1.getParameterizationGrid(0);
  BOOST_CHECK(currentGrid.get()==NULL);
  BOOST_CHECK_EQUAL(con1.getNumberOfGrids(), 0);
}

/**
* @test ControlTest - test function checkParameterChange
*/
BOOST_AUTO_TEST_CASE(TestCheckParameterChange)
{
  const double lowerBound = 0.0, upperBound = 1.0;
  const unsigned esoIndex = 5;
  Control c = createControlObject(lowerBound, upperBound, esoIndex);
  
  const unsigned numIntervals = 4;
  ParameterizationGrid::Ptr grid(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  const double durationValue = 8.4;
  grid->setDurationValue(durationValue);
  
  std::vector<ParameterizationGrid::Ptr> gridVector;
  gridVector.push_back(grid);
  c.setControlGrid(gridVector);
  
  // initial state of control should return true (no active parameter initialized
  BOOST_CHECK_EQUAL(c.checkParameterChange(0.0), true);
  
  c.initializeForFirstIntegration();
  BOOST_CHECK_EQUAL(c.checkParameterChange(0.0), false);
  
  //should be true again at time point durationValue/numIntervals
  const double epsilon = 0.02;
  const double timePoint = durationValue/numIntervals;
  BOOST_CHECK_EQUAL(c.checkParameterChange(timePoint - epsilon), false);
  BOOST_CHECK_EQUAL(c.checkParameterChange(timePoint), true);
  BOOST_CHECK_EQUAL(c.checkParameterChange(timePoint + epsilon), true);
}

/**
* @test ControlTest - test function getActiveDurationParameter
* 
* check whether a multigrid control returns the correct durationParameter
* for a given time.
*/
BOOST_AUTO_TEST_CASE(TestGetActiveDurationParameter)
{
  const double lowerBound = 0.0, upperBound = 1.0;
  const unsigned esoIndex = 5;
  Control c = createControlObject(lowerBound, upperBound, esoIndex);
  
  const unsigned numIntervals = 4;
  ParameterizationGrid::Ptr grid1(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  const double durationValue1 = 2.4;
  grid1->setDurationValue(durationValue1);
  
  ParameterizationGrid::Ptr grid2(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  const double durationValue2 = 4.7;
  grid2->setDurationValue(durationValue2);
  
  ParameterizationGrid::Ptr grid3(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  double durationValue3 = 8.4;
  grid3->setDurationValue(durationValue3);
  
  std::vector<ParameterizationGrid::Ptr> gridVector;
  gridVector.push_back(grid1);
  gridVector.push_back(grid2);
  gridVector.push_back(grid3);
  c.setControlGrid(gridVector);
  
  Parameter::Ptr result;
  const double epsilon = 0.03;
  result = c.getActiveDurationParameter(durationValue1 - epsilon);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue1);
  result = c.getActiveDurationParameter(durationValue1);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue1);
  result = c.getActiveDurationParameter(durationValue1 + epsilon);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue2);
  
  result = c.getActiveDurationParameter(durationValue2 - epsilon);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue2);
  result = c.getActiveDurationParameter(durationValue2);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue2);
  result = c.getActiveDurationParameter(durationValue2 + epsilon);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue3);
  
  result = c.getActiveDurationParameter(durationValue3 - epsilon);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue3);
  result = c.getActiveDurationParameter(durationValue3);
  BOOST_CHECK_EQUAL(result->getParameterValue(), durationValue3);
}

/**
* @test ControlTest - test function getActiveParameter
* 
* check whether a multigrid control returns the correct control Parameters
* for a given time.
*/
BOOST_AUTO_TEST_CASE(TestGetActiveParameter)
{
  const double lowerBound = 0.0, upperBound = 1.0;
  const unsigned esoIndex = 5;
  Control c = createControlObject(lowerBound, upperBound, esoIndex);
  
  const unsigned numIntervals = 4;
  ParameterizationGrid::Ptr grid1(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  const double durationValue1 = 2.4;
  grid1->setDurationValue(durationValue1);
  std::vector<double> valueVector1(numIntervals);
  for(unsigned i=0; i<numIntervals; i++){
    valueVector1[i] = 0.3 + i*0.5;
  }
  grid1->setAllParameterizationValues(valueVector1);
  
  ParameterizationGrid::Ptr grid2(new ParameterizationGrid(numIntervals, PieceWiseLinear));
  const double durationValue2 = 4.7;
  grid2->setDurationValue(durationValue2);
  //for piecewise linear the grid has an additional parameter
  std::vector<double> valueVector2(numIntervals+1);
  for(unsigned i=0; i<numIntervals; i++){
    valueVector2[i] = -0.4 - i*1.73;
  }
  grid2->setAllParameterizationValues(valueVector2);
  
  ParameterizationGrid::Ptr grid3(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  const double durationValue3 = 8.4;
  grid3->setDurationValue(durationValue3);
  std::vector<double> valueVector3(numIntervals);
  for(unsigned i=0; i<numIntervals; i++){
    valueVector3[i] = 12.324 + i*17.987;
  }
  grid3->setAllParameterizationValues(valueVector3);
  
  std::vector<ParameterizationGrid::Ptr> gridVector;
  gridVector.push_back(grid1);
  gridVector.push_back(grid2);
  gridVector.push_back(grid3);
  c.setControlGrid(gridVector);
  
  std::vector<Parameter::Ptr> result;
  const double epsilon = 0.03;
  result = c.getActiveParameter(0.0);
  BOOST_CHECK_EQUAL(result.size(), 1);
  BOOST_CHECK_EQUAL(result[0]->getParameterValue(), valueVector1.front());
  
  result = c.getActiveParameter(durationValue1 - epsilon);
  BOOST_CHECK_EQUAL(result.size(), 1);
  BOOST_CHECK_EQUAL(result[0]->getParameterValue(), valueVector1.back());
  
  // at durationValue1 the next grid gets active
  // because the second grid is piecewise linear, two parameters are returned
  result = c.getActiveParameter(durationValue1);
  BOOST_CHECK_EQUAL(result.size(), 2);
  BOOST_CHECK_EQUAL(result[0]->getParameterValue(), valueVector2[0]);
  BOOST_CHECK_EQUAL(result[1]->getParameterValue(), valueVector2[1]);
  
  // here the last two parameters of the second grid are active
  result = c.getActiveParameter(durationValue1 + durationValue2 - epsilon);
  BOOST_CHECK_EQUAL(result.size(), 2);
  BOOST_CHECK_EQUAL(result[0]->getParameterValue(), valueVector2[numIntervals-1]);
  BOOST_CHECK_EQUAL(result[1]->getParameterValue(), valueVector2[numIntervals]);
  
  // switch to grid3 here
  result = c.getActiveParameter(durationValue1 + durationValue2);
  BOOST_CHECK_EQUAL(result.size(), 1);
  BOOST_CHECK_EQUAL(result[0]->getParameterValue(), valueVector3.front());
  
  result = c.getActiveParameter(durationValue1 + durationValue2
                              + durationValue3-epsilon);
  BOOST_CHECK_EQUAL(result.size(), 1);
  BOOST_CHECK_EQUAL(result[0]->getParameterValue(), valueVector3.back());
  
  result = c.getActiveParameter(durationValue1 + durationValue2
                              + durationValue3);
  BOOST_CHECK_EQUAL(result.size(), 1);
  BOOST_CHECK_EQUAL(result[0]->getParameterValue(), valueVector3.back());
}



/** @test TestControlGridAndDuration creates controls with grid objects. 
*         The setting of the final time pointer is checked and if the grids are assembeld correctly by the control
*/
BOOST_AUTO_TEST_CASE(TestControlGridAndDuration)
{
  const double lb = -0.5;  double ub = 0.7;  int esoIndex = 3;
  Control con1, con2;
  con1 = createControlObject(lb, ub, esoIndex);
  con2 = createControlObject(lb, ub, esoIndex);

  const int numIntervals = 5;
  const int numIntervals2 = 10;
  // number of control structures for con1
  const unsigned numControlStructure = 2;
  // create grids which will be assigned to the controls
  ParameterizationGrid::Ptr Grid1(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  ParameterizationGrid::Ptr Grid2(new ParameterizationGrid(numIntervals2, PieceWiseConstant));
  ParameterizationGrid::Ptr Grid3(new ParameterizationGrid(numIntervals2, PieceWiseConstant));
  // create the global time parameter which is required for both controls to bet set from outside
  DurationParameter::Ptr globalDuration(new DurationParameter());
  // vectors required to store the control values
  std::vector<double> values1(numIntervals), values2(numIntervals2);
  // final times of the first control
  std::vector<double> durations(numControlStructure); 

  prepareControls(numIntervals, numIntervals2, numControlStructure,
                  Grid1, Grid2, Grid3, values1, values2, durations, con1, con2, globalDuration);
  // check if the total final time is correct
  BOOST_CHECK_EQUAL(con1.getTotalDuration(), globalDuration->getParameterValue());


  vector<double> mergedGrid =  con1.getControlGridScaled();
  vector<double> mergedGrid1 = con1.getControlGridUnscaled();
  const unsigned sizeGrid1 = Grid1->getDiscretizationGrid().size();
  // this grid is scaled and needs unscaling by the stage length
  vector<double> discGrid1 = Grid1->getDiscretizationGrid();
  // check if the grids that are generated in the control class are correct
  // the stage length of the first grid is equal to the final time of the first gird.
  const double lengthGrid1 = durations[0];
  for(unsigned i=0; i<sizeGrid1; i++){
    // merged grid is unscaled. Thus we unscale discgrid
    BOOST_CHECK_EQUAL(mergedGrid1[i], discGrid1[i] * lengthGrid1);
  }
  const unsigned sizeGrid2 = Grid2->getDiscretizationGrid().size();
  // stage length of the second time grid.
  const double lengthGrid2 = globalDuration->getParameterValue() - durations[0];
  vector<double> discGrid2 = Grid2->getDiscretizationGrid();
  for (unsigned i=sizeGrid1; i<sizeGrid2; i++){
    // the merged grid contains both grid1 and grid2. to compare the entries grid2 with the mergedGrid1
    // we have to add the size of Grid1 to the index of mergedGrid1.
    BOOST_CHECK_EQUAL(mergedGrid1[i+sizeGrid1], (discGrid2[i] * lengthGrid2) + durations[0]);
  }

  // check if all final times are returned in the right order and value
  std::vector<double> FinalTimesFromControl = con1.getFinalTimeVectorAbsolute();
  double checkDurationSum = 0.0;
  for(unsigned i=0; i<numControlStructure; i++){
    checkDurationSum += durations[i];
    BOOST_CHECK_EQUAL(checkDurationSum, FinalTimesFromControl[i]);
  }

  // test if the controls are without sensitivities when function is set
  con1.setControlWithoutSens();
  std::vector<ControlParameter::Ptr> conPara;
  for(unsigned i=0; i<numControlStructure; i++){
    conPara = con1.getParameterizationGrid(i)->getAllControlParameter();
    unsigned numberOfParameters = conPara.size();
    for(unsigned j=0; j<numberOfParameters; j++){
      BOOST_CHECK(!conPara[i]->getWithSensitivity());
    }
  }
}
/** @test ControlIntegratorInterface tests the interface between the control and the integrator
*/
BOOST_AUTO_TEST_CASE(ControlIntegratorInterface)
{
  const double lb = -0.5;  double ub = 0.7;  int esoIndex = 3;
  Control con1, con2;
  con1 = createControlObject(lb, ub, esoIndex);
  con2 = createControlObject(lb, ub, esoIndex);

  const int numIntervals = 5;
  const int numIntervals2 = 10;
  // number of control structures for con1
  const unsigned numControlStructure = 2;
  // create grids which will be assigned to the controls
  ParameterizationGrid::Ptr Grid1(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  ParameterizationGrid::Ptr Grid2(new ParameterizationGrid(numIntervals2, PieceWiseConstant));
  ParameterizationGrid::Ptr Grid3(new ParameterizationGrid(numIntervals2, PieceWiseConstant));
  // create the global time parameter which is required for both controls to bet set from outside
  DurationParameter::Ptr globalDuration(new DurationParameter());
  // vectors required to store the control values
  std::vector<double> values1(numIntervals), values2(numIntervals2);
  // final times of the first control
  std::vector<double> durations(numControlStructure); 

  prepareControls(numIntervals, numIntervals2, numControlStructure,
                  Grid1, Grid2, Grid3, values1, values2, durations, con1, con2, globalDuration);
  // we initialize the controls in order to go through the grid using the interface functions
  // with the meta eso
  con1.initializeForFirstIntegration();
  // we first check if the initialization was successful
  const unsigned currentGridIndex = con1.getActiveGridIndex();
  unsigned firstIndex = 0;
  BOOST_CHECK_EQUAL(currentGridIndex, firstIndex);
  vector<Parameter::Ptr> ConPara = con1.getActiveParameter();
  BOOST_CHECK(ConPara[0]->getParameterSensActivity());
  BOOST_CHECK_EQUAL(ConPara[0]->getParameterValue(), values1[0]);
  // we check that the checkParameterchange reacts only to absolute unscaled values
  // thus we first check for a random value of 0.001. Then for a value which corresponds to a scaled time
  // and finally to a value which triggers a change.
  double testingTimePoints[] = {0.001, 0.067, 0.201};
  bool checkChange;
  for(int i = 0; i < 2; i++){
    checkChange = con1.checkParameterChange(testingTimePoints[i]);
    BOOST_CHECK(!checkChange);
  }
  checkChange = con1.checkParameterChange(testingTimePoints[2]);
  BOOST_CHECK(checkChange);

  con1.changeActiveParameter();
  ConPara = con1.getActiveParameter();
  // the value is not influenced by the time since it is piece wise constant discretization
  BOOST_CHECK_EQUAL(con1.calculateCurrentControlValue(testingTimePoints[2]+0.00001), values1[1]);
  // we go to the end of the stage
  int skipIntervals = 3;
  for(int i=0; i<skipIntervals; i++){
    con1.changeActiveParameter();
  }
  // 1 corresponds to the end time of the first stage
  checkChange = con1.checkParameterChange(durations[0]);  BOOST_CHECK(checkChange);
  // now we are in the second stage
  con1.changeActiveParameter();
  ConPara = con1.getActiveParameter();
  BOOST_CHECK_EQUAL(ConPara[0]->getParameterValue(), values2[0]);
  BOOST_CHECK(ConPara[0]->getParameterSensActivity());
  // we go completly through the second stage
  skipIntervals = 8;
  for(int i=0; i<skipIntervals; i++){
    con1.changeActiveParameter();
  }
  ConPara = con1.getActiveParameter();
  BOOST_CHECK_EQUAL(ConPara[0]->getParameterValue(), values2[8]);
  // last grid point of the controls
  vector<double> mergedGrid1 = con1.getControlGridUnscaled();
  checkChange = con1.checkParameterChange(mergedGrid1[15]);  BOOST_CHECK(checkChange);
  con1.changeActiveParameter();
  ConPara = con1.getActiveParameter();
  BOOST_CHECK(ConPara[0]->getParameterSensActivity());
  BOOST_CHECK_EQUAL(ConPara[0]->getParameterValue(), values2[9]);
  // we calculate the control value with respect to the total time.
  // the discretization is piecewiselinear and is thus independent of the parameter
  const double evalControlTime = 0.9525; // random value
  BOOST_CHECK_EQUAL(con1.calculateCurrentControlValue(evalControlTime), values2[9]);

  // check if the address in the ControlParameter is equal to the address of the Control that contains them
  for(unsigned i=0; i<numControlStructure; i++){
    ParameterizationGrid::Ptr paraGrid = con1.getParameterizationGrid(i);
    std::vector<ControlParameter::Ptr> conParaPointer = paraGrid->getAllControlParameter();
    for(unsigned j=0; j<conParaPointer.size(); j++){
      Control* con2 = conParaPointer[j]->getControl();
      BOOST_CHECK_EQUAL(con2, &con1);
    }
  }
  BOOST_CHECK(con1.isCurrentDuration(durations[1]));

}
/** @test of the copy constructor of the Control class */
BOOST_AUTO_TEST_CASE(CopyControl)
{
  Control c1, c2, c3;
  Control copyc1(c1);
  BOOST_CHECK(c2.getParameterizationGrid(0).get() == NULL);
  const double lowerBound = 2;
  const double upperBound = 10;
  const int esoIndex = 3;
  c2 = createControlObject(lowerBound, upperBound, esoIndex);
  c3 = createControlObject(lowerBound, upperBound, esoIndex);
  const int numIntervals = 5;
  ParameterizationGrid::Ptr c2Grid(new ParameterizationGrid(numIntervals, PieceWiseConstant));

  int gridSize = 1;
  std::vector<ParameterizationGrid::Ptr> vecGrid(gridSize);
  vecGrid[0] = c2Grid;
  double durationValue = 1.3;
  DurationParameter::Ptr finTime(new DurationParameter(durationValue));
  vecGrid.back()->setDurationPointer(finTime);
  c2.setControlGrid(vecGrid);
  // override the pointer of the last control grid. In this case c2 has only one grid, so the 
  // last grid has index = 0.
  int lastGridIndex = gridSize - 1;
  Control copyc2(c2);
  DurationParameter::Ptr finTime2 = copyc2.getParameterizationGrid(lastGridIndex)->getDurationPointer();
  // check if address is the same because it is the last Final Time
  BOOST_CHECK_EQUAL(finTime2.get(), finTime.get());
  // check if we have a new parameterization pointer
  BOOST_CHECK(copyc2.getParameterizationGrid(lastGridIndex) != c2.getParameterizationGrid(lastGridIndex));
  // now we create a control with 2 grid and check if the copy went well
  
  
  ParameterizationGrid::Ptr lastgrid(new ParameterizationGrid(c2Grid.get()));
  ParameterizationGrid::Ptr firstgrid(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  gridSize = 2;
  vecGrid.resize(gridSize);
  durationValue = 0.7;
  firstgrid->setDurationValue(durationValue);
  vecGrid[0] = firstgrid;
  vecGrid[1] = lastgrid;
  lastgrid->setDurationPointer(finTime);
  c3.setControlGrid(vecGrid);
  // set the last pointer for the final time. In this case the gridSize is 2. And the last index is 1
  lastGridIndex = gridSize - 1;
  Control copyc3(c3);
  finTime2 = copyc3.getParameterizationGrid(lastGridIndex)->getDurationPointer();
  BOOST_CHECK_EQUAL(finTime2, finTime);
  // we check if the final time pointer of c3 of the first grid is different to the copy.
  const int firstGridIndex = 0;
  DurationParameter::Ptr finTime3 = copyc3.getParameterizationGrid(firstGridIndex)->getDurationPointer();
  finTime2 = c3.getParameterizationGrid(firstGridIndex)->getDurationPointer();
  // The first final times should be different
  BOOST_CHECK(finTime2.get() != finTime3.get());
}
/**
* @test TestGrid - tests the grid class
*/ 
BOOST_AUTO_TEST_CASE(TestGrid)
{
  const int numberOfIntervals = 5;
  ControlType PWC = PieceWiseConstant;
  ParameterizationGrid Grid(numberOfIntervals, PWC);

  
  Grid.setAllParameterBounds(4,5);

}

/**
* @test TestLinearControlGrid - creates a linear grid and tests the evaluation of the spline
*/ 
BOOST_AUTO_TEST_CASE(TestLinearControlGrid)
{
  const int numberOfIntervals = 5;
  ControlType pwl = PieceWiseLinear;
  ParameterizationGrid Grid(numberOfIntervals, pwl);
  vector<double> vals(numberOfIntervals + 1);
  for(int i=0; i<numberOfIntervals+1; i++){
    vals[i] = 3.28 * i + 1.72;
  }
  Grid.setAllParameterizationValues(vals);
  vector<ControlParameter::Ptr> controlParams = Grid.getAllControlParameter();
  for(unsigned i=0; i<controlParams.size(); i++){
    controlParams[i]->setParameterIndex(i);
  }

  // evaluate splines for a grid twice as fine as the parameterization grid
  const int numIntervalsSpline = numberOfIntervals * 2;
  vector<double> timePoints (numIntervalsSpline);
  for(int i=0; i<numIntervalsSpline; i++){
    timePoints[i] = double(i)/numIntervalsSpline;
  }
  timePoints.push_back(1.0);
  
  vector<double> evalSpline(timePoints.size());
  vector<unsigned> activeIndices(2);
  activeIndices[0]=0;
  activeIndices[1]=1;
  Grid.setActiveParameterIndex(activeIndices);
  for(unsigned i=0; i<evalSpline.size(); i++){
    evalSpline[i] = Grid.evalSpline(timePoints[i]);
    if((i+1)%2==0&&i<9){//set active indices
      activeIndices[0]++;
      activeIndices[1]++;
      Grid.setActiveParameterIndex(activeIndices);
    }
  }

  // create vector for comparsion
  vector<double> evalSplineTest(timePoints.size());
  int j=0;
  for(unsigned i=0; i<timePoints.size(); i++){
    j = j + i%2;
    if(i % 2 == 0)
      evalSplineTest[i] = vals[j];
    else
      evalSplineTest[i] = (vals[j] - vals[j-1])/2 + vals[j-1];
  }
  for(unsigned i=0; i<timePoints.size(); i++){
    BOOST_CHECK_CLOSE(evalSplineTest[i], evalSpline[i], BOOST_TOL);
  }

  activeIndices[0]=0;
  activeIndices[1]=1;
  Grid.setActiveParameterIndex(activeIndices);

  vector<double> evaldSpline1(timePoints.size());
  vector<double> evaldSpline2(timePoints.size());
  unsigned disIndex=1;
  for(unsigned i=0; i<evaldSpline1.size(); i++){
    vector<double> disGrid=Grid.getDiscretizationGrid();
    if(timePoints[i]>disGrid[disIndex]){//set active indices
      activeIndices[0]++;
      activeIndices[1]++;
      Grid.setActiveParameterIndex(activeIndices);
      if(disIndex<disGrid.size()-1)
        disIndex++;
    }
    evaldSpline1[i] = Grid.evaldSpline(timePoints[i], 0);
    evaldSpline2[i] = Grid.evaldSpline(timePoints[i], 1);
  }
  vector<double> evaldSplineTest1(timePoints.size(), 0.0);
  vector<double> evaldSplineTest2(timePoints.size(), 0.0);
  evaldSplineTest1[0] = 1.0;
  evaldSplineTest1[1] = 0.5;

  evaldSplineTest2[1] = 0.5;
  evaldSplineTest2[2] = 1;
  evaldSplineTest2[3] = 0.5;
  // rest is zero
  for(unsigned i=0; i<evaldSplineTest1.size(); i++){
    BOOST_CHECK_CLOSE(evaldSplineTest1[i], evaldSpline1[i], BOOST_TOL);
    BOOST_CHECK_CLOSE(evaldSplineTest2[i], evaldSpline2[i], BOOST_TOL);
  }

  activeIndices[0]=0;
  activeIndices[1]=1;
  Grid.setActiveParameterIndex(activeIndices);
  disIndex=1;
  for(unsigned i=0; i<evaldSpline1.size(); i++){
    vector<double> disGrid=Grid.getDiscretizationGrid();
    if(timePoints[i]>disGrid[disIndex]){//set active indices
      activeIndices[0]++;
      activeIndices[1]++;
      Grid.setActiveParameterIndex(activeIndices);
      if(disIndex<disGrid.size()-1)
        disIndex++;
    }
    evaldSpline1[i] = Grid.evaldSpline(timePoints[i], 1);
    evaldSpline2[i] = Grid.evaldSpline(timePoints[i], 2);
  }

  vector<double> evaldSplineTest3(timePoints.size(), 0.0);
  vector<double> evaldSplineTest4(timePoints.size(), 0.0);

  evaldSplineTest3[1] = 0.5;
  evaldSplineTest3[2] = 1.0;
  evaldSplineTest3[3] = 0.5;

  evaldSplineTest4[3] = 0.5;
  evaldSplineTest4[4] = 1.0;
  evaldSplineTest4[5] = 0.5;
  // rest is zero
  for(unsigned i=0; i<evaldSplineTest2.size(); i++){
    BOOST_CHECK_CLOSE(evaldSplineTest3[i], evaldSpline1[i], BOOST_TOL);
    BOOST_CHECK_CLOSE(evaldSplineTest4[i], evaldSpline2[i], BOOST_TOL);
  }

  // in case of linear approximations we have two active control parameter. So we need to test some more stuff

}


/** @test ControlIntegratorInterfaceForLinearSplines tests the interface between the control and the integrator
*         for linear splines
*/
BOOST_AUTO_TEST_CASE(ControlIntegratorInterfaceForLinearSplines)
{
  const double lb = -0.5;  double ub = 0.7;  int esoIndex = 3;
  Control con1, con2;
  con1 = createControlObject(lb, ub, esoIndex);
  con2 = createControlObject(lb, ub, esoIndex);

  const int numIntervals = 5;
  const int numIntervals2 = 10;
  // number of control structures for con1
  const unsigned numControlStructure = 2;
  // create grids which will be assigned to the controls
  ParameterizationGrid::Ptr Grid1(new ParameterizationGrid(numIntervals, PieceWiseLinear));
  ParameterizationGrid::Ptr Grid2(new ParameterizationGrid(numIntervals2, PieceWiseLinear));
  ParameterizationGrid::Ptr Grid3(new ParameterizationGrid(numIntervals2, PieceWiseLinear));
  // create the global time parameter which is required for both controls to bet set from outside
  DurationParameter::Ptr globalDuration(new DurationParameter());
  // vectors required to store the control values
  std::vector<double> values1(numIntervals), values2(numIntervals2);
  // final times of the first control
  std::vector<double> durations(numControlStructure); 

  prepareControls(numIntervals, numIntervals2, numControlStructure,
                  Grid1, Grid2, Grid3, values1, values2, durations, con1, con2, globalDuration);
  // we initialize the controls in order to go through the grid using the interface functions
  // with the meta eso
  con1.initializeForFirstIntegration();
  // we first check if the initialization was successfulridIndex();
  const unsigned firstIndex = 0;
  BOOST_CHECK_EQUAL(con1.getActiveGridIndex(), firstIndex);
  vector<Parameter::Ptr> ConPara = con1.getActiveParameter();
  for(unsigned i=0; i<ConPara.size(); i++){
    BOOST_CHECK(ConPara[i]->getParameterSensActivity());
    BOOST_CHECK_EQUAL(ConPara[i]->getParameterValue(), values1[i]);
  }
  // we check that the checkParameterchange reacts only to absolute unscaled values
  // thus we first check for a random value of 0.001. Then for a value which corresponds to a scaled time
  // and finally to a value which triggers a change.
  double testingTimePoints[] = {0.001, 0.067, 0.201};
  bool checkChange;
  for(int i = 0; i < 2; i++){
    checkChange = con1.checkParameterChange(testingTimePoints[i]);
    BOOST_CHECK(!checkChange);
  }
  checkChange = con1.checkParameterChange(testingTimePoints[2]);
  BOOST_CHECK(checkChange);

  con1.changeActiveParameter();
  ConPara = con1.getActiveParameter();
  // the value is not influenced by the time since it is piece wise constant discretization
  const double tp = testingTimePoints[2] + 0.00001;
  const double testValue = (values1[2] - values1[1]) *  (tp - 0.2)/0.2 + values1[1];
  BOOST_CHECK_EQUAL(con1.calculateCurrentControlValue(tp), testValue);
  // we go to the end of the stage
  int skipIntervals = 3;
  for(int i=0; i<skipIntervals; i++){
    con1.changeActiveParameter();
  }
  // 1 corresponds to the end time of the first stage
  checkChange = con1.checkParameterChange(durations[0]);  BOOST_CHECK(checkChange);
  // now we are in the second stage
  con1.changeActiveParameter();
  ConPara = con1.getActiveParameter();
  for(unsigned i=0; i<ConPara.size(); i++){
    BOOST_CHECK_EQUAL(ConPara[i]->getParameterValue(), values2[i]);
    BOOST_CHECK(ConPara[i]->getParameterSensActivity());
  }
  // we go completly through the second stage
  skipIntervals = 8;
  for(int i=0; i<skipIntervals; i++){
    con1.changeActiveParameter();
  }
  ConPara = con1.getActiveParameter();
  for(unsigned i=0; i<ConPara.size(); i++){
    BOOST_CHECK_EQUAL(ConPara[i]->getParameterValue(), values2[skipIntervals+i]);
    BOOST_CHECK(ConPara[i]->getParameterSensActivity());
  }
  // last grid point of the controls
  vector<double> mergedGrid1 = con1.getControlGridUnscaled();
  checkChange = con1.checkParameterChange(mergedGrid1[15]);  BOOST_CHECK(checkChange);
  con1.changeActiveParameter();
  ConPara = con1.getActiveParameter();
  for(unsigned i=0; i<ConPara.size(); i++){
    BOOST_CHECK_EQUAL(ConPara[i]->getParameterValue(), values2[skipIntervals+i+1]);
    BOOST_CHECK(ConPara[i]->getParameterSensActivity());
  }
  // we calculate the control value with respect to the total time.
  const double tp2 = (3.81 - 1.0)/3.0 ; // 1.0 duration of previous grid. 3.0 duration of current grid
  const double testValue2 = (values2[10] - values2[9]) *  (tp2 - 0.9)/0.1 + values2[9];
  BOOST_CHECK_CLOSE(con1.calculateCurrentControlValue(tp2), testValue2, 1E-10);

  // check if the address in the ControlParameter is equal to the address of the Control that contains them
  for(unsigned i=0; i<numControlStructure; i++){
    ParameterizationGrid::Ptr paraGrid = con1.getParameterizationGrid(i);
    std::vector<ControlParameter::Ptr> conParaPointer = paraGrid->getAllControlParameter();
    for(unsigned j=0; j<conParaPointer.size(); j++){
      Control* con2 = conParaPointer[j]->getControl();
      BOOST_CHECK_EQUAL(con2, &con1);
    }
  }
  BOOST_CHECK(con1.isCurrentDuration(durations[1]));

}

/**
* @test ControlTest - testSplittingOfPiecewiseConstantParameters
*       in this test a grid is split several times by adding additional
*       duration parameters. Three cases are checked:\n
*       1. insert an already existing duration parameter\n
*       2. insert a duration parameter that lies directly on the grid\n
*       3. insert a duration parameter that lies between two time points
*/
BOOST_AUTO_TEST_CASE(testSplittingOfPiecewiseConstantParameters)
{
  Control c;
  const unsigned numIntervals = 4;
  const double totalDuration = 1.235;
  // 1e-10 is the threshold used in Control class under which two doubles are 
  // treated as equal - so use a much smaller value to disturb some final times
  const double smallValue = 1e-12;
  const unsigned numGrids = 1;
  std::vector<ParameterizationGrid::Ptr> grids(numGrids);
  grids[0] = ParameterizationGrid::Ptr(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  grids[0]->setDurationValue(totalDuration);
  c.setControlGrid(grids);
  
  std::vector<ControlParameter::Ptr> controlParameters;
  controlParameters = c.getParameterizationGrid(0)->getAllControlParameter();
  for(unsigned i=0; i<controlParameters.size(); i++){
    controlParameters[i]->setParameterIndex(i);
  }
  BOOST_REQUIRE_EQUAL(controlParameters.size(), numIntervals);
  
  //insert a already existing final time - that should change nothing, but the
  //pointer of the inserted final time should be used
  {
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration));

    const unsigned indexOfFirstGrid = 0;
    c.insertDurationParameter(newDuration, indexOfFirstGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), numGrids);
  }
  
  //insert a final time very close to the existing final time
  {
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration-smallValue));

    const unsigned indexOfFirstGrid = 0;
    c.insertDurationParameter(newDuration, indexOfFirstGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), numGrids);
  }
  
  //insert a final time on a gridpoint splitting the grid without splitting any parameter
  {
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration/2));
    const unsigned indexOfFirstGrid = 0;
    c.insertDurationParameter(newDuration, indexOfFirstGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), numGrids + 1);
    std::vector<ControlParameter::Ptr> controlParams1, controlParams2;
    controlParams1 = c.getParameterizationGrid(indexOfFirstGrid)->getAllControlParameter();
    controlParams2 = c.getParameterizationGrid(indexOfFirstGrid+1)->getAllControlParameter();
    const unsigned numControlParams1 = controlParams1.size();
    const unsigned numControlParams2 = controlParams2.size();
    BOOST_CHECK_EQUAL(numControlParams1, 2);
    BOOST_CHECK_EQUAL(numControlParams2, 2);
    
    for(unsigned i=0; i<numControlParams1; i++){
      BOOST_CHECK_EQUAL(controlParams1[i].get(), controlParameters[i].get());
    }
    for(unsigned i=0; i<numControlParams2; i++){
      BOOST_CHECK_EQUAL(controlParams2[i].get(), controlParameters[i+numControlParams1].get());
    }
  }
  
  //insert a final time very close to a gridpoint splitting the grid without splitting any parameter
  {
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration/4 + smallValue));
    const unsigned indexOfSecondGrid = 1;
    const unsigned currentNumGrids = c.getNumberOfGrids();
    c.insertDurationParameter(newDuration, indexOfSecondGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), currentNumGrids + 1);
    std::vector<ControlParameter::Ptr> controlParams1, controlParams2;
    controlParams1 = c.getParameterizationGrid(indexOfSecondGrid)->getAllControlParameter();
    controlParams2 = c.getParameterizationGrid(indexOfSecondGrid+1)->getAllControlParameter();
    
    const unsigned numControlParamsGrid1 = c.getParameterizationGrid(0)->getAllControlParameter().size();
    const unsigned numControlParams1 = controlParams1.size();
    const unsigned numControlParams2 = controlParams2.size();
    BOOST_CHECK_EQUAL(numControlParams1, 1);
    BOOST_CHECK_EQUAL(numControlParams2, 1);
    
    for(unsigned i=0; i<numControlParams1; i++){
      BOOST_CHECK_EQUAL(controlParams1[i].get(), controlParameters[i + numControlParamsGrid1].get());
    }
    for(unsigned i=0; i<numControlParams2; i++){
      BOOST_CHECK_EQUAL(controlParams2[i].get(), controlParameters[i + numControlParamsGrid1
                                                                     + numControlParams1].get());
    }
  }
  
  //insert a final time between two gridpoints splitting a parameter
  {
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration/6));
    const unsigned indexOfFirstGrid = 0;
    const unsigned currentNumGrids = c.getNumberOfGrids();
    c.insertDurationParameter(newDuration, indexOfFirstGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), currentNumGrids + 1);
    std::vector<ControlParameter::Ptr> controlParams1, controlParams2;
    controlParams1 = c.getParameterizationGrid(indexOfFirstGrid)->getAllControlParameter();
    controlParams2 = c.getParameterizationGrid(indexOfFirstGrid+1)->getAllControlParameter();
    const unsigned numControlParams1 = controlParams1.size();
    const unsigned numControlParams2 = controlParams2.size();
    BOOST_CHECK_EQUAL(numControlParams1, 1);
    BOOST_CHECK_EQUAL(numControlParams2, 2);
    
    for(unsigned i=0; i<numControlParams1; i++){
      BOOST_CHECK_EQUAL(controlParams1[i].get(), controlParameters[i].get());
    }
    //first parameter should be replaced
    BOOST_CHECK(controlParams2.front().get() != controlParameters[numControlParams1-1].get());
    BOOST_CHECK_EQUAL(controlParams2.front()->getParameterIndex(),
                      controlParameters[numControlParams1-1]->getParameterIndex());
    for(unsigned i=1; i<numControlParams2; i++){
      BOOST_CHECK_EQUAL(controlParams2[i].get(), controlParameters[i + numControlParams1 - 1].get());
    }
    
  }
  
  //insert a final time between two gridpoints splitting a parameter on the last interval of a grid
  {
    //duration of the last grid should be totalDuration/4
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration/6));
    const unsigned currentNumGrids = c.getNumberOfGrids();
    const unsigned indexOfLastGrid = currentNumGrids-1;
    c.insertDurationParameter(newDuration, indexOfLastGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), currentNumGrids + 1);
    std::vector<ControlParameter::Ptr> controlParams1, controlParams2;
    controlParams1 = c.getParameterizationGrid(indexOfLastGrid)->getAllControlParameter();
    controlParams2 = c.getParameterizationGrid(indexOfLastGrid+1)->getAllControlParameter();
    const unsigned numControlParams1 = controlParams1.size();
    const unsigned numControlParams2 = controlParams2.size();
    BOOST_CHECK_EQUAL(numControlParams1, 1);
    BOOST_CHECK_EQUAL(numControlParams2, 1);
    
   const unsigned numParamsOfPreviousGrids = controlParameters.size() - numControlParams1;
    for(unsigned i=0; i<numControlParams1; i++){
      BOOST_CHECK_EQUAL(controlParams1[i].get(),
                        controlParameters[numParamsOfPreviousGrids + i].get());
    }
    //first parameter should be replaced
    BOOST_CHECK(controlParams2.front().get() != controlParameters.back().get());
    BOOST_CHECK_EQUAL(controlParams2.front()->getParameterIndex(),
                      controlParameters.back()->getParameterIndex());
  }
}

/**
* @test ControlTest - testSplittingOfPiecewiseLinearParameters
*       in this test a grid is split several times by adding additional
*       duration parameters. Two cases are checked:\n
*       1. insert a duration parameter that lies directly on the grid\n
*       2. insert a duration parameter that lies between two time points
*       The case of an already existing duration parameter is the same
*       as for piecewise constant and is not tested in this test case
*/
BOOST_AUTO_TEST_CASE(testSplittingOfPiecewiseLinearParameters)
{
  Control c;
  const unsigned numIntervals = 6;
  const double totalDuration = 3.435;
  // 1e-10 is the threshold used in Control class under which two doubles are 
  // treated as equal - so use a much smaller value to disturb some final times
  const unsigned numGrids = 1;
  std::vector<ParameterizationGrid::Ptr> grids(numGrids);
  grids[0] = ParameterizationGrid::Ptr(new ParameterizationGrid(numIntervals, PieceWiseLinear));
  grids[0]->setDurationValue(totalDuration);
  c.setControlGrid(grids);
  
  std::vector<ControlParameter::Ptr> controlParameters;
  controlParameters = c.getParameterizationGrid(0)->getAllControlParameter();
  for(unsigned i=0; i<controlParameters.size(); i++){
    controlParameters[i]->setParameterIndex(i);
  }
  BOOST_REQUIRE_EQUAL(controlParameters.size(), numIntervals+1);
  
  //insert a new final time parameter that lies directly on the grid
  {
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration/numIntervals));

    const unsigned indexOfFirstGrid = 0;
    c.insertDurationParameter(newDuration, indexOfFirstGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), numGrids + 1);
    
    std::vector<ControlParameter::Ptr> controlParams1, controlParams2;
    controlParams1 = c.getParameterizationGrid(indexOfFirstGrid)->getAllControlParameter();
    controlParams2 = c.getParameterizationGrid(indexOfFirstGrid+1)->getAllControlParameter();
    const unsigned numControlParams1 = controlParams1.size();
    const unsigned numControlParams2 = controlParams2.size();
    BOOST_CHECK_EQUAL(numControlParams1, 2);
    BOOST_CHECK_EQUAL(numControlParams2, numIntervals);
    
    for(unsigned i=0; i<numControlParams1; i++){
      BOOST_CHECK_EQUAL(controlParams1[i].get(), controlParameters[i].get());
    }
    // check if substitute parameter
    BOOST_CHECK( !controlParams2.front()->isOriginal());
    // the first parameter of the second grid should be a subsitute parameter of the first grid
    BOOST_CHECK_EQUAL(controlParams1.back().get()->getParameterIndex(), controlParams2.front().get()->getParameterIndex());
    //first parameter should be replaced
    for(unsigned i=1; i<numControlParams2; i++){
      BOOST_CHECK_EQUAL(controlParams2[i].get(), controlParameters[i + numControlParams1 - 1].get());
    }
  }
  
  //insert a new final time parameter that splits a parameter
  {
    DurationParameter::Ptr newDuration(new DurationParameter(totalDuration/(2*numIntervals)));
    const unsigned currentNumGrids = c.getNumberOfGrids();
    const unsigned indexOfSecondGrid = 1;
    c.insertDurationParameter(newDuration, indexOfSecondGrid);
    BOOST_CHECK_EQUAL(c.getNumberOfGrids(), currentNumGrids + 1);
    std::vector<ControlParameter::Ptr> controlParams1, controlParams2;
    controlParams1 = c.getParameterizationGrid(indexOfSecondGrid)->getAllControlParameter();
    controlParams2 = c.getParameterizationGrid(indexOfSecondGrid+1)->getAllControlParameter();
    const unsigned numControlParams1 = controlParams1.size();
    const unsigned numControlParams2 = controlParams2.size();
    BOOST_CHECK_EQUAL(numControlParams1, 2);
    BOOST_CHECK_EQUAL(numControlParams2, numIntervals);
    
    const unsigned numParamsOfPreviousGrids = 1;
    for(unsigned i=0; i<numControlParams1; i++){
      BOOST_CHECK_EQUAL(controlParams1[i].get()->getParameterIndex(),
                        controlParameters[numParamsOfPreviousGrids + i].get()->getParameterIndex());
    }
    // last parameter of controlParams1 and first parameter of controlParams2 should be 
    // a substitute parameter
    // note that for piecewise linear two parameters are overlapping
    BOOST_CHECK(controlParams1.back().get() != 
                controlParameters[numParamsOfPreviousGrids + numControlParams1-1].get());
    BOOST_CHECK_EQUAL(controlParams1.back()->getParameterIndex(),
                      controlParameters[numParamsOfPreviousGrids
                                        + numControlParams1-1]->getParameterIndex());
    BOOST_CHECK(controlParams2.front().get() !=
                controlParameters[numParamsOfPreviousGrids + numControlParams1-2].get());
    BOOST_CHECK_EQUAL(controlParams2.front()->getParameterIndex(),
                      controlParameters[numParamsOfPreviousGrids
                                        + numControlParams1-2]->getParameterIndex());
    for(unsigned i=1; i<numControlParams1; i++){
      BOOST_CHECK_EQUAL(controlParams2[i].get(),
                        controlParameters[numParamsOfPreviousGrids
                                          + numControlParams1-2 +i].get());
    }
  }
}

/**
* @test ControlTest - testSubstituteParameter
* 
* If a parameter on a piecewise linear grid is substituted, the substituted parameters
* should have the same behaviour as the original parameter.
* This is checked in this test
*/
BOOST_AUTO_TEST_CASE(testSubstituteParameter)
{
  Control c;
  const unsigned numIntervals = 1;
  const double totalDuration = 3.435;
  const unsigned numGrids = 1;
  std::vector<ParameterizationGrid::Ptr> grids(numGrids);
  grids[0] = ParameterizationGrid::Ptr(new ParameterizationGrid(numIntervals, PieceWiseLinear));
  grids[0]->setDurationValue(totalDuration);
  c.setControlGrid(grids);
  std::vector<ControlParameter::Ptr> controlParameters;
  controlParameters = c.getParameterizationGrid(0)->getAllControlParameter();
  for(unsigned i=0; i<controlParameters.size(); i++){
    controlParameters[i]->setParameterValue(1.0+i);
  }
  
  const unsigned numTimePoints = 12;
  std::vector<double> originalValues(numTimePoints);
  c.initializeForFirstIntegration();
  for(unsigned i=0; i<numTimePoints; i++){
    originalValues[i] = c.calculateCurrentControlValue((double(i)/(numTimePoints-1)));
  }
  
  DurationParameter::Ptr newDuration(new DurationParameter(totalDuration/3));
  const unsigned indexOfFirstGrid = 0;
  c.insertDurationParameter(newDuration, indexOfFirstGrid);
  c.initializeForFirstIntegration();
  
  for(unsigned i=0; i<numTimePoints; i++){
    double currentTime = (double(i)/(numTimePoints-1)) * 3;
    if(currentTime > 1.0){
      currentTime = ((double(i)/(numTimePoints-1)  - (1.0/3.0) )) * 3.0 / 2.0;
      if((c.checkParameterChange(currentTime) && i<numTimePoints-1) || currentTime < 0.05){
        c.changeActiveParameter();
      }
    }
    //do not change active parameter at the last position, because the control
    //attempts to change the grid, but is already on the last grid. This leads
    // to an error

    if(c.checkParameterChange(currentTime) && i<numTimePoints-1){
      c.changeActiveParameter();
    }
    BOOST_CHECK_CLOSE(originalValues[i],
                      c.calculateCurrentControlValue(currentTime),
                      THRESHOLD);
  }
  
}
BOOST_AUTO_TEST_CASE(testOriginalParameterizationGrid)
{
  Control c;
  const unsigned numGrids = 3;
  std::vector<ParameterizationGrid::Ptr> grids(numGrids);
  grids[0] = ParameterizationGrid::Ptr(new ParameterizationGrid(3, PieceWiseLinear));
  grids[0]->setDurationValue(2.5);
  grids[1] = ParameterizationGrid::Ptr(new ParameterizationGrid(4, PieceWiseLinear));
  grids[1]->setDurationValue(4);
  grids[2] = ParameterizationGrid::Ptr(new ParameterizationGrid(3, PieceWiseLinear));
  grids[2]->setDurationValue(4.5);

  vector<double> discret0, discret1, discret2;
  discret0.push_back(0.0); discret0.push_back(0.2); discret0.push_back(0.6); discret0.push_back(1.0);
  discret1.push_back(0.0); discret1.push_back(0.3); discret1.push_back(0.5); discret1.push_back(0.8); discret1.push_back(1.0);
  discret2.push_back(0.0); discret2.push_back(0.35); discret2.push_back(0.7); discret2.push_back(1.0);

  grids[0]->setDiscretizationGrid(discret0);
  grids[1]->setDiscretizationGrid(discret1);
  grids[2]->setDiscretizationGrid(discret2);
  vector<ControlParameter::Ptr> controlParams0, controlParams1, controlParams2;
  for(int i=0; i<4; i++){
    ControlParameter::Ptr tempParam0(new ControlParameter());
    ControlParameter::Ptr tempParam1(new ControlParameter());
    ControlParameter::Ptr tempParam2(new ControlParameter());
    tempParam0->setParameterValue(i+1);
    tempParam1->setParameterValue(i+2);
    tempParam2->setParameterValue(i+3);
    controlParams0.push_back(tempParam0);
    controlParams1.push_back(tempParam1);
    controlParams2.push_back(tempParam2);
  }
  ControlParameter::Ptr tempParam1(new ControlParameter());
  tempParam1->setParameterValue(4+2);
  controlParams1.push_back(tempParam1);

  grids[0]->setAllControlParameter(controlParams0);
  grids[1]->setAllControlParameter(controlParams1);
  grids[2]->setAllControlParameter(controlParams2);

  c.setControlGrid(grids);

  DurationParameter::Ptr newDuration0(new DurationParameter(1.6));
  c.insertDurationParameter(newDuration0, 1);
  DurationParameter::Ptr newDuration1(new DurationParameter(1.6));
  c.insertDurationParameter(newDuration1, 2);
  DurationParameter::Ptr newDuration2(new DurationParameter(3.6));
  c.insertDurationParameter(newDuration2, 4);
  DurationParameter::Ptr newDuration3(new DurationParameter(0.675));
  c.insertDurationParameter(newDuration3, 5);

  ParameterizationGrid::Ptr myGrid;
  vector<unsigned> activeIndices(2);
  vector<double> result0, res;

  //first grid
  result0.push_back(1.00000000000000000); result0.push_back(0.37500000000000000); result0.push_back(0.87500000000000000);
  result0.push_back(0.56250000000000000); result0.push_back(0.25000000000000000); result0.push_back(0.93750000000000000);
  result0.push_back(0.62500000000000000); result0.push_back(0.31250000000000000); result0.push_back(0.00000000000000000);
  result0.push_back(1.00000000000000000); result0.push_back(0.77777777777777779); result0.push_back(0.55555555555555558);
  result0.push_back(0.33333333333333326); result0.push_back(0.11111111111111105); result0.push_back(0.95238095238095233);
  result0.push_back(0.85714285714285710); result0.push_back(0.49999999999999989); result0.push_back(0.16666666666666663);
  result0.push_back(0.88888888888888895); result0.push_back(0.66666666666666641); result0.push_back(0.44444444444444431);
  result0.push_back(0.22222222222222210); result0.push_back(0.00000000000000000); result0.push_back(1.00000000000000000);
  result0.push_back(0.74999999999999978); result0.push_back(0.50000000000000000); result0.push_back(0.25000000000000033);
  result0.push_back(0.00000000000000000); result0.push_back(1.00000000000000000); result0.push_back(0.71428571428571419);
  result0.push_back(0.42857142857142849); result0.push_back(0.14285714285714268); result0.push_back(0.85714285714285698);
  result0.push_back(0.57142857142857140); result0.push_back(0.28571428571428537); result0.push_back(0.00000000000000000);
  result0.push_back(0.66666666666666641); result0.push_back(0.66666666666666641); result0.push_back(0.54166666666666652);
  result0.push_back(0.41666666666666663); result0.push_back(0.29166666666666630); result0.push_back(0.16666666666666641);
  result0.push_back(0.16666666666666641); result0.push_back(0.12499999999999956); result0.push_back(0.083333333333333037);
  result0.push_back(0.041666666666666186); result0.push_back(0.000000000000000000);

  //second grid
  result0.push_back(1.0000000000000000); result0.push_back(0.37500000000000000); result0.push_back(0.87500000000000000);
  result0.push_back(0.56250000000000000); result0.push_back(0.25000000000000000); result0.push_back(0.93750000000000000);
  result0.push_back(0.62500000000000000); result0.push_back(0.31250000000000000); result0.push_back(0.00000000000000000);
  result0.push_back(1.0000000000000000); result0.push_back(0.77777777777777768); result0.push_back(0.55555555555555547);
  result0.push_back(0.33333333333333315); result0.push_back(0.11111111111111094); result0.push_back(0.85714285714285698);
  result0.push_back(0.57142857142857117); result0.push_back(0.57142857142857117); result0.push_back(0.19047619047619035);
  result0.push_back(0.88888888888888873); result0.push_back(0.66666666666666674); result0.push_back(0.44444444444444442);
  result0.push_back(0.22222222222222221); result0.push_back(0.00000000000000000); result0.push_back(1.0000000000000000);
  result0.push_back(0.74999999999999989); result0.push_back(0.49999999999999978); result0.push_back(0.25000000000000011);
  result0.push_back(0.00000000000000000); result0.push_back(1.0000000000000000); result0.push_back(0.71428571428571419);
  result0.push_back(0.42857142857142849); result0.push_back(0.14285714285714268); result0.push_back(0.85714285714285698);
  result0.push_back(0.57142857142857140); result0.push_back(0.28571428571428537); result0.push_back(0.00000000000000000);
  result0.push_back(0.66666666666666641); result0.push_back(0.66666666666666641); result0.push_back(0.54166666666666652);
  result0.push_back(0.41666666666666663); result0.push_back(0.29166666666666630); result0.push_back(0.16666666666666641);
  result0.push_back(0.16666666666666641); result0.push_back(0.12499999999999956); result0.push_back(0.083333333333333037);
  result0.push_back(0.041666666666666186); result0.push_back(0.00000000000000000);

  //third grid;
  result0.push_back(1.0000000000000000); result0.push_back(0.37500000000000000); result0.push_back(0.87500000000000000);
  result0.push_back(0.56250000000000000); result0.push_back(0.25000000000000000); result0.push_back(0.93750000000000000);
  result0.push_back(0.62500000000000000); result0.push_back(0.31250000000000000); result0.push_back(0.00000000000000000);
  result0.push_back(1.0000000000000000); result0.push_back(0.77777777777777779); result0.push_back(0.55555555555555558);
  result0.push_back(0.33333333333333326); result0.push_back(0.11111111111111105); result0.push_back(0.80952380952380920);
  result0.push_back(0.42857142857142849); result0.push_back(0.42857142857142849); result0.push_back(0.14285714285714257);
  result0.push_back(0.88888888888888873); result0.push_back(0.66666666666666652); result0.push_back(0.44444444444444431);
  result0.push_back(0.22222222222222221); result0.push_back(0.00000000000000000);  result0.push_back(1.0000000000000000);
  result0.push_back(0.74999999999999956); result0.push_back(0.49999999999999967); result0.push_back(0.25000000000000044);
  result0.push_back(0.00000000000000000); result0.push_back(1.0000000000000000);  result0.push_back(0.71428571428571430);
  result0.push_back(0.42857142857142860); result0.push_back(0.14285714285714290); result0.push_back(0.85714285714285710);
  result0.push_back(0.57142857142857162); result0.push_back(0.28571428571428581); result0.push_back(0.00000000000000000);
  result0.push_back(0.47368421052631515); result0.push_back(0.47368421052631515); result0.push_back(0.41447368421052544);
  result0.push_back(0.35526315789473661); result0.push_back(0.29605263157894690); result0.push_back(0.23684210526315763);
  result0.push_back(0.23684210526315763); result0.push_back(0.17763157894736781); result0.push_back(0.11842105263157909);
  result0.push_back(0.059210526315788714); result0.push_back(0.00000000000000000);


  for(int round=0;round<3;round++){//round==0->Beispiel1, round==1->Beispiel2, round==2->Beispiel3
    unsigned parameterIndex=0;
    if(round==1){//change duration, keep sum durations the same
      c.getParameterizationGrid(1)->setDurationValue(2.0);
      c.getParameterizationGrid(1)->setDurationValue(1.2);
    }
    else if(round==2){//change duration and sum durations
      c.getParameterizationGrid(0)->setDurationValue(2.0);
      c.getParameterizationGrid(1)->setDurationValue(2.0);
      c.getParameterizationGrid(2)->setDurationValue(1.5);
      c.getParameterizationGrid(3)->setDurationValue(0.8);
      c.getParameterizationGrid(4)->setDurationValue(4.0);
      c.getParameterizationGrid(5)->setDurationValue(1.6);
      c.getParameterizationGrid(5)->setDurationValue(0.225);
    }
    c.initializeForFirstIntegration();
    for(unsigned i=0; i<c.getNumberOfGrids(); i++){
      unsigned disIndex=1;
      myGrid=c.getParameterizationGrid(i);
      vector<ControlParameter::Ptr> controlParams = myGrid->getAllControlParameter();
      for(unsigned l=0; l<controlParams.size(); l++){
        if(controlParams[l]->isOriginal()){
          controlParams[l]->setParameterIndex(parameterIndex);
          parameterIndex++;
        }
      }
      vector<double>DiscretizationGrid = myGrid->getDiscretizationGrid();
      vector<double> timePoints (DiscretizationGrid.size()*2);
      for(unsigned k=0; k<timePoints.size(); k++){
        timePoints[k] = double(k)/timePoints.size();
      }
      timePoints.push_back(1.0);
      activeIndices[0]=0;
      activeIndices[1]=1;
      myGrid->setActiveParameterIndex(activeIndices);

      c.setActiveGridIndex(i);
      for(unsigned r=0; r<timePoints.size(); r++){
        if(timePoints[r]>DiscretizationGrid[disIndex]){//change activeParameterIndice if necessary
          activeIndices[0]++;
          activeIndices[1]++;
          myGrid->setActiveParameterIndex(activeIndices);
          if(disIndex<DiscretizationGrid.size()-1)
            disIndex++;
        }
        unsigned paramIndex = myGrid->getControlParameter(activeIndices[0])->getParameterIndex();

        res.push_back(c.getCurrentDSplineValue(timePoints[r],paramIndex));
      }
    }
  }
  for(unsigned i=0; i<res.size(); i++){
    BOOST_CHECK_CLOSE(res[i], result0[i], THRESHOLD);
  }
}



BOOST_AUTO_TEST_CASE(TestExtraplotatingControlEvaluation)
{
  ParameterizationGrid::Ptr controlGrid1(new ParameterizationGrid(2, PieceWiseConstant));
  ParameterizationGrid::Ptr controlGrid2(new ParameterizationGrid(2, PieceWiseLinear));
  
  std::vector<double> discretizationGrid;
  discretizationGrid.push_back(0.0);
  discretizationGrid.push_back(0.5);
  discretizationGrid.push_back(1.0);
  controlGrid1->setDiscretizationGrid(discretizationGrid);
  controlGrid2->setDiscretizationGrid(discretizationGrid);
  
  std::vector<double> values;
  values.push_back(0.0);
  values.push_back(2.0);
  controlGrid1->setAllParameterizationValues(values);
  values.push_back(1.0);
  controlGrid2->setAllParameterizationValues(values);
  
  std::vector<unsigned> activeParameterIndices;
  activeParameterIndices.push_back(0);
  controlGrid1->setActiveParameterIndex(activeParameterIndices);
  activeParameterIndices.push_back(1);
  controlGrid2->setActiveParameterIndex(activeParameterIndices);
  
  controlGrid1->setDurationValue(4.0);
  controlGrid2->setDurationValue(3.0);
  
  std::vector<ParameterizationGrid::Ptr> controlGrids;
  controlGrids.push_back(controlGrid1);
  controlGrids.push_back(controlGrid2);
  Control c;
  c.setControlGrid(controlGrids);
  c.setActiveGridIndex(0);
  //check if control returns correct values
  double controlValue = c.calculateCurrentControlValue(0.2);
  BOOST_CHECK_CLOSE(controlValue, 0.0, THRESHOLD);
  controlValue = c.calculateCurrentControlValue(0.5);
  BOOST_CHECK_CLOSE(controlValue, 0.0, THRESHOLD);
  
  //now check if control can extrapolate
  controlValue = c.calculateCurrentControlValue(0.7);
  BOOST_CHECK_CLOSE(controlValue, 0.0, THRESHOLD);
  
  //now check behaviour for piece wise linear
  c.setActiveGridIndex(1);
  controlValue = c.calculateCurrentControlValue(0.25);
  BOOST_CHECK_CLOSE(controlValue, 1.0, THRESHOLD);
  controlValue = c.calculateCurrentControlValue(0.5);
  BOOST_CHECK_CLOSE(controlValue, 2.0, THRESHOLD);
  
  //now check if control can extrapolate
  controlValue = c.calculateCurrentControlValue(0.75);
  BOOST_CHECK_CLOSE(controlValue, 3.0, THRESHOLD);
}

BOOST_AUTO_TEST_SUITE_END()
