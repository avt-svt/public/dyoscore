/**
* @file IntegratorSingleStageMetaData.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Single Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* This file contains the definitions of the memberfunctions of the     \n
* IntegratorSingleStageMetaData class                                  \n
* =====================================================================\n
* @author Tjalf Hoffmann, Fady Assassa
* @date 10.11.2011
*/

#define MAKE_DYOS_DLL
#include "IntegratorSingleStageMetaData.hpp"
#include "MetaDataExceptions.hpp"
#include "Parameter.hpp"

#include "TripletMatrix.hpp"
#include "CscMatrix.hpp"
#include "EsoUtilities.hpp"
#include "UtilityFunctions.hpp"

#include <assert.h>
#include <sstream>
#include <set>

#define SCALED_GRID_THRESHOLD 1e-10

/**
* @brief standard constructor for derived dummy classes
* @warning some memberfunctions are not usable if created with this constructor
*          because of missing genericEso and missing controls
*/
IntegratorSingleStageMetaData::IntegratorSingleStageMetaData(): m_ut(1), m_rhsStatesDtime(1)
{
  // we have a single stage. thus m_intOptData has to be of size 1
  const int numStage = 1;
  m_intOptData.resize(numStage);
}

/**
* @brief constructor
*
* @param[in] genericEso (shared pointer to) GenericEso object used as model server
* @param[in] controls control object vector containing controls defined for the single stage ESO
* @param[in] initials initial value parameter for the single stage ESO
* @param[in] globalDuration (shared pointer to) duration of the stage
* @param[in] additionalGrid additional gridpoints (e.g. via StatePointConstraints)
*                           grid points in additionalGrid are supposed to have scaled values!
*/
IntegratorSingleStageMetaData::IntegratorSingleStageMetaData(const GenericEso::Ptr &genericEso,
                                                             const std::vector<Control> &controls,
                                                             const std::vector<InitialValueParameter::Ptr> &initials,
                                                             const DurationParameter::Ptr &globalDuration,
                                                             const std::vector<double> &additionalGrid)
                                                             ://preallocate workspace fields
                                                              m_ut(controls.size(), 0.0),
                                                              m_rhsStatesDtime(genericEso->getNumStates(), 0.0) 
{
  assert(genericEso.get() != NULL);
  m_controls = controls;
  m_genericEso = genericEso;
  m_globalDurationParameter = globalDuration;
  m_additionalGrid = additionalGrid;

  // we have a single stage. thus m_intOptData has to be of size 1
  const int numStage = 1;
  m_intOptData.resize(numStage);

  CsTripletMatrix::Ptr  Fu = createJacobianInTriplet();

  createJacobianFstates(Fu);
  createJacobianFup(Fu);
  createMassMatrix();
  
  // set m_gridDurations
  createGridDurationVector();
  
  double duration = 0.0;
  for(unsigned i=0; i<m_gridDurations.size(); i++){
    duration += m_gridDurations[i]->getParameterValue();
  }
  const unsigned numAdditionalGridpoints = m_additionalGrid.size();
  for(unsigned i=0; i<numAdditionalGridpoints; i++){
    m_additionalGrid[i] *= duration;
  }
  for(unsigned i=0; i<m_controls.size(); i++){
    m_controls[i].setGridPointsVector(m_additionalGrid);
  }

  // get data from ESO for the initial values
  const int numDifferential = m_genericEso->getNumDifferentialVariables();
  utils::Array<EsoIndex> diffIndices(numDifferential);
  utils::Array<EsoDouble> diffValues(numDifferential);
  m_genericEso->getDifferentialIndex(numDifferential, diffIndices.getData());
  m_genericEso->getDifferentialVariableValues(numDifferential, diffValues.getData());
  // intial values are assigned
  m_initialValues.resize(numDifferential);
  for(unsigned i=0; i<m_initialValues.size(); i++){
    m_initialValues[i] = InitialValueParameter::Ptr(new InitialValueParameter());
    m_initialValues[i]->setParameterValue(diffValues[i]);
    m_initialValues[i]->setEsoIndex(diffIndices[i]);
    EsoIndex equationIndex = getEquationIndexOfVariable(diffIndices[i], m_genericEso);
    assert(equationIndex >= 0);
    m_initialValues[i]->setEquationIndex(equationIndex);
    m_initialValues[i]->setWithSensitivity(false);
  }
  // set user given initials
  for(unsigned i=0; i<initials.size(); i++){
    bool initialFound = false;
    initials[i]->setUserFlag(true);
    for(unsigned j=0; j<m_initialValues.size(); j++){
      if(initials[i]->getEsoIndex() == m_initialValues[j]->getEsoIndex()){
        m_initialValues[j] = initials[i];
        initialFound = true;
        break;
      }
    }
    //todo throw an exception instead of this assert
    assert(initialFound);
  }

  //set sensitivity parameters
  defineSensParameters(m_parametersWithSensitivity);

  // set decision Variables
  defineDecisionVariables(m_decisionVariables);

  assignParameterIndices();

  m_observer = Observer::Ptr(new IntegrationObserver());
  
  m_plotGridActivated = false;
}

/**
* @brief destructor
*/
IntegratorSingleStageMetaData::~IntegratorSingleStageMetaData(){}

/**
* @brief getter for structure StageOutput
*
* @param[in] StageInput structure
* @param[in] StageOutput structure
*/
std::vector<DyosOutput::StageOutput> IntegratorSingleStageMetaData::getOutput()
{
  std::vector<DyosOutput::StageOutput> output(1);
  output.front().integrator = this->getStageOutput();
  return output;
}

/**
* @brief resets m_globalDurationParameter
*/
void IntegratorSingleStageMetaData::resetGolbalDurationParameter(void)
{
  // we can determine the total stage duration using any of the controls
  double stageDuration = getStageDuration();
  m_globalDurationParameter->setParameterValue(stageDuration);
}

/**
* @brief getter for vector of state trajectory values
*
* @param[in] state index
* @return constructed vector of state trajectory values
*/
std::vector<double> IntegratorSingleStageMetaData::getStateTrajectory(const unsigned stateIndex)
{
  const unsigned numGridPoints = m_completeGridUnscaled.size();
  std::vector<double> stateTrajectory(numGridPoints);
  for(unsigned i=0; i<numGridPoints; i++){
    utils::Array<double> statesAtTimePoint = m_observer->getStates(i);
    if(statesAtTimePoint.getSize() > 0){
      assert(statesAtTimePoint.getSize() > stateIndex);
      stateTrajectory[i] = statesAtTimePoint[stateIndex];
    }
  }
  return stateTrajectory;
}

/**
* @brief getter for vector of adjoint trajectory values
*
* @param[in] adjoint variable index
* @return constructed vector of adjoint trajectory values
*/
std::vector<double> IntegratorSingleStageMetaData::getAdjointTrajectory(const unsigned adjointIndex)
{
  const unsigned numGridPoints = m_completeGridUnscaled.size();
  std::vector<double> adjointTrajectory(numGridPoints);
  for(unsigned i=0; i<numGridPoints; i++){
    utils::Array<double> adjointsAtTimePoint = m_observer->getAdjoints(i);
    if(adjointsAtTimePoint.getSize() > 0){
      assert(adjointsAtTimePoint.getSize() > adjointIndex);
      adjointTrajectory[i] = adjointsAtTimePoint[adjointIndex];
    }
  }
  return adjointTrajectory;
}

/**
* @brief getter for the vector of StateOutput structures
* @return constructed vector of StateOutput structures
*/
std::vector<DyosOutput::StateOutput> IntegratorSingleStageMetaData::getStateOutputVector()
{
  const unsigned numStates = m_genericEso->getNumStates();
  std::vector<DyosOutput::StateOutput> output(numStates);

  utils::Array<EsoIndex> stateIndices(numStates);
  m_genericEso->getStateIndex(numStates, stateIndices.getData());

  for(unsigned i=0; i<numStates; i++){
    output[i].name = m_genericEso->getVariableNameOfIndex(stateIndices[i]);
    output[i].esoIndex = stateIndices[i];
    output[i].grid.gridPoints = m_completeGridUnscaled;
    output[i].grid.values = getStateTrajectory(i);
  }
  
  return output;
}

/**
* @brief getter for the vector of adjoint output structures
*/
std::vector<DyosOutput::StateGridOutput> IntegratorSingleStageMetaData::getAdjointOutputVector()
{
  const unsigned numStates = m_genericEso->getNumStates();
  const unsigned numAdjoints = numStates;

  std::vector<DyosOutput::StateGridOutput> output(numAdjoints);

  for(unsigned i=0; i<numAdjoints; i++){
    output[i].gridPoints = m_completeGridUnscaled;
    output[i].values = getAdjointTrajectory(i);
  }
  
  return output;
}

/**
* @brief getter for vector of ParameterOutput structures
* @return constructed vector of ParameterOutput structures
*/
std::vector<DyosOutput::ParameterOutput> IntegratorSingleStageMetaData::getDurationOutput()
{
  
  std::vector<DyosOutput::ParameterOutput> output(1);
  IntOptDataMap iODP = this->getIntOptData().front();
  DyosOutput::ParameterOutput out;
  out.esoIndex = 0;
  out.name = "durations";
  out.lowerBound = m_globalDurationParameter->getParameterLowerBound();
  out.upperBound = m_globalDurationParameter->getParameterUpperBound();
  out.isTimeInvariant = true;
  double sumDurations = 0.0;
  std::vector<unsigned> durationParameterIndex;
  out.grids.resize(1);
  for(unsigned i=0; i<m_gridDurations.size(); i++){
    out.grids.front().values.push_back(m_gridDurations[i]->getParameterValue());
    out.grids.front().lagrangeMultiplier.push_back(m_gridDurations[i]->getLagrangeMultiplier());
    durationParameterIndex.push_back(m_gridDurations[i]->getParameterIndex());
    sumDurations += m_gridDurations[i]->getParameterValue();
  }
  out.value = sumDurations;
  
  //collect duration grid output
  out.grids.front().firstSensitivityOutputVector = 
                Parameter::createSensitivityOutputVector(iODP, m_genericEso, durationParameterIndex);
  output.front() = out;
  /*std::vector<DyosOutput::ParameterOutput> output(m_gridDurations.size()+1);
  IntOptDataMap iODP = this->getIntOptData().front();
  output.front().esoIndex = 0;
  output.front().name = "Global final time";
  output.front().lowerBound = m_globalDurationParameter->getParameterLowerBound();
  output.front().upperBound = m_globalDurationParameter->getParameterUpperBound();
  output.front().isTimeInvariant = true;
  double sumDurations = 0.0;
  for(unsigned i=0; i<m_gridDurations.size(); i++)
  {
    std::stringstream sstream;
    sstream<<"Duration "<<i;
    output[i+1].esoIndex = 0;
    output[i+1].name = sstream.str();
    output[i+1].lowerBound = m_gridDurations[i]->getParameterLowerBound();
    output[i+1].upperBound = m_gridDurations[i]->getParameterUpperBound();
    output[i+1].value = m_gridDurations[i]->getParameterValue();
    sumDurations += output[i+1].value;
    output[i+1].isTimeInvariant = true;
    output[i+1].grids.resize(1);
    output[i+1].grids.front().hasFreeDuration = m_gridDurations[i]->getIsDecisionVariable();
    std::vector<unsigned> durationParameterIndex(1, m_gridDurations[i]->getParameterIndex());
    output[i+1].grids.front().firstSensitivityOutputVector = 
                Parameter::createSensitivityOutputVector(iODP, m_genericEso, durationParameterIndex);
  }  
  output.front().value = sumDurations;*/
  return output;
}

/**
* @brief getter for structure IntegratorStageOutput
*
* @param[in] IntegratorStageInput structure
* @return StageOutput structure
*/
DyosOutput::IntegratorStageOutput  IntegratorSingleStageMetaData::getStageOutput()
{
  DyosOutput::IntegratorStageOutput output;
  GenericEso::Ptr genEso = this->getGenericEso();
  std::vector<IntOptDataMap> iod = this->getIntOptData();
  const unsigned numControls = m_controls.size();
  for(unsigned i=0; i<numControls; i++)
  {
    output.parameters.push_back(m_controls[i].getOutput(m_genericEso, iod.front()));
  }

  output.durations = getDurationOutput();

  for(unsigned i=0; i<m_initialValues.size(); i++){
    if(m_initialValues[i]->getUserFlag())
      output.initials.push_back(m_initialValues[i]->getOutput(genEso, iod.front()));
  }

  output.states = getStateOutputVector();
  output.adjoints = getAdjointOutputVector();

  return output;
}

/**
* @brief create vector of global final times
*/
void IntegratorSingleStageMetaData::createFinalTimeVector()
{
  m_finalTimes.push_back(m_gridDurations.front()->getParameterValue());
  for(unsigned i=1; i<m_gridDurations.size(); i++){
    m_finalTimes.push_back(m_finalTimes.back() + m_gridDurations[i]->getParameterValue());
  }
}

/**
* @brief create the final time vector
*
* the final time vector is the union of all controls durations
* In addition all duration parameters are inserted in all controls
*/
void IntegratorSingleStageMetaData::createGridDurationVector()
{
  if(m_controls.empty()){
    DurationParameter::Ptr globalDuration(new DurationParameter(*m_globalDurationParameter));
    m_gridDurations.push_back(globalDuration);
    m_finalTimes.push_back(globalDuration->getParameterValue());
    return;
  }
  std::vector<double> controlGridDurations;
  for(unsigned i=0; i<m_controls.size(); i++){
    std::vector<double> dur = m_controls[i].getStageLength();
    for(unsigned j=1; j<dur.size(); j++){
      dur[j] += dur[j-1];
    }
    if(i==0){
      controlGridDurations = dur;
    }else{
      std::vector<double> mergeDurations(controlGridDurations.size() + dur.size());
      vector<double>::iterator it;
      it = std::set_union (controlGridDurations.begin(), controlGridDurations.end(),
                      dur.begin(), dur.end(), mergeDurations.begin());
      controlGridDurations.assign(mergeDurations.begin(), it);
    }
  }
  vector<double> removeRoundingError = controlGridDurations;
  int wki = 0;
  for(unsigned i=0; i<controlGridDurations.size()-1;i++){
    double compare = controlGridDurations[i+1] - controlGridDurations[i];
    if(fabs(compare)<1e-10){
      vector<double>::iterator it = removeRoundingError.begin();
      advance(it, wki);
      removeRoundingError.erase(it);
    }else{
      wki++;
    }
  }
  controlGridDurations = removeRoundingError;
  // delete entries coming from rounding errors
  for(unsigned i=controlGridDurations.size()-1; i>0;i--){
    controlGridDurations[i] -= controlGridDurations[i-1];
  }

  // create vector of DurationParameters
  for(unsigned i=0; i<controlGridDurations.size(); i++){
    DurationParameter::Ptr gridDurationParameter(new DurationParameter(controlGridDurations[i]));
    //this information will be set later when updating the grids
    gridDurationParameter->setWithSensitivity(false);
    gridDurationParameter->setIsDecisionVariable(false);
    if(controlGridDurations.size() == 1){
      gridDurationParameter = m_globalDurationParameter;
    }
    
    m_gridDurations.push_back(gridDurationParameter);
    for(unsigned j=0; j<m_controls.size(); j++){
      unsigned gridIndex = i;
      m_controls[j].insertDurationParameter(gridDurationParameter, gridIndex);
    }
  }
  
}

/**
* @brief create the global integration grid
*
* The global integration grid is the union of all gridpoints over all unscaled control grids
*/
void IntegratorSingleStageMetaData::createGlobalGrid()
{
  createIntegrationGrids();
  m_completeGridUnscaled.clear();
  m_completeGridUnscaled.push_back(0.0);
  double currentDurationSum = 0.0;
  for(unsigned i=0; i<m_integrationGrids.size(); i++){
    double currentDurationTime = m_gridDurations[i]->getParameterValue();
    for(unsigned j=1; j<m_integrationGrids[i].size(); j++){
      m_completeGridUnscaled.push_back(m_integrationGrids[i][j]*currentDurationTime + currentDurationSum);
    }
    currentDurationSum += currentDurationTime;
    m_completeGridUnscaled.back() = currentDurationSum;
  }
}


/**
* @brief create vector of integration grids
*
* For each duration a grid is created that is scaled to [0,1].
* Furthermore the complete unscaled grid is needed for having the current
* unscaled gridpoint (needed for function checkParameterChange).
* In addition all controls equal grid structures are set.
*/
void IntegratorSingleStageMetaData::createIntegrationGrids()
{
  m_integrationGrids.clear();
  double currentDuration = 0.0;
  const double totalDuration = getStageDuration();
  unsigned plotGridIndex = 0;
  const double stepWidthThreshold = SCALED_GRID_THRESHOLD;
  for(unsigned i=0; i<m_gridDurations.size(); i++){
    std::vector<double> singleIntegrationGrid;
    std::set<double> sortedGrid;
    // we always insert the values even if the controls are non exisiting
    sortedGrid.insert(0.0);
    sortedGrid.insert(1.0);
    for(unsigned j=0; j<m_controls.size(); j++){
      vector<double> grid = m_controls[j].getControlGrid(i);
      sortedGrid.insert(grid.begin(), grid.end());
    }
    if(m_plotGridActivated){
      while(currentDuration>= totalDuration*m_plotGrid[plotGridIndex]){
        plotGridIndex++;
        assert(plotGridIndex < m_plotGrid.size());
      }
      while(currentDuration + m_gridDurations[i]->getParameterValue()
          > m_plotGrid[plotGridIndex]*totalDuration + stepWidthThreshold){
        sortedGrid.insert((m_plotGrid[plotGridIndex]*totalDuration - currentDuration)
                          /m_gridDurations[i]->getParameterValue());
        plotGridIndex++;
        assert(plotGridIndex < m_plotGrid.size());
      }
      currentDuration += m_gridDurations[i]->getParameterValue();
      //delete "double" entries
      std::set<double>::iterator iter;
      std::set<double>::iterator comparedIter;
      for(iter=sortedGrid.end(), iter--; iter!=sortedGrid.begin(); iter=comparedIter){
        comparedIter = iter;
        comparedIter--;
        if(fabs(*iter - *comparedIter) < 1e-10){
          sortedGrid.erase(iter);
        }
      }
    }
    singleIntegrationGrid.assign(sortedGrid.begin(), sortedGrid.end());
    singleIntegrationGrid.front() = 0.0;
    singleIntegrationGrid.back() = 1.0;
    m_integrationGrids.push_back(singleIntegrationGrid);
  }
}

/**
* @brief help function of the constructor
*
* Creates a Jacobian matrix in csc format. This matrix is used to extract the
* matrices m_jacobianFup and m_jacbianFx.
* Since only the Jacobian struct is needed, the values are used for calculation
* of the mapping indices m_jacobianFupMapping and m_jacobianFstatesMapping
*
* @return Jacobian matrix in csc format
*/
CsTripletMatrix::Ptr IntegratorSingleStageMetaData::createJacobianInTriplet()
{
  // get information from Eso
  const EsoIndex num_nonzeros = m_genericEso->getNumNonZeroes();
  utils::Array<EsoIndex> rowIndices(num_nonzeros);
  utils::Array<EsoIndex> colIndices(num_nonzeros);
  utils::Array<double> vals(num_nonzeros);
  // since the values vector is not needed in the constructor,
  // it is used later to retrieve the indices of the Jacobian entries stored in
  // JacobianFu and JacobianFstates
  for(int i=0; i<num_nonzeros;i++){
    vals[i] = i;
  }
  m_genericEso->getJacobianStruct(num_nonzeros, rowIndices.getData(), colIndices.getData());
  assert(rowIndices.getSize() == (unsigned)m_genericEso->getNumNonZeroes());
  assert(colIndices.getSize() == (unsigned)m_genericEso->getNumNonZeroes());
  int numRows = m_genericEso->getNumEquations();
  int numCols = m_genericEso->getNumVariables();
  CsTripletMatrix::Ptr jacobianTriplet(new CsTripletMatrix(numRows,
                                                          numCols,
                                                          num_nonzeros,
                                                          rowIndices.getData(),
                                                          colIndices.getData(),
                                                          vals.getData()));

  return jacobianTriplet;
}
/**
* @brief calculate entries of attributes m_jacobianFstates and m_stateIndices
*
* @param Fu Jacobian matrix in csc format containing mapping indices as values
* @sa createJacobianInCsc
*/
void IntegratorSingleStageMetaData::createJacobianFstates(CsTripletMatrix::Ptr  Fu)
{
  assert(Fu != NULL);

  // get state indicesfrom Eso
  const EsoIndex n_states = m_genericEso->getNumStates();
  std::vector<EsoIndex> stateIndex_x(n_states);
  m_genericEso->getStateIndex(n_states, &stateIndex_x[0]);
  std::vector<int> allRowIndices;
  for (int i=0 ; i<m_genericEso->getNumEquations(); i++) {
    allRowIndices.push_back(i);
  }  
  assert(!allRowIndices.empty());
  assert(!stateIndex_x.empty());
  CsTripletMatrix::Ptr m_jacobianFstatesTriplet = Fu->get_matrix_slices(m_genericEso->getNumEquations(),
                                                                        &allRowIndices[0], n_states,
                                                                        &stateIndex_x[0]);
  m_jacobianFstates = m_jacobianFstatesTriplet->compress();
  // after compression the matrix size is in nzmax
  m_jacobianFstatesMapping.resize(m_jacobianFstates->get_matrix_ptr()->nzmax);
  // extract jacobian value index vector for states
  for(int i=0; i<m_jacobianFstates->get_matrix_ptr()->nzmax; i++){
    m_jacobianFstatesMapping[i] = (int)m_jacobianFstates->get_matrix_ptr()->x[i];
  }
  //utils::copy(m_jacobianFstates->get_matrix_ptr()->nzmax, m_jacobianFstates->get_matrix_ptr()->x[0], m_jacobianFstatesMapping);
}

/**
* @brief calculate entries of attributes m_jacobianFup and m_jacobianFupMapping
*
* @param Fu Jacobian matrix in csc format containing mapping indices as values
* @sa createJacobianInCsc
*/
void IntegratorSingleStageMetaData::createJacobianFup(CsTripletMatrix::Ptr Fu)
{
  assert(Fu != NULL);

  std::vector<int> uIndices(m_controls.size());

  for(unsigned i=0; i<m_controls.size(); i++){
    uIndices[i] = m_controls[i].getEsoIndex();
  }
  std::vector<int> allRowIndices;
  for (int i=0 ; i<m_genericEso->getNumEquations(); i++) {
    allRowIndices.push_back(i);
  }

  if(allRowIndices.empty() || uIndices.empty()){
    //todo: find better solution (eg implement empty matrix for CsCscMatrix
    m_jacobianFup=Fu->compress();
    m_jacobianFup->get_matrix_ptr()->nzmax = 0;
    m_jacobianFupReduced = Fu->compress();
    m_jacobianFupReduced->get_matrix_ptr()->nzmax = 0;
  }
  else{
    // extract the nonzero entries related to the assigned variables
    CsTripletMatrix::Ptr m_jacobianFupTriplet=Fu->get_matrix_slices_keep_size(m_genericEso->getNumEquations(),
                                                                              &allRowIndices[0],
                                                                              m_controls.size(),
                                                                              &uIndices[0]);
    m_jacobianFup=m_jacobianFupTriplet->compress();

    m_jacobianFupTriplet = Fu->get_matrix_slices(m_genericEso->getNumEquations(),
                                                &allRowIndices[0],
                                                 m_controls.size(),
                                                 &uIndices[0]),
    m_jacobianFupReduced = m_jacobianFupTriplet->compress();
  }
  m_jacobianFupMapping.resize(m_jacobianFup->get_matrix_ptr()->nzmax);
  //extract jacobian value index vector for control parameter
  for(int i=0; i<m_jacobianFup->get_matrix_ptr()->nzmax; i++){
    m_jacobianFupMapping[i] = (int)m_jacobianFup->get_matrix_ptr()->x[i];
  }
  m_evaluateFStates = true;
}
/**
* @brief caclulate entries of attribute m_massMatrix
*
* m_massMatrix is the matrix M in the equation M*x_dot = F(x,p,t)
* M is considered to be constant. x contains all variables (differential and algebraic).
* So for the algebraic variables M has zero-filled rows.
*
* @todo for the current used integrators the mass matrix is constant, but there
*       might be integrators capable of handling non constant mass matrices. In
*       that case the structure must be changed (getMassMatrix must update its
*       values). If the matrix is not constant there must be an exception thrown
*       if used by an integrator which can only handle constant mass matrices.
*/
void IntegratorSingleStageMetaData::createMassMatrix()
{
  const int numDiffNonZeros = m_genericEso->getNumDifferentialNonZeroes();
  const int numEquations = m_genericEso->getNumEquations();
  const int numStates = m_genericEso->getNumStates();
  const int numVars = m_genericEso->getNumVariables();
  // todo as numStates and numEquations are given by user, throw an exception here
  // we always have as many states as equations
  if (numStates != numEquations) {
    std::stringstream ss;
    ss << "The number of states must match the number of equations (numstates=" << numStates << ", numEquations=" << numEquations << ")";
    throw (MetaDataException(ss.str()));
  }
  // triplet = 1 creates a sparse matrix in coordinate format

  utils::Array<EsoDouble> values(numDiffNonZeros);
  m_genericEso->getDiffJacobianValues(numDiffNonZeros, values.getData());
  utils::Array<EsoIndex> massMatrixRows(numDiffNonZeros), massMatrixCols(numDiffNonZeros);
  m_genericEso->getDiffJacobianStruct(numDiffNonZeros, massMatrixRows.getData(), massMatrixCols.getData());

  CsTripletMatrix::Ptr massMatrix(new CsTripletMatrix(numEquations,
                                                         numVars,
                                                         values.getSize(),
                                                         massMatrixRows.getData(),
                                                         massMatrixCols.getData(),
                                                         values.getData()));
  std::vector<EsoIndex> rowIndices(numEquations), colIndices(numStates);
  //keep all equations
  for(int i=0; i<numEquations; i++){
    rowIndices[i] = i;
  }
  //just keep the colums of the states
  m_genericEso->getStateIndex(numStates, &colIndices[0]);

  massMatrix=massMatrix->get_matrix_slices(numEquations, &rowIndices[0], numStates, &colIndices[0]);
  m_massMatrix = massMatrix->compress();
  m_massMatrixCoo = massMatrix;
}

/**
* @brief update all controls and add new active parameters to the parameter vector
*/
void IntegratorSingleStageMetaData::updateControlParameter()
{
  const double absoluteStartTime = m_completeGridUnscaled[m_currentIndexCompleteGridUnscaled];
  bool changedParameter = false;
  for(unsigned i=0; i<m_controls.size(); i++){
    if(m_controls[i].checkParameterChange(absoluteStartTime)){
      m_controls[i].changeActiveParameter();
      changedParameter = true;
      vector<Parameter::Ptr> newActiveParameter = m_controls[i].getActiveParameter();
      for(unsigned j=0; j<newActiveParameter.size(); j++){
        if(newActiveParameter[j]->getWithSensitivity() && newActiveParameter[j]->isOriginal()){
          // if not already set, set the parameter index
          if(newActiveParameter[j]->getParameterIndex() < 0){
            newActiveParameter[j]->setParameterIndex(m_currentParamsWithSens.size());
          }
        }
      }
      //uptdate ut[i], for the value now has changed
      m_ut[i] = m_controls[i].getControlDerivative();
      // this works for linear and constant. And if the grid approximation changes from one
      // grid to another. e.g. constant -> linear
      for(unsigned j=0; j<newActiveParameter.size(); j++){
        if(newActiveParameter[j]->getWithSensitivity() ){
          addNewParameterToParameterWithSensitivityVector(newActiveParameter[j]);
        }
      }
    }
  }
  if(changedParameter){
    CsCscMatrix::Ptr fu = this->getJacobianFupReduced();
    //reset m_rhsStatesDtime
    for(unsigned i=0; i<m_rhsStatesDtime.getSize(); i++){
      m_rhsStatesDtime[i] = 0.0;
    }
    const int retval = cs_gaxpy(fu->get_matrix_ptr(), m_ut.getData(), m_rhsStatesDtime.getData()); // gaxpy: y = A*x + y
    assert(retval);
  }
}

/**
* @brief update all control parameters - remove parameters that are no longer active
*
* since the integration goes backwards some parameters are not active at a previous
* time point and have to be removed from the parameter vector. Others need to be set as active.
* At the first call (the last interval on the current grid) nothing is to be done.
*/
void IntegratorSingleStageMetaData::updateControlParameterBackwardsIntegration()
{
  assert(m_currentIndexCompleteGridUnscaled > 0);
  const double absoluteStartTime = m_completeGridUnscaled[m_currentIndexCompleteGridUnscaled-1];
  for(unsigned i=0; i<m_controls.size(); i++){
    if(m_controls[i].checkParameterChangeReverse(absoluteStartTime)){
      vector<Parameter::Ptr> oldParameters = m_controls[i].getActiveParameter();
      
      if(oldParameters.back()->getWithSensitivity() && oldParameters.back()->isOriginal()){
        removeParameterFromParameterWithSensitivityVector(oldParameters.back());
      }
      m_controls[i].changeActiveParameterReverse();
      vector<Parameter::Ptr> previousParameters = m_controls[i].getActiveParameter();
      
    //activate all previous parameters
      for(unsigned j=0; j<previousParameters.size(); j++){
        previousParameters[j]->setParameterSensActivity(true);
      }// for j
    }
  }
}

/**
* @brief add new active final time parameters to the parameter vector
*/
void IntegratorSingleStageMetaData::updateDurationParameter()
{
  DurationParameter::Ptr newDuration = m_gridDurations[m_currentIndexIntegrationGrid];
  if(newDuration->getWithSensitivity()){
    addNewParameterToParameterWithSensitivityVector(newDuration);
    newDuration->setParameterSensActivity(true);
  }
  if(m_currentIndexIntegrationGrid > 0){
    m_gridDurations[m_currentIndexIntegrationGrid-1]->setParameterSensActivity(false);
  }
}

/**
* @brief remove obsolete final time parameters from parameter vector and activate the current
*        final time parameter during a backwards integration
* @sa IntegratorSingleStageMetaData::updateDurationParameter,
*     IntegratorSingleStageMetaData::updateControlParameterBackwardsIntegration
*/
void IntegratorSingleStageMetaData::updateDurationParameterBackwardsIntegration()
{
  assert(m_currentIndexIntegrationGrid < m_gridDurations.size() - 1);
  DurationParameter::Ptr nextDurationParameter = m_gridDurations[m_currentIndexIntegrationGrid + 1];
  if(nextDurationParameter->getWithSensitivity()){
    removeParameterFromParameterWithSensitivityVector(nextDurationParameter);
  }
  m_gridDurations[m_currentIndexIntegrationGrid]->setParameterSensActivity(true);
}


/**
* @brief sorts new active parameters to the right position
*
* If the optimizer changes the global grid (e.g. by making one duration bigger) the order
* of the activation of sensitivity parameters may change. However the optimizer needs the vector
* of sensitivity parameters after the last integration step to be in the same order for every
* iteration.
* So whenever a parameter is activated for the first time, its position is stored. This method
* sorts the parameter vector with respect to the stored parameter index.
*/
void IntegratorSingleStageMetaData::sortControlParameters()
{
  std::sort(m_currentParamsWithSens.begin(), m_currentParamsWithSens.end(), Parameter::compareParameter);
}

/**
* @copydoc IntegratorMetaData::setStartTime
*/
void IntegratorSingleStageMetaData::setStepStartTime(const double startTime)
{
  assert(startTime >= 0.0);
  assert(startTime < 1.0);
  m_integrationStepTimes.startTime = startTime;
}

/**
* @copydoc IntegratorMetaData::getStartTime
*/
double IntegratorSingleStageMetaData::getStepStartTime()const
{
  return m_integrationStepTimes.startTime;
}

/**
* @copydoc IntegratorMetaData::setEndTime
*/
void IntegratorSingleStageMetaData::setStepEndTime(const double endTime)
{
  assert(endTime > 0.0);
  assert(endTime <= 1.0);
  m_integrationStepTimes.endTime = endTime;
}

/**
* @copydoc IntegratorMetaData::getEndTime
*/
double IntegratorSingleStageMetaData::getStepEndTime() const
{
  return m_integrationStepTimes.endTime;
}

/**
* @copydoc IntegratorMetaData::getNumStages
*/
unsigned IntegratorSingleStageMetaData::getNumStages() const
{
  return 1;
}

/**
* @copydoc IntegratorMetaData::setCurrentTime
*/
void IntegratorSingleStageMetaData::setStepCurrentTime(const double time)
{
  assert(m_integrationStepTimes.startTime <= time);
  //it might be that time is greater than endTime due to numeric errors
  //for switching systems time might be greater than endtime so for now remove assert and min function
  //const double threshold = 1e-10;
  //assert(m_integrationStepTimes.endTime + threshold >= time);
  //m_integrationStepTimes.currentTime = std::min(time, m_integrationStepTimes.endTime);
  m_integrationStepTimes.currentTime = time;
}

/**
* @copydoc IntegratorMetaData::getCurrentTime
*/
double IntegratorSingleStageMetaData::getStepCurrentTime() const
{
  return m_integrationStepTimes.currentTime;
}


/**
* @copydoc IntegratorMetaData::getDuration
*/
double IntegratorSingleStageMetaData::getIntegrationGridDuration() const
{
  return m_integrationBlockTimes.relativeDuration;
}

/**
* @copydoc IntegratorMetaData::getNumCurrentSensitivityParameters
*/
int IntegratorSingleStageMetaData::getNumCurrentSensitivityParameters() const
{
  return m_currentParamsWithSens.size();
}

/**
* @copydoc IntegratorMetaData::getNumIntegrationIntervals
*/
int IntegratorSingleStageMetaData::getNumIntegrationIntervals() const
{
  // each integration interval has its own integration grid, so the number of integration intervals
  // equals the number of integration grids
  return m_integrationGrids.size();
}



/**
* @copydoc IntegratorMetaData::setAllInitialValuesFree
*
* @warning this function will change the order of parameters
*/
void IntegratorSingleStageMetaData::setAllInitialValuesFree()
{
  //assign all initials as decision variables
  const unsigned numIVs = m_initialValues.size();
  for(unsigned int i=0; i < numIVs; i++){
    m_initialValues[i]->setWithSensitivity(true);
  }

  //reassign the parameter indices of all parameters
  defineSensParameters(m_parametersWithSensitivity);
  defineDecisionVariables(m_decisionVariables);
  assignParameterIndices();
}

/**
* @copydoc IntegratorMetaData::initializeForNewIntegrationStep
*/
void IntegratorSingleStageMetaData::initializeForNewIntegrationStep()
{
  // determine the new interval range according to the global grid
  m_integrationStepTimes.startTime = m_integrationStepTimes.endTime;
  m_currentIndexIntegrationGridpoint++;
  assert(m_currentIndexIntegrationGridpoint < m_integrationGrids[m_currentIndexIntegrationGrid].size());

  const int index1 = m_currentIndexIntegrationGrid;
  const int index2 = m_currentIndexIntegrationGridpoint;
  m_integrationStepTimes.endTime = m_integrationGrids[index1][index2];
  updateControlParameter();
  m_currentIndexCompleteGridUnscaled++;

  assert(m_currentIndexCompleteGridUnscaled < m_completeGridUnscaled.size());
}

/**
* @copydoc IntegratorMetaData::initializeForNewBackwardsIntegrationStep
*/
void IntegratorSingleStageMetaData::initializeForNewBackwardsIntegrationStep()
{
  // determine the new interval range according to the global grid
  int index1 = m_currentIndexIntegrationGrid;
  int index2 = m_currentIndexIntegrationGridpoint;
  m_integrationStepTimes.endTime = m_integrationGrids[index1][index2];
  assert(m_currentIndexIntegrationGridpoint > 0);
  m_currentIndexIntegrationGridpoint--;

  index1 = m_currentIndexIntegrationGrid;
  index2 = m_currentIndexIntegrationGridpoint;
  m_integrationStepTimes.startTime = m_integrationGrids[index1][index2];


  if(m_currentIndexCompleteGridUnscaled < m_completeGridUnscaled.size()-1){
    updateControlParameterBackwardsIntegration();
  }
  m_currentIndexCompleteGridUnscaled--;
  assert(m_currentIndexCompleteGridUnscaled >= 0);
}


/**
* @copydoc IntegratorMetaData::initializeForNextIntegration
*/
void IntegratorSingleStageMetaData::initializeForNextIntegration()
{
  const double absoluteStartTime = m_finalTimes[m_currentIndexIntegrationGrid];
  m_integrationBlockTimes.absoluteStartTime = absoluteStartTime;
  m_currentIndexIntegrationGrid++;
  assert(m_currentIndexIntegrationGrid < m_integrationGrids.size());

  m_integrationBlockTimes.relativeDuration = m_gridDurations[m_currentIndexIntegrationGrid]->getParameterValue();
  m_integrationStepTimes.startTime = 0.0;
  m_integrationStepTimes.endTime = 0.0;
  m_currentIndexIntegrationGridpoint = 0;


  // update all controls and add new active parameters to the parameter vector
  // this must be done before the duration parameters, since the function changeActiveParameter
  // updates the state of the control, which is essential for retrieving the right duration parameter
  updateControlParameter();

  updateDurationParameter();
}

/**
* @copydoc IntegratorMetaData::initializeForPreviousIntegrationBlock
*/
void IntegratorSingleStageMetaData::initializeForPreviousIntegrationBlock()
{
  m_currentIndexIntegrationGrid--;
  assert(m_currentIndexIntegrationGrid >= 0);
  //get previous start time
  double absoluteStartTime = 0.0;
  if(m_currentIndexIntegrationGrid > 0){
     absoluteStartTime = m_finalTimes[m_currentIndexIntegrationGrid-1];
  }
  m_integrationBlockTimes.absoluteStartTime = absoluteStartTime;


  m_integrationBlockTimes.relativeDuration = m_gridDurations[m_currentIndexIntegrationGrid]->getParameterValue();

  m_integrationStepTimes.startTime = m_integrationGrids[m_currentIndexIntegrationGrid].back();
  m_integrationStepTimes.endTime = m_integrationStepTimes.startTime;
  m_currentIndexIntegrationGridpoint = m_integrationGrids[m_currentIndexIntegrationGrid].size() - 1;

  updateDurationParameterBackwardsIntegration();
  
  //remove all original parameters from the current grid and update the grid Index
  for(unsigned i=0; i<m_controls.size(); i++){
    std::vector<Parameter::Ptr> gridParameter = m_controls[i].getActiveParameter();
    for(unsigned j=0; j<gridParameter.size(); j++){
      if(gridParameter[j]->isOriginal() && gridParameter[j]->getWithSensitivity()){
        removeParameterFromParameterWithSensitivityVector(gridParameter[j]);
      }
    }

    //activate all previous parameters
    m_controls[i].changeActiveParameterReverse();
    std::vector<Parameter::Ptr> previousParameters = m_controls[i].getActiveParameter();
    for(unsigned j=0; j<previousParameters.size(); j++){
      previousParameters[j]->setParameterSensActivity(true);
    }// for j
    
  }//for
  
}

/**
* @copydoc IntegratorMetaData::initializeForFirstIntegration
*/
void IntegratorSingleStageMetaData::initializeForFirstIntegration()
{
  //reset old data
  m_currentIndexCompleteGridUnscaled = 0;
  m_integrationStepTimes.startTime = 0.0;
  m_integrationStepTimes.endTime = 0.0;
  m_integrationStepTimes.currentTime = 0.0;
  m_currentIndexIntegrationGrid = 0;
  m_currentIndexIntegrationGridpoint = 0;

  m_currentSensitivities.clear();
  m_finalTimes.clear();
  m_currentParamsWithSens.clear();

  createFinalTimeVector();
  // createGlobalGrid implicitly calls createIntegrationGrid
  // by doing that both grids are consistent
  createGlobalGrid();
  assert(m_finalTimes.size() > 0);
  m_integrationBlockTimes.relativeDuration = m_gridDurations[0]->getParameterValue();
  m_integrationBlockTimes.absoluteStartTime = 0.0;

  const unsigned numDiffVars = m_genericEso->getNumDifferentialVariables();
  assert(m_initialValues.size() == numDiffVars);
  std::vector<EsoIndex> diffVarIndices(numDiffVars), userSetDiffVarIndices, originalDiffVarIndices;
  std::vector<EsoDouble> userSetDiffVals, originalDiffVals;
  m_genericEso->getDifferentialIndex(numDiffVars, &diffVarIndices[0]);
  // add initial parameters to the beginning of the parameters vector
  for(unsigned i=0; i<m_initialValues.size(); i++){
    assert(diffVarIndices[i] == m_initialValues[i]->getEsoIndex());
    if( m_initialValues[i]->getWithSensitivity() ){
      // if not already set, set the parameter index
      m_initialValues[i]->setParameterIndex(m_currentParamsWithSens.size());
      //todo change initial values to shared pointer
      Parameter::Ptr activeInitialParameter = m_initialValues[i];
      addNewParameterToParameterWithSensitivityVector(activeInitialParameter);
    }
    if(m_initialValues[i]->getUserFlag() == true){
      userSetDiffVarIndices.push_back(m_initialValues[i]->getEsoIndex());
      userSetDiffVals.push_back(m_initialValues[i]->getParameterValue());
    }
    else{
      originalDiffVarIndices.push_back(m_initialValues[i]->getEsoIndex());
      originalDiffVals.push_back(m_initialValues[i]->getParameterValue());
    }
  }


  utils::Array<int> controlIndices(m_controls.size());
  for(unsigned i=0; i<m_controls.size(); i++){
    m_controls[i].initializeForFirstIntegration();
    controlIndices[i] = m_controls[i].getEsoIndex();
  }
  // we need to update the eso such that it has the updated initial values.
  EsoIndex *userSetDiffVarIndicesPtr = 0, *originalDiffVarIndicesPtr = 0;
  EsoDouble *userSetDiffValPtr = 0, *originalDiffValPtr = 0;
  assert(userSetDiffVals.size() == userSetDiffVarIndices.size());
  assert(originalDiffVals.size() == originalDiffVarIndices.size());
  if(!userSetDiffVals.empty()){
    userSetDiffVarIndicesPtr = &userSetDiffVarIndices[0];
    userSetDiffValPtr = &userSetDiffVals[0];
  }
  if(!originalDiffVals.empty()){
    originalDiffVarIndicesPtr = &originalDiffVarIndices[0];
    originalDiffValPtr = &originalDiffVals[0];
  }
  setControlsToEso();
  //reset diff vals that are not user set - some might be overridden by setInitialValues,
  //but otherwise they might contain data from previous runs
  m_genericEso->setVariableValues(originalDiffVals.size(),
                                  originalDiffValPtr,
                                  originalDiffVarIndicesPtr);
  m_evaluateFStates = true;
  GenericEso::RetFlag retFlag = m_genericEso->setInitialValues(userSetDiffVarIndices.size(),
                                                               userSetDiffVarIndicesPtr,
                                                               userSetDiffValPtr,
                                                               controlIndices.getSize(),
                                                               controlIndices.getData());
  m_evaluateFStates = true;
  if(retFlag != GenericEso::OK){
    std::stringstream sstream;
    sstream<<"An error occurred while setting initial values. This error probably has its origin in the ";
    sstream<<"initial section of the model. If any initial equation contains any parameters, following ";
    sstream<<"restrictions are made:"<<std::endl;
    sstream<<"1. Each equation must have at least one degree of freedom.";
    sstream<<" Do not specify initial value parameter and all controls of an initial equation at the same time"<<std::endl;
    sstream<<"2. The equations containing parameters must be explicitly solvable."<<std::endl;
    throw InitialSectionException(sstream.str());
  }
  updateDurationParameter();

  // add active parameters to parameters vector
  for(unsigned i=0; i<m_controls.size(); i++){
    m_ut[i] = m_controls[i].getControlDerivative();
    vector<Parameter::Ptr> firstActiveParameter = m_controls[i].getActiveParameter();
    for(unsigned j=0; j<firstActiveParameter.size(); j++){
      if(firstActiveParameter[j]->getWithSensitivity()){
        // if not already set, set the parameter index
        if(firstActiveParameter[j]->getParameterIndex() < 0){
          firstActiveParameter[j]->setParameterIndex(m_currentParamsWithSens.size());
        }
        addNewParameterToParameterWithSensitivityVector(firstActiveParameter[j]);
      }
    }
  }
  CsCscMatrix::Ptr fu = this->getJacobianFupReduced();
  //reset m_rhsStatesDtime
  for(unsigned i=0; i<m_rhsStatesDtime.getSize(); i++){
    m_rhsStatesDtime[i] = 0.0;
  }
  if (m_ut.getSize() > 0) {
    const int retval = cs_gaxpy(fu->get_matrix_ptr(), m_ut.getData(), m_rhsStatesDtime.getData()); // gaxpy: y = A*x + y
    assert(retval);
  }
}

void IntegratorSingleStageMetaData::initializeMatrices()
{
  CsTripletMatrix::Ptr Fu = createJacobianInTriplet();
  createJacobianFstates(Fu);
  createJacobianFup(Fu);
  createMassMatrix();
}

/**
* @copydoc IntegratorMetaData::getJacobianFup
*/
CsCscMatrix::Ptr IntegratorSingleStageMetaData::getJacobianFup()
{
  //utils::WrappingArray<double> values(m_jacobianFup->nzmax, m_jacobianFup->x);
  m_genericEso->getJacobianValues(m_jacobianFup->get_matrix_ptr()->nzmax,
                                  m_jacobianFup->get_matrix_ptr()->x,
                                  &m_jacobianFupMapping[0]);
  return m_jacobianFup;
}

CsCscMatrix::Ptr IntegratorSingleStageMetaData::getJacobianFupReduced()
{
  if(m_jacobianFupReduced->get_matrix_ptr()->nzmax > 0){
    m_genericEso->getJacobianValues(m_jacobianFupReduced->get_matrix_ptr()->nzmax,
                                  m_jacobianFupReduced->get_matrix_ptr()->x,
                                  &m_jacobianFupMapping[0]);
  }
  return m_jacobianFupReduced;
}
/**
* @copydoc IntegratorMetaData::getJacobianFstates
*/
CsCscMatrix::Ptr IntegratorSingleStageMetaData::getJacobianFstates()
{
  if(m_evaluateFStates){
    //utils::WrappingArray<double> values(m_jacobianFstates->nzmax, m_jacobianFstates->x);
    m_genericEso->getJacobianValues(m_jacobianFstates->get_matrix_ptr()->nzmax,
                                    m_jacobianFstates->get_matrix_ptr()->x,
                                    &m_jacobianFstatesMapping[0]);
  }
  m_evaluateFStates = false;
  return m_jacobianFstates;
}
/**
* @copydoc IntegratorMetaData::getGenericEso
*/
GenericEso::Ptr IntegratorSingleStageMetaData::getGenericEso()
{
  return m_genericEso;
}

/**
* @copydoc IntegratorMetaData::setStates
*/
void IntegratorSingleStageMetaData::setStates(const utils::Array<double> &states)
{
  m_genericEso->setStateValues(states.getSize(), states.getData());
  m_evaluateFStates = true;
  setNonlinearConstraints();
  if (m_observer) {
    m_observer->setStates(m_currentIndexCompleteGridUnscaled, states);
  }
}

void IntegratorSingleStageMetaData::storeInitialStates(const utils::Array<double> &states)
{
  setNonlinearConstraints();
  if(m_observer){
    m_observer->setStates(0, states);
  }
}

/**
* @copydoc IntegratorMetaData::setSensitivities
*/
void IntegratorSingleStageMetaData::setSensitivities(const utils::Array<double> &sens)
{
  m_currentSensitivities.resize(sens.getSize());
  m_currentSensitivities.assign(sens.getData(), sens.getData() + sens.getSize());
  setNonlinearConstraintsGradients();
  for(unsigned i=0; i<m_currentParamsWithSens.size(); i++){
    m_currentParamsWithSens[i]->setIsInitialized(true);
  }
}

/**
* @copydoc IntegratorMetaData::setAdjointsAndLagrangeDerivatives
*/
void IntegratorSingleStageMetaData::setAdjointsAndLagrangeDerivatives(
                                                const utils::Array<double> &adjoints,
                                                const utils::Array<double> &lagrangeDerivatives)
{
  setAdjointsAndLagrangeDerivativesNoObserver(adjoints, lagrangeDerivatives);

  if (m_observer) {
    utils::WrappingArray<const double> firstOrderAdjoints(m_genericEso->getNumStates(), adjoints.getData());
    m_observer->setAdjoints(m_currentIndexCompleteGridUnscaled, firstOrderAdjoints);
  }
}

/**
* @copydoc IntegratorMetaData::setAdjointsAndLagrangeDerivatives
*/
void IntegratorSingleStageMetaData::setAdjointsAndLagrangeDerivativesNoObserver(
                                                const utils::Array<double> &adjoints,
                                                const utils::Array<double> &lagrangeDerivatives)
{
  assert(adjoints.getSize() == m_adjoints.size());
  m_adjoints.assign(adjoints.getData(), adjoints.getData() + adjoints.getSize());
  const unsigned numSensPars = getNumSensitivityParameters();
  assert(lagrangeDerivatives.getSize() == numSensPars ||
         lagrangeDerivatives.getSize() == m_lagrangeDers.size());

  const unsigned numLagrangeDers = lagrangeDerivatives.getSize();
  for(unsigned i=0; i<numLagrangeDers; i++){
    m_lagrangeDers[i] += lagrangeDerivatives[i];
  }
}

void IntegratorSingleStageMetaData::setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers)
{
  //set LagrangeMultipliers to the intOptDataMap
  IntOptDataMap intOptDataMap = m_intOptData.front();
  IntOptDataMap::iterator dataMapIter;
  for(dataMapIter = intOptDataMap.begin(); dataMapIter != intOptDataMap.end(); dataMapIter++){
    SavedOptDataStruct entry = dataMapIter->second;
    assert(entry.nonLinConstIndices.size() == entry.lagrangeMultipliers.size());
    for(unsigned i=0; i<entry.nonLinConstIndices.size(); i++){
      //only set local constraint multipliers - global multipliers are handled on multistage level
      if(entry.nonLinConstIndices[i] < nonLinMultipliers.getSize()){
        entry.lagrangeMultipliers[i] = nonLinMultipliers[entry.nonLinConstIndices[i]];
      }
    }
    dataMapIter->second = entry;
  }
  m_intOptData.front() = intOptDataMap;
}

/**
* @copydoc IntegratorMetaData::setInitialLagrangeDerivatives
*/
void IntegratorSingleStageMetaData::setInitialLagrangeDerivatives()
{
  const unsigned numAdjoints = m_adjoints.size();
  const int numEquations = m_genericEso->getNumEquations();
  const unsigned numAdjVectors = numAdjoints/(unsigned)numEquations;
  const unsigned numSensPars = getNumSensitivityParameters();



  for(unsigned j=0; j<numAdjVectors; j++){
    if(numSensPars > 0){
      assert(m_adjoints.size() >= (j+1)*numEquations);
      utils::WrappingArray<double> adjointsSlice(numEquations, &m_adjoints[j*numEquations]);
      assert(m_lagrangeDers.size() >= (j+1)*numSensPars);
      utils::WrappingArray<double> lagrangeDersSlice(numSensPars, &m_lagrangeDers[j*numSensPars]);
      updateInitialLagrangeDerivatives(m_initialValues,
                                       m_parametersWithSensitivity,
                                       adjointsSlice,
                                       lagrangeDersSlice);
    }
  }
}


/**
* @copydoc IntegratorMetaData::updateAdjoints
*/
void IntegratorSingleStageMetaData::updateAdjoints()
{
  const unsigned timeIndex = m_currentIndexCompleteGridUnscaled;

  if(m_intOptData[0].count(timeIndex) > 0){
    unsigned numConstraints = m_intOptData[0][timeIndex].nonLinConstStateIndices.size();
    assert(m_intOptData[0][timeIndex].lagrangeMultipliers.size() == numConstraints);
    for(unsigned i=0; i<numConstraints; i++){
      unsigned constraintStateIndex = m_intOptData[0][timeIndex].nonLinConstStateIndices[i];
      assert(constraintStateIndex < m_adjoints.size());
      m_adjoints[constraintStateIndex] += m_intOptData[0][timeIndex].lagrangeMultipliers[i];
    }
  }
}

/**
* @copydoc IntegratorMetaData::initializeAtDuration
*/
void IntegratorSingleStageMetaData::initializeAtStageDuration()
{
  m_adjoints.assign(m_genericEso->getNumStates(), 0.0);
  m_lagrangeDers.assign(getNumSensitivityParameters(), 0.0);
  
  //initialize m_nextReverseTimePoint
  for(unsigned i=0; i<m_controls.size(); i++){
    m_controls[i].setReverseTimePoint();
  }
}

/**
* @copydoc IntegratorMetaData::calculateRhsStates
*/
void IntegratorSingleStageMetaData::calculateRhsStates(const utils::Array<double> & inputStates,
                                                       utils::Array<double> &rhsStates)
{
  // set state values
  m_genericEso->setStateValues(inputStates.getSize(), inputStates.getData());
  m_evaluateFStates = true;

  // set control values
  setControlsToEso();

  utils::Array<double>dummyDerivatives(m_genericEso->getNumDifferentialVariables(), 0.0);

  // set all derivatives to 0
  m_genericEso->setDerivativeValues(dummyDerivatives.getSize(), dummyDerivatives.getData());

  // retrieve the residuals (having now the values of the right hand side)
  if(m_genericEso->getAllResiduals(rhsStates.getSize(), rhsStates.getData()) != GenericEso::OK){
    throw EsoEvaluationErrorException();
  }
  
  for(unsigned i=0; i<rhsStates.getSize(); i++){
    rhsStates[i] = rhsStates[i]*getIntegrationGridDuration();
  }
}

/**
* @copydoc IntegratorMetaData::calculateSensitivitiesForward1stOrder
*/
void IntegratorSingleStageMetaData::calculateSensitivitiesForward1stOrder(const utils::Array<double> &inputSensitivities,
                                                                          utils::Array<double> &rhsSensitivities)
{
  const int numEquations = m_genericEso->getNumEquations();
    
  utils::Array<double>dummyDerivatives(m_genericEso->getNumDifferentialVariables(), 0.0);

  // set all derivatives to 0 (for sensitivity calculation of duration parameters)
  m_genericEso->setDerivativeValues(dummyDerivatives.getSize(), dummyDerivatives.getData());

  assert(rhsSensitivities.getSize() == numEquations*m_currentParamsWithSens.size());
  assert(rhsSensitivities.getSize() == inputSensitivities.getSize());

  utils::Array<double> inputSens(inputSensitivities); //non-const copy of inputSensitivities

  // pointer for iterating over the complete output field
  double *rhsSensitivitiesIterator = rhsSensitivities.getData();
  // pointer for iterating over the complete input field
  double *inputSensitivitiesIterator = inputSens.getData();
  CsCscMatrix::Ptr jacobianFstates = getJacobianFstates();
  for(unsigned i=0; i<m_currentParamsWithSens.size(); i++){
    assert(int(rhsSensitivities.getSize()) > (rhsSensitivitiesIterator - rhsSensitivities.getData()) );
    assert(int(inputSens.getSize()) > (inputSensitivitiesIterator - inputSens.getData()) );
    //wrap pointers for function call
    utils::WrappingArray<double> rhsSensitivitiesSlice(numEquations, rhsSensitivitiesIterator);
    utils::WrappingArray<double> inputSensitivitiesSlice(numEquations, inputSensitivitiesIterator);
    m_currentParamsWithSens[i]->getRhsSensForward1stOrder(inputSensitivitiesSlice,
                                                          jacobianFstates->get_matrix_ptr(),
                                                          this,
                                                          rhsSensitivitiesSlice);
    // increment pointer with numEquations (output slice size)
    rhsSensitivitiesIterator += numEquations;
    // increment pointer with numStates (input slice size)
    inputSensitivitiesIterator += numEquations;
  }
}

/**
* @copydoc IntegratorMetaData::calculateAdjointsReverse1stOrder
*/
void IntegratorSingleStageMetaData::calculateAdjointsReverse1stOrder
                                               (const utils::Array<double> &states,
                                                const utils::Array<double> &adjoints,
                                                      utils::Array<double> &rhsAdjoints,
                                                      utils::Array<double> &rhsDers)
{
  m_genericEso->setStateValues(states.getSize(), states.getData());
  m_evaluateFStates = true;

  calculateRhsAdjoints(adjoints, rhsAdjoints);

  assert(rhsDers.getSize() >= getNumSensitivityParameters());
  for(unsigned i=0; i<m_currentParamsWithSens.size(); i++){
    unsigned paramIndex = m_currentParamsWithSens[i]->getParameterIndex();
    rhsDers[paramIndex] = m_currentParamsWithSens[i]->getRhsDerReverse1stOrder(adjoints, this);
  }

}

/**
* @brief calculate rhsAdjoints = (adjoints)T * df_dx
* @param[in] adjoints must be of size numStates
* @param rhsAdjoints array receiving the result of the calculation - must be of size numStates
*/
void IntegratorSingleStageMetaData::calculateRhsAdjoints
                                   (const utils::Array<double> &adjoints,
                                          utils::Array<double> &rhsAdjoints)
{
  // update Jacobian
  getJacobianFstates();
  // multiplication df_dx * y
  const int allocateValues = 1;
  CsCscMatrix::Ptr jacFStatesTransposed(new CsCscMatrix(cs_transpose(m_jacobianFstates->get_matrix_ptr(), 
                                                                     allocateValues),
                                                        true));
  const unsigned numStates = m_genericEso->getNumStates();
  assert(adjoints.getSize() == numStates);
  assert(rhsAdjoints.getSize() == numStates);
  assert((unsigned)jacFStatesTransposed->get_n_cols() == numStates);
  for(unsigned i=0; i<numStates; i++){
    rhsAdjoints[i] = 0.0;
  }
  const int retval = cs_gaxpy(jacFStatesTransposed->get_matrix_ptr(), adjoints.getData(), rhsAdjoints.getData()); // gaxpy: y = A*x + y

  // scale with final time
  for (unsigned i=0; i<rhsAdjoints.getSize(); i++){
    rhsAdjoints[i] =  rhsAdjoints[i] * getIntegrationGridDuration();
  }

  assert(retval == 1);
}


void IntegratorSingleStageMetaData::calculateRhsStatesDtime(utils::Array<double> &rhsStatesDtime)
{
  assert(rhsStatesDtime.getSize() == m_rhsStatesDtime.getSize());
  for(unsigned i=0; i<rhsStatesDtime.getSize(); i++){
    rhsStatesDtime[i] = m_rhsStatesDtime[i];
  }
}

/**
* @copydoc IntegratorMetaData::evaluateInitialValues
*
* initialStates are simply the states currently set in the ESO
*/
void IntegratorSingleStageMetaData::evaluateInitialValues(utils::Array<double> &initialStates,
                                                          utils::Array<double> &initialSensitivities)
{
  //evaluate intitial states
  assert(initialStates.getSize() == (unsigned)m_genericEso->getNumStates());
  setControlsToEso();
  m_genericEso->getInitialStateValues(initialStates.getSize(), initialStates.getData());

  if(initialSensitivities.getSize() == 0){
    return;
  }
  // evaluate intitial sensitivities of newly set sensitivity parameters
  // and set values of m_currentSensitivities as the initial values of the old parameters
  assert(m_currentSensitivities.size() <= initialSensitivities.getSize());
  const unsigned numParameters = getNumCurrentSensitivityParameters();
  const unsigned numEquations = m_genericEso->getNumEquations();
  assert(numEquations != 0);
  unsigned currentSensIndex = 0;
  assert(initialSensitivities.getSize() == numParameters*numEquations);

  // each parameter provides an array of initial values of length numEquations
  double *initialSensitivitiesIterator = initialSensitivities.getData();
  for(unsigned i = 0; i<numParameters; i++){
    utils::WrappingArray<double> initialSensSlice(numEquations, initialSensitivitiesIterator);
    if(m_currentParamsWithSens[i]->getIsInitialized() == true){
      for(unsigned j=0; j<numEquations; j++, currentSensIndex++){
        assert(currentSensIndex < m_currentSensitivities.size());
        initialSensSlice[j] = m_currentSensitivities[currentSensIndex];
      }
    }
    else{
      m_currentParamsWithSens[i]->getInitialValuesForIntegration(initialSensSlice, m_genericEso);
    }
    initialSensitivitiesIterator += numEquations;
  }
  // ugly workaround: setSensitivities usually is called at the end of an integration step
  // thats why after initialization for integration the grid index is never 0
  // however we need to store the sensitivies at start point, too
  if(m_currentIndexCompleteGridUnscaled == 1){
    m_currentIndexCompleteGridUnscaled--;
    setSensitivities(initialSensitivities);
    m_currentIndexCompleteGridUnscaled++;
  }
}

/**
* @copydoc IntegratorMetaData::getMMatrix
*/
CsCscMatrix::Ptr IntegratorSingleStageMetaData::getMMatrix(void) const
{
  return m_massMatrix;
}
/**
* @copydoc IntegratorMetaData::getMMatrixCoo
*/
CsTripletMatrix::Ptr IntegratorSingleStageMetaData::getMMatrixCoo(void) const
{
  return m_massMatrixCoo;
}
/**
* @copydoc IntegratorMetaData::getNumStateNonZeroes
*/
int IntegratorSingleStageMetaData::getNumStateNonZeroes()
{
  return m_jacobianFstates->get_matrix_ptr()->nzmax;
}

/**
* @brief set the value of the global duration parameter
*
* @note every control has a pointer to the global duration, so by changing it here,
*       the value of every last duration inside the controls is changed as well.
* @param duration value to be set
*/
void IntegratorSingleStageMetaData::setGlobalDuration(const double &duration)
{
  assert(false);
  assert(duration >= 0.0);
  m_globalDurationParameter->setParameterValue(duration);
}

/**
* @brief return all parameters with sensitivity information
*
* @param sensParams vector receiving parameter pointers - will be resized
*/
void IntegratorSingleStageMetaData::defineSensParameters(std::vector<Parameter::Ptr> &sensParams)
{
  sensParams.clear();
  //add all initialValueParameters with sensitivity
  for(unsigned i=0; i<m_initialValues.size(); i++){
    if(m_initialValues[i]->getWithSensitivity()){
      sensParams.push_back(m_initialValues[i]);
    }
  }


  //add all control parameters and all duration parameters with sensitivity
  for(unsigned i=0; i<m_controls.size(); i++){
    for(unsigned j=0; j<m_controls[i].getNumberOfGrids(); j++){
      ParameterizationGrid::Ptr grid = m_controls[i].getParameterizationGrid(j);
      std::vector<ControlParameter::Ptr> controlParams = grid->getAllControlParameter();
      for(unsigned k=0; k<controlParams.size(); k++){
        if(controlParams[k]->getWithSensitivity() && controlParams[k]->isOriginal()){
          sensParams.push_back(controlParams[k]);
        }
      }
    }
  }
  
  for(unsigned i=0; i<m_gridDurations.size(); i++){
    if(m_gridDurations[i]->getWithSensitivity()){
      sensParams.push_back(m_gridDurations[i]);
    }
  }
}

/**
* @brief collect all parameters that are decision variables
*
* The decision variables are a subset of sensitivity parameters. So remove from that vector all parameters
* that are not decision variables.
* @param decisionVariables vector receiving parameter pointers
*/
void IntegratorSingleStageMetaData::defineDecisionVariables(std::vector<Parameter::Ptr> &decisionVariables)
{
  defineSensParameters(decisionVariables);
  std::vector<Parameter::Ptr>::iterator iter = decisionVariables.begin();
  while(iter != decisionVariables.end()){
    if(!(*iter)->getIsDecisionVariable()){
      iter = decisionVariables.erase(iter);
    }
    else{
      iter++;
    }
  }
}

/**
* @brief getter for the attribute m_decisionVariables
*
* @param[out] decisionVariables vector receiving the Parameter pointer stored
*                               in m_decisionVariables - will be resized
*/
void IntegratorSingleStageMetaData::getDecisionVariables(std::vector<Parameter::Ptr> &decisionVariables) const
{
  decisionVariables.assign(m_decisionVariables.begin(), m_decisionVariables.end());
}

/**
* @brief getter for the attribute m_parametersWithSensitivity
*
* @param[out] sensParams vector receiving the Parameter pointer stored
*                        in m_parametersWithSensitivity - will be resized
*/
void IntegratorSingleStageMetaData::getSensitivityParameters(std::vector<Parameter::Ptr> &sensParams) const
{
  sensParams.assign(m_parametersWithSensitivity.begin(), m_parametersWithSensitivity.end());
}

/**
* @brief set the parameter indices of the decision variables vector
*
* This function should be called whenever the decision variable vector has changed.
* This can be the case if initial value parameter are assigned after instantiation
* of the IntegratorMetaData object.
*/
void IntegratorSingleStageMetaData::assignParameterIndices()
{
  for(unsigned i=0; i<m_parametersWithSensitivity.size(); i++){
    m_parametersWithSensitivity[i]->setParameterIndex(i);
  }
  for(unsigned i=0; i<m_decisionVariables.size(); i++){
    m_decisionVariables[i]->setDecVarIndex(i);
  }
}

/**
* @brief includes a nonlinear constraint in the m_initOptData
*
* @param esoIndex of the corresponding state point constraint
* @param constIndex constraint index in the constraint Jacobian matrix
* @param timePoint the time point where the constraint is evaluated
* @param lagrangeMultiplier
*/
unsigned IntegratorSingleStageMetaData::addNonlinearConstraint(const unsigned esoIndex,
                                                               const unsigned constIndex,
                                                               const double timePoint,
                                                               const double lagrangeMultiplier)
{
  const unsigned stateIndex = m_genericEso->getStateIndexOfVariable(esoIndex);
  assert(stateIndex < (unsigned)m_genericEso->getNumStates());
  const double defaultValue = 0.0;
  if(m_completeGridUnscaled.empty()){
    createGlobalGrid();
  }
  
  //find timePoint in integration grid
  unsigned timeIndex = m_completeGridUnscaled.size();
  const double threshold = SCALED_GRID_THRESHOLD;
  const double duration = this->getStageDuration();
  for(unsigned i=0; i<m_completeGridUnscaled.size(); i++){
    //check if given timePoint is close enough
    if(fabs(duration*timePoint - m_completeGridUnscaled[i]) < threshold*duration){
       timeIndex = i;
       break;
    }
  }
  assert(timeIndex < m_completeGridUnscaled.size());

  if(m_intOptData[0].count(timeIndex) > 0){
    // if the time point is found we need to check if the constraint was added
    vector<unsigned>::iterator findIndex;
    // look for the constraint index
    findIndex = find(m_intOptData[0][timeIndex].nonLinConstIndices.begin(),
                     m_intOptData[0][timeIndex].nonLinConstIndices.end(),
                     constIndex);
    if(findIndex != m_intOptData[0][timeIndex].nonLinConstIndices.end()){
      // if the constraint is already included we don't need to include it again.
    }
    else{
      // if the index was not found, we have to include the constraint for this time point
      m_intOptData[0][timeIndex].nonLinConstVarIndices.push_back(esoIndex);
      m_intOptData[0][timeIndex].nonLinConstStateIndices.push_back(stateIndex);
      m_intOptData[0][timeIndex].nonLinConstValues.append(defaultValue);
      m_intOptData[0][timeIndex].nonLinConstIndices.push_back(constIndex);
      m_intOptData[0][timeIndex].lagrangeMultipliers.push_back(lagrangeMultiplier);
      m_intOptData[0][timeIndex].numSensParam = 0;
    }
  }
  else{
    // if the time point was not found,
    // we have to include a new time point including the constraint
    SavedOptDataStruct newTimePoint;
    newTimePoint.nonLinConstVarIndices.push_back(esoIndex);
    newTimePoint.nonLinConstStateIndices.push_back(stateIndex);
    newTimePoint.nonLinConstValues.append(defaultValue);
    newTimePoint.nonLinConstIndices.push_back(constIndex);
    newTimePoint.lagrangeMultipliers.push_back(lagrangeMultiplier);
    newTimePoint.numSensParam = 0;
    newTimePoint.timePoint = timePoint;
    m_intOptData[0].insert(pair<unsigned, SavedOptDataStruct>(timeIndex, newTimePoint));
  }
  return timeIndex;
}


/**
* @brief sets the values of the nonlinear constraints required in the optimizer
*/
void IntegratorSingleStageMetaData::setNonlinearConstraints(void)
{
  // we update the values from the eso at the current time
  const unsigned timeIndex = m_currentIndexCompleteGridUnscaled;
  assert(!m_intOptData.empty());
  if(m_intOptData[0].count(timeIndex) > 0){
    assert(!m_intOptData[0][timeIndex].nonLinConstVarIndices.empty());
    m_genericEso->getVariableValues(m_intOptData[0][timeIndex].nonLinConstValues.getSize(),
                                    m_intOptData[0][timeIndex].nonLinConstValues.getData(),
                                    &m_intOptData[0][timeIndex].nonLinConstVarIndices[0]);
  }
}

/**
* @brief returns the current sensitivity information
* @param[out] curSens double vector of the current sensitivity values
*/
void IntegratorSingleStageMetaData::getCurrentSensitivities(std::vector<double> &curSens) const
{
  curSens.assign(m_currentSensitivities.begin(), m_currentSensitivities.end());
}

/**
* @copydoc IntegratorMetaData::getAdjoints
*/
void IntegratorSingleStageMetaData::getAdjoints(std::vector<double> &adjoints) const
{
  adjoints.assign(m_adjoints.begin(), m_adjoints.end());
}

/**
* @copydoc IntegratorMetaData::getLagrangeDers
*/
void IntegratorSingleStageMetaData::getLagrangeDers(std::vector<double> &LagrangeDers) const
{
  LagrangeDers.assign(m_lagrangeDers.begin(), m_lagrangeDers.end());
}

/**
* @brief extract current sensitivities for a subset of equations (constraints)
*
* @param equationIndices vector containing the equation indices of the constraints
* @param sensitivities array receiving the sensitivity values -
*                      must be of size getNumCurrentSensitivityParameters * equationIndices.size()
*/
void IntegratorSingleStageMetaData::getConstraintSensitivities(const std::vector<unsigned> &equationIndices,
                                                                     utils::Array<double>  &sensitivities) const
{
  const unsigned numParameters = getNumCurrentSensitivityParameters();
  const unsigned numEquations = m_genericEso->getNumEquations();

  assert(sensitivities.getSize() == equationIndices.size()*numParameters);

  std::vector<double> currentSensitivities(numParameters*numEquations);

  getCurrentSensitivities(currentSensitivities);

  for(unsigned i=0; i<equationIndices.size(); i++){
    assert(equationIndices[i] < numEquations);
    for(unsigned j=0; j<numParameters; j++){
      const unsigned outputSensIndex = i*numParameters + j;
      const unsigned currentSensIndex = j*numEquations +equationIndices[i];
      assert(outputSensIndex < sensitivities.getSize());
      assert(currentSensIndex < currentSensitivities.size());
      sensitivities[outputSensIndex] = currentSensitivities[currentSensIndex];
    }
  }
}

/**
* @brief store current sensitivities of all point constraints referring to the current time point
*/
void IntegratorSingleStageMetaData::setNonlinearConstraintsGradients(void)
{

  const unsigned numParameters = getNumCurrentSensitivityParameters();
  const unsigned timeIndex = m_currentIndexCompleteGridUnscaled;
  assert(!m_intOptData.empty());
  if(m_intOptData[0].count(timeIndex)>0){
    m_intOptData[0][timeIndex].numSensParam = numParameters;
    // allocate the size of the gradient vector
    const double defaultVal = 0.0;
    // THIS RESIZE SHOULD BE IN THE INITIALIZATION SINCE THE SIZE IS KNOWN IN ADVANCE
    // THIS WILL SLOW DOWN THE INTEGRATION
    // It will not slow down the integration much, as nothing is done, if the gradient vector is
    // of sufficient size. Furthermore the number of parameters changes from timepoint to
    // timepoint, so the size of the vector can not be easily calculated in advance - tjho
    const unsigned numStateIndices = m_intOptData[0][timeIndex].nonLinConstStateIndices.size();
    m_intOptData[0][timeIndex].nonLinConstGradients.resize(numParameters*numStateIndices, defaultVal);
    getConstraintSensitivities(m_intOptData[0][timeIndex].nonLinConstStateIndices,
                                 m_intOptData[0][timeIndex].nonLinConstGradients);
    m_intOptData[0][timeIndex].activeParameterIndices.resize(numParameters);
    m_intOptData[0][timeIndex].activeDecVarIndices.resize(numParameters);
    for(unsigned i=0; i<numParameters; i++){
      m_intOptData[0][timeIndex].activeParameterIndices[i] = m_currentParamsWithSens[i]->getParameterIndex();
      m_intOptData[0][timeIndex].activeDecVarIndices[i] = m_currentParamsWithSens[i]->getDecVarIndex();
    }
  }
}

/**
* @brief add parameter to vector m_parameterWithSensitivity
*
* @param[in] p parameter pointer to be added
*/
void IntegratorSingleStageMetaData::addNewParameterToParameterWithSensitivityVector
                                                                (const Parameter::Ptr &p)
{

  // we assume that m_currentParamsWithSens is always sorted according to the parameter->index
  const bool found = std::binary_search(m_currentParamsWithSens.begin(),
                     m_currentParamsWithSens.end(),
                     p,
                     Parameter::compareParameter);
  // we only include parameters that we didn't find in the binary search.
  if (!found){
    if(p->isOriginal()){
      m_currentParamsWithSens.push_back(p);
    }else{
      m_currentParamsWithSens.push_back(p->getRelatedParameter());
    }
    sortControlParameters();
    p->setIsInitialized(false);
  }
}

/**
* @brief search for the given parameter and remove it if found
*
* binary search is used to find the position of the parameter
* @param[in] p parameter pointer to be removed
*/
void IntegratorSingleStageMetaData::removeParameterFromParameterWithSensitivityVector
                                                                (const Parameter::Ptr &p)
{
  std::vector<Parameter::Ptr>::iterator leftBorder, rightBorder, mean;
  leftBorder = m_currentParamsWithSens.begin();
  rightBorder = m_currentParamsWithSens.end();
  while(leftBorder <= rightBorder){
    //calculate (leftBorder + rightBorder)/2, but + is no allowed operation on two iterators
    mean = leftBorder + (rightBorder - leftBorder)/2;
    if((*mean)->getParameterIndex() < p->getParameterIndex()){
      leftBorder = mean + 1;
    }
    else if((*mean)->getParameterIndex() > p->getParameterIndex()){
      rightBorder = mean - 1;
    }
    else{
      // erased parameters have to be deactivated as well
      (*mean)->setParameterSensActivity(false);
      m_currentParamsWithSens.erase(mean);
      return;
    }// if - else
  }// while
}

/**
* @brief get the global final time parameter
*
* @return global final time parameter
*/
DurationParameter::Ptr IntegratorSingleStageMetaData::getGlobalDurationParameter()const
{
  return m_globalDurationParameter;
}

/**
* @brief getter for the attribute m_initialValues
*
* This function is needed by IntegratorMSMetaData2ndOrder class
* @param[out] initials vector receiving the InitialValueParameter
*/
void IntegratorSingleStageMetaData::getInitialValueParameter
                                  (std::vector<InitialValueParameter::Ptr> &initials) const
{
  initials.assign(m_initialValues.begin(), m_initialValues.end());
}

std::vector<double> IntegratorSingleStageMetaData::getStageGridScaled() const
{
  std::vector<double> scaledGrid = m_completeGridUnscaled;
  for(unsigned i=0; i<scaledGrid.size(); i++){
    scaledGrid[i] /= getStageDuration();
  }
  scaledGrid.front() = 0.0;
  scaledGrid.back() = 1.0;
  return scaledGrid;
}

/**
* @copydoc IntegratorMetaData::getStageDuration
*/
double IntegratorSingleStageMetaData::getStageDuration() const
{
  double stageDuration = 0.0;
  for(unsigned i=0; i<m_gridDurations.size(); i++){
    stageDuration += m_gridDurations[i]->getParameterValue();
  }
  return stageDuration;
}

/**
* @copydoc IntegratorMetaData::getNumSensitivityParameters
*/
unsigned IntegratorSingleStageMetaData::getNumSensitivityParameters() const
{
  return m_parametersWithSensitivity.size();
}

/**
* @brief get the number of all decision variables
*
* @return number of decision variables
*/
unsigned IntegratorSingleStageMetaData::getNumDecisionVariables() const
{
  return m_decisionVariables.size();
}

/**
* @brief sets values of mapped differential states to values from the last stage
*
* @param values values of the mapped states
* @param esoIndices indices of the mapped states
*/
void IntegratorSingleStageMetaData::setMappingValues(const utils::Array<double> &values,
                                                     const std::vector<int> &esoIndices)
{
  assert(esoIndices.size() == values.getSize());
  for(unsigned i=0; i<esoIndices.size(); i++){
    for(unsigned j=0; j<m_initialValues.size(); j++){
      if(esoIndices[i] == m_initialValues[j]->getEsoIndex()){
        //do not set value if user specified an initial value parameter.
        if(!m_initialValues[j]->getUserFlag()){
          m_initialValues[j]->setParameterValue(values[i]);
        }
        break;
      }
    }
  }
  
  assert(m_genericEso->getNumDifferentialVariables() == int(m_initialValues.size()));
  utils::Array<EsoDouble> diffVarValues(m_initialValues.size());
  for(unsigned i=0; i<m_initialValues.size(); i++){
    diffVarValues[i] = m_initialValues[i]->getParameterValue();
  }
  m_genericEso->setDifferentialVariableValues(m_initialValues.size(), diffVarValues.getData());
  m_evaluateFStates = true;
}

/**
* @brief collects the duration parameters of all parameterization grids
*        (excluding the global final time parameter)
*
* for every control a vector is returned containing the duration parameters of that
* control.
*/
void IntegratorSingleStageMetaData::getDurationParameters
      (std::vector<DurationParameter::Ptr> &durationParameters)const
{
  assert(!m_gridDurations.empty());
  durationParameters.assign(m_gridDurations.begin(), m_gridDurations.end());
}

/**
* @copydoc IntegratorMetaData::getStageCurrentTime
*/
double IntegratorSingleStageMetaData::getStageCurrentTime()const
{
  // determine the total time that is required for the Spline evaluations
  const double currentTime = getStepCurrentTime(); // scaled
  const double relativeDuration = m_integrationBlockTimes.relativeDuration;
  const double absoluteStartTime = m_integrationBlockTimes.absoluteStartTime;
  return absoluteStartTime + currentTime * relativeDuration;
}

/**
* @brief calculate the current scaled stage time
*
* for now use the state of the object to get the current scaled time in global context
* @return current stage time scaled by global final time.
* @warning this function currently do not work if called during an integration, because
*          it is assumed, that the current time is equal to the start time or end time
*          of the current integration step.
*/
double IntegratorSingleStageMetaData::getStageCurrentTimeScaled() const
{
  double currentTime = m_completeGridUnscaled[m_currentIndexCompleteGridUnscaled];
  currentTime /= getStageDuration();
  return currentTime;
}

/**
* @brief set the current control values to the Eso
*/
void IntegratorSingleStageMetaData::setControlsToEso()
{
  const unsigned numControls = m_controls.size();
  utils::Array<int> controlIndices(numControls);
  utils::Array<double> controlValues(numControls);

  const double localTime = getStepCurrentTime();
  for(unsigned i=0; i< numControls; i++){
    controlIndices[i] = m_controls[i].getEsoIndex();
    controlValues[i] = m_controls[i].calculateCurrentControlValue(localTime);
//    std::cout << "eso control = " << controlValues[i]  << std::endl;
  }
  m_genericEso->setVariableValues(controlValues.getSize(), controlValues.getData(), controlIndices.getData());
  m_evaluateFStates = true;
}

/**
* @brief clears the m_lagrangeDers and m_adjoints attributes
*
*/
void IntegratorSingleStageMetaData::reset()
{
  m_lagrangeDers.clear();
  m_adjoints.clear();
}

/**
* @brief finds the root for the switching function
*
*/
void IntegratorSingleStageMetaData::foundRoot()
{
  /// @todo: compute updates for sensitivities

  // for now this just resets the eso to the new conditions.
  // This is correct as long as sigma_p=0 and the system has no algebraic vars.
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "A switch in the conditions was reported to ISSMD.\n");
  const EsoIndex ncond = m_genericEso->getNumConditions();
  utils::Array<double> res_cond(ncond);
  m_genericEso->getConditionResiduals(ncond, res_cond.getData());
  EsoIndex n_switch = -1;
  // @todo: some solvers(idas) return index of switching function- should use that information 
  double tol = 1e-6; // @todo: should't hardcode this! - this is connected to the accuracy in the integrator!
  for (int k=0; k<ncond; ++k) {
    if (fabs(res_cond[k])<tol) {
      if (n_switch==-1) {
        n_switch = k;
      } else {
        MetaDataException exc("Found more than one active switching function. Don't know what to do...");
        throw(exc);
      }
    }
  }
  if (n_switch==-1) {
    MetaDataException exc("The integrator reported a root of the switching function but I can't find one!");
    throw(exc);
  }
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Found switch in function " );
  utils::Array<int> curr_conditions(ncond);
  m_genericEso->getCurrentConditions(ncond, curr_conditions.getData());
  curr_conditions[n_switch] = !curr_conditions[n_switch];
  m_genericEso->setConditions(ncond, curr_conditions.getData());
  utils::Array<int> locks(ncond, 1);
  m_genericEso->setLocks(ncond, locks.getData());
  // update everything (jacobian structure etc)
  CsTripletMatrix::Ptr Fu = createJacobianInTriplet();
  createJacobianFstates(Fu);
  createJacobianFup(Fu);
  createMassMatrix();
}

/**
* @copydoc IntegratorMetaData::activatePlotGrid
*/
void IntegratorSingleStageMetaData::activatePlotGrid()
{
  m_plotGridActivated = true;
}


/**
* @brief store an equidistant plot grid with given resolution
*
* this grid is inserted to the parameter grid, if plot grid is activated
* @param plotGridResolution number of intervals the plot grid is going to have. Must be positive.
*/
void IntegratorSingleStageMetaData::setPlotGrid(const unsigned int plotGridResolution,
                                                const std::vector<double> &explicitPlotGrid)
{
  assert(plotGridResolution > 0);
  unsigned explicitPlotGridIndex = 0;
  m_plotGrid.clear();
  m_plotGrid.reserve(plotGridResolution+1 + explicitPlotGrid.size());
  for(unsigned i=1; i<plotGridResolution; i++){
    const double plotGridResolutionValue = 1.0/plotGridResolution * i;
    if(explicitPlotGridIndex < explicitPlotGrid.size()){
      if(explicitPlotGrid[explicitPlotGridIndex] < plotGridResolutionValue){
        m_plotGrid.push_back(explicitPlotGrid[explicitPlotGridIndex]);
        explicitPlotGridIndex++;
      }
    }
    m_plotGrid.push_back(plotGridResolutionValue);
  }
  for(unsigned i=explicitPlotGridIndex; i<explicitPlotGrid.size(); i++){
    m_plotGrid.push_back(explicitPlotGrid[i]);
  }
  m_plotGrid.push_back(1.0);
}

/**
* @brief sets values for attributes required for computation of sparsity structure
*
* the method sets the values for the attributes activeParameterIndices and 
* numSensParam in the intOptData structure. This is needed for the computation 
* of the sparsity structure of the Jacobian of the nonlinear constraints, as 
* required by some optimizers (e.g. SNOPT, IPOPT). These (structural) data is 
* in principal known before any integration. So far, these data had only been 
* collected during the integrations of the model/sensitivity system. However, 
* these data is required when calling the optimizer (i.e. before having performed 
* an integration).
*
* @remark method name too unspecific as we set only the values of 2 of the (in total) 9 attributs of intOptData
*/
void IntegratorSingleStageMetaData::setIntOptData()
{
  initializeForFirstIntegration();
  
  unsigned timeIndex = 0, numPoints = 0;
  // at startpoint only initial value and time invariant parameters have an influence on constraints
  if(m_intOptData[0].count(timeIndex) > 0){
    const unsigned numParameters = getNumCurrentSensitivityParameters();
    m_intOptData[0][timeIndex].numSensParam = numParameters;
    m_intOptData[0][timeIndex].activeParameterIndices.resize(numParameters);
    m_intOptData[0][timeIndex].activeDecVarIndices.resize(numParameters);
    for(unsigned i=0; i<numParameters; i++){
      m_intOptData[0][timeIndex].activeParameterIndices[i] = m_currentParamsWithSens[i]->getParameterIndex();
      m_intOptData[0][timeIndex].activeDecVarIndices[i] = m_currentParamsWithSens[i]->getDecVarIndex();
    }
    numPoints ++;
  }

  const unsigned numIntervals = getNumIntegrationIntervals();
  for (unsigned i=0; i<numIntervals; i++) {
    while(getStepEndTime() < 1.0) {
      initializeForNewIntegrationStep();
      timeIndex = m_currentIndexCompleteGridUnscaled;
      // set the data
      if (m_intOptData[0].count(timeIndex)> 0) {
        const unsigned numParameters = getNumCurrentSensitivityParameters();
        m_intOptData[0][timeIndex].numSensParam = numParameters;
        m_intOptData[0][timeIndex].activeParameterIndices.resize(numParameters);
        m_intOptData[0][timeIndex].activeDecVarIndices.resize(numParameters);
        for(unsigned i=0; i<numParameters; i++){
          m_intOptData[0][timeIndex].activeParameterIndices[i] = m_currentParamsWithSens[i]->getParameterIndex();
          m_intOptData[0][timeIndex].activeDecVarIndices[i] = m_currentParamsWithSens[i]->getDecVarIndex();
        }
        numPoints++;
      }
    }
    if (i<numIntervals-1) { // otherwise assert in initializeForNextIntegration() will fail
      initializeForNextIntegration();
    }
  }
  assert(numPoints == m_intOptData[0].size());
}



/**
* @copydoc IntegratorSingleStageMetaData::IntegratorSingleStageMetaData(const GenericEso::Ptr&,
*                                                             const std::vector<Control>&,
*                                                             const std::vector<InitialValueParameter::Ptr>&,
*                                                             const DurationParameter::Ptr&,
*                                                             const std::vector<double>&)
*
* @param genericEso2ndOrder a GenericEso object able to calculate second-order model derivatives
*/
IntegratorSSMetaData2ndOrder::IntegratorSSMetaData2ndOrder(const GenericEso2ndOrder::Ptr &genericEso2ndOrder,
                                                           const std::vector<Control> &controls,
                                                           const std::vector<InitialValueParameter::Ptr> &initials,
                                                           const DurationParameter::Ptr &globalDuration,
                                                           const std::vector<double> &additionalGrid):
IntegratorSingleStageMetaData(genericEso2ndOrder, controls, initials, globalDuration, additionalGrid),
m_genericEso2ndOrder(genericEso2ndOrder)
{
  m_secondOrderAdjointsObserver = Observer::Ptr(new IntegrationObserver());
}

/**
* @brief destructor
*/
IntegratorSSMetaData2ndOrder::~IntegratorSSMetaData2ndOrder()
{
}

/**
* @copydoc IntegratorMetaData::getGenericEso
*/
GenericEso2ndOrder::Ptr IntegratorSSMetaData2ndOrder::getGenericEso2ndOrder() const
{
  return m_genericEso2ndOrder;
}

/**
* @copydoc IntegratorMetaData::initializeAtDuration
*/
void IntegratorSSMetaData2ndOrder::initializeAtStageDuration()
{
  IntegratorSingleStageMetaData::initializeAtStageDuration();
  const unsigned numSensPars = getNumSensitivityParameters();
  const unsigned numEqns = m_genericEso2ndOrder->getNumEquations();

  m_adjoints.resize(numEqns * (1 + numSensPars), 0.0);
  m_lagrangeDers.resize(numSensPars * (1 + numSensPars), 0.0);
}

/**
* @brief set m_lagrangeDers to the correct size
*
* In multis stage mode the lagrange derivatives are managed by the single stages.
* This means the mixed terms of the 2nd order derivatives are saved on the single stages as well.
* Since the single stage object does not reserve memory in function initializeAtDuration for the
* mixed 2nd order derivatives, the vector has to be resized manually in multi stage mode.
* @param[in] numGlobalSensPars number of parameters with sensitivity of the multi stage object
*/
void IntegratorSSMetaData2ndOrder::resizeLagrangeDerivatives
                                   (const unsigned numGlobalSensPars)
{
  const unsigned numLocalSensPars = getNumSensitivityParameters();
  m_lagrangeDers.resize(numLocalSensPars *(1 + numGlobalSensPars), 0.0);
}

/**
* @brief activate some parameters
*
* If a stage becomes active, some parameters have to be reactivated
* that have been deactivated as long as the stage itself has not been active.
* The parameters to be activated are the initial value parameters, the last
* control parameter from each control grid and the global final time parameter.
* These parameters are those that are active at the final time point of the stage.
*/
void IntegratorSingleStageMetaData::reactivateParameters()
{
  m_currentParamsWithSens.assign(m_parametersWithSensitivity.begin(),
                                 m_parametersWithSensitivity.end());
  for(unsigned i=0; i<m_initialValues.size(); i++){
    m_initialValues[i]->setParameterSensActivity(true);
  }
  for(unsigned i=0; i<m_controls.size(); i++){
    const unsigned lastGridIndex = m_controls[i].getNumberOfGrids()-1;
    ParameterizationGrid::Ptr lastGrid = m_controls[i].getParameterizationGrid(lastGridIndex);
    std::vector<ControlParameter::Ptr> controlParams = lastGrid->getAllControlParameter();
    controlParams.back()->setParameterSensActivity(true);
    if(lastGrid->getControlType() == PieceWiseLinear){
      controlParams[controlParams.size() -2]->setParameterSensActivity(true);
    }
  }
  if(!m_gridDurations.empty()){
    m_gridDurations.back()->setParameterSensActivity(true);
  }
}

/**
* @brief clear the attribute vector m_currentParamsWithSens
*
* During backwards integration the parameters all are to be removed if
* a backwards integration of a stage is completed. So far an empty current parameters
* vector is only needed for multi stage 2nd order
*/
void IntegratorSingleStageMetaData::clearActiveParameters()
{
  m_currentParamsWithSens.clear();
}


/**
* @brief get pointers of all parameters with sensitivity information
*
* @param[out] vector receiving the parameter pointer - will be resized
*/
void IntegratorSSMetaData2ndOrder::getSensitivityParameters(std::vector<Parameter::Ptr> &parameters) const
{
  parameters.assign(m_parametersWithSensitivity.begin(), m_parametersWithSensitivity.end());
}

/**
* @copydoc IntegratorMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder
*/
void IntegratorSSMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder(
                                                   utils::Array<double> &adjoints1stOrder,
                                                   utils::Array<double> &adjoints2ndOrder,
                                                   Eso2ndOrderEvaluation &input,
                                                   utils::Array<double> &rhsDers2ndOrder)
{
  calculateRhsLagrangeDers2ndOrder(adjoints1stOrder,adjoints2ndOrder,input,rhsDers2ndOrder,
                                      m_parametersWithSensitivity, this);
}

/**
* @copydoc IntegratorMetaData2ndOrder::calculateAdjointsReverse2ndOrder
*/
void IntegratorSSMetaData2ndOrder::calculateAdjointsReverse2ndOrder
                                                         (utils::Array<double> &states,
                                                          utils::Array<double> &sens,
                                                          utils::Array<double> &adjoints1stAnd2nd,
                                                          utils::Array<double> &rhsAdjoints2nd,
                                                          utils::Array<double> &rhsDers2nd)
{
  const unsigned numStates = states.getSize();
  assert(numStates == (unsigned)m_genericEso->getNumStates());
  assert(adjoints1stAnd2nd.getSize() >= numStates);
  const unsigned num2nd = adjoints1stAnd2nd.getSize()-numStates;
  utils::WrappingArray<double> adjoints1st(numStates, adjoints1stAnd2nd.getData());
  utils::WrappingArray<double> adjoints2nd(num2nd, adjoints1stAnd2nd.getData()+numStates);

  //preset eso
  m_genericEso2ndOrder->setStateValues(numStates, states.getData());
  this->setControlsToEso();
  // set all derivatives to 0
  utils::Array<double>dummyDerivatives(m_genericEso->getNumDifferentialVariables(), 0.0);
  m_genericEso->setDerivativeValues(dummyDerivatives.getSize(), dummyDerivatives.getData());

  calculateAdjointsReverse2ndOrder(sens,
                                   adjoints1st,
                                   adjoints2nd,
                                   rhsAdjoints2nd,
                                   rhsDers2nd,
                                   this);
}

/**
* @copybrief IntegratorMetaData2ndOrder::calculateAdjointsReverse2ndOrder
*
* In multi stage this function does some calculation on the multi stage object with the
* single stage data not available on the multi stage level.
* @param[in] sens saved sens at the current grid point - must be of size
*                 getNumCurrentSensitivityParameters() * numStates (local)
* @param[in] adjoints1st adjoints of first order -
                         must be of size numMaxStates
* @param[in] adjoints2nd adjoints of second order - must be of size
*                      getNumDecsisionVariables() * numMaxStates (local)
* @param[out] rhsAdjoints2ndOrder rhs adjoints for second order to be calculated - must be of
*                                 size numEqns * getNumDecsisionVariables() (local)
* @param[out] rhsDers2nd rhs for the second order derivatives of the Lagrange function -
*                      must be of size getNumDecsisionVariables() * getNumDecsisionVariables()(local)
* @param[in] imd2nd IntegratorMetaData2ndOrder object from which the GenericEso2ndOrder is used to call
*                   function evaluate2ndOrderDerivatives. The function calculateRhsLagrangeDers2ndOrder
*                   is called on the IntegratorMetaData2ndOrder object as well.
*/
void IntegratorSSMetaData2ndOrder::calculateAdjointsReverse2ndOrder
                                     (utils::Array<double> &sens,
                                      utils::Array<double> &adjoints1st,
                                      utils::Array<double> &adjoints2nd,
                                      utils::Array<double> &rhsAdjoints2nd,
                                      utils::Array<double> &rhsDers2nd,
                                      IntegratorMetaData2ndOrder *imd2nd)
{
  const unsigned numStates = imd2nd->getGenericEso()->getNumStates();
  const unsigned numEqns = unsigned(m_genericEso2ndOrder->getNumEquations());
  const double tf = imd2nd->getIntegrationGridDuration();

  // calculate output arguments
  const unsigned numSensPars = getNumSensitivityParameters();
  const int numEsoParams = m_genericEso2ndOrder->getNumParameters();
  Eso2ndOrderEvaluationArgs esoEvalArgs(numStates, numEsoParams, numEqns);

  for(unsigned i=0; i<numSensPars; i++){
    initializeEso2ndOrderEvaluationArgs(esoEvalArgs, rhsAdjoints2nd, i);

    initializeSensWrtP_i(m_currentParamsWithSens,
                         m_parametersWithSensitivity[i],
                         sens,
                         esoEvalArgs);

    setAdjoints(esoEvalArgs, adjoints1st, adjoints2nd);

    const int paramEsoIndex = getActiveParameterIndex(m_parametersWithSensitivity[i],
                                                m_genericEso);

    double dudp = m_parametersWithSensitivity[i]->getDuDp(this);
    if(paramEsoIndex >= 0){
      esoEvalArgs.eso2ndOrder.dEsoParamsDp_i[paramEsoIndex] = dudp;
    }

    callEvaluate2ndOrderDerivatives(esoEvalArgs, imd2nd->getGenericEso2ndOrder());

    adjustOutput(esoEvalArgs, tf, paramEsoIndex);

    setAdjoints(esoEvalArgs, adjoints1st, adjoints2nd);

    // calculate rhsDers2nd = t2A1EsoParameters * dudpForRhsDers
    const unsigned numGlobalSensPars = imd2nd->getNumSensitivityParameters();
    const unsigned offset = i*numGlobalSensPars;
    utils::WrappingArray<double> rhsDers2ndOrder(numGlobalSensPars, rhsDers2nd.getData()+offset);
    imd2nd->calculateRhsLagrangeDers2ndOrder(esoEvalArgs.adjoints1stOrder, esoEvalArgs.adjoints2ndOrder,
                                             esoEvalArgs.eso2ndOrder, rhsDers2ndOrder);
    // reset value for next iteration
    if(paramEsoIndex >= 0){
      esoEvalArgs.eso2ndOrder.dEsoParamsDp_i[paramEsoIndex] = 0.0;
    }
  }
}

/**
* @brief set some fields of a Eso2ndOrderEvaluationArgs struct to their initial state
* @param[in,out] in struct to be initialized
* @param[in] rhsAdjoints2nd array from which a pointer is stored into the struct
* @param[in] sensParIndex index of current sensitivity parameter
*/
void initializeEso2ndOrderEvaluationArgs(Eso2ndOrderEvaluationArgs &in,
                                          utils::Array<double> &rhsAdjoints2nd,
                                          unsigned sensParIndex)
{
  in.m_sensParIndex = sensParIndex;
  in.eso2ndOrder.sensWrtP_i.assign(in.m_numStates, 0.0);
  assert(rhsAdjoints2nd.getSize() >= sensParIndex*in.m_numEqns + in.m_numStates);
  in.rhsAdjs2ndOrder = rhsAdjoints2nd.getData() + sensParIndex*in.m_numEqns;
  for(unsigned j=0; j<in.m_numEsoParams; j++){
    in.a1P[j] = 0.0;
    in.eso2ndOrder.t2A1EsoParameters[j] = 0.0;
  }
  for(unsigned j=0; j<in.m_numStates; j++){
    in.a1States[j] = 0.0;
    in.a1DerStates[j] = 0.0;
    in.t2A1DerStates[j] = 0.0;
  }
}

/**
* @brief define 1st and 2nd order adjoints on Eso2ndOrderEvaluationArgs struct
*
* @param[in,out] struct containing adjoints1stOrder and adjoints2ndOrder
* @param[in] adjoints1stOrder array containing 1st order adjoints to be set
* @param[in] adjoints2ndOrder array containing 2nd order adjoints to be set
*/
void setAdjoints(Eso2ndOrderEvaluationArgs &in,
           const utils::Array<double> &adjoints1stOrder,
           const utils::Array<double> &adjoints2ndOrder)
{
  utils::copy(in.m_numStates, adjoints1stOrder.getData(), in.adjoints1stOrder.getData());
  for(unsigned j=0; j<in.m_numStates; j++){
    // values must be copied because evaluate2ndOrderDerivatives overrides them with 0.0
    in.adjoints1stOrder[j] = adjoints1stOrder[j];
    assert(adjoints2ndOrder.getSize() > in.m_sensParIndex*in.m_numEqns + j);
    in.adjoints2ndOrder[j] = adjoints2ndOrder[in.m_sensParIndex*in.m_numEqns + j];
  }
  
}

/**
* @brief wraps the calling of eso function evaluate2ndOrderDerivatives
*
* For code reduction the arguments of the function evaluate2ndOrderDerivatives
* are stored into a struct and the function call itself is wrapped by this function
* @param[in,out] args struct containing all arguments of function call
* @param[in] eso GenericEso object on which evaluate2ndOrderDerivatives is called
*/
void callEvaluate2ndOrderDerivatives(Eso2ndOrderEvaluationArgs &args,
                                     GenericEso2ndOrder::Ptr eso)
{
   double* sensWrtP_iPtr = NULL;
    if(args.m_numStates > 0){
      assert(!args.eso2ndOrder.sensWrtP_i.empty());
      sensWrtP_iPtr = &args.eso2ndOrder.sensWrtP_i[0];
    }
    double *dEsoParamsDp_iPtr = NULL;
    double *t2A1EsoParametersPtr = NULL;
    if(args.m_numEsoParams > 0){
      assert(!args.eso2ndOrder.dEsoParamsDp_i.empty());
      assert(!args.eso2ndOrder.t2A1EsoParameters.empty());
      dEsoParamsDp_iPtr = &args.eso2ndOrder.dEsoParamsDp_i[0];
      t2A1EsoParametersPtr = &args.eso2ndOrder.t2A1EsoParameters[0];
    }
    eso->evaluate2ndOrderDerivatives(args.m_numStates, sensWrtP_iPtr,
                                     args.m_numStates, args.t2DerStates.getData(),
                                     args.m_numEsoParams, dEsoParamsDp_iPtr,
                                     args.m_numStates, args.a1States.getData(),
                                     args.m_numStates, args.a1DerStates.getData(),
                                     args.m_numEsoParams, args.a1P.getData(),
                                     args.m_numStates, args.rhsAdjs2ndOrder,
                                     args.m_numStates, args.t2A1DerStates.getData(),
                                     args.m_numEsoParams, t2A1EsoParametersPtr,
                                     args.m_numEqns, args.adjoints1stOrder.getData(),
                                     args.m_numEqns, args.adjoints2ndOrder.getData());
}

/**
* @brief initialize sensWrtP_i
*
* @param[in] currentActiveParameters vector of parameters excluding not activated parameters
* @param[in] parameter current parameter (can also be an inactive parameter)
* @param[in] sens vector of sensitivities - is expected be at least of size numStates*numParameters
* @param[in,out] esoEvalArgs struct containing sensWrtP_i
*/
void initializeSensWrtP_i(std::vector<Parameter::Ptr> currentActiveParameters,
                          Parameter::Ptr parameter,
                          utils::Array<double> &sens,
                          Eso2ndOrderEvaluationArgs &esoEvalArgs)
{
  //find the parameter position in the active parameter vector
  std::vector<Parameter::Ptr>::iterator position;
  position = std::find(currentActiveParameters.begin(),
                       currentActiveParameters.end(),
                       parameter);
  //if found copy the corresponding sensitivity inputs to sensWrtP_i
  if(position != currentActiveParameters.end()){
    const unsigned index = position - currentActiveParameters.begin();
    for(unsigned j=0; j<esoEvalArgs.m_numStates; j++){
      const unsigned sensIndex = (index * esoEvalArgs.m_numStates) + j;
      assert(sens.getSize() > sensIndex);
      esoEvalArgs.eso2ndOrder.sensWrtP_i[j] = sens[sensIndex];
    }
  }
}

/**
* @brief identify parameter Eso index
*
* @param[in] parameter pointer to the current parameter for calling getDuDp
* @param[in] genericEso for calling getParameterIndex
* @apram[in] imd callback object for parameter function getDuDp
* @return parameter Eso Index
*         paramEsoIndex is the position index of the parameter.
*         So the parameter with the smallest eso index will have paramEsoIndex 0
*         and so on (up to index numParams-1)
*         if the parameter is no active control parameter the index is negative:
*         -1 for iinitial value parameter
*         -2 for final time parameter
*         -3 for inactive parameters
*/
int getActiveParameterIndex(Parameter::Ptr parameter,
                            GenericEso::Ptr &genericEso)
{
  int paramEsoIndex = parameter->getDuDpIndex();
  if(paramEsoIndex >= 0){
    const unsigned numParameters = genericEso->getNumParameters();
    utils::Array<EsoIndex> parameterIndices(numParameters);
    genericEso->getParameterIndex(numParameters, parameterIndices.getData());
    // overwrite paramEsoIndex with index position in parameterIndices
    paramEsoIndex = std::find(parameterIndices.getData(),
                              parameterIndices.getData() + numParameters,
                              paramEsoIndex) - parameterIndices.getData();
    assert(paramEsoIndex < int(numParameters));
    assert(paramEsoIndex >= 0);
  }
  return paramEsoIndex;
}

/**
* @brief scale rhsAdj2ndOrder and t2A1Esoparameters with final time,
*        make output adjustments for final time parameters
*
* @param[in, out] esoEvalArgs struct containing rhsAdjs2ndOrder, t2A1EsoParameters
* @param[in] duration scaling factor
* @param[in] paramEsoIndex -2 if current parameter is a DurationParameter
*/
void adjustOutput(Eso2ndOrderEvaluationArgs &esoEvalArgs,
                  double duration,
                  int paramEsoIndex)
{
  // scale with final time
  for (unsigned j=0; j<esoEvalArgs.m_numEqns; j++){
    esoEvalArgs.rhsAdjs2ndOrder[j] *= duration;
  }
  for (unsigned j=0; j<esoEvalArgs.m_numEsoParams; j++){
    esoEvalArgs.eso2ndOrder.t2A1EsoParameters[j] *= duration;
  }

  if(paramEsoIndex == -2){
    //in case where the current sensitivity parametere is a final time parameter
    for(unsigned j=0; j<esoEvalArgs.m_numEqns; j++){
      esoEvalArgs.rhsAdjs2ndOrder[j] += esoEvalArgs.a1States[j];
    }
    for(unsigned j=0; j<esoEvalArgs.m_numEsoParams; j++){
      esoEvalArgs.eso2ndOrder.t2A1EsoParameters[j] += esoEvalArgs.a1P[j];
    }
  }
}

/**
* @brief update lagrange derivatives with adjoints of the initial value parameter
*
* Lagrange derivatives should remain 0.0 at all time points except for the initial time point.
* So these Lagrange derivatives are updated in this function
* @param[in] initialValueParameter vector of InitialValueParameters
* @param[in] sensParameters vector of parameters with sensitivity information
* @param[in] adjoints vector of adjoints containing the values for the update
*                     - must be of size numEquations
* @param[out] lagrangeDerivatives vector receiving the Lagrange derivatives for the initial
*                                 value parameters
*                                 - must be of the same size as currentSensParameters
*/
void updateInitialLagrangeDerivatives(std::vector<InitialValueParameter::Ptr> &initialValueParameter,
                                      std::vector<Parameter::Ptr> &sensParameters,
                                      utils::Array<double> &adjoints,
                                      utils::Array<double> &lagrangeDerivatives)
{
  assert(lagrangeDerivatives.getSize() == sensParameters.size());
  for(unsigned i=0; i<initialValueParameter.size(); i++){
    if(initialValueParameter[i]->getWithSensitivity() == true){
      //get position of the initial value parameter in the parameter vector
      unsigned position = sensParameters.size();
      int parameterIndex = initialValueParameter[i]->getParameterIndex();
      for(unsigned j=0; j<sensParameters.size(); j++){
        if(parameterIndex == sensParameters[j]->getParameterIndex()){
          position = j;
          break;
        }
      }
      assert(position < lagrangeDerivatives.getSize());

      const unsigned stateIndex = initialValueParameter[i]->getStateIndex();
      lagrangeDerivatives[position] = adjoints[stateIndex];
    }
  }
}


/**
* @copydoc IntegratorSSMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder(
*                                     utils::Array<double> &adjoints1stOrder,
*                                     utils::Array<double> &adjoints2ndOrder,
*                                     Eso2ndOrderEvaluation &input,
*                                     utils::Array<double> &rhsDers2ndOrder)
*
* @param[in] sensParameters vector containing pointers to all parameters with sensitivity
* @param[in] imd2nd current IntegratorMetaData2ndOrder object used for callback and GenericEso
*/
void IntegratorSSMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder(
                                      utils::Array<double> &adjoints1stOrder,
                                      utils::Array<double> &adjoints2ndOrder,
                                      Eso2ndOrderEvaluation &input,
                                      utils::Array<double> &rhsDers2ndOrder,
                                      std::vector<Parameter::Ptr> &sensParameters,
                                      IntegratorMetaData2ndOrder *imd2nd)
{
  GenericEso2ndOrder::Ptr genEso = imd2nd->getGenericEso2ndOrder();
  const unsigned numStates = input.sensWrtP_i.size();
  const unsigned numEqns = (unsigned)genEso->getNumEquations();
  const unsigned numSensPars = sensParameters.size();
  const int numEsoParams = genEso->getNumParameters();

  utils::Array<double> daeResiduals(numEqns, 0.0);
  utils::Array<double> t1DaeResiduals(numEqns, 0.0);
  utils::Array<double> zerosNumStates(numStates, 0.0);

  for(unsigned j=0; j<numSensPars; j++){
    GenericEso::Ptr genEsoCommon = genEso;
    const int dudpIndex = getActiveParameterIndex(sensParameters[j],
                                            genEsoCommon);
    const double dudpForRhsDers = sensParameters[j]->getDuDp(imd2nd);

    if(dudpIndex >= 0){
      rhsDers2ndOrder[j] = input.t2A1EsoParameters[dudpIndex] * dudpForRhsDers;
    }
    else if(dudpIndex == -1){ // j is the index of an ivParameter
        // rhsDers2ndOrder has value of 0 because df/d_ivParameter = 0
        rhsDers2ndOrder[j] = 0.0;
    }
    else if(dudpIndex == -2){ // j is the index of a ftParameter
      // rhsDers2ndOrder reduces to
      // rhsDers2ndOrder = (dadjoints/dp_i)^T * f_unscaled +
      //                   adjoints^T * (d/dx(f_unscaled) * dx/dp_i + d/du(f_unscaled) * du/dp_i)
      for(unsigned k=0; k<numEqns; k++){// initialization with 0.0
        daeResiduals[k] = 0.0;
        t1DaeResiduals[k] = 0.0;
      }
      genEso->getAllResiduals(numEqns, daeResiduals.getData());
      double* sensWrtP_iPtr = NULL;
      if(numStates > 0){
        assert(input.sensWrtP_i.size() == numEqns);
        sensWrtP_iPtr = &input.sensWrtP_i[0];
      }
      double* dEsoParamsDp_iPtr = NULL;
      if(numEsoParams > 0){
        assert(input.dEsoParamsDp_i.size() == (unsigned)numEsoParams);
        dEsoParamsDp_iPtr = &input.dEsoParamsDp_i[0];
      }
      genEso->evaluate1stOrdForwDerivatives(numStates,
                                            sensWrtP_iPtr,
                                            numStates,
                                            zerosNumStates.getData(),
                                            numEsoParams,
                                            dEsoParamsDp_iPtr,
                                            numEqns,
                                            t1DaeResiduals.getData());
      rhsDers2ndOrder[j] = 0.0;
      for(unsigned k=0; k<numEqns; k++){
        //multiply with LagrangeDerivatives?
        rhsDers2ndOrder[j] += adjoints1stOrder[k] * t1DaeResiduals[k]
                              + adjoints2ndOrder[k] * daeResiduals[k];
      }
    }
  }
}

void IntegratorSSMetaData2ndOrder::store2ndOrderAdjoints()
{
  if(m_secondOrderAdjointsObserver){
    unsigned numStates = m_genericEso->getNumStates();
    utils::WrappingArray<const double> secondOrderAdjoints(m_adjoints.size() - numStates, &m_adjoints[numStates]);
    //store adjoints at index 0 (as only one set is stored the index itself is arbitrary)
    m_secondOrderAdjointsObserver->setAdjoints(0, secondOrderAdjoints);
  }
}

void IntegratorSSMetaData2ndOrder::setInitialLagrangeDerivatives()
{
  IntegratorSingleStageMetaData::setInitialLagrangeDerivatives();
  store2ndOrderAdjoints();
}

DyosOutput::IntegratorStageOutput  IntegratorSSMetaData2ndOrder::getStageOutput()
{
  DyosOutput::IntegratorStageOutput out = IntegratorSingleStageMetaData::getStageOutput();
  utils::Array<double> secondOrderAdjoints = m_secondOrderAdjointsObserver->getAdjoints(0);
  out.adjoints2ndOrder.gridPoints.push_back(0.0);
  out.adjoints2ndOrder.values.assign(secondOrderAdjoints.getData(),
                                     secondOrderAdjoints.getData() + secondOrderAdjoints.getSize());
  return out;
}
