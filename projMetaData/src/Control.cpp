/**
* @file Control.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaSingleStageEso - Part of DyOS                                    \n
* =====================================================================\n
* This file contains the definitions of the memberfunctions of class   \n
* Control according to the UML Diagramm METAESO_2                      \n
* =====================================================================\n
* @author Fady Assassa
* @date 17.10.2011
*/

#include "Control.hpp"
#include <algorithm>
#include <functional>
#include <algorithm>
#include <assert.h>
#include <math.h>
#include <set>


#define TOLERANCE 1e-5
#define EPSILON 1e-10

void transformScaledGrid(const double currentStageLength,
                         const double previousDurations,
                         std::vector<double> &currentGrid);
/**
* @brief standard constructor (EMPTY)
*/
Control::Control() :
m_esoIndex(0), m_lowerBound(0.0), m_upperBound(0.0),  m_activeGridIndex(0),
m_nextTimePoint(0.0), m_nextReverseTimePoint(0.0), m_isTimeInvariant(false)
{
}


/**
* @brief destructor
*/
Control::~Control()
{
}

/**
* @brief Copy constructor. Creates a deep copy of a control
*
* @param c reference of a control which needs to be copied
* @throw bad_alloc
*
* This copy constructor is required since we have pointer members in the class. Their memory is freed within
* this class such that we don't have to deal with this issue outside of the function. The last Grid is treated
* different since the total final time is a global for all controls
*/
Control::Control(const Control &c)
{
  m_esoIndex = c.getEsoIndex();
  m_lowerBound = c.getLowerBound();
  m_upperBound = c.getUpperBound();
  m_isTimeInvariant = c.getIsInvariant();
  const unsigned numberOfGrids = c.m_parameterizationGrid.size();
  m_parameterizationGrid.resize(numberOfGrids);
  m_controlGrids.resize(numberOfGrids);
  if (numberOfGrids > 0){
    for(unsigned i = 0; i<numberOfGrids; i++){
      // a deep copy of the control is needed so new grids have to be created
      const ParameterizationGrid::Ptr tempGrid(new ParameterizationGrid(c.m_parameterizationGrid[i].get()));
      m_parameterizationGrid[i] = tempGrid;
      m_controlGrids[i] = c.m_controlGrids[i];
    }
    //the duration of the last grid is the global duration, so copy the pointer of the last grid copy duration
    const ParameterizationGrid::Ptr lastGrid = m_parameterizationGrid.back();
    const ParameterizationGrid::Ptr lastGridOfCopy = c.m_parameterizationGrid.back();
    DurationParameter::Ptr duration_ptr(lastGridOfCopy->getDurationPointer());
    lastGrid->setDurationPointer(duration_ptr);
  }
  m_activeGridIndex = c.getActiveGridIndex();
  m_nextTimePoint = c.getNextTimePoint();
  m_nextReverseTimePoint = c.m_nextReverseTimePoint;

  //make copy of m_parameterizationGrid for m_originalParameterizationGrid
  m_originalParameterizationGrid.resize(numberOfGrids);
  for(unsigned i=0; i<numberOfGrids; i++){
    OriginalParameterizationGrid::Ptr tempGrid(new OriginalParameterizationGrid(c.m_parameterizationGrid[i].get()));
    m_originalParameterizationGrid[i] = tempGrid;
    m_originalParameterizationGrid[i]->insertParameterizationGrid(i,
                                                                  c.m_parameterizationGrid[i]);
    }
}

/**
* @brief getter for attribute m_esoIndex
*
* @return value of m_esoIndex
*/
unsigned Control::getEsoIndex() const
{
  return m_esoIndex;
}

/**
* @brief setter for attribute m_esoIndex
*
* @param esoIndex new value for m_esoIndex
*/
void Control::setEsoIndex(const unsigned esoIndex)
{
  m_esoIndex = esoIndex;
}



/**
* @brief getter for attribute m_lowerBound
*
* @return value of m_lowerBound
*/
double Control::getLowerBound() const
{
  return m_lowerBound;
}

/**
* @brief setter for attribute m_lowerBound
*
* @param lowerBound new value for m_lowerBound
*/
void Control::setLowerBound(const double lowerBound)
{
  m_lowerBound = lowerBound;
}

/**
* @brief getter for attribute m_upperBound
*
* @return value of m_upperBound
*/
double Control::getUpperBound() const
{
  return m_upperBound;
}

/**
* @brief setter for attribute m_upperBound
*
* @param upperBound new value for m_upperBound
*/
void Control::setUpperBound(const double upperBound)
{
  m_upperBound = upperBound;
}
/** @brief returns attribute m_isTimeInvariant */
bool Control::getIsInvariant() const
{
  return m_isTimeInvariant;
}
/** @brief sets attribute m_isTimeInvariant */
void Control::setIsInvariant(const bool isInvariant)
{
  m_isTimeInvariant = isInvariant;
}
/** @brief returns the ParametrizationGrid of given index
*   @param index of which Grid is desired.
*   @return If no grid is allocated we return an empty pointer
*           else the parameterization grid of the index is returned
*/
ParameterizationGrid::Ptr Control::getParameterizationGrid(const unsigned index) const
{
  if(!m_parameterizationGrid.empty()){
    unsigned size = m_parameterizationGrid.size();
    assert(index < size);
    return m_parameterizationGrid[index];
  }
  else{
    ParameterizationGrid::Ptr emptyPtr;
    return emptyPtr;
  }
}

/** @brief sets the control grids which were detected during structure detection.
*
*   @param controlGrids is a vector of ParamterizationGrid Pointer. The last entry needs to be a Grid
*   of the type LastParameterizationGrid
*
*   @throw bad_alloc
*
*   If the grid is initiliazed using this function, a final time needs to be set for each control grid.
*   Mind that the final end time is a global which is allocated outside of the scope of the control.
*   The remaining durations are dealt within the control.
*/
void Control::setControlGrid(const std::vector<ParameterizationGrid::Ptr> &controlGrids)
{
  // Here, we need to check wether a final time was set.
  // this assert does not work with the current testsuite. At the moment it is possible for an
  // object to have an inconsistent state
  /*for (unsigned i=0; i<controlGrids.size(); i++) {
    assert(controlGrids[i]->getDurationPointer() != NULL);
  }*/
  // check if we have a time variant or invariant control. We don't allow to set the control grid for
  // time invariant controls.
  assert(!m_isTimeInvariant);
  const unsigned numGrids = controlGrids.size();
  m_parameterizationGrid.resize(numGrids);
  if (numGrids > 0){
    m_parameterizationGrid.assign(controlGrids.begin(), controlGrids.end());
  }
  // the bounds of the control have to be included
  const double lowerbound = getLowerBound();
  const double upperbound = getUpperBound();
  std::vector<double> additionalTimePoints;
  double duration = 0;
  for (unsigned i=0; i<numGrids; i++) {
    m_parameterizationGrid[i]->setAllParameterBounds(lowerbound,upperbound);
    vector<double> points = m_parameterizationGrid[i]->getConstraintGrid();
    additionalTimePoints.reserve(additionalTimePoints.size() + points.size());
    for(unsigned j=0; j<points.size(); j++){
      points[j] += duration;
      additionalTimePoints.push_back(points[j]);
    }
    duration += m_parameterizationGrid[i]->getDurationValue();
  }

  setGridPointsVector(additionalTimePoints);

  m_originalParameterizationGrid.resize(m_parameterizationGrid.size());
  for(unsigned i=0; i<m_parameterizationGrid.size(); i++){
    OriginalParameterizationGrid::Ptr tempGrid(new OriginalParameterizationGrid(m_parameterizationGrid[i].get()));
    m_originalParameterizationGrid[i]=tempGrid;
    m_originalParameterizationGrid[i]->insertParameterizationGrid(i,
                                                                  m_parameterizationGrid[i]);
  }
  //initialize 
}

/** @brief assembles the discretization grid of the control variable scaled with the total final time.
*
*   @return the entire control grid (over all control stages) scaled for the global final time.
*
*   This grid is scaled for the global final time. It has double entries where the two stages meet. It calls
*   the function getControlGridUnscaled().
*   This function might lead to a bottle neck if it is called too often by the MetaSingleStageEso,
*   since the grid is assembeld from back to forward in the function getControlGridUnscaled(). A faster
*   alternative to assemble the grid might be a linked list.
*/
std::vector<double> Control::getControlGridScaled(void)const
{
  std::vector<double> currentGrid = getControlGridUnscaled();
  const double totalDurationValue = getTotalDuration();
  assert(totalDurationValue != 0.0);

  // we divide each entry in the vector by the total final time value
  std::transform(currentGrid.begin(), currentGrid.end(), currentGrid.begin(),
                 std::bind1st(std::multiplies<double>(), 1.0/totalDurationValue));

  assert(currentGrid.front() == 0.0); // @todo no exact comparison of doubles
  assert(currentGrid.back() == 1.0);  // @todo no exact comparison of doubles

  return currentGrid;
}

std::vector<double> Control::getControlGrid(unsigned gridIndex)const
{
  return m_controlGrids[gridIndex];
}
/** @brief assembles the discretization grid of the control variable unscaled. Thus it goes from 0.0 to
*          final time
*   @return the entire control grid from 0 to global final time
*
*   The control grid is assembled from back to forward. Every control stage is completed with a final
*   discretization point. Meaning that the "scaled" grid goes from 0.0 to 1.0 even for piecewise constant
*   discretization. Thus, we have double entries where the point 1.0 of the preceding control stage
*   meets the point 0.0 of the current control stage.
*/
std::vector<double> Control::getControlGridUnscaled(void)const
{
  assert(m_parameterizationGrid.size() > 0);

  const unsigned numGrids = m_parameterizationGrid.size();

  // initialize vector with the last entry of the structure detection
  std::vector<double> stageLength =  getStageLength();
  std::vector<double> finalTimesVector = getFinalTimeVectorAbsolute();

  // data structure for the assembled grid
  std::vector<double> assembledGrid;
  double previousDuration = 0.0;
  for (unsigned i = 0; i<numGrids; i++){
    // get current parameterization grid (scaled values from 0.0 to 1.0)
    vector<double> currentGrid =  m_controlGrids[i];
    assert(currentGrid.size() >= 2);
    assert(previousDuration >= 0.0);
    assert(stageLength[i] >= 0.0);
    // apply transformations (move by previousDuration and unscale with factor stageLength)
    transformScaledGrid(stageLength[i], previousDuration, currentGrid);
    //firts point must be exactly the absolute duration of the previous grid
    //or 0.0 if first grid
    currentGrid.front() = previousDuration;
    //last point must be exactly the absolute final time of the grid
    currentGrid.back() = finalTimesVector[i];
    previousDuration = finalTimesVector[i];

    // merge grids
    assembledGrid.insert(assembledGrid.end(),
                         currentGrid.begin(), currentGrid.end());
  }
  return assembledGrid;
}

/** @brief calculates the sum of all final times of the current control
*   @return the global final time value
*/
double Control::getTotalDuration(void) const
{
  assert(!m_parameterizationGrid.empty());

  const unsigned numberOfControlStage = m_parameterizationGrid.size();
  double totalDurationValue = 0.0;
  for(unsigned i=0; i<numberOfControlStage; i++){
    totalDurationValue += m_parameterizationGrid[i]->getDurationValue();
  }
  return totalDurationValue;
}

/** @brief returns the final time previous to the activeGridIndex
*   @param activeGridIndex: index which is considered for calculation of the elapsed final times
*   @return final time previous to the active grid index. If it is the first grid, then 0 is returned.
*
*   This function is trivial. Since, it just returns the previous final time value. Mind that we
*   only deal with global final times
*/
double Control::getElapsedDurations(unsigned activeGridIndex)const
{
  const unsigned numberOfControlStage = m_parameterizationGrid.size();
  assert(activeGridIndex <= numberOfControlStage);

  double elapsedDurationValue=0.0;
  for(unsigned i=0; i<activeGridIndex; i++){
    elapsedDurationValue += m_parameterizationGrid[i]->getDurationValue();
  }
  return elapsedDurationValue;
}

/** @brief returns all final time in according to their absolute values.
*   @return vector of all final times
*/
std::vector<double> Control::getFinalTimeVectorAbsolute(void)const
{
  std::vector<double> finalTimes = getStageLength();
  for (unsigned i=1; i< finalTimes.size(); i++) {
    finalTimes[i] += finalTimes[i-1];
  }
  return finalTimes;
}

/** @brief calculates the previous time point relative to m_nextTimePoint
*   @return a double for the previous time point
*
*   This function is intended to obtain the previous time point with respect to the member m_nexttimepoint.
*   We need this function in order to go through the grid which is required by the integrator.
*
*   Only implemented for pwc and pwl
*/
double Control::getPreviousTimePoint(void) const
{
  assert(getParameterizationGrid(m_activeGridIndex) != NULL);

  const unsigned currentActiveGridIndex = m_activeGridIndex;
  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(currentActiveGridIndex);

  const double currentDuration = currentGrid->getDurationValue();
  const double elapsedDurations = getElapsedDurations(m_activeGridIndex);

  vector<unsigned> currentActiveParameterIndices = currentGrid->getActiveParameterIndex();
  double previousPoint = 0.0;
  assert(!currentActiveParameterIndices.empty());
  
  if (currentActiveParameterIndices[0] > 0){
    assert(currentActiveParameterIndices[0] <= currentGrid->getDiscretizationGrid().size());
    const double lastGridPoint = currentGrid->getDiscretizationGrid()[currentActiveParameterIndices[0]-1];
    previousPoint = (lastGridPoint * currentDuration) + elapsedDurations;
  }

  return previousPoint;
}

/** @brief evaluates control for current time point.
*   @param totalTime this is a total time between the getPreviousTimePoint() und getNextTimePoint()
*   @return the value of the control at that time point
*
*   Before this function is called, make sure that checkParameterChange()
*   was called prevouisly.
*/
double Control::calculateCurrentControlValue(const double localTime)const
{
  assert(localTime >= 0.0);
  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(m_activeGridIndex);

  //evalSpline function on grid should cover the PWC case,
  //so check whether if/else block can be replaced by return currentGrid->evalSpline(localTime);
  double evalSpline = 0.0;
  if(currentGrid->getControlType() == PieceWiseConstant){
    vector<unsigned> activeInd = currentGrid->getActiveParameterIndex();
    assert(activeInd.size() == 1);
    Parameter::Ptr activeParam = currentGrid->getControlParameter(activeInd[0]);
    evalSpline = activeParam->getParameterValue();
  }
  else{
    evalSpline = currentGrid->evalSpline(localTime);
  }
  return evalSpline;
}
/** @brief returns the current active control parameter as an abstract parameter class to the meta eso
*   @return abstract Parameter class of the ControlParameter
*   Before this function is called, make sure that checkParameterChange() was called prevouisly.
*/
vector<Parameter::Ptr> Control::getActiveParameter(void)const
{
  // initialize values of current status of the class
  const unsigned currentActiveGridIndex = m_activeGridIndex;
  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(currentActiveGridIndex);

  vector<unsigned> currentActiveParameterIndices = currentGrid->getActiveParameterIndex();
  vector<Parameter::Ptr> currentActiveParameters(currentActiveParameterIndices.size());
  for (unsigned i=0; i<currentActiveParameterIndices.size(); i++){
    const unsigned curInd = currentActiveParameterIndices[i];
    const ControlParameter::Ptr controlParameter = currentGrid->getControlParameter(curInd);
    assert(controlParameter.get() != NULL);
    currentActiveParameters[i] = controlParameter;
  }
  return currentActiveParameters;
}

/** @brief returns the current active control parameter as an abstract parameter class to the meta eso
*   @param[in] current global time
*   @return abstract Parameter class of the ControlParameter
*   Before this function is called, make sure that checkParameterChange() was called prevouisly.
*/
vector<Parameter::Ptr> Control::getActiveParameter(const double currentGlobalTime)const
{

  unsigned gridIndex = 0;
  double globalTime = currentGlobalTime;

  while(globalTime + EPSILON > m_parameterizationGrid[gridIndex]->getDurationValue()){
    globalTime -= m_parameterizationGrid[gridIndex]->getDurationValue();
    globalTime = std::max(globalTime, 0.0);
    gridIndex++;
    assert(gridIndex <= m_parameterizationGrid.size());
    if(gridIndex == m_parameterizationGrid.size()){
      assert(globalTime < EPSILON);
      gridIndex --;
      break;
    }
  }

  std::vector<double> grid;
  grid = m_parameterizationGrid[gridIndex]->getDiscretizationGrid();
  std::vector<ControlParameter::Ptr> allParameters;
  allParameters = m_parameterizationGrid[gridIndex]->getAllControlParameter();
  std::vector<Parameter::Ptr> activeParameters;

  unsigned gridPointIndex = 0;

  double currentLocalTimeScaled = currentGlobalTime - getElapsedDurations(gridIndex);
  currentLocalTimeScaled /= getStageLength()[gridIndex];
  assert(currentLocalTimeScaled <= 1.0);
  assert(currentLocalTimeScaled >= 0.0);

  gridPointIndex = grid.size() - 2;
  while(currentLocalTimeScaled < grid[gridPointIndex]){
    gridPointIndex--;
  }
  // stored 0.0, 0.5, 1.0
  // time 0.75
  // desired result: 1
  // it might be that currentLocalTimeScaled is due to numerical errors
  // not exactly on the gridpoint. Make a close check for that purpose.
  // Note that in that case currentLocalTimeScaled always the smaller value
  if( grid[gridPointIndex + 1] - currentLocalTimeScaled < EPSILON
    && gridPointIndex < allParameters.size() -1){
    gridPointIndex++;
  }



  switch(m_parameterizationGrid[gridIndex]->getControlType()){
    case PieceWiseConstant:
      assert(allParameters.size() > gridPointIndex);
      activeParameters.push_back(allParameters[gridPointIndex]);
      break;
    case PieceWiseLinear:
      assert(allParameters.size() + 1 > gridPointIndex);
      activeParameters.push_back(allParameters[gridPointIndex]);
      activeParameters.push_back(allParameters[gridPointIndex +1]);
      break;
    default:
      assert(false);
  }

  return activeParameters;
}

/** @brief checks if we are past a time point in the control grid
*   @param time is a global time between 0 - global final time.
*   @return true if the parameter has changed and else false
*
*   Checks if we are past a time point in the control grid. If this is the case, we have the next
*   parameterization and thus we give back true. This function needs to be called first to determine
*   a parameter change. Only afterwards the functions changeActiveParameter(), calculateControlValue()
*   should be called.
*
*/
bool Control::checkParameterChange(const double time)const
{
  assert(time >= 0.0);
  assert(time <= getTotalDuration());

  // initialize values of current status of the class
  const double nextTimePoint = getNextTimePoint();

  const double relDiff = (nextTimePoint - time)/nextTimePoint;
  if(relDiff>1e-15){
    // the parameter did not change
    return false;
  }
  else{ // time >= nextTimePoint
    return true;
  }
}

/** @brief checks if we are past a time point in the control grid
*   @param time is a global time between 0 - global final time.
*   @return false if the parameter has changed and else true
*   does reverse as checkParameterChange function does
*
*/
bool Control::checkParameterChangeReverse(const double time)const
{
  assert(time >= 0.0);
  assert(time <= getTotalDuration());
  
  const double threshold = 1e-15;
  double relDiff;
  if(m_nextReverseTimePoint < threshold){
    relDiff = (m_nextReverseTimePoint - time)/threshold;
  }
  else{
    relDiff = (m_nextReverseTimePoint - time)/m_nextReverseTimePoint;
  }
  if(relDiff < threshold){
    return false;
  }
  else{
    return true;
  }
}

/** @brief this function changes the current active parameter to the next active parameter
*
*   This function changes the activity of the parameters corresponding to the parameter class
*   such that the integrator knows which parameter is active and which parameter is inactive.
*   Further it is required to change the parameter such that the control value evaluation can be done
*   in an efficient manner. Make sure that the function checkParameterChange() is called prior
*   to calling this function.
*/
void Control::changeActiveParameter(void)
{
// initialize values of current status of the class
  unsigned currentActiveGridIndex = m_activeGridIndex;
  ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(currentActiveGridIndex);

  double elapsedDurations = getElapsedDurations(m_activeGridIndex);

  vector<unsigned> currentActiveParameterIndices = currentGrid->getActiveParameterIndex();
  const unsigned gridSize = currentGrid->getDiscretizationGrid().size();
  // Here we want to obtain the largest parameterization index.
  // The time grid includes the final time for piecewise constant approximations. Thus, the true size
  // of available parameters is smaller by one.
  unsigned lastTimeEntry = 0;
  if(currentGrid->getControlType() == PieceWiseConstant){
    lastTimeEntry = 1;
  }
  // To determine the largest parameterization index we need to subtract another one.
  const unsigned transformToIndex = 1;
  const unsigned lastParameterizationIndex = gridSize - lastTimeEntry - transformToIndex;

  vector<ControlParameter::Ptr> currentActiveParameters(currentActiveParameterIndices.size());
  for(unsigned i=0; i<currentActiveParameterIndices.size(); i++){
    currentActiveParameters[i] = currentGrid->getControlParameter(currentActiveParameterIndices[i]);
    assert(currentActiveParameters[i] != NULL);
  }
  //// wether the parameter changes or not was checked in the function checkParameterChange
  //// so here the current active parameter is set to false anyway.
  //// In case of piecewiselinear, we always turn off all parameters.
  for(unsigned i=0; i<currentActiveParameters.size(); i++){
    currentActiveParameters[i]->setParameterSensActivity(false);
  }

  if (currentActiveParameterIndices.back() < lastParameterizationIndex){
  // if we are in the same stage we just take the next parameters and update the pointers
    for(unsigned i=0; i<currentActiveParameterIndices.size(); i++){
      currentActiveParameterIndices[i]++;
    }
  }else{ //  currentActiveParameterIndex >= lastParameterizationIndex
    //before jumping to the next grid - set the old DurationParameter inactive
    const bool active = true;
    
    //jump to the next grid
    currentActiveGridIndex++;
    setActiveGridIndex(currentActiveGridIndex);
    // when we jump to the next stage we start counting the active parameter index from 0
    currentGrid = getParameterizationGrid(currentActiveGridIndex);
    currentActiveParameterIndices.clear();
    currentActiveParameterIndices.reserve(2); // we can have a maximum of two entries
    currentActiveParameterIndices.push_back(0);
    if(currentGrid->getControlType() == PieceWiseLinear){
      currentActiveParameterIndices.push_back(1);
    }
    currentActiveParameters.resize(currentActiveParameterIndices.size());
    // since we are in the next stage we have another final time in front of our active index.
    elapsedDurations = getElapsedDurations(currentActiveGridIndex);
    //activate the next DurationParameter
    getActiveDurationParameter()->setParameterSensActivity(active);
  }
  currentGrid->setActiveParameterIndex(currentActiveParameterIndices);
  // activate new parameters
  for(unsigned i=0; i<currentActiveParameterIndices.size(); i++){
    currentActiveParameters[i] = currentGrid->getControlParameter(currentActiveParameterIndices[i]);
    currentActiveParameters[i]->setParameterSensActivity(true);
  }
  // we also need to update the next time point.
  std::vector<double> stageLength = getStageLength();
  // obtain the next grid point
  assert(currentActiveParameterIndices[0]+1 < currentGrid->getDiscretizationGrid().size());
  const double nextGridPoint = currentGrid->getDiscretizationGrid()[currentActiveParameterIndices[0]+1];
  // the grid point is scaled and needs to be unscaled
  assert(currentActiveGridIndex < stageLength.size());
  const double nextTimePoint = nextGridPoint*stageLength[currentActiveGridIndex] + elapsedDurations;

  setNextTimePoint(nextTimePoint);
}

/** @brief this function does reverse as changeActiveParameter function does
*
*/
void Control::changeActiveParameterReverse()
{
  // initialize values of current status of the class
  unsigned currentActiveGridIndex = m_activeGridIndex;
  ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(currentActiveGridIndex);


  vector<unsigned> currentActiveParameterIndices = currentGrid->getActiveParameterIndex();
    
  const unsigned firstParameterizationIndex = 0;

  vector<ControlParameter::Ptr> currentActiveParameters(currentActiveParameterIndices.size());
  for(unsigned i=0; i<currentActiveParameterIndices.size(); i++){
    currentActiveParameters[i] = currentGrid->getControlParameter(currentActiveParameterIndices[i]);
    assert(currentActiveParameters[i] != NULL);
  }

  if (currentActiveParameterIndices.front() > firstParameterizationIndex){
  // if we are in the same stage we just take the next parameters and update the pointers
    for(unsigned i=0; i<currentActiveParameterIndices.size(); i++){
      currentActiveParameterIndices[i]--;
    }
  }else{ //  currentActiveParameterIndex == firstParameterizationIndex
    //jump to the previous grid
    assert(currentActiveGridIndex > 0);
    currentActiveGridIndex--;
    setActiveGridIndex(currentActiveGridIndex);
    // when we jump to the next stage we start counting the active parameter index from 0
    currentGrid = getParameterizationGrid(currentActiveGridIndex);
    // assumed that the current grid is in the end state of the forward integration
    // so the last (or last two) parameter(s) should be still set
    currentActiveParameterIndices = currentGrid->getActiveParameterIndex();
  }
  currentGrid->setActiveParameterIndex(currentActiveParameterIndices);
  
  //set nextReversePoint
  std::vector<double> stageLength = getStageLength();
  const double currentGridPoint = currentGrid->getDiscretizationGrid()[currentActiveParameterIndices[0]];
  const double elapsedDurations = getElapsedDurations(m_activeGridIndex);
  m_nextReverseTimePoint = currentGridPoint*stageLength[m_activeGridIndex] + elapsedDurations;
}


double Control::getOriginalLocalTime(const double localTime,
                                     const ParameterizationGrid::Ptr &currentGrid, 
                                     const OriginalParameterizationGrid::Ptr &currentOriginalGrid)const
{
  //find originalLocalTime
  double originalLocalTime = localTime;
  if(currentOriginalGrid->getNumParameterizationGrids() != 1){//to avoid precision errors due to multiplication and then division with same value
    double sumDurations = currentOriginalGrid->sumUpDurations(m_activeGridIndex);
    originalLocalTime = sumDurations+currentGrid->getDurationValue()*localTime;
    originalLocalTime /= currentOriginalGrid->getDurationValue();
  }
  return originalLocalTime;
}


unsigned Control::getCurrentIndex()const
{
  //find originalGrid
  unsigned index=0;
  unsigned numberParameterizationGrids = m_originalParameterizationGrid[index]->getNumParameterizationGrids();
  while(m_activeGridIndex>=numberParameterizationGrids){
    index++;
    numberParameterizationGrids += m_originalParameterizationGrid[index]->getNumParameterizationGrids();
  }
  return index;
}

std::vector<unsigned> Control::getActiveInd(const ParameterizationGrid::Ptr &currentGrid)const
{
  //find activeParameterIndices
  vector<unsigned> localActiveInd = currentGrid->getActiveParameterIndex();
  vector<Parameter::Ptr> activeParam(1);
  vector<unsigned> activeInd(1);
  activeParam[0]=currentGrid->getControlParameter(localActiveInd[0]);
  activeInd[0]=activeParam[0]->getParameterIndex();
  if(currentGrid->getControlType() == PieceWiseLinear){
    activeParam.push_back(currentGrid->getControlParameter(localActiveInd[1]));
    activeInd.push_back(activeParam[1]->getParameterIndex());
  }
  return activeInd;
}

/**
* @brief evaluates the control derivative ut at the time point t
*   @param[in] localTime is a local time.
*   @param[in] paramIndex index of parameter
*   @return the derivative of the control
*/
double Control::getControlDerivative()const
{
  
  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(m_activeGridIndex);

  unsigned index = getCurrentIndex();
  OriginalParameterizationGrid::Ptr currentOriginalGrid=m_originalParameterizationGrid[index];



  std::vector<unsigned> activeInd = getActiveInd(currentGrid);
  
  
  double controlDerivative = currentOriginalGrid->getSplineDerivative(activeInd);

  //scale derivative with stage duration
  return controlDerivative*getTotalDuration();
}

/** @brief evaluates the spline for current time point.
*   @param[in] time is a local time.
*   @param[in] index of parameter
*   @return the value of the d-spline
*/
double Control::getCurrentDSplineValue(const double localTime, const unsigned paramIndex)const
{

  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(m_activeGridIndex);

  unsigned index = getCurrentIndex();
  OriginalParameterizationGrid::Ptr currentOriginalGrid=m_originalParameterizationGrid[index];


  double originalLocalTime = getOriginalLocalTime(localTime, currentGrid, currentOriginalGrid);

  std::vector<unsigned> activeInd = getActiveInd(currentGrid);
  
  
  double dSplineValue = currentOriginalGrid->evaldSpline(originalLocalTime,
                                                          paramIndex,
                                                          activeInd);

  return dSplineValue;  
}

/** @brief sets all control parameters to be without sensitivity
*
* This function requires that the parameters are already initialized.
* unsets isDecisionVariable as well, since there cannot be a decision variable
* without sensitivities.
*/
void Control::setControlWithoutSens(void)
{
  const unsigned numberOfGrids = m_parameterizationGrid.size();
  for (unsigned i=0; i<numberOfGrids; i++){
    m_parameterizationGrid[i]->setAllParametersWithoutSens();
  }
  unsetIsDecisionVariable();
}

/**
* @brief sets for all control parameters isDecisionVariable to false
*
* @note this function requires the parameters to be initialized
*/
void Control::unsetIsDecisionVariable(void)
{
  const unsigned numberOfGrids = m_parameterizationGrid.size();
  for(unsigned i=0; i<numberOfGrids; i++){
    std::vector<ControlParameter::Ptr> p = m_parameterizationGrid[i]->getAllControlParameter();
    for(unsigned j=0; j< p.size(); j++){
      p[j]->setIsDecisionVariable(false);
    }
  }
}

/** @brief initializes the control vector for first Integration step
*
*   Sets all required members to zero and finds the next time point.
*/
void Control::initializeForFirstIntegration(void)
{
  const unsigned firstGridIndex = 0;
  setActiveGridIndex(firstGridIndex);
  vector<unsigned> firstParameterIndices;
  const ControlType type = m_parameterizationGrid[firstGridIndex]->getControlType();
  if(type == PieceWiseConstant){
    firstParameterIndices.push_back(0);
  }else if(type == PieceWiseLinear){
    firstParameterIndices.push_back(0);
    firstParameterIndices.push_back(1);
  }else{
    // we should never get here
    assert(1==0);
  }
  m_parameterizationGrid[m_activeGridIndex]->setActiveParameterIndex(firstParameterIndices);

  const double currentDuration = m_parameterizationGrid[firstGridIndex]->getDurationValue();
  assert(m_parameterizationGrid[firstGridIndex]->getDiscretizationGrid().size() > 1);
  const double firstPoint = m_parameterizationGrid[firstGridIndex]
                        ->getDiscretizationGrid()[firstParameterIndices[0]+1]*currentDuration;
  setNextTimePoint(firstPoint);

  this->setControlPointerForParameters();
  vector<Parameter::Ptr> activeParams = getActiveParameter();
  for (unsigned i=0; i<activeParams.size(); i++){
    activeParams[i]->setParameterSensActivity(true);
  }

  getActiveDurationParameter()->setParameterSensActivity(true);

  //reset durationMultiplier for new integration
  for(unsigned i=0; i<m_parameterizationGrid.size(); i++){
    m_parameterizationGrid[i]->getDurationPointer()->resetDurationMultiplier();
  }

  //update all original grids
  for(unsigned i=0; i<m_originalParameterizationGrid.size(); i++){
    m_originalParameterizationGrid[i]->update();
  }
}

/** @brief sets the value of m_nextReverseTimePoint attribute
*
*/
void Control::setReverseTimePoint()
{
  
  std::vector<double> controlGrid = m_parameterizationGrid.back()->getDiscretizationGrid();
  const double elapsedDuration = this->getElapsedDurations(m_parameterizationGrid.size()-1);
  const double stageLength = m_parameterizationGrid.back()->getDurationValue();
  unsigned gridSize = controlGrid.size();
  assert(gridSize > 1);
  m_nextReverseTimePoint = elapsedDuration + stageLength*controlGrid[gridSize - 2];
}


/** @brief sets the pointer of the current control to the related control paramters
*
*   The control parameter needs to know to which control it belongs in order to evaluate the
*   correct dspline function. Thus, we pass the pointer to the control parameter.
*/
void Control::setControlPointerForParameters(void)
{
  const unsigned numberOfGrids = m_parameterizationGrid.size();
  for(unsigned i = 0; i < numberOfGrids; i++){
    std::vector<ControlParameter::Ptr> allParameters = m_parameterizationGrid[i]->getAllControlParameter();
    const unsigned numberOfParameters = allParameters.size();
    for(unsigned j=0; j<numberOfParameters; j++){
      allParameters[j]->setControl(this);
    }
  }
}

/** @brief returns the duration parameter of the active grid
*   @return a pointer to the DurationParameter as Parameter
*/
DurationParameter::Ptr Control::getActiveDurationParameter(void)const
{
  // initialize values of current status of the class
  const unsigned gridIndex = m_activeGridIndex;
  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(gridIndex);

  return currentGrid->getDurationPointer();
}

/**
* @copydoc Control::getActiveDurationParameter(void)
*
* Here the correct grid is calculated using a given time. This time must not be greater
* than the global duration (duration stored on the last grid).
* @param currentGlobalDuration current duration of the MetaData final time vector
*/
DurationParameter::Ptr Control::getActiveDurationParameter(const double currentGlobalDuration)const
{
  assert(currentGlobalDuration <= m_parameterizationGrid.back()->getDurationValue());
  unsigned gridIndex = 0;
  while(currentGlobalDuration > m_parameterizationGrid[gridIndex]->getDurationValue()){
    gridIndex++;
  }
  return m_parameterizationGrid[gridIndex]->getDurationPointer();
}

/** @brief checks if the given value is equal to the final time of the active control grid
*   @return true if the final time of the integrator is equal to the final time of the control
*   @param integratorDuration has to be a global final time
*
*   This function is required to find the final time to the corresponding control. This information is
*   required by the integrator to sucessfully switch the control stages.
*/
bool Control::isCurrentDuration(const double integratorDuration)const
{
  assert(integratorDuration >= 0.0);

  const unsigned gridIndex = m_activeGridIndex;
  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(gridIndex);

  const double currentDuration = currentGrid->getDurationValue();
  return currentDuration == integratorDuration;
}
/** @brief determine the stage length
*   @return the stage lengths as double vector
*/
std::vector<double> Control::getStageLength(void) const
{
  const unsigned numberOfGrids = m_parameterizationGrid.size();
  std::vector<double> stageLength(numberOfGrids);

  for (unsigned i=0; i< numberOfGrids; i++) {
    stageLength[i] = m_parameterizationGrid[i]->getDurationValue();
  }
  return stageLength;
}

/** @brief applies scaling transformation to the current Grid
*   @param currentStageLength current stage length which determins the weighting of the grid points
*   @param previousDurations previous final times that should be added to the grid points
*   @param currentGrid which runs on a total time
*
*   The grid points are multiplied with the the stage length and then the previous final times
*   are added such that we obtain a grid running on a total time.
*/
void transformScaledGrid(const double currentStageLength,
                         const double previousDurations,
                         std::vector<double> &currentGrid)
{
  assert(currentStageLength >= 0.0);
  assert(previousDurations >= 0.0);

  // mulitply each entry with the stage length value
  std::transform(currentGrid.begin(), currentGrid.end(), currentGrid.begin(),
               std::bind1st(std::multiplies<double>(),currentStageLength));

  // next add all final times of previous stages to the current stage
  std::transform(currentGrid.begin(), currentGrid.end(), currentGrid.begin(),
               std::bind1st(std::plus<double>(),previousDurations));
}

/** @brief transforms a total time to a global time. Required for the spline evlaution
*   @param[in] totalTime the value of the total time
*   @return value of the local time
*/
double Control::transformTotalTimeToLocalTime(const double totalTime) const
{
  const double nextPoint = getNextTimePoint();
  const double previousPoint = getPreviousTimePoint();
  // check if the requested time is in the current time interval
  assert(totalTime <= nextPoint && previousPoint >= 0.0);

  const ParameterizationGrid::Ptr currentGrid = getParameterizationGrid(m_activeGridIndex);

  // Since every parameterization grid runs from 0 to 1. we need the local time in the
  // parameterization grid.
  double localTime = totalTime;
  const double elapsedTf =  getElapsedDurations(m_activeGridIndex);
  vector<double> stageLength = getStageLength();
  localTime = (localTime - elapsedTf) / stageLength[m_activeGridIndex];
  assert(localTime>=0);
  return localTime;
}

/** @brief sets duration parameters of m_parameterizationGrid
*   @param[in] inserted duration parametr
*   @param[in] index of the grid point
*/
void Control::insertDurationParameter(const DurationParameter::Ptr &insertedDuration,
                                             unsigned gridIndex)
{
  //find right grid (first grid with final time equal or higher than given duration
  unsigned numGrids = m_parameterizationGrid.size();

  assert(gridIndex < numGrids);
  //if final times are equal, replace grid final time parameter with given final time parameter
  double a = m_parameterizationGrid[gridIndex]->getDurationValue();
  double b = insertedDuration->getParameterValue();;
  if(fabs(a-b)<EPSILON){
    //copy information from local DurationParameter to global DurationParameter
    DurationParameter::Ptr localDuration = m_parameterizationGrid[gridIndex]->getDurationPointer();
    bool withSensitivity = insertedDuration->getWithSensitivity()
                        || localDuration->getWithSensitivity();
    insertedDuration->setWithSensitivity(withSensitivity);
    bool isDecisionVariable = insertedDuration->getIsDecisionVariable()
                        || localDuration->getIsDecisionVariable();
    insertedDuration->setIsDecisionVariable(isDecisionVariable);
    m_parameterizationGrid[gridIndex]->setDurationPointer(insertedDuration);
  }
  //else split grid into two
  else{
    splitGrid(insertedDuration, gridIndex);
  }
}

/** @brief create out of grid at grid Index two separate grids splitted at insertedDurationValue
*
*   Determines which points are on the right and on the left of the inserted duration
*	and calculates at which point an grid interval is splitted.
*  e.g. grid = [0,0.25, 0.75, 1.0], duration =4, inserted duration = 2
* => discretizationGrid1 =  [0.0, 0.5, 1.0], discretizationGrid2 = [0.0, 0.5, 1.0]
*  splittingFactor = 0.5
*   Gridpoint 0.5(on the old grid) is artificially inserted in the grid (equals now 1.0 on grid1, 0.0 on grid2)
*   @param[out] discretizationGrid1 grid points on the left of inserted duration
*   @param[out] discretizationGrid2 grid points on the right of the inserted duration
*   @param[in] gridIndex index of current grid
*   @param[in] insertedDurationValue inserted duration value
*   @param[out] splittingFactor determines point at which a grid interval is splitted
*/
void Control::getSplittedDiscretizationGrids(std::vector<double> &discretizationGrid1,
                                             std::vector<double> &discretizationGrid2,
                                       const unsigned gridIndex,
                                       const double insertedDurationValue,
                                             double &splittingFactor)const
{
  ParameterizationGrid::Ptr currentGrid = m_parameterizationGrid[gridIndex];
  double oldDuration = currentGrid->getDurationValue();
  std::vector<double> gridPoints = currentGrid->getDiscretizationGrid();

  //calculate new grids and divide parameter vector
  double newGridBorder = insertedDurationValue/oldDuration;
  assert(newGridBorder < 1.0);

  assert(gridPoints.front() == 0.0 && gridPoints.back() == 1.0);
  discretizationGrid1.push_back(0.0);
  discretizationGrid2.push_back(0.0);
  //leave out first and last gridpoints (0.0 and 1.0)
  for(unsigned i=1; i<gridPoints.size(); i++){
    if(gridPoints[i] <= newGridBorder + EPSILON){
      discretizationGrid1.push_back(gridPoints[i] * oldDuration);
    }
    else if(gridPoints[i] > newGridBorder + EPSILON){
      discretizationGrid2.push_back(gridPoints[i]*oldDuration);
    }
  }
  //if no point (other than 0) was put into grid2, then newGridBorder is equal
  //to the duration - that case was already handled in insertDurationParameter
  assert(discretizationGrid2.size() > 1);
  //calculate point at which an interval is splitted
  splittingFactor = (insertedDurationValue - discretizationGrid1.back())
                    /(discretizationGrid2[1] - discretizationGrid1.back());

  //scale both discretization grids
  for(unsigned i=1; i<discretizationGrid1.size(); i++){
    discretizationGrid1[i] /= insertedDurationValue;
  }
  for(unsigned i=1; i<discretizationGrid2.size(); i++){
    discretizationGrid2[i] -= insertedDurationValue;
    discretizationGrid2[i] /= (oldDuration - insertedDurationValue);
  }
  //if last point is very close to 1.0, reset point to 1.0, otherwise add 1.0 to grid
  if(discretizationGrid1.back()+ EPSILON > 1.0){
    discretizationGrid1.pop_back();
  }
  discretizationGrid1.push_back(1.0);

  //last point is always exactly 1.0 - override rounding errors
  discretizationGrid2.back() = 1.0;
}

/** @brief split control vector in controls on grid1 and controls on grid2 (see function getSplittedDiscretizationGrids)
*
* @param[out] parameters1 parameters corresponding to grid1
* @param[out] parameters2 parameters corresponding to grid2
* @param[in] gridIndex index of grid to be splitted
* @param[in] numPointsGrid1 number of gridpoints on grid1
* @param[in] splittingFactor point at which a grid interval is splitted
 * @todo split code for piecewise linear and constant in two separate functions. This needs rework
 */
void Control::getSplittedControlParameters(std::vector<ControlParameter::Ptr> &parameters1,
                                           std::vector<ControlParameter::Ptr> &parameters2,
                                     const unsigned gridIndex,
                                     const unsigned numPointsGrid1,
                                     const double splittingFactor)const
{
  ParameterizationGrid::Ptr currentGrid = m_parameterizationGrid[gridIndex];
  std::vector<ControlParameter::Ptr> allControlParameters;
  allControlParameters = currentGrid->getAllControlParameter();
  // now divide parameters into two new vectors
  // if a parameter itself is splitted it may appear twice
  // (using a SubstituteParameter for the second grid)
  unsigned numParameters1 = std::min(numPointsGrid1-1, (unsigned)allControlParameters.size());
  parameters1.assign(allControlParameters.begin(),
                     allControlParameters.begin() + numParameters1);
  parameters2.assign(allControlParameters.begin() + numParameters1,
                       allControlParameters.end());

  if(currentGrid->getControlType() == PieceWiseLinear){
    //in case of piecewise linear two parameters overlap, so add a further parameter
    parameters1.push_back(allControlParameters[numParameters1]);
    //do not substitute if inserted final time is on the grid
    if(splittingFactor > EPSILON){
      parameters2.insert(parameters2.begin(), allControlParameters[numPointsGrid1-2]);
      // both grids overlap in two parameters:
      // grid1 = (p1,p2,...,pm,pm+1)
      // grid2 = (pm,pm+1,pm+2,...,pn)
      // substitute last parameter of the first grid (pm+1) and the first parameter of the second grid (pm)
      // formula for the value computation is pm*(1-t)+(pm+1)*t.
      ControlParameter::Ptr sub1(
           new SubstituteParameter(parameters2[1], //related parameter (pm+1)
                                   parameters1[parameters1.size()-2], //co parameter (pm)
                                   splittingFactor)); //factor of the substituted parameter
      parameters1.back() = sub1;
      // for substitution of the first parameter of the second grid, simply switch inputs of sub1
      ControlParameter::Ptr sub2(new SubstituteParameter(parameters1[parameters1.size()-2],
                                                        parameters2[1] ,
                                                        1.0 - splittingFactor));
      parameters2.front() = sub2;
    }else{
      ControlParameter::Ptr sub2(new SubstituteParameter(parameters1.back(),
                                                      parameters2[1] ,
                                                      1.0 - splittingFactor));
      parameters2.front() = sub2;
    }
  }
  else if(currentGrid->getControlType() == PieceWiseConstant){
    if(splittingFactor > EPSILON){
      ControlParameter::Ptr sub(new SubstituteParameter(parameters1.back(),
                                                        parameters1.back(),
                                                        1.0));
      parameters2.insert(parameters2.begin(), sub);
    }
  }
}

/** @brief splits grids
*
* 1. Determine discretization grids of the new splitted grids
* 2. Create Parameterization for the splitted discretization grid
* 3. Create two separate grids with new parameterization and discretization
* 4. Set inserted duration to grid1
* 5. Create temporary duration for grid2
*  @param[in,out] insertedDuration duration parameter to be inserted
*  @param[in] gridIndex index of the current grid that is to be splitted
*/
void Control::splitGrid(const DurationParameter::Ptr &insertedDuration,
                                             unsigned gridIndex)
{
  double insertedDurationValue = insertedDuration->getParameterValue();
  //current grid becomes first grid
  ParameterizationGrid::Ptr currentGrid = m_parameterizationGrid[gridIndex];
  double oldDuration = currentGrid->getDurationValue();
  //new grid becomes second grid
  ParameterizationGrid::Ptr newGrid(new ParameterizationGrid());

  std::vector<double> discretizationGrid1, discretizationGrid2;
  double splittingFactor = 0.0;
  getSplittedDiscretizationGrids(discretizationGrid1,
                                 discretizationGrid2,
                                 gridIndex,
                                 insertedDurationValue,
                                 splittingFactor); //this function determines the splitting factor

  std::vector<ControlParameter::Ptr> parameters1, parameters2;
  const unsigned numPointsGrid1 = discretizationGrid1.size();
  getSplittedControlParameters(parameters1,
                               parameters2,
                               gridIndex,
                               numPointsGrid1,
                               splittingFactor); //this function uses the splitting factor
  // before we update the current grid, we have to get the information with respect to
  // the optimization of the final time, such that we can transfer it to the new parameter
  bool isDecVar = currentGrid->getDurationPointer()->getIsDecisionVariable();
  bool isSensVar = currentGrid->getDurationPointer()->getWithSensitivity();
  // update current grid
  currentGrid->setDurationPointer(insertedDuration);
  currentGrid->setDiscretizationGrid(discretizationGrid1);
  currentGrid->setAllControlParameter(parameters1);

  //set up new grid
  newGrid->setLogger(this->m_logger);
  newGrid->setControlType(currentGrid->getControlType());
  newGrid->setDiscretizationGrid(discretizationGrid2);
  newGrid->setAllControlParameter(parameters2);
  // create a (temporary) new final time that is reduced. That final time will be
  // replaced during the next call to insertDuration
  DurationParameter::Ptr newGridDuration(
                      new DurationParameter(oldDuration - insertedDurationValue));
  newGridDuration->setIsDecisionVariable(isDecVar);
  newGridDuration->setWithSensitivity(isSensVar);
  newGrid->setDurationPointer(newGridDuration);

  m_parameterizationGrid.insert(m_parameterizationGrid.begin() + gridIndex + 1, newGrid);


  //get original gridIndex
  double totalDuration=insertedDuration->getParameterValue()+this->getElapsedDurations(gridIndex);
  double temp=0;
  unsigned index;
   for(index=0; index<m_originalParameterizationGrid.size(); index++){
    temp+=m_originalParameterizationGrid[index]->getDurationValue();
    if(temp>totalDuration){
      m_originalParameterizationGrid[index]->insertParameterizationGrid(gridIndex,
                                                                    currentGrid,
                                                                    newGrid);
      break;
    }
  }
  for(unsigned i=index+1; i<m_originalParameterizationGrid.size(); i++){
    m_originalParameterizationGrid[i]->changeIndices();
  }
  for(unsigned i=0; i<m_originalParameterizationGrid.size(); i++){
    m_originalParameterizationGrid[i]->update();
  }

}

/**
* @brief creates control output
*
* @param[in] getPtr pointer to Eso (for replacing EsoIndices with names in the model)
* @param[in] iODP IntOptDataMap mapping (contains sensitivities)
* @return ParameterOutput structure
*/
DyosOutput::ParameterOutput  Control::getOutput(GenericEso::Ptr &genPtr,
                                                IntOptDataMap iODP)
{
  DyosOutput::ParameterOutput output;

  const unsigned controlEsoIndex = getEsoIndex();
  output.esoIndex = controlEsoIndex;
  output.name = genPtr->getVariableNameOfIndex(controlEsoIndex);
  output.esoIndex = controlEsoIndex;
  output.lowerBound = m_lowerBound;
  output.upperBound = m_upperBound;
  output.isTimeInvariant = m_isTimeInvariant;
  output.grids.resize(getNumberOfGrids());
  for(unsigned i=0; i<getNumberOfGrids(); i++)
  {
    output.grids[i] = getParameterizationGrid(i)->createParameterGridOutput(iODP, genPtr);
  }
  if (m_isTimeInvariant) {
    output.value = getParameterizationGrid(0)->evalSpline(0.0);
  }
  return output;
}

/** @brief Inlcudes time points into the control grid,
  *        e.g. required for doubling the resolution of the constraint.
  * @param[in] additionalTimepoints are unscaled and not specific for a grid.
  *
  * This function assembles a grid from the control discretization and additional grid points.
  * First we merge the additional time points into the right control grid.
  * Then, we delete points which are very close to each other (rounding error).
  * This can be done in a more efficient way, but this is easy to understand.
  */
void Control::setGridPointsVector(const std::vector<double> &additionalTimepoints)
{
  m_controlGrids.clear();
  vector<double> finalTimeVec(1,0.0);

  std::set<double> allGrids;
  for(unsigned i=0; i<m_parameterizationGrid.size(); i++){
    vector<double> grid = m_parameterizationGrid[i]->getDiscretizationGrid();
    // scale
    double duration = m_parameterizationGrid[i]->getDurationValue();
    for(unsigned j=0; j<grid.size(); j++){
      grid[j] = grid[j] * duration + finalTimeVec.back();
    }
    allGrids.insert(grid.begin(), grid.end());
    finalTimeVec.push_back(duration + finalTimeVec.back());
  }
  allGrids.insert(additionalTimepoints.begin(), additionalTimepoints.end());


  //distribute gridpoints of allGrids to separate parametrization grids
  vector<vector<double> > grid;
  grid.resize(finalTimeVec.size() - 1);
  set<double>::iterator firstEntry, lastEntry;
  firstEntry = allGrids.begin();
  for(unsigned i=1; i<finalTimeVec.size(); i++){
    set<double>::iterator pos = allGrids.find(finalTimeVec[i]);
    lastEntry = pos;
	//advance should have the same effect as lastEntry++;
    std::advance(lastEntry, 1); // so we copy the value as well
    grid[i-1].insert(grid[i-1].begin(), firstEntry, lastEntry);
    firstEntry = pos;
  }


  if(additionalTimepoints.size() > 0){
    // we only need to remove points if we actually included any
    for(unsigned i=0; i<grid.size(); i++){
      vector<double> manipulate = grid[i];
      unsigned wki = 0;
      for(unsigned j=0; j<grid[i].size()-1; j++){
        double compare = grid[i][j] - grid[i][j+1];
        if(fabs(compare)< EPSILON){
          vector<double>::iterator it = manipulate.begin();
          advance(it, wki);
          if(std::find(additionalTimepoints.begin(), additionalTimepoints.end(), *it) 
             != additionalTimepoints.end()){
            // if we find the value, we have to advance to the next one.
            // we delete the value that belongs to the control and not to the additionalTimePoints.
            // these values are the same for all controls
            advance(it, 1);
          }
          
          manipulate.erase(it);
        }else{
          wki++;
        }
      }
      grid[i] = manipulate;
    }
  }
  double duration = 0;
  for(unsigned i=0; i<grid.size(); i++){
    //sort(grid[i].begin(), grid[i].end());
    double gridLength = m_parameterizationGrid[i]->getDurationValue();
    for(unsigned j=0; j<grid[i].size();j++){
      grid[i][j] -= duration;
      grid[i][j] /= gridLength;
    }
    // so we don't have computational difficulties.
    grid[i].front() = 0.0;
    grid[i].back() = 1.0;
    duration += gridLength;
    m_controlGrids.push_back(grid[i]);
  }

}
