/**
* @file Parameter.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData - Part of DyOS                                              \n
* =====================================================================\n
* This file contains the definitions of the memberfunctions of classe  \n
* Parameter according to the UML Diagramm METAESO_8                    \n
* =====================================================================\n
* @author Fady Assassa, Klaus Stockmann, Tjalf Hoffmann
* @date 17.10.2011
*/

#include <cassert>
#include <vector>
#include <cfloat>

#include "Array.hpp"
#include "Parameter.hpp"
#include "IntegratorMetaData.hpp"
#include "MetaDataExceptions.hpp"
#include "Control.hpp"


/** @brief standard constructor */
Parameter::Parameter()
{
  initializeParameterActivity();
}

/** @brief constructor that sets the value*/
Parameter::Parameter(const double value)
{
  initializeParameterActivity();
  m_value = value;
}

/**
* @brief copy constructor
* @param toCopy Parameter object to be copied
*/
Parameter::Parameter(const Parameter &toCopy)
{
  m_lowerbound = toCopy.m_lowerbound;
  m_upperbound = toCopy.m_upperbound;
  m_parameterIndex = toCopy.m_parameterIndex;
  m_decVarIndex = toCopy.m_decVarIndex;
  m_sensActivity = toCopy.m_sensActivity;
  m_value = toCopy.m_value;
  m_withSensitivity = toCopy.m_withSensitivity;
  m_isDecisionVariable = toCopy.m_isDecisionVariable;
  m_isInitialized = false;
  m_lagrangeMultiplier = toCopy.m_lagrangeMultiplier;
}

/** @brief used only in the constructor to set the parameter activity */
void Parameter::initializeParameterActivity(void)
{
  m_withSensitivity = true;
  m_isDecisionVariable = true;
  m_sensActivity = true;
  m_parameterIndex = -1;
  m_decVarIndex = -1;
  m_lowerbound = -DYOS_DBL_MAX;
  m_upperbound = DYOS_DBL_MAX;
  m_value = 0.0;
  m_lagrangeMultiplier = 0.0;
}
/** @brief standard destructor */
Parameter::~Parameter(){}

/** @brief sets the parameter value */
void Parameter::setParameterValue(const double value)
{
  m_value = value;
}

/** @brief gets the parameter value */
double Parameter::getParameterValue(void) const
{
  return m_value;
}

/**
* @brief set the parameter index to a new value
* @param parameterIndex value to be set
*/
void Parameter::setParameterIndex(int parameterIndex)
{
  m_parameterIndex = parameterIndex;
}

/**
* @brief set the decision variable index to a new value
* @param decVarIndex value to be set
*/
void Parameter::setDecVarIndex(int decVarIndex)
{
  m_decVarIndex = decVarIndex;
}


/** @brief gets the lower bound of the parameter */
double Parameter::getParameterLowerBound(void) const
{
  return m_lowerbound;
}
/** @brief gets the upper bound of the parameter */
double Parameter::getParameterUpperBound(void) const
{
  return m_upperbound;
}
/** @brief sets the lower bound of the parameter */
void Parameter::setParameterLowerBound(double lowerbound)
{
  m_lowerbound = lowerbound;
}
/** @brief sets the lower bound of the parameter */
void Parameter::setParameterUpperBound(double upperbound)
{
  m_upperbound = upperbound;
}
/**
* @brief get the current value of the parameter index
* @return current value of the parameter index, if -1 the parameter index has not been set yet
*/
int Parameter::getParameterIndex()const
{
  return m_parameterIndex;
}

/**
* @brief get the current value of the decision Variable index
* @return current value of the decisian variable index, if -1 the parameter is no decision variable
*/
int Parameter::getDecVarIndex()const
{
  return m_decVarIndex;
}

int Parameter::getEsoIndex() const
{
  return -1;
}

/**
* @brief sets flag whether the parameter is active
* @param activity activity flag
*/
void Parameter::setParameterSensActivity(const bool activity)
{
  m_sensActivity = activity;
}

/**
* @brief get flag whether the parameter is active
* @return 'true' if parameter is active, 'false' otherwise
*/
bool Parameter::getParameterSensActivity(void) const
{
  return m_sensActivity;
}

/**
* @brief set flag whether the parameter is a decision variable
*
* implicitly sets withSens to true if isDec is true
* @param isDec true if parameter is a decision variable
*/
void Parameter::setIsDecisionVariable(const bool isDec)
{
  m_isDecisionVariable = isDec;
  m_withSensitivity = m_withSensitivity || isDec;
}

/**
* @brief get flag whether the parameter is a decision variable
* @return true if parameter is a decision variable
*/
bool Parameter::getIsDecisionVariable(void) const
{
  return m_isDecisionVariable;
}

/**
* @brief sets boolean value telling if sensitivities are required for this particular parameter
* @param withSens 'true' if parameter has sensitivity information, 'false' otherwise
*/
void Parameter::setWithSensitivity(const bool withSens)
{
  m_withSensitivity = withSens;
}


/**
* @brief gets boolean value informing if sensitivities are required for this particular parameter
* @return 'true' if parameter has sensitivity information, 'false' otherwise
*/
bool Parameter::getWithSensitivity(void) const
{
  return m_withSensitivity;
}

/**
* @brief sets boolean value informing if parameter has been initialized
* @param[in] initialStatus new value for m_isInitialized
*/
void Parameter::setIsInitialized(bool initialStatus)
{
  m_isInitialized = initialStatus;
}

/**
* @brief gets boolean value informing if parameter has been initialized
* @return value of m_isInitialized
*/
bool Parameter::getIsInitialized() const
{
  return m_isInitialized;
}

void Parameter::setLagrangeMultiplier(const double multiplier)
{
  m_lagrangeMultiplier = multiplier;
}

double Parameter::getLagrangeMultiplier() const
{
  return m_lagrangeMultiplier;
}

Parameter::Ptr Parameter::getRelatedParameter() const
{
  assert(false);
  Parameter::Ptr empty;
  return empty;
}

/**
* @brief calculates f_x * s_p_i and adds df/dp_i if active
*
* @param[in] currentSens vector containing the current sensitivities s_p - must be of size jacobianFstates->m
* @param[in] jacobianFstates the jacobian values of the states
* @param[in] imd IntegratorMetaData providing duration and jacobianFup (needed for control parameter)
* @param[out] rhsSens array receiving the right hand side sensitivities
* @sa IntegratorSingleStageMetaData::getJacobianFstates
*/
void Parameter::getRhsSensForward1stOrder(const utils::Array<double> &currentSens,
                                          const cs* jacobianFstate,
                                                IntegratorMetaData *imd,
                                                utils::Array<double> &rhsSens)
{
  assert(getWithSensitivity() == true);
  getDfDxTimesY(currentSens, jacobianFstate, imd->getIntegrationGridDuration(), rhsSens);
  // now, rhsSens contains f_x * s_p_i

  if (m_sensActivity == true) {
    addDfDp(rhsSens, imd);
  }
}

/**
* @brief Calculates (f_x * y)
*
* In this function explicit format is expected
*
* @param[in] y vector containing either adjoints or s_p_i - must be of size jacobianFstates->m
* @param[in] jacobianFstates the jacobian values of the states
* @param[in] duration final time of the current integration interval
* @param[out] result array receiving the result of the calculation
* @sa IntegratorSingleStageMetaData::getJacobianFstates
*/
void Parameter::getDfDxTimesY(const utils::Array<double> &y,
                              const cs* jacobianFstates,
                              const double duration,
                              utils::Array<double> &result)
{
  assert(y.getSize() == result.getSize());

  const unsigned n = result.getSize();

  // initialize output
  std::fill_n(result.getData(), n, 0.0);
  //for (unsigned i=0; i<n; i++) result[i] = 0.0;

  // get Jacobian f_x
  // this is evaluated outside of the function
  // const struct cs *jacobianFstates = imd->getJacobianFstates();
  assert(jacobianFstates->m == jacobianFstates->n); // square (m x m) Matrix, with m = number of states
  assert((unsigned)jacobianFstates->m == result.getSize());

  // multiplication f_x * y
  //jacobianFstates is A and result is z(set to 0)
  const int retval = cs_gaxpy(jacobianFstates, y.getData(), result.getData()); // gaxpy: z = A*y + z
  assert(retval == 1);

  // scale with final time
  //transform function does the same as for loop
   /*for (unsigned i=0; i<n; i++){
    result[i] = duration * result[i];
  }*/
  std::transform(result.getData(),result.getData() + n,
                 result.getData(),
                 std::bind1st(std::multiplies<double>(),duration));
 
}

/**
* @brief get the derivatives of F wrt p 
* 
* This is achieved by setting paramDer to 0 and using addDfDp
* @param[out] paramDer on exit contains values of DfDp
* @param[out] imd IntegratorMetaData object containing this parameter
* @sa addDfDp
*/
void Parameter::getDfDp(std::vector<double> &paramDer, IntegratorMetaData *imd)
{
  unsigned numEquations = imd->getGenericEso()->getNumEquations();
  assert(numEquations != 0);
  paramDer.assign(numEquations, 0.0);
  utils::WrappingArray<double> y(numEquations, &paramDer[0]);
  addDfDp(y, imd);
}

/**
* @brief compares two parameters with respect to the parameter index - needed for sorting
*
* @param[in] p1 first parameter to be compared
* @param[in] p2 second parameter to be compared
* @return true, if p1 has lower parameter index than p2, false otherwise
*/
bool Parameter::compareParameter(Parameter::Ptr p1, Parameter::Ptr p2)
{
  return p1->getParameterIndex() < p2->getParameterIndex();
}

/**
* @brief calculate the adjoint derivative of this parameter
* @param[in] adjoints adjoint vector
* @param[in] imd IntegratorMetaData providing duration and jacobianFup (needed for control parameter)
* @return value of the adjoint derivative
* @note There is a more efficient vector-matrix product for calculation of lambda*df_du directly on GenericEso
*       use that if that has a significant speedup
*/
double Parameter::getRhsDerReverse1stOrder(const utils::Array<double> &adjoints,
                                                 IntegratorMetaData *imd)
{
  if(m_sensActivity){
    std::vector<double> dfDp;
    getDfDp(dfDp, imd);
    double rhsDer = 0.0;
    unsigned numStates = dfDp.size();
    assert(numStates == adjoints.getSize());
    for(unsigned i=0; i<numStates; i++){
      rhsDer += dfDp[i]*adjoints[i];
    }
    return rhsDer;
  }
  else{
    return 0.0;
  }
}


/**
* @brief creates parameter output
*
* @param[in] pointer to generic eso structure
* @param[in] IntOptDataMap
* @return Parameter Output structure
*/
DyosOutput::ParameterOutput Parameter::getOutput(GenericEso::Ptr eso,
                                                 IntOptDataMap iodm)
{
  DyosOutput::ParameterOutput output;
  output.value = getParameterValue();
  output.grids.resize(1);
  std::vector<unsigned> iv(1);
  int index = getParameterIndex();
  if(index >= 0){
    iv[0] = index;
    output.grids.front().firstSensitivityOutputVector = createSensitivityOutputVector(iodm, eso, iv);
  }
  return output;
}

/**
* @brief creates FirstSensitivityOutput vector
*
* @param[in] SavedOptDataStruct structure
* @param[in] pointer to generic eso class
* @param[in] vector of parameter indices
* @return FirstSensitivityOutput vector
*/
std::vector <DyosOutput::FirstSensitivityOutput>  Parameter::
                                                  createSensitivityOutputVector(IntOptDataMap iODP,
                                                  const GenericEso::Ptr &genPtr,
                                                  const std::vector <unsigned> &parameterIndices)
{
  std::vector <DyosOutput::FirstSensitivityOutput> output;
  IntOptDataMap::iterator iter;
  for(iter = iODP.begin(); iter != iODP.end(); iter++)
  {
    DyosOutput::FirstSensitivityOutput out;
    SavedOptDataStruct structSODS = (*iter).second;
    const unsigned numParams = structSODS.activeParameterIndices.size();
    const unsigned numGradientParts = structSODS.nonLinConstVarIndices.size();
    for(unsigned i=0; i<numGradientParts; i++)
    {
      int index = structSODS.nonLinConstVarIndices.at(i);
      std::string str = genPtr->getVariableNameOfIndex(index);
      std::pair <std::string, int> tv;
      tv.first = str;
      tv.second = i;
      out.mapConstrRows.insert(tv);
    }


    std::vector<unsigned> parameterPositions;
    parameterPositions.reserve(parameterIndices.size());
    for(unsigned j=0; j<parameterIndices.size(); j++)
    {
      std::vector<unsigned>::iterator elementPtr;
      elementPtr = std::find(structSODS.activeParameterIndices.begin(),
                             structSODS.activeParameterIndices.end(),
                             parameterIndices[j]);
      if(elementPtr != structSODS.activeParameterIndices.end())
      {
        const unsigned elementPos = elementPtr - structSODS.activeParameterIndices.begin();
        parameterPositions.push_back(elementPos);
        std::pair<int,int> paramColEntry;
        paramColEntry.first = parameterPositions.size() -1;
        paramColEntry.second = parameterIndices[j];
        out.mapParamsCols.insert(paramColEntry);
      }
    }

    out.values.resize(numGradientParts);
    std::vector<double>parameterValues(parameterPositions.size());
    // we dont have sensitivities when first order reverse is calculated
    if(structSODS.nonLinConstGradients.getSize() > 0){
      if(structSODS.nonLinConstGradients.getSize() != numParams*numGradientParts){
        throw MissingOutputException();
      }
      for(unsigned j=0; j<numGradientParts; j++)
      {
        //structSODS.nonLinConstGradients[j];
        const unsigned numExtracedParameters = parameterPositions.size();
        for(unsigned k=0; k<numExtracedParameters; k++){
          assert(structSODS.nonLinConstGradients.getSize() > j*numParams+parameterPositions[k]);
          parameterValues[k] = structSODS.nonLinConstGradients[j*numParams+parameterPositions[k]];
        }// for k
        out.values[j].assign(parameterValues.begin(), parameterValues.end());
      }// for j
      if(!out.mapConstrRows.empty()){
        // store only points with at least one set constraint
        //plot grid points do not have necessarily constraints set
        output.push_back(out);
      }
    }
  }

  return output;
}

// implementations of methods for derived classes
/**
* @brief standard constructor
*/
ControlParameter::ControlParameter()
{
  m_control = NULL;
}

ControlParameter::ControlParameter(const ControlParameter &toCopy) : Parameter(toCopy)
{
  m_control = toCopy.m_control;
}

/**
* @brief destructor
*/
ControlParameter::~ControlParameter()
{}

/**
* @brief set the corresponding control to this parameter
* @param[in] control pointer to the corresponding control
*/
void ControlParameter::setControl(Control *control)
{
  m_control = control;
}

/**
* @brief get the corresponding control to this parameter
* @return pointer to the corresponding control
*/
Control *ControlParameter::getControl() const
{
  return m_control;
}

/**
* @copydoc Parameter::getInitialValuesForIntegration
*
* For control parameter all initial sensitivity values are set to 0.0,
* except for states that are initialized in the initial section with the control
* of this parameter
*/
void ControlParameter::getInitialValuesForIntegration(utils::Array<double> &initialValues,
                                                      GenericEso::Ptr &genEso)const
{
  const unsigned n = initialValues.getSize();
  for (unsigned i=0; i<n; i++){
    initialValues[i] = 0.0;
  }
  

  const int numDiffNonZeroes = genEso->getNumDifferentialNonZeroes();
  utils::Array<EsoIndex> diffRows(numDiffNonZeroes), diffCols(numDiffNonZeroes);
  genEso->getDiffJacobianStruct(numDiffNonZeroes, diffRows.getData(), diffCols.getData());
  
  const int numInitialNonZeroes = genEso->getNumInitialNonZeroes();
  utils::Array<EsoIndex> initRows(numInitialNonZeroes), initCols(numInitialNonZeroes);
  utils::Array<EsoDouble> initValues(numInitialNonZeroes);
  genEso->getInitialJacobian(numInitialNonZeroes, initRows.getData(), initCols.getData(), initValues.getData());
  
  EsoIndex numStates = genEso->getNumStates();
  utils::Array<EsoIndex> stateIndices(numStates);
  genEso->getStateIndex(numStates, stateIndices.getData());
  
  int esoIndex = getEsoIndex();

  //iterate over all non constant values in the initial block
  for(int i=0; i<numInitialNonZeroes; i++){
	//is this the column of the current control in the initial Jacobian?
    if(esoIndex == initCols[i]){
      int diffStateIndex = -1;
      EsoIndex *position = 0;
      for(int j=0; j<numInitialNonZeroes; j++){
		//check all entries of the corresponding row
        if(initRows[i] == initRows[j]){
		  //is there a state in the same row as the control?
          position = std::find(stateIndices.getData(), stateIndices.getData() + numStates, initCols[j]);
          if(position != stateIndices.getData() + numStates){
            diffStateIndex = position - stateIndices.getData();
            break;
          }
        }
      }
      if(position == stateIndices.getData() + numStates){
        throw InitialSectionException("Initial equation had no differential state set - this case cannot be handled by Dyos.");
      }
	  //the current esoIndex
      initialValues[diffStateIndex] = - initValues[i];
    }
  }
}

/**
* @copydoc Parameter::addDfDp
*/
void ControlParameter::addDfDp(utils::Array<double> &y,
                               IntegratorMetaData *imd)
{
  // multiply column extracted from jacobianFup in previous step with dSpline and with duration;
  const double duration = imd->getIntegrationGridDuration();
  const double localTime = imd->getStepCurrentTime();
  int paramIndex = this->getParameterIndex();
  assert(paramIndex > -1);
  const double dSpline = m_control->getCurrentDSplineValue(localTime, paramIndex);
  if(dSpline == 0.0){
    return;
  }
  
  const unsigned esoIndex = m_control->getEsoIndex();
//  const struct cs *jacobianFup = imd->getJacobianFup();
  const CsCscMatrix::Ptr jacobianFup = imd->getJacobianFup();
  // extract one colum of jacobianFup which corresponds to current parameter
  // Remark: jacobianFup has same size as complete ESO Jacobian,
  // but only nonzeros in columns corresponding to parameters
  const unsigned numDerivatives = y.getSize();
  utils::Array<double> dfdp(numDerivatives,0.0);
  jacobianFup->extractColumn(esoIndex, dfdp.getData(), int(numDerivatives));

  for(unsigned i=0; i<numDerivatives; i++){
    y[i]+= dfdp[i]*duration * dSpline;
  }
}

/**
* @copydoc Parameter::getDuDpIndex
*/
int ControlParameter::getDuDpIndex() const
{
  int esoIndex = -3;
  if(m_sensActivity){
    esoIndex = (int)m_control->getEsoIndex();
  }
  return esoIndex;
}

int ControlParameter::getEsoIndex() const
{
  return m_control->getEsoIndex();
}

/**
* @copydoc Parameter::getDuDp
*/
double ControlParameter::getDuDp(const IntegratorMetaData *imd) const
{
  double dSpline = 0.0;
  if(m_sensActivity){
    int paramIndex = this->getParameterIndex();
    assert(paramIndex > -1);
    const double localTime = imd->getStepCurrentTime();
    dSpline = m_control->getCurrentDSplineValue(localTime, paramIndex);
  }
  return dSpline;
}

bool ControlParameter::isOriginal()
{
  return true;
}

/**
* @brief constructor
*/
InitialValueParameter::InitialValueParameter()
{
  m_bIsUserSet = false;
}

/**
* @brief copy constructor
*
* @param toCopy InitialValueParameter object to be copied
*/
InitialValueParameter::InitialValueParameter(const InitialValueParameter &toCopy) : Parameter(toCopy)
{
  m_esoIndex = toCopy.m_esoIndex;
  m_equationIndex = toCopy.m_equationIndex;
  m_bIsUserSet = toCopy.m_bIsUserSet;
  m_stateIndex = toCopy.m_stateIndex;
}

/**
* @brief destructor
*/
InitialValueParameter::~InitialValueParameter()
{}

/**
* @brief setter for the eso index of the corresponding initial value
* @param[in] esoIndex eso index of the initial value
*/
void InitialValueParameter::setEsoIndex(const unsigned esoIndex)
{
  m_esoIndex = esoIndex;
}


bool InitialValueParameter::isOriginal()
{
  return true;
}
/**
* @brief getter for the eso index of the corresponding initial value
* @return eso index of the initial value
*/
int InitialValueParameter::getEsoIndex() const
{
  return m_esoIndex;
}

/**
* @brief setter for the equation index of the corresponding initial value
* @param[in] equationIndex equation index of the initial value
*/
void InitialValueParameter::setEquationIndex(const unsigned equationIndex)
{
  m_equationIndex = equationIndex;
}

/**
* @brief getter for the equation index of the corresponding initial value
* @return equation index of the initial value
*/
unsigned InitialValueParameter::getEquationIndex() const
{
  return m_equationIndex;
}

void InitialValueParameter::setStateIndex(const unsigned stateIndex)
{
  m_stateIndex = stateIndex;
}

unsigned InitialValueParameter::getStateIndex() const
{
  return m_stateIndex;
}

/**
* @copydoc Parameter::getInitialValuesForIntegration
*
* the initialvalue on the diagonal (with the own state index) is set to 1.0,
* all other initial values are set to zero
* initialValues do not refer to equations but to states.
* For FMU: As output equation is explict y=f(x,p), the Jacobian at time 0 can 
* be used to determine the derivative of dy(0)/d(x_0). 
* Otherwise, the DAE must already be initialized.
*/
void InitialValueParameter::getInitialValuesForIntegration(utils::Array<double> &initialValues,
                                                           GenericEso::Ptr &genEso)const
{
  const unsigned n = initialValues.getSize();

  for (unsigned i=0; i<n; i++){
    initialValues[i] = 0.0;
  }
 initialValues[m_stateIndex] = 1.0;

#ifdef BUILD_WITH_FMU
// Only vaild if algebraic variables are output variables and have the form: 
 // y = g(x,p); 

 // Test: with some equation
 // y1 = 1.1*x1; 
 // y2 = 1.1*x1 + 1.2*x2; 
 // y3 = x1^2 + x2^3; 
 
 EsoIndex n_states = n;
 // only need derivative with respect to current initial state
 std::vector<EsoDouble> seedStates(n_states, 0.0);
 seedStates[m_stateIndex] = 1.0;

 EsoIndex n_params = genEso->getNumParameters();
 std::vector<EsoDouble> seedParameters(n_params, 0.0);
 
 std::vector<EsoDouble> jacValues(n_states);
 
 const EsoIndex numAlgVars = genEso->getNumAlgebraicVariables();
 utils::Array<EsoIndex> algVarIndices(numAlgVars);
 genEso->getAlgebraicIndex(numAlgVars, algVarIndices.getData());

 // works only with output equation is explicit. Otherwise, the actual initial equation system 
 GenericEso::RetFlag retFlag = genEso->getJacobianMultVector(false, n_states, seedStates.data(), n_params, seedParameters.data(), n_states, jacValues.data());
 
 // write derivative value for each output equation
 // negative value as Jacobian is computed for residual res = y - g(x,p)
 for (int i = 0; i < numAlgVars; ++i) {
	 initialValues[algVarIndices[i]] = -jacValues[algVarIndices[i]];
 }
#endif // BUILD_WITH_FMU
 
}

/**
* @copydoc Parameter::addDfDp
*/
void InitialValueParameter::addDfDp(utils::Array<double> &y,
                                     IntegratorMetaData *imd)
{
}

/**
* @copydoc Parameter::getDuDp
*
* DuDp index of this class is always -1
*/
int InitialValueParameter::getDuDpIndex() const
{
  return -1;
}

/**
* @copydoc Parameter::getDuDp
*/
double InitialValueParameter::getDuDp(const IntegratorMetaData *) const
{
  return 0.0;
}

DyosOutput::ParameterOutput InitialValueParameter::getOutput(GenericEso::Ptr eso,
                                                             IntOptDataMap iodm)
{
  DyosOutput::ParameterOutput out = Parameter::getOutput(eso, iodm);
  out.name = eso->getVariableNameOfIndex(getEsoIndex());
  out.esoIndex = getEsoIndex();
  return out;
}

/**
* @brief standard constructor
*/
DurationParameter::DurationParameter()
{
  m_lowerbound = DURATION_LOWERBOUND_TOLERANCE;
  m_durationMultiplier = 1.0;
}

/**
* @copydoc Parameter::setParameterLowerBound
*
* @note lowerbound must not be negative
*/
void DurationParameter::setParameterLowerBound(const double lowerbound)
{
  if(lowerbound >= DURATION_LOWERBOUND_TOLERANCE){
    Parameter::setParameterLowerBound(lowerbound);
  }
  else{
    m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY,
                          "warning: set lower bound for duration too small - set lowerbound to allowed minimum."
                          "probably user forgot to specify a lower bound for a duration");
    Parameter::setParameterLowerBound(DURATION_LOWERBOUND_TOLERANCE);
  }
}

/**
* @brief constructor setting a value
* @param[in] value value the parameter is initialized with
*/
DurationParameter::DurationParameter(const double value) : Parameter(value)
{
  m_lowerbound = DURATION_LOWERBOUND_TOLERANCE;
  m_durationMultiplier = 1.0;
}

/**
* @brief copy constructor
*
* @param toCopy DurationParameter object to be copied
*/
DurationParameter::DurationParameter(const DurationParameter &toCopy):Parameter(toCopy)
{
  this->m_durationMultiplier = toCopy.m_durationMultiplier;
}

/**
* @brief destructor
*/
DurationParameter::~DurationParameter()
{}

/**
* @copydoc Parameter::getInitialValuesForIntegration
*
* For final time parameter all initial values are set to 0.0.
*/
void DurationParameter::getInitialValuesForIntegration(utils::Array<double> &initialValues,
                                                       GenericEso::Ptr &genEso)const
{
  const unsigned n = initialValues.getSize();
  for (unsigned i=0; i<n; i++){
     initialValues[i] = 0.0;
  }
}


/**
* @copydoc Parameter::addDfDp
*/
void DurationParameter::addDfDp(utils::Array<double> &y,
                                  IntegratorMetaData *imd)
{
  const EsoIndex n = imd->getGenericEso()->getNumEquations();
  // get daeRhs
  // if the derivatives are set to zero,
  // daeRhs is simply the residual vector
  // derivatives should be already set to zero in 
  // IntegratorSingleStageMetaData::calculateSensitivitiesForwardFirstOrder
  // const unsigned numDers = imd->getGenericEso()->getNumDifferentialVariables();
  // utils::Array<double> derivatives(numDers, 0.0);
  // imd->getGenericEso()->setDerivativeValues(derivatives.getSize(), derivatives.getData());
  utils::Array<double> daeResiduals(n);
  imd->getGenericEso()->getAllResiduals(daeResiduals.getSize(), daeResiduals.getData());
  for(int i=0; i<n; i++){
    y[i] += daeResiduals[i];
  }

}

/**
* @copydoc Parameter::getDuDp
*
* DuDp index of this class is -2 if this parameter is active
* an inactive duration parameter behaves like an inactive control parameter
* in 2nd order integration - so in this case the index is -3 as well
*/
int DurationParameter::getDuDpIndex() const
{
  if(m_sensActivity){
    return -2;
  }
  else{
    return -3;
  }
}

/**
* @copydoc Parameter::getEsoIndex
*/
int DurationParameter::getEsoIndex() const
{
  return -2;
}

bool DurationParameter::isOriginal(){
  return true;
}

/**
* @copydoc Parameter::getDuDp
*/
double DurationParameter::getDuDp(const IntegratorMetaData *) const
{
  return 0.0;
}

void DurationParameter::switchDurationMultiplier()
{
  m_durationMultiplier = -m_durationMultiplier;
}

void DurationParameter::resetDurationMultiplier()
{
  m_durationMultiplier = 1.0;
}


SubstituteParameter::SubstituteParameter(const ControlParameter::Ptr &relatedParameter,
                                         const ControlParameter::Ptr &coParameter,
                                         const double timePoint)
{
  m_relatedParameter = relatedParameter;
  m_coParameter = coParameter;
  m_timePoint = timePoint;
  m_control = relatedParameter->getControl();
  m_isDecisionVariable = false;
  m_sensActivity = false;
  m_withSensitivity = false;
}

SubstituteParameter::SubstituteParameter(const SubstituteParameter &toCopy):ControlParameter(toCopy)
{
  this->m_relatedParameter = toCopy.m_relatedParameter;
  this->m_coParameter = toCopy.m_coParameter;
}


void SubstituteParameter::addDfDp(utils::Array<double> &y,
                                  IntegratorMetaData *imd)
{
  assert(false);
}

void SubstituteParameter::setParameterValue(const double value)
{
  assert(false);
  m_relatedParameter->setParameterValue(value);
}

/**
* @copydoc Parameter::getParameterValue
*
* If an interval is split at time point t and p1 and p2 are active parameters
* on the interval, the value of the new parameter is p1*(1-t)+p2*t.
* The formula here asserts, that p2 is the related parameter and p1 the co
* parameter. If it is the other way round, timePoint must be 1-t.
*/
double SubstituteParameter::getParameterValue(void) const
{
  double value =   m_relatedParameter->getParameterValue() * m_timePoint
                 + m_coParameter->getParameterValue() * (1 - m_timePoint);
  return value;
}

void SubstituteParameter::setParameterSensActivity(const bool activity)
{
  m_relatedParameter->setParameterSensActivity(activity);
}

bool SubstituteParameter::getParameterSensActivity(void) const
{
  return m_relatedParameter->getParameterSensActivity();
}

void SubstituteParameter::setWithSensitivity(const bool withSens)
{
  m_relatedParameter->setWithSensitivity(withSens);
}

bool SubstituteParameter::getWithSensitivity(void) const
{
  return m_relatedParameter->getWithSensitivity();
}

void SubstituteParameter::setIsDecisionVariable(const bool isDec)
{
  m_relatedParameter->setIsDecisionVariable(isDec);
}

bool SubstituteParameter::getIsDecisionVariable(void) const
{
  return m_relatedParameter->getIsDecisionVariable();
}

void SubstituteParameter::setParameterLowerBound(const double lowerbound)
{
  m_relatedParameter->setParameterLowerBound(lowerbound);
}

double SubstituteParameter::getParameterLowerBound(void) const
{
  return m_relatedParameter->getParameterLowerBound();
}

void SubstituteParameter::setParameterUpperBound(const double upperbound)
{
  m_relatedParameter->setParameterUpperBound(upperbound);
}

double SubstituteParameter::getParameterUpperBound(void) const
{
  return m_relatedParameter->getParameterUpperBound();
}

void SubstituteParameter::setParameterIndex(const int parameterIndex)
{
  m_relatedParameter->setParameterIndex(parameterIndex);
}

int SubstituteParameter::getParameterIndex() const
{
  return m_relatedParameter->getParameterIndex();
}

int SubstituteParameter::getEsoIndex() const
{
  return m_relatedParameter->getEsoIndex();
}

void SubstituteParameter::setIsInitialized(bool initialStatus)
{
  m_relatedParameter->setIsInitialized(initialStatus);
}

bool SubstituteParameter::getIsInitialized() const
{
  return m_relatedParameter->getIsInitialized();
}

void SubstituteParameter::getInitialValuesForIntegration(utils::Array<double> &initialValues,
                                                         GenericEso::Ptr &genEso)
{
  m_relatedParameter->getInitialValuesForIntegration(initialValues, genEso);
}

void SubstituteParameter::getRhsSensForward1stOrder(const utils::Array<double> &currentSens,
                                                    const cs* jacobianFstate,
                                                          IntegratorMetaData *imd,
                                                          utils::Array<double> &rhsSens)
{
  assert(false);
  m_relatedParameter->getRhsSensForward1stOrder(currentSens, jacobianFstate, imd, rhsSens);
}

double SubstituteParameter::getRhsDerReverse1stOrder(const utils::Array<double> &adjoints,
                                                           IntegratorMetaData *imd)
{
  assert(false);
  return m_relatedParameter->getRhsDerReverse1stOrder(adjoints, imd);
}

int SubstituteParameter::getDuDpIndex()
{
  assert(false);
  return m_relatedParameter->getDuDpIndex();
}

double SubstituteParameter::getDuDp(IntegratorMetaData *imd)
{
  assert(false);
  return m_relatedParameter->getDuDp(imd);
}

bool SubstituteParameter::isOriginal()
{
  return false;
}

DyosOutput::ParameterOutput SubstituteParameter::getOutput(GenericEso::Ptr eso, IntOptDataMap iodm)
{
  // todo : what is the output of the substitute parameter?
  return m_relatedParameter->getOutput(eso, iodm);
}

Parameter::Ptr SubstituteParameter::getRelatedParameter() const
{
  return m_relatedParameter;
}
