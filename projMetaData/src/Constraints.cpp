/** 
* @file Constraints.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaSingleStageEso - Part of DyOS                                    \n
* =====================================================================\n
* This file contains the definitions of the memberfunctions of classes \n
* StatePathConstraint and StatePointConstraint according to the UML    \n
* Diagramm METAESO_2                                                   \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 22.8.2011
*/

#include "Constraints.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include <cassert>

/**
* @brief standard constructor
*/
StatePointConstraint::StatePointConstraint() :
//  : m_equationIndex() , m_esoIndex() ,
  m_esoIndex(0), m_time(-1.0), m_lowerBound(0.0), m_upperBound(0.0), m_lagrangeMultiplier(0.0)
{
}

/**
* @brief copy constructor
*
* @param statePointConstraint object to be copied
*/
StatePointConstraint::StatePointConstraint(const StatePointConstraint &statePointConstraint)
{
  m_esoIndex = statePointConstraint.getEsoIndex();
  m_time = statePointConstraint.getTime();
  m_lowerBound = statePointConstraint.getLowerBound();
  m_upperBound = statePointConstraint.getUpperBound();
  m_lagrangeMultiplier = statePointConstraint.getLagrangeMultiplier();
}

/**
* @brief destructor
*/
StatePointConstraint::~StatePointConstraint()
{
}

/**
* @brief getter for attribute m_esoIndex
*
* @return value of m_esoIndex
*/
unsigned StatePointConstraint::getEsoIndex() const
{
  return m_esoIndex;
}

/**
* @brief setter for attribute m_esoIndex
*
* @param esoIndex new value for m_esoIndex
*/
void StatePointConstraint::setEsoIndex(const unsigned esoIndex)
{
  m_esoIndex = esoIndex;
}


/**
* @brief getter for attribute m_time
*
* @return value of m_time
*/
double StatePointConstraint::getTime() const
{
  return m_time;
}

/**
* @brief setter for attribute m_time
*
* @param time new value for m_time (0.0 <= time <= 1.0)
*/
void StatePointConstraint::setTime(const double time)
{
  assert(time>=0.0 && time<=1.0);
  m_time = time;
}

/**
* @brief getter for attribute m_lowerBound
*
* @return value of m_lowerBound
*/
double StatePointConstraint::getLowerBound() const
{
  return m_lowerBound;
}

/**
* @brief setter for attribute m_lowerBound
*
* @param lowerBound new value for m_lowerBound
*/
void StatePointConstraint::setLowerBound(const double lowerBound)
{
  m_lowerBound = lowerBound;
}

/**
* @brief getter for attribute m_upperBound
*
* @return value of m_upperBound
*/
double StatePointConstraint::getUpperBound() const
{
  return m_upperBound;
}

/**
* @brief setter for attribute m_upperBound
*
* @param upperBound new value for m_upperBound
*/
void StatePointConstraint::setUpperBound(const double upperBound)
{
  m_upperBound = upperBound;
}

/**
* @brief getter for attribute m_lagrangeMultiplier
*
* @return value of m_lagragneMultiplier
*/
double StatePointConstraint::getLagrangeMultiplier() const 
{
  return m_lagrangeMultiplier;
}

/**
* @brief setter for attribute m_lagrangeMultiplier
*
* @param lagrangeMultiplier new value for m_lagrangeMultiplier
*/
void StatePointConstraint::setLagrangeMultiplier(const double lagrangeMultiplier)
{
  m_lagrangeMultiplier = lagrangeMultiplier;
}

/**
* @brief setter for attribute m_timeIndex
*
* @param timeIndex new value for m_timeIndex
*/
void StatePointConstraint::setTimeIndex(const unsigned timeIndex)
{
  m_timeIndex = timeIndex;
}

/**
* @brief getter for attribute m_timeIndex
*
* @return value of m_timeIndex
*/
unsigned StatePointConstraint::getTimeIndex() const
{
  return m_timeIndex;
}

/**
* @brief converts ConstraintInput structure to comparable output
*
* @param[in] pointer to the ConstraintInput structure
* @return the constructed ConstraintOutput structure
*/
DyosOutput::ConstraintOutput  StatePointConstraint::getOutput(const GenericEso::Ptr &eso)
{
  DyosOutput::ConstraintOutput output;
  output.lagrangeMultiplier = m_lagrangeMultiplier;
  output.name = eso->getVariableNameOfIndex(m_esoIndex);
  output.esoIndex = m_esoIndex;
  output.timePoint = m_time;
  output.lowerBound = m_lowerBound;
  output.upperBound = m_upperBound;
  return output;
}

/**
* @brief constructor
* 
* multiple shooting constraints have bounds of [0.0, 0.0] and are seen as endpoint constraints
* of the previous stage (taking the initial value parameter of the next stage into account)
*/
MultipleShootingConstraint::MultipleShootingConstraint()
{
  setLowerBound(0.0);
  setUpperBound(0.0);
  setTime(1.0);
}

/**
* @brief set the initial valua parameter
* @param initValPar InitialValueParameter that corresponds with this constraint
*/
void MultipleShootingConstraint::setInitialValueParameterNextStage
                                    (InitialValueParameter::Ptr initValPar)
{
  m_initValParNextStage = initValPar;
}

/**
* @brief set the stage index
* 
* The multiple shooting constraint is interpreted as an endpoint constraint of
* stage n and in addition using an InitialValueParameter of stage n+1. stageIndex n
* is set by this function
* @param stageIndex stage index to be set
*/
void MultipleShootingConstraint::setStageIndex(unsigned int stageIndex)
{
  m_stageIndex = stageIndex;
}

/**
* @copydoc GlobalConstraint::evaluateConstraint
*
* constraint evaluation is here the difference of the value of the corresponding 
* InitialValueParameter on stage n+1 and the endpoint value of the variable on stage n
*/
double MultipleShootingConstraint::evaluateConstraint
                        (const std::vector<OptimizerSingleStageMetaData::Ptr> &stages)
{
  double constraintValue = m_initValParNextStage->getParameterValue();
  
  OptimizerSingleStageMetaData::Ptr thisStage = stages[m_stageIndex];
  IntOptDataMap stageIntOptDataMap = thisStage->getIntegrationOptData().front();
  SavedOptDataStruct lastPoint = stageIntOptDataMap[getTimeIndex()];
  std::vector<int>::iterator found = std::find(lastPoint.nonLinConstVarIndices.begin(),
                                               lastPoint.nonLinConstVarIndices.end(),
                                               getEsoIndex());
  assert(found != lastPoint.nonLinConstVarIndices.end());
  unsigned position = found - lastPoint.nonLinConstVarIndices.begin();
  constraintValue -= lastPoint.nonLinConstValues[position];
  return constraintValue;
  
}

/**
* @copydoc GlobalConstraint::addConstraint
*
* on stage n an endpoint constraint is added (which is not evaluated on the stage)
*/
void MultipleShootingConstraint::addConstraint
                            (std::vector<OptimizerSingleStageMetaData::Ptr> &optStages,
                             const unsigned constraintIndex)
{
  optStages[m_stageIndex]->addConstraint(*this, constraintIndex);
  //store global parameter index of initial value parameter
  m_numParametersPreviousStages = 0;
  for(unsigned i=0; i<m_stageIndex + 1; i++){
    m_numParametersPreviousStages += optStages[i]->getOptProbDim().numOptVars;
    m_numParametersPreviousStages += optStages[i]->getOptProbDim().numConstParams;
  }
}

/**
* @copydoc GlobalConstraint::evaluateConstraintSens
*
* take the sensitivities of the corresponding endpoint on stage n (similar to StatePointConstraints)
* then add an entry for the InitialValueParameter of stage n+1 (value of 1.0)
*/
void MultipleShootingConstraint::evaluateConstraintSens(
                              const std::vector<OptimizerSingleStageMetaData::Ptr> &optStages,
                                    CsTripletMatrix::Ptr derivativeMatrix,
                                    std::vector<unsigned> varIndices,
                              const unsigned constraintIndex)
{
  //extract sensitivity data from previous stage
  IntOptDataMap stageMap = optStages[m_stageIndex]->getIntegrationOptData().front();
  SavedOptDataStruct endpoint = stageMap[m_timeIndex];
  //add offset for objectives for each stage
  unsigned storedConstraintIndex = constraintIndex + optStages.size();
  unsigned constraintPosition = std::find(endpoint.nonLinConstIndices.begin(),
                                          endpoint.nonLinConstIndices.end(),
                                          storedConstraintIndex)
                                - endpoint.nonLinConstIndices.begin();
  assert(constraintPosition < endpoint.nonLinConstIndices.size());
  assert(int(constraintIndex) < derivativeMatrix->get_n_rows());
  unsigned numStageParameters = endpoint.numSensParam;
  // we extract now the gradients with respect to decision variables
  std::vector<unsigned>::iterator varIndexPtr;
  for(unsigned j=0; j<numStageParameters; j++){
    // store only sensitivities of given variable indices (either decision Variables
    // or constant parameters)
    unsigned parameterOffset = m_numParametersPreviousStages
                             - optStages[m_stageIndex]->getOptProbDim().numConstParams
                             - optStages[m_stageIndex]->getOptProbDim().numOptVars;
    varIndexPtr = std::find(varIndices.begin(),
                            varIndices.end(),
                            endpoint.activeParameterIndices[j] + parameterOffset);
    if(varIndexPtr != varIndices.end()){
      // the parameterIndex is the column position of the parameter in the sensitivity matrix
      const unsigned parameterIndex = varIndexPtr - varIndices.begin();
      assert(int(parameterIndex) < derivativeMatrix->get_n_cols());
      // gradientIndex is the position of the the gradient in the intOptData struct
      // e.g. they are sorted as: x1p1, x1p2, x1p3, x2p1, x2p2, x2p3
      // x1p1 means: the sensitivity of state x1 with respect to parameter p1
      const unsigned gradientIndex = constraintPosition* numStageParameters + j;
      assert(gradientIndex < endpoint.nonLinConstGradients.getSize());
      const double gradientValue = -endpoint.nonLinConstGradients[gradientIndex];
      // we have zero data in the entire cs matrix. So we only override the value.
      derivativeMatrix->setValue(constraintIndex, parameterIndex, gradientValue);
    }//if
  }//for
  
  //add sensitivity data for initial value parameter of next stage
  // (only if it can be found in current parameter vector)
  varIndexPtr = std::find(varIndices.begin(),
                          varIndices.end(),
                          m_numParametersPreviousStages
                        + m_initValParNextStage->getParameterIndex());
  if(varIndexPtr != varIndices.end()){
    const unsigned parameterIndex = varIndexPtr - varIndices.begin();
    derivativeMatrix->setValue(constraintIndex, parameterIndex, 1.0);
  }
}

/**
* @copydoc GlobalConstraint::getJacobianDecVarColIndices
*
* return global indices of all decision variables of stage n and the global
* parameter index of the InitialValueParameter of stage n+1
*/
std::vector<unsigned> MultipleShootingConstraint::getJacobianDecVarColIndices
                          (const std::vector<OptimizerSingleStageMetaData::Ptr> &optStages)
{
  unsigned numDecVarsStage = optStages[m_stageIndex]->getOptProbDim().numOptVars;
  std::vector<unsigned> jacColIndices(numDecVarsStage);

  unsigned parameterOffset = 0;
  for(unsigned i=0; i<m_stageIndex; i++){
    parameterOffset += optStages[i]->getOptProbDim().numOptVars;
  } 
  for(unsigned i=0; i<jacColIndices.size(); i++){
    jacColIndices[i] = i + parameterOffset;
  }
  parameterOffset += optStages[m_stageIndex]->getOptProbDim().numOptVars;
  
  std::vector<unsigned> decVarIndNextStage;
  optStages[m_stageIndex+1]->getDecisionVariableIndices(decVarIndNextStage);
  bool found = false;
  for(unsigned i=0; i<decVarIndNextStage.size(); i++){
    if(int(decVarIndNextStage[i]) == m_initValParNextStage->getParameterIndex()){
      jacColIndices.push_back(i+parameterOffset);
      found = true;
      break;
    }
  }
  assert(found);
  return jacColIndices;
}

/**
* @copydoc GlobalConstraint::getJacobianConstParamColIndices
* 
* return global indices of all constant parameters of stage n
*/
std::vector<unsigned> MultipleShootingConstraint::getJacobianConstParamColIndices
                          (const std::vector<OptimizerSingleStageMetaData::Ptr> &optStages)
{
  unsigned numConstParamsStage = optStages[m_stageIndex]->getOptProbDim().numConstParams;
  std::vector<unsigned> jacColIndices(numConstParamsStage);

  unsigned parameterOffset = 0;
  for(unsigned i=0; i<m_stageIndex; i++){
    parameterOffset += optStages[i]->getOptProbDim().numConstParams;
  } 
  for(unsigned i=0; i<jacColIndices.size(); i++){
    jacColIndices[i] = i + parameterOffset;
  }
  return jacColIndices;
}
