/**
* @file OptimizerSingleStageMetaData.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the OptimizerSingleStageMetaData class
* according to MetaEso8.vsd                                            \n
* =====================================================================\n
* @author Tjalf Hoffmann, Fady Assassa, Kathrin Frankl
* @date 13.09.2012
*/

#include <cassert>
#include <vector>
#include <algorithm>
#include <cfloat>
#include "OptimizerSingleStageMetaData.hpp"
#include "Parameter.hpp"

/**
* @brief standard constructor
*/
OptimizerSingleStageMetaData::OptimizerSingleStageMetaData()
{
}

/**
* @brief constructor setting the integratorMetaData
*
* @param integratorMetaData object which is used by this class
*/
OptimizerSingleStageMetaData::OptimizerSingleStageMetaData(const IntegratorSingleStageMetaData::Ptr &integratorMetaData)
{
  assert(integratorMetaData.get() != NULL);
  m_integratorMetaData = integratorMetaData;
}

/**
* @brief allocate a matrix in coo format and provide it with a full structure
*
* @param matrix pointer receiving the created matrix
* @param numRows number of rows of matrix
* @param numCols number of cols of matrix
*/
void OptimizerSingleStageMetaData::constructFullCooMatrix(CsTripletMatrix::Ptr &matrix,
                                                         const unsigned numRows,
                                                         const unsigned numCols)
{
  const unsigned numNonZeroes = numRows * numCols;
  int * rowIndices = new int[numNonZeroes];
  int * colIndices = new int[numNonZeroes];
  const double * valuesSpace = new double[numNonZeroes];

  // provide full structure
  for(unsigned i=0; i<numRows; i++){
    for(unsigned j=0; j<numCols; j++){
       const unsigned currentIndex = i*numCols + j;
       rowIndices[currentIndex] = i;
       colIndices[currentIndex] = j;
    }
  }
  matrix = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(numRows,
                                                          numCols,
                                                          numNonZeroes,
                                                          rowIndices,
                                                          colIndices,
                                                          valuesSpace));
  delete []rowIndices;
  delete []colIndices;
  delete []valuesSpace;
}


/**
* @brief allocate a matrix in coo format and provide it with a sparse structure
*
* @param matrix pointer receiving the created matrix
* @param rowIndices row indices of the matrix
* @param colIndices column indices of the matirx
* @param numRows number of rows of matrix
* @param numCols number of cols of matrix
*/
void OptimizerSingleStageMetaData::constructSparseCooMatrix(CsTripletMatrix::Ptr &matrix,
                                                            const std::vector<int> rowIndices,
                                                            const std::vector<int> colIndices,
                                                            const unsigned numRows,
                                                            const unsigned numCols)
{
  assert(rowIndices.size() == colIndices.size());

  const unsigned numNonZeroes = rowIndices.size();
  utils::Array<double> valuesSpace(numNonZeroes, 0.0);
  if(!rowIndices.empty()){
    matrix = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(numRows,
                                                            numCols,
                                                            numNonZeroes,
                                                            &rowIndices[0],
                                                            &colIndices[0],
                                                            valuesSpace.getData()));
  }
  else{
    // if indices are empty, use reference to a dummyNumber 
    // numNonZeroes is zero, so value is not read at all
    const int dummyNumber = 0;
    matrix = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(numRows,
                                                            numCols,
                                                            numNonZeroes,
                                                            &dummyNumber,
                                                            &dummyNumber,
                                                            valuesSpace.getData()));
  }
  
}


/**
* @brief calculate the structure of the optimization Jacobian
*
* this method calculates the true sparsity structure.
*
* @param intOptData IntOptDataMap required to determine the sparsity structure
*/
void OptimizerSingleStageMetaData::calculateNonLinJacDecVarStructure(IntOptDataMap &intOptData)
{
  std::vector<unsigned> varIndices;
  getDecisionVariableIndices(varIndices);
  unsigned objectiveIndex = getObjectiveData().getEsoIndex();

  IntegratorSingleStageMetaData::Ptr integratorMetaData = m_integratorMetaData;
  
  std::vector<int> constraintIndexVec, parameterIndexVec; 
  //determine fieldsizes
  IntOptDataMap::iterator timeIt;
  unsigned maxVecSize = 0;
  for(timeIt = intOptData.begin() ; timeIt != intOptData.end(); timeIt++ ){
    const unsigned numConstraints = (*timeIt).second.nonLinConstIndices.size();
    const unsigned numParameters = (*timeIt).second.numSensParam;
    maxVecSize += numConstraints*numParameters;
  }
  constraintIndexVec.reserve(maxVecSize);
  parameterIndexVec.reserve(maxVecSize);
  
  // iterate trough time points
  for(timeIt = intOptData.begin() ; timeIt != intOptData.end(); timeIt++ ){
    // extract int opt data for the current time point
    SavedOptDataStruct optStruct = (*timeIt).second;
    const unsigned numConstraints = optStruct.nonLinConstIndices.size();
    const unsigned numParameters = optStruct.numSensParam;
    // now we iterate through all constraints and skip the objective
    for(unsigned i=0; i<numConstraints; i++){
      // remove objective gradient from constraint gradients
      if ((unsigned)optStruct.nonLinConstVarIndices[i] != objectiveIndex){
        // the constraint index was assigend by the user,
        // which is the row index of the sensitivity matrix
        const unsigned constraintIndex = optStruct.nonLinConstIndices[i];
        //assert(int(constraintIndex) < nonLinConstGradients->get_n_rows() + 1);//+1 for the objective // currently does not work
        // for constraint i, we extract now the gradients with respect to decision variables
        for(unsigned j=0; j<numParameters; j++){
          if(optStruct.activeDecVarIndices[j] >= 0){
            // the parameterIndex is the column position of the parameter in the sensitivity matrix
            const unsigned parameterIndex = optStruct.activeDecVarIndices[j];
            //assert(int(parameterIndex) < nonLinConstGradients->get_n_cols()); // currently does not work
            constraintIndexVec.push_back(constraintIndex);
            parameterIndexVec.push_back(parameterIndex);
          }
        }
      }
    }
  }

  // now construct the matrix m_optProbDim.nonLinJacDecVar using constraintIndexVec (row indices) and 
  // parameterIndexVec (column indices)
  const unsigned numRows = m_optProbDim.numNonLinConstraints;
  const unsigned numCols = m_optProbDim.numOptVars;
 
  constructSparseCooMatrix(m_optProbDim.nonLinJacDecVar, constraintIndexVec, parameterIndexVec, numRows, numCols);
}


void OptimizerSingleStageMetaData::calculateNonLinJacConstParStructure()
{
  const unsigned numRows = m_optProbDim.numNonLinConstraints;
  const unsigned numCols = m_optProbDim.numConstParams;
  constructFullCooMatrix(m_optProbDim.nonLinJacConstPar, numRows, numCols);
}

void OptimizerSingleStageMetaData::getDecisionVariableIndices(std::vector<unsigned> &decVarInd) const
{
  const unsigned numDecVars = m_integratorMetaData->getNumDecisionVariables();

  decVarInd.resize(numDecVars);

  std::vector<Parameter::Ptr> decisionVariables(numDecVars);
  m_integratorMetaData->getDecisionVariables(decisionVariables);

  assert(decisionVariables.size() == numDecVars);
  for(unsigned int i=0; i<numDecVars; i++){
    decVarInd[i] = decisionVariables[i]->getParameterIndex();
  }
}

void OptimizerSingleStageMetaData::getConstantParameterIndices
                                      (std::vector<unsigned> &constParamInd) const
{
  const unsigned numConstParams = m_optProbDim.numConstParams;
  constParamInd.clear();
  constParamInd.reserve(numConstParams);

  std::vector<unsigned> decVarInd;
  getDecisionVariableIndices(decVarInd);

  std::vector<Parameter::Ptr> sensParams;
  m_integratorMetaData->getSensitivityParameters(sensParams);

  assert(sensParams.size() == numConstParams + decVarInd.size());

  for(unsigned i=0; i<sensParams.size(); i++){
    // add if parameter is no decision variable
    if(sensParams[i]->getIsDecisionVariable() == false){
      constParamInd.push_back(sensParams[i]->getParameterIndex());
    }
  }
}

/**
* @brief set up the optimizer problem dimensions
*
* this should be the last function called by the factory since this function needs
* all other attributes to be set beforehand
*/
void OptimizerSingleStageMetaData::calculateOptimizerProblemDimensions()
{
  // There are linear constraints in the single stage. However, they have not been implemented.
  // They occur for the final times of the parameterization gird.
  m_optProbDim.numLinConstraints = 0;

  // number of all sensitivity parameters
  m_optProbDim.numOptVars = m_integratorMetaData->getNumDecisionVariables();

  const unsigned numSensParams= m_integratorMetaData->getNumSensitivityParameters();
  assert(numSensParams >= m_optProbDim.numOptVars);
  m_optProbDim.numConstParams = numSensParams - m_optProbDim.numOptVars;

  m_optProbDim.numNonLinConstraints = m_constraints.size();

  m_optProbDim.nonLinObjIndex = m_objective.getEsoIndex();

  // retrieve data we need to calculate the sparsity structure of the Jacobian
  // of the nonlinear constraints and put it into intOptData (formerly this data had
  // been collected only during a model integration, though - as structural data - it 
  // is independent of the integration).
  // Remark: method name is a bit misleading: not all attributes in intOptData are 
  // set, only the ones we need for the computation of the sparsity structure
  m_integratorMetaData->setIntOptData();
  IntOptDataMap intOptData = m_integratorMetaData->getIntOptData().front();

  calculateNonLinJacDecVarStructure(intOptData);
  
  calculateNonLinJacConstParStructure();

  calculateLinJacStructure();

  m_linConLagrangeMultiplier.resize(m_optProbDim.numLinConstraints, 0.0);
}

/**
* @copydoc OptimizerMetaData::setLagrangeMultipliers
*/
void OptimizerSingleStageMetaData::setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                                          const utils::Array<double> &linMultipliers)
{
  assert(m_optProbDim.numNonLinConstraints == m_constraints.size());
  assert(m_constraints.size() == nonLinMultipliers.getSize());
  assert(m_optProbDim.numLinConstraints == linMultipliers.getSize());
  for(unsigned i=0; i<m_optProbDim.numNonLinConstraints; i++){
    m_constraints[i].setLagrangeMultiplier(nonLinMultipliers[i]);
  }
  m_linConLagrangeMultiplier.assign(linMultipliers.getData(),
                                    linMultipliers.getData() + linMultipliers.getSize());
  unsigned numNonLinMult = nonLinMultipliers.getSize();
  utils::Array<double> nonLinAndObjectiveMultipliers(numNonLinMult+1);
  utils::copy(numNonLinMult, nonLinMultipliers.getData(), nonLinAndObjectiveMultipliers.getData());
  nonLinAndObjectiveMultipliers[numNonLinMult] = m_objective.getLagrangeMultiplier();
  m_integratorMetaData->setLagrangeMultipliers(nonLinAndObjectiveMultipliers);
}

/**
* @copydoc OptimizerMetaData::getLagrangeMultipliers
*/
void OptimizerSingleStageMetaData::getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                                          utils::Array<double> &linMultipliers) const
{
  assert(nonLinMultipliers.getSize() == m_optProbDim.numNonLinConstraints);
  for(unsigned i=0; i<m_optProbDim.numNonLinConstraints ; i++){
    nonLinMultipliers[i] = m_constraints[i].getLagrangeMultiplier();
  }
  assert(linMultipliers.getSize() == m_optProbDim.numLinConstraints);
  assert(m_linConLagrangeMultiplier.size() == m_optProbDim.numLinConstraints);
  for(unsigned i=0; i<m_optProbDim.numLinConstraints; i++){
    linMultipliers[i] = m_linConLagrangeMultiplier[i];
  }
  //utils::copy(m_optProbDim.numLinConstraints, &m_linConLagrangeMultiplier[0], linMultipliers.getData());
}

/**
* @copydoc OptimizerMetaData::setDecVarLagrangeMultipliers
*/
void OptimizerSingleStageMetaData::setDecVarLagrangeMultipliers(
                                    const utils::Array<double> &lagrangeMultiplier)
{
  std::vector<Parameter::Ptr> decisionVariables;
  m_integratorMetaData->getDecisionVariables(decisionVariables);
  assert(decisionVariables.size() == lagrangeMultiplier.getSize());
  for(unsigned i=0; i<lagrangeMultiplier.getSize(); i++){
    decisionVariables[i]->setLagrangeMultiplier(lagrangeMultiplier[i]);
  }
}

/**
* @copydoc OptimizerMetaData::setObjectiveLagrangeMultiplier
*/
void OptimizerSingleStageMetaData::setObjectiveLagrangeMultiplier(const double lagrangeMultiplier)
{
  m_objective.setLagrangeMultiplier(lagrangeMultiplier);
  
  unsigned numNonLinMult = m_optProbDim.numNonLinConstraints;
  unsigned numLinMult = m_optProbDim.numLinConstraints;
  utils::Array<double> nonLinAndObjectiveMultipliers(numNonLinMult+1);
  utils::WrappingArray<double> nonLinMultipliers(numNonLinMult, nonLinAndObjectiveMultipliers.getData());
  utils::Array<double> linMultipliers(numLinMult);
  getLagrangeMultipliers(nonLinMultipliers, linMultipliers);
  nonLinAndObjectiveMultipliers[numNonLinMult] = lagrangeMultiplier;
  m_integratorMetaData->setLagrangeMultipliers(nonLinAndObjectiveMultipliers);
}

/**
* @brief setter for attribute m_constraints
*
* @param[in] constraints new value for m_constraints
*/
void OptimizerSingleStageMetaData::setConstraints(const std::vector<StatePointConstraint> &constraints)
{
  m_constraints.assign(constraints.begin(), constraints.end());
  // add all set constraints
  for(unsigned i=0; i<m_constraints.size(); i++){
    addConstraint(m_constraints[i], i);
  }
}

void OptimizerSingleStageMetaData::addConstraint(StatePointConstraint &constraint,
                                                 const unsigned constraintIndex)
{
  const unsigned timeIndex = m_integratorMetaData->addNonlinearConstraint(
                                                 constraint.getEsoIndex(),
                                                 constraintIndex,
                                                 constraint.getTime(),
                                                 constraint.getLagrangeMultiplier());
  constraint.setTimeIndex(timeIndex);
}

/**
* @brief setter for attribute m_objective
*
* @param[in] objective new value for m_objective
*/
void OptimizerSingleStageMetaData::setObjective(const StatePointConstraint &objective)
{
  m_objective = objective;
  const unsigned timeIndex = m_integratorMetaData->addNonlinearConstraint(
                                                 m_objective.getEsoIndex(),
                                                 m_constraints.size(),
                                                 1.0,
                                                 m_objective.getLagrangeMultiplier());
  m_objective.setTimeIndex(timeIndex);
}

/**
* @copydoc OptimizerMetaData::getDecVarBounds
*/
void OptimizerSingleStageMetaData::getDecVarBounds(utils::Array<double> &lowerBounds,
                                                   utils::Array<double> &upperBounds) const
{
  std::vector<Parameter::Ptr> decisionVariables;
  m_integratorMetaData->getDecisionVariables(decisionVariables);

  const unsigned numParams = decisionVariables.size();
  assert(upperBounds.getSize() == numParams);
  assert(lowerBounds.getSize() == numParams);
  for(unsigned i=0; i<numParams; i++){
    lowerBounds[i] = decisionVariables[i]->getParameterLowerBound();
    upperBounds[i] = decisionVariables[i]->getParameterUpperBound();
  }
}


/**
* @copydoc OptimizerMetaData::getDecVarValues
*/
void OptimizerSingleStageMetaData::getDecVarValues(utils::Array<double> &values) const
{
  std::vector<Parameter::Ptr> decisionVariables;
  m_integratorMetaData->getDecisionVariables(decisionVariables);

  const unsigned numParams = decisionVariables.size();
  assert(values.getSize() == numParams);
  for(unsigned i=0; i<numParams; i++){
      values[i] = decisionVariables[i]->getParameterValue();
  }
}


/**
* @copydoc OptimizerMetaData::setDecVarValues
*/
void OptimizerSingleStageMetaData::setDecVarValues(const utils::Array<const double> &decVars)
{
  std::vector<Parameter::Ptr> decisionVariables;
  m_integratorMetaData->getDecisionVariables(decisionVariables);

  const unsigned numParams = decisionVariables.size();
  assert(decVars.getSize() == numParams);
  for(unsigned i=0; i<numParams; i++){
    decisionVariables[i]->setParameterValue(decVars[i]);
  }
  m_integratorMetaData->resetGolbalDurationParameter();
}

/**
* @copydoc OptimizerMetaData::getNonLinConstraintBounds
*/
void OptimizerSingleStageMetaData::getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                                             utils::Array<double> &upperBounds) const
{
  const unsigned numIPC = m_constraints.size();
  assert(lowerBounds.getSize() == numIPC);
  assert(upperBounds.getSize() == numIPC);
  for(unsigned i=0; i<numIPC; i++){
    lowerBounds[i] = m_constraints[i].getLowerBound();
    upperBounds[i] = m_constraints[i].getUpperBound();
  }
}

/**
* @copydoc OptimizerMetaData::getNonLinConstraintValues
*/
void OptimizerSingleStageMetaData::getNonLinConstraintValues(utils::Array<double> &values) const
{
  const unsigned numConstraints = m_constraints.size();
  assert(values.getSize() == numConstraints);

  IntOptDataMap::iterator timeIt;
  std::vector<IntOptDataMap> intOptData = m_integratorMetaData->getIntOptData();
  // iterate trough time points
  for(timeIt = intOptData[0].begin() ; timeIt != intOptData[0].end(); timeIt++ ) {
    // iterate through eso indices
    const unsigned numConst = (*timeIt).second.nonLinConstVarIndices.size();
    for(unsigned i = 0; i < numConst; i++){
      // insert value in the vector except the one for the objective
      if ((unsigned)(*timeIt).second.nonLinConstVarIndices[i] != m_objective.getEsoIndex()) {
        unsigned constrIndex = (*timeIt).second.nonLinConstIndices[i];
        // global constraints are evaluated on multi stage level - so skip these
        if(constrIndex < values.getSize()){
         values[constrIndex] = (*timeIt).second.nonLinConstValues[i];
        }
      }
    }
  }
}

/**
* @copydoc OptimizerMetaData::getNonLinObjValue
*/
double OptimizerSingleStageMetaData::getNonLinObjValue() const
{
  double objValue = 0.0;
  std::vector<IntOptDataMap> intOptData = m_integratorMetaData->getIntOptData();

  const unsigned timeIndex = m_objective.getTimeIndex();
  assert(intOptData[0].count(timeIndex) > 0);
  const unsigned numConst = intOptData[0][timeIndex].nonLinConstVarIndices.size();
  for(unsigned i = 0; i < numConst; i++){
    // insert value only the value of the objective
    if ((unsigned)intOptData[0][timeIndex].nonLinConstVarIndices[i] == m_objective.getEsoIndex())
    {
      objValue = intOptData[0][timeIndex].nonLinConstValues[i];
      return objValue; // so we avoid going through the entire loop if we find the index early
    }
  }
  // we should never get here. This basically means that we didn't find an objective index.
  assert(1 == 0);

  return objValue;
}
void OptimizerSingleStageMetaData::retrieveNonLinConstGradients(CsTripletMatrix::Ptr nonLinConstGradients,
                                                                std::vector<unsigned> &varIndices,
                                                                IntOptDataMap &intOptData,
                                                                const unsigned objectiveIndex)
{
  
  //calculateNonLinJacDecVarStructure(varIndices, intOptData, objectiveIndex);
  
  
  for(int i=0; i<nonLinConstGradients->get_matrix_ptr()->nz; i++){
    nonLinConstGradients->get_matrix_ptr()->x[i] = 0.0;
  }

  std::vector<unsigned> constraintIndexVec, parameterIndexVec; 
  std::vector<unsigned> gradientIndexVec;
  std::vector<double> gradientValueVec;
  

  // iterate trough time points
  IntOptDataMap::iterator timeIt;
  for(timeIt = intOptData.begin() ; timeIt != intOptData.end(); timeIt++ ){
    // extract int opt data for the current time point
    SavedOptDataStruct optStruct = (*timeIt).second;
    const unsigned numConstraints = optStruct.nonLinConstIndices.size();
    const unsigned numParameters = optStruct.numSensParam;
    const unsigned numGrad = optStruct.nonLinConstGradients.getSize();
    // now we iterate through all constraints and skip the objective
    for(unsigned i=0; i<numConstraints; i++){
      // remove objective gradient from constraint gradients
      if ((unsigned)optStruct.nonLinConstVarIndices[i] != objectiveIndex){
        // the constraint index was assigend by the user,
        // which is the row index of the sensitivity matrix
        const unsigned constraintIndex = optStruct.nonLinConstIndices[i];
        //skip global constraints
        if(int(constraintIndex) >= nonLinConstGradients->get_n_rows() + 1){//+1 for the objective
          continue; 
        }
        // for constraint i, we extract now the gradients with respect to decision variables
        for(unsigned j=0; j<numParameters; j++){
           // store only sensitivities of decision variables
           std::vector<unsigned>::iterator varIndexPtr;
           varIndexPtr = std::find(varIndices.begin(),
                                   varIndices.end(),
                                   optStruct.activeParameterIndices[j]);
          if(varIndexPtr != varIndices.end()){
            // the parameterIndex is the column position of the parameter in the sensitivity matrix
            const unsigned parameterIndex = varIndexPtr - varIndices.begin();
            assert(int(parameterIndex) < nonLinConstGradients->get_n_cols());
            // gradientIndex is the position of the the gradient in the intOptData struct
            // e.g. they are sorted as: x1p1, x1p2, x1p3, x2p1, x2p2, x2p3
            // x1p1 means: the sensitivity of state x1 with respect to parameter p1
            const unsigned gradientIndex = i * numParameters + j;
            assert(gradientIndex < numGrad);
            const double gradientValue = optStruct.nonLinConstGradients[gradientIndex];
            // we have zero data in the entire cs matrix. So we only override the value.
            constraintIndexVec.push_back(constraintIndex);
            parameterIndexVec.push_back(parameterIndex);
            gradientIndexVec.push_back(gradientIndex);
            gradientValueVec.push_back(gradientValue);
            nonLinConstGradients->setValue(constraintIndex, parameterIndex, gradientValue);
          }
        }
      }
    }
  }
}



/**
* @copydoc OptimizerMetaData::getNonLinConstraintDerivativesDecVar
*/
void OptimizerSingleStageMetaData::getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const
{
  // we assume that the nonLinConstGradients cs matrix is sparse but all non zero elements
  // are allocated
  assert(derivativeMatrix->get_nnz() == m_optProbDim.nonLinJacDecVar->get_nnz()); //assert(nonLinConstGradients->get_nnz() == m_optProbDim.nonLinJacDecVar->get_nnz());


  std::vector<unsigned> decVarIndices;
  getDecisionVariableIndices(decVarIndices);
  std::vector<IntOptDataMap> intOptDataVec = m_integratorMetaData->getIntOptData();

  retrieveNonLinConstGradients(derivativeMatrix,
                               decVarIndices,
                               intOptDataVec.front(),
                               m_objective.getEsoIndex());
}
/**
* @copydoc OptimizerMetaData::getNonLinConstraintDerivativesConstParam
*/
void OptimizerSingleStageMetaData::getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr &nonLinConstGradients) const
{
  // we assume that the nonLinConstGradients cs matrix is sparse but all non zero elements
  // are allocated
  assert(nonLinConstGradients->get_nnz() == m_optProbDim.nonLinJacConstPar->get_nnz());

  std::vector<unsigned> constParamIndices;
  getConstantParameterIndices(constParamIndices);
    std::vector<IntOptDataMap> intOptDataVec = m_integratorMetaData->getIntOptData();

  retrieveNonLinConstGradients(nonLinConstGradients,
                               constParamIndices,
                               intOptDataVec.front(),
                               m_objective.getEsoIndex());
}

/**
* @copydoc OptimizerMetaData::getNonLinObjDerivative
*/
void OptimizerSingleStageMetaData::getNonLinObjDerivative(utils::Array<double> &values) const
{
  std::vector<IntOptDataMap> intOptData = m_integratorMetaData->getIntOptData();
  // to evaluate the gradient of the objective we only need the data at the final time.
  IntOptDataMap::iterator lastIter = intOptData[0].end();
  lastIter--;

  // look for the objective eso index within the constraints at the end point
  const unsigned objIndex = m_objective.getEsoIndex();
  std::vector<int> constVarIndices = (*lastIter).second.nonLinConstVarIndices;

  std::vector<int>::iterator objPosIter;
  objPosIter = std::find(constVarIndices.begin(),
                         constVarIndices.end(),
                         objIndex);
  assert(objPosIter != constVarIndices.end());
  const int objPos = objPosIter - constVarIndices.begin();
  // we know the position of the objective.
  // Now we need to extract the sensitivities with respect to all active parameters
  const unsigned numParams = (*lastIter).second.numSensParam;
  assert(values.getSize() == numParams);
  for (unsigned i=0; i<numParams; i++){
    values[i] = (*lastIter).second.nonLinConstGradients[numParams * objPos + i];
  }
}

/**
* @brief get the data struct needed by the OptimizerSingleStageMetaData
* @return the struct containing the collected data during an integration
*/
std::vector<IntOptDataMap> OptimizerSingleStageMetaData::getIntegrationOptData() const
{
  std::vector<IntOptDataMap> integrationOutput = m_integratorMetaData->getIntOptData();
  return integrationOutput;
}

/** @brief returns a copy of the objective data
*   @return objective data
*/
StatePointConstraint OptimizerSingleStageMetaData::getObjectiveData(void)
{
  return m_objective;
}

/**
* @copydoc OptimizerMetaData::getLinConstraintValues
*/
void OptimizerSingleStageMetaData::getLinConstraintValues(utils::Array<double> &values) const
{
  assert(values.getSize() == m_optProbDim.numLinConstraints);
  if(m_optProbDim.numLinConstraints == 0){
    return;
  }
  assert(m_optProbDim.numLinConstraints == 1);
  values[0] = m_integratorMetaData->getStageDuration();
}

/**
* @copydoc OptimizerMetaData::getLinConstraintBounds
*/
void OptimizerSingleStageMetaData::getLinConstraintBounds
                                          (utils::Array<double> &lowerBounds,
                                           utils::Array<double> &upperBounds) const
{
  std::vector<DurationParameter::Ptr> durationParameters;
  m_integratorMetaData->getDurationParameters(durationParameters);
  assert(lowerBounds.getSize() == m_optProbDim.numLinConstraints);
  assert(upperBounds.getSize() == m_optProbDim.numLinConstraints);

  if(m_optProbDim.numLinConstraints == 0){
    return;
  }
  assert(m_optProbDim.numLinConstraints == 1);

  DurationParameter::Ptr globalDurationParameter;
  globalDurationParameter = m_integratorMetaData->getGlobalDurationParameter();

  double lowerBound = globalDurationParameter->getParameterLowerBound();
  double upperBound = globalDurationParameter->getParameterUpperBound();
  // only linear constraints are  lb<=sum(ti)<=ub
  // all fixed parameters have to be subtracted from the bounds
  for(unsigned i=0; i<durationParameters.size(); i++){
    if(!durationParameters[i]->getIsDecisionVariable()){
      lowerBound -= durationParameters[i]->getParameterValue();
      upperBound -= durationParameters[i]->getParameterValue();
    }
  }

  lowerBounds[0] = lowerBound;
  upperBounds[0] = upperBound;
}

/**
* @brief collect all derivatives of the linear constraints and store it in a sparse matrix
*
* @param linJac pointer to a sparse matrix containing the derivatives of the
*               linear constraints in triplet format
*/
void OptimizerSingleStageMetaData::calculateLinJacStructure()
{
  //getDecisionVariables assigns a parameter index to all parameters
  std::vector<Parameter::Ptr> sensParams;
  m_integratorMetaData->getSensitivityParameters(sensParams);
  //collect the dimensions of the linJac
  unsigned numDurationConstraints = 0;
  std::vector<DurationParameter::Ptr> durationParameters;
  m_integratorMetaData->getDurationParameters(durationParameters);
  //calculate dimensions of linJac
  unsigned numLinNonZeroes = 0;
  for(unsigned i=0; i<durationParameters.size(); i++){
    if(durationParameters[i]->getIsDecisionVariable()){
      numLinNonZeroes ++;
    }
  }
  // only add linear constraint if any duration is decision variable
  if(numLinNonZeroes > 0){
    numDurationConstraints ++;
  }


  m_optProbDim.numLinConstraints = numDurationConstraints;
  // only dummies for rows and columns
  const int* indicesSpace = new int[numLinNonZeroes]();
  const double* valuesSpace = new double[numLinNonZeroes]();
  //maybe better to write data into array first and then put it into the TripletMatrix
  m_optProbDim.linJac = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(m_optProbDim.numLinConstraints,
                                                                       m_optProbDim.numOptVars,
                                                                       numLinNonZeroes,
                                                                       indicesSpace,
                                                                       indicesSpace,
                                                                       valuesSpace));
  
  delete []indicesSpace;
  delete []valuesSpace;
  CsTripletMatrix::Ptr linJac = m_optProbDim.linJac;

  // provide data for linJac
  // set lb<=sum(ti)<=ub
  unsigned arrayIndex = 0;
  for(unsigned i=0; i<durationParameters.size(); i++){
    if(durationParameters[i]->getIsDecisionVariable()){
      assert(arrayIndex < numLinNonZeroes);
      linJac->get_matrix_ptr()->i[arrayIndex] = 0;
      //adjust parameter index to decision variable index
      int parameterIndex = durationParameters[i]->getParameterIndex();
      int numMissingParameters = 0;
      for(int j=0; j<parameterIndex; j++){
        if(!sensParams[j]->getIsDecisionVariable()){
          numMissingParameters++;//constant parameters are not decision variables
        }
      }
      linJac->get_matrix_ptr()->p[arrayIndex] = parameterIndex - numMissingParameters;
      linJac->get_matrix_ptr()->x[arrayIndex] = 1.0;
      arrayIndex++;
    }
  }// for i
}// OptimizerSingleStageMetaData::calculateLinJacStructure



/**
* @brief getter for structure StageOutput
*
* @param[in] StageInput structure
* @param[in] StageOutput structure
*/
std::vector<DyosOutput::StageOutput> OptimizerSingleStageMetaData::getOutput()
{
  std::vector<DyosOutput::StageOutput> output(1);
  output.front().optimizer = this->getStageOutput();
  output.front().integrator = m_integratorMetaData->getOutput().front().integrator;
  return output;
}


/**
* @brief getter for structure OptimizerStageOutput
*
* @param[in] OptimizerOutput structure
* @param[in]       OptimizerStageInput structure
* @return OptimizerStageOutput
*/
DyosOutput::OptimizerStageOutput  OptimizerSingleStageMetaData::getStageOutput()
{
  DyosOutput::OptimizerStageOutput output;
  const unsigned numConstraints = m_constraints.size();
  output.nonLinearConstraints.resize(numConstraints);

  GenericEso::Ptr genEso = m_integratorMetaData->getGenericEso();
  std::vector<IntOptDataMap> optData = m_integratorMetaData->getIntOptData();
  for(unsigned i=0; i<numConstraints; i++){
    output.nonLinearConstraints[i] = m_constraints[i].getOutput(genEso);
    const unsigned timeIndex = m_constraints[i].getTimeIndex();
    double timePoint = m_integratorMetaData->getStageGridScaled()[timeIndex];
    output.nonLinearConstraints[i].timePoint = timePoint;
    const int pos = std::find(
      optData[0][timeIndex].nonLinConstVarIndices.begin(),
      optData[0][timeIndex].nonLinConstVarIndices.end(),
      output.nonLinearConstraints[i].esoIndex) - optData[0][timeIndex].nonLinConstVarIndices.begin();
    output.nonLinearConstraints[i].value = optData[0][timeIndex].nonLinConstValues[pos];
  }

  output.objective = m_objective.getOutput(genEso);
  output.objective.value = getNonLinObjValue();

  return output;
}

OptimizerSSMetaData2ndOrder::OptimizerSSMetaData2ndOrder(
                             const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData)
                            :OptimizerSingleStageMetaData(integratorMetaData)
{
  m_integratorMetaData2ndOrder = integratorMetaData;
}

OptimizerSSMetaData2ndOrder::~OptimizerSSMetaData2ndOrder()
{
}


void OptimizerSSMetaData2ndOrder::getNonLinConstraintHessian(CsTripletMatrix::Ptr &hessianMatrix,
                                                             const std::vector<unsigned> &indices) const
{ //ders is derivatives
  std::vector<double> lagrangeDers, lagrangeDers2ndOrder;
  m_integratorMetaData2ndOrder->getLagrangeDers(lagrangeDers);
  const unsigned numColsTotal = m_integratorMetaData->getNumSensitivityParameters();
  lagrangeDers2ndOrder.assign(lagrangeDers.begin() + numColsTotal, lagrangeDers.end());
  //extractValuesToMatrix(hessianMatrix, indices, indices, numColsTotal, numColsTotal, lagrangeDers2ndOrder);  
  extractValuesToMatrix(hessianMatrix, indices, numColsTotal, numColsTotal, lagrangeDers2ndOrder);
}


/**
* 
*/
void OptimizerSSMetaData2ndOrder::extractValuesToMatrix(CsTripletMatrix::Ptr &matrix,
                                                        const std::vector<unsigned> &indices,
                                                        const unsigned numRows,
                                                        const unsigned numColsTotal,
                                                        const std::vector<double> &values)
{

  // clear hessianMatrix - will be reallocated here
  unsigned numCols = indices.size();
  OptimizerSingleStageMetaData::constructFullCooMatrix(matrix, numRows, numCols);

  assert(values.size() == numRows * numColsTotal);
  for(unsigned int i=0; i<numRows; i++){
    for(unsigned int j=0; j<numCols; j++){
      //skip values of previous rows
      unsigned valueIndex = i*numColsTotal;
      // get to the current entry
      valueIndex += indices[j];
      assert(valueIndex < values.size());
      matrix->get_matrix_ptr()->x[i*numCols + j] = values[valueIndex];
    }
  }
}

/**
* @note corrected version (allowing hessian to have correct size)
*/
void OptimizerSSMetaData2ndOrder::extractValuesToMatrix(CsTripletMatrix::Ptr &matrix,
                                                        const std::vector<unsigned> &colIndices,
                                                        const std::vector<unsigned> &rowIndices,
                                                        const unsigned numRowsTotal,
                                                        const unsigned numColsTotal,
                                                        const std::vector<double> &values)
{

  // clear hessianMatrix - will be reallocated here
  unsigned numCols = colIndices.size();
  unsigned numRows = rowIndices.size();
  OptimizerSingleStageMetaData::constructFullCooMatrix(matrix, numRows, numCols);

  assert(values.size() == numRowsTotal * numColsTotal);
  for(unsigned int i=0; i<numRows; i++){
    for(unsigned int j=0; j<numCols; j++){
      //skip values of previous rows
      unsigned valueIndex = rowIndices[i]*numColsTotal;
      // get to the current enttry
      valueIndex += colIndices[j];
      assert(valueIndex < values.size());
      matrix->get_matrix_ptr()->x[i*numCols + j] = values[valueIndex];
    }
  }
}

void OptimizerSSMetaData2ndOrder::getNonLinConstraintHessianDecVar(CsTripletMatrix::Ptr &hessianMatrix) const
{
  std::vector<unsigned> decisionVariableIndices;
  getDecisionVariableIndices(decisionVariableIndices);

  getNonLinConstraintHessian(hessianMatrix, decisionVariableIndices);
}

void OptimizerSSMetaData2ndOrder::getNonLinConstraintHessianConstParam(CsTripletMatrix::Ptr &hessianMatrix) const
{
  std::vector<unsigned> constantParameterIndices;
  getConstantParameterIndices(constantParameterIndices);

  getNonLinConstraintHessian(hessianMatrix, constantParameterIndices);
}

void OptimizerSSMetaData2ndOrder::getAdjoints(std::vector<double> &adjoints1stOrder,
                                              CsTripletMatrix::Ptr &adjoints2ndOrder,
                                              std::vector<unsigned> &indices) const
{

  std::vector<double> adjoints;
  m_integratorMetaData2ndOrder->getAdjoints(adjoints);

  const unsigned numStates = m_integratorMetaData->getGenericEso()->getNumStates();
  const unsigned numSensParam = m_integratorMetaData->getNumSensitivityParameters();

  assert(adjoints.size() == numStates * (1 + numSensParam));

  adjoints1stOrder.assign(adjoints.begin(), adjoints.begin() + numStates);

  std::vector<double> adjoints2ndOrderVec;
  adjoints2ndOrderVec.assign(adjoints.begin() + numStates, adjoints.end());
  std::vector<unsigned> rowIndices(numStates);
  for(unsigned i=0; i<numStates; i++){
    rowIndices[i] = i;
  }
  extractValuesToMatrix(adjoints2ndOrder, indices, rowIndices, numStates, numSensParam, adjoints2ndOrderVec);
}


void OptimizerSSMetaData2ndOrder::getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
                                                    CsTripletMatrix::Ptr &adjoints2ndOrder) const
{
  std::vector<unsigned> decisionVariableIndices;
  getDecisionVariableIndices(decisionVariableIndices);

  getAdjoints(adjoints1stOrder, adjoints2ndOrder, decisionVariableIndices);
}

void OptimizerSSMetaData2ndOrder::getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                                        CsTripletMatrix::Ptr &adjoints2ndOrder) const
{
  std::vector<unsigned> constantParameterIndices;
  getConstantParameterIndices(constantParameterIndices);

  getAdjoints(adjoints1stOrder, adjoints2ndOrder, constantParameterIndices);
}
