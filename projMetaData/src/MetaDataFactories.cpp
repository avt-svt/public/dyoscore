/**
* @file MetaDataFactories.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData                                                             \n
* =====================================================================\n
* This file contains the definitions of member functions of the        \n
* factories for the classes IntegratorMetaData, OptimizerMetaData,     \n
* Parameter and Control                                                \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 15.02.2012
*/

#include <cassert>
#include <cmath>

#include "MetaDataFactories.hpp"
#include "MetaDataExceptions.hpp"
#include "EsoUtilities.hpp"

#define EPSILON 1e-12

using namespace FactoryInput;

/**
* @brief change multigrid input to single grid input for creation of a continuous control parameter
*
* @param[in] input ParameterInput with type CONTINUOUS_CONTROL
* @return ParameterInput for an adequate parameter with type CONTROL.
*         For this parameter all grids have been merged to one
*/ 
 ParameterInput ControlFactory::mergeInputGridsIntoOne
                                  (const ParameterInput &input) const
{
  ParameterInput mergedInput = input;
  mergedInput.type = ParameterInput::CONTROL;
  mergedInput.grids.resize(1);
  double totalDuration = mergedInput.grids.front().duration;
  for(unsigned i=1; i<input.grids.size(); i++)
  {
    if(input.grids[i-1].approxType != PieceWiseConstant 
      || input.grids[i].approxType != PieceWiseConstant){
      //merge inputs
      //here approxType is assumed to be either PieceWiseLinear or PieceWiseConstant
      //so the merged grid is always PieceWiseLinear
      mergedInput.grids.back().approxType = PieceWiseLinear;
      
      //merge values
      std::vector<double> currentValues = mergedInput.grids.back().values;
      std::vector<double> nextValues = input.grids[i].values;
      if(input.grids[i-1].approxType == PieceWiseConstant){
        //if the grid i-2 is PieceWiseLinear The parameter already has been doubled
        //so do not add another parameter then
        if(i<2 || input.grids[i-2].approxType == PieceWiseConstant){
          currentValues.push_back(currentValues.back());
        }
        nextValues.erase(nextValues.begin());
      }
      else if(input.grids[i].approxType == PieceWiseConstant){
        currentValues.pop_back();
        nextValues.push_back(nextValues.back());
      }
      else{
        currentValues.pop_back();
      }
      currentValues.insert(currentValues.end(), nextValues.begin(), nextValues.end());
      mergedInput.grids.back().values = currentValues;
      
      //merge timePoints;
      double nextDuration = input.grids[i].duration;
      std::vector<double> currentTimePoints = mergedInput.grids.back().timePoints;
      //point 1.0 is equal to point 0.0 of the next grid - so one of the points have to be removed
      currentTimePoints.pop_back();
      std::vector<double> nextTimePoints = input.grids[i].timePoints;
      //unscale all timepoints
      for(unsigned j=0; j<currentTimePoints.size(); j++){
        currentTimePoints[j] *= totalDuration;
      }
      for(unsigned j=0; j<nextTimePoints.size(); j++){
        nextTimePoints[j] = totalDuration + nextTimePoints[j]*nextDuration;
      }
      currentTimePoints.insert(currentTimePoints.end(), nextTimePoints.begin(), nextTimePoints.end());
      totalDuration += nextDuration;
      
      //rescale timepoints
      for(unsigned j=0; j<currentTimePoints.size(); j++){
        currentTimePoints[j] /= totalDuration;
      }
      currentTimePoints.front() = 0.0;
      currentTimePoints.back() = 1.0;
      mergedInput.grids.back().timePoints = currentTimePoints;
    }
    else{
      //in this case two adjacent grids are piecewise constant, so no continuous behaviour possible
      mergedInput.grids.back().duration = totalDuration;
      mergedInput.grids.push_back(input.grids[i]);
      totalDuration = input.grids[i].duration;
    }
  }
  mergedInput.grids.back().duration = totalDuration;
  return mergedInput;
}

/**
* @brief factory method for creating a single control
*
* @param[in] input ParameterInput struct containing information of construction;
*                  the type should be CONTROL
* @return created Control object created according to the input struct
* @todo there is still the problem if a successing grid is piecewise constant, since 
*       the function insert grid always has the first value of the successing grid refer to 
*       the previous one. One option would be to make the function more flexible.
*/
Control ControlFactory::createControl(const ParameterInput &input) const
{
  assert(input.type == ParameterInput::CONTROL 
         || input.type == ParameterInput::CONTINUOUS_CONTROL);
  
  if(input.type == ParameterInput::CONTINUOUS_CONTROL && input.grids.size() > 1){
    ParameterInput mergedGridsInput = mergeInputGridsIntoOne(input);
    Control c = createControl(mergedGridsInput);
    double totalDuration = 0.0;
    unsigned index = 0;
    for(unsigned i=0; i<input.grids.size(); i++){
      totalDuration += input.grids[i].duration;
      DurationParameter::Ptr duration(new DurationParameter(input.grids[i].duration));
      c.insertDurationParameter(duration, i);
      std::vector<ControlParameter::Ptr> allControls = 
                  c.getParameterizationGrid(i)->getAllControlParameter();
      //EPSILON is a define of this file
	  //mergedGridsInput merges only PWL grids to other grids (can contain more than one)
	  //index is the index of the current grid of the merged grid input
	  //totalDuration is the total duration of the current merged grid (reset to 0 if index is changed)
      if(totalDuration < mergedGridsInput.grids[index].duration - EPSILON){
      
        //adjust the newly created grid
        if(input.grids[i].approxType == PieceWiseConstant){
          //the last value was copied - so remove the last parameter.
          assert(allControls.size() >=2);
          allControls.pop_back();
          // in case of PWL->PWC swap the parameters between PWL and PWC,
          // such that PWL has the Substitute and  PWC has the normal parameter
		  //apparently this if statement is always false, since approx type is PieceWiseConstant
		  //do we mean input.grids[i-1]?
          if(i>1 && input.grids[i].approxType == PieceWiseLinear){
            std::vector<ControlParameter::Ptr> previousControlParameters =
                c.getParameterizationGrid(i-1)->getAllControlParameter();
            ControlParameter::Ptr temp = previousControlParameters.back();
            previousControlParameters.back() = allControls.front();
            allControls.front() = temp;
            c.getParameterizationGrid(i-1)->setAllControlParameter(previousControlParameters);
          }
          
          //the parameter of the next  grid must be related to the last parameter only
          //grid i+1 must exist, since the grids have been split by insertDurationParameter
		  //update potentially swapped parameter? Only relevant if if-condition above is triggered?
		  //we assume that related parameter already created in insertDuration function
          std::vector<ControlParameter::Ptr> nextControlParameters =
                c.getParameterizationGrid(i+1)->getAllControlParameter();
          ControlParameter::Ptr relatedToConstant(new SubstituteParameter(allControls.back(),
                                                                          allControls.back(),
                                                                          1.0));
          nextControlParameters.front() = relatedToConstant;
          c.getParameterizationGrid(i+1)->setAllControlParameter(nextControlParameters);
        }
      }
      else {
        //adjust the grid of the last split
        if(input.grids[i].approxType != c.getParameterizationGrid(i)->getControlType()){
          assert(input.grids[i].approxType == PieceWiseConstant);
          // the first value is a copie of the second, but since the first is 
          // linked to the previous grid, remove the second parameter
		  // shouldn't it be allControls.erase(allControls.begin()+1)?
          assert(allControls.size() >=2);
          allControls.erase(allControls.begin());
        }
        //skip to the next grid
        index ++; 
        totalDuration = 0.0;
      }
      c.getParameterizationGrid(i)->setControlType(input.grids[i].approxType);
      c.getParameterizationGrid(i)->setAllControlParameter(allControls);
     
    }
    std::vector<double> noAdditionalPoints;
    //adjust controlGrid of c (this function calculates the new current controlGrid vector)
    c.setGridPointsVector(noAdditionalPoints);
    return c;
  }
  Control c;

  c.setEsoIndex(input.esoIndex);
  c.setLowerBound(input.lowerBound);
  c.setUpperBound(input.upperBound);
  
  
  const unsigned numGrids = input.grids.size();
  std::vector<ParameterizationGrid::Ptr> controlGrids(numGrids);

  for(unsigned i=0; i<numGrids; i++){
    int numIntervals = input.grids[i].timePoints.size()-1;
    assert(input.grids[i].timePoints.back() == 1.0);
    ParameterizationGrid::Ptr tempGrid(new ParameterizationGrid(numIntervals,
                                                                input.grids[i].approxType));
    tempGrid->setAllParameterBounds(input.lowerBound, input.upperBound);
    tempGrid->setAllParameterizationValues(input.grids[i].values);
    tempGrid->setDiscretizationGrid(input.grids[i].timePoints);
    tempGrid->setPCResolution(input.grids[i].pcresolution);
    tempGrid->setLogger(m_logger);
    if(input.grids[i].parametersFixed){ // this comes from structure detection
      tempGrid->setAllParametersWithoutSens();
      tempGrid->setAllParametersNotDecisionVars();
    }
    DurationParameter::Ptr duration(new DurationParameter(input.grids[i].duration));
    duration->setWithSensitivity(input.grids[i].hasFreeDuration);
    duration->setIsDecisionVariable(input.grids[i].hasFreeDuration);
    duration->setLogger(m_logger);
    tempGrid->setDurationPointer(duration);
    controlGrids[i] = tempGrid;
  }//for

  c.setControlGrid(controlGrids);
  switch(input.sensType){
    case ParameterInput::NO:
      c.setControlWithoutSens();
      break;
    case ParameterInput::FRACTIONAL:
      c.unsetIsDecisionVariable();
      break;
    default:
      ;//do nothing
  }
  c.setLogger(m_logger);
  return c;
}

/**
* @brief constructs a time invariant control
*
* This factory constructs a restricted control which corresponds to a time invariant parameter.
* It only has one parameterization grid with one piece wise constant approximation.
* @param[in] input ParameterInput struct containing information of construction
*              the type should be TIME_INVARIANT
* @return time invariant control
*/
Control ControlFactory::createTimeInvariantParameter(const ParameterInput &input) const
{
  assert(input.type == ParameterInput::TIME_INVARIANT);
  Control c;
  // setting Bounds and Index
  c.setEsoIndex(input.esoIndex);
  c.setLowerBound(input.lowerBound);
  c.setUpperBound(input.upperBound);
  // create retarded grid with one piece wise constant approximation
  const unsigned numIntervals = 1;
  ParameterizationGrid::Ptr retardedGrid(new ParameterizationGrid(numIntervals, PieceWiseConstant));
  // set the default user value of the parameter
  std::vector<double> parameterizationValues(numIntervals);
  parameterizationValues[0] = input.value;
  retardedGrid->setAllParameterBounds(input.lowerBound, input.upperBound);
  retardedGrid->setAllParameterizationValues(parameterizationValues);
  retardedGrid->setLogger(m_logger);
  DurationParameter::Ptr duration(new DurationParameter(input.grids[0].duration));
  duration->setWithSensitivity(false);
  duration->setIsDecisionVariable(false);
  duration->setLogger(m_logger);
  retardedGrid->setDurationPointer(duration);
  // set the retarded time invariant control grid
  std::vector<ParameterizationGrid::Ptr> retardedGridVector;
  retardedGridVector.push_back(retardedGrid);
  c.setControlGrid(retardedGridVector);
  // after we set the control grid we have to "lock it".
  bool isInvariant = true;
  c.setIsInvariant(isInvariant);
  switch(input.sensType){
    case ParameterInput::NO:
      c.setControlWithoutSens();
      break;
    case ParameterInput::FRACTIONAL:
      c.unsetIsDecisionVariable();
      break;
    default:
      ;//do nothing
  }
  c.setLogger(m_logger);
  return c;
}

/**
* @brief factory method for creating a vector of controls
*
* @param[in] inputVector vector of ParameterInput structs containing information of construction
*                    the type should be PROFILE or TIME_INVARIANT
* @return vector of Control objects created according to inputVector
*/
std::vector<Control> ControlFactory::createControlVector(const std::vector<ParameterInput> &inputVector)const
{
  const unsigned numControls = inputVector.size();
  std::vector<Control> cVector(numControls);

  for(unsigned i=0; i<numControls; i++){
    //function createControl asserts input type
    switch(inputVector[i].type){
      case ParameterInput::CONTROL:
        cVector[i] = createControl(inputVector[i]);
        break;
      case ParameterInput::CONTINUOUS_CONTROL:
        cVector[i] = createControl(inputVector[i]);
        break;
      case ParameterInput::TIME_INVARIANT:
        cVector[i] = createTimeInvariantParameter(inputVector[i]);
        break;
      default:
        assert(false);
    }
  }
  return cVector;
}

/**
* @brief set sensitivity type information ubto parameter
*
* @param[in,out] p Parameter pointer in which sensitivity type is to be set
* @param[in] sensType ParameterSensitivityType to be set in p
*/
void ParameterFactory::setSensTypeInformation(Parameter::Ptr p,
                                        const ParameterInput::ParameterSensitivityType sensType) const
{
  switch(sensType){
    case ParameterInput::NO:
      p->setWithSensitivity(false);
      p->setIsDecisionVariable(false);
      break;
    case ParameterInput::FRACTIONAL:
      p->setIsDecisionVariable(false);
      p->setWithSensitivity(true);
      break;
    case ParameterInput::FULL:
      p->setIsDecisionVariable(true);
      p->setWithSensitivity(true);
      break;
    default:
      assert(false);
  }
}

/**
* @brief factory method for creating a final time parameter
*
* @param[in] input ParameterInput struct containing information of construction
*              the type must be DURATION
* @return DurationParameter object pointer created according to input
*/
DurationParameter::Ptr ParameterFactory::createDurationParameter(const ParameterInput &input)const
{
  assert(input.type == ParameterInput::DURATION);
  DurationParameter::Ptr duration (new DurationParameter(input.value));
  if(input.sensType == ParameterInput::FULL){
    duration->setParameterLowerBound(input.lowerBound);
    duration->setParameterUpperBound(input.upperBound);
  }
  else{
    duration->setParameterLowerBound(input.value);
    duration->setParameterUpperBound(input.value);
  }

  setSensTypeInformation(duration, input.sensType);
  duration->setLogger(m_logger);
  return duration;
}

/**
* @brief factory method for creating an initial value parameter
*
* @param[in] input ParameterInput struct containing information of construction
               the type must be INITIAL
* @return InitialValueParameter object created according to input
*/
InitialValueParameter::Ptr ParameterFactory::createInitialValueParameter(const ParameterInput &input,
                                                                     const GenericEso::Ptr &genEso)const
{
  assert(input.type == ParameterInput::INITIAL);
  InitialValueParameter::Ptr initial (new InitialValueParameter());
  initial->setEsoIndex(input.esoIndex);
  EsoIndex equationIndex = getEquationIndexOfVariable(input.esoIndex, genEso);
  assert(equationIndex >= 0);
  initial->setEquationIndex(equationIndex);
  initial->setParameterLowerBound(input.lowerBound);
  initial->setParameterUpperBound(input.upperBound);
  initial->setParameterValue(input.value);
  initial->setStateIndex(genEso->getStateIndexOfVariable(input.esoIndex));

  setSensTypeInformation(initial, input.sensType);
  initial->setLogger(m_logger);
  return initial;
}

/**
* @brief factory method for creating a vector of InitialValueParameter
*
* @param[in] inputVector vector of ParameterInput containing information of construction
*                    must be of type INITIAL
* @return vector of initial value parameters
*/
std::vector<InitialValueParameter::Ptr> ParameterFactory::createInitialValueParameterVector
                              (const std::vector<ParameterInput> &inputVector,
                               const GenericEso::Ptr &genEso)const
{
  const unsigned numParameter = inputVector.size();

  std::vector<InitialValueParameter::Ptr> parameters(numParameter);
  for(unsigned i=0; i<numParameter; i++){
    //function createInitialValueParameter asserts input type
    parameters[i] = InitialValueParameter::Ptr(createInitialValueParameter(inputVector[i], genEso));
  }

  return parameters;
}

/**
* @brief factory method for creating an IntegratorMetaData object using standard factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*              an IntegratorMetaData object
* @return pointer to the created IntegtratorMetaData object
*/
IntegratorMetaData::Ptr IntegratorMetaDataFactory::createIntegratorMetaData(
                                               const struct IntegratorMetaDataInput &input)
{
  if(input.stages.empty()){
    return createIntegratorSingleStageMetaData(input);
  }
  else{
    return createIntegratorMultiStageMetaData(input);
  }
}

/**
* @brief factory method for creating an IntegratorMetaData object using standard factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*              an IntegratorMetaData object
* @return pointer to the created IntegtratorMetaData object
*/
IntegratorMetaData2ndOrder::Ptr IntegratorMetaDataFactory::createIntegratorMetaData2ndOrder(
                                               const struct IntegratorMetaDataInput &input)
{
  if(input.stages.empty()){
    return createIntegratorSSMetaData2ndOrder(input);
  }
  else{
    return createIntegratorMSMetaData2ndOrder(input);
  }
}

/**
* @brief factory method for creating an IntegratorSingleStageMetaData object using standard factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*              an IntegratorMetaData object
* @return pointer to the created IntegtratorSingleStageMetaData object
*/
IntegratorSingleStageMetaData::Ptr IntegratorMetaDataFactory::createIntegratorSingleStageMetaData(
                                               const struct IntegratorMetaDataInput &input)
{
  ControlFactory controlFactory;
  controlFactory.setLogger(m_logger);
  ParameterFactory paramFactory;
  paramFactory.setLogger(m_logger);
  GenericEsoFactory genEsoFactory;

  return createIntegratorSingleStageMetaData(input, controlFactory, paramFactory, genEsoFactory);
}

/**
* @brief factory method for creating an IntegratorSSMetaData2ndOrder object using standard factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*                  an IntegratorMetaData object
* @return pointer to the created IntegtratorSingleStageMetaData object
*/
IntegratorSSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::createIntegratorSSMetaData2ndOrder(
                                               const struct IntegratorMetaDataInput &input)
{
  ControlFactory controlFactory;
  controlFactory.setLogger(m_logger);
  ParameterFactory paramFactory;
  paramFactory.setLogger(m_logger);
  GenericEsoFactory genEsoFactory;

  return createIntegratorSSMetaData2ndOrder(input, controlFactory, paramFactory, genEsoFactory);
}

/**
* @brief factory method for creating an IntegratorSingleStageMetaData object using given factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*                  an IntegratorMetaData object
* @param[in] controlFactory factory used to create Controls for the IntegratorMetaData object
* @param[in] paramFactory factory used to create the InitialValueParameter objects and the
                          DurationParameter object for the IntegratorMetaData object
* @param[in] genEsoFactory factory used to create GenericEso object for the IntegratorMetadata object
* @return pointer to the created IntegratorSingleStageMetaData object
*/
IntegratorSingleStageMetaData::Ptr IntegratorMetaDataFactory::createIntegratorSingleStageMetaData(
                                                        const struct IntegratorMetaDataInput &input,
                                                        const ControlFactory &controlFactory,
                                                        const ParameterFactory &paramFactory,
                                                        const GenericEsoFactory &genEsoFactory)
{
  std::vector<Control> controls = controlFactory.createControlVector(input.controls);
  DurationParameter::Ptr globalDuration(paramFactory.createDurationParameter(input.duration));
  globalDuration->setParameterSensActivity(true);
  std::vector<InitialValueParameter::Ptr> initials;
  StdCapture capture;
  capture.beginCapture();
  GenericEso::Ptr genEso = input.esoPtr;
  capture.endCapture();
  m_logger->sendMessage(Logger::ESO, OutputChannel::STANDARD_VERBOSITY, capture.getCapture());
  m_logger->newlineAndFlush(Logger::ESO, OutputChannel::STANDARD_VERBOSITY);
  //check Eso for allowed structure (M is constant - no STNS)
  if(genEso->getNumConditions() > 0){
    throw ModelHasConditionsException();
  }
  if(!BMatrixIsConstant(genEso)){
    throw MassMatrixIsNotTimeInvariantException();
  }
  initials = paramFactory.createInitialValueParameterVector(input.initials, genEso);
  std::vector<double> userGrid = input.userGrid;

  return createIntegratorSingleStageMetaData(controls, initials, globalDuration,
                                             userGrid, genEso, input.plotGridResolution, input.explicitPlotGrid);
}

/**
* @brief factory method for creating an IntegratorSingleStageMetaData object using given factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*                  an IntegratorMetaData object
* @param[in] controlFactory factory used to create Controls for the IntegratorMetaData object
* @param[in] paramFactory factory used to create the InitialValueParameter objects and the
                          DurationParameter object for the IntegratorMetaData object
* @param[in] genEsoFactory factory used to create GenericEso object for the IntegratorMetadata object
* @return pointer to the created IntegratorSingleStageMetaData object
*/
IntegratorSSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::
                                            createIntegratorSSMetaData2ndOrder(
                                            const struct IntegratorMetaDataInput &input,
                                            const ControlFactory &controlFactory,
                                            const ParameterFactory &paramFactory,
                                            const GenericEsoFactory &genEsoFactory)
{
  std::vector<Control> controls = controlFactory.createControlVector(input.controls);
  DurationParameter::Ptr globalDuration(paramFactory.createDurationParameter(input.duration));
  globalDuration->setParameterSensActivity(true);
  std::vector<InitialValueParameter::Ptr> initials;
  
  //capture output from GenericEso initialization (important to get gPROMS output in MATLAB)
  StdCapture capture;
  capture.beginCapture();
  GenericEso2ndOrder::Ptr genEso2ndOrder = dynamic_pointer_cast<GenericEso2ndOrder>(input.esoPtr);
  capture.endCapture();
  m_logger->sendMessage(Logger::ESO, OutputChannel::STANDARD_VERBOSITY, capture.getCapture());
  m_logger->newlineAndFlush(Logger::ESO, OutputChannel::STANDARD_VERBOSITY);
  GenericEso::Ptr genEso = genEso2ndOrder;  //check Eso for allowed structure (M is constant - no STNS)
  if(genEso->getNumConditions() > 0){
    throw ModelHasConditionsException();
  }
  if(!BMatrixIsConstant(genEso)){
    throw MassMatrixIsNotTimeInvariantException();
  }
  initials = paramFactory.createInitialValueParameterVector(input.initials, genEso);
  std::vector<double> userGrid = input.userGrid;

  return createIntegratorSSMetaData2ndOrder(controls,
                                            initials,
                                            globalDuration,
                                            userGrid,
                                            genEso2ndOrder,
                                            input.plotGridResolution,
                                            input.explicitPlotGrid);
}

/**
* @brief factory method for creating an IntegratorSingleStageMetaData object
*
* @param[in] controls vector of Control objects used to create the object
* @param[in] initials vector of InitialValueParameter objects used to create the object
* @param[in] globalDuration DurationParameter of the IntegratorMetaData object
* @param[in] genEso GenericEso object used to create the IntegratorMetaData object
* @param[in] userGrid additional gridpoints (generated by constraints) that are to be set
*                     IntegratorSingleStageMetaData object
* @return pointer to the created IntegratorSingleStageMetaData object
*/
IntegratorSingleStageMetaData::Ptr IntegratorMetaDataFactory::createIntegratorSingleStageMetaData(
                                               const std::vector<Control> &controls,
                                               const std::vector<InitialValueParameter::Ptr> &initials,
                                               const DurationParameter::Ptr &globalDuration,
                                               const std::vector<double> &userGrid,
                                               const GenericEso::Ptr &genEso,
                                               const unsigned plotGridResolution,
                                               const std::vector<double> &explicitPlotGrid)
{
  
  setProblemStageGrid(controls, globalDuration->getParameterValue(), userGrid);
  // the ISSMD is created using the problemGrid as user grid. Due to the introduction of
  // retarded parameters, we can get a different global grid. Thus, the points would not be
  // included in the integration grid.
  IntegratorSingleStageMetaData::Ptr issmd (new IntegratorSingleStageMetaData(genEso,
                                                                           controls,
                                                                           initials,
                                                                           globalDuration,
                                                                           m_problemGrid.front()));
  issmd->setLogger(m_logger);
  issmd->setPlotGrid(plotGridResolution, explicitPlotGrid);
  return issmd;
}

/**
* @brief factory method for creating a IntegratorSSeMetaData2ndOrder object
*
* @param[in] controls vector of Control objects used to create the object
* @param[in] initials vector of InitialValueParameter objects used to create the object
* @param[in] globalDuration DurationParameter of the IntegratorMetaData object
* @param[in] userGrid additional gridpoints (generated by constraints) that are to be set
*                     IntegratorSingleStageMetaData object
* @param[in] genEso GenericEso2ndOrder object used to create the IntegratorMetaData object
* @return pointer to the created IntegratorSSMetaData2ndOrder object
*/
IntegratorSSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::createIntegratorSSMetaData2ndOrder(
                                               const std::vector<Control> &controls,
                                               const std::vector<InitialValueParameter::Ptr> &initials,
                                               const DurationParameter::Ptr &globalDuration,
                                               const std::vector<double> &userGrid,
                                               const GenericEso2ndOrder::Ptr &genEso,
                                               const unsigned plotGridResolution,
                                               const std::vector<double> &explicitPlotGrid)
{
  setProblemStageGrid(controls, globalDuration->getParameterValue(), userGrid);
  // the ISSMD is created using the problemGrid as user grid. Due to the introduction of
  // retarded parameters, we can get a different global grid. Thus, the points would not be
  // included in the integration grid.
  IntegratorSSMetaData2ndOrder::Ptr issmd2nd (new IntegratorSSMetaData2ndOrder(genEso,
                                                                            controls,
                                                                            initials,
                                                                            globalDuration,
                                                                            m_problemGrid.front()));
  issmd2nd->setPlotGrid(plotGridResolution, explicitPlotGrid);
  issmd2nd->setLogger(m_logger);
  return issmd2nd;
}

/**
* @brief factory method to create a vector of IntegratorSingleStageMetaData pointer
*
* The optimizer needs the OptimizerSingleStageMetaData objects and the IntegratorMultiStageMetaData
* object to point to the same IntegratorSingleStageMetaData objects. So in order to create an
* OptimizerMultiStageMetaData the vector of integrator single stage objects have to be created
* first and then the integrator multi stage object created using the single stage object. Then the
* OptimizerMetaData object is created using both the single stage objects and the multi stage
* object. For that reason this function is needed to create the vector of single stage objects
* @param[in] inputVector vector of IntegratorMetaDataInput structs containing the information to create
*                        the IntegratorSingleStageMetaData pointer
* @return vector of created IntegratorSingleStageMetaData pointers
*/
std::vector<IntegratorSingleStageMetaData::Ptr> IntegratorMetaDataFactory::
                                     createIntegratorSingleStageMetaDataVector(
                                     const std::vector<IntegratorMetaDataInput> &inputVector)
{
  std::vector<IntegratorSingleStageMetaData::Ptr> stages(inputVector.size());
  m_problemGrid.resize(inputVector.size() + 1);
  for(unsigned i=0; i<inputVector.size(); i++){
    IntegratorSingleStageMetaData::Ptr stage(createIntegratorSingleStageMetaData(inputVector[i]));
    stages[i] = stage;
    m_problemGrid[i+1] = m_problemGrid.front();
  }
  m_problemGrid.erase(m_problemGrid.begin());
  return stages;
}

/**
* @brief factory method to create a vector of IntegratorSingleStageMetaData2ndOrder pointer
*
* @param[in] inputVector vector of IntegratorMetaDataInput structs containing the information to create
*                        the IntegratorSingleStageMetaData2ndOrder pointer
* @return vector of created IntegratorSingleStageMetaData2ndOrder pointers
* @sa createIntegratorSingleStageMetaDataVector
*/
std::vector<IntegratorSSMetaData2ndOrder::Ptr> IntegratorMetaDataFactory::
                         createIntegratorSSMetaData2ndOrderVector
                        (const std::vector<FactoryInput::IntegratorMetaDataInput> &inputVector)
{
  std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages(inputVector.size());
  m_problemGrid.resize(inputVector.size() + 1);
  for(unsigned i=0; i<inputVector.size(); i++){
    stages[i] = IntegratorSSMetaData2ndOrder::Ptr(createIntegratorSSMetaData2ndOrder(inputVector[i]));
    m_problemGrid[i+1] = m_problemGrid.front();
  }
  //delete dummy
  m_problemGrid.erase(m_problemGrid.begin());
  return stages;
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object using standard factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*                  an IntegratorMetaData object
* @return pointer to the created IntegtratorMultiStageMetaData object
*/
IntegratorMultiStageMetaData::Ptr IntegratorMetaDataFactory::
                              createIntegratorMultiStageMetaDataWithSingleStage(
                                               const struct IntegratorMetaDataInput &input)
{
  IntegratorSingleStageMetaData::Ptr stage(createIntegratorSingleStageMetaData(input));

  return createIntegratorMultiStageMetaDataWithSingleStage(stage);
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object using standard factories
*        for creating Control, Parameter and GenericEso objects
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed for creating
*                  an IntegratorMetaData object
* @return pointer to the created IntegtratorMultiStageMetaData object
*/
IntegratorMSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::
                              createIntegratorMSMetaData2ndOrderWithSingleStage(
                                               const struct IntegratorMetaDataInput &input)
{
  IntegratorSSMetaData2ndOrder::Ptr stage(this->createIntegratorSSMetaData2ndOrder(input));

  return createIntegratorMSMetaData2ndOrderWithSingleStage(stage);
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object using a single
*        IntegratorSingleStageMetaData pointer
*
* For optimization of a single stage problem the integrator and the OptimizerMetaData both need a
* reference to the same IntegratorSingleStageMetaData object. Since the OptimizerMetaData
* explicitly needs an IntegratorSingleStageMetaData object but the integrator needs a general
* IntegratorMetaData object, they cannot share the pointer using shared_ptr. Thus the integrator
* receives a simple multi stage object. The Mutlistage object and the OptimizerMetaData however
* share the pointer to the single stage object.
* @param[in] stage IntegratorSingleStageMetaData pointer representing the single stage
* @return pointer to the created IntegtratorMultiStageMetaData object
*/
IntegratorMultiStageMetaData::Ptr IntegratorMetaDataFactory::
                              createIntegratorMultiStageMetaDataWithSingleStage
                              (const IntegratorSingleStageMetaData::Ptr &stage)const
{
  std::vector<Mapping::Ptr> dummyMapping;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages;
  stages.push_back(stage);
  IntegratorMultiStageMetaData::Ptr imsmd (new IntegratorMultiStageMetaData(dummyMapping, stages));
  imsmd->setLogger(m_logger);
  return imsmd;
}

/**
* @brief factory method for creating an IntegratorMSMetaData2ndOrder object using a single
*        IntegratorSSMetaData2ndOrder pointer
*
* For optimization of a single stage problem the integrator and the OptimizerMetaData both need a
* reference to the same IntegratorSingleStageMetaData object. Since the OptimizerMetaData
* explicitly needs a IntegratorSingleStageMetaData object but the integrator needs a general
* IntegratorMetaData object, they cannot share the pointer using shared_ptr. Thus the integrator
* receives a simple multi stage object. The Mutlistage object and the OptimizerMetaData however
* share the pointer to the single stage object.
* @param[in] stage IntegratorSingleStageMetaData pointer representing the single stage
* @return pointer to the created IntegtratorMultiStageMetaData object
*/
IntegratorMSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::
                              createIntegratorMSMetaData2ndOrderWithSingleStage
                              (const IntegratorSSMetaData2ndOrder::Ptr &stage)const
{
  std::vector<Mapping::Ptr> dummyMapping;
  std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages;
  stages.push_back(stage);
  IntegratorMSMetaData2ndOrder::Ptr imsmd2nd (new IntegratorMSMetaData2ndOrder(dummyMapping, stages));
  imsmd2nd->setLogger(m_logger);
  return imsmd2nd;
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object
*        using standard factoryfor creating Mappings
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed
*                  to create the IntegratorMultiStageMetaData object
* @return pointer to IntegratorMultiStageMetaData
*/
IntegratorMultiStageMetaData::Ptr IntegratorMetaDataFactory::
                              createIntegratorMultiStageMetaData
                              (const IntegratorMetaDataInput &input)
{
  MappingFactory mappingFactory;
  mappingFactory.setLogger(m_logger);
  return createIntegratorMultiStageMetaData(input, mappingFactory);
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object
*        using standard factory for creating Mappings
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed
*                  to create the IntegratorMultiStageMetaData object
* @return pointer to IntegratorMultiStageMetaData
*/
IntegratorMSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::
                              createIntegratorMSMetaData2ndOrder
                              (const IntegratorMetaDataInput &input)
{
  MappingFactory mappingFactory;
  mappingFactory.setLogger(m_logger);
  return createIntegratorMSMetaData2ndOrder(input, mappingFactory);
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object
*        using a given vector of IntegratorSingleStageMetaData pointers
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed
*              to create the IntegratorMultiStageMetaData object
* @param[in] stages vector of IntegratorSingleStageMetaData pointers used to create
*               the IntegratorMultiStageMetaData object
* @return pointer to the created IntegratorMultiStageMetaData object
* @sa IntegratorMetaDataFactory::createIntegratorSingleStageMetaDataVector
*/
IntegratorMultiStageMetaData::Ptr IntegratorMetaDataFactory::createIntegratorMultiStageMetaData
                              (const IntegratorMetaDataInput &input,
                               const std::vector<IntegratorSingleStageMetaData::Ptr> &stages)const
{
  MappingFactory mappingFactory;
  mappingFactory.setLogger(m_logger);
  std::vector<Mapping::Ptr> mappings = mappingFactory.createMappingVector(input.mappings);
  return createIntegratorMultiStageMetaData(mappings, stages);
}

/**
* @brief factory method for creating an IntegratorMSMetaData2ndOrder object
*        using a given vector of IntegratorSingleStageMetaData pointers
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed
*                  to create the IntegratorMultiStageMetaData object
* @param[in] stages vector of IntegratorSSMetaData2ndOrder pointers used to create
*                   the IntegratorMSMetaData2ndOrder object
* @return pointer to the created IntegratorMSMetaData2ndOrder object
* @sa IntegratorMetaDataFactory::createIntegratorSingleStageMetaDataVector
*/
IntegratorMSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::createIntegratorMSMetaData2ndOrder
                              (const IntegratorMetaDataInput &input,
                               const std::vector<IntegratorSSMetaData2ndOrder::Ptr> &stages)const
{
  MappingFactory mappingFactory;
  mappingFactory.setLogger(m_logger);
  std::vector<Mapping::Ptr> mappings = mappingFactory.createMappingVector(input.mappings);
  return createIntegratorMSMetaData2ndOrder(mappings, stages);
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed
*                  to create the IntegratorMultiStageMetaData object
* @param[in] mappingFactory factory used to create the mappings
* @param[in] singleStageFactory factory used to create the IntegratorSingleStageMetaData
*                               objects representing the stages
* @return pointer to the created IntegratorMultiStageMetaData object
*/
IntegratorMultiStageMetaData::Ptr IntegratorMetaDataFactory::
                              createIntegratorMultiStageMetaData
                              (const struct IntegratorMetaDataInput &input,
                               const MappingFactory &mappingFactory)
{
  std::vector<Mapping::Ptr> mappings = mappingFactory.createMappingVector(input.mappings);
  std::vector<IntegratorSingleStageMetaData::Ptr> stages;
  stages = createIntegratorSingleStageMetaDataVector(input.stages);
  return createIntegratorMultiStageMetaData(mappings, stages);
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object
*
* @param[in] input IntegratorMetaDataInput struct containing all information needed
*                  to create the IntegratorMultiStageMetaData object
* @param[in] mappingFactory factory used to create the mappings
* @param[in] singleStageFactory factory used to create the IntegratorSingleStageMetaData
*                               objects representing the stages
* @return pointer to the created IntegratorMultiStageMetaData object
*/
IntegratorMSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::
                              createIntegratorMSMetaData2ndOrder
                              (const struct IntegratorMetaDataInput &input,
                               const MappingFactory &mappingFactory)
{
  std::vector<Mapping::Ptr> mappings = mappingFactory.createMappingVector(input.mappings);
  std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages;
  stages = createIntegratorSSMetaData2ndOrderVector(input.stages);
  return createIntegratorMSMetaData2ndOrder(mappings, stages);
}

/**
* @brief factory method for creating an IntegratorMultiStageMetaData object
*
* @param[in] mappings vector of Mapping pointer needed for the creation of the multi stage object
* @param[in] stages vector of IntegratorSingleStageMetaData pointer needed for the
*                   creation of the multi stage object
* @return pointer to the created IntegratorMultiStageMetaData object
*/
IntegratorMultiStageMetaData::Ptr IntegratorMetaDataFactory::
                              createIntegratorMultiStageMetaData
                              (const std::vector<Mapping::Ptr> &mappings,
                               const std::vector<IntegratorSingleStageMetaData::Ptr> &stages) const
{
  assert(mappings.size() == (stages.size() - 1));
  IntegratorMultiStageMetaData::Ptr imsmd (new IntegratorMultiStageMetaData(mappings, stages));
  imsmd->setLogger(m_logger);
  return imsmd;
}

/**
* @brief factory method for creating an IntegratorMSMetaData2ndOrder object
*
* @param[in] mappings vector of Mapping pointer needed for the creation of the multi stage object
* @param[in] stages vector of IntegratorSSMetaData2ndOrderPtr pointer needed for the
*                   creation of the multi stage object
* @return pointer to the created IntegratorMSMetaData2ndOrder object
*/
IntegratorMSMetaData2ndOrder::Ptr IntegratorMetaDataFactory::
                              createIntegratorMSMetaData2ndOrder
                              (const std::vector<Mapping::Ptr> &mappings,
                               const std::vector<IntegratorSSMetaData2ndOrder::Ptr> &stages) const
{
  assert(mappings.size() == (stages.size() - 1));
  IntegratorMSMetaData2ndOrder::Ptr imsmd2nd (new IntegratorMSMetaData2ndOrder(mappings, stages));
  imsmd2nd->setLogger(m_logger);
  return imsmd2nd;
}

/**
* @brief returns m_problemGrid attribute
*
*/
std::vector<std::vector<double> > IntegratorMetaDataFactory::getProblemGrid() const
{
  return m_problemGrid;
}

/**
* @brief sets the m_problemGrid vector
*
* @param[in] controls vector
* @param[in] duration value
* @param[in] user grid vector
*/
void IntegratorMetaDataFactory::setProblemStageGrid(const std::vector<Control> &controls,
                                                    const double duration,
                                                    const vector<double> &userGrid)
{
  if(m_problemGrid.empty()){
    m_problemGrid.resize(1);
  }
  // calculate grid for path constraints
  std::set<double> constraintGrid;

  for(unsigned i=0; i<controls.size(); i++){
    std::vector<double> controlGrid = controls[i].getControlGridUnscaled();
    constraintGrid.insert(controlGrid.begin(), controlGrid.end());
  }

  m_problemGrid.front().assign(constraintGrid.begin(), constraintGrid.end());

  //constraint grid is scaled
  for(unsigned i=0; i<m_problemGrid.front().size(); i++){
    m_problemGrid.front()[i] /= duration;
  }
  vector<double> userGridCopy = userGrid;
  m_problemGrid.front().insert(m_problemGrid.front().end(), userGridCopy.begin(), userGridCopy.end() );
  sort(m_problemGrid.front().begin(), m_problemGrid.front().end());

  m_problemGrid.front().erase( 
    unique( m_problemGrid.front().begin(),m_problemGrid.front().end() ),
    m_problemGrid.front().end() );

  // delete close entries
  vector<double> manipulate = m_problemGrid.front();
  int wki = 0;
  for(int i=0; i<int(m_problemGrid.front().size())-1; i++){
    double compare = m_problemGrid.front()[i+1] - m_problemGrid.front()[i];
    if(compare < EPSILON){
      vector<double>::iterator it = manipulate.begin();
      advance(it, wki + 1);
      manipulate.erase(it);
    }else{
      wki++;
    }
  }
  m_problemGrid.front() = manipulate;
}


/**
* @brief factory method for creating a StatePointConstraint object
*
* @param[in] input ConstraintInput struct containing all information needed for creating
*                  a Constraint object
* @return copy of the created StatePointConstraint object
*/
StatePointConstraint ConstraintFactory::createConstraint(const ConstraintInput &input) const
{
  StatePointConstraint c;
  c.setEsoIndex(input.esoIndex);
  c.setLowerBound(input.lowerBound);
  c.setUpperBound(input.upperBound);
  c.setTime(input.timePoint);
  if(input.type == ConstraintInput::ENDPOINT){
    c.setTime(1.0);
  }
  c.setLagrangeMultiplier(input.lagrangeMultiplier);
  c.setLogger(m_logger);
  return c;
}

/**
* @brief factory method for creating a vector of StatePointConstraint objects
*
* @param[in] inputVector vector of input information structs to create the StatePointConstraint objects
* @return vector of created StatePointConstraints
*/
std::vector<StatePointConstraint> ConstraintFactory::createConstraintVector
                              (const std::vector<ConstraintInput> &inputVector,
                               const std::vector<double> &constraintGrid) const
{
  unsigned numConstraints = inputVector.size();
  std::vector<ConstraintInput> statePointConstraintVector;

  for(unsigned i=0; i<numConstraints; i++){
    if(inputVector[i].type == ConstraintInput::PATH){
      std::vector<ConstraintInput> pathConstraints(constraintGrid.size());
      std::vector<double> lagrangeMultiplierVector = inputVector[i].lagrangeMultiplierVector;
      if(inputVector[i].excludeZero && lagrangeMultiplierVector.size() < constraintGrid.size()){
        lagrangeMultiplierVector.insert(lagrangeMultiplierVector.begin(), 0.0);
      }
      for(unsigned j=0; j<constraintGrid.size(); j++){
        pathConstraints[j] = inputVector[i];
        pathConstraints[j].timePoint = constraintGrid[j];
        if(!inputVector[i].lagrangeMultiplierVector.empty()){
          assert(j < lagrangeMultiplierVector.size());
          pathConstraints[j].lagrangeMultiplier = lagrangeMultiplierVector[j];
        }
      }
       if(inputVector[i].excludeZero){
        pathConstraints.erase(pathConstraints.begin());
      }
      statePointConstraintVector.insert(statePointConstraintVector.end(),
                                        pathConstraints.begin(),
                                        pathConstraints.end());
    }
    else{
      statePointConstraintVector.push_back(inputVector[i]);
      if(statePointConstraintVector.back().type == ConstraintInput::ENDPOINT){
        // time point must be set correctly for removeDoubleConstraints function
        statePointConstraintVector.back().timePoint = 1.0;
      }
    }
  }

  removeDoubleConstraints(statePointConstraintVector);

  numConstraints = statePointConstraintVector.size();

  std::vector<StatePointConstraint> constraints(numConstraints);
  for(unsigned i=0; i<numConstraints; i++){
    constraints[i] = createConstraint(statePointConstraintVector[i]);
  }
  return constraints;
}


/**
* @brief merge constraints with multiple occurrence
*
* The constraint vector may contain (by defining path and point constraints for
* the same variable mainly) constraints that have the same time point and the
* same eso index. In that case the constraints are merged into one point
* constraint using the strictest bounds.
* @param[in,out] constraints ConstraintInput vector in which the constraints with
*                multiple occurrence are merged
*/
void ConstraintFactory::removeDoubleConstraints(
                        std::vector<FactoryInput::ConstraintInput> &constraints) const
{
  for(unsigned i=0; i<constraints.size(); i++){
    unsigned j=i+1;
    while(j<constraints.size()){
      if(constraints[i].esoIndex == constraints[j].esoIndex){
        double distance = abs(constraints[i].timePoint - constraints[j].timePoint);
        if(distance < EPSILON){
          // make sure, that a path constraint remains
          if(constraints[j].type == ConstraintInput::PATH){
            FactoryInput::ConstraintInput temp = constraints[j];
            constraints[j] = constraints[i];
            constraints[i] = temp;
          }
          constraints[i].lowerBound = max(constraints[i].lowerBound, constraints[j].lowerBound);
          constraints[i].upperBound = min(constraints[i].upperBound, constraints[j].upperBound);
          // take the LagrangeMultiplier with the biggest absolute value
          // at this point the path constraint is decomposited into point constraints
          if(fabs(constraints[j].lagrangeMultiplier) > fabs(constraints[i].lagrangeMultiplier)){
            constraints[i].lagrangeMultiplier = constraints[j].lagrangeMultiplier;
          }
          constraints.erase(constraints.begin() + j);
        }//if distance < EPSILON
        else{
          //increment if nothing has been erased
          j++;
        }
      }//if constraints[i].esoIndex == constraints[j].esoIndex
      else{
        j++;
      }
    }
  }
}

std::vector<GlobalConstraint::Ptr> ConstraintFactory::createGlobalConstraintVector
                      (const std::vector<FactoryInput::ConstraintInput> &inputVector,
                       const std::vector<IntegratorSingleStageMetaData::Ptr> &stages)const
{
  std::vector<GlobalConstraint::Ptr> globalConstraints;
  for(unsigned i=0; i<inputVector.size(); i++){
    std::vector<GlobalConstraint::Ptr> multipleShootingConstraintVector;
    switch(inputVector[i].type){
      case FactoryInput::ConstraintInput::MULTIPLE_SHOOTING:
         multipleShootingConstraintVector =
              createMultipleShootingConstraintVector(inputVector[i],stages);
        globalConstraints.insert(globalConstraints.end(),
                                 multipleShootingConstraintVector.begin(),
                                 multipleShootingConstraintVector.end());
        break;
      default:
        assert(false);
    }
  }
  return globalConstraints;
}


std::vector<GlobalConstraint::Ptr>  ConstraintFactory::createMultipleShootingConstraintVector
                      (const FactoryInput::ConstraintInput &input,
                       const std::vector<IntegratorSingleStageMetaData::Ptr> &stages)const
{
  assert(input.type == FactoryInput::ConstraintInput::MULTIPLE_SHOOTING);
  std::vector<GlobalConstraint::Ptr> multipleShootingConstraintVector;
  assert(input.lagrangeMultiplierVector.empty() ||
         input.lagrangeMultiplierVector.size() == stages.size()-1);
  for(unsigned i=0; i<stages.size()-1; i++){
    MultipleShootingConstraint *multipleShootingConstraint = new MultipleShootingConstraint();
    multipleShootingConstraint->setEsoIndex(input.esoIndex);
    if(!input.lagrangeMultiplierVector.empty()){
      multipleShootingConstraint->setLagrangeMultiplier(input.lagrangeMultiplierVector[i]);
    }
    multipleShootingConstraint->setLogger(m_logger);
    std::vector<InitialValueParameter::Ptr> initialValues;
    stages[i+1]->getInitialValueParameter(initialValues);
    bool initialValueParameterFound = false;
    for(unsigned j=0; j<initialValues.size(); j++){
      if(initialValues[j]->getEsoIndex() == int(input.esoIndex)){
        assert(initialValues[j]->getIsDecisionVariable() == true);
        multipleShootingConstraint->setInitialValueParameterNextStage(initialValues[j]);
        initialValueParameterFound = true;
        break;
      }
    }
    assert(initialValueParameterFound);
    multipleShootingConstraint->setStageIndex(i);
    GlobalConstraint::Ptr multipleShootingConstraintPtr(multipleShootingConstraint);
    multipleShootingConstraintVector.push_back(multipleShootingConstraintPtr);
  }
  return multipleShootingConstraintVector;
}

/**
* @brief factory method for creating an OptimizerSingleStageMetaData object
*        using the standard factories for creating StatePointConstraint objects
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed for creating
*                  an OptimizerSingleStageMetaData object
* @param[in] integratorMetaData IntegratorMetaData pointer used to create the OptimizerMetaData
* @return pointer to the created OptimizerMetaData object
*/
OptimizerSingleStageMetaData::Ptr OptimizerMetaDataFactory::createOptimizerSingleStageMetaData(
                                    const OptimizerMetaDataInput &input,
                                    const IntegratorSingleStageMetaData::Ptr &integratorMetaData,
                                    const std::vector<double> &constraintGrid) const
{
  ConstraintFactory constFactory;
  return createOptimizerSingleStageMetaData(input,
                                            constFactory,
                                            integratorMetaData,
                                            constraintGrid);
}

/**
* @brief factory method for creating an OptimizerSSMetaData2ndOrder object
*        using the standard factories for creating StatePointConstraint objects
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed for creating
*                  an OptimizerSingleStageMetaData object
* @param[in] integratorMetaData IntegratorMetaData pointer used to create the OptimizerMetaData
* @return pointer to the created OptimizerMetaData object
*/
OptimizerSSMetaData2ndOrder::Ptr OptimizerMetaDataFactory::createOptimizerSSMetaData2ndOrder(
                                        const OptimizerMetaDataInput &input,
                                        const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData,
                                        const std::vector<double> &constraintGrid) const
{
  ConstraintFactory constFactory;
  constFactory.setLogger(m_logger);
  return createOptimizerSSMetaData2ndOrder(input,
                                           constFactory,
                                           integratorMetaData,
                                           constraintGrid);
}


/**
* @brief factory method for creating an OptimizerSingleStageMetaData object using given
*        factories for creating StatePointConstraint and IntegratorMetaData objects
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed for creating
*                  the OptimizerSingleStageMetaData object
* @param[in] constFactory factory used to create the StatePointConstraint objects needed for creating
*                         the OptimizerSingleStageMetaData object
* @param[in] integratorMetaData pointer to an IntegratorMetaData object needed for creating
*                               the OptimizerSingleStageMetaData object (created by an
*                               corresponding integrator)
* @return pointer to the created OptimizerSingleStageMetaData object
*/
OptimizerSingleStageMetaData::Ptr OptimizerMetaDataFactory::createOptimizerSingleStageMetaData(
                                    const OptimizerMetaDataInput &input,
                                    const ConstraintFactory &constFactory,
                                    const IntegratorSingleStageMetaData::Ptr &integratorMetaData,
                                    const std::vector<double> &constraintGrid) const
{
  std::vector<StatePointConstraint> constraints;
  constraints = constFactory.createConstraintVector(input.constraints, constraintGrid);

  StatePointConstraint objective = constFactory.createConstraint(input.objective);
  if(input.objective.lagrangeMultiplier <= 0.0){
    objective.setLagrangeMultiplier(1.0);
  }

  return createOptimizerSingleStageMetaData(constraints,
                                            objective,
                                            integratorMetaData);
}

/**
* @brief factory method for creating an OptimizerSSMetaData2ndOrder object using given
*        factories for creating StatePointConstraint and IntegratorMetaData objects
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed for creating
*                  the OptimizerSingleStageMetaData object
* @param[in] constFactory factory used to create the StatePointConstraint objects needed for creating
*                         the OptimizerSingleStageMetaData object
* @param[in] integratorMetaData pointer to an IntegratorMetaData object needed for creating
*                               the OptimizerSSMetaData2ndOrder object (created by an
*                               corresponding integrator)
* @return pointer to the created OptimizerSSMetaData2ndOrder object
*/
OptimizerSSMetaData2ndOrder::Ptr OptimizerMetaDataFactory::createOptimizerSSMetaData2ndOrder(
                                            const OptimizerMetaDataInput &input,
                                            const ConstraintFactory &constFactory,
                                            const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData,
                                            const std::vector<double> &constraintGrid) const
{
  std::vector<StatePointConstraint> constraints;
  constraints = constFactory.createConstraintVector(input.constraints, constraintGrid);

  StatePointConstraint objective = constFactory.createConstraint(input.objective);

  return createOptimizerSSMetaData2ndOrder(constraints, objective, integratorMetaData);
}

/**
* @brief factory method for creating an OptimizerSingleStageMetaData object
*
* @param[in] constraints vector of StatePointConstraint objects to be set in the object
* @param[in] objective to be set in the object
* @param[in] integratorMetaData IntegratorMetaData object used to create the object
* @return pointer to the created OptimizerSingleStageMetaData object
*/
OptimizerSingleStageMetaData::Ptr OptimizerMetaDataFactory::createOptimizerSingleStageMetaData(
                                  const std::vector<StatePointConstraint> &constraints,
                                  const StatePointConstraint &objective,
                                  const IntegratorSingleStageMetaData::Ptr &integratorMetaData)const
{
  OptimizerSingleStageMetaData::Ptr optMD (new OptimizerSingleStageMetaData(integratorMetaData));
  optMD->setConstraints(constraints);
  optMD->setObjective(objective);
  optMD->calculateOptimizerProblemDimensions();
  optMD->setLogger(m_logger);
  return optMD;
}

/**
* @brief factory method for creating an OptimizerSSMetaData2ndOrder object
*
* @param[in] constraints vector of StatePointConstraint objects to be set in the object
* @param[in] objective to be set in the object
* @param[in] integratorMetaData IntegratorMetaData object used to create the object
* @return pointer to the created OptimizerSingleStageMetaData object
*/
OptimizerSSMetaData2ndOrder::Ptr OptimizerMetaDataFactory::createOptimizerSSMetaData2ndOrder(
                                  const std::vector<StatePointConstraint> &constraints,
                                  const StatePointConstraint &objective,
                                  const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData)const
{
  OptimizerSSMetaData2ndOrder::Ptr optMD (new OptimizerSSMetaData2ndOrder(integratorMetaData));
  optMD->setConstraints(constraints);
  optMD->setObjective(objective);
  optMD->calculateOptimizerProblemDimensions();
  optMD->setLogger(m_logger);
  return optMD;
}

/**
* @brief factory method for creating an OptimizerMultiStageMetaData object
*        using standard factories for creating the single stages and the
*        stage independent constraints
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed
*              to create the OptimizerMultiStageMetaData object
* @param[in] integratorMetaDataStages IntegratorSingleStageMetaData pointer used to
*        create the OptimizerSingleStageMetaData objects
* @param[in] intMSMD IntegratorMultiStageMetaData object used to create the
*                OptimizerMultiStageMetaData object
* @return pointer to the created OptimizerMultiStageMetaData object
*/
OptimizerMultiStageMetaData::Ptr OptimizerMetaDataFactory::createOptimizerMultiStageMetaData(
                    const OptimizerMetaDataInput &input,
                    const std::vector<IntegratorSingleStageMetaData::Ptr> &integratorMetaDataStages,
                    const IntegratorMultiStageMetaData::Ptr intMSMD,
                    const std::vector<std::vector<double> > &constraintGrids) const
{
  OptimizerMetaDataFactory stageFactory;
  stageFactory.setLogger(m_logger);
  ConstraintFactory constraintFactory;
  constraintFactory.setLogger(m_logger);
  return createOptimizerMultiStageMetaData(input,
                                           stageFactory,
                                           constraintFactory,
                                           integratorMetaDataStages,
                                           intMSMD,
                                           constraintGrids);
}

/**
* @brief factory method for creating an OptimizerMSMetaData2ndOrder object
*        using standard factories for creating the single stages and the
*        stage independent constraints
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed
*              to create the OptimizerMultiStageMetaData object
* @param[in] integratorMetaDataStages IntegratorSingleStageMetaData pointer used to
*        create the OptimizerSingleStageMetaData objects
* @param[in] intMSMD IntegratorMultiStageMetaData object used to create the
*                OptimizerMSMetaData2ndOrder object
* @return pointer to the created OptimizerMSMetaData2ndOrder object
*/
OptimizerMSMetaData2ndOrder::Ptr OptimizerMetaDataFactory::createOptimizerMSMetaData2ndOrder(
                  const OptimizerMetaDataInput &input,
                  const std::vector<IntegratorSSMetaData2ndOrder::Ptr> &integratorMetaDataStages,
                  const IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                  const std::vector<std::vector<double> > &constraintGrids) const
{
  OptimizerMetaDataFactory stageFactory;
  stageFactory.setLogger(m_logger);
  ConstraintFactory constraintFactory;
  constraintFactory.setLogger(m_logger);
  return createOptimizerMSMetaData2ndOrder(input,
                                           stageFactory,
                                           constraintFactory,
                                           integratorMetaDataStages,
                                           intMSMD,
                                           constraintGrids);
}

/**
* @brief factory method for creating an OptimizerMultiStageMetaData object
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed
*                  to create the OptimizerMultiStageMetaData object
* @param[in] stageFactory factory used to create the OptimizerSingleStageMetaData objects
* @param[in] constraintFactory factory used to create the stage independent constraints
* @param[in] integratorMetaDataStages IntegratorSingleStageMetaData pointer used to
*                                     create the OptimizerSingleStageMetaData objects
* @param[in] intMSMD IntegratorMultiStageMetaData object used to create the
*                    OptimizerMultiStageMetaData object
* @return pointer to the created OptimizerMultiStageMetaData object
*/
OptimizerMultiStageMetaData::Ptr OptimizerMetaDataFactory::createOptimizerMultiStageMetaData(
                    const OptimizerMetaDataInput &input,
                    const OptimizerMetaDataFactory &stageFactory,
                    const ConstraintFactory &constraintFactory,
                    const std::vector<IntegratorSingleStageMetaData::Ptr> integratorMetaDataStages,
                    const IntegratorMultiStageMetaData::Ptr intMSMD,
                    const std::vector<std::vector<double> > &constraintGrids)const
{
  assert(input.stages.size() == integratorMetaDataStages.size());
  std::vector<OptimizerSingleStageMetaData::Ptr> stages(input.stages.size());
  for(unsigned i=0; i<input.stages.size(); i++){
    OptimizerSingleStageMetaData::Ptr stage(stageFactory.createOptimizerSingleStageMetaData(
                                               input.stages[i],
                                               integratorMetaDataStages[i],
                                               constraintGrids[i]));
    stages[i] = stage;
  }

  std::vector<bool> treatObjective = input.treatObjective;
  OptimizerMultiStageMetaData::Ptr createdObject;
  createdObject = createOptimizerMultiStageMetaData(stages,
                                                    intMSMD,
                                                    treatObjective);
  std::vector<GlobalConstraint::Ptr> globalConstraints =
                constraintFactory.createGlobalConstraintVector(input.constraints,
                                                               integratorMetaDataStages);
  createdObject->setGlobalConstraints(globalConstraints);
  if(input.hasTotalEndTime){
    createdObject->setTotalTimeConstraint(input.totalEndTimeLowerBound,
                                          input.totalEndTimeUpperBound,
                                          input.constantDurationsValue);
  }
  return createdObject;
}

/**
* @brief factory method for creating an OptimizerMSMetaData2ndOrder object
*
* @param[in] input OptimizerMetaDataInput struct containing all information needed
*                  to create the OptimizerMSMetaData2ndOrder object
* @param[in] stageFactory factory used to create the OptimizerSSMetaData2ndOrder objects
* @param[in] constraintFactory factory used to create the stage independent constraints
* @param[in] integratorMetaDataStages IntegratorSSMetaData2ndOrder pointer used to
*                                     create the OptimizerSSMetaData2ndOrder objects
* @param[in] intMSMD IntegratorMSMetaData2ndOrder object used to create the
*                    OptimizerMSMetaData2ndOrder object
* @return pointer to the created OptimizerMSMetaData2ndOrder object
*/
OptimizerMSMetaData2ndOrder::Ptr OptimizerMetaDataFactory::createOptimizerMSMetaData2ndOrder(
                    const OptimizerMetaDataInput &input,
                    const OptimizerMetaDataFactory &stageFactory,
                    const ConstraintFactory &constraintFactory,
                    const std::vector<IntegratorSSMetaData2ndOrder::Ptr> integratorMetaDataStages,
                    const IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                    const std::vector<std::vector<double> > &constraintGrids)const
{
  assert(input.stages.size() == integratorMetaDataStages.size());
  std::vector<OptimizerSingleStageMetaData::Ptr> stages(input.stages.size());
  for(unsigned i=0; i<input.stages.size(); i++){
    OptimizerSingleStageMetaData::Ptr stage(stageFactory.createOptimizerSSMetaData2ndOrder(
                                               input.stages[i],
                                               integratorMetaDataStages[i],
                                               constraintGrids[i]));
    stages[i] = stage;
  }

  std::vector<bool> treatObjective = input.treatObjective;
  OptimizerMSMetaData2ndOrder::Ptr createdObject;
  createdObject = createOptimizerMSMetaData2ndOrder(stages,
                                                    intMSMD,
                                                    treatObjective);
  std::vector<IntegratorSingleStageMetaData::Ptr> issmd;
  issmd.assign(integratorMetaDataStages.begin(), integratorMetaDataStages.end());

  std::vector<GlobalConstraint::Ptr> globalConstraints =
                constraintFactory.createGlobalConstraintVector(input.constraints,
                                                               issmd);
  createdObject->setGlobalConstraints(globalConstraints);
  if(input.hasTotalEndTime){
    createdObject->setTotalTimeConstraint(input.totalEndTimeLowerBound,
                                          input.totalEndTimeUpperBound,
                                          input.constantDurationsValue);
  }
  return createdObject;
}

/**
* @brief create a pointer to an OptimizerMultiStageMetaData object
*
* @param[in] stages OptimizerSingleStageMetaData objects used to create the object
* @param[in] intMSMD IntegratorMultiStageMetaData used to create the object
* @param[in] treatObjective vector of flags
*                           (true if objective of the corresponding stage is taken into account)
* @return pointer to the created OptimizerMultiStageMetaData object
*/
OptimizerMultiStageMetaData::Ptr OptimizerMetaDataFactory::createOptimizerMultiStageMetaData(
                             const std::vector<OptimizerSingleStageMetaData::Ptr> &stages,
                             const IntegratorMultiStageMetaData::Ptr intMSMD,
                             const std::vector<bool> &treatObjective)const
{
  assert(stages.size() == intMSMD->getNumStages());
  assert(stages.size() == treatObjective.size());
  OptimizerMultiStageMetaData::Ptr omsmd (new OptimizerMultiStageMetaData(stages,
                                                                       intMSMD,
                                                                       treatObjective));
  omsmd->setLogger(m_logger);
  return omsmd;
}

/**
* @brief create a pointer to an OptimizerMSMetaData2ndOrder object
*
* @param[in] stages OptimizerSingleStageMetaData objects used to create the object
* @param[in] intMSMD IntegratorMultiStageMetaData used to create the object
* @param[in] treatObjective vector of flags
*                           (true if objective of the corresponding stage is taken into account)
* @return pointer to the created OptimizerMultiStageMetaData object
*/
OptimizerMSMetaData2ndOrder::Ptr OptimizerMetaDataFactory::createOptimizerMSMetaData2ndOrder(
                             const std::vector<OptimizerSingleStageMetaData::Ptr> &stages,
                             const IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                             const std::vector<bool> &treatObjective)const
{
  assert(stages.size() == intMSMD->getNumStages());
  assert(stages.size() == treatObjective.size());
  OptimizerMSMetaData2ndOrder::Ptr omsmd2nd (new OptimizerMSMetaData2ndOrder(stages,
                                                                          intMSMD,
                                                                          treatObjective));
  omsmd2nd->setLogger(m_logger);
  return omsmd2nd;
}

/**
* @brief factory method for creating a Mapping object
*
* @param[in] input MappingInput struct containing all information needed for creating a Mapping object
* @todo include other mappings when implemented
* @return pointer to the created Mapping object
*/
Mapping::Ptr MappingFactory::createMapping(const MappingInput &input)const
{
  Mapping::Ptr createdMapping;
  //createdMapping = NULL;
  switch(input.type){
    case MappingInput::SINGLE_SHOOTING:
      createdMapping = createSingleShootingMapping(input);
      break;
    case MappingInput::MULTIPLE_SHOOTING:
      createdMapping = createMultipleShootingMapping(input);
      break;
    default:
      assert(false);
  }

  return createdMapping;
}

/**
* @brief factory method for creating a vector of Mapping object pointers
* @param[in] inputVector vector containing the mapping information
* @return vector of mapping pointers
*/
std::vector<Mapping::Ptr> MappingFactory::createMappingVector
                                 (const std::vector<MappingInput> &inputVector)const
{
  std::vector<Mapping::Ptr> mappings(inputVector.size());
  for(unsigned i=0; i<inputVector.size(); i++){
    mappings[i] = Mapping::Ptr(createMapping(inputVector[i]));
  }
  return mappings;
}

/**
* @brief factory method for creating a SingleShootingMapping object
*
* @param[in] input MappingInput struct containing all information needed for
*              creating a SingleShootingMapping object
* @return mapping object with single shooting mapping
*/
SingleShootingMapping::Ptr MappingFactory::createSingleShootingMapping
                                            (const MappingInput &input)const
{
  SingleShootingMapping::Ptr createdMapping( new SingleShootingMapping());

  createdMapping->setIndexMaps(input.varIndexMap, input.eqnIndexMap);
  createdMapping->setLogger(m_logger);
  return createdMapping;
}

MultipleShootingMapping::Ptr MappingFactory::createMultipleShootingMapping
                                            (const MappingInput &input) const
{
  MultipleShootingMapping::Ptr createdMapping(new MultipleShootingMapping());
  
  createdMapping->setLogger(m_logger);
  return createdMapping;
}

