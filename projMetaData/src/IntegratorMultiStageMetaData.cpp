/**
* @file IntegratorMultiStageMetaData.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData - Part of DyOS                                              \n
* =====================================================================\n
* This file contains the memebr function definitions of the classes    \n
* IntegratorMultiStageMetaData and IntegratorMSMetaData2ndOrder        \n
* =====================================================================\n
* @author Tjalf Hoffmann, Fady Assassa
* @date 20.09.2012
*/

#include "IntegratorMultiStageMetaData.hpp"
#include "Parameter.hpp"
#include <Mapping.hpp>

#include <assert.h>
#include <algorithm>

/**
* @brief standard constructor for derived dummy classes
*/
IntegratorMultiStageMetaData::IntegratorMultiStageMetaData()
{
  m_currentStageIndex = 0;
  m_isInitialized = false;
  m_isInitializedBackwards = false;
}
/**
* @brief constructor that creates a multistage object from multiple mappings and single stages
*
* @param[in] mappings a vector that contains the maps between the stages. The size is numStages - 1
* @param[in] stages a vector that contains the IntegratorSingleStageMetaData
*/
IntegratorMultiStageMetaData::IntegratorMultiStageMetaData(std::vector<Mapping::Ptr> mappings,
                                                           std::vector<IntegratorSingleStageMetaData::Ptr> stages)
{
  m_mapping = mappings;
  m_stages = stages;
  m_currentStageIndex = 0;
  m_isInitialized = false;
  m_isInitializedBackwards = false;
  m_intOptData.resize(stages.size());
}

/**
* @brief standard destructor for derived dummy classes
*/
IntegratorMultiStageMetaData::~IntegratorMultiStageMetaData()
{
}

void IntegratorMultiStageMetaData::resetGolbalDurationParameter(void)
{
   m_stages[m_currentStageIndex]->resetGolbalDurationParameter();
}
/**
* @copydoc IntegratorMetaData::initializeForFirstIntegration
*/
void IntegratorMultiStageMetaData::initializeForFirstIntegration()
{
  if(m_isInitialized){
    m_currentStageIndex = m_currentStageIndex + 1;
  }
  else{
    m_isInitialized = true;
    m_currentStageIndex = 0;
  }

  //reset stage index, if it is out of bounds
  if(m_currentStageIndex >= m_stages.size()){
    m_currentStageIndex = 0;
    m_mappingSensitivities.clear();
  }

  // We map the sensitivities and the state from the old stage to the current stage
  if (m_currentStageIndex > 0){ // not required for the first stage
    // map last stage to current stage according to different rule (SS,MS,HS)
    const unsigned lastStageIndex = m_currentStageIndex - 1;
    const unsigned mappingIndex = m_currentStageIndex -1;
    m_mapping[mappingIndex]->applyStageMapping(m_stages[lastStageIndex],
                                               m_stages[m_currentStageIndex],
                                               m_mappingSensitivities);
  }
  //the initial state values of the stage are set (amongst others)
  m_stages[m_currentStageIndex]->initializeForFirstIntegration();
}

void IntegratorMultiStageMetaData::initializeMatrices()
{
  m_stages[m_currentStageIndex]->initializeMatrices();
}


/**
* @copydoc IntegratorMetaData::initializeForNextIntegration
*/
void IntegratorMultiStageMetaData::initializeForNextIntegration()
{
  m_stages[m_currentStageIndex]->initializeForNextIntegration();
}

/**
* @copydoc IntegratorMetaData::initializeForPreviousIntegrationBlock
*/
void IntegratorMultiStageMetaData::initializeForPreviousIntegrationBlock()
{
  m_stages[m_currentStageIndex]->initializeForPreviousIntegrationBlock();
}

/**
* @copydoc IntegratorMetaData::initializeForNewIntegrationStep
*/
void IntegratorMultiStageMetaData::initializeForNewIntegrationStep()
{
  m_stages[m_currentStageIndex]->initializeForNewIntegrationStep();
}

/**
* @copydoc IntegratorMetaData::initializeForNewBackwardsIntegrationStep
*/
void IntegratorMultiStageMetaData::initializeForNewBackwardsIntegrationStep()
{
  m_stages[m_currentStageIndex]->initializeForNewBackwardsIntegrationStep();
}

/**
* @copydoc IntegratorMetaData::setStartTime
*/
void IntegratorMultiStageMetaData::setStepStartTime(const double startTime)
{
  m_stages[m_currentStageIndex]->setStepStartTime(startTime);
}

/**
* @copydoc IntegratorMetaData::getStartTime
*/
double IntegratorMultiStageMetaData::getStepStartTime()const
{
  return m_stages[m_currentStageIndex]->getStepStartTime();
}

/**
* @copydoc IntegratorMetaData::setEndTime
*/
void IntegratorMultiStageMetaData::setStepEndTime(const double endTime)
{
  m_stages[m_currentStageIndex]->setStepEndTime(endTime);
}

/**
* @copydoc IntegratorMetaData::getEndTime
*/
double IntegratorMultiStageMetaData::getStepEndTime() const
{
  return m_stages[m_currentStageIndex]->getStepEndTime();
}

/**
* @copydoc IntegratorMetaData::setCurrentTime
*/
void IntegratorMultiStageMetaData::setStepCurrentTime(const double time)
{
  m_stages[m_currentStageIndex]->setStepCurrentTime(time);
}

/**
* @copydoc IntegratorMetaData::getCurrentTime
*/
double IntegratorMultiStageMetaData::getStepCurrentTime() const
{
  return m_stages[m_currentStageIndex]->getStepCurrentTime();
}

/**
* @copydoc IntegratorMetaData::getNumStages
*/
unsigned IntegratorMultiStageMetaData::getNumStages() const
{
  return m_stages.size();
}

/**
* @copydoc IntegratorMetaData::calculateSensitivitiesForward1stOrder
*/
void IntegratorMultiStageMetaData::calculateSensitivitiesForward1stOrder(
                                        const utils::Array<double> &inputSensitivities,
                                              utils::Array<double> &rhsSensitivities)
{
  // If the else part is written general, then we don't need to distinguish between the first
  // and all other stages. But for clarity, that no mapping is taking place, we include this
  // distinguishing.
  if(m_currentStageIndex == 0){
    m_stages[m_currentStageIndex]->calculateSensitivitiesForward1stOrder(inputSensitivities,
                                                                         rhsSensitivities);
  }
  else{
    const unsigned numMappingSens = m_mappingSensitivities.size();
    const unsigned numSensParameters = m_stages[m_currentStageIndex]->getNumCurrentSensitivityParameters();
    const unsigned numEquations = m_stages[m_currentStageIndex]->getGenericEso()->getNumEquations();
    const unsigned numStageSens = numSensParameters * numEquations;
    assert(inputSensitivities.getSize() == numMappingSens + numStageSens);
    assert(inputSensitivities.getSize() == rhsSensitivities.getSize());


    //non constant copy of inputSensitvities
    utils::Array<double> inputSens(inputSensitivities);

    CsCscMatrix::Ptr jacobianFstates = m_stages[m_currentStageIndex]->getJacobianFstates();
    assert(numMappingSens % numEquations == 0);
    const unsigned numOldParams = numMappingSens/numEquations;
    //for use in loop
    const double duration = m_stages[m_currentStageIndex]->getIntegrationGridDuration();

    // calculate s*F for each parameter of the old stages, while s are corresponding
    // inputMappingSensitivities and F is state Jacobian
    for(unsigned i=0; i<numOldParams; i++){
      const unsigned blockAddress = i*numEquations;
      utils::WrappingArray<double> mappingInputSens(numEquations, inputSens.getData() + blockAddress);
      utils::WrappingArray<double> mappingRhsSens(numEquations, rhsSensitivities.getData() + blockAddress);

      // DfDx is jacobianFstates
      // y is mappingInputSens
      // result is mappingRhsSens
      Parameter::getDfDxTimesY(mappingInputSens,
                               jacobianFstates->get_matrix_ptr(),
                               duration,
                               mappingRhsSens);
    }

    // call function on current stage to assemble the remaining sensitivities. They are appended at
    // the end of the sensitivity vector of the multi stage problem.
    utils::WrappingArray<double> stageInputSens(numStageSens, inputSens.getData() + numMappingSens);
    utils::WrappingArray<double> stageRhsSens(numStageSens, rhsSensitivities.getData() + numMappingSens);
    m_stages[m_currentStageIndex]->calculateSensitivitiesForward1stOrder(stageInputSens, stageRhsSens);
  }
}


/**
* @copydoc IntegratorMetaData::calculateRhsStates
*/
void IntegratorMultiStageMetaData::calculateRhsStates(const utils::Array<double> & inputStates,
                                                            utils::Array<double> &rhsStates)
{
  m_stages[m_currentStageIndex]->calculateRhsStates(inputStates, rhsStates);
}

/**
* @copydoc IntegratorMetaDAta::calculateAdjointsReverse1stOrder
*/
 void IntegratorMultiStageMetaData::calculateAdjointsReverse1stOrder
                                               (const utils::Array<double> &states,
                                                const utils::Array<double> &adjoints,
                                                      utils::Array<double> &rhsAdjoints,
                                                      utils::Array<double> &rhsDers)
{
  unsigned currentParameterIndex = 0;
  for(unsigned i=0; i<m_currentStageIndex; i++){
    currentParameterIndex += m_stages[i]->getNumSensitivityParameters();
  }
  const unsigned numSensParamsStage = m_stages[m_currentStageIndex]->getNumSensitivityParameters();
  assert(rhsDers.getSize() >= getNumSensitivityParameters());
  utils::WrappingArray<double> rhsDersStage(numSensParamsStage,
                                            rhsDers.getData() + currentParameterIndex);
  m_stages[m_currentStageIndex]->calculateAdjointsReverse1stOrder(states,
                                                                  adjoints,
                                                                  rhsAdjoints,
                                                                  rhsDersStage);
}

/**
* @copydoc IntegratorMetaData::calculateRhsAdjoints
*/
void IntegratorMultiStageMetaData::calculateRhsAdjoints(const utils::Array<double> &adjoints,
                                                              utils::Array<double> &rhsAdjoints)
{
  m_stages[m_currentStageIndex]->calculateRhsAdjoints(adjoints, rhsAdjoints);
}

void IntegratorMultiStageMetaData::calculateRhsStatesDtime(utils::Array<double> &rhsStatesDtime)
{
  //only controls of the active stage are relevant for rhsStatesDtime
  //this only works if the number of states is constant over all stages
  m_stages[m_currentStageIndex]->calculateRhsStatesDtime(rhsStatesDtime);
}

/**
* @copydoc IntegratorMetaData::setStates
*/
void IntegratorMultiStageMetaData::setStates(const utils::Array<double> &states)
{
  m_stages[m_currentStageIndex]->setStates(states);
}

void IntegratorMultiStageMetaData::storeInitialStates(const utils::Array<double> &states)
{
  m_stages[m_currentStageIndex]->storeInitialStates(states);
}

/**
* @copydoc IntegratorMetaData::getMMatrix
*/
CsCscMatrix::Ptr IntegratorMultiStageMetaData::getMMatrix(void) const
{
  return m_stages[m_currentStageIndex]->getMMatrix();
}

/**
* @copydoc IntegratorMetaData::getMMatrixCoo
*/
CsTripletMatrix::Ptr IntegratorMultiStageMetaData::getMMatrixCoo(void) const
{
  return m_stages[m_currentStageIndex]->getMMatrixCoo();
}

/**
* @copydoc IntegratorMetaData::evaluateInitialValues
*/
void IntegratorMultiStageMetaData::evaluateInitialValues(utils::Array<double> &initialStates,
                                                          utils::Array<double> &initialSensitivities)
{
  const unsigned numMappingSens = m_mappingSensitivities.size();
  const unsigned numParameters = m_stages[m_currentStageIndex]->getNumCurrentSensitivityParameters();
  const unsigned numEquations = m_stages[m_currentStageIndex]->getGenericEso()->getNumEquations();
  const unsigned numStageSens = numParameters * numEquations;
  //if no sensitivities are to be calculated, skip the copying of the mapping sensitivites
  if(numMappingSens > 0 && initialSensitivities.getSize() > 0){
    assert(initialSensitivities.getSize() == numMappingSens + numStageSens);

    //first entries are the mapping sensitivities only
    for(unsigned i=0; i<numMappingSens; i++){
      initialSensitivities[i] = m_mappingSensitivities[i];
    }
    //utils::copy(numMappingSens, m_mappingSensitivities, initialSensitivities.getData());
      // The remaining sensitivities of the initial values of the current stage are initialized.
    utils::WrappingArray<double> stageInitialSensitivities(numStageSens,
                                                           initialSensitivities.getData() + numMappingSens);
    m_stages[m_currentStageIndex]->evaluateInitialValues(initialStates, stageInitialSensitivities);
  }
  else{
    m_stages[m_currentStageIndex]->evaluateInitialValues(initialStates, initialSensitivities);
  }
  
  if(m_stages[m_currentStageIndex]->getTimeIndex() <= 1){
    addIntOptDataPoint(0);
  }
}

/**
* @copydoc IntegratorMetaData::getNumCurrentSensitivityParameters
*
* in multistage all the sensitivity parameters of all stages up to the current stage are added
*/
int IntegratorMultiStageMetaData::getNumCurrentSensitivityParameters() const
{
  const unsigned numEquations = m_stages[m_currentStageIndex]->getGenericEso()->getNumEquations();
  int numSensParameters = m_mappingSensitivities.size()/numEquations;
  numSensParameters += m_stages[m_currentStageIndex]->getNumCurrentSensitivityParameters();
  return numSensParameters;
}

void IntegratorMultiStageMetaData::getSensitivityParameters
                            (std::vector<Parameter::Ptr> &sensParams) const
{
  sensParams.clear();
  sensParams.reserve(getNumCurrentSensitivityParameters());
  for(unsigned i=0; i<m_stages.size(); i++){
    std::vector<Parameter::Ptr> stageSensParams;
    m_stages[i]->getSensitivityParameters(stageSensParams);
    sensParams.insert(sensParams.end(), stageSensParams.begin(), stageSensParams.end());
  }
}

/**
* @copydoc IntegratorMetaData::getNumIntegrationIntervals
*/
int IntegratorMultiStageMetaData::getNumIntegrationIntervals() const
{
  return m_stages[m_currentStageIndex]->getNumIntegrationIntervals();
}

/**
* @copydoc IntegratorMetaData::setAllInitialValuesFree
*/
void IntegratorMultiStageMetaData::setAllInitialValuesFree()
{
  m_stages[m_currentStageIndex]->setAllInitialValuesFree();
}

/**
* @copydoc IntegratorMetaData::getGenericEso
*/
GenericEso::Ptr IntegratorMultiStageMetaData::getGenericEso()
{
  return m_stages[m_currentStageIndex]->getGenericEso();
}

/**
* @copydoc IntegratorMetaData::getNumStateNonZeroes
*/
int IntegratorMultiStageMetaData::getNumStateNonZeroes()
{
  return m_stages[m_currentStageIndex]->getNumStateNonZeroes();
}

/**
* @copydoc IntegratorMetaData::getStageDuration
*/
double IntegratorMultiStageMetaData::getStageDuration() const
{
  return m_stages[m_currentStageIndex]->getStageDuration();
}

/**
* @copydoc IntegratorMetaData::setControlsToEso
*/
void IntegratorMultiStageMetaData::setControlsToEso()
{
  m_stages[m_currentStageIndex]->setControlsToEso();
}

/**
* @copydoc IntegratorMetaData::getNumSensitivityParameters
*/
unsigned IntegratorMultiStageMetaData::getNumSensitivityParameters() const
{
  unsigned numSensParams = 0;
  for(unsigned i=0; i<m_stages.size(); i++){
    numSensParams += m_stages[i]->getNumSensitivityParameters();
  }
  return numSensParams;
}

/**
* @copydoc IntegratorMetaData::setSensitivities
*/
void IntegratorMultiStageMetaData::setSensitivities(const utils::Array<double> &sens)
{
  // write the multi stage sensitivities first into the sensitivity vector
  const unsigned sizeMultiStageSens = m_mappingSensitivities.size();
  for(unsigned i=0; i<sizeMultiStageSens; i++){
    m_mappingSensitivities[i] = sens[i];
  }
  
  // write the sensitivities of the current stage into the stage itself
  utils::Array<double> stageSens(sens.getSize() - sizeMultiStageSens);
  for(unsigned i=0; i<stageSens.getSize(); i++){
    stageSens[i] = sens[i + sizeMultiStageSens];
  }

  m_stages[m_currentStageIndex]->setSensitivities(stageSens);

  // new data struct for multi stage with a copy of the local information on the single stage
  if(m_currentStageIndex > 0){
    addIntOptDataPoint(m_stages[m_currentStageIndex]->getTimeIndex());
  }

}

/**
* @copydoc IntegratorMetaData::setAdjointsAndLagrangeDers
*/
void IntegratorMultiStageMetaData::setAdjointsAndLagrangeDerivatives(
                                                  const utils::Array<double> &adjoints,
                                                  const utils::Array<double> &lagrangeDerivatives)
{
  int derivativeIndexForCurrentStage = 0;
  for(unsigned i=0; i<m_stages.size(); i++){
    // todo number of adjoints may differ. What are the correct adjoints to be set
    // for lower stages?

    //WrappingArray does not work because lagrangeDers are const - so make a a copy
    const int numParametersCurrentStage = m_stages[i]->getNumSensitivityParameters();
    assert(int(lagrangeDerivatives.getSize()) >=
          (derivativeIndexForCurrentStage + numParametersCurrentStage));
    utils::Array<double> lagrangeDersStage(numParametersCurrentStage);
    for(int j=0; j<numParametersCurrentStage; j++){
      lagrangeDersStage[j] = lagrangeDerivatives[derivativeIndexForCurrentStage + j];
    }
    if(i == m_currentStageIndex)
      m_stages[i]->setAdjointsAndLagrangeDerivatives(adjoints, lagrangeDersStage);
    else
      m_stages[i]->setAdjointsAndLagrangeDerivativesNoObserver(adjoints, lagrangeDersStage);

    derivativeIndexForCurrentStage += numParametersCurrentStage;
  }

}

/**
* @copydoc IntegratorMetaData::setInitialLagrangeDerivatives
*/
void IntegratorMultiStageMetaData::setInitialLagrangeDerivatives()
{
  m_stages[m_currentStageIndex]->setInitialLagrangeDerivatives();
}

/**
* @copydoc IntegratorMetaData::updateAdjoints
*/
void IntegratorMultiStageMetaData::updateAdjoints()
{
  m_stages[m_currentStageIndex]->updateAdjoints();
}

/**
* @brief update adjoints for the next stage during reverse integration
*
* Adjoints and lagrange derivatives are stored and updated only on the current stage.
* So if the stage changes during reverse integration the adjoints and lagrange derivatives
* have to be copied to the new stage
*/
void IntegratorMultiStageMetaData::copyAdjointsAndLagrangeDersToPreviousStage()
{
  std::vector<double> adjoints, lagrangeDers;
  assert(m_currentStageIndex+1 < m_stages.size());
  m_stages[m_currentStageIndex+1]->getAdjoints(adjoints);
  m_stages[m_currentStageIndex+1]->getLagrangeDers(lagrangeDers);
  double *adjointsPtr=0;
  if(!adjoints.empty()){
    adjointsPtr = &adjoints[0];
  }
  
  // todo apply mapping to adjoints set to different stage (see 2nd order)
  const unsigned numLagrangeDers = m_stages[m_currentStageIndex]->getNumSensitivityParameters();
  utils::WrappingArray<double> adjointsAr(adjoints.size(), adjointsPtr);
  utils::Array<double> lagrangeDersAr(numLagrangeDers, 0.0);
  m_stages[m_currentStageIndex]->setAdjointsAndLagrangeDerivatives(adjointsAr, lagrangeDersAr);
}

/**
* @copydoc IntegratorMetaData::initializeAtDuration
*/
void IntegratorMultiStageMetaData::initializeAtStageDuration()
{
  if(m_isInitializedBackwards){
    m_stages[m_currentStageIndex]->clearActiveParameters();
    // set stage index to previous stage
    // and copy adjoints into next stage
    //todo: warning: this version only works if number of states does not differ from stage to stage

    m_currentStageIndex--;
    assert(m_currentStageIndex >= 0);
    assert(m_currentStageIndex < m_stages.size());

    copyAdjointsAndLagrangeDersToPreviousStage();

    //reduce the size of vector mappingSensitivities by the number of the stage sensitivities
    if(!m_mappingSensitivities.empty()){
      const unsigned numSensParams = m_stages[m_currentStageIndex]->getNumSensitivityParameters();
      const unsigned numEquations = m_stages[m_currentStageIndex]->getGenericEso()->getNumEquations();
      const unsigned numStageSens = numSensParams * numEquations;
      const unsigned numTotalMappingSens = m_mappingSensitivities.size();
      assert(numTotalMappingSens >= numStageSens);
      m_mappingSensitivities.resize(numTotalMappingSens - numStageSens);
    }
  }
  else{
    // if called for the first time, initialize adjoints and derivative vectors
    // for all stages
    m_isInitializedBackwards = true;
    for(unsigned i=0; i<m_stages.size(); i++){
      m_stages[i]->initializeAtStageDuration();
    }
    if(m_mappingSensitivities.empty()){
      for(unsigned i=0; i<m_stages.size()-1; i++){
        m_stages[i]->clearActiveParameters();
      }
    }
  }
  //reactivate parameters on current stage
  m_stages[m_currentStageIndex]->reactivateParameters();
}

/**
* @brief Inserts a data point with nonlinear gradient information
*        required by the optimizer on the multi stage layer
*
* The key of the OptDataStruct is the current time, which is stored in the object when the function
* is called. The data required for the optimizer is stored within each stage separatley.
*/
void IntegratorMultiStageMetaData::addIntOptDataPoint(const unsigned timeIndex)
{
  // we want to include the multi stage gradients of the constraints (for previous stages)
  // current gradients are handled in the current stage. meaning the call to
  // extract current data from the single stage
  IntOptDataMap currentSSOptData = m_stages[m_currentStageIndex]->getIntOptData().front();

  // key and struct of the current stage we are at. currentPointData can be used to extract the
  // required gradient information from the multistage problem
  // the time index of the current time point has to be found first
  const double currentTime = m_stages[m_currentStageIndex]->getStageCurrentTimeScaled();
  //assert(currentSSOptData.count(currentTimeIndex) > 0);

  SavedOptDataStruct currentPointData = currentSSOptData[timeIndex];

  SavedOptDataStruct newMSTimePoint;
  // deep copy of the struct
  newMSTimePoint.nonLinConstVarIndices   = currentPointData.nonLinConstVarIndices;

  newMSTimePoint.nonLinConstIndices      = currentPointData.nonLinConstIndices;
  newMSTimePoint.nonLinConstStateIndices = currentPointData.nonLinConstStateIndices;
  //needed for "=" operator, since the operator is not implemented yet
  newMSTimePoint.nonLinConstValues.resize(currentPointData.nonLinConstValues.getSize(), 0.0);
  newMSTimePoint.nonLinConstValues     = currentPointData.nonLinConstValues;
  // the number of already activated parameters differs from the single stage.
  // We don't want to get the number of parameters of the current stage,
  // since they are already handled in the single stage
  newMSTimePoint.numSensParam = 0;
  newMSTimePoint.timePoint = currentTime;


  //update numSensParam and activeParameterIndices on the struct
  // collecting data from all previous stages
  for(int i = 0; i<int(m_currentStageIndex); i++){
    // for the mapping the data at the endpoint is needed
    IntOptDataMap optDataLoop = m_stages[i]->getIntOptData().front();
    const unsigned endTimeIndex = findTimeIndex(optDataLoop, 1.0);
    if(optDataLoop.count(endTimeIndex) > 0){
      SavedOptDataStruct stageEndPoint = optDataLoop[endTimeIndex];

      // at the last time point all parameters should be present
      assert(m_stages[i]->getNumSensitivityParameters() == stageEndPoint.numSensParam);
      const unsigned newNumberOfParameters = newMSTimePoint.numSensParam + stageEndPoint.numSensParam;
      const unsigned oldNumberOfParameters = newMSTimePoint.numSensParam;
      newMSTimePoint.activeParameterIndices.reserve(newNumberOfParameters);
      newMSTimePoint.activeDecVarIndices.reserve(newNumberOfParameters);

      // add oldNumberOfParameters to the parameter indices to assert continuous parameter indices
      // on the multistage level
      for(unsigned j=0; j<stageEndPoint.numSensParam; j++){
        const unsigned newParameterIndex = stageEndPoint.activeParameterIndices[j] + oldNumberOfParameters;
        newMSTimePoint.activeParameterIndices.push_back(newParameterIndex);
      }


      // update the total number of sensitivity parameters
      newMSTimePoint.numSensParam = newNumberOfParameters;
    }
  }//for

  // extract gradient information
  const unsigned numStateIndices = newMSTimePoint.nonLinConstStateIndices.size();
  const double defaultVal = 0;
  newMSTimePoint.nonLinConstGradients.resize(newMSTimePoint.numSensParam*numStateIndices, defaultVal);
  getConstraintSensitivities(newMSTimePoint.nonLinConstStateIndices,
                             newMSTimePoint.numSensParam,
                             m_stages[m_currentStageIndex]->getGenericEso()->getNumEquations(),
                             newMSTimePoint.nonLinConstGradients);

  // we insert the data in the multi stage m_intOptData
  m_intOptData[m_currentStageIndex].insert
              (pair<unsigned,SavedOptDataStruct>(timeIndex, newMSTimePoint));
}

/**
* @brief extract current sensitivities for a subset of equations (constraints).
*
* @param equationIndices vector containing the equation indices of the constraints
* @param numParams number of parameters from the previous multi stages
* @param numEqn number of equations of the current DAE model
* @param sensitivities array receiving the sensitivity values -
*                      must be of size numParams * equationIndices.size()
*/
void IntegratorMultiStageMetaData::getConstraintSensitivities(const std::vector<unsigned> &equationIndices,
                                                              const unsigned numParams,
                                                              const unsigned numEqn,
                                                              utils::Array<double>  &sensitivities)
{
  if(m_mappingSensitivities.empty()){
    return;
  }
  const unsigned numParameters = numParams;
  const unsigned numEquations = numEqn;

  assert(sensitivities.getSize() == equationIndices.size() * numParameters);

  for(unsigned i=0; i<equationIndices.size(); i++){
    assert(equationIndices[i] < numEquations);
    for(unsigned j=0; j<numParameters; j++){
      const unsigned outputSensIndex = i * numParameters + j;
      const unsigned currentSensIndex = j * numEquations + equationIndices[i];
      assert(outputSensIndex < sensitivities.getSize());
      assert(currentSensIndex < m_mappingSensitivities.size());
      sensitivities[outputSensIndex] = m_mappingSensitivities[currentSensIndex];
    }
  }
}



/**@copydoc IntegratorMetaData::getDuration
*/
double IntegratorMultiStageMetaData::getIntegrationGridDuration()const
{
  return m_stages[m_currentStageIndex]->getIntegrationGridDuration();
}

/**
* @copydoc IntegratorMetaData::getJacobianFstates
*/
CsCscMatrix::Ptr IntegratorMultiStageMetaData::getJacobianFstates(){
  return m_stages[m_currentStageIndex]->getJacobianFstates();
}
/**
* @copydoc IntegratorMultiStageMetaData::getJacobianFup
*/
CsCscMatrix::Ptr IntegratorMultiStageMetaData::getJacobianFup()
{
  return m_stages[m_currentStageIndex]->getJacobianFup();
}
/** @brief returns the multi stage mapping sensitivities
*   @param[out] curSens the multi stage sensitivities at the current time point
*/
void IntegratorMultiStageMetaData::getCurrentSensitivities(std::vector<double> &curSens) const
{
  curSens.assign(m_mappingSensitivities.begin(), m_mappingSensitivities.end());
}

/**
* @copydoc IntegratorMetaData::getAdjoints
*/
void IntegratorMultiStageMetaData::getAdjoints(std::vector<double> &adjoints) const
{
  m_stages[m_currentStageIndex]->getAdjoints(adjoints);
}

/**
* @copydoc IntegratorMetaData::getLagrangeDers
*/
void IntegratorMultiStageMetaData::getLagrangeDers(std::vector<double> &LagrangeDers) const
{
  const unsigned numGlobalSensParams = getNumSensitivityParameters();
  LagrangeDers.resize(numGlobalSensParams);
  unsigned lagrangeDerIndex1stOrder = 0;
  for(unsigned i=0; i<m_stages.size(); i++){
    const unsigned numStageParameters = m_stages[i]->getNumSensitivityParameters();
    std::vector<double> stageLagrangeDers;
    m_stages[i]->getLagrangeDers(stageLagrangeDers);
    assert(numStageParameters == stageLagrangeDers.size() ||
           numStageParameters *(numGlobalSensParams +1) == stageLagrangeDers.size());
    for(unsigned j=0; j<numStageParameters; j++){
      assert(lagrangeDerIndex1stOrder +j < LagrangeDers.size());
      LagrangeDers[lagrangeDerIndex1stOrder + j] = stageLagrangeDers[j];
    }
    //only relevant for 2nd order ->override function?
    for(unsigned j=numStageParameters; j<stageLagrangeDers.size(); j++){
      LagrangeDers.push_back(stageLagrangeDers[j]);
    }
    lagrangeDerIndex1stOrder += numStageParameters;
  }
}

/**
* @brief returns the sum of all single stage global final times. Thus, the unscaled final time of ms problem
*
* @return sum of final time values
*/
double IntegratorMultiStageMetaData::getMsDurationVal() const
{
  double msDuration = 0;
  for (unsigned i=0;i<m_stages.size(); i++){
    const double ssDuration = m_stages[i]->getStageDuration();
    msDuration += ssDuration;
  }
  return msDuration;
}


/** @brief returns the sum over all single stages of the lower and upper bounds of the DurationParameter
*
* this function is needed to assure that there is no contradiction between the bounds of individual stages
* and the bounds of the total final time Tf = sum(tf, i=0..nStage-1). It must hold sum(tfl_i, i=1..nStages-1) <= Tfu
* and sum(tfu_i, i=1..nStages-1)>= Tfl
* if the final time tf of an individual stage is fixed it holds that lower and upper bounds are equal to tf
*
* @param[out] sumLowerBounds sum of all lower bounds of DurationParameter
* @param[out] sumUpperBounds sum of all upper bounds of DurationParameter
*/
void IntegratorMultiStageMetaData::getSumOfDurationBounds(double &sumLowerBounds, double &sumUpperBounds) const
{
  sumLowerBounds = 0.0;
  sumUpperBounds = 0.0;

  for (unsigned i=0;i<m_stages.size(); i++){
    std::vector<DurationParameter::Ptr> durations;
    m_stages[i]->getDurationParameters(durations);

    double sumStageLowerBounds = 0.0, sumStageUpperBounds = 0.0;
    for(unsigned j=0; j<durations.size(); j++){
      if(durations[j]->getIsDecisionVariable() == true){
        sumStageLowerBounds += durations[j]->getParameterLowerBound();
        sumStageUpperBounds += durations[j]->getParameterUpperBound();
      }
      else{
        sumStageLowerBounds += durations[j]->getParameterValue();
        sumStageUpperBounds += durations[j]->getParameterValue();
      }
    }
    
    DurationParameter::Ptr stageDuration = m_stages[i]->getGlobalDurationParameter();
    if(stageDuration->getIsDecisionVariable()){
      sumStageLowerBounds = std::max(sumStageLowerBounds, stageDuration->getParameterLowerBound());
      sumStageUpperBounds = std::min(sumStageUpperBounds, stageDuration->getParameterUpperBound());
    }
    

    sumLowerBounds += sumStageLowerBounds;
    sumUpperBounds += sumStageUpperBounds;
  }
}


/** @brief returns a vector containing the final time parameter indices of all final times
* the length of the returned vector is equal to the number of stages. If the final time is not
* free in a particular stage, then the corresponding entry is '-1', e.g.:
* if return vector is [3, -1, 0], then there are free final time parameters in stages 1 and 3 (with
* indices '3' and '0', whereas the final time in stage 2 is fixed
* @return vector with final time indices
*/
std::vector<std::vector<int> > IntegratorMultiStageMetaData::getFreeDurationIndices()const
{
  std::vector<std::vector<int> > indexVec(m_stages.size());

  for (unsigned i=0; i<m_stages.size(); i++) {
    std::vector<DurationParameter::Ptr> durations;
    m_stages[i]->getDurationParameters(durations);
    for(unsigned j=0; j<durations.size(); j++){
      if(durations[j]->getIsDecisionVariable()){
        indexVec[i].push_back(durations[j]->getParameterIndex());
      }
    }
  }

  return indexVec;
}

/** @brief collects m_IntOptDataMaps of all single stages and returns them as a vector
*   @return vector of IntOptDataMaps of all stages
*/
std::vector<IntOptDataMap> IntegratorMultiStageMetaData::getIntOptDataStages(void)
{
  const int numStages = getNumStages();
  std::vector<IntOptDataMap> collectOptData(numStages);
  for(int i=0; i<numStages; i++){
    collectOptData[i] = m_stages[i]->getIntOptData()[0];
  }
  return collectOptData;
}

/** @brief determines the number of sensitivity parameters of the stages previous to m_currentStageIndex
*   @return the number of sensitivity of the mappings
*/
int IntegratorMultiStageMetaData::getNumMappingParameters() const
{
  int numSensParameters = 0;
  for(unsigned i=0; i<m_currentStageIndex; i++){
    numSensParameters += m_stages[i]->getNumSensitivityParameters();
  }
  return numSensParameters;
}
/**
* @copydoc IntetgratorMetaData::reset
*
* in multistage the intOptData map must be cleared
*/
void IntegratorMultiStageMetaData::reset()
{
  for(unsigned i=0; i<m_stages.size(); i++){
    m_stages[i]->reset();
  }
  for(unsigned i=0; i<m_intOptData.size(); i++){
    m_intOptData[i].clear();
  }
  m_isInitialized = false;
  m_isInitializedBackwards = false;
  m_mappingSensitivities.clear();
}


std::vector<Mapping::Ptr> IntegratorMultiStageMetaData::getMappings() const
{
  return m_mapping;
}

/**
 * @copydoc IntegratorMetaData::foundRoot
 *
 * Just pass the call down to the current stages' MetaData
 */
void IntegratorMultiStageMetaData::foundRoot()
{
  m_stages[m_currentStageIndex]->foundRoot();
}

/**
* @copydoc IntegratorMetaData::activatePlotGrid
*
* activate plot grid on all stages
*/
void IntegratorMultiStageMetaData::activatePlotGrid()
{
  for(unsigned i=0; i<m_stages.size(); i++){
    m_stages[i]->activatePlotGrid();
  }
}

/** @copydoc IntegratorMetaData::getTotalTime */
double IntegratorMultiStageMetaData::getStageCurrentTime()const
{
  return m_stages[m_currentStageIndex]->getStageCurrentTime();
}

/**
* @brief get all global parameter indices of a control of all previous stages
*
* @param esoIndex with the eso index the control is identified
* @param stageIndex stage number up to which the parameter indices are collected
* @note this function is supposed to be used by the output module. It changes the state of the parameters,
*       so this function should not be used during an integration
*/
std::vector<unsigned> IntegratorMultiStageMetaData::getParameterIndicesOfPreviousStages(
                                           const int esoIndex,
                                           const unsigned stageIndex)
{
  std::vector<unsigned> parameterIndices;
  unsigned numParametersPreviousStages = 0;
  for(unsigned i=0; i<stageIndex; i++){
    std::vector<Parameter::Ptr> sensParameters;
    m_stages[i]->getSensitivityParameters(sensParameters);
    for(unsigned j=0; j<sensParameters.size(); j++){
      int sensParamEsoIndex = sensParameters[j]->getEsoIndex();
      if(sensParamEsoIndex == esoIndex){
        parameterIndices.push_back(sensParameters[j]->getParameterIndex()
                                 + numParametersPreviousStages);
      }
    }
    numParametersPreviousStages += sensParameters.size();
  }
  return parameterIndices;
}

void IntegratorMultiStageMetaData::updateParameterOutput(DyosOutput::ParameterOutput &parameterOutput,
                                                   const std::vector<unsigned> &parameterIndices,
                                                   const unsigned stageIndex)
{
  if(stageIndex == 0){
    return;
  }
  
  std::vector<DyosOutput::FirstSensitivityOutput> multiStageFirstSensOut;
  multiStageFirstSensOut = Parameter::createSensitivityOutputVector(
                                          m_intOptData[stageIndex],
                                          m_stages[stageIndex]->getGenericEso(),
                                          parameterIndices);

  unsigned numSensParamsPrevStages = 0;
  for(unsigned i=0; i<stageIndex; i++){
    numSensParamsPrevStages += m_stages[i]->getNumSensitivityParameters();
  }

  for(unsigned i=0; i<parameterOutput.grids.size(); i++){
    unsigned firstSensOutIndex = 0;
    for(unsigned j=0; j<parameterOutput.grids[i].firstSensitivityOutputVector.size(); j++){
      DyosOutput::FirstSensitivityOutput currentFirstSensOut;
      currentFirstSensOut = parameterOutput.grids[i].firstSensitivityOutputVector[j];
      std::map<std::string,int>::iterator iter;
      unsigned numPrevStageValues = 0;
      std::map<int,int> currentMap = currentFirstSensOut.mapParamsCols;
      
      if(!parameterIndices.empty()){
        for(iter = currentFirstSensOut.mapConstrRows.begin();
            iter != currentFirstSensOut.mapConstrRows.end(); iter++){
            unsigned valuesRowIndex = iter->second;
            assert(firstSensOutIndex < multiStageFirstSensOut.size());
            unsigned debugInt = multiStageFirstSensOut[firstSensOutIndex].mapConstrRows.count(iter->first);
            if(debugInt == 0)
              break;
            assert(multiStageFirstSensOut[firstSensOutIndex].mapConstrRows.count(iter->first) > 0);
            unsigned valuesMSRowIndex = multiStageFirstSensOut[firstSensOutIndex].mapConstrRows[iter->first];
            currentFirstSensOut.values[valuesRowIndex].insert(
                        currentFirstSensOut.values[valuesRowIndex].begin(),
                        multiStageFirstSensOut[firstSensOutIndex].values[valuesMSRowIndex].begin(),
                        multiStageFirstSensOut[firstSensOutIndex].values[valuesMSRowIndex].end());
        }//for iter
        currentFirstSensOut.mapParamsCols = multiStageFirstSensOut[firstSensOutIndex].mapParamsCols;
        numPrevStageValues = multiStageFirstSensOut[firstSensOutIndex].values.front().size();
      }
      //update param col map
      std::map<int,int>::iterator iter1;
      for(iter1 = currentMap.begin(); iter1 != currentMap.end(); iter1++){
        currentFirstSensOut.mapParamsCols[numPrevStageValues + iter1->first]
                             = iter1->second + numSensParamsPrevStages;
      }
      parameterOutput.grids[i].firstSensitivityOutputVector[j] = currentFirstSensOut;
      firstSensOutIndex ++;
    }//for j
  }//for i
}

void IntegratorMultiStageMetaData::adjustMultistageParameterStructure(
                                        std::vector<DyosOutput::ParameterOutput> &parameters,
                                        std::vector<DyosOutput::ParameterOutput> &parametersNextStage,
                                        const unsigned stageIndex)
{
  const unsigned numStages = getNumStages();
  //insert mapping sensitivities for that stage
  for(unsigned j=0; j<parameters.size(); j++){
    DyosOutput::ParameterOutput currentParameterOutput;
    currentParameterOutput = parameters[j];
    std::vector<unsigned> parameterIndices;
    parameterIndices = getParameterIndicesOfPreviousStages(currentParameterOutput.esoIndex,
                                                           stageIndex);
    updateParameterOutput(currentParameterOutput, parameterIndices, stageIndex);
    parameters[j] = currentParameterOutput;
    
    // propagate parameter information to the next stage if the parameter is not redefined there
    // otherwise the sensitivity output will be lost on the next stage
    // caution: since esoIndices are compared, this only works if all stages have the same Eso
    if(stageIndex +1 < numStages){
      std::vector<unsigned> paramIndicesPlusNextStage, paramIndicesPlusThisStage;
      paramIndicesPlusThisStage = getParameterIndicesOfPreviousStages(currentParameterOutput.esoIndex,
                                                                      stageIndex + 1);
      paramIndicesPlusNextStage = getParameterIndicesOfPreviousStages(currentParameterOutput.esoIndex,
                                                                      stageIndex + 2);
      if(!paramIndicesPlusThisStage.empty()
         && paramIndicesPlusThisStage.size() == paramIndicesPlusNextStage.size()){
        DyosOutput::ParameterOutput dummyParam = currentParameterOutput;
        if(!dummyParam.grids.empty()){
          if(!dummyParam.grids.back().values.empty()){
            dummyParam.value = dummyParam.grids.back().values.back();
          }
        }
        dummyParam.grids.clear();
        DyosOutput::ParameterOutput modelParam = m_stages[stageIndex + 1]->getOutput().front().integrator.parameters.front();
        dummyParam.grids.resize(modelParam.grids.size());
        for(unsigned i=0; i<dummyParam.grids.size(); i++){
          dummyParam.grids[i].firstSensitivityOutputVector = modelParam.grids[i].firstSensitivityOutputVector;
          for(unsigned k=0; k<dummyParam.grids[i].firstSensitivityOutputVector.size(); k++){
            dummyParam.grids[i].firstSensitivityOutputVector[k].mapParamsCols.clear();
            //clear the actual content of values, but keep field length, since number of rows is constant for all parameters
            for(unsigned j = 0; j<dummyParam.grids[i].firstSensitivityOutputVector[k].values.size(); j++){
              dummyParam.grids[i].firstSensitivityOutputVector[k].values[j].clear();
            }
          }
        }
        parametersNextStage.push_back(dummyParam);
        
      }
    }
  }
}

/**
* @brief getter for structure StageOutput
*
* @param[in] StageInput structure
* @param[in] StageOutput structure
*/
std::vector<DyosOutput::StageOutput> IntegratorMultiStageMetaData::getOutput()
{
  const unsigned numStages = getNumStages();
  std::vector <DyosOutput::StageOutput> stageOutput(numStages);
  for(unsigned stageIndex=0; stageIndex<numStages; stageIndex++){
    DyosOutput::StageOutput thisStage = m_stages[stageIndex]->getOutput().front();
    std::vector<DyosOutput::ParameterOutput> dummyParams;
    for(unsigned i=0; i<stageOutput[stageIndex].integrator.parameters.size(); i++){
      bool insert = true;
      for(unsigned j=0; j<thisStage.integrator.parameters.size(); j++){
        if(stageOutput[stageIndex].integrator.parameters[i].esoIndex == 
          thisStage.integrator.parameters[j].esoIndex){
            insert = false;
            break;
        }
      }
      if (insert)
        dummyParams.push_back(stageOutput[stageIndex].integrator.parameters[i]);
    }
    thisStage.integrator.parameters.insert(thisStage.integrator.parameters.end(),
                                           dummyParams.begin(),
                                           dummyParams.end());
    thisStage.integrator.initials.insert(thisStage.integrator.initials.end(),
                                         stageOutput[stageIndex].integrator.initials.begin(),
                                         stageOutput[stageIndex].integrator.initials.end());
    DyosOutput::StageOutput nextStage = stageOutput[stageIndex];
    if(stageIndex < numStages - 1){
      nextStage = stageOutput[stageIndex + 1];
    }
    adjustMultistageParameterStructure(thisStage.integrator.parameters,
                                       nextStage.integrator.parameters,
                                       stageIndex);
    adjustMultistageParameterStructure(thisStage.integrator.initials,
                                       nextStage.integrator.initials,
                                       stageIndex);
    if(stageIndex < numStages -1){
      stageOutput[stageIndex + 1] = nextStage;
    }
    stageOutput[stageIndex] = thisStage;
    //don't update parameter indices on last durations struct (already correct)
    for(unsigned j=0; j<stageOutput[stageIndex].integrator.durations.size(); j++){
      std::vector<unsigned> emptyParameterIndexVector;
      updateParameterOutput(stageOutput[stageIndex].integrator.durations[j],
                            emptyParameterIndexVector,
                            stageIndex);
    }
    std::vector<unsigned> parameterIndices = getParameterIndicesOfPreviousStages(-2, stageIndex);
    
    
    //add duration parameter sensitivities of previous stages
    if(!parameterIndices.empty()){
      std::vector<DyosOutput::FirstSensitivityOutput> firstSensOut = 
          Parameter::createSensitivityOutputVector(m_intOptData[stageIndex], 
                                                   getGenericEso(),
                                                   parameterIndices);
      DyosOutput::ParameterOutput durationOut = stageOutput[stageIndex].integrator.durations.front();
      std::vector<DyosOutput::FirstSensitivityOutput> original = 
        durationOut.grids.front().firstSensitivityOutputVector;
      if(firstSensOut.size() == original.size()){
        assert(firstSensOut.size() == original.size());
        for(unsigned i=0; i<firstSensOut.size(); i++){
          assert(firstSensOut[i].mapConstrRows.size() == original[i].mapConstrRows.size());
          std::map<int, int>::iterator iter;
          const unsigned offset = firstSensOut[i].mapParamsCols.size();
          for(iter = original[i].mapParamsCols.begin(); iter != original[i].mapParamsCols.end(); iter++){
            firstSensOut[i].mapParamsCols[iter->first+ offset] = iter->second ;
          }
          for(unsigned j=0; j<firstSensOut[i].values.size(); j++){
            firstSensOut[i].values[j].insert(firstSensOut[i].values[j].end(),
              original[i].values[j].begin(),
              original[i].values[j].end());
          }//for j
        }//for i
      }
      stageOutput[stageIndex].integrator.durations.front().grids.front().firstSensitivityOutputVector = firstSensOut;
    }//if !parameterIndices.empty()
  }//for stageIndex

  return stageOutput;
}

/**
* @brief constructor
*/
IntegratorMSMetaData2ndOrder::IntegratorMSMetaData2ndOrder(
                               std::vector<Mapping::Ptr> mappings,
                               std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages)
{
  m_mapping = mappings;
  m_stages.assign(stages.begin(), stages.end());
  m_stages2ndOrder = stages;
  m_currentStageIndex = 0;
  m_isInitialized = false;
  m_isInitializedBackwards = false;
  IntegratorMultiStageMetaData::m_intOptData.resize(stages.size());
}

/**
* @brief destrcutor
*/
IntegratorMSMetaData2ndOrder::~IntegratorMSMetaData2ndOrder()
{
}


/**
* @copydoc IntegratorMetaData2ndOrder::calculateAdjointsReverse2ndOrder

*/
void IntegratorMSMetaData2ndOrder::calculateAdjointsReverse2ndOrder
                                               (utils::Array<double> &states,
                                                utils::Array<double> &sens,
                                                utils::Array<double> &adjoints1stAnd2nd,
                                                utils::Array<double> &rhsAdjoints2nd,
                                                utils::Array<double> &rhsDers2nd)
{
  //preset eso
  // set state values
  const int numStates = states.getSize();
  assert(numStates == getGenericEso()->getNumStates());
  getGenericEso()->setStateValues(numStates, states.getData());

  // set control values
  m_stages[m_currentStageIndex]->setControlsToEso();

  // set all derivatives to 0.0
  utils::Array<double>dummyDerivatives(getGenericEso()->getNumDifferentialVariables(), 0.0);
  getGenericEso()->setDerivativeValues(dummyDerivatives.getSize(), dummyDerivatives.getData());

  //todo implement function getMaxNumStates
  const unsigned numMaxStates = m_stages[m_currentStageIndex]->getGenericEso()->getNumStates();
  assert(numMaxStates == states.getSize());
  utils::WrappingArray<double> adjoints1st(numMaxStates, adjoints1stAnd2nd.getData());
  unsigned numGlobalSensParams = IntegratorMultiStageMetaData::getNumSensitivityParameters();
  unsigned rhsDers2ndFirstIndex = 0;
  unsigned adjoints2ndFirstIndex = 0;
  unsigned sensFirstIndex = 0;
  for(unsigned i=0; i<m_stages.size(); i++){
    const unsigned numStageSensParams = m_stages[i]->getNumSensitivityParameters();
    const unsigned numActiveParameters = m_stages[i]->getNumCurrentSensitivityParameters();
    assert(rhsDers2ndFirstIndex + numStageSensParams*numGlobalSensParams <=
                                                    rhsDers2nd.getSize());
    utils::WrappingArray<double> rhsDers2ndLocal(numStageSensParams*numGlobalSensParams,
                                                 rhsDers2nd.getData() + rhsDers2ndFirstIndex);
    const unsigned dataIndex = numMaxStates + adjoints2ndFirstIndex;
    assert(dataIndex +numMaxStates*numStageSensParams <= adjoints1stAnd2nd.getSize());
    //todo make code work for stages with different number of states
    utils::WrappingArray<double> adjoints2nd(numMaxStates*numStageSensParams,
                                             adjoints1stAnd2nd.getData() + dataIndex);
    assert(adjoints2ndFirstIndex + numMaxStates*numStageSensParams <= rhsAdjoints2nd.getSize());
    utils::WrappingArray<double> rhsAdjoints2ndStage(numMaxStates * numStageSensParams,
                                                     rhsAdjoints2nd.getData() + adjoints2ndFirstIndex);
    utils::WrappingArray<double> sensStage(numMaxStates * numActiveParameters,
                                           sens.getData() + sensFirstIndex);
    m_stages2ndOrder[i]->calculateAdjointsReverse2ndOrder(sensStage,
                                                          adjoints1st,
                                                          adjoints2nd,
                                                          rhsAdjoints2ndStage,
                                                          rhsDers2ndLocal,
                                                          this);
    rhsDers2ndFirstIndex += numGlobalSensParams*numStageSensParams;
    adjoints2ndFirstIndex += numStageSensParams*numMaxStates;
    sensFirstIndex += numActiveParameters*numMaxStates;
  }
}

/**
* @copydoc IntegratorMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder
*/
void IntegratorMSMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder(
                                                             utils::Array<double> &adjoints1stOrder,
                                                             utils::Array<double> &adjoints2ndOrder,
                                                             Eso2ndOrderEvaluation &input,
                                                             utils::Array<double> &rhsDers2ndOrder)
{
  // collect the decision variables of all stages
  std::vector<Parameter::Ptr> sensParameters;
  for(unsigned i=0; i<m_stages.size(); i++){
    std::vector<Parameter::Ptr> parametersStage;
    m_stages2ndOrder[i]->getSensitivityParameters(parametersStage);
    sensParameters.insert(sensParameters.end(),
                          parametersStage.begin(),
                          parametersStage.end());
  }

  IntegratorSSMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder(adjoints1stOrder,
                                                                 adjoints2ndOrder,
                                                                 input,
                                                                 rhsDers2ndOrder,
                                                                 sensParameters,
                                                                 this);
}

/**
* @copy IntegratorMetaData2ndOrder::getGenericEso2ndOrder
*/
GenericEso2ndOrder::Ptr IntegratorMSMetaData2ndOrder::getGenericEso2ndOrder() const
{
  return m_stages2ndOrder[m_currentStageIndex]->getGenericEso2ndOrder();
}

/**
* @copydoc IntegratorMetaData::initializeAtDuration
*
* before initialization of the new stage all parameters on the current stage
* are deleted such that getNumCurrentSensitivityParameters is 0.
* After initialization of the new stage all lagrange derivative vectors must be
* resized, because the local routine on the single stages allocates the 2nd
* order derivatives to a wrong size.
*/
void IntegratorMSMetaData2ndOrder::initializeAtStageDuration()
{
  IntegratorMultiStageMetaData::initializeAtStageDuration();
  const unsigned numGlobalSensParams = getNumSensitivityParameters();
  for(unsigned i=0; i<m_stages2ndOrder.size(); i++){
    m_stages2ndOrder[i]->resizeLagrangeDerivatives(numGlobalSensParams);
    //deactivate all parameters
    std::vector<Parameter::Ptr> parameters;
    m_stages2ndOrder[i]->getSensitivityParameters(parameters);
    for(unsigned j=0; j<parameters.size(); j++){
      parameters[j]->setParameterSensActivity(false);
    }
  }
  //reactivate parameters on current stage
  m_stages2ndOrder[m_currentStageIndex]->reactivateParameters();
}

/**
* @copyDoc IntegratorMetaData::setAdjointsAndLagrangeDerivatives
*
* Divide the adjoints and the lagrangeDers vector to the single stages.
* The 1st order adjoints are copied into all stages.
*/
void IntegratorMSMetaData2ndOrder::setAdjointsAndLagrangeDerivatives(
                                                 const utils::Array<double> &adjoints,
                                                 const utils::Array<double> &lagrangeDerivatives)
{
  const unsigned numGlobalSensParams = getNumSensitivityParameters();
  const unsigned maxNumStates = getGenericEso()->getNumStates();
  unsigned adjoints2ndOrderIndex = maxNumStates;
  unsigned numSensParamsOfPreviousStages = 0;
  for(unsigned i=0; i<m_stages.size(); i++){
    const unsigned numLocalSensParams = m_stages[i]->getNumSensitivityParameters();
    const unsigned numLocalEquations = m_stages[i]->getGenericEso()->getNumEquations();

    //set adjoints to local stage
    //create local adjoints vector
    utils::Array<double> stageAdjoints(numLocalEquations*(1+numLocalSensParams), 0.0);

    iiMap equationMap = Mapping::getEquationsVectorMap(m_mapping,
                                                       numLocalEquations,
                                                       m_currentStageIndex,
                                                       i);
    iiMap::iterator iter;
    for(iter = equationMap.begin(); iter!= equationMap.end(); iter++){
      stageAdjoints[iter->second] = adjoints[iter->first];
      for(unsigned j=0; j<numLocalSensParams; j++){
        const unsigned stageAdjointsIndex = (j+1)*numLocalEquations + iter->second;
        assert(stageAdjointsIndex < stageAdjoints.getSize());
        const unsigned adjointsIndex = adjoints2ndOrderIndex + j*maxNumStates + iter->first;
        assert(adjointsIndex < adjoints.getSize());
        stageAdjoints[stageAdjointsIndex] = adjoints[adjointsIndex];
      }
    }// for equationMap
    adjoints2ndOrderIndex += maxNumStates * numLocalSensParams;

    //set lagrangeDers on local stage
    //create local lagrangeDers vector
    const unsigned stageLagrangeDersSize = numLocalSensParams * (1 + numGlobalSensParams);
    utils::Array<double> stageLagrangeDers(stageLagrangeDersSize, 0.0);

    for(unsigned j=0; j<numLocalSensParams; j++){
      //set first order lagrange derivatives
      stageLagrangeDers[j] = lagrangeDerivatives[j + numSensParamsOfPreviousStages];
      //set second order lagrange derivatives
      for(unsigned k=0; k<numGlobalSensParams; k++){
        const unsigned stageLagrangeDersIndex = numLocalSensParams + j*numGlobalSensParams + k;
        const unsigned lagrangeDersIndex =   numGlobalSensParams
                                     + numSensParamsOfPreviousStages*numGlobalSensParams
                                     + j*numGlobalSensParams + k;
        stageLagrangeDers[stageLagrangeDersIndex] = lagrangeDerivatives[lagrangeDersIndex];
      }
    }// for j
    numSensParamsOfPreviousStages += numLocalSensParams;
    if(m_mappingSensitivities.empty()){
      for(unsigned i=0; i<maxNumStates; i++){
        stageAdjoints[i] = adjoints[i];
      }
    }
    m_stages[i]->setAdjointsAndLagrangeDerivativesNoObserver(stageAdjoints, stageLagrangeDers);
  }//for i<m_stages.size()
}

/**
* @copydoc IntegratorMultiStageMetaData::copyAdjointsAndLagrangeDersToPreviousStage
*
* In 2nd order mode in all stages the adjoints and lagrange derivatives are always
* up to date. So this function can be left empty.
*/
void IntegratorMSMetaData2ndOrder::copyAdjointsAndLagrangeDersToPreviousStage()
{
}

/**
* @copydoc IntegratorMetaData::getAdjoints
*
* collect the adjoints from all stages. 1st order adjoints are copied
* from the current stage only.
*/
void IntegratorMSMetaData2ndOrder::getAdjoints(std::vector<double> &adjoints) const
{
  const unsigned numMaxStates = m_stages[m_currentStageIndex]->getGenericEso()->getNumEquations();
  const unsigned numParams = getNumSensitivityParameters();
  adjoints.reserve(numMaxStates*(1+numParams));
  adjoints.resize(numMaxStates);
  for(unsigned i=0; i<m_stages.size(); i++){
    std::vector<double> stageAdjoints;
    m_stages[i]->getAdjoints(stageAdjoints);
    const unsigned numEquationsStage = m_stages[i]->getGenericEso()->getNumEquations();
    if (i == m_currentStageIndex){
      for(unsigned j=0; j<numEquationsStage; j++){
        adjoints[j] = stageAdjoints[j];
      }
    }
    adjoints.insert(adjoints.end(), stageAdjoints.begin()+numEquationsStage, stageAdjoints.end());
  }
}

/**
* @copydoc IntegratorMetaData::updateAdjoints
*
* update the adjoints on the current stage and copy the updated
* 1st order adjoints to all other stages.
* @todo make this work for problems without full state mapping
*/
void IntegratorMSMetaData2ndOrder::updateAdjoints()
{
  m_stages[m_currentStageIndex]->updateAdjoints();

  //copy updated adjoints to all other stages
  std::vector<double> currentStageAdjoints;
  m_stages[m_currentStageIndex]->getAdjoints(currentStageAdjoints);
  const unsigned numMaxStates = m_stages[m_currentStageIndex]->getGenericEso()->getNumStates();
  assert(currentStageAdjoints.size() >= numMaxStates);
  for(unsigned i=0; i<m_stages.size(); i++){
    std::vector<double> stageAdjoints;
    m_stages[i]->getAdjoints(stageAdjoints);
    //override 1st order adjoints only
    assert(stageAdjoints.size() >= numMaxStates);
    for(unsigned j=0; j<numMaxStates; j++){
      stageAdjoints[j] = currentStageAdjoints[j];
    }
    //stageAdjoints.assign(currentStageAdjoints.begin(), currentStageAdjoints.end());
    std::vector<double> lagrangeDers;
    m_stages[i]->getLagrangeDers(lagrangeDers);
    utils::WrappingArray<double> stageAdjointsArray(stageAdjoints.size(), &stageAdjoints[0]);
    utils::Array<double> lagrangeDersArray(lagrangeDers.size(), 0.0);
    if(i == m_currentStageIndex)
      m_stages[i]->setAdjointsAndLagrangeDerivatives(stageAdjointsArray, lagrangeDersArray);
    else
      m_stages[i]->setAdjointsAndLagrangeDerivativesNoObserver(stageAdjointsArray, lagrangeDersArray);
  }
}

/**
* @copydoc IntegratorMetaData::setInitialLagrangeDerivatives
*
* This function is a copy of the single stage version, but uses global data instead
* of the local data of the single stage.
* Every parameter on each stage has corresponding mixed 2nd order derivatives on all
* ohter stages. So for each stage the initial lagrange derivatives are to be calculated
* and then stored on the stage, that manages the derivatives.
* @todo make this function work for multistage problems without full state mapping
*/
void IntegratorMSMetaData2ndOrder::setInitialLagrangeDerivatives()
{
  std::vector<double> adjoints, lagrangeDers;
  getAdjoints(adjoints);
  getLagrangeDers(lagrangeDers);
  utils::Array<double> initialLagrangeDersAddend(lagrangeDers.size(), 0.0);
  assert(!adjoints.empty());
  utils::WrappingArray<double> adjointsArray(adjoints.size(), &adjoints[0]);

  const unsigned numAdjoints = adjoints.size();
  const int numEquations = getGenericEso()->getNumEquations();
  const unsigned numAdjVectors = numAdjoints/(unsigned)numEquations;
  const unsigned numSensParams = getNumSensitivityParameters();

  for(unsigned j=0; j<numAdjVectors; j++){
    unsigned numParamsOfPreviousStages = 0;
    for(unsigned k=0; k<m_stages2ndOrder.size(); k++){
      std::vector<InitialValueParameter::Ptr> initials;
      m_stages2ndOrder[k]->getInitialValueParameter(initials);
      std::vector<Parameter::Ptr> sensParameters;
      m_stages2ndOrder[k]->getSensitivityParameters(sensParameters);
      const unsigned numStageSensParam = sensParameters.size();
      if(numStageSensParam > 0){
        assert(adjoints.size() >= (j+1)*numEquations);
        utils::WrappingArray<double> adjointsSlice(numEquations, &adjoints[j*numEquations]);


        assert(initialLagrangeDersAddend.getSize() >= j*numSensParams + numStageSensParam
                                                                      + numParamsOfPreviousStages);
        utils::WrappingArray<double> lagrangeDersSlice(numStageSensParam,
                                     initialLagrangeDersAddend.getData() + j*numSensParams
                                                                         + numParamsOfPreviousStages);

        updateInitialLagrangeDerivatives(initials,
                                         sensParameters,
                                         adjointsSlice,
                                         lagrangeDersSlice);
      }
      numParamsOfPreviousStages += m_stages2ndOrder[k]->getNumSensitivityParameters();
    }//for k
  }//for j

  // sum up the lagrange derivatives for the initialValueParameters on all stages
  // these entries should be zero at the end of an reverse integration, so they are
  // actually set in this function. All other lagrange derivatives values are unchanged.
  setAdjointsAndLagrangeDerivatives(adjointsArray, initialLagrangeDersAddend);
  //set 2nd order adjoints on all stages
  assert(m_currentStageIndex == 0);
  for(unsigned i=0; i<m_stages.size(); i++){
    m_stages2ndOrder[i]->store2ndOrderAdjoints();
  }
}
