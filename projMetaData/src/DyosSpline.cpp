/** @file DyosSpline.cpp
*    @brief implementation of DyosSpline class
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 08.03.2012                                       
*/

#include <cassert>
#include <cstdio>
#include <vector>

#include "DyosSpline.hpp"


/** @brief constructor
  *
  * constructor for Spline class. Spline is constructed with simple knots only (no knot duplicity, save for
  * the first and the last knot, depending on the spline order).
  * The timepoints must be scaled onto the closed interval [0.0, 1.0], i.e. the first timepoint must be t[0]=0.0 
  * and the last timepoint t[end] = 1.0. The number of timepoints must be equal to the number of values. In the
  * current implementation, the order is restricted to 1 (piecewise constant) and 2 (piecewise linear).
  *
  * @param[in] t vector with timepoints t = [0, .., 1.0]
  * @param[in] x pointer to a vector of ControlParameter
  * @param[in] order spline order (currently only supported piecewise constant: 1, piecewise linear: 2)  
  */ 
DyosSpline::DyosSpline(const std::vector<double> &t, const std::vector<ControlParameter::Ptr> &x, const unsigned order)
{
  m_parameters = x;
  const unsigned numParams = m_parameters.size();;
  static std::vector<double> vals;
  vals.resize(numParams);
  for(unsigned i=0; i<numParams; i++){
    vals[i] = x[i]->getParameterValue();
  }
  // all grids go from 0 to 1. Thus we have to include the final value by hand, since there is 
  // no parameter for pwc at that time point
  if (order == 1){
    vals.push_back(vals.back());
  }
  Spline::initializeSpline(t,vals,order);
}

/** @brief copy constructor
  *
  * @param[in] splineIn object of class DyosSpline to be copied
  */  
DyosSpline::DyosSpline(const DyosSpline &splineIn)
{
  Spline::m_order = splineIn.m_order;
  Spline::m_t = splineIn.m_t;
  Spline::m_x = splineIn.m_x;
  Spline::m_knots = splineIn.m_knots;
  m_parameters = splineIn.m_parameters;
}

/**
* @brief updates the spline if there was any change by the user and passes the call to the original spline class
*
* @param[in] tp timepoint at which spline is evaluated
* @return spline value
*/
double DyosSpline::eval(const double tp)
{
  // update spline with decision variables
  updateSpline();
  return Spline::eval(tp);
}

/**
 * @brief updates the underlying spline class with the current decision variables 
*/
void DyosSpline::updateSpline(void)
{
  const unsigned numParams = m_parameters.size();
  assert(numParams > 0);
  static std::vector<double> vals;
  vals.resize(numParams);
  for(unsigned i=0; i<numParams; i++){
    Spline::m_x[i] = m_parameters[i]->getParameterValue();
  }
  assert(m_x.size() == m_t.size());
  // all grids go from 0 to 1. Thus we have to include the final value by hand, since there is 
  // no parameter for pwc at that time point
  if (Spline::m_order == 1){
    unsigned lastIndex = m_x.size()-1;
    Spline::m_x[lastIndex] = Spline::m_x[lastIndex-1];
  }
  Spline::m_d = Spline::getSplineCoefficients(Spline::m_t, Spline::m_x, Spline::m_order);
  
}
/**
* @brief updates the spline if there was any change by the user and passes the call to the original spline class
*
* @param[in] tp timepoint at which spline is evaluated
* @param[in] splineIndex index of basis spline
* @return derivative value
*/
double DyosSpline::dSpline(const double tp, const unsigned splineIndex)
{
  updateSpline();
  // spline indices start with 1 in the original spline tool
  return Spline::dSpline(tp, splineIndex + 1);
}

