/**
* @file ParameterizationGrid.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaSingleStageEso - Part of DyOS                                    \n
* =====================================================================\n
* This file contains the definitions of the memberfunctions of class   \n
* ParameterizationGrid according to the UML Diagramm METAESO_2         \n
* =====================================================================\n
* @author Fady Assassa
* @date 17.10.2011
*/

#include "ParameterizationGrid.hpp"

#include <cassert>
#include <set>

/**
* @brief standard constructor sets the m_duration pointer to NULL
*/
ParameterizationGrid::ParameterizationGrid()
{
}
/**
* @brief standard constructor for equidistant time.
* @param numberOfIntervals the desired number of intervals
* @param controlApproximation the desired approximation (PWC or PWL)
*/
ParameterizationGrid::ParameterizationGrid(const unsigned numberOfIntervals,
                                           const ControlType controlApproximation)
{
  m_discretizationGrid.resize(numberOfIntervals);
  m_controlApproximation = controlApproximation;
  if (m_controlApproximation == PieceWiseLinear){
    // in case of pwl, we get an additional parameter on the final point.
    m_parameterization.resize(numberOfIntervals + 1);
  }else{
    m_parameterization.resize(numberOfIntervals);
  }
  for(unsigned i=0; i<m_parameterization.size(); i++){
    m_parameterization[i] = ControlParameter::Ptr(new ControlParameter());
  }

  for (unsigned i=0; i<numberOfIntervals; i++){
    m_discretizationGrid[i] = double(i)/numberOfIntervals;
  }
  m_discretizationGrid.push_back(1.0);
  m_pcresolution = 1;
}

/** @brief copy constructor based on a ParameterizationGrid pointer
*   @param pg a pointer which that is deep copied
*
*   This fucntion is required to copy a control. If the duration is set we create a new DurationParameter.
*   The memory which is allocated in the copy constructor is freed in the destructor.
*/
ParameterizationGrid::ParameterizationGrid(const ParameterizationGrid *pg)
{
  assert(pg != NULL);

  m_discretizationGrid = pg->m_discretizationGrid;
  m_controlApproximation = pg->m_controlApproximation;
  m_parameterization = pg->m_parameterization;
  if (pg->m_duration != NULL){
    DurationParameter::Ptr duration(new DurationParameter());
    m_duration = duration;
    m_duration->setParameterValue(pg->getDurationValue());
    m_duration->setWithSensitivity(pg->m_duration->getWithSensitivity());
    m_duration->setIsDecisionVariable(pg->m_duration->getIsDecisionVariable());
    m_duration->setParameterSensActivity(pg->m_duration->getParameterSensActivity());
    m_duration->setParameterLowerBound(pg->m_duration->getParameterLowerBound());
    m_duration->setParameterUpperBound(pg->m_duration->getParameterUpperBound());
  }
  m_activeParameterIndices = pg->m_activeParameterIndices;
  m_pcresolution = pg->m_pcresolution;
}
/**
* @brief sets all bounds of the Parameters
*
* @param lowerbound is the lower bound of the parameterization which is a constant
* @param upperbound is the upper bound of the parameterization which is a constant
*/
void ParameterizationGrid::setAllParameterBounds(const double lowerbound,const double upperbound)
{
  const unsigned numPar = m_parameterization.size();
  for (unsigned i=0; i<numPar; i++) {
    m_parameterization[i]->setParameterLowerBound(lowerbound);
    m_parameterization[i]->setParameterUpperBound(upperbound);
  }
}
/** @brief assigns all control parameters to the member m_parameterization
  * @param[in] parameters contains a vector of control parameters
  */
void ParameterizationGrid::setAllControlParameter(const std::vector<ControlParameter::Ptr> &parameters)
{
  if(m_controlApproximation == PieceWiseConstant){
    assert(parameters.size() == m_discretizationGrid.size()-1);
  }
  else if(m_controlApproximation == PieceWiseLinear){
    assert(parameters.size() == m_discretizationGrid.size());
  }
  m_parameterization.assign(parameters.begin(), parameters.end());
}

/**
* @brief destructor which frees the m_duration if it was allocated during the life of the class
*/
ParameterizationGrid::~ParameterizationGrid()
{
}

/** @brief sets variable m_pcresolution
  * @param[in] resolution 
  */
void ParameterizationGrid::setPCResolution(const int resolution)
{
  m_pcresolution = resolution;
}

/** @brief returns the type of control (PieceWiseConstant or PieceWiseLinear)
*   @return the approximation tpye (PieceWiseConstant or PieceWiseLinear)
*/
ControlType ParameterizationGrid::getControlType(void) const
{
  return m_controlApproximation;
}

/** @brief returns the entire control grid including the final time
*   @return a double vector of the control grid
*/
std::vector<double> ParameterizationGrid::getDiscretizationGrid(void)const
{
  return m_discretizationGrid;
}

/** @brief returns the value of member m_pcresolution 
  * @return the member value
  */
int ParameterizationGrid::getPCresolution(void) const
{
  return m_pcresolution;
}

/** @brief returns the additional grid points added by pcresolution,
  *        where the path constraint should be evaluated
  * @return double vector for the constraint grid
  */
std::vector<double> ParameterizationGrid::getConstraintGrid(void)const
{
  std::vector<double> points;
  unsigned size = m_discretizationGrid.size();
  for(unsigned i=0; i<size-1; i++){
    double intervalSize = ( m_discretizationGrid[i+1] - m_discretizationGrid[i])/double(m_pcresolution);
    for(int j=1; j<m_pcresolution; j++){
      double value = m_discretizationGrid[i] + j * intervalSize;
      value *= this->getDurationValue();
      points.push_back(value);
    }
  }
  return points;
}

/** @brief sets the new Grid and allocates the memory for the parameterization.
*   @param discretizationGrid is a vector of the grid ranging from 0.0 to 1.0
*/
void ParameterizationGrid::setDiscretizationGrid(const std::vector<double> discretizationGrid)
{
  assert(!discretizationGrid.empty());
  // We always start at 0.0, thus we have to do an exact comparison. When the Grid is created the value has
  // to be overwritten such that is exactely 0.0. -> faas, 25.01.12
  assert(discretizationGrid.front() == 0.0);
  assert(discretizationGrid.back() == 1.0);
  m_discretizationGrid = discretizationGrid;
}

/** @brief set the new control type
*   @param controlApproximation is either PieceWiseConstant or PieceWiseLinear
*/
void ParameterizationGrid::setControlType(const ControlType controlApproximation)
{
  m_controlApproximation = controlApproximation;
}

/** @brief Sets the index of the current active parameter
*   @param activeIndex is the index of the parameter which is set to active
*/
void ParameterizationGrid::setActiveParameterIndex(const vector<unsigned> activeIndices)
{
  for(unsigned i=0; i<activeIndices.size();i++)
    assert( activeIndices[i] < m_parameterization.size());
  m_activeParameterIndices = activeIndices;
}

/** @brief the parameterization is overriden with the given parameter. The size is according to the given grid.
*   @param values is a vector of the length of the parameterization, with the values of the parameter
*/
void ParameterizationGrid::setAllParameterizationValues(const std::vector<double> values)
{
  const unsigned valSize = values.size();
  const unsigned paramSize = m_parameterization.size();

  assert(valSize > 0);
  assert(paramSize > 0);
  assert(valSize == paramSize);

  for(unsigned i=0; i<values.size(); i++){
    m_parameterization[i]->setParameterValue(values[i]);
  }
}
/** @brief In case the control is without sensitivity information we need to turn the flag off
*   Goes through all controls and turns of the flag for sensitivity information.
*/
void ParameterizationGrid::setAllParametersWithoutSens(void)
{
  const unsigned numberOfParameters = m_parameterization.size();
  for (unsigned i=0; i<numberOfParameters; i++){
    m_parameterization[i]->setWithSensitivity(false);
  }
}

/** @brief In case the control is has no sensitivity it cannot be a decision variable. Thus,
*   information we need to turn the flag off
*   Goes through all controls and turns of the flag for decision variable information.
*/
void ParameterizationGrid::setAllParametersNotDecisionVars(void)
{
  const unsigned numberOfParameters = m_parameterization.size();
  for (unsigned i=0; i<numberOfParameters; i++){
    m_parameterization[i]->setIsDecisionVariable(false);
  }
}

/** @brief get final time value
*   @return final time value
*/
double ParameterizationGrid::getDurationValue(void) const
{
  assert(m_duration.get()!=NULL);
  return m_duration->getParameterValue();
}


/** @brief get control parameter
*   @param index index of control parameter
*   @return control parameter
*/
ControlParameter::Ptr ParameterizationGrid::getControlParameter(const unsigned index) const
{
  assert(index < m_parameterization.size());
  return m_parameterization[index];
}

/** @brief allocates the memory of the durationParameter and sets its value
*   @param value is a global final time value.
*   @throw bad_alloc
*
*   This function allocates the memory required for the DurationParameter. It is freed in the
*   destructor.
*/
void ParameterizationGrid::setDurationValue(const double value)
{
  assert(value >= 0.0); // are intervals of length 0.0 allowed?

  if (m_duration.get() == NULL){
    DurationParameter::Ptr duration(new DurationParameter());
    m_duration = duration;
  }
  m_duration->setParameterValue(value);
}


/** @brief returns the vector of Control Parameter
*   @return Vector of Control parameter
*/
std::vector<ControlParameter::Ptr> ParameterizationGrid::getAllControlParameter(void)const
{
  return m_parameterization;
}

/** @brief returns pointer to the current final time
*   @return pointer to the current final time
*/
DurationParameter::Ptr ParameterizationGrid::getDurationPointer(void) const
{
  return m_duration;
}

/** @brief returns active parameter index of the control. Required for the integration.
*   @return value of the current parameter index
*/
vector<unsigned> ParameterizationGrid::getActiveParameterIndex(void) const
{
  return m_activeParameterIndices;
}
/** @brief evaluates the spline of the control at time point timePoint
*   @param[in] timePoint is the time point where the spline should be evaluated
*/
double ParameterizationGrid::evalSpline(const double timePoint){

  if(m_controlApproximation==PieceWiseConstant)
    return m_parameterization[m_activeParameterIndices[0]]->getParameterValue();

  double lb=m_discretizationGrid[m_activeParameterIndices[0]];
  double rb=m_discretizationGrid[m_activeParameterIndices[1]];
  double timepoint=timePoint;
  if(timePoint<lb)
    timepoint=lb;
  double t=(timepoint-lb)/(rb-lb);

  double evalSpline=m_parameterization[m_activeParameterIndices[0]]->getParameterValue()*(1-t)
                +m_parameterization[m_activeParameterIndices[1]]->getParameterValue()*t;

  return evalSpline;
}
/** @brief evaluates the d-spline of the control at time point timePoint
*   @param[in] timePoint is the time point where the d-spline should be evaluated
*/
double ParameterizationGrid::evaldSpline(const double timePoint, unsigned paramIndex){

  vector<unsigned> activeInd;

  if(m_controlApproximation == PieceWiseConstant){
    assert(m_activeParameterIndices.size() == 1);
    activeInd.push_back(m_parameterization[m_activeParameterIndices[0]]->getParameterIndex());
    if(activeInd[0] == paramIndex)
      return 1.0;
    else
      return 0.0;
  }

  assert(m_activeParameterIndices.size() == 2);
  activeInd.push_back(m_parameterization[m_activeParameterIndices[0]]->getParameterIndex());
  activeInd.push_back(m_parameterization[m_activeParameterIndices[1]]->getParameterIndex());

  double lb=m_discretizationGrid[m_activeParameterIndices[0]];
  double rb=m_discretizationGrid[m_activeParameterIndices[1]];
  double t=(timePoint-lb)/(rb-lb);

  if(paramIndex==activeInd[0])
    return 1-t;
  else if(paramIndex==activeInd[1])
    return t;
  else
    return 0;
}
/**
* @brief creates FirstSensitivityOutput vector
*
* @param[in] IntOptDataMap vector
* @param[in] pointer to generic eso class
* @return ParameterGridOutput structure
*/
DyosOutput::ParameterGridOutput  ParameterizationGrid::createParameterGridOutput
                                                                         (IntOptDataMap iODP,
                                                                          GenericEso::Ptr &genEso)
{
  DyosOutput::ParameterGridOutput output;

  switch(m_controlApproximation){
    case PieceWiseConstant:
      output.approximation = DyosOutput::ParameterGridOutput::PieceWiseConstant;
      break;
    case PieceWiseLinear:
      output.approximation = DyosOutput::ParameterGridOutput::PieceWiseLinear;
      break;
    default:
      assert(false);
  }
  output.duration = getDurationValue();
  output.hasFreeDuration = m_duration->getWithSensitivity();
  output.gridPoints = getDiscretizationGrid();
  std::vector<ControlParameter::Ptr> ParameterPtr = getAllControlParameter();
  unsigned numParameters = ParameterPtr.size();
  std::vector<unsigned> ParameterIndices;
  ParameterIndices.reserve(numParameters + 1);
  output.values.resize(numParameters);
  output.lagrangeMultiplier.resize(numParameters);
  for(unsigned i=0; i<numParameters; i++){
    if(ParameterPtr[i]->getWithSensitivity()){
      ParameterIndices.push_back(ParameterPtr[i]->getParameterIndex());
      output.lagrangeMultiplier[i] = ParameterPtr[i]->getLagrangeMultiplier();
    }else{
      output.isFixed = true;
    }
    output.values[i] = ParameterPtr[i]->getParameterValue();
  }
  output.firstSensitivityOutputVector = Parameter::createSensitivityOutputVector(iODP,
                                                                                 genEso,
                                                                                 ParameterIndices);
  return output;
}
/**
* @brief updates the originalParameterizationGrid, that means rescaling the discretizationGrid
* and setting the durationValue
*/
void OriginalParameterizationGrid::update(){
  std::vector<std::pair<unsigned, ParameterizationGrid::Ptr> >::iterator iter;
  double prevDuration=0;
  double currentDuration=0;
  double sumDurations=0;
  std::vector<double>disGrid;
  std::vector<ControlParameter::Ptr> parameterGrid;
  unsigned index=0;
  for(unsigned i=0; i!=m_parameterizationGrids.size(); i++){
    sumDurations += m_parameterizationGrids[i].second->getDurationValue();
  }
  this->setDurationValue(sumDurations);
  //go through all parameterizationGrids and change original m_parameterizationGrid if parameter is original
  for(iter=m_parameterizationGrids.begin(); iter!=m_parameterizationGrids.end(); iter++){
    disGrid = iter->second->getDiscretizationGrid();
    parameterGrid = iter->second->getAllControlParameter();
    for(unsigned i=0; i<parameterGrid.size(); i++){
      currentDuration = prevDuration+iter->second->getDurationValue()*disGrid[i];
      if(parameterGrid[i]->isOriginal()){
        m_discretizationGrid[index]=currentDuration/sumDurations;
        index++;
      }
    }
    prevDuration += iter->second->getDurationValue();
  }
}

double OriginalParameterizationGrid::getSplineDerivative(const vector<unsigned> &activeIndices)const
{
  if(m_controlApproximation == PieceWiseConstant){
    return 0.0;
  }
  assert(activeIndices.size() == 2);
  double lb = 0.0;
  double rb = 0.0;
  double x=0.0;
  double y=0.0;
  for(unsigned i=0; i<m_parameterization.size(); i++){
    if(m_parameterization[i]->getParameterIndex()==int(activeIndices[0])){
        lb = m_discretizationGrid[i];
        x = m_parameterization[i]->getParameterValue();
    }
    else if(m_parameterization[i]->getParameterIndex()==int(activeIndices[1])){
        rb = m_discretizationGrid[i];
        y = m_parameterization[i]->getParameterValue();
    }
  }

  //double t=(originalTimePoint-lb)/(rb-lb);
  assert(rb>lb);
  return (y-x)/(rb-lb);
}

/**
* @brief evaluates the d-spline of the control at time point timePoint
*
* @param[in] timePoint on originalGrid
* @param[in] timePoint on currentGrid
* @param[in] paramIndex
* @param[in] vector of activeIndices
* @return dSpline value
*/
double OriginalParameterizationGrid::evaldSpline(const double originalTimePoint,
                                                 unsigned paramIndex,
                                                 vector<unsigned> activeIndices)const
{
  if(m_controlApproximation == PieceWiseConstant){
    assert(activeIndices.size() == 1);
    if(activeIndices[0] == paramIndex)
      return 1.0;
    else
      return 0.0;
  }

  assert(activeIndices.size() == 2);
  double lb, rb;
  for(unsigned i=0; i<m_parameterization.size(); i++){
    if(m_parameterization[i]->getParameterIndex()==int(activeIndices[0])){
        lb = m_discretizationGrid[i];
    }else if(m_parameterization[i]->getParameterIndex()==int(activeIndices[1])){
        rb = m_discretizationGrid[i];
    }
  }

  double t=(originalTimePoint-lb)/(rb-lb);

  if(paramIndex==activeIndices[0])
    return 1-t;
  else if(paramIndex==activeIndices[1])
    return t;
  else
    return 0;
}
/**
* @brief increases all indices of the parameterizationGrids by one
*/
void OriginalParameterizationGrid::changeIndices()
{
  for(unsigned i=0; i!=m_parameterizationGrids.size(); i++)
    m_parameterizationGrids[i].first++;
}
/**
* @brief inserts one or two parameterizationGrids into the originalGrid
* @param[in] the gridIndex where the first grid is supposed to be inserted
* @param[in] first grid to be inserted
* @param[in] second grid to be inserted
*/
void OriginalParameterizationGrid::insertParameterizationGrid(const unsigned gridIndex,
                                                              const ParameterizationGrid::Ptr insertedGrid1,
                                                              const ParameterizationGrid::Ptr insertedGrid2)
{
  if(m_parameterizationGrids.size()==0){//only one grid is being inserted this happens when a new control is constructed
    m_parameterizationGrids.push_back(std::make_pair(gridIndex,insertedGrid1));
    return;
  }
  std::vector<std::pair<unsigned, ParameterizationGrid::Ptr> >::iterator it=m_parameterizationGrids.begin();
  for(it=m_parameterizationGrids.begin(); it!=m_parameterizationGrids.end(); it++){
    if(it->first==gridIndex){
      it->second=insertedGrid1; //replace grid with index grindIndex with insertedGrid1
      it++;
      it=m_parameterizationGrids.insert(it,std::make_pair(gridIndex+1,insertedGrid2)); //insert insertedGrid2
      for(it++; it!=m_parameterizationGrids.end(); it++){//increase all following indices by one
        it->first=it->first+1;
      }
      break;
    }
  }
}
/**
* @brief returns the number of parameterizationGrids that can be assigned to the originalGrid
* @return number of parameterizationGrids
*/
unsigned OriginalParameterizationGrid::getNumParameterizationGrids()const
{
  return m_parameterizationGrids.size();
}
/**
* @brief sums up all durations until the given gridIndex
* @param[in] gridIndex
* @return duration
*/
double OriginalParameterizationGrid::sumUpDurations(const unsigned gridIndex)const
{
  std::vector<std::pair<unsigned, ParameterizationGrid::Ptr> >::const_iterator it=m_parameterizationGrids.begin();
  double duration=0;
  while(it->first!=gridIndex){
    duration += it->second->getDurationValue();
    it++;
  }
  return duration;
}
unsigned OriginalParameterizationGrid::getNumParameters()const
{
  return m_parameterization.size();
}
