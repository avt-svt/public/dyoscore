/**
* @file MetaDataOutput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the interface of the MetaData module for IO module\n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 05.09.2012
*/
#pragma once

#include <string>
#include <vector>
#include <map>
#include <cfloat>

namespace DyosOutput
{
  struct FirstSensitivityOutput
  {
    std::map<int, int> mapParamsCols;
    std::map<std::string, int> mapConstrRows;
    std::vector<std::vector<double> > values;
  };

  struct ParameterGridOutput
  {
    /** 
    * @enum ControlType
    * @brief enumeration of possible control types
    */
    enum ControlType
    {
      PieceWiseConstant = 1,
      PieceWiseLinear = 2 
    };
    std::vector<double> gridPoints;
    std::vector<double> values;
    std::vector<double> lagrangeMultiplier;
    double duration;
    bool isFixed;
    ControlType approximation;
    std::vector<FirstSensitivityOutput> firstSensitivityOutputVector;
    bool hasFreeDuration;
    
    ParameterGridOutput()
    {
      duration = 0.0;
      approximation = PieceWiseConstant;
      hasFreeDuration = true;
      isFixed = false;
    }
  };

  struct ParameterOutput
  {
    std::string name;
    double value;
    unsigned esoIndex;
    std::vector<ParameterGridOutput> grids;
    double lowerBound;
    double upperBound;
    bool isTimeInvariant;
    double lagrangeMultiplier;
    ParameterOutput()
    {
      name = "";
      value = 0.0;
      esoIndex = 0;
      lowerBound = 0.0;
      upperBound = 0.0;
      isTimeInvariant = false;
      lagrangeMultiplier = 0.0;
    }
  };

  struct StateGridOutput
  {
    std::vector<double> gridPoints;
    std::vector<double> values;
  };

  struct StateOutput
  {
    std::string name;
    unsigned esoIndex;
    StateGridOutput grid;
    StateOutput()
    {
      name = "";
      esoIndex = 0;
    }
  };

  struct IntegratorStageOutput
  {
    std::vector<ParameterOutput> durations;
    std::vector<ParameterOutput> parameters;
    std::vector<ParameterOutput> initials;
    std::vector<StateOutput> states;
    std::vector<StateGridOutput> adjoints;
    StateGridOutput adjoints2ndOrder;
  };



  struct ConstraintOutput
  {
    std::string name;
    unsigned esoIndex;
    double value;
    double timePoint;
    double lagrangeMultiplier;
    double lowerBound;
    double upperBound;
    ConstraintOutput()
    {
      name = "";
      esoIndex = 0;
      value = 0.0;
      timePoint = 0.0;
      lagrangeMultiplier = 0.0;
      lowerBound = 0.0;
      upperBound = 0.0;
    }
  };

  struct OptimizerStageOutput
  {
    ConstraintOutput objective;
    std::vector<ConstraintOutput> nonLinearConstraints;
    std::vector<ConstraintOutput> linearConstraints;
  };

  struct StageOutput
  {
    std::vector<double> stageGrid;
    IntegratorStageOutput integrator;
    OptimizerStageOutput optimizer;
  };
}
