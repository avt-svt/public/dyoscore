/**
* @file OptimizerMultiStageMetaData.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the OptimizerMultiStageMetaData class             \n
* according to MetaEso8.vsd                                            \n
* =====================================================================\n
* @author Klaus Stockmann, Tjalf Hoffmann
* @date 13.09.2012
*/

#pragma once

#include "OptimizerMetaData.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include "Mapping.hpp"
#include "IntegratorMultiStageMetaData.hpp"

/**
* @class OptimizerMultiStageMetaData
* @brief class implementing a multi-stage case of the OptimizerMetaData interface
* this includes information on mapping, on the contribution of objectives to the overall
* objective, on total time constraint and on stage independent constraints
*/
class OptimizerMultiStageMetaData:public virtual OptimizerMetaData
{
protected:
  //! vector of optimizer single stage meta data
  std::vector<OptimizerSingleStageMetaData::Ptr> m_optSSMDVec;
  /**
  * pointer to integrator multi-stage meta data
  * @remark access to mapping information via IntegratorMultiStageMetaData
  */
  IntegratorMultiStageMetaData::Ptr m_intMSMD;
  //! logical vector indicating if objective of a particular stage contributes to the overall objective
  std::vector<bool> m_treatObjective;

  // multistage data/methods
  /* lagrange multipliers of linear constraints, needed in multi-stage case. Currently
  *  only the constraint on the total time. In multiple shooting problems this will
  *  also contain constraints due to mappings of states.
  */
  vector<double> m_linConstraintsLagrangeMultipliers;
  
  std::vector<GlobalConstraint::Ptr> m_globalConstraints;

  /*
  * @struct totalTimeData
  * @brief structure containting data for total time constraint
  */
  struct totalTimeData {
    double lowerBound; // -1.0e20;
    double upperBound; // = 1.0e20;
    double constantDurationsValue; // = 0.0;
    bool isSet;        // = false;
  } m_totalTimeData;

  std::vector<double> assembleMSObjDer(const int stageIter, const std::vector<IntOptDataMap> msOptData) const;
  void assembleSSObjDer(const int stageIter,
                        const std::vector<IntOptDataMap> ssOptData,
                        std::vector<double> &objDerMS) const;
  void assembleNonLinJacobian();
  void assembleLinJacobian();
  
  void updateNonLinJacobianDecVarForNewGlobalConstraints
                      (const std::vector<GlobalConstraint::Ptr> &globalConstraints,
                       int jacRowsOffset);
  void updateNonLinJacobianConstParamForNewGlobalConstraints
                      (const std::vector<GlobalConstraint::Ptr> &globalConstraints,
                       int jacRowsOffset);
  
  virtual void getDecVarIndicesOfPreviousStages(std::vector<unsigned> &decVarIndices,
                                                const unsigned stageIndex) const;
  virtual void getConstParamIndicesOfPreviousStages(std::vector<unsigned> &constParamIndices,
                                                    const unsigned stageIndex) const;
  
  virtual void collectMultiStageGradients(CsTripletMatrix::Ptr globalDerivativeMatrix,
                                          const unsigned stageIndex,
                                          std::vector<unsigned> &varIndices) const;
  virtual void collectSingleStageGradients(CsTripletMatrix::Ptr globalDerivativeMatrix,
                                           const unsigned stageIndex,
                                           std::vector<unsigned> &varIndices) const;
public:
  typedef std::shared_ptr<OptimizerMultiStageMetaData> Ptr;
  OptimizerMultiStageMetaData();
  OptimizerMultiStageMetaData(const OptimizerMultiStageMetaData &copy);
  OptimizerMultiStageMetaData(std::vector<OptimizerSingleStageMetaData::Ptr> optSSMDVec,
                              IntegratorMultiStageMetaData::Ptr intMSMD,
                              std::vector<bool> treatObjective);
  virtual ~OptimizerMultiStageMetaData();

  void setConstraints(const std::vector<StatePointConstraint> &constraints);
  void setGlobalConstraints(const std::vector<GlobalConstraint::Ptr> &globalConstraints);

  //methods of the OptimizerMetaData interface
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const;
  virtual void getDecVarValues(utils::Array<double> &values) const;
  virtual void setDecVarValues(const utils::Array<const double> &values);
  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const;
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const;
  virtual void getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const;
  virtual void getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr &nonLinConstGradients) const;

  virtual double getNonLinObjValue() const;
  virtual void getNonLinObjDerivative(utils::Array<double> &values) const;
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers);
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const;
  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier);
  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier);
  // additional functions for multi-stage case
  virtual void setTotalTimeConstraint(const double lowerBound, const double upperBound,
                                      const double constantDurationsValue);

  virtual void getLinConstraintValues(utils::Array<double> &values) const;
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds) const;
  virtual void getLinConstraintDerivatives(CsTripletMatrix::Ptr linDerivativeMatrix);
  std::vector <DyosOutput::StageOutput> getOutput();
};

class OptimizerMSMetaData2ndOrder : public virtual OptimizerMetaData2ndOrder,
                                    public virtual OptimizerMultiStageMetaData
{
public:
  typedef std::shared_ptr<OptimizerMSMetaData2ndOrder> Ptr;
  OptimizerMSMetaData2ndOrder(std::vector<OptimizerSingleStageMetaData::Ptr> optSSMDVec,
                              IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                              std::vector<bool> treatObjective);
  virtual ~OptimizerMSMetaData2ndOrder();

protected:
  OptimizerMSMetaData2ndOrder(){}
  
  IntegratorMSMetaData2ndOrder::Ptr m_integratorMetaData2ndOrder;
  
  virtual void getNonLinConstraintHessian(CsTripletMatrix::Ptr &hessianMatrix,
                                          const std::vector<unsigned> &indices) const;
  
  virtual void getAdjoints(std::vector<double> &adjoints1stOrder,
                           CsTripletMatrix::Ptr &adjoints2ndOrder,
                           const std::vector<unsigned> &indices) const;

public:
  virtual void getNonLinConstraintHessianDecVar(CsTripletMatrix::Ptr &hessianMatrix) const;
  virtual void getNonLinConstraintHessianConstParam(CsTripletMatrix::Ptr &hessianMatrix) const;
  virtual void getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
                                 CsTripletMatrix::Ptr &adjoints2ndOrder) const;
  virtual void getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                     CsTripletMatrix::Ptr &adjoints2ndOrder) const;
};
