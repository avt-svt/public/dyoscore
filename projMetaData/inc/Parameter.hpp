/**
* @file Parameter.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* IntegratorSingleStageMetaData - Part of DyOS                                    \n
* =====================================================================\n
* This file contains the class definitions of the Parameter            \n
*                          according to the UML Diagramm METAESO_2     \n
* =====================================================================\n
* @author Fady Assassa, Klaus Stockmann
* @date 17.10.2011
*/
#pragma once

#include <vector>
#include "Array.hpp"
#include "cs.h"
//#include "boost/shared_ptr.hpp"
#include <memory>
#include "DyosOutput.hpp"
#include "MetaDataExtern.hpp"
#include "GenericEso.hpp"
#include "DyosObject.hpp"

//declare IntegratorSingleStageMetaData separately, because of an critical include circle
//if IntegratorSingleStageMetaData.hpp is included in this file
class IntegratorMetaData;
//class declarations for Ptr-types below. It is better to have them at top level of the header
class Control;
class Parameter;
class DurationParameter;
class ControlParameter;
class InitialValueParameter;

//! shared pointer to parameter objects
//typedef boost::shared_ptr<Parameter> ParameterPtr;
//! shared pointer to final DurationParameter objects
//typedef boost::shared_ptr<DurationParameter> DurationParameterPtr;
//! shared pointer to ControlParameter objects
//typedef boost::shared_ptr<ControlParameter> ControlParameterPtr;
//! shared pointer to InitialValueParameter objects
//typedef boost::shared_ptr<InitialValueParameter> InitialValueParameterPtr;

#define DURATION_LOWERBOUND_TOLERANCE 1e-3


/**
* @class Parameter
*
* @brief abstract parameter class used for initial value parameters, control parameters, final time parameters
*
* Main purpose of the class is to return sensitivity information (initial sensitivities and current rhs).
* Class deals with one parameter at a time, assembling the complete sensitivity vector for all parameters
* as used by the integrator is done in class IntegratorSingleStageMetaData. Class IntegratorSingleStageMetaData also handles
* conversion of explicit to implicit representation if needed. For each type of parameter (initial value,
* control, final time, there exists a derived class.
*/
class Parameter : public DyosObject
{
protected:
  //! value of the parameter
  double m_value;
  /*!
  * @var m_sensActivity
  * boolean value telling whether parameter is currently active (needed for control parameters)
  */
  bool m_sensActivity;
  /*!
  * @var m_withSensitivity
  * boolean value telling if the sensitivities are needed for this particular parameter. In some
  * cases, there are parameters for which no sensitivities are required.
  */
  bool m_withSensitivity;

  /**
  * @var m_isDecisionVariable
  * true, if parameter is a decision variable
  */
  bool m_isDecisionVariable;
  /*!
  * @var m_parameterIndex
  * index for sorting order of the parameter vector in IntegratorSingleStageMetaData.
  * The initial value is -1 meaning that the index must be set.
  */
  int m_parameterIndex;
  /**
  * @var m_decVarIndex
  * index of the decision variable (if set) which is needed for the Jacobian struct of the optimization problem
  * The value of -1 means, that the parameter is no decision variable
  */
  int m_decVarIndex;
  /*!
  * @var m_lowerbound
  * lower bound of the parameter
  */
  double m_lowerbound;
  /*!
  * @var m_upperbound
  * upper bound of the parameter
  */
  double m_upperbound;
  /**
  * @brief flag stating whether the parameter has been initialized or not
  *
  * If a parameter is newly activated, IntegratorMetaData has call the
  * getInitialValuesForIntegration function on the parameter. Once a Parameter
  * has been initialized that way the initial sensitivities provided by the
  * integrator is used.
  */
  bool m_isInitialized;

  /**
  * @var m_lagrangeMultiplier
  * lagrange multiplier (result of an optimization)
  */
  double m_lagrangeMultiplier;

  virtual void initializeParameterActivity(void);
  /**
  * @brief add DfDp to given array
  *
  * For sensitivity calculation chunks of the sensitivity vector are submitted in the
  * function getRhsSensForward1stOrder. DfDp vectors simply are added to the chunks. 
  * To avoid unnecessary allocation the DfDp content is directly added in this function.
  * @param y array to which DfDp is added
  * @param imd IntegratorMetaData pointer for function callbacks (eg getGenericEso)
  */
  virtual void addDfDp(utils::Array<double> &y,
                       IntegratorMetaData *imd) = 0;
public:
  typedef std::shared_ptr<Parameter> Ptr;
  Parameter();
  Parameter(const double value);
  Parameter(const Parameter &toCopy);
  ~Parameter();

  static void getDfDxTimesY(const utils::Array<double> &y,
                             const cs* jacobianFstates,
                             const double duration,
                             utils::Array<double> &result);
  virtual void getDfDp(std::vector<double> &paramDer,
                       IntegratorMetaData *imd);
  

  // getter und setter
  virtual void setParameterValue(const double value);
  virtual double getParameterValue(void) const;
  virtual void setParameterSensActivity(const bool activity);
  virtual bool getParameterSensActivity(void) const;
  virtual void setWithSensitivity(const bool withSens);
  virtual bool getWithSensitivity(void) const;
  virtual void setIsDecisionVariable(const bool isDec);
  virtual bool getIsDecisionVariable(void) const;
  virtual void setParameterLowerBound(const double lowerbound);
  virtual double getParameterLowerBound(void) const;
  virtual void setParameterUpperBound(const double upperbound);
  virtual double getParameterUpperBound(void) const;
  virtual void setParameterIndex(const int parameterIndex);
  virtual int getParameterIndex() const;
  virtual void setDecVarIndex(const int decVarIndex);
  virtual int getDecVarIndex() const;
  virtual int getEsoIndex() const;
  virtual void setIsInitialized(bool initialStatus);
  virtual bool getIsInitialized() const;
  virtual void setLagrangeMultiplier(const double multiplier);
  virtual double getLagrangeMultiplier() const;
  virtual  Parameter::Ptr getRelatedParameter()const;
  // Interfaces
  /**
  * @brief retrieve the initial sensitivity values of the parameter
  * @param initialValues array receiving the initial values
  */
  virtual void getInitialValuesForIntegration(utils::Array<double> &initialValues,
                                              GenericEso::Ptr &genEso) const = 0;
  // no pure virtual method: contains common code, the computation of the term f_x * s_p_i
  virtual void getRhsSensForward1stOrder(const utils::Array<double> &currentSens,
                                         const cs* jacobianFstate,
                                               IntegratorMetaData *imd,
                                               utils::Array<double> &rhsSens);
  virtual double getRhsDerReverse1stOrder(const utils::Array<double> &adjoints,
                                               IntegratorMetaData *imd);
  /**
  * @brief returns the DuDp index
  *
  * The DuDp index of a parameter is a index used for second order evaluation
  * For control parameters the DuDp index is -3 for not yet activated parameters, otherwise
  * it is the eso index. All other Parameter types have different negative DuDp indices.
  * @return DuDp index
  */
  virtual int getDuDpIndex() const = 0;
  virtual bool isOriginal() = 0;
  virtual double getDuDp(const IntegratorMetaData *imd) const = 0;
  static bool compareParameter(Parameter::Ptr p1, Parameter::Ptr p2);

  virtual DyosOutput::ParameterOutput getOutput(GenericEso::Ptr eso, IntOptDataMap iodm);

  static std::vector <DyosOutput::FirstSensitivityOutput> createSensitivityOutputVector
                                                 (IntOptDataMap iODP,
                                                  const GenericEso::Ptr &genPtr,
                                                  const std::vector <unsigned> &parameterIndices);

};


/**
* @class ControlParameter
*
* @brief class that administrates the sensitivity calculation of the Control Parameters
*/
class ControlParameter : public Parameter
{
protected:
  //! pointer to instance of Control class
  Control *m_control;
  virtual void addDfDp(utils::Array<double> &y,
                        IntegratorMetaData *imd);
public:
  typedef std::shared_ptr<ControlParameter> Ptr;
  ControlParameter();
  /**
   * @brief copy constructor
   *
   * @param[in] toCopy instance of class ControlParameter to be copied
   */
  ControlParameter(const ControlParameter &toCopy);
  ~ControlParameter();

  virtual int getEsoIndex() const;
  virtual int getDuDpIndex() const;
  virtual double getDuDp(const IntegratorMetaData *imd) const;
  void setControl(Control *control);
  virtual bool isOriginal();
  Control *getControl() const;

  void getInitialValuesForIntegration(utils::Array<double> &initialValues, GenericEso::Ptr &genEso) const;
};

/**
* @class InitialValueParameter
*
* @brief class that administrates the sensitivity calculation of the initial values
* @note InitialValueParameter can get a huge speedup by overriding the functions
*       getRhsSensForward1stOrder and getRhsDerReverse1stOrder. Most problems have only
*       comparatively few initialValueParameters so the speedup is expected to be
*       insignificant for a complete integration. To keep the code as simple as possible,
*       the functions are not overloaded.
*/
class InitialValueParameter : public Parameter
{
protected:
  //! index of corresponding variable
  unsigned m_esoIndex;
  unsigned m_stateIndex;
  unsigned m_equationIndex;
  bool m_bIsUserSet;

  virtual void addDfDp(utils::Array<double> &y,
                        IntegratorMetaData *imd);
public:
  typedef std::shared_ptr<InitialValueParameter> Ptr;
  InitialValueParameter();
  InitialValueParameter(const InitialValueParameter &toCopy);
  ~InitialValueParameter();
  void setUserFlag(bool bIsUserSet){m_bIsUserSet = bIsUserSet;};
  bool getUserFlag(){return m_bIsUserSet;};
  void setEsoIndex(const unsigned esoIndex);
  virtual int getEsoIndex() const;
  void setStateIndex(const unsigned stateIndex);
  unsigned getStateIndex() const;
  void setEquationIndex(const unsigned equationIndex);
  unsigned getEquationIndex() const;

  virtual int getDuDpIndex() const;
  virtual bool isOriginal();
  virtual double getDuDp(const IntegratorMetaData *imd) const;
  void getInitialValuesForIntegration(utils::Array<double> &initialValues, GenericEso::Ptr &genEso)const;
  DyosOutput::ParameterOutput getOutput(GenericEso::Ptr eso, IntOptDataMap iodm);

};

/**
* @class DurationParameter
*
* @brief class that administrates the sensitivity calculation of the final time value
*/
class DurationParameter : public Parameter
{
protected:
  virtual void addDfDp(utils::Array<double> &y,
                        IntegratorMetaData *imd);

  double m_durationMultiplier;
public:
  typedef std::shared_ptr<DurationParameter> Ptr;
  DurationParameter();
  DurationParameter(const DurationParameter &toCopy);
  DurationParameter(const double value);
  ~DurationParameter();

  virtual int getDuDpIndex() const;
  virtual int getEsoIndex() const;
  virtual void switchDurationMultiplier();
  virtual void resetDurationMultiplier();
  virtual double getDuDp(const IntegratorMetaData *imd) const;
  virtual bool isOriginal();
  void setParameterLowerBound(const double lowerbound);
  void getInitialValuesForIntegration(utils::Array<double> &initialValues, GenericEso::Ptr &genEso)const;


};







class SubstituteParameter :public ControlParameter
{
protected:
  Parameter::Ptr m_relatedParameter;
  Parameter::Ptr m_coParameter;
  double m_timePoint;

  virtual void addDfDp(utils::Array<double> &y,
                        IntegratorMetaData *imd);
public:
  SubstituteParameter(const ControlParameter::Ptr &relatedParameter,
                      const ControlParameter::Ptr &coParameter,
                      const double timePoint);
  SubstituteParameter( const SubstituteParameter &toCopy);

  virtual void setParameterValue(const double value);
  virtual double getParameterValue(void) const;
  virtual void setParameterSensActivity(const bool activity);
  virtual bool getParameterSensActivity(void) const;
  virtual void setWithSensitivity(const bool withSens);
  virtual bool getWithSensitivity(void) const;
  virtual void setIsDecisionVariable(const bool isDec);
  virtual bool getIsDecisionVariable(void) const;
  virtual void setParameterLowerBound(const double lowerbound);
  virtual double getParameterLowerBound(void) const;
  virtual void setParameterUpperBound(const double upperbound);
  virtual double getParameterUpperBound(void) const;
  virtual void setParameterIndex(const int parameterIndex);
  virtual int getParameterIndex() const;
  virtual int getEsoIndex() const;
  virtual void setIsInitialized(bool initialStatus);
  virtual bool getIsInitialized() const;
  virtual void getInitialValuesForIntegration(utils::Array<double> &initialValues, GenericEso::Ptr &genEso);
  virtual void getRhsSensForward1stOrder(const utils::Array<double> &currentSens,
                                         const cs* jacobianFstate,
                                               IntegratorMetaData *imd,
                                               utils::Array<double> &rhsSens);
  virtual double getRhsDerReverse1stOrder(const utils::Array<double> &adjoints,
                                               IntegratorMetaData *imd);
  virtual int getDuDpIndex();
  virtual double getDuDp(IntegratorMetaData *imd);
  virtual bool isOriginal();
  virtual  Parameter::Ptr getRelatedParameter()const;
  virtual DyosOutput::ParameterOutput getOutput(GenericEso::Ptr eso, IntOptDataMap iodm);
};
