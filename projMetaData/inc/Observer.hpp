// Author: Hans Pirnay
// Copyright: AVT.PT, RWTH Aachen
// Date: 2012-06-04

#pragma once
#ifdef WIN32
#pragma warning(disable : 4996)
#endif

#include "boost/shared_ptr.hpp"
#include <map>
#include "Array.hpp"
#include "DyosObject.hpp"

class Observer : public DyosObject
{
public:
  typedef boost::shared_ptr<Observer> Ptr;
  virtual void setStates(const unsigned timeIndex, const utils::Array<double> &states) =0;
  virtual utils::Array<double> getStates(const unsigned time) = 0;
  virtual void setAdjoints(const unsigned timeIndex, const utils::Array<const double> &states) =0;
  virtual utils::Array<double> getAdjoints(const unsigned time) = 0;
};

class IntegrationObserver : public Observer {
protected:
  std::map<unsigned, std::vector<double> > m_stateMap;
  std::map<unsigned, std::vector<double> > m_adjointMap;
public:
  typedef boost::shared_ptr<IntegrationObserver> Ptr;
  virtual void setStates(const unsigned timeIndex, const utils::Array<double> &states) {
    std::vector<double> vecStates(states.getData(), states.getData() + states.getSize());
    m_stateMap[timeIndex] = vecStates;
  }
  
  utils::Array<double> getStates(const unsigned timeIndex)
  {
    if(m_stateMap.count(timeIndex) > 0){
      std::vector<double> &vecStates = m_stateMap.find(timeIndex)->second;
      utils::Array<double> states(vecStates.size());
      std::copy(vecStates.begin(), vecStates.end(), states.getData());
      return states;
    }
    else{
      return utils::Array<double>(0);
    }
  }

  virtual void setAdjoints(const unsigned timeIndex, const utils::Array<const double> &adjoints) {
    std::vector<double> vecAdjoints(adjoints.getData(), adjoints.getData() + adjoints.getSize());
    m_adjointMap[timeIndex] = vecAdjoints;
  }

  utils::Array<double> getAdjoints(const unsigned timeIndex)
  {
    if(m_adjointMap.count(timeIndex) > 0){
      std::vector<double> &vecAdjoints = m_adjointMap.find(timeIndex)->second;
      utils::Array<double> adjoints(vecAdjoints.size());
      std::copy(vecAdjoints.begin(), vecAdjoints.end(), adjoints.getData());
      return adjoints;
    }
    else{
      return utils::Array<double>(0);
    }
  }

};

