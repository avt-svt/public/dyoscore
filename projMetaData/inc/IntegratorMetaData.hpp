/**
* @file IntegratorMetaData.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the abstract interface of the IntegratorMetaData  \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 30.01.2012
*/
#pragma once

//#define MAKE_DYOS_DLL
#include "Parameter.hpp"
#include "GenericEso.hpp"
#include "UserOutput.hpp"
#include "UserInput.hpp"
#include "DyosOutput.hpp"
#include "cs.h"
#include <map>
//#include <boost/shared_ptr.hpp>
#include <memory>
#include "MetaDataExtern.hpp"
#include "DyosObject.hpp"
#include "CscMatrix.hpp"
#include "TripletMatrix.hpp"

/** @brief data generated during integration but needed for optimization is saved here
*/

/**
* @class IntegratorMetaData
*
* @brief interface for access to all integration relevant data
*/
class IntegratorMetaData : public DyosObject
{
protected:
  //! saves the data required by the optimizer in the integrator. In case of single stage the vector has length 1.
  vector<IntOptDataMap> m_intOptData;

public:
  typedef std::shared_ptr<IntegratorMetaData> Ptr;
  /**
  * @brief destructor
  */
  virtual ~IntegratorMetaData(){}

  /**
  * @brief getter for structure StageOutput
  *
  * @param[in] StageInput structure
  * @param[in] StageOutput structure
  */
  virtual std::vector<DyosOutput::StageOutput> getOutput() = 0;

  /** @brief the total final time of each stage has to be updated, since
    * it is not a decision variable anymore and it is determined by the stage durations of the
    * underyling multiple grids
    */
  virtual void resetGolbalDurationParameter(void) = 0;
  /**
  * @brief set the start time of the current integration step
  *
  * An integration step is called the interval between two gridpoints.
  * Each control may have several parameterization grids. The set of integration
  * grids of the stage is created by the intersection of all parameterization
  * grids of all controls. For the integrator each integration grid has the
  * start time of 0.0 and the end time of 1.0, so the actual start and end times
  * of the integration grid are scaled and shifted.
  * @param[in] startTime new value for step start time - must be in range [0.0,1.0)
  */
  virtual void setStepStartTime(const double startTime) = 0;

  /**
  * @brief get the start time of the current integration step
  *
  * @return current step start time
  * @sa IntegratorMetaData::setStepStartTime
  */
  virtual double getStepStartTime()const = 0;

 /**
 * @brief set the end time of the current integration step
 *
 * @param[in] endTime new value for step end time - must be in range (0.0,1.0]
 * @sa IntegratorMetaData::setStepStartTime
 */
  virtual void setStepEndTime(const double endTime) = 0;

  /**
  * @brief get the end time of the current integration step
  *
  * @return current step end time
  * @sa IntegratorMetaData::setStepStartTime
  */
  virtual double getStepEndTime()const = 0;

  /**
  * @brief get the number of stages managed by this class
  * @return the number of stages
  */
  virtual unsigned getNumStages() const = 0;

  /**
  * @brief set the current integration step time
  *
  * The current step time may be of any value between the start time and the
  * end time of the current integration step
  * @param[in] time new value for current step time
  * @sa IntegratorMetaData::setStepStartTime
  */
  virtual void setStepCurrentTime(const double time) = 0;

  /**
  * @brief get the current integration step time
  *
  * @return current integration step time
  * @sa IntegratorMetaData::setStepCurrentTime
  */
  virtual double getStepCurrentTime()const = 0;

  /**
  * @brief returns the total stage time required by the spline evaluation
  *
  * A stage time is the unscaled time on the current stage in the limits of
  * 0.0 and the global final time.
  * A stage may consist several integration grids. These integration grids are
  * computed by intersection of all control's parameterization grids. So the
  * parameterization grids of the controls may overlap, so the current step time
  * might refer to an integration grid interval with limits that are different
  * from the limits of the controls own parameterization grid. To evaluate a spline
  * value of a control parameter the unscaled time of the stage is needed to
  * identify the actual time point of the current parameter.
  * @return total stage time
  */
  virtual double getStageCurrentTime()const=0;

  /**
  * @brief get the current duration value
  *
  * The complete integration interval may be divided by the user in separate
  * integration stages. Whenever the user defines an additional parameterization
  * grid for a control, a new integration with an own duration is created.
  * @return duration of the current integration grid
  * @sa IntegratorMetaData::getStageCurrentTime
  */
  virtual double getIntegrationGridDuration()const = 0;

  // Integrator Interface methods

  /**
  * @brief get the right hand side of the 1st order forward sensitities
  *
  * @param[in] inputSensitivities array of input sensitivities
                                - must be of size getNumEquations*numParameters
  * @param[out] rhsSensitivities array of output rhs sensitivities
                               - must be of size numEquations*numParameters
  * @note the function calculateRhsStates must be called first
  */
  virtual void calculateSensitivitiesForward1stOrder(const utils::Array<double> &inputSensitivities,
                                                           utils::Array<double> &rhsSensitivities) = 0;
  /**
  * @brief evaluate the right hand side of all equations
  *
  * Evaluate the right hand side of the system Mx' = tf*F(x,y,u,p)
  * To do so the states are set to the given values, the control variables are set to
  * the current values and x' is set to zero. Then the residuals are evaluated.
  * As the states are scaled state values, the residuals must be unscaled before output
  *
  * @param[in] inputStates current states to be set before evaluation
  * @param[out] rhsStates residuals of all equations (derivatives are set to zero)
  * @note this function must be called before calling function
  *       calculateSensitivitiesForward1stOrder depending on the states to be set
  *       as well
  * @note This function will not work if metaEso is not properly initialized.
  *       At least initializeForFirstIntegration and initializeForNextIntegrationStep
  *       must have been called.
  *       The current time (sa setCurrentTime) must have been updated as well.
  */
  virtual void calculateRhsStates(const utils::Array<double> &inputStates,
                                        utils::Array<double> &rhsStates) = 0;

  /**
  * @brief evaluate the rhsAdjoints and the adjoint derivatives
  * @param[in] states saved states at the current grid point - must be of size numStates
  * @param[in] adjoints must be of size numStates
  * @param[in] rhsAdjoints rhs adjoints to be calculated - must be of size numStates
  * @param[in] rhsDers derivatives of the adjoints to be calculated - must be of size numParams
  */
  virtual void calculateAdjointsReverse1stOrder(const utils::Array<double> &states,
                                                const utils::Array<double> &adjoints,
                                                      utils::Array<double> &rhsAdjoints,
                                                      utils::Array<double> &rhsDers) = 0;


  virtual void calculateRhsAdjoints(const utils::Array<double> &adjoints,
                                          utils::Array<double> &rhsAdjoints) = 0;

  virtual void calculateRhsStatesDtime(utils::Array<double> &rhsDTime) = 0;

  /**
  * @brief set states in genericEso
  *
  * @param[in] states array containing the states to be set - must be of size numStates
  * @note this function does not set the values of the controls as in calculateRhsStates
  */
  virtual void setStates(const utils::Array<double> &states) = 0;

  /**
  * @brief store initial states in observer
  *
  * After the dae initialization all states have to be stored for the output, since the 
  * function setStates is never called for the initial values
  * @param[in] states array containing the states to be set - must be of size numStates
  */
  virtual void storeInitialStates(const utils::Array<double> &states) = 0;
  
  /**
  * @brief store the current sensitivities into m_currentSensitivities
  *
  * this function also stores the relevant sensitivities for the active constraints
  * @param[in] sens array containing the current sensitivity values
  */
  virtual void setSensitivities(const utils::Array<double> &sens) = 0;

  /**
  * @brief stores adjoints and the first order derivatives of the Lagrangian.
  *
  * @param[in] adjoints array containing the current adjoint values
  * @param[in] lagrangeDerivatives array containing the current values of the
  *                                derivatives of the Lagrange function
  */
  virtual void setAdjointsAndLagrangeDerivatives(
                                    const utils::Array<double> &adjoints,
                                    const utils::Array<double> &lagrangeDerivatives) = 0;

  /**
  * @brief set adjoint values in the lagrange derivatives
  *        for all initial value parameters with sensitivity
  */
  virtual void setInitialLagrangeDerivatives() = 0;

  /**
  * @brief adds lagrange multipliers of the constraints at the current timepoint to the adjoints
  *        saved in the IntegratorMetaData object
  *
  * This function performs the operation described in eq. (4.9) in the paper: "Continuous and
  * discrete composite adjoints for the Hessiian of the Lagrangian in shooting algorithms for
  * dynamic optimization" (Hannemann et al. 2010).
  */
  virtual void updateAdjoints() = 0;

  /**
  * @brief for every new integration step - IntegratorSingleStageMetaData must be initialized
  *
  * First step of initialization is to determine the new Integration interval,
  * so after execution of this initialize function the Integrator may request the
  * interval range of the new integration step.
  * Then all parameters are updated, so the set of active parameters  and
  * probably the number of sensitivity parameters will change.
  */
  virtual void initializeForNewIntegrationStep() = 0;

  /**
  * @brief ininitalize integration step for backwards integration
  * @sa initializeForNewIntegrationStep
  */
  virtual void initializeForNewBackwardsIntegrationStep() = 0;

  /**
  * @brief initialize IntegratorSingleStageMetaData for next integration block
  *
  * The user may define more than one time grid for a control. In that case the
  * control needs to be integrated over both grids each having its own final time.
  * The integrator handles this case as several singular integrations on the grid
  * assuming the integration range to be [0,1]. However between these two integration
  * blocks the vector of inactive parameters stays the same and the output of the previous
  * integration is kept in memory.
  * This initialization method determines the new scaled final time, the index for
  * the integration grid vector is incremented and the grid point index is reset to 0
  */
  virtual void initializeForNextIntegration() = 0;

  /**
  * @brief initialize the previous integration block for backward integration
  * @sa intializeForNextIntegration
  */
  virtual void initializeForPreviousIntegrationBlock() = 0;

  /**
  * @brief initialize IntegratorSingleStageMetaData for a new complete integration
  *
  * For that purpose all index variables have to be reset and the output
  * data vectors have to be emptied. All controls must be set to its initial states.
  * The parameters vector must be cleared as well and be filled with the initial value parameters,
  * the first final time parameters and the active control parameters for the first grids
  */
  virtual void initializeForFirstIntegration() = 0;

  /**
  * @brief initialize structures of Jacobian matrices and initialize BMatrix
  */
  virtual void initializeMatrices() = 0;
  
  
  /**
  * @brief initializes the adjoints and the derivatives of the Lagrangian.
  *
  * This function has to be called once at the beginning of the reverse integration
  * of each stage at the stage final time.
  */
  virtual void initializeAtStageDuration() = 0;

  /**
  * @brief retrieve the M-Matrix of the model
  *
  * The model is given in the form of Mx' = F(x,y,p) and M is expected to be constant.
  * @return M-Matrix in csc format
  */
  virtual CsCscMatrix::Ptr getMMatrix(void) const = 0;
  virtual CsTripletMatrix::Ptr getMMatrixCoo(void) const = 0;
  /**
  * @brief get initial states and initial sensitivities
  *
  * @param[out] initialStates array receiving the initial states - must be of size
  *                           getNumStates
  * @param[out] initialSensitivities array reveiving the initial sensitivities -
  *                                  must be of size getNumStates*getNumEquations
  */
  virtual void evaluateInitialValues(utils::Array<double> &initialStates,
                                     utils::Array<double> &initialSensitivities) = 0;


  /**
  * @brief getter for the number of the current parameters with sensitivity
  *
  * @return number of current parameters
  */
  virtual int getNumCurrentSensitivityParameters() const = 0;


  
  virtual void getSensitivityParameters(std::vector<Parameter::Ptr> &sensParams) const = 0;

  /**
  * @brief getter for the number of integration intervals
  *
  * Usually there is only one integration interval. However, the user may specify
  * additional Parameterization grids on a control and thus potentially adding
  * additional integration intervals.
  *
  * @return the total number of inftegration intervals
  */
  virtual int getNumIntegrationIntervals() const = 0;

  /**
  * @brief set all initial values with sensitivities. e.g. multiple shooting
  *
  */
  virtual void setAllInitialValuesFree() = 0;

  /**
  * @brief get the GenericEso object of this object
  *
  * If any Eso functions are to be called use this object.
  * @return shared pointer to the GenericEso object of this object
  */
  virtual GenericEso::Ptr getGenericEso() = 0;

  /**
  * @brief get the number of nonzero entries from the state Jacobian
  *
  * @return number of nonzeroes
  */
  virtual int getNumStateNonZeroes() = 0;

  /**
  * @brief returns the entire duration of the stage
  * @return stage duration
  */
  virtual double getStageDuration()const = 0;

  // set eso parameters to current control values
  virtual void setControlsToEso() = 0;
  // Parameter interface methods
  // no differentiation between time independent and control parameters

  /**
  * @brief get the number of all parameters with sensitivity
  *
  * The number of all parameters with sensitivity is equal to the result of the function
  * getNumCurrentSensitivityParameters at the end of an integration
  *
  * @return number of parameters with sensitivity
  */
  virtual unsigned getNumSensitivityParameters() const = 0;

  /**
    * @brief retrieve the partial matrix including only control parameter nonzeroes
    *
    * The control nonzeroes are extracted from the full Jaobian keeping the original
    * matrix format. Practically the state nonzeroes are simply removed from the original.
    * @note constant controls are considered as control parameters too
    * @return partial control parameter Jacobian matrix in csc format
    */
  virtual CsCscMatrix::Ptr getJacobianFup() = 0;
  /**
    * @brief retrieve the partial matrix including only state nonzeroes
    *
    * The state nonzeroes are extracted from the full Jacobian reducing the matrix
    * column dimension to numStates
    * @return partial state Jacobian matrix in csc format
    */
  virtual CsCscMatrix::Ptr getJacobianFstates() = 0;

  /**
  * @brief returns the current sensitivity information
  * @param[out] curSens double vector of the current sensitivity information
  */
  virtual void getCurrentSensitivities(std::vector<double> &curSens) const = 0;

  /**
  * @brief returns the current adjoint information
  * @param[out] adjoints double array of the current adjoint information
  */
  virtual void getAdjoints(std::vector<double> &adjoints) const = 0;

  /**
  * @brief returns the current Lagrange derivatives information
  * @param[out] LagrangeDers double array of the current Lagrange derivatives information
  */
  virtual void getLagrangeDers(std::vector<double> &LagrangeDers) const = 0;

  /**
  * @brief get the data struct needed by the OptimizerMetaData
  * @return the struct containing the collected data during an integration
  */
  virtual std::vector<IntOptDataMap> getIntOptData(void)const{return m_intOptData;};

  /**
  * @brief reset metaData for new integration
  */
  virtual void reset(){}

  /**
   * @brief call this function if a root has been found.
   *
   * This function takes care of everything that needs to be done when a root of a switching function is found
   */
  virtual void foundRoot() {}
  
  /**
  * @brief activates plot grid
  *
  * If state information is needed on a finer grid than the optimization grid, additional timepoint are inserted
  * in the parameter grids. If used in optimization, the Constraints refer to indices of timepoints and the 
  * insertion of the plotgrid may change the time point indices. So this function must be called before creation
  * of an OptimizationProblem object.
  */
  virtual void activatePlotGrid() = 0;
  
  virtual void setIntOptData(){}
};

/**
* @brief struct uniting all input parameters of function
*        IntegratorMetaData2ndOrder::calculateRhsLagrangeDers2ndOrder
*/
struct Eso2ndOrderEvaluation
{
  ///first-order state senstivities with respect to parameter p_i - must be of size numStates
  std::vector<double> sensWrtP_i;
  /**
  * equals du/dp_i, the derivative evaluation of all Eso parameters wrt
  * to sens parameter p_i - must be of size numEsoParameters
  */
  std::vector<double> dEsoParamsDp_i;
  /**
  * must be of size numEsoParameters and corresponds to
  * tf * (adj1stOrder)^T * [d2/dudx(f) d2/du2(f)][dx/dp_i du/dp_i]^T +
  * (adj2ndOrd)^T * d/du(f)
  * where u are the ESO parameters, x the states and tf the duration corresponding
  * to IntegratorSingleStageMetaData::getDuration(). If p_i corresponds to a
  * initial value parameter or a final time parameter, then the input was
  * calculated with du/dp_i = 0.
  */
  std::vector<double> t2A1EsoParameters;
};


/**
* @class IntegratorMetaData2ndOrder
* @brief IntegratorMetaData class with 2nd order derivatives
*/
class IntegratorMetaData2ndOrder : public virtual IntegratorMetaData
{
public:
  typedef std::shared_ptr<IntegratorMetaData2ndOrder> Ptr;
  virtual ~IntegratorMetaData2ndOrder(){}

  /**
  * @brief evaluate the rhsLagrangeDers for 2nd order
  *
  * This function has to be called once for every sensitivity parameter p_i.
  * Calculates rhsDers2ndOrder =
  *                   (dadjoints/dp_i)^T * f_p +
  *                   adjoints^T * (d2/dudx(f) * dx/dp_i + d2/du2(f) * du/dp_i) * du/dp
  * where 'd' stands for partial derivative, x for states, p for the vector of all decision
  * variables, u for the ESO parameters and p_i stands for the actual sensitivity parameter
  * @note This function is not a function meant to be called by the integrator. But since
  *       this function is to be called by other objects as well, it has to be public and in
  *       the abstract interface.
  *
  * @param[in] adjoints1stOrder first-order adjoint variables - must be of size numEquations
  * @param[in] adjoints2ndOrder second-order adjoint variables. They correspond to d/dp_i
  *                             (adjoints1stOrder) where p_i is the actual sensitivity parameter
  *                             - must be of size numEquations
  * @param[in] sensWrtP_i first-order state senstivities with respect to parameter p_i -
  *                       must be of size numStates
  * @param[in] dEsoParamsDp_i equals du/dp_i, the derivative evaluation of all Eso parameters wrt
  *                           to sens parameter p_i - must be of size numEsoParameters
  * @param[in] t2A1EsoParameters must be of size numEsoParameters and corresponds to
  *                 tf * (adj1stOrder)^T * [d2/dudx(f) d2/du2(f)][dx/dp_i du/dp_i]^T +
  *                 (adj2ndOrd)^T * d/du(f)
  *                 where u are the ESO parameters, x the states and tf the duration corresponding
  *                 to IntegratorSingleStageMetaData::getDuration(). If p_i corresponds to a
  *                 initial value parameter or a final time parameter, then the input was
  *                 calculated with du/dp_i = 0.
  * @param[out] rhsDers2ndOrder contains D/Dp_i(adjoints1stOrder * f_p(t,x,p)) after the call
  *                 - must be of size numTotalSensParameter. p represents all sensitivity parameters
  */
  virtual void calculateRhsLagrangeDers2ndOrder(utils::Array<double> &adjoints1stOrder,
                                                utils::Array<double> &adjoints2ndOrder,
                                                Eso2ndOrderEvaluation &input,
                                                utils::Array<double> &rhsDers2ndOrder) = 0;
  /**
  * @brief evaluate the rhsAdjoints and the adjoint derivatives for 2nd order
  *
  * @param[in] states saved states at the current grid point - must be of size numStates
  * @param[in] sens saved sens at the current grid point - must be of size
  *                 getNumCurrentSensitivityParameters() * numStates
  * @param[in] adjoints1stAnd2nd adjoints of first and second order - must be of size
  *                              (getNumDecsisionVariables() + 1) * numEqns
  * @param[out] rhsAdjoints2ndOrder rhs adjoints for second order to be calculated - must be of
  *                                 size numEqns * getNumDecsisionVariables()
  * @param[out] rhsDers2nd rhs for the second order derivatives of the Lagrange function -
  *                        must be of size getNumDecsisionVariables() * getNumDecsisionVariables()
  */
  virtual void calculateAdjointsReverse2ndOrder(utils::Array<double> &states,
                                                utils::Array<double> &sens,
                                                utils::Array<double> &adjoints1stAnd2nd,
                                                utils::Array<double> &rhsAdjoints2nd,
                                                utils::Array<double> &rhsDers2nd) = 0;

  /**
  * @brief get the GenericEso2ndOrder object of 'this' object
  *
  * If any Eso functions are to be called use this object.
  * @return shared pointer to the GenericEso2ndOrder object of 'this' object
  */
  virtual GenericEso2ndOrder::Ptr getGenericEso2ndOrder() const = 0;

};
