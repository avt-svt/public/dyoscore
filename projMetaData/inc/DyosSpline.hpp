/** @file DyosSpline.hpp
*    @brief Specific spline required by DyOS. Automatically updates the decision variables
*    for the call to the Spline tool.
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 29.05.2012
*/
#pragma once


#include <vector>
#include <ostream>
#include "Spline.hpp"
#include "Parameter.hpp"
#include "DyosObject.hpp"

/** @brief Dyosspline is a class that wraps around the Spline class in utils*/
class DyosSpline : public Spline,
                   public DyosObject
{
protected:
  //! @brief vector of parameters at time points t
  std::vector<ControlParameter::Ptr> m_parameters;

  void updateSpline(void);

public:
  //! @brief standard constructor
  DyosSpline(){};

  //! @brief constructor
  DyosSpline(const std::vector<double> &t, const std::vector<ControlParameter::Ptr> &x, const unsigned order);
  //! @brief copy constructor
  DyosSpline(const DyosSpline &splineIn);
  
  //! @brief destructor
  ~DyosSpline(){};

  double eval(const double t);
  double dSpline(const double tp, const unsigned splineIndex);
};

