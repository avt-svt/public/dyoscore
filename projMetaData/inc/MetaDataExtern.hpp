/**
* @file MetaDataExtern.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the external interface of the IntegratorMetaData  \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 29.08.2012
*/
#pragma once
#include "Array.hpp"
#include <vector>
#include <map>

struct SavedOptDataStruct{
  //! number of active sensitivity parameter
  unsigned numSensParam;
  //! time point of the struct entry
  double timePoint;
  //! nonlinear constraint eso indices
  std::vector<int> nonLinConstVarIndices;
  /**
  * other than nonLinConstVarIndices these only refer to states - so first state has index 0
  * regardless what its eso index is
  */
  std::vector<unsigned> nonLinConstStateIndices;
  //! indices of active sensitivity parameters according to parameter class
  std::vector<unsigned> activeParameterIndices;
  std::vector<int> activeDecVarIndices;
  //! constraint index in the constraint Jacobian matrix
  std::vector<unsigned> nonLinConstIndices;
  //! lagrange multipliers (needed for backwards integration)
  std::vector<double> lagrangeMultipliers;
  //! nonlinear constraint values
  utils::ExtendableArray<double> nonLinConstValues;
  //! nonlinear constraint gradients
  utils::ExtendableArray<double> nonLinConstGradients;

  SavedOptDataStruct() 
  {
    numSensParam = 0;
    timePoint = 0.0;
  }
};
//! @brief map including all the data required by the optimizer sorted by the time points
typedef std::map <unsigned, SavedOptDataStruct> IntOptDataMap;


unsigned findTimeIndex(IntOptDataMap &optData, const double timePoint);