/** 
* @file Constraints.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaSingleStageEso - Part of DyOS                                    \n
* =====================================================================\n
* This file contains the classdefinitions of StaetPathConstraints      \n
* and StatePointConstraint according to the UML Diagramm METAESO_2     \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 22.8.2011
*/
#pragma once

#include <vector>
#include "GenericEso.hpp"
#include "MetaDataOutput.hpp"
#include "DyosObject.hpp"
#include "Parameter.hpp"
#include "IntegratorSingleStageMetaData.hpp"

class OptimizerSingleStageMetaData;

/**
* @class StatePointConstraint
*
* @brief defining a constraint at a fixed time point for a given variable
*/
class StatePointConstraint : public DyosObject
{
protected:
  //! index of correspondong variable
  unsigned m_esoIndex;
  //! scaled time point of the constraint (must be between 0 and 1)
  double m_time;
  //! lower bound of the constraint
  double m_lowerBound;
  //! upper bound of the constraint
  double m_upperBound;
  //! lagrange multiplier of the constraint
  double m_lagrangeMultiplier;
  //! index of the gridpoint, used instead of double as double comparison is dangerous
  unsigned m_timeIndex;
public:
  StatePointConstraint();
  StatePointConstraint(const StatePointConstraint &statePointConstraint);
  ~StatePointConstraint();
  void setEsoIndex(const unsigned esoIndex);
  unsigned getEsoIndex() const;
  void setTime(const double time);
  double getTime() const;
  void setLowerBound(const double lowerBound);
  double getLowerBound() const;
  void setUpperBound(const double upperBound);
  double getUpperBound() const;
  void setLagrangeMultiplier(const double lagrangeMultiplier);
  double getLagrangeMultiplier() const;
  void setTimeIndex(const unsigned timeIndex);
  unsigned getTimeIndex() const;
  
  DyosOutput::ConstraintOutput getOutput(const GenericEso::Ptr &eso);
};

/**
* @class GlobalConstraint
* @brief constraint that is handled on the multistage level
*/
class GlobalConstraint : public StatePointConstraint
{
public:
  typedef std::shared_ptr<GlobalConstraint> Ptr;
  /**
  * @brief calculate the value of the constraint
  * @param stages OptimizerSingleStageMetaData stages used to evaluate constraint
  * @return constraint value
  */
  virtual double evaluateConstraint(const std::vector<std::shared_ptr
                                             <OptimizerSingleStageMetaData> > &stages) = 0;
  /**
  * @brief add constraint to the intOptDataMaps of optStages
  * @param optStages OptimizerSingleStageMetaData stages used to add constraint
  * @param constraintIndex global index of the constraint (set by multistage object)
  */
  virtual void addConstraint(std::vector<std::shared_ptr
                                      <OptimizerSingleStageMetaData> > &optStages,
                             const unsigned constraintIndex) = 0;
  /**
  * @brief evaluate constraint sensitivities
  * @param optStages OptimizerSingleStageMetaData stages used to evaluate sensitivities
  * @param derivativeMatrix matric receiving the sensitivities
  * @param varIndices indices of variables for which sensitivities are evaluated
  *                   (e.g. all decision variables)
  * @param constraint index global index of the constraint
  */
  virtual void evaluateConstraintSens(const std::vector<std::shared_ptr
                                          <OptimizerSingleStageMetaData> > &optStages,
                                            CsTripletMatrix::Ptr derivativeMatrix,
                                            std::vector<unsigned> varIndices,
                                      const unsigned constraintIndex) = 0;
  /**
  * @brief retrieve the column indices of all decision variables
  * @param optStages OptimizerSingleStageMetaData stages used to evaluate indices
  * @return vector of column indices of the decision variables
  */
  virtual std::vector<unsigned> getJacobianDecVarColIndices
                                   (const std::vector<std::shared_ptr
                                            <OptimizerSingleStageMetaData> > &optStages) = 0;
  /**
  * @brief retrieve the column indices of oll constant parameters
  * @param optStages OptimizerSingleStageMetaData stages used to evaluate indices
  * @return vector of column indices of the constant parameters
  */
  virtual std::vector<unsigned> getJacobianConstParamColIndices
                                   (const std::vector<std::shared_ptr
                                            <OptimizerSingleStageMetaData> > &optStages) = 0;
};

/**
* @class MultipleShootingConstraint
* @brief constraint for multiple shooting
*
* this constraints states that for the given stage index n the initial 
* value parameter of stage n+1 must be equal the state value at the endpoint of stage n.
*/
class MultipleShootingConstraint : public GlobalConstraint
{
protected:
  /// initialparameter of next stage corresponding to this constraint
  InitialValueParameter::Ptr m_initValParNextStage;
  ///total number of parameters of stages 0..m_stageIndex-1
  unsigned m_numParametersPreviousStages;
  unsigned m_stageIndex; ///< index of the constraint border
public:
  MultipleShootingConstraint();
  void setInitialValueParameterNextStage(InitialValueParameter::Ptr initValPar);
  void setStageIndex(unsigned stageIndex);
  virtual double evaluateConstraint(const std::vector<std::shared_ptr
                                              <OptimizerSingleStageMetaData> > &stages);
  virtual void addConstraint(std::vector<std::shared_ptr
                                <OptimizerSingleStageMetaData> > &optStages,
                             const unsigned constraintIndex);
  virtual void evaluateConstraintSens(const std::vector<std::shared_ptr
                                            <OptimizerSingleStageMetaData> > &optStages,
                                            CsTripletMatrix::Ptr derivativeMatrix,
                                            std::vector<unsigned> varIndices,
                                      const unsigned constraintIndex);
  virtual std::vector<unsigned> getJacobianDecVarColIndices
                                   (const std::vector<std::shared_ptr
                                            <OptimizerSingleStageMetaData> > &optStages);
  virtual std::vector<unsigned> getJacobianConstParamColIndices
                                   (const std::vector<std::shared_ptr
                                            <OptimizerSingleStageMetaData> > &optStages);
};