/**
* @file MetaDataException.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData Exceptions - Part of DyOS                                   \n
* =====================================================================\n
* This file contains the Exceptions used in MetaData classes           \n
* =====================================================================\n
* @author Hans Pirnay
* @date 2012-10-16
*/

#include <exception>

class MetaDataException : public std::exception {
  std::string m_msg;
public:
  MetaDataException(const std::string& msg) : m_msg("MetaDataException: " + msg) {}
  virtual ~MetaDataException() throw() {}
  virtual const char* what() const throw() {
    return m_msg.c_str();
  }
};

class MissingOutputException : public MetaDataException
{
public:
  MissingOutputException() : 
    MetaDataException("output fields had fewer entries than expected.") {}
  MissingOutputException(const std::string &msg) : MetaDataException(msg) {}
};

class EsoEvaluationErrorException : public MetaDataException
{
public:
  EsoEvaluationErrorException() : MetaDataException("eso output let to indefinite numbers") {}
  EsoEvaluationErrorException(const std::string &msg) : MetaDataException(msg) {}
};

class ControlGridPointsTooCloseException : public MetaDataException
{
public:
  ControlGridPointsTooCloseException() 
   : MetaDataException("Control grids produced a global grid,"
                       "in which too gridpoints were too close together."){}
};

class MassMatrixIsNotTimeInvariantException : public MetaDataException
{
public:
  MassMatrixIsNotTimeInvariantException()
    : MetaDataException("Used a model with a non constant mass matrix,"
                        "this cannot be handled by Dyos"){}
};

class ModelHasConditionsException : public MetaDataException
{
public:
  ModelHasConditionsException()
    : MetaDataException("Up to now handling of conditions is not implemented."){}
};

class InitialSectionException : public MetaDataException
{
public:
  InitialSectionException()
    : MetaDataException("An error occured while initializing differential states."){}
  InitialSectionException(const std::string &message) : MetaDataException(message) {}
};