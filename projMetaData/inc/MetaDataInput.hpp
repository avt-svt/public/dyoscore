/**
* @file MetaDataInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData                                                             \n
* =====================================================================\n
* This file contains the declarations of the input structs for creating\n
* IntegratorMetaData and OptimizerMetaData objects.                    \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 15.02.2012
*/

#pragma once

#include "EsoInput.hpp"
#include "GridRefinementInput.hpp"
#include "Mapping.hpp"

#include <vector>
#include <string>

namespace FactoryInput
{

  /**
  * @struct ParameterizationGridInput
  * @brief information provided by user to create a ParameterizationGrid object
  */
  struct ParameterizationGridInput
  {
    ControlType approxType; ///< approximation type of the grid
    std::vector<double> timePoints; ///< given grid points
    std::vector<double> values; ///< given values on the grid points
    double duration; ///< duration of the grid
    bool parametersFixed;
    bool hasFreeDuration; ///< true, if duration is an optimization parameter, false if duration is fixed
    int pcresolution; ///< resolution of path constraint with respect to the control grid
    std::vector<double> explicitPlotGrid;
    AdaptationInput adapt;
    ParameterizationGridInput()
    {
     parametersFixed = false;
     pcresolution = 1;
     duration = 0.0;
     hasFreeDuration = false;
     approxType = PieceWiseConstant;
    }
  };

    /** @struct StructureDetectionInput
   *  @brief contains the information required for structure detection. Structure
   *  detection takes place within one stage. Not over stage boundaries, thus it is located
   *  in the stage input
   */
  struct StructureDetectionInput
  {
    unsigned maxStructureSteps;
    bool createContinuousGrids;
    StructureDetectionInput() : maxStructureSteps(0), createContinuousGrids(false){};
  };

  /**
  * @struct ParameterInput
  * @brief information provided by user to create either Control or Parameter objects
  */
  struct ParameterInput
  {
    /// @brief type of the parameter or control to be created
    enum ParameterType
    {
      INITIAL, ///< create a InitialValueParameter
      CONTROL, ///< create a Control
      CONTINUOUS_CONTROL, ///<create a Control with continuous grids in structure detection
      DURATION, ///< create a DurationParameter
      TIME_INVARIANT ///< create a Control with an one-interval grid
    };

    /// @brief state of the sensitivity information
    enum ParameterSensitivityType
    {
      NO, ///no sensitivity information needed
      FRACTIONAL, ///calculate only mixed Hessian - no decision variable
      FULL ///calculate full sensitivity matrix - decision variable
    };

    ParameterType type; ///< type of the Control or Parameter
    ParameterSensitivityType sensType; ///< type describing wether parameter is with sensitivity information
    std::vector<ParameterizationGridInput> grids; ///< vector of grids defined (for Control only)
    unsigned esoIndex; ///< user given eso index of the Parameter or Control
    double lowerBound; ///< user given lower bound of the Parameter or Control
    double upperBound; ///< user given upper bound of the Parameter or Control
    double value; ///< user given value (for Parameter only)
    //! constructor predefining types
    ParameterInput()
    {
      type = CONTROL;
      sensType = FULL;
      esoIndex = 0;
      lowerBound = 0.0;
      upperBound = 0.0;
      value = 0.0;
    }
  };

  struct MappingInput
  {
    enum MappingType
    {
      SINGLE_SHOOTING,
      MULTIPLE_SHOOTING
    };
    MappingType type;
    std::map<unsigned, unsigned> varIndexMap;
    std::map<unsigned, unsigned> eqnIndexMap;
    //! constructor predefining types
    MappingInput()
    {
      type = SINGLE_SHOOTING;
    }
  };

  /**
  * @struct IntegratorMetaDataInput
  * @brief infomration provided by user to create an IntegratorMetaData object
  */
  struct IntegratorMetaDataInput
  {	  
	typedef std::shared_ptr<IntegratorMetaDataInput> Ptr;
    /// input information for all single stages in multi stage mode
    std::vector<IntegratorMetaDataInput> stages;
    std::vector<MappingInput> mappings; ///< input information for all mappings in multi stage mode
    std::vector<ParameterInput> controls; ///< input information for all Controls
    std::vector<ParameterInput> initials; ///< input information for all InitialValueParameters
    ParameterInput duration; ///< input information for the DurationParameter
	GenericEso::Ptr esoPtr; ///< holds the Eso that is passed to the IntegratorMetaData
    std::vector<double> userGrid;///< additional gridpoints set by user (via state point constraints)
    unsigned plotGridResolution;
    std::vector<double> explicitPlotGrid;
    IntegratorMetaDataInput()
    {
      plotGridResolution = 1;
    }
  };
  
  
  /**
  * @struct ConstraintInput
  * @brief information provided by user to create a StatePointConstraint object
  */
  struct ConstraintInput
  {
    enum ConstraintType{
      PATH,
      POINT,
      ENDPOINT,
      MULTIPLE_SHOOTING
    };
    unsigned esoIndex; ///< user given eso index of the constraint
    double lowerBound; ///< user given lower bound of the constraint
    double upperBound; ///< user given upper bound of the constraint
    double timePoint; ///< time point of the constraint (for point constraints)
    double lagrangeMultiplier; ///< initial Lagrange multiplier of the constraint
    std::vector<double> lagrangeMultiplierVector; ///< initial Lagrange multiplier vector (for PATH)
    ConstraintType type;
    bool excludeZero;
    ConstraintInput()
    {
      type = PATH;
      esoIndex = 0;
      lowerBound = 0.0;
      upperBound = 0.0;
      timePoint = 0.0;
      lagrangeMultiplier = 0.0;
      excludeZero = false;
    }
  };

  /**
  * @struct OptimizerMetaDataInput
  * @brief information provided by user to create an OptimizerMetaDataInput
  */
  struct OptimizerMetaDataInput
  {
    /// input information of all single stages in multi stage mode
    std::vector<OptimizerMetaDataInput> stages;
    /// input information for all StatePointConstraint objects
    std::vector<ConstraintInput> constraints;
    /// input information for the objective
    ConstraintInput objective;
    /// vector stating whether a single stage objective is treated in multi stage mode
    std::vector<bool> treatObjective;

    StructureDetectionInput structureDetection;

    bool hasTotalEndTime;
    double totalEndTimeLowerBound;
    double totalEndTimeUpperBound;
    double constantDurationsValue;
    OptimizerMetaDataInput()
    {
      hasTotalEndTime = true;
      totalEndTimeLowerBound = 0.0;
      totalEndTimeUpperBound = 0.0;
      constantDurationsValue = 0.0;
    }
  };
}// FactoryInput
