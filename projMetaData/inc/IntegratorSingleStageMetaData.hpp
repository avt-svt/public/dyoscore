/**
* @file IntegratorSingleStageMetaData.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Single Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* This file contains the IntegratorSingleStageMetaData class           \n
* and the class IntegratorSSMetaData2ndOrder                           \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 10.11.2011
*/
#pragma once

//#define MAKE_DYOS_DLL
//#include "InputOutputConversions.hpp"
#include "IntegratorMetaData.hpp"
#include "Observer.hpp"
#include "Control.hpp"
#include "cs.h"
#include <map>

// due to a bug in VS this warning will appear anytime in any diamond inheritance
// even though it is by using virtual inheritance and abstract interfaces somehow safe.
#ifdef WIN32
#pragma warning( disable : 4250 )
#endif

/**
* @brief struct containing scaled time values used by IntegratorSingleStageMetaData
*/
struct ScaledTimeStruct
{
  //! time at the beginning of an integration interval
  double startTime;
  //! time at the end of an integration interval
  double endTime;
  //! current time value of an integration interval
  double currentTime;
};

/**
* @brief struct containing unscaled time values used by IntegratorSingleStageMetaData
*/
struct UnscaledTimeStruct
{
  //! difference between the current and the previous absolute final time within an integration
  double relativeDuration;
  //! previous absolute final time
  double absoluteStartTime;
};

//typedef boost::shared_ptr<Parameter> Parameter::Ptr;

/**
* @class IntegratorSingleStageMetaData
*
* @brief class defining a complete single stage problem
*
* This class can provide all information needed to solve a single stage problem.
* The complete IntegratorMetaData interface is implemented. In addition some
* functions called by OptimizerSingleStageMetaData and IntegratorMultiStageMetaData
* are provided.
*/
class IntegratorSingleStageMetaData : public virtual IntegratorMetaData
{
public:
  typedef std::shared_ptr<IntegratorSingleStageMetaData> Ptr;
protected:
  //! struct containing scaled times updated each integration step
  ScaledTimeStruct m_integrationStepTimes;
  //! struct containing unscaled times updated each integration block
  UnscaledTimeStruct m_integrationBlockTimes;
  //! Modelserver object of the stage
  GenericEso::Ptr m_genericEso;
  //! mass matrix is constant and is thus created only once and saved here
  CsCscMatrix::Ptr m_massMatrix;
  CsTripletMatrix::Ptr m_massMatrixCoo;
  //! variable representing the controls defining the optimization problem
  std::vector<Control> m_controls;
  //! variable representing the initial value parameters free for optimization
  std::vector<InitialValueParameter::Ptr> m_initialValues;
  //! cs_sparse struct (csc format) representing the Model derived w.r.t. the controls, full size
  CsCscMatrix::Ptr m_jacobianFup;
  //! cs_sparse struct (csc format) m_jacobianFup, but only reduced numbers of colums (number of controls)
  CsCscMatrix::Ptr m_jacobianFupReduced;
  //! cs_sparse struct (csc format) representing the Model derived w.r.t. all states
  CsCscMatrix::Ptr m_jacobianFstates;
  //! store the current time derivatives of the controls (this is constant data between the control steps)
  utils::Array<double> m_ut;

  utils::Array<double> m_rhsStatesDtime;

  bool m_evaluateFStates;
  

  /**
  * control index vector mapping JacobianFup values vector
  * from coordinate order to csc order
  */
  std::vector<int> m_jacobianFupMapping;
  /**
  * state index vector mapping JacobianFstates values vector
  * from coordinate order to csc order
  */
  std::vector<int> m_jacobianFstatesMapping;
  /**
  * sensitivities of the previous integration step. For newly activated
  * parameter getInitialValuesForIntegration is evaluated
  */
  std::vector<double> m_currentSensitivities;
  /**
  * values of the current adjoint variables. They are used during a reverse integration.
  */
  std::vector<double> m_adjoints;
  /**
  * values of the current derivatives of the Lagrange function. They are used during a reverse
  * integration.
  */
  std::vector<double> m_lagrangeDers;

  //! set of all parameters with sensitivities
  std::vector<Parameter::Ptr> m_parametersWithSensitivity;
  //! set of current parameters with sensitivity (active and formerly active parameters)
  std::vector<Parameter::Ptr> m_currentParamsWithSens;
  //! set of all decision variables - subset of m_sensParametersWithSensitivity
  std::vector<Parameter::Ptr> m_decisionVariables;
  /*! DurationParameter shared by all controls representing the global duration
      of the single stage */
  DurationParameter::Ptr m_globalDurationParameter;
  //! vector of all final time parameters
  std::vector<DurationParameter::Ptr> m_gridDurations;

  //! set of gridpoints being set externally - e.g. by setting constraints
  std::vector<double> m_additionalGrid;
  //! set of gridpoints resulting from the union of the user set grid and all parameter grids
  std::vector<double> m_completeGridUnscaled;
  //! index of the grid endpoint of the current integration interval
  unsigned m_currentIndexCompleteGridUnscaled;

  //! vector of absolute final times of the integration grids
  std::vector<double> m_finalTimes;
  /*! vector of integration grids - one integration grid contains scaled interval
      limits for the integration steps */
  std::vector<std::vector<double> > m_integrationGrids;
  //! index of the current integration grid
  unsigned m_currentIndexIntegrationGrid;
  //! index of the current integration step on the current integration grid
  unsigned m_currentIndexIntegrationGridpoint;

  /** indices of the already initialized parameters in the m_parametersWithSensitivity vector
  * This vector will be updated each time a new parameter is added to the vector.*/
  std::vector<unsigned> m_oldParameterPositions;

  Observer::Ptr m_observer;
  
  bool m_plotGridActivated;
  std::vector<double> m_plotGrid;

  // protected methods
  // initialization methods
  CsTripletMatrix::Ptr createJacobianInTriplet();
  void createJacobianFstates(CsTripletMatrix::Ptr Fu);
  void createJacobianFup(CsTripletMatrix::Ptr Fu);
  void createMassMatrix();
  void assignParameterIndices();
  void createGridDurationVector();

  // help functions for initializeForFirstIntegration
  void createFinalTimeVector();
  virtual void createGlobalGrid();
  void createIntegrationGrids();

  void updateControlParameter();
  void updateControlParameterBackwardsIntegration();
  void updateDurationParameter();
  void updateDurationParameterBackwardsIntegration();
  void setNonlinearConstraints(void);
  void setNonlinearConstraintsGradients(void);
  void addNewParameterToParameterWithSensitivityVector(const Parameter::Ptr &p);
  void removeParameterFromParameterWithSensitivityVector(const Parameter::Ptr &p);


  virtual void defineDecisionVariables(std::vector<Parameter::Ptr> &decisionVariables);
  virtual void defineSensParameters(std::vector<Parameter::Ptr> &sensParams);
  virtual void getConstraintSensitivities(const std::vector<unsigned> &equationIndices,
                                                utils::Array<double>  &sensitivities) const;

  virtual std::vector<DyosOutput::ParameterOutput> getDurationOutput();
  virtual DyosOutput::IntegratorStageOutput  getStageOutput();


  IntegratorSingleStageMetaData();


public:
  IntegratorSingleStageMetaData(const GenericEso::Ptr &genericEso,
                                const std::vector<Control> &controls,
                                const std::vector<InitialValueParameter::Ptr> &initials,
                                const DurationParameter::Ptr &globalDuration,
                                const std::vector<double> &userGrid);
  ~IntegratorSingleStageMetaData();

  virtual void setStepStartTime(const double startTime);
  virtual void setStepEndTime(const double endTime);

  virtual unsigned getNumStages() const;
  virtual unsigned getNumSensitivityParameters() const;

  // Integrator Interface methods
  virtual void calculateSensitivitiesForward1stOrder(const utils::Array<double> &inputSensitivities,
                                                           utils::Array<double> &rhsSensitivities);

  virtual void calculateRhsStates(const utils::Array<double> &inputStates,
                                        utils::Array<double> &rhsStates);
  virtual void calculateAdjointsReverse1stOrder(const utils::Array<double> &states,
                                                const utils::Array<double> &adjoints,
                                                      utils::Array<double> &rhsAdjoints,
                                                      utils::Array<double> &rhsDers);
  virtual void calculateRhsAdjoints(const utils::Array<double> &adjoints,
                                          utils::Array<double> &rhsAdjoints);
  virtual void calculateRhsStatesDtime(utils::Array<double> &rhsDTime);
  virtual void setStates(const utils::Array<double> &states);
  virtual void storeInitialStates(const utils::Array<double> &states);
  virtual void setSensitivities(const utils::Array<double> &sens);
  virtual void setAdjointsAndLagrangeDerivatives(const utils::Array<double> &adjoints,
                                                 const utils::Array<double> &lagrangeDerivatives);
  void setAdjointsAndLagrangeDerivativesNoObserver(const utils::Array<double> &adjoints,
                                                   const utils::Array<double> &lagrangeDerivatives);
  
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers);
  virtual void setInitialLagrangeDerivatives();
  virtual void updateAdjoints();
  virtual void initializeForNewIntegrationStep();
  virtual void initializeForNewBackwardsIntegrationStep();
  virtual void initializeForNextIntegration();
  virtual void initializeForPreviousIntegrationBlock();
  virtual void initializeForFirstIntegration();
  virtual void initializeMatrices();
  virtual void initializeAtStageDuration();

  virtual double getStepStartTime()const;
  virtual double getStepEndTime()const;

  virtual CsCscMatrix::Ptr getMMatrix(void) const;
  virtual CsTripletMatrix::Ptr getMMatrixCoo(void) const;
  virtual void evaluateInitialValues(utils::Array<double> &initialStates,
                                     utils::Array<double> &initialSensitivities);

  virtual void setStepCurrentTime(const double time);

  virtual int getNumCurrentSensitivityParameters() const;

  virtual int getNumIntegrationIntervals() const;

  virtual void setAllInitialValuesFree();

  virtual GenericEso::Ptr getGenericEso();

  virtual int getNumStateNonZeroes();

  virtual double getStageDuration() const;

  virtual void setControlsToEso();
  
  virtual void reset();

  virtual void resetGolbalDurationParameter(void);

  // Parameter interface methods
  // no differentiation between time independent and control parameters
  virtual CsCscMatrix::Ptr getJacobianFup();
  virtual CsCscMatrix::Ptr getJacobianFupReduced();
  virtual CsCscMatrix::Ptr getJacobianFstates();
  virtual double getStepCurrentTime()const;
  virtual double getIntegrationGridDuration()const;

  // Optimizer interface methods
  virtual void setGlobalDuration(const double &duration);
  virtual void sortControlParameters();
  virtual void getDecisionVariables(std::vector<Parameter::Ptr> &decisionVariables) const;
  virtual void getSensitivityParameters(std::vector<Parameter::Ptr> &sensParams) const;
  virtual unsigned addNonlinearConstraint(const unsigned esoIndex,
                                          const unsigned constIndex,
                                          const double timePoint,
                                          const double lagrangeMultiplier);
  virtual void getCurrentSensitivities(std::vector<double> &curSens) const;
  virtual void getAdjoints(std::vector<double> &adjoints) const;
  virtual void getLagrangeDers(std::vector<double> &LagrangeDers) const;
  virtual unsigned getNumDecisionVariables() const;
  virtual void getDurationParameters
    (std::vector<DurationParameter::Ptr> &durationParameters)const;
  virtual DurationParameter::Ptr getGlobalDurationParameter()const;
  virtual void getInitialValueParameter(std::vector<InitialValueParameter::Ptr> &initials) const;
  
  std::vector<double> getStageGridScaled()const;

  //Mapping interface methods
  virtual void setMappingValues(const utils::Array<double> &values,
                                const std::vector<int> &esoIndices);

  virtual double getStageCurrentTime() const;
  virtual double getStageCurrentTimeScaled() const;

  void setObserver(const Observer::Ptr &observer) { m_observer = observer;}

  virtual std::vector<DyosOutput::StageOutput> getOutput();
  virtual std::vector<DyosOutput::StateOutput> getStateOutputVector();
  virtual std::vector<DyosOutput::StateGridOutput> getAdjointOutputVector();
  virtual std::vector<double> getStateTrajectory(const unsigned stateIndex);
  virtual std::vector<double> getAdjointTrajectory(const unsigned adjointIndex);

  virtual unsigned getTimeIndex()const{return m_currentIndexCompleteGridUnscaled;};

  virtual void clearActiveParameters();
  virtual void reactivateParameters();

  // switching system interface functions
  virtual void foundRoot();
  
  virtual void activatePlotGrid();
  virtual void setPlotGrid(const unsigned plotGridResolution,
                           const std::vector<double> &explicitPlotGrid);

  virtual void setIntOptData();
};


/**
* @class IntegratorSSMetaData2ndOrder
* @brief implementation of IntegratorMetaData2ndOrder for a single stage
*/
class IntegratorSSMetaData2ndOrder : public virtual IntegratorSingleStageMetaData,
                                     public virtual IntegratorMetaData2ndOrder
{
protected:
  IntegratorSSMetaData2ndOrder(){}
public:
  typedef std::shared_ptr<IntegratorSSMetaData2ndOrder> Ptr;
  IntegratorSSMetaData2ndOrder(const GenericEso2ndOrder::Ptr &genericEso2ndOrder,
                               const std::vector<Control> &controls,
                               const std::vector<InitialValueParameter::Ptr> &initials,
                               const DurationParameter::Ptr &globalDuration,
                               const std::vector<double> &additionalGrid);
  ~IntegratorSSMetaData2ndOrder();
  virtual void store2ndOrderAdjoints();
  
  // for multi stage meta data
  virtual void resizeLagrangeDerivatives(const unsigned numGlobalDecisionVariables);
  virtual void getSensitivityParameters(std::vector<Parameter::Ptr> &parameters) const;

  //functions inherited from IntegratorSingleStageMetaData
  void setInitialLagrangeDerivatives();
  DyosOutput::IntegratorStageOutput getStageOutput();

  //functions inherited from IntegratorMetaData2ndOrder

  virtual void initializeAtStageDuration();

  virtual void calculateRhsLagrangeDers2ndOrder(utils::Array<double> &adjoints1stOrder,
                                                utils::Array<double> &adjoints2ndOrder,
                                                Eso2ndOrderEvaluation &input,
                                                utils::Array<double> &rhsDers2ndOrder);

  virtual void calculateAdjointsReverse2ndOrder(utils::Array<double> &states,
                                                utils::Array<double> &sens,
                                                utils::Array<double> &adjoints1stAnd2nd,
                                                utils::Array<double> &rhsAdjoints2nd,
                                                utils::Array<double> &rhsDers2nd);

  void calculateAdjointsReverse2ndOrder(utils::Array<double> &sens,
                                        utils::Array<double> &adjoints1st,
                                        utils::Array<double> &adjoints2nd,
                                        utils::Array<double> &rhsAdjoints2nd,
                                        utils::Array<double> &rhsDers2nd,
                                        IntegratorMetaData2ndOrder *imd2nd);

  static void calculateRhsLagrangeDers2ndOrder(utils::Array<double> &adjoints1stOrder,
                                               utils::Array<double> &adjoints2ndOrder,
                                               Eso2ndOrderEvaluation &input,
                                               utils::Array<double> &rhsDers2ndOrder,
                                               std::vector<Parameter::Ptr> &sensParameters,
                                               IntegratorMetaData2ndOrder *imd2nd);

  virtual GenericEso2ndOrder::Ptr getGenericEso2ndOrder() const;
protected:
  //! observer for 2nd order adjoints
  Observer::Ptr m_secondOrderAdjointsObserver;
  //! Modelserver object of the stage
  GenericEso2ndOrder::Ptr m_genericEso2ndOrder;
};

////////////////////////////////
// help structs and functions //
////////////////////////////////

/**
* @brief contains all arguments for function evaluate2ndOrderDerivatives
*
* this structure is only for code reduction
*/
struct Eso2ndOrderEvaluationArgs
{
  Eso2ndOrderEvaluation eso2ndOrder;
  utils::Array<double> t2DerStates;
  utils::Array<double> a1DerStates;
  utils::Array<double> t2A1DerStates;
  utils::Array<double> a1States;
  utils::Array<double> a1P;
  utils::Array<double> adjoints1stOrder;
  utils::Array<double> adjoints2ndOrder;
  utils::Array<double> zerosNumStates;
  unsigned m_numStates;
  unsigned m_numEsoParams;
  unsigned m_sensParIndex;
  unsigned m_numEqns;
  double *rhsAdjs2ndOrder;

  Eso2ndOrderEvaluationArgs(unsigned numStates, unsigned numEsoParams, unsigned numEqns)
  : t2DerStates(numStates, 0.0),
    a1DerStates(numStates, 0.0),
    t2A1DerStates(numStates, 0.0),
    a1States(numStates, 0.0),
    a1P(numEsoParams, 0.0),
    adjoints1stOrder(numStates, 0.0),
    adjoints2ndOrder(numStates, 0.0),
    zerosNumStates(numStates, 0.0)
  {
    eso2ndOrder.dEsoParamsDp_i.assign(numEsoParams, 0.0);
    eso2ndOrder.t2A1EsoParameters.assign(numEsoParams, 0.0);
    m_numStates = numStates;
    m_numEsoParams = numEsoParams;
    m_numEqns = numEqns;
  }
  
  ~Eso2ndOrderEvaluationArgs()
  {
  }
};

void initializeEso2ndOrderEvaluationArgs(Eso2ndOrderEvaluationArgs &in,
                                          utils::Array<double> &rhsAdjoints2nd,
                                          unsigned sensParIndex);

void setAdjoints(Eso2ndOrderEvaluationArgs &in,
           const utils::Array<double> &adjoints1stOrder,
           const utils::Array<double> &adjoints2ndOrder);


void callEvaluate2ndOrderDerivatives(Eso2ndOrderEvaluationArgs &args,
                                     GenericEso2ndOrder::Ptr eso);


void initializeSensWrtP_i(std::vector<Parameter::Ptr> currentActiveParameters,
                          Parameter::Ptr parameter,
                          utils::Array<double> &sens,
                          Eso2ndOrderEvaluationArgs &esoEvalArgs);

int getActiveParameterIndex(Parameter::Ptr parameter,
                            GenericEso::Ptr &genericEso);

void adjustOutput(Eso2ndOrderEvaluationArgs &esoEvalArgs,
                  double duration,
                  int paramEsoIndex);

void updateInitialLagrangeDerivatives(std::vector<InitialValueParameter::Ptr> &initialValueParameter,
                                      std::vector<Parameter::Ptr> &currentSensParameters,
                                      utils::Array<double> &adjoints,
                                      utils::Array<double> &lagrangeDerivatives);
