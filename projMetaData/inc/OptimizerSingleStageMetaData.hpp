/**
* @file OptimizerSingleStageMetaData.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the OptimizerSingleStageMetaData class            \n
* according to MetaEso8.vsd                                            \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 13.09.2011
*/

#pragma once

#include "OptimizerMetaData.hpp"
#include "IntegratorSingleStageMetaData.hpp"
#include "Constraints.hpp"
#include "UtilityFunctions.hpp"


//#include <boost/shared_ptr.hpp>
#include <memory>

/**
* @class OptimizerSingleStageMetaData
* @brief class implementing a single stage case of the OptimizerMetaData interface
*
* @author Kathrin Frankl, Fady Assassa, Tjalf Hoffmann
* @date 15.12.2011
*/
class OptimizerSingleStageMetaData : virtual public OptimizerMetaData
{
public:
  typedef std::shared_ptr<OptimizerSingleStageMetaData> Ptr;

  DyosOutput::OptimizerStageOutput  getStageOutput();
protected:
  //! point constraints for the states
  std::vector<StatePointConstraint> m_constraints;
  //! variable representing the value of the objective function
  StatePointConstraint m_objective;

  std::vector<double> m_linConLagrangeMultiplier;

  //! pointer to the corresponding IntegratorMetaData object
  IntegratorSingleStageMetaData::Ptr m_integratorMetaData;

  //help functions
  virtual void calculateNonLinJacDecVarStructure(IntOptDataMap &intOptData);
  virtual void calculateNonLinJacConstParStructure();
  virtual void calculateLinJacStructure();

public:
  OptimizerSingleStageMetaData();
  OptimizerSingleStageMetaData(const IntegratorSingleStageMetaData::Ptr &integratorMetaData);
  /** @brief standard empty destructor */
  virtual ~OptimizerSingleStageMetaData(){};

  // methods for factory use only.
  virtual void setConstraints(const std::vector<StatePointConstraint> &constraints);
  virtual void setObjective(const StatePointConstraint &objective);
  virtual void calculateOptimizerProblemDimensions();

  //methods of the OptimizerMetaData interface
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const;
  virtual void getDecVarValues(utils::Array<double> &values) const;
  virtual void setDecVarValues(const utils::Array<const double> &values);
  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const;
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const;
  virtual void getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const;
  virtual void getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr &nonLinConstGradients) const;
  virtual double getNonLinObjValue() const;
  virtual void getNonLinObjDerivative(utils::Array<double> &values) const;
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers);
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const;
  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier);
  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier);
  virtual void getLinConstraintValues(utils::Array<double> &values) const;
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds) const;
  virtual void addConstraint(StatePointConstraint &constraint, unsigned constraintIndex);

  std::vector<IntOptDataMap> getIntegrationOptData() const;

  virtual StatePointConstraint getObjectiveData(void);
  
  
  virtual void getDecisionVariableIndices(std::vector<unsigned> &decVarInd) const;
  virtual void getConstantParameterIndices(std::vector<unsigned> &constParamInd) const;
  
  
  //static help functions (also used by other modules (eq OptimizerMultiStageMetaData)
  static void retrieveNonLinConstGradients(CsTripletMatrix::Ptr nonLinConstGradients,
                                           std::vector<unsigned> &varIndices,
                                           IntOptDataMap &intOptData,
                                           const unsigned objectiveIndex);
  static void constructFullCooMatrix(CsTripletMatrix::Ptr &matrix, const unsigned numRows, const unsigned numCols);
  static void constructSparseCooMatrix(CsTripletMatrix::Ptr &matrix,
                                       const std::vector<int> rowIndices,
                                       const std::vector<int> colIndices,
                                       const unsigned numRows,
                                       const unsigned numCols);
  
  std::vector <DyosOutput::StageOutput> getOutput();
};

class OptimizerSSMetaData2ndOrder : public virtual OptimizerMetaData2ndOrder,
                                    public virtual OptimizerSingleStageMetaData
{
public:
  typedef std::shared_ptr<OptimizerSSMetaData2ndOrder> Ptr;
  OptimizerSSMetaData2ndOrder(const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData);
  virtual ~OptimizerSSMetaData2ndOrder();

protected:
  OptimizerSSMetaData2ndOrder(){}
  
  IntegratorSSMetaData2ndOrder::Ptr m_integratorMetaData2ndOrder;
  
  virtual void getNonLinConstraintHessian(CsTripletMatrix::Ptr &hessianMatrix,
                                          const std::vector<unsigned> &indices) const;
  virtual void getAdjoints(std::vector<double> &adjoints1stOrder,
                           CsTripletMatrix::Ptr &adjoints2ndOrder,
                           std::vector<unsigned> &indices) const;

public:
  virtual void getNonLinConstraintHessianDecVar(CsTripletMatrix::Ptr &hessianMatrix) const;
  virtual void getNonLinConstraintHessianConstParam(CsTripletMatrix::Ptr &hessianMatrix) const;
  virtual void getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
                                 CsTripletMatrix::Ptr &adjoints2ndOrder) const;
  virtual void getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                     CsTripletMatrix::Ptr &adjoints2ndOrder) const;

  static void extractValuesToMatrix(CsTripletMatrix::Ptr &matrix,
                                    const std::vector<unsigned> &indices,
                                    const unsigned numRows,
                                    const unsigned numColsTotal,
                                    const std::vector<double> &values);
                                    
  static void extractValuesToMatrix(CsTripletMatrix::Ptr &matrix,
                                  const std::vector<unsigned> &colIndices,
                                  const std::vector<unsigned> &rowIndices,
                                  const unsigned numRows,
                                  const unsigned numColsTotal,
                                  const std::vector<double> &values);
  //static void getAdjoints
};
