/**
* @file OptimizerMetaData.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the OptimizerMetaData class                       \n
* according to MetaEso8.vsd                                            \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 10.12.2011
*/

#pragma once

#include <vector>
#include <string>
//#include <boost/shared_ptr.hpp>
#include <memory>

#include "cs.h"
#include "Array.hpp"
#include "UserOutput.hpp"
#include "UserInput.hpp"
#include "GenericOptimizer.hpp"
#include "MetaDataOutput.hpp"
#include "DyosObject.hpp"
#include "TripletMatrix.hpp"

/**
* @struct OptimizerProblemDimensions
*
* @brief structural information of the optimization problem
*/
struct OptimizerProblemDimensions
{
  //! numOptVars number of optimization variables
  unsigned numOptVars;
  //! numConstParams number of parameters with sensitivity that are not optimization variables
  unsigned numConstParams;
  //! numLinConstraints number of linear constraints (=0 for single stage)
  unsigned numLinConstraints;
  //! numNonLinConstraints number of nonlinear constraints
  unsigned numNonLinConstraints;
  //! nonLinObjIndex Eso index of the objective variable (equation index needed)
  unsigned nonLinObjIndex;
  //! linJac structure of linear Jacobian in coo-format (empty for single stage)
  CsTripletMatrix::Ptr linJac;
  //! nonLinJacDecVar structure of nonlinear decision variable Jacobian in coo-format
  CsTripletMatrix::Ptr nonLinJacDecVar;
  //! nonLinJacConstPar structure of nonlinear constant parameter Jacobian in coo-format
  CsTripletMatrix::Ptr nonLinJacConstPar;
};

/**
* @class OptimizerMetaData
*
* @brief data interface for the optimizer
*/
class OptimizerMetaData : public DyosObject
{
protected:
  //! instance of OptimizerProblemDimension
  OptimizerProblemDimensions m_optProbDim;

public:
  typedef std::shared_ptr<OptimizerMetaData> Ptr;
  /**
  * @brief destructor
  */
  virtual ~OptimizerMetaData(){}
  /**
  * @brief getter for the instance of OptimizerProblemDimension describing
  *        the structure of the current optimization problem defined in this class
  *
  * @return reference on the current OptimizerProblemDimension structure
  */
  virtual const OptimizerProblemDimensions &getOptProbDim() const {return m_optProbDim;};

  /**
  * @brief get the bounds of all decision variables
  *
  * @param upperBounds array receiving the upper bounds of the decision variables -
  *                    must be of size numOptVars
  * @param lowerBounds array receiving the lower bounds of the decision variables -
  *                    must be of size numOptVars
  * @sa OptimizerProblemDimensions
  */
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const = 0;

  /**
  * @brief get the values of all decision variables
  *
  * @param values array receiving the values of the decision variables -
  *               must be of size numOptVars
  * @sa OptimizerProblemDimensions
  */
  virtual void getDecVarValues(utils::Array<double> &values) const = 0;

  /**
  * @brief set the values of all decision variables from the optimizer
  *
  * @param decVars array containing the values to be set -
  *               must be of size numOptVars
  * @sa OptimizerProblemDimensions
  */
  virtual void setDecVarValues(const utils::Array<const double> &decVars) = 0;

  /**
  * @brief get the bounds of all constraints
  *
  * The sorting of the bounds is according to the sorting of the interior point constraints
  * @param lowerBounds lower bounds of the constraints - will be resized to size numNonLinConstraints
  * @param upperBounds upper bounds of the constraints - will be resized to size numNonLinConstraints.
  * @sa OptimizerProblemDimensions
  */
  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const = 0;
  /**
  * @brief get the values of the nonlinear constraints
  *
  * @param values array receiving the values of the nonlinear constraints -
                  must be of size numNonLinConstraints
  * @sa OptimizerProblemDimensions
  */
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const = 0;

  /**
  * @brief get the decision variables sensitivities of the nonlinear constraints
  *
  * @param derivativeMatrix matrix receiving the derivatives of the nonlinear constraints -
  *                         matrix must be preallocated and of same structure as nonLinJacDecVar
  * @sa OptimizerProblemDimensions
  */
  virtual void getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const = 0;
  /**
  * @brief get the constant parameter sensitivities of the nonlinear constraints
  *
  * @param nonLinConstGradients matrix receiving the derivatives of the nonlinear
  *                             constraints - matrix must be preallocated and of
                                same structure as nonLinJacConsPar
  * @sa OptimizerProblemDimensions
  */
  virtual void getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr &nonLinConstGradients) const = 0;
  /**
  * @brief get the value of the objective function
  *
  * @return the value of the objective function
  */
  virtual double getNonLinObjValue() const = 0;

  /**
  * @brief get the sensitivities of the nonlinear objective
  *
  * @param values array receiving the sensitivities -
  *               must be of size numOptVar
  * @sa OptimizerProblemDimensions
  */
  virtual void getNonLinObjDerivative(utils::Array<double> &values) const = 0;

  /**
  * @brief set the lagrange multipliers of all constraints
  *
  * @param values array containing the values to be set -
  *               must be of size numNonLinConstraints + numLinConstraints
  * @sa OptimizerProblemDimensions
  */
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers) = 0;

  /**
  * @brief get the lagrange multipliers of all costraints
  *
  * @param values array receiving the values -
                  must be of size numNonLinConstraints + numLinConstraints
                  nonlinear lagrange multiplier are first
  */
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const = 0;
  
  
  /**
  * @brief set the lagrange multipliers of all decision variables
  */
  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier) = 0;
  
  /**
  * @brief set the lagrange multiplier of the objective
  */
  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier) = 0;
  
  /**
  * @brief get the values of the linear constraints
  *
  * @param values array receiving the values of the linear constraints -
                  must be of size numLinConstraints
  * @sa OptimizerMetaData::getLinConstraintValues
  */
  virtual void getLinConstraintValues(utils::Array<double> &values) const = 0;
 
  /**
  * @brief get the bounds of all linear constraints
  *
  * The sorting of the bounds is according to the sorting of the linear constraints
  * @param lowerBounds lower bounds of the constraints - will be resized to size numLinConstraints
  * @param upperBounds upper bounds of the constraints - will be resized to size numLinConstraints.
  * @sa OptimizerMetaData::getLinConstraintBounds
  */
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds) const = 0;
  

  virtual std::vector <DyosOutput::StageOutput> getOutput() = 0;
};

class OptimizerMetaData2ndOrder : public virtual OptimizerMetaData
{
public:
  typedef std::shared_ptr<OptimizerMetaData2ndOrder> Ptr;
    /**
  * @brief get the decision variables sensitivities of the nonlinear constraints
  *
  * @param derivativeMatrix matrix receiving the derivatives of the nonlinear constraints -
  *                         matrix must be preallocated and of same structure as nonLinJacDecVar
  * @sa OptimizerProblemDimensions
  */
  virtual void getNonLinConstraintHessianDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const = 0;
  /**
  * @brief get the constant parameter sensitivities of the nonlinear constraints
  *
  * @param derivativeMatrix matrix receiving the derivatives of the nonlinear constraints -
  *                         matrix must be preallocated and of same structure as nonLinJacConsPar
  * @sa OptimizerProblemDimensions
  */
  virtual void getNonLinConstraintHessianConstParam(CsTripletMatrix::Ptr &nonLinConstGradients) const = 0;
  /**
  * @brief get first and second order adjoints of decisionVariables
  *
  * @param[out] array receiving first order adjoints - must be of size numStates
  * @param[out] array receiving second order adjoints
                - must be of size numStates*numSensitivityParameters
  */
  virtual void getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
                                 CsTripletMatrix::Ptr &adjoints2ndOrder) const = 0;
  /**
  * @brief get first and second order adjoints of constant parameters
  *
  * @param[out] array receiving first order adjoints - must be of size numStates
  * @param[out] array receiving second order adjoints
                - must be of size numStates*numSensitivityParameters
  */
  virtual void getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                     CsTripletMatrix::Ptr &adjoints2ndOrder) const = 0;
};
