/** 
* @file Mapping.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mapping - Part of DyOS Meta Data                                     \n
* =====================================================================\n
* This file contains the definitions of the memberfunctions of class   \n
* Mapping according to the UML Diagramm METAESO_8                      \n
* =====================================================================\n
* @author Fady Assassa
* @date 22.03.2012
*/
#pragma once

#include <vector>
#include <map>
//#include <boost/shared_ptr.hpp>
#include <memory>
#include "IntegratorSingleStageMetaData.hpp"
#include "DyosObject.hpp"

//! (shared) pointer to IntegratorSingleStageMetaData class
typedef IntegratorSingleStageMetaData::Ptr IntegratorSSMetaDataPtr;

//! (shared) pointer to IntegratorSSMetaData2ndOrder class
typedef IntegratorSSMetaData2ndOrder::Ptr IntegratorSSMD2ndOrderPtr;

//! index index map
typedef std::map <unsigned, unsigned> iiMap;
/**
* @class Mapping
*
* @brief Defining mapping of multi stage problem. Single , Multiple , Hybrid Shooting, Model Change
*/
class Mapping : public DyosObject
{
public:
  typedef std::shared_ptr<Mapping> Ptr;
  Mapping();
  ~Mapping();
  /** 
  * @brief maps the states between two stages
  * @param[in] lastStage is the last stage integrated
  * @param[out] currentStage is the stage that is to be integrated
  * @param[out] multiStageSens Sensitivities that are carried over from one stage to the next
  */
  virtual void applyStageMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                                       IntegratorSingleStageMetaData::Ptr currentStage,
                                       std::vector<double> &multiStageSens) = 0;
  /**
  * @brief set the attributes of the mapping class that define the mapping
  * \param[in] varIndexMap containts the mapping of the eso variables
  * \param[in] eqnIndexMap containts the mapping of the equation indices
  */
  virtual void setIndexMaps(const iiMap varIndexMap, 
                            const iiMap eqnIndexMap) = 0;

  /**
  * @brief apply mapping of this object to a given map
  *
  * To map from one stage the the next, there might map with a set of indices calculated by
  * other mappings that is not identical to the set of key values of this map.
  * So some indices of the given map that are not in the set of key values are to
  * be left out and all others are to be mapped to the indices of the next stage.
  * @param[in] equationsMap given map which the mapping is applied to
  * @return resulting map
  * @sa Mapping::getEquationsVectorMap(std::vector<Mapping::Ptr> mappings,
                                 const unsigned numEquationsCurrentStage,
                                 const unsigned currentStage,
                                 const unsigned targetStage
  */
  virtual iiMap getEquationsVectorMap(const iiMap &equationsMap) = 0;

  /**
  * @brief apply backwards mapping of this object to a given map mapping
  *
  * The objects mapping information is about mapping one stage to the next.
  * In some cases there also is a reverse mapping needed, so the mapping 
  * direction has to be reversed. 
  * @param[in] equationMapgiven map which the reverse mapping is applied to
  * @return resulting map
  * @sa iiMap Mapping::getEquationsVectorMap(const iiMap &equationsMap)
  *     iiMap Mapping::getEquationsVectorMap(std::vector<Mapping::Ptr> mappings,
  *                                    const unsigned numEquationsCurrentStage,
  *                                    const unsigned currentStage,
  *                                    const unsigned targetStage)
  */
  virtual iiMap getReverseEquationsVectorMap
                          (const iiMap &equationsMap) = 0;

  virtual bool isDecoupled();

  static iiMap getEquationsVectorMap(const std::vector<Mapping::Ptr> mappings,
                               const unsigned numEquationsCurrentStage,
                               const unsigned currentStage,
                               const unsigned targetStage);
};

/**
* @class SingleShootingMapping
*
* @brief Defining mapping of multi stage problem. in case of single shooting
* the class assumes that we have the same model for each stage.
*/
class SingleShootingMapping:public Mapping
{
protected:
  //! variable that keeps track for the variable mappings
  iiMap m_varIndexMap;
  /**
  * @brief variable that keeps track for the equation mappings
  *
  * Maps equation index of the old stage to the equation index of the new stage.
  */
  iiMap m_equationIndexMap;
  
  void fullStateMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                              IntegratorSingleStageMetaData::Ptr currentStage);
  void mapSensitivities(const IntegratorSingleStageMetaData::Ptr lastStage,
                                    IntegratorSingleStageMetaData::Ptr currentStage,
                                    std::vector<double> &multiStageSens);
public:
  /**
  * @brief empty standard constructor
  **/
  SingleShootingMapping(){};
  /**
  * @brief empty standard destructor
  **/
  ~SingleShootingMapping(){};
  void applyStageMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                               IntegratorSingleStageMetaData::Ptr currentStage,
                               std::vector<double> &multiStageSens);
  void virtual setIndexMaps(const iiMap varIndexMap, 
                            const iiMap eqnIndexMap);
  virtual iiMap getEquationsVectorMap(const iiMap &equationsMap);
  virtual iiMap getReverseEquationsVectorMap
                          (const iiMap &equationsMap);
  virtual bool isDecoupled();
};

/**
* @class MultipleShootingMapping
*
* @brief Defining mapping of multi stage problem. in case of mulitple shooting
* the stages are discontinuous, thus there is no mapping. Therefore it doesn't matter, if we have different
* models.
*/
class MultipleShootingMapping : public Mapping
{
public:
  /**
  * @brief empty standard constructor
  **/
  MultipleShootingMapping(){};
  /**
  * @brief empty standard destructor
  **/
  ~MultipleShootingMapping(){};
  
  void applyStageMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                               IntegratorSingleStageMetaData::Ptr currentStage,
                               std::vector<double> &multiStageSens);
  void virtual setIndexMaps(const iiMap varIndexMap, 
                            const iiMap eqnIndexMap);
  virtual iiMap getEquationsVectorMap(const iiMap &equationsMap);
  virtual iiMap getReverseEquationsVectorMap
                          (const iiMap &equationsMap);
};

/**
* @class HybridShootingMapping
*
* @brief Defining mapping of multi stage problem. in case of hybrid shooting
* Here, we need to assume same models for each stage.
*/
class HybridShootingMapping:public Mapping
{
  /**
  * @brief empty standard constructor
  **/
  HybridShootingMapping(){};
  /**
  * @brief empty standard destructor
  **/
  ~HybridShootingMapping(){};
};

/**
* @class ModelChangeMapping
*
* @brief Defining mapping of multi stage problem. in case of model change mapping
*/
class ModelChangeMapping:public Mapping
{
  /**
  * @brief empty standard constructor
  **/
  ModelChangeMapping(){};
  /**
  * @brief empty standard destructor
  **/
  ~ModelChangeMapping(){};
  
};

