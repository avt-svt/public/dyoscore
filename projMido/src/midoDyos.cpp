#define MAKE_MIDO_DLL

#include "MidoDyos.hpp"
#include <cassert>
#include "DyosWithLogger.hpp"
//#include "ilcplex/cplex.h"

enum BinaryStatus
{
  RELAXED, ///< variable may have values between 0 and 1 and is optimized
  FIXED_ZERO, ///< variable is fixed on 0.0
  FIXED_ONE ///< variable is fixed on 1.0
};

UserInput::Input createMidoStepInput(const struct UserInput::MidoInput &input,
                                     const std::vector<std::vector<BinaryStatus> > &binStatus);
void optimizeMido(const struct UserInput::MidoInput &input,
                  const struct UserOutput::Output &dyosOut,
                  const std::vector<std::vector<BinaryStatus> > &binStatus,
                        double &upperBound,
                  const double lowerBound,
                        struct UserOutput::Output &bestSolution,
                  const Logger::Ptr &logger);

bool isAllFixed(const std::vector<std::vector<BinaryStatus> > &binStatus);
void getNextBinary(const struct UserInput::MidoInput &input,
                   const struct UserOutput::Output &dyosOut,
                   const std::vector<std::vector<BinaryStatus> > &binStatus,
                         unsigned &stageIndex,
                         unsigned &binaryIndex,
                         BinaryStatus &boundStatus);
void checkForBestSolution(struct UserOutput::Output &dyosOut,
                          struct UserOutput::Output &optimumCandidate,
                          double &upperBound);
void fixAllBounded(const struct UserInput::MidoInput &input,
                   const struct UserOutput::Output &dyosOut,
                         std::vector<std::vector<BinaryStatus> > &binStatus);
                                 

/**
* @brief convert MidoInput into a general UserInput converting binaries to parameter
*
* the converted input is a copy of the mido input in which all relaxed binary
* parameters are added as additional optimization parameters. Fixed binaries
* are added as constant parameters. Binary Parameters must not be parameters
* of the original optimization problem
* @param input MidoInput to be converted
* @param binStatus vector field (one vector for each stage) of the binary
                   status of all binary parameters
* @return converted Input
*/
UserInput::Input createMidoStepInput(const struct UserInput::MidoInput &input,
                                     const std::vector<std::vector<BinaryStatus> > &binStatus)
{
  assert(binStatus.size() == input.midoStages.size());
  UserInput::Input midoStepInput = input;
  for(unsigned i=0; i<input.midoStages.size(); i++){
    assert(binStatus[i].size() == input.midoStages[i].binaryParameters.size());
    for(unsigned j=0; j<input.midoStages[i].binaryParameters.size(); j++){
      UserInput::ParameterInput binaryParameter = input.midoStages[i].binaryParameters[j];
      binaryParameter.paramType = UserInput::ParameterInput::TIME_INVARIANT;
      switch(binStatus[i][j]){
        case RELAXED:
          binaryParameter.sensType = UserInput::ParameterInput::FULL;
          binaryParameter.lowerBound = 0.0;
          binaryParameter.upperBound = 1.0;
          binaryParameter.value = 0.5;
          break;
        case FIXED_ZERO:
          // maybe the user could set that to fractional as well
          binaryParameter.sensType = UserInput::ParameterInput::NO;
          binaryParameter.lowerBound = 0.0;
          binaryParameter.upperBound = 0.0;
          binaryParameter.value = 0.0;
          break;
        case FIXED_ONE:
          binaryParameter.sensType = UserInput::ParameterInput::NO;
          binaryParameter.lowerBound = 1.0;
          binaryParameter.upperBound = 1.0;
          binaryParameter.value = 1.0;
          break;
        default:
          assert(false);
      }

      midoStepInput.stages[i].integrator.parameters.push_back(binaryParameter);
    }
  }
  return midoStepInput;
}

/**
* @brief recursive branch and bound function
*
* @param[in] input MidoInput containing the optimization problem
* @param[in] dyosOut output of the last optimization (to determine the next binary to fix)
* @param[in] binStatus current status of all binaries
* @param[in,out] upperBound ojective value of the currently best result 
*                           - is overridden if this optimization leads to a better result
* @param lowerBound[in] optimization result of the parent node (always better than all child nodes)
* @param bestSolution[in,out] currently best result
*                           - is overridden if this optimization leads to a better result
* @note currently child nodes still are calculated even if the parent node was not optimal.
*       The algorithm would be faster, if such children would be rejected.
*/
void optimizeMido(const struct UserInput::MidoInput &input,
                  const struct UserOutput::Output &dyosOut,
                  const std::vector<std::vector<BinaryStatus> > &binStatus,
                        double &upperBound,
                  const double lowerBound,
                        struct UserOutput::Output &bestSolution,
                  const Logger::Ptr &logger)
{
  /*if(dyosOut.optimizerOutput.resultFlag != UserOutput::OptimizerOutput::OK
     && dyosOut.optimizerOutput.resultFlag != UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS){
     return;
  }*/
  if(upperBound - lowerBound < 1E-8){ //replace fixed number with user given tolerance
    return;
  }
  // determine next binary to fix
  BinaryStatus boundStatus = RELAXED;
  unsigned stageIndex, binaryIndex;
  getNextBinary(input, dyosOut, binStatus, stageIndex, binaryIndex, boundStatus);
  if(boundStatus != RELAXED){
    //only binaries on bounds left - so no further branching necessary
    std::vector<std::vector<BinaryStatus> > binStatusCopy = binStatus;
    binStatusCopy[stageIndex][binaryIndex] = boundStatus;
    fixAllBounded(input, dyosOut, binStatusCopy);
    
    UserInput::Input currentNodeInput = createMidoStepInput(input, binStatusCopy);
    UserOutput::Output currentDyosOut = runDyos(currentNodeInput, logger);
    bestSolution.solutionHistory.push_back(currentDyosOut);
    checkForBestSolution(bestSolution, currentDyosOut, upperBound);
  }
  else{
    // create two childNodes and optimize both
    std::vector<std::vector<BinaryStatus> > binStatusZero = binStatus, binStatusOne = binStatus;
    binStatusZero[stageIndex][binaryIndex] = FIXED_ZERO;
    binStatusOne[stageIndex][binaryIndex] = FIXED_ONE;
 
    UserInput::Input zeroNodeInput = createMidoStepInput(input, binStatusZero);
    UserInput::Input oneNodeInput = createMidoStepInput(input, binStatusOne);
    
    UserOutput::Output zeroNodeOut = runDyos(zeroNodeInput, logger);
    UserOutput::Output oneNodeOut = runDyos(oneNodeInput, logger);
    bestSolution.solutionHistory.push_back(zeroNodeOut);
    bestSolution.solutionHistory.push_back(oneNodeOut);
    assert(isAllFixed(binStatusOne) == isAllFixed(binStatusZero));
    if(isAllFixed(binStatusZero)){
      checkForBestSolution(bestSolution, zeroNodeOut, upperBound);
      checkForBestSolution(bestSolution, oneNodeOut, upperBound);
    }
    else{
      //continue algorithm in branches (start with the branch having the better result)
      if(zeroNodeOut.optimizerOutput.objVal < oneNodeOut.optimizerOutput.objVal){
        optimizeMido(input, zeroNodeOut, binStatusZero, upperBound, zeroNodeOut.optimizerOutput.objVal, bestSolution, logger);
        optimizeMido(input, oneNodeOut, binStatusOne, upperBound, oneNodeOut.optimizerOutput.objVal, bestSolution, logger);
      }
      else{
        optimizeMido(input, oneNodeOut, binStatusOne, upperBound, oneNodeOut.optimizerOutput.objVal, bestSolution, logger);
        optimizeMido(input, zeroNodeOut, binStatusZero, upperBound, zeroNodeOut.optimizerOutput.objVal, bestSolution, logger);
      }//else -> if zeroNodeOut > oneNodeOut
    }//else -> if isAllFixed
  }//else -> if boundStatus != RELAXED
  
}

/**
* @brief check if all binaries are fixed
* @binStatus status vector field (a vector for each stage) of all binaries
* @return true, if all binaries are fixed, false otherwise
*/
bool isAllFixed(const std::vector<std::vector<BinaryStatus> > &binStatus)
{
  for(unsigned i=0; i<binStatus.size(); i++){
    for(unsigned j=0; j<binStatus[i].size(); j++){
      if(binStatus[i][j] == RELAXED){
        return false;
      }
    }
  }
  return true;
}

/**
* @brief determine the next relaxed binary that is to be fixed
* @param[in] input mido input containing the problem (needed to identify the binaries in the output)
* @param[in] dyosOut output of the parent node (needed to analyse whether the relaxed binary is on the bound)
* @param[in] binStatus current status of the binaries
* @param[out] stageIndex stage index of the next binary to be fixed
* @param[out] binaryIndex parameter index of the next binary to be fixed
* @param[out] boundStatus status of the found relaxed binary ot be fixed
*                         if the binary was on the bound in the output the status is fixed,
*                         otherwise it is relaxed
*/
void getNextBinary(const struct UserInput::MidoInput &input,
                   const struct UserOutput::Output &dyosOut,
                   const std::vector<std::vector<BinaryStatus> > &binStatus,
                         unsigned &stageIndex,
                         unsigned &binaryIndex,
                         BinaryStatus &boundStatus)
{
  const double THRESHOLD = 1E-8;
  boundStatus = FIXED_ZERO;
  assert(binStatus.size() == input.midoStages.size());
  
  for(unsigned i=0; i<binStatus.size(); i++){
    assert(binStatus[i].size() == input.midoStages[i].binaryParameters.size());
    for(unsigned j=0; j<binStatus[i].size(); j++){
      if(binStatus[i][j] == RELAXED){
        stageIndex = i;
        binaryIndex = j;
        //check if binary is bounded
        for(unsigned k=0; k<dyosOut.stages[i].integrator.parameters.size(); k++){
          if(input.midoStages[i].binaryParameters[j].name == dyosOut.stages[i].integrator.parameters[k].name){
            if(dyosOut.stages[i].integrator.parameters[k].value < THRESHOLD){
              boundStatus = FIXED_ZERO;
            }
            else if(dyosOut.stages[i].integrator.parameters[k].value > 1.0 - THRESHOLD){
              boundStatus = FIXED_ONE;
            }
            else{
              boundStatus = RELAXED;
              return;
            }
            break;
          }//if
        }// for k
      }// if binStatus[i][j] == RELAXED
    }// for j
  }// for i
}

/**
* @brief check if the given output is a better solution than the current best one
*
* @param[in,out] dyosOut current best solution, will be overridden if candidate is better solution
* @param[in] optimumCandidate new solution to be checked
* @param[in,out] upperBound objective value of the current best solution,
*                           will be overridden if candidate is better solution
*/
void checkForBestSolution(struct UserOutput::Output &dyosOut,
                          struct UserOutput::Output &optimumCandidate,
                          double &upperBound)
{
  if(upperBound > optimumCandidate.optimizerOutput.objVal){
    if(optimumCandidate.optimizerOutput.resultFlag == UserOutput::OptimizerOutput::OK
       || optimumCandidate.optimizerOutput.resultFlag == UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS){
      //todo check which optimization result should lead to which reaction of the algorithm
      upperBound = optimumCandidate.optimizerOutput.objVal;
      optimumCandidate.solutionHistory = dyosOut.solutionHistory;
      dyosOut = optimumCandidate;
    }
  }
}

/**
* @brief set all binaries to fixed
* 
* if all binary variables that are not fixed are on their bounds after optimization, there is no need 
* to make further branches, so all such binaries are fixed on their bounds
* @param[in] input complete problem input (needed for getNextBinary)
* @param[in] dyosOut dyos output of the parent node (needed for getNextBinary)
* @param[in,out] binStatus vector field of the status of all binaries in which all binaries are to be fixed
*/
void fixAllBounded(const UserInput::MidoInput &input, const UserOutput::Output &dyosOut,
                         std::vector<std::vector<BinaryStatus> > &binStatus)
{
  unsigned stageIndex=0, binaryIndex = 0;
  BinaryStatus boundStatus = RELAXED;
  while(!isAllFixed(binStatus)){
    getNextBinary(input, dyosOut, binStatus, stageIndex, binaryIndex, boundStatus);
    assert(boundStatus != RELAXED);
    binStatus[stageIndex][binaryIndex] = boundStatus;
  }
}

/**
* @brief main routine of midos branc and bound
*
* @param[in] input input containing complete input
* @return result of the branch and bound algorithm
*/
LINKDLL UserOutput::Output runMidoDyos(const struct UserInput::MidoInput &input)
{
  Logger::Ptr logger(new StandardLogger());
  
  return runMidoDyosWithLogger(input, logger);
}

/**
* @brief main routine of midos branc and bound
*
* @param[in] input input containing complete input
* @return result of the branch and bound algorithm
*/
LINKDLL UserOutput::Output runMidoDyosWithLogger(const struct UserInput::MidoInput &input,
                                                 const Logger::Ptr &logger)
{
  std::vector<std::vector<BinaryStatus> > binStatus;
  binStatus.resize(input.midoStages.size());
  for(unsigned i=0; i<input.midoStages.size(); i++){
    binStatus[i].resize(input.midoStages[i].binaryParameters.size(), RELAXED);
  }
  
  //calculate first node of branch and bound
  UserInput::Input startNode = createMidoStepInput(input, binStatus);
  UserOutput::Output dyosOut = runDyos(startNode, logger);
  UserOutput::Output dyosOutOpt;
  dyosOutOpt.solutionHistory.push_back(dyosOut);
  //if first integration did not work - stop right here
  //Mido algorithm needs to access to dyosOut.stage[0]
  if(dyosOut.stages.size() == 0){
    return dyosOutOpt;
  }
  // currently initialize for minimization otherwise swap upperBound and lowerBound
  double lowerBound = dyosOut.optimizerOutput.objVal;
  double upperBound = DYOS_DBL_MAX;
  
  //start branch and bound algorithm
  optimizeMido(input, dyosOut, binStatus, upperBound, lowerBound, dyosOutOpt, logger);
  
  return dyosOutOpt;
}