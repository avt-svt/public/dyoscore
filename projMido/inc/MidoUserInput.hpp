/**
* @file MidoUserInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mido                                                                 \n
* =====================================================================\n
* This file contains the enhanced input struct for Mido users. The     \n
* parameter struct gets the additional type BINARY and Mido options are\n
* added to the toplevel input struct.                                  \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 29.04.2013
* @sa UserInput.hpp
*/

#pragma once

#include "UserInput.hpp"


#ifdef WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT 
#endif

//namespace documented in UserInput.hpp
namespace UserInput
{
  struct MidoStages
  {
    std::vector<UserInput::ParameterInput> binaryParameters;
  };
  
  /**
  * @struct MidoInput
  * @brief information provided by user to create a GenericEso object
  */
  struct MidoInput : public Input
  {
    std::vector<MidoStages> midoStages;
  };
  

}