/**
* @file MidoDyos.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mido                                                                 \n
* =====================================================================\n
* This file contains the Dyos interface for Mido users. The interface  \n
* contains an enhanced user input struct.                              \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 29.04.2013
*/

#pragma once
#include "MidoUserInput.hpp"
#include "UserOutput.hpp"
#include "Logger.hpp"

/**
* @def LINKDLL
* @brief defines whether a function is to be imported or exported
*/
#undef LINKDLL
#if WIN32
#ifdef MAKE_MIDO_DLL
#define LINKDLL __declspec(dllexport)
#else
#define LINKDLL __declspec(dllimport)
#endif
#else
#define LINKDLL
#endif

LINKDLL UserOutput::Output runMidoDyos(const struct UserInput::MidoInput &input);
LINKDLL UserOutput::Output runMidoDyosWithLogger(const struct UserInput::MidoInput &input,
                                                 const Logger::Ptr &logger);