/**
* @file ForwardIntegratorNixe.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* ForwardIntegratorNixe - Part of DyOS                                    \n
* ========================================================================\n
* This file contains the class definitions of the ForwardIntegrator class \n
* ========================================================================\n
* @author Moritz Schmitz, Tjalf Hoffmann
* @date 3.9.2012
*/
#pragma once

#include "GenericIntegratorForward.hpp"
#include "MetaSSEDaeInterface.hpp"
#include "NixeForwardSolver.hpp"
#include "NixeOptions.hpp"


//! definition of type ForwardSolverNixePtr
typedef std::shared_ptr<Nixe::ForwardSolver<Nixe::CompressedColumn> > ForwardSolverNixePtr;

/**
* @class ForwardIntegratorNixe
*
* @brief Class defining a GenericIntegrator
*
* Derived class of the class GenericIntegrator. With this class, the user is able to call the inte-
* grator NIXE (Hannemann et al.) for forward integration of an optimization problem defined by the
* IntegratorMetaData object provided to the constructor. This forward integration can be either of
* zeroth order (only state integration) or of first order (state integration and first-order
* forward sensitivity calculation) depending on the input 'order'.
*/
class ForwardIntegratorNixe : virtual public GenericIntegratorForward
{
public:
  struct NixeOptions
  {
    double initialStepSize;
    double absTolScalar;
    double relTolScalar;
    //! order of the sensitivity calculation done by Nixe: -'zerothOrder': only state integration
    //! -'firstForwOrder': state integration and calculation of first-order forward sensitivities
    TypeDaeInterface order;
    NixeOptions()
    {
      initialStepSize = 1e-3;
      absTolScalar = 1e-5;
      relTolScalar = 1e-5;
      order = firstForwOrder;
    }
  };

  typedef std::shared_ptr<ForwardIntegratorNixe> Ptr;
  ///@deprecated
  ForwardIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                        const NixeOptions &options);
  ForwardIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                        const NixeOptions &options,
                        const DaeInitialization::Ptr &daeInit);
  virtual ~ForwardIntegratorNixe();
  IntegratorMetaData::Ptr getIntegratorMDPtr() const;
  DaeInitialization::Ptr getDaeInitializationPtr() const;
  virtual ResultFlag solve();
  virtual GenericIntegratorForward::SolveStepResult solveStep(double& starttime,
                                                              const double endtime,
                                                              utils::Array<double> &finalStates,
                                                              utils::Array<double> &finalSensitivities);

  virtual void initializeStageIntegration();



protected:
  ForwardIntegratorNixe();
  //! shared pointer to an object of type IntegratorMetaData accessing all integration relevant data
  IntegratorMetaData::Ptr m_integratorMetaData;
  //! shared pointer to an object of type MetaSSEDaeInterface defining the interface to Nixe
  NIXE_SHARED_PTR<MetaSSEDaeInterface> m_metaSSEDae;
  //! driver for Nixe used in case of a forward integration, that takes as argument
  //! an object of type Nixe::DaeInterface and an object of type Nixe::Options
  //Nixe::ForwardSolver::Ptr ForwardSolverNixePtr;
  ForwardSolverNixePtr m_nixeFor;
  //! options for the integration with Nixe
  NIXE_SHARED_PTR<Nixe::Options> m_optionsNixe;
  //! Vector of vector of Nixe:Checkpoints saving states (if m_order = firstRevOrder) and first
  //! order forward sens (if m_order = secondRevOrder). They are saved after each integration
  //! interval defined by m_integratorMetaData
  std::vector<std::vector<NIXE_SHARED_PTR<Nixe::Checkpoints> > > m_checkpoints;
  //! vector of Nixe:Checkpoints saving states (if m_order = firstRevOrder) and first
  //! order forward sens (if m_order = secondRevOrder) of only initial value parameters. They
  //! are saved at the beginning of a new physical stage
  std::vector<NIXE_SHARED_PTR<Nixe::Checkpoints> > m_initialValueCheckpoints;
  //! array of size 'numStates' to extract the states from Nixe after each solved interval.
  //! They are transformed afterwards and finally stored in m_integratorMetaData
  NIXE_SHARED_PTR<Nixe::Array<double> > m_states;
  //! user set options for Nixe (such as order and tolerances
  NixeOptions m_options;

  //DaeInitialization object used to initialize the Eso for each integration step
  DaeInitialization::Ptr m_daeInitialization;

  //! flag that is only set if integrator is to be called for the first time
  bool m_isFirstIntegrationCall;
  //! index of the stage that is currently integrated
  int m_stageIndex;


  virtual void storeSensInformation(utils::Array<double> &finalSensitivities) const;
};
