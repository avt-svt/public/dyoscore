#include <nvector/nvector_serial.h>
#include <sundials/sundials_direct.h>
#include <sundials/sundials_dense.h>
#include <sunlinsol/sunlinsol_dense.h>
#include <sunmatrix/sunmatrix_sparse.h>

/** Compute residual of DAE system */
int idas_res(double t, N_Vector yy, N_Vector yp, N_Vector resval, void *user_data);

/** Compute sensitivity RHS */
int idas_resS(int Ns, double t,
	      N_Vector yy, N_Vector yp, N_Vector resval,
	      N_Vector *yyS, N_Vector *ypS, N_Vector *resvalS,
	      void *user_data,
	      N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);

int ida_dense_jac(realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector rr, SUNMatrix Jac, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);
int ida_sparse_jac(realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector rr, SUNMatrix Jac, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);

int ida_root(double t, N_Vector yy, N_Vector yp, double * gout, void *user_data);