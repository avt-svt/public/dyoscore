/** @file LimexLoader.hpp
*    @brief header declaring LimexLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails LimexLoader
*    =====================================================================\n
*   @author Stefan Jaeger, Klaus Stockmann
*   @date 12.1.2012
*/
#pragma once

#include <cstdlib>
#include <cstdio>
#include <vector>
#include "Array.hpp"
#include "Loader.hpp"

//typedefs of function pointers for call of Limex - only for better readability of the Limex_func typedef

typedef void (*fcn_ptr)(const int &, int &, const double &, const double *, const double *,
                        const int *, double *, double *, int *, int *, int &, const int *);

typedef void (*jacobian_ptr)(const int &, const double &, const double *, const double *,
                             const double *, const int *, double *, int *, int *, int &, int &);

typedef void ( *slimexs_func)(int*, //nDAE
                                      int*, //n
                                      fcn_ptr, //Fcn
                                      jacobian_ptr, //Jacobian
                                      double*, //t_Begin
                                      double*, //t_End
                                      double[], //y
                                      double[], //ys
                                      double[], //Rpar
                                      int[],  //Ipar
                                      double*, //rTol
                                      double*, //aTol
                                      double*, //h
                                      int*, //Iopt
                                      int[], //IoptS
                                      double[], //Ropt
                                      int[], //IPos
                                      int*, //IFail
                                      int*, //ConvTest
                                      double*, //upperBounds
                                      double*, //lowerBounds
                                      int*); //Integration_Failure



/**
* @class LimexLoader
*
* @brief Load Limex DLL
*
* Derived class of the class utils::Loader.
*/
class LimexLoader:public utils::Loader
{
protected:
  /** @brief  handle to Limex main routine */
  slimexs_func slimexs_func_handle;

public:
  //! @brief contains all options required by the limex integrator
  struct LimexOptions{
    utils::Array<int> IFail;
    utils::Array<double> Rpar;
    utils::Array<int> Iopt;
    utils::Array<int> IPos;
    utils::Array<double> Ropt;
    utils::Array<int> Iopts;
    utils::Array<int> Ipar;
    utils::Array<double> rTol;
    utils::Array<double> aTol;
    utils::Array<int> /* logical*/  ConvTest;
    utils::Array<int> Integ_Fail;
    utils::Array<double> lower_bounds;
    utils::Array<double> upper_bounds;
    //vector Rpar is not used at all inside the fortran code, but is used to push some data
    //into the callbacks. Since these data are probably not needed the Rpar vector is left
    //empty for present.
    LimexOptions(int nModelEqns, int nStatesSens) :
      IFail(3,0), Rpar(0), Iopt(30, 0), IPos(nStatesSens, 0),
      Ropt(10, 0.0), Iopts(10, 0), Ipar(nStatesSens, 0),
      rTol(nStatesSens, 0.0), aTol(nStatesSens, 0.0),
      ConvTest(nModelEqns, 1), Integ_Fail(nModelEqns, 0),
      lower_bounds(nModelEqns, -1.0e10), upper_bounds(nModelEqns, +1.0e10)
    {
    }
  };
  LimexLoader();
  //! copy constructor
  LimexLoader(const LimexLoader &LimexLoader);
  ~LimexLoader(){};


/**
  * @brief wrapper for Fortran function SLIMEXS
  *
  * @param[in] nDAE number of equations in DAE system
  * @param[in] n total number of equations (DAE and sensitivity equations)
  * @param[in] fcn pointer to callback function providing rhs of DAE and possibly sensitivity equations
  * @param[in] jacobian pointer to callback function providing Jacobian matrix of DAE system
  * @param[in, out] t_Begin On entry, starting point  of the integration.
  *                         On exit, the time point up to which LIMEX has solved the DAE successfully
  * @param[in] t_End end point of the integration
  * @param[in, out] y vector of  size  n, must  be set  by caller  on
  *                   input. The  initial  values  of the  solution at  the
  *                   starting  point  t_Begin. On  exit,  y  contains  the
  *                   solution  at  the  final  point t_End, resp. at  this
  *                   value of the independent variable, up  to which LIMEX
  *                   has solved the DAE successfully
  * @param[in, out] ys vector of size n. On input, derivatives  of the solution at
  *                    t_Begin. Only the values  of ys for  the differential
  *                    variables  are needed. If the  values for ys are  not
  *                    known, set ys to  zero. If only estimates are  known,
  *                    these  should be  used. However, the  option  Iopt(6)
  *                    for the  computation of consistent initial values can
  *                    be used to  start always with  correct values for ys.
  *                    On exit, ys contains  the derivatives at t_End, resp.
  *                    at this  value  of  the  independent  variable, up to
  *                    which LIMEX has solved the DAE successfully.
  * @param[in] Rpar vector of arbitrary length holding
  *                 the parameters for which sensitivities are required.
  *                 Rpar is transfered to the function evaluation and jacobian
  *                 subroutines.
  * @param[in, out] Ipar an integer vector of size nDAE holding parameters,
  *                      which are transfered to the function evaluation and
  *                      jacobian subroutines
  *                      It is used to collect information of not converged states
  *                      (in relation with ErrFlag = -46)
  * @param[in] rTol vector  of size 1 at least for Iopt(11) = 0 or n
  *                 for Iopt(11) = 1. Values  must be  set by  caller  on
  *                 input.  Relative error tolerances,
  *                 interpreted as a scalar or a vector  depending on the
  *                 value of Iopt(11). The code keeps the local  error of
  *                 y(i)  below  rTol(i)*abs(y(i))+aTol(i).
  *                 Restriction: rTol(*) > 0.
  * @param[in] aTol vector of size 1 at least  for Iopt(11) = 0 or n
  *                 for  Iopt(11) = 1. Values  must be set by  caller  on
  *                 input,  not  modified.  Absolute  error   tolerances,
  *                 interpreted as a scalar or a vector  depending on the
  *                 value of Iopt(11).
  *
  *                 Restriction: aTol(*) => 0.
  *
  *                 Remark: For any  component y(i), which could  be zero
  *                 during the integration  one has to set aTol(i) > 0 to
  *                 prevent  divisions  by  zero.  In  many  well  scaled
  *                 problems a reasonable setting is aTol(i) = rTol(i).
  * @param[in, out] h Integrator stepsize, must  be  set  by  caller  on  input.
  *                   Initial  stepsize  guess, if h  is  set  to zero, the
  *                   program puts  h = h_Min. This  variable is  currently
  *                   set  in a  parameter statement to 1.d0-4, but  may be
  *                   changed. On exit h is the  estimated optimal stepsize
  *                   for the next  integration step. If an  error occurred
  *                   h is set to zero.
  * @param[in] Iopt Integer  array  of  size 30, values  from  Iopt(1) to
  *                 Iopt(23) must be set by caller on  input. Integration
  *                 control parameters. On output, Iopt(24) through Iopt(30)
  *                 contain various statistical data. Todo: change Iopt to [in, out] parameter,
  *                 or, better, split Iopt into input and output vector
  * @param[in] IoptS is a integer vector of size 10 containing additional
  *                  Info flags for the sensitivity analysis which are not
  *                  available in the standart version of LIMEX (cf.
  *                  Chapter 3 of Martin Schlegel's thesis)
  * @param[in] Ropt Real array of size 5, Ropt(1) to Ropt(5) must  be set
  *                 by  caller on input, Ropt(4) only if Iopt(19) = 1 and
  *                 Ropt(5)  only  if  Iopt(20) > 0. Integration  control
  *                 parameter.
  * @param[in] IPos Integer array of size n, values must be set by caller
  *                 on input, not modified. If IPos(i) = 1, the values of
  *                 the corresponding  solution component will be checked
  *                 to be  nonnegative. If  during  the  extrapolation  a
  *                 negative value occurs, the stepsize is reduced by the
  *                 heuristic factor RedPos, which  is set in a parameter
  *                 statement to 0.5, but  may  be changed. If IPos(i) is
  *                 not equal 1, no check will be performed.
  * @param[out] IFail Integer array of size 3, need not to be set by caller
  *                   on input. Error indicator field.
  * @param[in] ConvTest vector of size nDAE. not yet documented
  * @param[in] upperbounds vector of size nDAE. not yet documented
  * @param[in] lowerbounds vector of size nDAE. not yet documented
  * @param[out] Integration_Failure vector of size nDAE. not yet documented
  */
  int slimexs(const int nDae, // input; size: 1
              const int n, // input; size: 1
              fcn_ptr fcn,
              jacobian_ptr jacobian,
              double &t_Begin, //double *t_Begin,  // in, out; size: 1
              const double t_End, // input; size: 1
              utils::Array<double> &y, // in, out; size: n
              utils::Array<double> &ys, // in, out; size: n
              double &h, //double *h, // in, out; size: 1
              LimexOptions &options);

};
