/**
* @file Rev2ndOrdIntegratorNixe.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* Rev2ndOrdIntegratorNixe - Part of DyOS                                  \n
* ========================================================================\n
* This file contains the class definitions of the ReverseIntegrator class \n
* providing functions to calculate second order Sensitivities of the      \n
* Lagrange function                                                       \n
* ========================================================================\n
* @author Moritz Schmitz
* @date 14.06.2012
*/
#pragma once

#include "Generic2ndOrderIntegrator.hpp"
#include "ReverseIntegratorNixe.hpp"
#include "MetaDaeInterface2ndOrder.hpp"


/**
* @class Rev2ndOrdIntegratorNixe
*
* @brief Class defining a GenericIntegrator
*
* Derived class of the class GenericIntegrator. With this class, the user is able to call the inte-
* grator NIXE (Hannemann et al.) for forward and subsequent reverse integration of an optimization
* problem defined by the IntegratorMetaData object provided to the constructor. This reverse
* integration is of second order (state and first-order sensitivity integration in forward mode and
* first and second order adjoints and first and second order Lagrange derivatives reverse mode).
*/
class Rev2ndOrdIntegratorNixe : virtual public Generic2ndOrderIntegrator,
                                virtual public ReverseIntegratorNixe
{
public:
  ///@deprecated
  Rev2ndOrdIntegratorNixe(const IntegratorMetaData2ndOrder::Ptr &integratorMD2ndOrder,
                          const ForwardIntegratorNixe::NixeOptions &options);
  Rev2ndOrdIntegratorNixe(const IntegratorMetaData2ndOrder::Ptr &integratorMD2ndOrder,
                          const ForwardIntegratorNixe::NixeOptions &options,
                          const DaeInitialization::Ptr &daeInit);
  ~Rev2ndOrdIntegratorNixe();
  IntegratorMetaData::Ptr getIntegratorMDPtr() const;
  ResultFlag solve();
  ResultFlag solveInterval();
  GenericIntegratorForward::SolveStepResult solveStep(double& starttime,
                                                      const double endtime,
                                                      utils::Array<double> &finalStates,
                                                      utils::Array<double> &finalSensitivities);
  void initializeStageIntegration();
  virtual void initializeStageIntegrationReverse();
  void solveStepReverse(double& reverseStartTime,
                        const double reverseEndTime,
                        utils::Array<double> &adjoints,
                        utils::Array<double> &lagrangeDerivatives);
  virtual void evaluateAtInitialPointReverse(utils::Array<double> &adjoints,
                                             utils::Array<double> &lagrangeDerivatives);
  unsigned getNumAdjoints() const;
  unsigned getNumLagrangeDers() const;
  virtual DaeInitialization::Ptr getDaeInitializationPtr() const;

protected:
  //! shared pointer to an object of type IntegratorMetaData2ndOrder accessing all integration relevant data
  //! containing an ESO able to calculate second order model derivatives
  IntegratorMetaData2ndOrder::Ptr m_integratorMD2ndOrder;
  //! shared pointer to an object of type MetaDaeInterface2ndOrder defining the interface to Nixe
  NIXE_SHARED_PTR<MetaDaeInterface2ndOrder> m_metaDae2ndOrder;

  virtual void storeSensInformation(utils::Array<double> &finalSensitivities) const;
};
