/**
* @file MetaSSEDaeInterface.hpp
*
* ==========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                         \n
* ==========================================================================\n
* MetaSSEDaeInterface - Part of DyOS                                        \n
* ==========================================================================\n
* This file contains the class definitions of the MetaSSEDaeInterface class \n
* ==========================================================================\n
* @author Moritz Schmitz
* @date 19.10.2011
*/
#pragma once

#include "NixeDaeInterface.hpp"
#include "NixeCCSInterface.hpp"
#include "NixeProblemInfo.hpp"
#include "IntegratorMetaData.hpp"
#include "DyosObject.hpp"

//#include <boost/shared_ptr.hpp>
#include <memory>
#include "TypeDaeInterface.hpp"

#ifndef NIXE_SHARED_PTR
#define NIXE_SHARED_PTR std::shared_ptr
#endif


/**
* @class MetaSSEDaeInterface
*
* @brief Class defining a NixeDaeInterface
*
* Derived class of the class NixeDaeInterface. This class is an implementation for the
* NixeDaeInterface that has to be provided in order to use the NIXE integrator for a
* problem defined as MetaSingleStageEso object.
*/
class MetaSSEDaeInterface : public Nixe::DaeInterface<Nixe::CCSInterface>,
                            public DyosObject
{
public:
  typedef std::shared_ptr<MetaSSEDaeInterface> Ptr;
  MetaSSEDaeInterface(const IntegratorMetaData::Ptr &integratorMetaData, TypeDaeInterface type);
  void initializeForNewIntegration(const double starttime, const double endtime);
  virtual void EvalMass(Nixe::CCSInterface *mass);
  virtual void EvalForward(bool evalJac, double time, double *states, double *parameters,
                           double *rhsStates, double *sens, Nixe::CCSInterface *jacobian,
                           double *rhsStatesDtime, double *rhsSens,
                           const Nixe::ProblemInfo &problemInfo) const;
  virtual void EvalReverse(double time, double *states, double *parameters, double *sens,
                           double *adjoints, double *rhsAdjoints, double *rhsDers,
                           const Nixe::ProblemInfo &problemInfo) const;
  virtual void EvalInitialValues(double time, double *parameters, double *states, double *sens,
                                 const Nixe::ProblemInfo &problemInfo);
  virtual void GetStateJacobianStruct(int &nNonZeroDfdx, int *rows, int *cols);
  virtual void EvalFinalValues(double time, double *states, double *parameters, double *sens,
                               double *adjoints, const Nixe::ProblemInfo &problemInfo);
  virtual void EvalSwitchingFunction(double time, double *states, double *parameters,
                                     double *switchingFctn, const Nixe::ProblemInfo &problemInfo) const;
  virtual void EvalParameters(double *parameters, const Nixe::ProblemInfo &problemInfo);
  virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo();
  virtual void EvalRhs(int len, long *indices, double time, double *states, int dimStates,
                       double *rhsStates);
  virtual void EvalStateJac(double time, double* states, int dimStates, int* rows,
                            int* cols, int dim, int* SparsityPattern, int* jacRowInd,
                            int* jacColInd, double* jacVal, int len);

private:
  IntegratorMetaData::Ptr m_integratorMetaData;
  TypeDaeInterface m_typeDaeInterface;
  NIXE_SHARED_PTR<Nixe::ProblemInfo> m_problemInfo;
};
