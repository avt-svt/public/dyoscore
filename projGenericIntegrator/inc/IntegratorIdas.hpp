
#pragma once

#include "GenericIntegratorForward.hpp"

// forward declarations
struct _generic_N_Vector;
typedef _generic_N_Vector* N_Vector;

class ForwardIntegratorIdas : public GenericIntegratorForward
{
public:

	struct IdasOptions {
		double m_relativeTolerance;
		double m_absoluteTolerance;
		long int m_maxSteps;
		int m_maxNumH;
		int m_maxNumNonLinItSens;
		bool m_errorControlSen;
		int m_sensIntMethod;     // IDA_SIMULTANEOUS = 1
								 // IDA_STAGGERED = 2
		IdasOptions() {
			m_relativeTolerance = 1e-6;
			m_absoluteTolerance = 1e-6;
			m_maxSteps = 100000;
			m_maxNumH = 100;
			m_maxNumNonLinItSens = 30;
			m_errorControlSen = false; 
			m_sensIntMethod = 2; // IDA_SIMULTANEOUS = 1
								 // IDA_STAGGERED = 2
		}
	};

  ForwardIntegratorIdas(const IntegratorMetaData::Ptr &integratorMetaData, const bool calculateSensitivities,
                        const DaeInitialization::Ptr &daeInit, const IdasOptions idasOptions);
  ForwardIntegratorIdas(const IntegratorMetaData::Ptr &integratorMetaData,const DaeInitialization::Ptr &daeInit);

  virtual ~ForwardIntegratorIdas();
  virtual IntegratorMetaData::Ptr getIntegratorMDPtr() const {return m_intmd;}
  virtual DaeInitialization::Ptr getDaeInitializationPtr() const {return m_daeInit;}
  virtual GenericIntegratorForward::ResultFlag solveInterval();
  virtual GenericIntegratorForward::SolveStepResult solveStep(double& starttime,
                                                              const double endtime,
                                                              utils::Array<double> &finalStates,
                                                              utils::Array<double> &finalSensitivities);
  void initialize(N_Vector& yvec, N_Vector& ydvec, N_Vector*& ysensvec, N_Vector*& ydsensvec);


private:
	IntegratorMetaData::Ptr m_intmd;
	DaeInitialization::Ptr m_daeInit;

	IdasOptions m_IntegratorOptions;

	// true if sensitivities are calculated
	bool m_calculateSensitivities;

  /** Loader class that provides all IDAS functions */
  //IdasLoader m_idas;
  /** IDAS memory object that needs to be passed to each IDAS function call */
  void* m_ida_mem;
};
