
#pragma once
//! Characterization of the DaeInterface.
enum TypeDaeInterface
{
  zerothOrder,
  firstForwOrder,
  firstRevOrder,
  secondRevOrder
};
