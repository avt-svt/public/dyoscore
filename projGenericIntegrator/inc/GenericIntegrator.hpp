/**
* @file GenericIntegrator.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericIntegrator - Part of DyOS                                     \n
* =====================================================================\n
* This file contains the declaration of the Integrator class           \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 18.10.2011
*/
#pragma once

//#include <boost/shared_ptr.hpp>
#include <memory>
#include <vector>
#include "MetaDataOutput.hpp"
#include "DyosObject.hpp"

/**
* @class GenericIntegrator
*
* @brief abstract class for integrators used for a problem defined as IntegratorMetaData object
*
* Abstract class of the integrator providing functions to integrate and calculate sensitivities
*/
class GenericIntegrator : public DyosObject
{
public:
  typedef std::shared_ptr<GenericIntegrator> Ptr;
  /**
  * @brief destructor
  */
  virtual ~GenericIntegrator(){};

  enum ResultFlag {
    FAILED,
    SUCCESS
  };
  /**
  * @brief integrates problem and calculates sensitivities
  *
  * The problem defined by the IntegratorMetaData object is integrated and Sensitivities are calcu-
  * lated when indicated. The calculated quantities are then stored into the IntegratorMetaData
  * object.
  */
  virtual ResultFlag solve() = 0;

  virtual void activatePlotGrid() = 0;
  virtual std::vector<DyosOutput::StageOutput> getOutput() = 0;
};
