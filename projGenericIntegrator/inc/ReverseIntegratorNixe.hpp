/**
* @file ReverseIntegratorNixe.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* ReverseIntegratorNixe - Part of DyOS                                    \n
* ========================================================================\n
* This file contains the class definitions of the ReverseIntegrator class \n
* ========================================================================\n
* @author Moritz Schmitz
* @date 05.06.2012
*/
#pragma once

#include "GenericIntegratorReverse.hpp"
#include "ForwardIntegratorNixe.hpp"
#include "MetaSSEDaeInterface.hpp"

#include "NixeReverseSolver.hpp"
#include "NixeOptions.hpp"


/**
* @class ReverseIntegratorNixe
*
* @brief Class defining a GenericIntegrator
*
* Derived class of the class GenericIntegrator. With this class, the user is able to call the inte-
* grator NIXE (Hannemann et al.) for forward and subsequent reverse integration of an optimization
* problem defined by the IntegratorMetaData object provided to the constructor. This reverse
* integration is of first order (state integration in forward mode and adjoints and first order
* Lagrange sensitivity calculation in reverse mode).
*/
class ReverseIntegratorNixe : virtual public GenericIntegratorReverse,
                              virtual public ForwardIntegratorNixe
{
public:
  typedef std::shared_ptr<ReverseIntegratorNixe> Ptr;

  ///@deprecated
  ReverseIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                        const ForwardIntegratorNixe::NixeOptions &options);
  ReverseIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                        const ForwardIntegratorNixe::NixeOptions &options,
                        const DaeInitialization::Ptr &daeInit);
  ~ReverseIntegratorNixe();
  ResultFlag solve();
  ResultFlag solveForward();
  void solveReverse();
  virtual GenericIntegratorForward::SolveStepResult solveStep(double& starttime,
                                                              const double endtime,
                                                              utils::Array<double> &finalStates,
                                                              utils::Array<double> &finalSensitivities);
  virtual ResultFlag solveInterval();
  virtual void initializeStageIntegration();
  void solveStepReverse(double& reverseStartTime,
                        const double reverseEndTime,
                        utils::Array<double> &adjoints,
                        utils::Array<double> &lagrangeDerivatives);
  virtual void evaluateAtInitialPointReverse(utils::Array<double> &adjoints,
                                             utils::Array<double> &lagrangeDerivatives);
  virtual void initializeStageIntegrationReverse();
  IntegratorMetaData::Ptr getIntegratorMDPtr() const;
  unsigned getNumAdjoints() const;
  unsigned getNumLagrangeDers() const;
  virtual DaeInitialization::Ptr getDaeInitializationPtr() const;

protected:
  ReverseIntegratorNixe();
  //! index of the physical stage on which solveIntervalReverse is called
  int m_revStageIndex;
  //! counts number of calls of solveIntervalReverse() in order to choose the right checkpoint
  int m_numCallsActualStage;
  //! driver for Nixe used in case of a reverse integration, that takes as argument
  //! an object of type Nixe::DaeInterface and an object of type Nixe::Options
  NIXE_SHARED_PTR<Nixe::ReverseSolver<Nixe::CompressedColumn> > m_nixeRev;

  void copyAdjointsAndLagrangeDers(utils::Array<double> &adjoints,
                                   utils::Array<double> &lagrangeDerivatives);
  virtual void storeSensInformation(utils::Array<double> &finalSensitivities) const;
};
