/**
* @file GenericIntegratorReverse.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericIntegratorReverse - Part of DyOS                              \n
* =====================================================================\n
* This file contains the declaration of the Integrator class for       \n
* reverse integration                                                  \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 14.06.2012
*/
#pragma once

#include "GenericIntegratorForward.hpp"

/**
* @class GenericIntegratorReverse
*
* @brief abstract class for integrators used for a problem defined as IntegratorMetaData object
*
* Abstract class of the integrator providing functions to integrate and calculate sensitivities
* of the Lagrange function up to first order of an optimization problem defined by a
* IntegratorMetaData
*/
class GenericIntegratorReverse : virtual public GenericIntegratorForward
{
public:
    typedef std::shared_ptr<GenericIntegratorReverse> Ptr;
  
  /**
  * @brief destructor
  */
  virtual ~GenericIntegratorReverse(){};


  virtual void solveReverse()
  {
    // backward first order adjoint sensitivity calculation
    IntegratorMetaData::Ptr integratorMetaData = getIntegratorMDPtr();

    const unsigned numStages = integratorMetaData->getNumStages();
    /*Iteration over all physical stages*/
    for(int i=int(numStages)-1; i>=0; i--){
      if(i!=int(numStages)-1){
        m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "|" );
      }
      integratorMetaData->initializeAtStageDuration();
      initializeStageIntegrationReverse();
      solveIntervalReverse();
      //resultFlag = solveIntervalReverse();
      //if(resultFlag != SUCCESS){
      //  return resultFlag;
     //}

      /*Backward integration for the next (numIntervals-1) intervals*/
      const int numIntervals = integratorMetaData->getNumIntegrationIntervals();
      for(int j=numIntervals-2; j>=0; j--){
        integratorMetaData->initializeForPreviousIntegrationBlock();
        solveIntervalReverse();
       /* resultFlag = solveIntervalReverse();
        if(resultFlag != SUCCESS){
          return resultFlag;
        }*/
      }
      setInitialStageState();
    }
    integratorMetaData->setInitialLagrangeDerivatives();
    m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "\n" );
  }
  
  virtual ResultFlag solveForward()
  {
    return GenericIntegratorForward::solve();
  }
  
  /**
  * @brief integrates problem in forward mode and calculates first order adjoints and Lagrange
  *        sensitivities
  *
  * The problem defined by the IntegratorMetaData object is integrated and adjoints and Lagrange
  * sensitivities are calculated. The calculated quantities are then stored into the
  * IntegratorMetaData object.
  */
  virtual ResultFlag solve()
  {
    // forward state integration
    const ResultFlag resultFlag = solveForward();

    if(resultFlag != SUCCESS){
      return resultFlag;
    }
    
    solveReverse();
    return resultFlag;
  }

  /**
  * @brief integrates one interval backwards from t_f = 1.0 to t_0 = 0.0.
  *
  * After a call to IntegratorMetaData::initializeForNextIntegration(), the data are set for a new
  * integration starting from the actual timepoint to IntegratorMetaData::getEndTime()==1.
  */
  virtual void solveIntervalReverse()
  {
    IntegratorMetaData::Ptr integratorMetaData = getIntegratorMDPtr();

    do{
      integratorMetaData->updateAdjoints();
      //set adjoints at final time point
      std::vector<double> adjointsVec(getNumAdjoints(), 0.0);
      utils::Array<double> dummyLagrangeDers(getNumLagrangeDers(), 0.0);
      integratorMetaData->getAdjoints(adjointsVec);
      assert(!adjointsVec.empty());
      utils::WrappingArray<double> adjointsArray(getNumAdjoints(), &adjointsVec[0]);
      integratorMetaData->setAdjointsAndLagrangeDerivatives(adjointsArray, dummyLagrangeDers);
      integratorMetaData->initializeForNewBackwardsIntegrationStep();

      const double reverseStartTime = integratorMetaData->getStepStartTime();
      double reverseEndTime   = integratorMetaData->getStepEndTime();
      while (reverseStartTime < reverseEndTime) {
         utils::Array<double> adjoints(getNumAdjoints()), lagrangeDers(getNumLagrangeDers());
        solveStepReverse(reverseEndTime, reverseStartTime, adjoints, lagrangeDers);
        integratorMetaData->setStepCurrentTime(reverseEndTime);
        integratorMetaData->setAdjointsAndLagrangeDerivatives(adjoints, lagrangeDers);
        m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "." );
      }
    }while(integratorMetaData->getStepStartTime()>0.0);
    //needed to have consistent value in m_integratorMetaData (setting not done by solve)
    integratorMetaData->setStepCurrentTime(0.0);
  }

  /**
  * @brief make evaluation at initial point of the reverse integration
  *        and set result to integrator meta data object.
  */
  void setInitialStageState()
  {
    IntegratorMetaData::Ptr integratorMetaData = getIntegratorMDPtr();
    integratorMetaData->updateAdjoints();

    utils::Array<double> adjoints(getNumAdjoints()), lagrangeDers(getNumLagrangeDers());
    evaluateAtInitialPointReverse(adjoints, lagrangeDers);
    integratorMetaData->setAdjointsAndLagrangeDerivatives(adjoints, lagrangeDers);
  }

  /**
  * @brief returns the number of scalar adjoint variables
  */
  virtual unsigned getNumAdjoints() const
  {
    return getIntegratorMDPtr()->getGenericEso()->getNumStates();
  }

  /**
  * @brief returns the number of scalar Lagrange derivatives
  */
  virtual unsigned getNumLagrangeDers() const
  {
    IntegratorMetaData::Ptr integratorMetaData = getIntegratorMDPtr();
    const unsigned numStates = integratorMetaData->getGenericEso()->getNumStates();
    const unsigned numSensParams = integratorMetaData->getNumSensitivityParameters();
    return numStates*numSensParams;
  }

  /**
  * @brief if a new stage is selected during reverse integration, this function makes necessary
  *        updates for the integrator
  *
  * @note This function is empty by default.
  *       Override if anything has to be done in the implementation
  */
  virtual void initializeStageIntegrationReverse(){};

  /**
  * @brief make a reverse step in reverse integration
  *
  * @param[out] adjoints calculated by the reverse step
  * @param[out] lagrangeDerivatives calculated by the reverse step
  */
  virtual void solveStepReverse(double& reverseStartTime,
                                const double reverseEndTime,
                                utils::Array<double> &adjoints,
                                utils::Array<double> &lagrangeDerivatives) = 0;

  /**
  * @brief integrator call at the initial point at the end of a reverse integration
  *
  * @param[out] adjoints result of the evaluation
  * @param[out] lagrangeDerivatives result of the evaluation
  */
  virtual void evaluateAtInitialPointReverse(utils::Array<double> &adjoints,
                                             utils::Array<double> &lagrangeDerivatives) = 0;

};
