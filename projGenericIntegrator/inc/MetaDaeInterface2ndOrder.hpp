/**
* @file MetaDaeInterface2ndOrder.hpp
*
* ==========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                         \n
* ==========================================================================\n
* MetaDaeInterface2ndOrder - Part of DyOS                                   \n
* ==========================================================================\n
* This file contains the class definitions of the MetaDaeInterface2ndOrder  \n
* class                                                                     \n
* ==========================================================================\n
* @author Moritz Schmitz
* @date 15.06.2012
*/
#pragma once

#include "MetaSSEDaeInterface.hpp"

/**
* @class MetaDaeInterface2ndOrder
*
* @brief Class defining a NixeDaeInterface used for 2nd order adjoint sensitivities
*
*/
class MetaDaeInterface2ndOrder : public MetaSSEDaeInterface
{
public:
  typedef std::shared_ptr<MetaDaeInterface2ndOrder> Ptr;
  MetaDaeInterface2ndOrder(const IntegratorMetaData2ndOrder::Ptr &integratorMD2ndOrder);
  virtual void EvalReverse(double time, double *states, double *parameters, double *sens,
                           double *adjoints, double *rhsAdjoints, double *rhsDers,
                           const Nixe::ProblemInfo &problemInfo) const;
private:
  IntegratorMetaData2ndOrder::Ptr m_integratorMD2ndOrder;
};
