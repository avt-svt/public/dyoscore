/**
* @file IntegratorFactory.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericIntegrator                                                    \n
* =====================================================================\n
* This file contains the declarations of class GenericIntegratorFactory\n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.02.2012
*/
#pragma once


#include "IntegratorInput.hpp"
#include "MetaDataFactories.hpp"
#include "IntegratorMetaData.hpp"
#include "GenericIntegratorForward.hpp"
#include "GenericIntegratorReverse.hpp"
#include "Generic2ndOrderIntegrator.hpp"
//#include "MetaSSEDaeInterface.hpp"
#include "TypeDaeInterface.hpp"
#include "DyosObject.hpp"

/**
* @class IntegratorFactory
* @brief factory class for creating GenericIntegrator objects
*/
class IntegratorFactory : public DyosObject
{
public:
  GenericIntegrator::Ptr createGenericIntegrator(const FactoryInput::IntegratorInput &input);
  GenericIntegrator::Ptr createGenericIntegrator(const FactoryInput::IntegratorInput &input,
                                                   IntegratorMetaDataFactory     &imdFactory);
  GenericIntegrator::Ptr createGenericIntegrator(const FactoryInput::IntegratorInput &input,
                                             const IntegratorMetaData::Ptr &integratorMetaData);
  Generic2ndOrderIntegrator::Ptr createGeneric2ndOrderIntegrator(
                                     const FactoryInput::IntegratorInput &input,
                                     const IntegratorMetaData2ndOrder::Ptr &integratorMetaData);
  GenericIntegratorForward::Ptr createNixeForward(
                                      const IntegratorMetaData::Ptr &integratorMetaData,
                                      const TypeDaeInterface order,
                                      const DaeInitialization::Ptr &daeInit,
                                      const std::map<std::string, std::string> &integratorOptions);
  GenericIntegratorReverse::Ptr createNixeReverse(
                                      const IntegratorMetaData::Ptr &integratorMetaData,
                                      const  TypeDaeInterface order,
                                      const DaeInitialization::Ptr &daeInit,
                                      const std::map<std::string, std::string> &integratorOptions);
  Generic2ndOrderIntegrator::Ptr createNixe2ndOrder(
                              const IntegratorMetaData2ndOrder::Ptr &integratorMetaData,
                              const  TypeDaeInterface order,
                              const DaeInitialization::Ptr &daeInit,
                              const std::map<std::string, std::string> &integratorOptions);
  #ifdef HAVE_LIMEX
  GenericIntegratorForward::Ptr createLimex(
                              const IntegratorMetaData::Ptr &integratorMetaData,
                              const FactoryInput::IntegratorInput::IntegrationOrder order,
                              const DaeInitialization::Ptr &daeInit,
                              const std::map<std::string, std::string> &integratorOptions);
#endif
  GenericIntegratorForward::Ptr createIdas(
                              const IntegratorMetaData::Ptr &integratorMetaData,
							  const FactoryInput::IntegratorInput::IntegrationOrder order,
                              const DaeInitialization::Ptr &daeInit,
                              const std::map<std::string, std::string> &integratorOptions);
};
