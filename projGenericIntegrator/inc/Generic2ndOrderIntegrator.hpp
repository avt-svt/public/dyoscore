/**
* @file Generic2ndOrderIntegrator.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic2ndOrderIntegrator - Part of DyOS                             \n
* =====================================================================\n
* This file contains the declaration of the Integrator class for       \n
* 2nd order reverse integration                                        \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 14.06.2012
*/
#pragma once

#include "GenericIntegratorReverse.hpp"

//! @brief shared pointer to IntegratorMetaData objects
//typedef boost::shared_ptr<IntegratorMetaData2ndOrder> IntegratorMD2ndOrderPtr;
//IntegratorMetaData2ndOrder::Ptr IntegratorMD2ndOrderPtr;

/**
* @class Generic2ndOrderIntegrator
*
* @brief abstract class for integrators used for a problem defined as IntegratorMetaData2ndOrder
*        object
*
* Abstract class of the integrator providing functions to integrate and calculate sensitivities
* of the Lagrange function up to second order of an optimization problem defined by a
* IntegratorMetaData2ndOrder
*/
class Generic2ndOrderIntegrator : virtual public GenericIntegratorReverse
{
public:
  /**
  * @brief destructor
  */
  virtual ~Generic2ndOrderIntegrator(){};

  /**
  * @brief integrates problem in forward mode and calculates first- and second-order adjoints
  *         and Lagrange sensitivities
  *
  * The problem defined by the IntegratorMetaData2ndOrder object is integrated and first- and
  * second-order adjoints and Lagrange sensitivities are calculated. The calculated quantities are
  * then stored into the IntegratorMetaData2ndOrder object.
  */
  virtual ResultFlag solve()
  {
    // forward and reverse sweep
    return GenericIntegratorReverse::solve();
  }
};
