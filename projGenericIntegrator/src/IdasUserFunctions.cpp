#include "IdasUserFunctions.hpp"
#include "IntegratorMetaData.hpp"
#include "EsoUtilities.hpp"
#include "ArrayAlgorithm.hpp"
#include "cs.h"


int idas_res(double t, N_Vector yy, N_Vector yd, N_Vector resval, void *user_data)
{
  IntegratorMetaData::Ptr intmd = *((IntegratorMetaData::Ptr*)(user_data));
  intmd->setStepCurrentTime(t);
  utils::WrappingArray<double> y(NV_LENGTH_S(yy), NV_DATA_S(yy));
  utils::WrappingArray<double> rhsStates(NV_LENGTH_S(resval), NV_DATA_S(resval));
  intmd->calculateRhsStates(y, rhsStates);
  utils::WrappingArray<double> ydwrap(NV_LENGTH_S(yd), NV_DATA_S(yd));

  CsCscMatrix::Ptr mass_mat = intmd->getMMatrix();
  cs_gaxpy(mass_mat->get_matrix_ptr(), ydwrap.getData(), rhsStates.getData()); //Mass Matrix M is internally stored with negative sign, so we can add it to rhs to get residual

  return 0;
}

int idas_resS(int Ns, double t,
	      N_Vector yvec, N_Vector ydvec, N_Vector resvalvec,
	      N_Vector *yySvec, N_Vector *ydSvec, N_Vector *resvalSvec,
	      void *user_data,
	      N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{
  IntegratorMetaData::Ptr intmd = *((IntegratorMetaData::Ptr*)(user_data));
  intmd->setStepCurrentTime(t);
  GenericEso::Ptr eso = intmd->getGenericEso();
  int n_states = eso->getNumStates();
  int n_p = intmd->getNumCurrentSensitivityParameters();
  int n_sens = n_states*n_p;

  utils::Array<double> yyS(n_sens);
  int curr_start_idx = 0;
  //std::cout << "*** np=" << n_p << std::endl;
  for (int k=0; k<n_p; ++k) {
    double* yySvec_data = NV_DATA_S(yySvec[k]);
    std::copy(yySvec_data, yySvec_data+n_states, yyS.getData()+curr_start_idx);
    curr_start_idx += n_states;
  }
  utils::Array<double> resvalS(n_sens);
  intmd->calculateSensitivitiesForward1stOrder(yyS, resvalS); // this only calculates dF/dx s + dF/dp
  curr_start_idx = 0;
  for (int k=0; k<n_p; ++k) {
    std::copy(resvalS.getData()+curr_start_idx, resvalS.getData()+curr_start_idx+n_states, NV_DATA_S(resvalSvec[k]));
    curr_start_idx += n_states;
  }
  // we still need to add dF/dx_dot s_dot
  CsCscMatrix::Ptr mass_mat = intmd->getMMatrix();
  //cs* mass_mat_t = cs_transpose(&mass_mat, 1);
  assert(mass_mat->get_n_rows() == mass_mat->get_n_cols());
  assert(mass_mat->get_n_cols() == n_states);
  for (int j=0; j<n_p; ++j) {
    cs_gaxpy(mass_mat->get_matrix_ptr(), NV_DATA_S(ydSvec[j]), NV_DATA_S(resvalSvec[j]));
  }

  return 0;
}  

int ida_dense_jac(realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector rr, SUNMatrix Jac, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3) {
	IntegratorMetaData::Ptr intmd = *((IntegratorMetaData::Ptr*)(user_data));
	intmd->setStepCurrentTime(tt);


	GenericEso::Ptr eso = intmd->getGenericEso();
	int neq = eso->getNumEquations();

	CsCscMatrix::Ptr mmat = intmd->getMMatrix();
	int curr_elem = 0;
	for (int k = 0; k< mmat->get_n_cols(); ++k) {
		while (curr_elem< mmat->get_matrix_ptr()->p[k + 1]) {
			SM_ELEMENT_D(Jac, mmat->get_matrix_ptr()->i[curr_elem], k) = cj * mmat->get_matrix_ptr()->x[curr_elem];
			curr_elem++;
		}
	}

	CsCscMatrix::Ptr jacStates = intmd->getJacobianFstates();
	double tFinal = intmd->getIntegrationGridDuration();
	curr_elem = 0;
	utils::Array<double> tmpdiffcol(neq, 0.0);
	for (int k = 0; k < jacStates->get_n_cols(); ++k) {
		double* colk = SM_COLUMN_D(Jac, k);
		utils::WrappingArray<double> colk_wrap(neq, colk);
		jacStates->extractColumn(k, tmpdiffcol.getData(), neq);
		daxpy(tmpdiffcol, tFinal, colk_wrap);
	}



	return 0;
}



int ida_sparse_jac(realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector rr, SUNMatrix Jac, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3) {
	IntegratorMetaData::Ptr intmd = *((IntegratorMetaData::Ptr*)(user_data));



	intmd->setStepCurrentTime(tt);

	double tFinal = intmd->getIntegrationGridDuration();

	SUNMatZero(Jac);

	sunindextype *colptrs = SUNSparseMatrix_IndexPointers(Jac);
	sunindextype *rowvals = SUNSparseMatrix_IndexValues(Jac);
	realtype *data = SUNSparseMatrix_Data(Jac);

	CsCscMatrix::Ptr jacStates = intmd->getJacobianFstates();
	CsCscMatrix::Ptr mmat = intmd->getMMatrix();

	cs* mat_add = cs_add(jacStates->get_matrix_ptr(), mmat->get_matrix_ptr(), tFinal, 1*cj);

	for (int k = 0; k< mat_add->nzmax; ++k) {
		data[k] = mat_add->x[k];
		rowvals[k] = mat_add->i[k];
	}

	for (int j = 0; j< mat_add->n+1; ++j) {
		colptrs[j] = mat_add->p[j];
	}

	
	delete[] mat_add;

	return 0;
}

int ida_root(double t, N_Vector yy, N_Vector yp, double * gout, void *user_data)
{
  IntegratorMetaData::Ptr intmd = *((IntegratorMetaData::Ptr*)(user_data));
  intmd->setStepCurrentTime(t);
  GenericEso::Ptr eso = intmd->getGenericEso();
  EsoIndex numSFuncs = eso->getNumConditions();
  EsoIndex numStates = eso->getNumStates();
  EsoIndex numDiffVars = eso->getNumDifferentialVariables();
  eso->setStateValues(numStates, NV_DATA_S(yy));
  eso->setDerivativeValues(numDiffVars, NV_DATA_S(yp));
  if(eso->getConditionResiduals(numSFuncs, gout)==GenericEso::OK)
    return 0;
  return 1;
}
