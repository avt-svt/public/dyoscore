/**
* @file Rev2ndOrdIntegratorNixe.cpp
*
* =========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                        \n
* =========================================================================\n
* Rev2ndOrdIntegratorNixe - Part of DyOS                                   \n
* =========================================================================\n
* This file contains the method definitions of the Rev2ndOrdIntegratorNixe \n
* class                                                                    \n
* =========================================================================\n
* @author Moritz Schmitz
* @date 14.06.2012
*/

#include <cassert>
#include "Rev2ndOrdIntegratorNixe.hpp"

/**
* @brief constructor
*
* @param integratorMetaData IntegratorMetaData2ndOrder object describing the model to be solved
* @param order integration mode
* @sa TypeDaeInterface
*/
Rev2ndOrdIntegratorNixe::Rev2ndOrdIntegratorNixe(const IntegratorMetaData2ndOrder::Ptr &integratorMD2ndOrder,
                                                 const ForwardIntegratorNixe::NixeOptions &options)
{
  assert(options.order == secondRevOrder);
  m_options = options;
  m_integratorMD2ndOrder = integratorMD2ndOrder;
  m_metaDae2ndOrder = NIXE_SHARED_PTR<MetaDaeInterface2ndOrder>(new MetaDaeInterface2ndOrder(integratorMD2ndOrder));

  m_integratorMetaData = integratorMD2ndOrder;
  const unsigned numStages = m_integratorMetaData->getNumStages();
  m_metaSSEDae = m_metaDae2ndOrder;

  /*Checkpoints are needed in case of a subsequent reverse integration*/

  m_checkpoints.resize(numStages);
  m_initialValueCheckpoints.resize(numStages);
  m_daeInitialization = DaeInitialization::Ptr(new NoDaeInitialization());
}

Rev2ndOrdIntegratorNixe::Rev2ndOrdIntegratorNixe(const IntegratorMetaData2ndOrder::Ptr &integratorMD2ndOrder,
                                                 const ForwardIntegratorNixe::NixeOptions &options,
                                                 const DaeInitialization::Ptr &daeInit)
{
  assert(options.order == secondRevOrder);
  m_options = options;
  m_integratorMD2ndOrder = integratorMD2ndOrder;
  m_metaDae2ndOrder = NIXE_SHARED_PTR<MetaDaeInterface2ndOrder>(new MetaDaeInterface2ndOrder(integratorMD2ndOrder));

  m_integratorMetaData = integratorMD2ndOrder;
  const unsigned numStages = m_integratorMetaData->getNumStages();
  m_metaSSEDae = m_metaDae2ndOrder;

  /*Checkpoints are needed in case of a subsequent reverse integration*/

  m_checkpoints.resize(numStages);
  m_initialValueCheckpoints.resize(numStages);
  m_daeInitialization = daeInit;
}


/**
* @brief destructor
*/
Rev2ndOrdIntegratorNixe::~Rev2ndOrdIntegratorNixe()
{
}

/**
* @copydoc ReverseIntegratorNixe::getIntegratorMDPtr
*/
IntegratorMetaData::Ptr Rev2ndOrdIntegratorNixe::getIntegratorMDPtr() const
{
  return m_integratorMD2ndOrder;
}

/**
* @copydoc Generic2ndOrderIntegrator::solve
*/
GenericIntegrator::ResultFlag Rev2ndOrdIntegratorNixe::solve()
{
  m_stageIndex = -1;
  m_revStageIndex = m_integratorMetaData->getNumStages();
  return Generic2ndOrderIntegrator::solve();
}

/**
* @copydoc ReverseIntegratorNixe::solveInterval
*/
GenericIntegrator::ResultFlag Rev2ndOrdIntegratorNixe::solveInterval()
{
  return ReverseIntegratorNixe::solveInterval();
}

/**
* @copydoc ReverseIntegratorNixe::initializeStageIntegration
*/
void Rev2ndOrdIntegratorNixe::initializeStageIntegration()
{
  ReverseIntegratorNixe::initializeStageIntegration();
}

/**
* @copydoc ReverseIntegratorNixe::initializeStageIntegrationReverse
*/
void Rev2ndOrdIntegratorNixe::initializeStageIntegrationReverse()
{
  ReverseIntegratorNixe::initializeStageIntegrationReverse();
}

/**
* @copydoc ReverseIntegratorNixe::solveStep
*/
GenericIntegratorForward::SolveStepResult Rev2ndOrdIntegratorNixe::solveStep(double& startTime,
                                                                             const double endTime,
                                                                             utils::Array<double> &finalStates,
                                                                             utils::Array<double> &finalSensitivities)
{
  return ReverseIntegratorNixe::solveStep(startTime, endTime, finalStates, finalSensitivities);
}

/**
* @copydoc ReverseIntegratorNixe::solveStepReverse
*/
void Rev2ndOrdIntegratorNixe::solveStepReverse(double& reverseStartTime,
                                               const double reverseEndTime,
                                               utils::Array<double> &adjoints,
                                               utils::Array<double> &lagrangeDerivatives)
{
  return ReverseIntegratorNixe::solveStepReverse(reverseStartTime, reverseEndTime, adjoints, lagrangeDerivatives);
}

/**
* @copydoc ReverseIntegratorNixe::evaluateAtInitialPointReverse
*/
void Rev2ndOrdIntegratorNixe::evaluateAtInitialPointReverse(
                                                 utils::Array<double> &adjoints,
                                                 utils::Array<double> &lagrangeDerivatives)
{
  return ReverseIntegratorNixe::evaluateAtInitialPointReverse(adjoints, lagrangeDerivatives);
}


/**
* @copydoc GenericIntegratorReverse::getNumAdjoints
*/
unsigned Rev2ndOrdIntegratorNixe::getNumAdjoints() const
{
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = m_metaDae2ndOrder->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numAdjointVectors = problemInfo->NumAdjoints();
  return (unsigned)numStates * (unsigned)numAdjointVectors;
}

/**
* @copydoc GenericIntegratorReverse::getNumLagrangeDers
*/
unsigned Rev2ndOrdIntegratorNixe::getNumLagrangeDers() const
{
   return (unsigned)m_metaDae2ndOrder->GetProblemInfo()->NumDers();
}

/**
* @copydoc ForwardIntegratorNixe::storeSensInformation
*/
void Rev2ndOrdIntegratorNixe::storeSensInformation(utils::Array<double> &finalSensitivities) const
{
  ForwardIntegratorNixe::storeSensInformation(finalSensitivities);
}

DaeInitialization::Ptr Rev2ndOrdIntegratorNixe::getDaeInitializationPtr() const
{
  return ReverseIntegratorNixe::getDaeInitializationPtr();
}
