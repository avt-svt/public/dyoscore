/** @file LimexLoader.cpp
*    @brief definitions of member functions of LimexLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails LimexLoader
*    =====================================================================\n
*   @author Stefan Jaeger, Klaus Stockmann, Adrian Caspari
*   @date 17.10.2018
*/


#include "IntegratorLimex.hpp"
//only temporary - should be put to the factory
#include "BlockDaeInitialization.hpp"
#include "Ma28Solver.hpp"
//#include "KLUSolver.hpp"
#include "Nleq1sSolver.hpp"

IntegratorMetaData::Ptr IntegratorLimex::m_staticIntMD;

/**
* @brief constructor
*
* @param[in] integratorMetaData IntegratorMetaData object describing the model to be solved
* @param[in] initTol struct containing tolerance values for relTol and absTol of m_options
*/
IntegratorLimex::IntegratorLimex(const IntegratorMetaData::Ptr &integratorMetaData,
                                 const InitialTolerance &initTol,
                                 const bool calculateSensitivities):
m_calculateSensitivities(calculateSensitivities),
m_numModelEqns(integratorMetaData->getGenericEso()->getNumEquations()),
m_numParams(integratorMetaData->getNumSensitivityParameters()),
m_numAllSens(m_numModelEqns * m_numParams),
m_hStepIn(0.0), m_hStepOut(0.0),
m_y(m_numModelEqns + m_numAllSens, 0.0),
m_ys(m_numModelEqns + m_numAllSens, 0.0),
m_integratorMetaData(integratorMetaData),
m_initialTolerance(initTol), m_isFirstIntegrationCall(true)
{
  LinearSolver::Ptr linSolver(new Ma28Solver());
  //LinearSolver::Ptr linSolver(new KLUSolver());
  NonLinearSolver::Ptr nonLinSolver(new Nleq1sSolver());
  m_daeInitialization = DaeInitialization::Ptr(new BlockDaeInitialization(linSolver, nonLinSolver, 1e-10));
}

IntegratorLimex::IntegratorLimex(const IntegratorMetaData::Ptr &integratorMetaData,
                                 const InitialTolerance &initTol,
                                 const bool calculateSensitivities,
                                 const DaeInitialization::Ptr &daeInit):
m_calculateSensitivities(calculateSensitivities),
m_numModelEqns(integratorMetaData->getGenericEso()->getNumEquations()),
m_numParams(integratorMetaData->getNumSensitivityParameters()),
m_numAllSens(m_numModelEqns * m_numParams),
m_hStepIn(0.0), m_hStepOut(0.0),
m_y(m_numModelEqns + m_numAllSens, 0.0),
m_ys(m_numModelEqns + m_numAllSens, 0.0),
m_integratorMetaData(integratorMetaData),
m_initialTolerance(initTol), m_isFirstIntegrationCall(true)
{
  m_daeInitialization = daeInit;
}




/**
* @brief destructor
*/
IntegratorLimex::~IntegratorLimex()
{
}

void IntegratorLimex::printErrorMessage(const int limexRetval)
{
  switch(limexRetval){
    case -1:
    {
      std::string message = "Max_Nr_of_Equations < n or n < 1."
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -2:
    {
      std::string message = "rTol(i) <= 0 for some index i"
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -3:
    {
      std::string message = "aTol(i) < 0 for some index i"
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -4:
    case -5:
    case -6:
    case -7:
    case -8:
    case -9:
    case -10:
    case -11:
    case -12:
    case -13:
    case -14:
    case -15:
    case -16:
    case -17:
    case -18:
    case -19:
    case -20:
    case -21:
    case -22:
    case -23:
    case -24:
    case -25:
    case -26:
    {
      std::string message = "Iopt(i) has an illegal value for some index i"
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -27:
    case -28:
    case -29:
    case -30:
    {
      std::string message = "Ropt(i) has an illegal value for some index i"
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -31:
    {
      std::string message = "Not enough memory available in GMRES.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -32:
    {
      std::string message = "An initial value of y is less than zero,"
                            " but by the setting of IPos it should be >= 0."
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -33:
    {
      std::string message = "The routine Fcn returns with an error (FcnInfo <0)"
                            " in the first call of Fcn in the current integration interval";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -34:
    {
      std::string message = "More than Max_Non_Zeroes_B nonzero entries in matrix B(t,y) defined"
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -35:
    {
      std::string message = "The routine Fcn returns with an error (FcnInfo < 0)"
                            " in the numerical evaluation of the Jacobian.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -36:
    {
      std::string message = "The routine Jacobian returns with an error (JacInfo < 0).\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -37:
    {
      std::string message = "The Jacobian has more non-zero entries than allowed by Max_Non_Zeros_Jacobian."
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -38:
    {
      std::string message = "The number of non-zero entries in matrix B changes during one integrations step."
                            " This error apparently occurs because of a bug in the IntegratorLimex class.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -39:
    {
      std::string message = "Internal error in the MA28 routine MA28AD, see IFail(2) and the explanations in"
                            " the source of the routine MA30BD.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -40:
    {
      std::string message = "Internal error in the MA28 routine MA28BD, see IFail(2) and the explanations in"
                            " the source of the routine MA30BD.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -41:
    {
      std::string message = "Internal error in the routine Precon, see IFail(2)"
                            " and the documentation in Precon.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -42:
    {
      std::string message = "Too many iterations within th solution of a linear system by"
                            "GMRES or BICGSTAB. This error can occur in the computation"
                            "of CIVs only, since otherwise a stepsize reduction will be tried.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -43:
    {
      std::string message = "Problem not solvable with this LIMEX verstion, most probable reason:"
                            " index of the DAE > 1.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -44:
    {
      std::string message = "Problem not solvable with this LIMEX version, most probable reason:"
                            " initial values not consistent or index of the DAE > 1.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -45:
    {
      std::string message = "A value of the solution vector y within the CIV computation is negative,"
                            " but should be >= 0 by the settings of IPos.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -46:
    {
      std::string message = "More stepsize reductions than allowed due to failing convergence"
                            " in the extrapolation.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -47:
    {
      std::string message = "Singular matrix pencil B - hA, problem not solvable with LIMEX.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -48:
    {
      std::string message = "More integration steps than allowed.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -49:
    {
      std::string message = "More internal Newton steps for the computation of consistent initial"
                            "values than allowed.\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    case -50:
    {
      std::string message = "Stepsize too small, in most cases due to too many stepsize reductions\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
      break;
    }
    default:
    {
      std::string message = "unknown limex error code\n";
      m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::ERROR_VERBOSITY, message);
    }
  }
}

/**
* @copydoc GenericIntegratorForward::getIntegratorMDPtr
*/
IntegratorMetaData::Ptr IntegratorLimex::getIntegratorMDPtr() const
{
  return m_integratorMetaData;
}

DaeInitialization::Ptr IntegratorLimex::getDaeInitializationPtr() const
{
  return m_daeInitialization;
}

GenericIntegrator::ResultFlag IntegratorLimex::solveInterval()
{
  // initial stepsize
  m_hStepIn = 0.0;
  m_hStepOut = 0.0;
  return GenericIntegratorForward::solveInterval();
}



/**
* @copydoc GenericIntegratorForward::initializeStageIntegration
*/
void IntegratorLimex::initializeStageIntegration()
{
	m_isFirstIntegrationCall = true;
	
}


/**
* @brief solves the model given in m_integratorMetaData
*/
GenericIntegratorForward::SolveStepResult IntegratorLimex::solveStep(double& startTime,
                                                                     const double endTime,
                                                                     utils::Array<double> &finalStates,
                                                                     utils::Array<double> &finalSensitivities)
{
  m_staticIntMD = this->m_integratorMetaData;


  // set options
  LimexLoader::LimexOptions options(m_numModelEqns, m_numModelEqns + m_numAllSens);
  initOptions(options);


  setIntegratorTolerance(m_initialTolerance.absoluteTolerance,
                         m_initialTolerance.relativeTolerance,
                         options);

  /*
  if (m_isFirstIntegrationCall) {

	  initializeEso(this->m_integratorMetaData->getGenericEso());
	  m_isFirstIntegrationCall = false;
  }
  */

  
  initializeEso(this->m_integratorMetaData->getGenericEso());



  applyInitialValues(m_y, m_ys);
  m_hStepIn = m_hStepOut;

  int nActiveParams = m_integratorMetaData->getNumCurrentSensitivityParameters();
  // compute consistent initial values here.
  int limexRetval; // limex return value, see description there
  int n = m_numModelEqns;
  if(m_calculateSensitivities){
    n += m_numModelEqns*nActiveParams;
  }
  limexRetval = m_slimexs.slimexs(m_numModelEqns,
                                  n,
                                  fcn,
                                  jacobian,
                                  startTime,
                                  endTime,
                                  m_y,
                                  m_ys,
                                  m_hStepIn,
                                  options);
  if(options.Ropt[6] != 0)
    m_hStepOut = options.Ropt[6];

  m_hStepIn = options.Ropt[5];
  // set states
  assert((unsigned)m_numModelEqns == finalStates.getSize());
  //dcopy_(m_y.getData(), 1, finalStates.getData(), 1);
  for(int i=0; i<m_numModelEqns; i++){
    finalStates[i] = m_y[i];
  }
  if(nActiveParams > 0){
    assert(finalSensitivities.getSize() == (unsigned)m_numModelEqns*nActiveParams);
    for(unsigned i=0; i<finalSensitivities.getSize(); i++){
      //m_ys contains the differential variables and is only used as input
      //all relevant data for the output is stored in m_y
      finalSensitivities[i] = m_y[m_numModelEqns + i];
    }
  }
  GenericIntegratorForward::SolveStepResult retval;
  if (limexRetval < 0) {
    printErrorMessage(limexRetval);
    retval = GenericIntegratorForward::SolveStepFailed;
  } else {
    retval = GenericIntegratorForward::SolveStepSuccess;
  }
  return retval;
}

/**
* @brief compute residual and M-matrix in M * x' = F(x, t)
*
* This function includes the state and sensitivity equations of
* the DAE - System B * x' = F(x, t)
* here we compute B (in coordinate format) and the residual res = F(x, t)
* @param[in] n size of DAE system, including sensitivities
* @param[out] nz number of nonzero entries in M-matrix
* @param[in] t current value of independent variable (t)
* @param[in] y current value of solution vector (dimension: n)
* @param[in] rpar real parameters
* @param[in] ipar integer parameters (not in use)
* @param[out] f current value of residual F(U, Time), dimension: n
* @param[out] M nonzero values of M-matrix
* @param[out] ir row indices of M-matrix
* @param[out] ic column indices of M-matrix
* @param[in, out] Info
*                 on input: indicates type of call to fcn:\n
*                 Info = 0: first call of fcn in curr. integration step\n
*                 Info = 1: call to fcn within extrapolation process\n
*                 Info = 2: call to fcn within the numerical evaluation\n
*                 of the jacobian\n
*                 Info = 3: a call to fcn, in which AllBlocksConv is true\n
*                               (see l. 3992 of limex source)\n
*                 on output: err. flag, if no error occurs set Info=0
* @param[in] BlockConv logical vector (impl. as int), dimension: 'Max_Param' (set via size_definitions.h)
*/

void IntegratorLimex::fcn(const int &n, int &nz, const double &t, const double *y, const double *rpar,
                          const int *ipar, double *f, double *M, int *ir, int *ic,
                          int &Info, const int *BlockConv)
{
  m_staticIntMD->setStepCurrentTime(t);
  CsTripletMatrix::Ptr massMatrix = m_staticIntMD->getMMatrixCoo();

  for (int i=0; i< n; i++){
    f[i] = 0.0; // the rest is set to zero
  }
  nz = massMatrix->get_matrix_ptr()->nzmax;      //maximum number of entries
  for (int j=0; j<nz; j++) {
    M[j] = - massMatrix->get_matrix_ptr()->x[j];
    ic[j] = massMatrix->get_matrix_ptr()->p[j]+1;       //offset 1 c2fortran
    ir[j] = massMatrix->get_matrix_ptr()->i[j]+1;       //offset 1 c2fortran
  }

  const int numVars = m_staticIntMD->getGenericEso()->getNumVariables();
  const int numAssigned = m_staticIntMD->getGenericEso()->getNumParameters();
  const int numIntVars = numVars - numAssigned;   //number of integration variables

  //evaluate the right hand side of the system Mx' = tf*F(x,y,u,p)
  utils::Array<double> inputStates(numIntVars); //input-states for RHS calculation
  for (int i = 0; i<numIntVars; i++) {
    inputStates[i] = y[i];
  }
  utils::WrappingArray<double> rhsStates(numIntVars, f);   //results of RHS calculation
  m_staticIntMD->calculateRhsStates(inputStates, rhsStates);

  /*for (int i = 0; i<numIntVars; i++) {
    f[i] = rhsStates[i];
  }*/

  //get the right hand side of the 1st order forward sensitivities
  if (n > numIntVars) {
    const int numParams = m_staticIntMD->getNumCurrentSensitivityParameters();             //number of input sensitivities
    const int numSens = numParams * numIntVars;
    utils::Array<double> inputSens(numSens);  //input-sensitivities of RHS calculation
    for (int i = 0; i<numSens; i++) {
      inputSens[i] = y[i + numIntVars];
    }

    utils::WrappingArray<double> rhsSens(numSens, f + numIntVars);    //results of RHS sensitivity calculation
    m_staticIntMD->calculateSensitivitiesForward1stOrder(inputSens, rhsSens);

    /*for (int i = 0; i<numSens; i++) {
      f[i+numIntVars] = rhsSens[i];
    }*/
  }
}

/**
  * @brief This function calculates the Jacobian entries for the state equations
           of the DAE - System
  * @param[in] n size of DAE system
  * @param[in] t current value of independent variable
  * @param[in] *y current value of solution, dimension: n
  * @param[in] *ys current value of sensitivities, dimension: n
  * @param[in] *rpar parameters
  * @param[in] *ipar integer parameters (currently not in use)
  * @param[out] *Jac nonzero entries of Jacobian
  * @param[out] *ir row indices of nonzero Jacobian entries
  * @param[out] *ic column indices of nonzero Jacobian entries
  * @param[out] LenJac number of nonzero entries in Jacobian
  * @param[in, out] JacInfo; on output: error indicator, if an error occured set\n
  *                          JacInfo=-1, else =0\n
  *                          on input: \n
  *                          if JacInfo == 0 fcn has been called before call of jacobian\n
  *                          if JacInfo == 1 call to jacobian has not been preceded by call to fcn
  */

void IntegratorLimex::jacobian(const int &n, const double &t, const double *y, const double *ys,
                               const double *rpar, const int *ipar,
                               double *Jac, int *ir, int *ic, int &LenJac, int &JacInfo)
{
  m_staticIntMD->setStepCurrentTime(t);
  CsCscMatrix::Ptr JCsc = m_staticIntMD->getJacobianFstates();
  assert(JCsc->get_matrix_ptr()->nz == -1);
  CsTripletMatrix::Ptr jTrip(new CsTripletMatrix(JCsc->get_matrix_ptr(), false));

  const double TFinal = m_staticIntMD->getIntegrationGridDuration();
  LenJac = jTrip->get_nnz();//number of nonzeroes
  for (int j=0; j<LenJac; j++) {
    Jac[j] = TFinal*jTrip->get_matrix_ptr()->x[j];
    ic[j] = jTrip->get_matrix_ptr()->p[j]+1;         //offset 1 c2fortran
    ir[j] = jTrip->get_matrix_ptr()->i[j]+1;         //offset 1 c2fortran
  }
}

/**
* @initialization of options for integration
*/
void IntegratorLimex::initOptions(LimexLoader::LimexOptions &options)
{
  //-----------------------------------------------------------------------

  //     Control parameters

  //     Iopt[1-1]  Integration monitoring
  //              = 0 : no output
  //              = 1 : standard output
  //              = 2 : additional integration monitor

  //     Iopt[2-1]  Unit number for monitor output

  //     Iopt[3-1]  Solution output
  //              = 0 : no output
  //              = 1 : initial and solution values
  //              = 2 : additional solution values at intermediate
  //                    points

  //     Iopt[4-1]  Unit number for solution output

  //     Iopt[5-1]  Singular on nonsingular matrix B
  //              = 0 : matrix B may be singular
  //              = 1 : matrix B is nonsingular

  //     Iopt[6-1]  Consistent initial value determination
  //              = 0 : no determination of CIVs
  //              = 1 : determination of CIVs

  //     Iopt[7-1]  Numerical or analytical computation of the Jacobian
  //              = 0 : Numerical approximation of the Jacobian
  //              = 1 : Analytical computation of the Jacobian

  //     Iopt[8-1]  Lower bandwith of the Jacobian if numerical
  //              evaluated

  //     Iopt[9-1]  Upper bandwith of the Jacobian if numerical
  //              evaluated

  //     Iopt[10-1] Control of reuse of the Jacobian
  //              = 0 : no Jacobian reuse
  //              = 1 : reuse of the Jacobian

  //     Iopt[11-1] Switch for error tolerances
  //              = 0 : both rTol and aTol are scalars
  //              = 1 : both rTol and aTol are vectors

  //     Iopt[12-1] Sw1tch for one step mode
  //              = 0 : one step mode off
  //              = 1 : one step mode on
  //              = 2 : one step mode on, return on dense output
  //                    points

  //     Iopt[13-1] Dense output option
  //              = 0 : no dense output
  //              = 1 : dense output on equidistant points in the
  //                    whole integration interval
  //              = 2 : dense output on equidistant points in
  //                    every integration interval
  //              = 3 : dense output at additional points, that
  //                    the distance between two dense output
  //                    points is at least Ropt[2-1]

  //     Iopt[14-1] The number of equidistant output points if
  //              Iopt[13-1] = 1 or 2

  //     Iopt[15-1] Unit number for dense solution output

  //     Iopt[16-1] Type of call
  //              = 0 : first LIMEX call
  //              = 1 : continuation call

  //     Iopt[17-1] Switch for behavior of LIMEX on t_End
  //              = 0 : integrate exactly up to t_End
  //              = 1 : LIMEX may internally integrate to t > t_End

  //     Iopt[18-1] Generation of of a PostScript plot of the Jacobian
  //              = 0 : no PostScript plot produced
  //              - 1 : in the initial step the file 'Jacobian.ps'
  //                    will be produced with the initial Jacobian
  //              > 0 : in step Iopt[18-1] the file 'Jacobian.ps'
  //                    will be produced with the current Jacobian

  //     Iopt[19-1] Dynamic sparsing of the Jacobian
  //              = 0 : no sparsing
  //              = 1 : neglecting entries in the Jacobian with absolute
  //                    scaled values less than Sigma / Stepsize

  //     Iopt[20-1] Switch for direct or iterative solution of the linear
  //              systems
  //              = 0 : direct solution with MA28 routines
  //              = 1 : iterative solution with GMRES
  //              = 2 : iterative solution with BICGSTAB


  //     Iopt[21-1] Fill-in parameter for the variable ILU-preconditioner

  //     Iopt[22-1] The maximal number of iterations permitted in GMRES
  //              or BICGSTAB

  //     Iopt[23-1] The maximal dimension of the Krylov subspace in GMRES

//       Iopt(24-1) On return the number of function evaluations
//                during the integration without those for the
//                computation of the Jacobian.
//
      //Iopt(25-1) On return the number of function evaluations
      //        needed for the  numerical computation of the
      //        Jacobian.

      //Iopt(26-1) On  return the  number of sparse LU decompo-
      //        sitions by the routines MA28AD and MA28BD if
      //        Iopt(20) = 0 (direct solvers) or  number  of
      //        preconditioner  setups (ILU  decompositions)
      //        if Iopt(20) > 0 (iterative solvers).

      //Iopt(27-1) On return the number of backsubstitutions by
      //        the routine  MA28CD  if Iopt(20) = 0 (direct
      //        solvers) or number of calls  of an iterative
      //        solver if Iopt(20) > 0 (iterative solvers).

      //Iopt(28-1) On return the number of integration steps.

      //Iopt(29-1) On  return  the  number  of Jacobian  matrix
      //        evaluations.

      //Iopt(30-1) On  return  the whole  number of  iterations
      //        used by GMRES or BICGSTAB.


  options.Iopt[5-1] = 1;
  options.Iopt[7-1] = 1;
  int nActiveParams = 0;
  if (m_calculateSensitivities)
    nActiveParams = m_integratorMetaData->getNumCurrentSensitivityParameters();
  options.Iopt[8-1] = m_numModelEqns*(1+nActiveParams);
  options.Iopt[9-1] = m_numModelEqns*(1+nActiveParams);
  options.Iopt[21-1] = 2;


//IoptS       is a integer vector of size 10 containing additional
//           Info flags for the sensitivity analysis which are not
//           available in the standart version of LIMEX (cf.
//           Chapter 3 of Martin's thesis)
//
//           IoptS(1-1)  1 => no sens. in the error control
//
//           IoptS(2-1)  1 => B matrix is constant
//
//           IoptS(3-1)  1 => don't check for nonpositive components
//
//           IoptS(4-1)  1 => don't calculate yprime
//
//           IoptS(5-1)   This flag is used to switch on the monitor of the
//                      sensitivity accuracy, as described in Martin's thesis
//                      in Section 3.51.
//                      0 is default and means that this option is switched off
//                      1 means that all sensitivities are checked for convergence.
//                        If all are converged, they are excluded from further
//                        extrapolation.
//                      2 means that only significant sensitivities are checked for
//                        convergence, which are defined by setting ConvText[i] = 1
//
//           IoptS(6-1)   This option is the switch for reusing the LU decomposition, as
//                      described in Martin's thesis in Section 3.5.2.
//                      0 is default and means that standard LIMEX is used
//                      1 takes care that MA28BD is used instead of MA28AD where possible.
//                        This option might be faster, but only for large-scale problems
//                        where the computational effort for MA28AD is very significant.
//                        It might lead to stability problems in some cases.
//
//           IoptS(7-1)   currently not used, set equal to 0
//
//           IoptS(8-1)   currently not used, set equal to 0
//
//           IoptS(9-1)   currently not used, set equal to 0
//
//           IoptS(10-1)  currently not used, set equal to 0

  options.Iopts[1-1] = 1;
  options.Iopts[2-1] = 1;
  options.Iopts[3-1] = 1;
  options.Iopts[4-1] = 0;
  options.Iopts[5-1] = 1;
  options.Iopts[6-1] = 0;

  //     Ropt[1-1]  Maximum allowed stepsize

  //     Ropt[2-1]  Maximum distance between two dense output points,
  //              if Iopt[13-1] = 3

  //     Ropt[3-1]  Upper bound for t, used only if Iopt[17-1] = 1

  //     Ropt[4-1]  Sigma-value for dynamisparsing of the Jacobian.

  //     Ropt[5-1]  Threshold value for neglecting small terms during the
  //              computation of the variable ILU preconditioner.

  options.Ropt[4-1] = 1e-7;
}

/** @brief evaluate initial values from meta data and set the states of the integrator
  * @param[in,out] integrator states
  * @param[in,out] integrator derivatives
  */
void IntegratorLimex::applyInitialValues(utils::Array<double> &y,
                                         utils::Array<double> &ys)
{
  // get the number of active parameters from the eso
  const int nActiveParams = m_integratorMetaData->getNumCurrentSensitivityParameters();
  utils::Array<double> states(m_numModelEqns), sensitivities(m_numModelEqns * nActiveParams);
  // evaluate the initial values from meta data.
  m_integratorMetaData->evaluateInitialValues(states, sensitivities);

  // override current values
  for(int i=0;i<m_numModelEqns; i++){
    y[i] = states[i];
  }
  for(int i=0;i< m_numModelEqns*nActiveParams; i++){
    y[i+m_numModelEqns] = sensitivities[i];
  }

  // get derivative values from ESO and put them into correct location in ys
  const int numDiffVars = m_integratorMetaData->getGenericEso()->getNumDifferentialVariables();
  utils::Array<double> derivativeValues(numDiffVars);
  utils::Array<int> differentialIndices(numDiffVars);
  m_integratorMetaData->getGenericEso()->getDerivativeValues(numDiffVars, derivativeValues.getData());
  m_integratorMetaData->getGenericEso()->getDifferentialIndex(numDiffVars, differentialIndices.getData());

  for (int i=0; i<numDiffVars; i++) {
    int index = m_integratorMetaData->getGenericEso()->getStateIndexOfVariable(differentialIndices[i]);
    ys[index] = derivativeValues[i] * m_integratorMetaData->getIntegrationGridDuration();
  }

  //m_integratorMetaData->getGenericEso()->setDerivativeValues(m_numModelEqns, ys.getData());
}
/** @brief sets the integrator tolerance
  * @param[in] absTol the absolute state tolerance
  * @param[out] relTol the relative state tolerance
  */
void IntegratorLimex::setIntegratorTolerance(const double absTol, const double relTol,
                                             LimexLoader::LimexOptions &options)
{
  const unsigned numTol = options.aTol.getSize();
  assert(numTol == options.rTol.getSize());
  for(unsigned i=0; i<numTol; i++){
    options.aTol[i] = absTol;
    options.rTol[i] = relTol;
  }
}
