/**
* @file IntegratorFactory.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericIntegrator                                                    \n
* =====================================================================\n
* This file contains the definition of the factory methods for         \n
* GenericIntegrator                                                    \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.02.2012
*/

#include <cassert>

#include "IntegratorFactory.hpp"
#ifdef HAVE_LIMEX
#include "IntegratorLimex.hpp"
#endif
#include "ForwardIntegratorNixe.hpp"
#include "ReverseIntegratorNixe.hpp"
#include "Rev2ndOrdIntegratorNixe.hpp"
#include "DaeInitializationFactories.hpp"
#include "IntegratorIdas.hpp"
#include "InputExceptions.hpp"
#include <sstream>

/**
* @class IntegratorException
* @brief exception class for any exception of the Integrator module
* @note hotfix for Linux error
*/
class IntegratorException : public std::exception
{
protected:
  const std::string m_errorMessage;
public:
  /// @brief standard constructor setting a standard error message
  IntegratorException()
    : m_errorMessage("")
  {}
  
  /**
  * @brief constructor
  * @param[in] message string containing a specific error message
  */ 
  IntegratorException(const std::string &message)
    : m_errorMessage(message)
  {
  }

  /// @brief destructor
  virtual ~IntegratorException() throw() {}

  /**
  * @brief get error message
  * @return error message of the exception
  */
  const char *what() const throw()
  {
    return m_errorMessage.c_str();
  }

};


ForwardIntegratorNixe::NixeOptions createNixeOptions(
                                const std::map<std::string, std::string> &integratorOptions);
#ifdef HAVE_LIMEX
IntegratorLimex::InitialTolerance createLimexInitTol(
                                const std::map<std::string, std::string> &integratorOptions);
#endif

#ifdef HAVE_IDAS
ForwardIntegratorIdas::IdasOptions createIdasOptions(const std::map<std::string, std::string> &integratorOptions);
#endif

double getDoubleEntry(const std::map<std::string, std::string> &integratorOptions,
                      std::string &key);


/**
* @brief create a GenericIntegrator object using the standard IntegratorMetaDataFactory
         for creating a IntegratorMetaData object
*
* @param input IntegratorInput struct containing all information needed
*              to construct a GenericIntegrator object
* @return pointer to the created GenericIntegrator object
*/
GenericIntegrator::Ptr IntegratorFactory::createGenericIntegrator(
                                      const FactoryInput::IntegratorInput &input)
{
  IntegratorMetaDataFactory imdFactory;
  imdFactory.setLogger(m_logger);
  return createGenericIntegrator(input, imdFactory);
}

/**
* @brief create a GenericIntegrator object using a given IntegratorMetaDataFactory
         for creating a IntegratorMetaData object
*
* @param input IntegratorInput struct containing all information needed
*              to construct a GenericIntegrator object
* @param imdFactory factory used to create a IntegratorMetaData object
* @return pointer to the created GenericIntegrator object
*/
GenericIntegrator::Ptr IntegratorFactory::createGenericIntegrator(
                                      const FactoryInput::IntegratorInput &input,
                                            IntegratorMetaDataFactory &imdFactory)
{
  GenericIntegrator::Ptr integrator;
  if(input.order == FactoryInput::IntegratorInput::SECOND_REVERSE){
    IntegratorMetaData2ndOrder::Ptr imd2ndPtr;
    imd2ndPtr = imdFactory.createIntegratorMetaData2ndOrder(input.metaDataInput);
    IntegratorMetaData2ndOrder::Ptr imd2nd(imd2ndPtr);
    integrator = createGeneric2ndOrderIntegrator(input, imd2nd);
  }
  else{
    IntegratorMetaData::Ptr integratorMetaData(imdFactory.createIntegratorMetaData(input.metaDataInput));
    integrator = createGenericIntegrator(input, integratorMetaData);
  }
  return integrator;
}

/**
* @brief create a GenericIntegrator object
*
* @param input IntegratorInput struct containing all information needed
*              to construct a GenericIntegrator object
* @param integratorMetaData IntegratorMetaData object used to create the integrator
* @return pointer to the created GenericIntegrator object
*/
GenericIntegrator::Ptr IntegratorFactory::createGenericIntegrator(
                                      const FactoryInput::IntegratorInput &input,
                                      const IntegratorMetaData::Ptr &integratorMetaData)
{

  GenericIntegrator::Ptr integrator;
  DaeInitializationFactory daeFac;
  DaeInitialization::Ptr daeInit(daeFac.createDaeInitialization(input.daeInitInput));

  switch(input.type){
    case FactoryInput::IntegratorInput::NIXE:

      switch(input.order){
        case FactoryInput::IntegratorInput::ZEROTH:
          integrator = createNixeForward(integratorMetaData, zerothOrder, daeInit,
                                         input.integratorOptions);
          break;
        case FactoryInput::IntegratorInput::FIRST_FORWARD:
          integrator = createNixeForward(integratorMetaData, firstForwOrder, daeInit,
                                         input.integratorOptions);
          break;
        case FactoryInput::IntegratorInput::FIRST_REVERSE:
          integrator = createNixeReverse(integratorMetaData, firstRevOrder, daeInit,
                                         input.integratorOptions);
          break;
        default:
          assert(false);
      }
      break;
#ifdef HAVE_LIMEX
    case FactoryInput::IntegratorInput::LIMEX:
      assert(input.order != FactoryInput::IntegratorInput::FIRST_REVERSE);
      integrator = createLimex(integratorMetaData, input.order, daeInit, input.integratorOptions);
      break;
#endif
    case FactoryInput::IntegratorInput::IDAS:
      integrator = createIdas(integratorMetaData, input.order, daeInit, input.integratorOptions);
      break;
    default:
      assert(false);
  }
  return integrator;
}

/**
* @brief create a GenericIntegrator object
*
* @param input IntegratorInput struct containing all information needed
*              to construct a GenericIntegrator object
* @param integratorMetaData IntegratorMetaData object used to create the integrator
* @return pointer to the created GenericIntegrator object
*/
Generic2ndOrderIntegrator::Ptr IntegratorFactory::createGeneric2ndOrderIntegrator(
                                     const FactoryInput::IntegratorInput &input,
                                     const IntegratorMetaData2ndOrder::Ptr &integratorMetaData)
{
  assert(input.type == FactoryInput::IntegratorInput::NIXE);
  assert(input.order == FactoryInput::IntegratorInput::SECOND_REVERSE);


  DaeInitializationFactory daeFac;
  DaeInitialization::Ptr daeInit(daeFac.createDaeInitialization(input.daeInitInput));
  return createNixe2ndOrder(integratorMetaData, secondRevOrder, daeInit, input.integratorOptions);
}

/**
* @brief create a ForwardIntegratorNixe object
*
* @param integratorMetaData IntegratorMetaData object used to create the integrator
* @param order integration order with which the integrator is initialized
* @return pointer to the created ForwardIntegratorNixe object
*/
GenericIntegratorForward::Ptr IntegratorFactory::createNixeForward(
                                     const IntegratorMetaData::Ptr &integratorMetaData,
                                     const TypeDaeInterface order,
                                     const DaeInitialization::Ptr &daeInit,
                                     const std::map<std::string, std::string> &integratorOptions)
{
  ForwardIntegratorNixe::NixeOptions options = createNixeOptions(integratorOptions);
  options.order = order;
  GenericIntegratorForward::Ptr nixeForw (new ForwardIntegratorNixe(integratorMetaData,
                                                                 options, daeInit));
  nixeForw->setLogger(m_logger);
  return nixeForw;
}

/**
* @brief create a ReverseIntegratorNixe object
*
* @param integratorMetaData IntegratorMetaData object used to create the integrator
* @param order integration order with which the integrator is initialized
* @return pointer to the created ReverseIntegratorNixe object
*/
GenericIntegratorReverse::Ptr IntegratorFactory::createNixeReverse(
                                     const IntegratorMetaData::Ptr &integratorMetaData,
                                     const TypeDaeInterface order,
                                     const DaeInitialization::Ptr &daeInit,
                                     const std::map<std::string, std::string> &integratorOptions)
{
  ForwardIntegratorNixe::NixeOptions options = createNixeOptions(integratorOptions);
  options.order = order;
  GenericIntegratorReverse::Ptr nixeRev (new ReverseIntegratorNixe(integratorMetaData,
                                                                 options, daeInit));
  nixeRev->setLogger(m_logger);
  return nixeRev;
}

/**
* @brief create a ReverseIntegratorNixe object
*
* @param integratorMetaData IntegratorMetaData object used to create the integrator
* @param order integration order with which the integrator is initialized
* @return pointer to the created ReverseIntegratorNixe object
*/
Generic2ndOrderIntegrator::Ptr IntegratorFactory::createNixe2ndOrder(
                                     const IntegratorMetaData2ndOrder::Ptr &integratorMetaData,
                                     const  TypeDaeInterface order,
                                     const DaeInitialization::Ptr &daeInit,
                                     const std::map<std::string, std::string> &integratorOptions)
{
  ForwardIntegratorNixe::NixeOptions options = createNixeOptions(integratorOptions);
  options.order = order;
  Generic2ndOrderIntegrator::Ptr nixe2nd (new Rev2ndOrdIntegratorNixe(integratorMetaData,
                                                                   options, daeInit));
  nixe2nd->setLogger(m_logger);
  return nixe2nd;
}

/**
* @brief create an IntegratorLimex object
*
* @param integratorMetaData IntegratorMetaData object used to create the integrator
* @return pointer to the created IntegratorLimex object
*/
#ifdef HAVE_LIMEX
GenericIntegratorForward::Ptr IntegratorFactory::createLimex(
                                     const IntegratorMetaData::Ptr &integratorMetaData,
                                     const FactoryInput::IntegratorInput::IntegrationOrder order,
                                     const DaeInitialization::Ptr &daeInit,
                                     const std::map<std::string, std::string> &integratorOptions)
{
  IntegratorLimex::InitialTolerance initTol = createLimexInitTol(integratorOptions);
  bool calculateSensitivities = true;
  if(order == FactoryInput::IntegratorInput::ZEROTH){
    calculateSensitivities = false;
  }
  GenericIntegratorForward::Ptr limex (new IntegratorLimex(integratorMetaData,initTol,
                                                        calculateSensitivities, daeInit));
  limex->setLogger(m_logger);
  return limex;
}
#endif

GenericIntegratorForward::Ptr IntegratorFactory::createIdas(
                                     const IntegratorMetaData::Ptr &integratorMetaData,
										const FactoryInput::IntegratorInput::IntegrationOrder order,
                                     const DaeInitialization::Ptr &daeInit,
                                     const std::map<std::string, std::string> &integratorOptions)
{
#ifdef HAVE_IDAS
	bool calculateSensitivities = true;
	if (order == FactoryInput::IntegratorInput::ZEROTH) {
		calculateSensitivities = false;
	}
	ForwardIntegratorIdas::IdasOptions idasOptions = createIdasOptions(integratorOptions);
  GenericIntegratorForward::Ptr idas (new ForwardIntegratorIdas(integratorMetaData, calculateSensitivities, daeInit, idasOptions));
  idas->setLogger(m_logger);
  return idas;
#else
  throw IntegratorException("IDAS not implemented!");
#endif
}


ForwardIntegratorNixe::NixeOptions createNixeOptions
                            (const std::map<std::string,std::string> &integratorOptions)
{
  ForwardIntegratorNixe::NixeOptions options;
  std::string absTolScalarKey = "absolute tolerance";
  if(integratorOptions.count(absTolScalarKey)>0){
    options.absTolScalar = getDoubleEntry(integratorOptions, absTolScalarKey);
  }
  
  std::string relTolScalarKey = "relative tolerance";
  if(integratorOptions.count(relTolScalarKey)>0){
    options.relTolScalar = getDoubleEntry(integratorOptions, relTolScalarKey);
  }

  std::string initialStepSizeKey = "initial stepsize";
  if(integratorOptions.count(initialStepSizeKey)>0){
    options.initialStepSize = getDoubleEntry(integratorOptions, initialStepSizeKey);
  }
  
  return options;
}

#ifdef HAVE_LIMEX
IntegratorLimex::InitialTolerance createLimexInitTol(
                                const std::map<std::string, std::string> &integratorOptions)
{
  IntegratorLimex::InitialTolerance initTol;
  std::string absTolKey = "absolute tolerance";
  if(integratorOptions.count(absTolKey)>0){
    initTol.absoluteTolerance = getDoubleEntry(integratorOptions, absTolKey);
  }
  
  std::string relTolKey = "relative tolerance";
  if(integratorOptions.count(relTolKey)>0){
    initTol.relativeTolerance = getDoubleEntry(integratorOptions, relTolKey);
  }
  return initTol;
}
#endif


#ifdef HAVE_IDAS
ForwardIntegratorIdas::IdasOptions createIdasOptions(const std::map<std::string, std::string> &integratorOptions)
{
	ForwardIntegratorIdas::IdasOptions options;
	std::string absTolKey = "absolute tolerance";
	if (integratorOptions.count(absTolKey)>0) {
		options.m_absoluteTolerance = getDoubleEntry(integratorOptions, absTolKey);
	}

	std::string relTolKey = "relative tolerance";
	if (integratorOptions.count(relTolKey)>0) {
		options.m_relativeTolerance = getDoubleEntry(integratorOptions, relTolKey);
	}

	std::string relSensMeth = "forward sensitivity method";
	if (integratorOptions.count(relSensMeth)>0) {
		std::string value;
		std::map<std::string, std::string> nonConstMap = integratorOptions;
		value = nonConstMap[relSensMeth];
		if (value == "simultaneous") {
			options.m_sensIntMethod = 1;
		}
		else if (value == "staggered") {

			options.m_sensIntMethod = 2;
		}

		else {

			throw FalseOptionInputException(nonConstMap[relTolKey], relTolKey);

		}
	}
	return options;
}
#endif


double getDoubleEntry(const std::map<std::string, std::string> &integratorOptions, std::string &key)
{
  //need non const map for operator []
  std::map<std::string, std::string> nonConstMap = integratorOptions;
  std::stringstream sstr("");
  sstr<<nonConstMap[key];
  double doubleEntry = 0;
  if(sstr>>doubleEntry){
   return doubleEntry;
  }
  else{
    throw FalseOptionInputException(nonConstMap[key], key);
  }
  return 0.0;
}

