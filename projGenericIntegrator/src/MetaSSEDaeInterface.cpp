/**
* @file MetaSSEDaeInterface.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Member definitions of class MetaSSEDaeInterface                      \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 20.10.2011
*/

#include "MetaSSEDaeInterface.hpp"
#include "Array.hpp"

/**
* @brief constructor
*
* @param[in] integratorMetaData object of type IntegratorMetaData accessing all integration relevant data
* @param[in] type specifiying the order of the sensitivity calculation done by Nixe:
*           -'zerothOrder': only state integration
*           -'firstForwOrder': state integration and calculation of first-order forward sensitivities
*/
MetaSSEDaeInterface::MetaSSEDaeInterface(const IntegratorMetaData::Ptr &integratorMetaData, TypeDaeInterface type)
{
   m_integratorMetaData = integratorMetaData;
   m_typeDaeInterface = type;
}

void MetaSSEDaeInterface::initializeForNewIntegration(const double startTime, const double endTime)
{
  NIXE_SHARED_PTR<Nixe::ProblemInfo> tempProblemInfo(new Nixe::ProblemInfo);
  tempProblemInfo->SetStartTime(startTime);
  tempProblemInfo->SetFinalTime(endTime);
  tempProblemInfo->SetNumStates(m_integratorMetaData->getGenericEso()->getNumStates());
  tempProblemInfo->SetNumSens(0);
  const int numCurSensPar = m_integratorMetaData->getNumCurrentSensitivityParameters();
  const unsigned numDecVars = m_integratorMetaData->getNumSensitivityParameters();
  tempProblemInfo->SetNumParams(numDecVars);
  switch (m_typeDaeInterface){
    case zerothOrder:
      {
        tempProblemInfo->SetMaxOrderSens(0); // used for initialization
      break;
      }
    case firstForwOrder:
      {
        tempProblemInfo->SetNumSens(numCurSensPar);
        tempProblemInfo->SetMaxOrderSens(1);
      break;
      }
  case firstRevOrder:
      {
        tempProblemInfo->SetNumSens(0);
        tempProblemInfo->SetNumAdjoints(1);
        tempProblemInfo->SetNumDers(numDecVars);
        tempProblemInfo->SetNumSaveSens(0);
        tempProblemInfo->SetSave(true);
      break;
      }
  case secondRevOrder:
      {
        tempProblemInfo->SetNumSens(numCurSensPar);
        tempProblemInfo->SetNumAdjoints(1 + numDecVars);
        tempProblemInfo->SetNumDers(numDecVars * (numDecVars + 1));
        tempProblemInfo->SetNumSaveSens(numCurSensPar);
        tempProblemInfo->SetSave(true);
      }
  default:
      break;
  }
  tempProblemInfo->SetNumNonZerosMass(m_integratorMetaData->getGenericEso()->getNumDifferentialNonZeroes());
  tempProblemInfo->SetNumNonZerosJac(m_integratorMetaData->getNumStateNonZeroes());
  tempProblemInfo->SetNumSwitchFctns(0);

  m_problemInfo = tempProblemInfo;
}

/**
* @brief Eval the mass matrix M of the linearly-implicit DAE  M*x' = f(t,x,p)
*
* @param[out] mass: time-invariant mass matrix: M
*/
void MetaSSEDaeInterface::EvalMass(Nixe::CCSInterface *mass)
{
  const int numStates = mass->n;
  const int numDiffNonZeroes = m_integratorMetaData->getGenericEso()->getNumDifferentialNonZeroes();

  CsCscMatrix::Ptr A = m_integratorMetaData->getMMatrix();

  Nixe::VectorCopy(numDiffNonZeroes,mass->ir,A->get_matrix_ptr()->i);
  Nixe::VectorCopy(numStates+1,mass->pc,A->get_matrix_ptr()->p);
  Nixe::VectorCopy(numDiffNonZeroes,mass->values,A->get_matrix_ptr()->x);
  for (int i=0; i<numDiffNonZeroes; i++)
    mass->values[i]*=-1.0;

}

/**
* @brief provide necessary information about the DAE system, like number of states, sensitivities,
* number of nonzeros in the Jacobian, etc. This routine returns a smart pointer to the ProblemInfo class described in
* NixeProblemInfo.hpp
*
* @return Information about the DAE system, see Nixe::ProblemInfo.hpp
*/
NIXE_SHARED_PTR<Nixe::ProblemInfo> MetaSSEDaeInterface::GetProblemInfo()
{
  assert(m_problemInfo);
  return m_problemInfo;
}

/**
* @brief Given the DAE system M*x'=f(t,x,p) and the first-order
* sensitivity equations M*S'= D/Dp f(t,x,p), (higher orders are also allowed)
* this routine provides the right hand side f(t,x,p), possibly higher order tangent-linear
* models of the right hand side, and the Jacobian f_x(t,x,p) of the right hand side
*
* @param[in] evalJac true, if Jacobian has to be set, false if not
* @param[in] time  current time t
* @param[in] states states at current time : x(t), size problemInfo.NumStates()
* @param[in] parameters p
* @param[out] rhsStates f(t,x,p)
* @param[in] sens sensitivities at current time : S(t) ) D/Dp x(t)
* @param[out] jacobian values of the jacobian matrix. They are evaluated if evalJac is true
* @param[out] rhsStatesDtime df/dt f(t,x,p) (partial derivative of ths with respect to time t) only to be set if evalJac==true
* @param[out] rhsSens  D/Dp f(t,x,p) , (higher-order) tangent linear models of f(t,x,p)
* @param[in] problemInfo information about DAE system
*/
void MetaSSEDaeInterface::EvalForward(bool evalJac, double time, double *states, double *parameters,
                                      double *rhsStates, double *sens, Nixe::CCSInterface *jacobian,
                                      double *rhsStatesDtime, double *rhsSens,
                                      const Nixe::ProblemInfo &problemInfo) const
{
  const int numStates = problemInfo.NumStates();
  const int numEqns = m_integratorMetaData->getGenericEso()->getNumEquations();
  const int numSens = problemInfo.IsForwardSweep()?problemInfo.NumSens():problemInfo.NumSaveSens();

  m_integratorMetaData->setStepCurrentTime(time);

  utils::WrappingArray<double> statesArray(numStates, states);
  utils::WrappingArray<double> rhsStatesArray(numEqns, rhsStates);
  m_integratorMetaData->calculateRhsStates(statesArray, rhsStatesArray);


  /********** sensitivities ****************/

  if(numSens > 0){
    utils::WrappingArray<double> sensArray(numStates*numSens,sens);
    utils::WrappingArray<double> rhsSensArray(numStates*numSens,rhsSens);
    m_integratorMetaData->calculateSensitivitiesForward1stOrder(sensArray, rhsSensArray);
  }


  /************* Jacobian ***************/

  if(evalJac){
    const int n_jxp = m_integratorMetaData->getNumStateNonZeroes();
    CsCscMatrix::Ptr jacobianFstates = m_integratorMetaData->getJacobianFstates();
    //preset field to 0.0 before calling rhsStatesDtime
    utils::WrappingArray<double> rhsStatesDTimeArray(problemInfo.NumStates(), rhsStatesDtime);
    for(int i=0; i<problemInfo.NumStates();i++){
      rhsStatesDTimeArray[i] = 0.0;
    }
    m_integratorMetaData->calculateRhsStatesDtime(rhsStatesDTimeArray);

    Nixe::VectorCopy(n_jxp,jacobian->ir,jacobianFstates->get_matrix_ptr()->i);
    Nixe::VectorCopy(numStates+1,jacobian->pc,jacobianFstates->get_matrix_ptr()->p);
    Nixe::VectorCopy(n_jxp,jacobian->values,jacobianFstates->get_matrix_ptr()->x);
    for(int i=0; i<n_jxp; i++){
      // @comment mosc01 (21.12.2011): scaling of the jacobian is necessary because rhs and rhsSens
      // are also scaled by MetaEso, but the jacobian not. Scaling of jacobian in MetaEso makes no
      // sense because all the scaling-operations are done after performing calculations with the
      // jacobian. Here we need jacobian explicitly, so we have to scale explicitly.
      // Would be better if the entire scaling task would take place in one module (e.g. MetaEso).
      // This could be done by using always the scaled jacobian in MetaEso end remove the
      // "afterwards" scaling of res and rhsSens.
      jacobian->values[i] *= m_integratorMetaData->getIntegrationGridDuration();
    }
    jacobian->nzmax = jacobianFstates->get_matrix_ptr()->nzmax;
  }
}

/**
* @brief Set the intial values of the state variables x(t_0) and sensitivities S(t_0)
*
* @param[in] time: start time, where the states and sensitiviteis are to be set
* @param[in] parameters: p
* @param[out] states: states at intial  time : x(t_0)
* @param[out] sens: sensitivities at intial time : S(t_0)
* @param[in] problemInfo: information about DAE system
*/
void MetaSSEDaeInterface::EvalInitialValues(double time, double *parameters, double *states,
                                            double *sens, const Nixe::ProblemInfo &problemInfo)
{
  const int numStates = problemInfo.NumStates();
  const int numSens = problemInfo.NumSens();

  utils::WrappingArray<double> statesArray(numStates, states);
  utils::WrappingArray<double> sensArray(numStates*numSens, sens);
  m_integratorMetaData->evaluateInitialValues(statesArray, sensArray);
}

/**
* @brief obtain the structure of the jacobian matrix
*
* The method obtains information about the jacobian matrix (
* the derivatives of f w.r.t x ), x contains ONLY states.
* The information returned comprises the location of entries in the
* jacobian matrix. The caller of the method has to allocate space in
* appropriate dimensions in the rows and cols parameters.
*
* @param[out] nNonZeroDfdx - holds the number of non zeros in df/dx
*        after the call
* @param[out] rows - holds the rows of the elements after the call
* @param[out] cols - holds the columns of the elements after the call
*/
void MetaSSEDaeInterface::GetStateJacobianStruct(int& nNonZeroDfdx, int* rows, int* cols)
{
  CsCscMatrix::Ptr jacobianFstatesCSC = m_integratorMetaData->getJacobianFstates();
  CsTripletMatrix::Ptr jacobianFstatesCOO(new CsTripletMatrix(
                                                jacobianFstatesCSC->get_matrix_ptr(), false));
  nNonZeroDfdx = jacobianFstatesCOO->get_nnz();
  assert(nNonZeroDfdx == m_integratorMetaData->getNumStateNonZeroes());
  Nixe::VectorCopy(nNonZeroDfdx, rows, jacobianFstatesCOO->get_matrix_ptr()->i);
  Nixe::VectorCopy(nNonZeroDfdx, cols, jacobianFstatesCOO->get_matrix_ptr()->p);
}

/**
* @brief Initial set of parameters p of the DAE right hand side f(t,x,p)
*
* @param[out] parameters: p
* @param[in] problemInfo information about DAE system
*/
void MetaSSEDaeInterface::EvalParameters(double *parameters, const Nixe::ProblemInfo &problemInfo)
{
  std::vector<Parameter::Ptr> sensParams;
  m_integratorMetaData->getSensitivityParameters(sensParams);
  assert(unsigned(problemInfo.NumParams()) == sensParams.size());
  for(unsigned i=0; i<sensParams.size(); i++){
    parameters[i] = sensParams[i]->getParameterValue();
  }
}

/**
* @brief Given the DAE system M*x'=f(t,x,p) this right routine evaluation
* (higher order adjoint projections) lambda^T*f_x(t,x,p) and lambda^T*f_p(t,x,p). Here lambda are
* the possibly first-order adjoints. For second-order adjoints the total derivatives with respect
* to the parameters p_i are computed: D/Dp_i lambda^T*f_x(t,x,p) and D/Dp_i lambda^T*f_p(t,x,p)
*
* @param[in] time: t
* @param[in] states: states at current time : x(t)
* @param[in] parameters: p
* @param[in] sens: sensitivities at current time : S(t)
* @param[in] adjoints: (higher-order) adjoints at current time : lambda(t) or D/Dp_i lambda
* @param[out] rhsAdjoints: lambda^T*f_x(t,x,p) or D/Dp_i lambda^T*f_x(t,x,p)
* @param[out] rhsDers:  lam^T*f_p(t,x,p) or D/Dp_i lam^T*f_p(t,x,p)
* @param[in] problemInfo: information about DAE system
*/
void MetaSSEDaeInterface::EvalReverse(double time, double *states, double *parameters,
                                      double *sens, double *adjoints, double *rhsAdjoints,
                                      double *rhsDers, const Nixe::ProblemInfo &problemInfo) const
{
  GenericEso::Ptr genericEso = m_integratorMetaData->getGenericEso();
  const int numEqns = genericEso->getNumEquations();
  const int numStates = problemInfo.NumStates();
  const int numDers = problemInfo.NumDers();
  
  utils::WrappingArray<double> statesArray(numStates, states);
  utils::WrappingArray<double> adjointsArray(numEqns, adjoints);
  utils::WrappingArray<double> rhsAdjointsArray(numStates, rhsAdjoints);
  utils::WrappingArray<double> rhsDersArray(numDers, rhsDers);

  // set time
  m_integratorMetaData->setStepCurrentTime(time);

  // initialize rhsDers with 0
  for(int i=0; i<numDers; i++){
    rhsDersArray[i] = 0.0;
  }

  // calculating rhsAdjoints and rhsDers for 1st order
  m_integratorMetaData->calculateAdjointsReverse1stOrder(statesArray, adjointsArray,
                                                         rhsAdjointsArray, rhsDersArray);
}

/**
* @brief Set the final values of the adjoint variables
*
* @param[in] time: final time, where the adjoints are to be set
* @param[in] parameters: p
* @param[in] states: states at final time : x(t_f)
* @param[in] sens: sensitivities at final time : S(t_f)
* @param[out] adjoints: adjoints at final time : lambda(t_f)
* @param[in] problemInfo: information about DAE system
*/
void MetaSSEDaeInterface::EvalFinalValues(double time, double *states, double *parameters,
                                          double *sens, double *adjoints,
                                          const Nixe::ProblemInfo &problemInfo)
{
  std::vector<double> adjointsVec;
  m_integratorMetaData->getAdjoints(adjointsVec);
  for(unsigned i=0; i<adjointsVec.size(); i++){
    adjoints[i] = adjointsVec[i];
  }
}

/**
* @brief Eval (vector-valued) switching function sigma(t,x,p) for nonsmooth systems
*
* @param[in]  time: t
* @param[in] states: states at current time : x(t)
* @param[in] parameters: p
* @param[out] switchingFctn: values of switching function sigma(t,x,p)
* @param[in] problemInfo: information about DAE system
*/
void MetaSSEDaeInterface::EvalSwitchingFunction(double time, double *states, double *parameters,
                                                double *switchingFctn,
                                                const Nixe::ProblemInfo &problemInfo) const
{
}

/**
* @brief obtain selected rhs values
*
* The method stores the selected rhs-values in rhsStates. It takes as input the equation indices
* of length len, the time and all the statevalues.
*
* @param[in] len: length of indices
* @param[in] indices: equation indices of rhsStates
* @param[in] time: Evaluation time
* @param[in] states: State-values for the full System (size = numStates in ESO)
* @param[in] dimStates: dimension of states array
* @param[out] rhsStates: array of length len that contains the right-hand-side values after the call
*/
void MetaSSEDaeInterface::EvalRhs(int len, long* indices, double time, double* states,
                                  int dimStates, double* rhsStates)
{
  GenericEso::Ptr genericEso = m_integratorMetaData->getGenericEso();

  utils::WrappingArray<double> statesArray(dimStates, states);
  utils::WrappingArray<double> rhsStatesArray(len, rhsStates);
  utils::Array<double> dummyDerivatives(genericEso->getNumDifferentialVariables(),0.0);
  std::vector<int> equationIndex(len);
  for(int i=0; i<len; i++){
    equationIndex[i] = (int)indices[i];
  }

  // set state values
  genericEso->setStateValues(statesArray.getSize(), statesArray.getData());

  // set all derivatives to 0
  genericEso->setDerivativeValues(dummyDerivatives.getSize(), dummyDerivatives.getData());

  // retrieve (block-wise) residuals
  genericEso->getResiduals(rhsStatesArray.getSize(), rhsStatesArray.getData(), &equationIndex[0]);

  // multiplication by duration of interval
  const double durationInterval = m_integratorMetaData->getIntegrationGridDuration();
  for(unsigned i=0; i<rhsStatesArray.getSize(); i++){
    rhsStates[i] = rhsStates[i]*durationInterval;
  }
}

/**
* @brief Evaluates a quadratic subpart of the state-jacobian of the entire system and returns it in
*        triplet format
*
* Given the row- and column indices of the state-jacobian subpart in "rows" and "cols", the method
* calculates the "reduced Jacobian" by using the row-wise evaluation ("block-evaluation") of the
* tangent-linear AD code.
*
* @param[in] time: Evaluation time
* @param[in] states: State-values for the full System (size = numStates in ESO)
* @param[in] dimStates: dimension of states array
* @param[in] rows: row indices of "reduced" Jacobian in coordinate format
* @param[in] cols: column indices of "reduced" Jacobian in coordinate format
* @param[in] dim: dimension of reduced Jacobian
* @param[in] SparsityPattern : It contains the indices of the non-zero entries of the reduced
*                              jacobian matrix in row-major format.
*                                                                            x 0 0 x
*                              For example: the sparsity pattern of matrix   0 0 x x  would be:
*                                                                            0 x x x
*                                                                            0 x 0 x
*                              [0 3 6 7 9 10 11 13 15].
* @param[in] len: number of nonZeros in reduced Jacobian
* @param[out] jacRowInd: row indices of "reduced" Jacobian entries
* @param[out] jacColInd: col indices of "reduced" Jacobian entries
* @param[out] jacVal: values of "reduced" Jacobian in coordinate format
*/
void MetaSSEDaeInterface::EvalStateJac(double time, double* states, int dimStates, int* rows,
                                       int* cols, int dim, int* SparsityPattern, int* jacRowInd,
                                       int* jacColInd, double* jacVal, int len)
{
  GenericEso::Ptr genericEso = m_integratorMetaData->getGenericEso();

  utils::WrappingArray<double> statesArray(dimStates, states);
  //utils::WrappingArray<double> jacValArray(len, jacVal);
  std::vector<int> indices(len);

  // set state values
  genericEso->setStateValues(statesArray.getSize(), statesArray.getData());

  // get the overall jacobian sparsity pattern
  const EsoIndex nnz = genericEso->getNumNonZeroes();
  std::vector<int> overallJacRows(nnz), overallJacCols(nnz);
  genericEso->getJacobianStruct(nnz, &overallJacRows[0], &overallJacCols[0]);

  // indices of each entry in the reduced jacobian with respect to overall jacobian
  int overallRowIndex, overallColIndex;
  for(int i=0; i<len; i++){
    jacColInd[i] = SparsityPattern[i]%dim;
    jacRowInd[i] = (SparsityPattern[i]-jacColInd[i])/dim;
    overallColIndex = cols[jacColInd[i]];
    overallRowIndex = rows[jacRowInd[i]];
    // look for overallIndices in overall sparsity pattern to define 'indices'
    for(unsigned j=0; j<overallJacRows.size(); j++){
      if((overallJacCols[j]==overallColIndex) && (overallJacRows[j]==overallRowIndex)){
        indices[i] = j;
        break;
      }
    }
  }

  genericEso->getJacobianValues(len, jacVal, &indices[0]);

  // multiplication by duration of interval
  const double durationInterval = m_integratorMetaData->getIntegrationGridDuration();
  for(int i=0; i<len; i++){
    jacVal[i] = jacVal[i]*durationInterval;
  }
}
