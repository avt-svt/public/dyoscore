/** @file LimexLoader.cpp
*    @brief definitions of member functions of LimexLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails LimexLoader
*    =====================================================================\n
*   @author Stefan Jaeger, Klaus Stockmann
*   @date 12.1.2012
*/

#include <cassert>
#include <cstdlib>
#include <cstdio>

#include "LimexLoader.hpp"

#ifdef WIN32
#define FORTRAN_UNDERSCORE ""
#else
#define FORTRAN_UNDERSCORE "_"
#endif

//! calling convention for calling FORTRAN function
#if WIN32
#define CALLTYPE __stdcall
#endif

LimexLoader::LimexLoader():Loader("slimex42B1")
{
  slimexs_func_handle = NULL;

  // load function from LIMEX DLL using Windows system function GetProcAddress()
  loadFcn(slimexs_func_handle, "slimexs" FORTRAN_UNDERSCORE);
}

LimexLoader::LimexLoader(const LimexLoader &LimexLoader):Loader(LimexLoader)
{
  slimexs_func_handle = NULL;
  // load function from LIMEX DLL using Windows system function GetProcAddress()
  loadFcn(slimexs_func_handle, "slimexs" FORTRAN_UNDERSCORE);
}


int LimexLoader::slimexs(const int nDae, // input; size: 1
                          const int n, // input; size: 1
                          fcn_ptr fcn,
                          jacobian_ptr jacobian,
                          double &t_Begin, //double *t_Begin,  // in, out; size: 1
                          const double t_End, // input; size: 1
                          utils::Array<double> &y, // in, out; size: n
                          utils::Array<double> &ys, // in, out; size: n
                          double &h, //double *h, // in, out; size: 1
                          LimexOptions &options) // output; size: nDAE
{
  // the casts are required such that we can call the fortran limex call.
  int* nDaeptr = const_cast<int *>(&nDae);
  int* nptr =  const_cast<int *>(&n);
  double* tendptr =  const_cast<double *>(&t_End);
  double* yptr = y.getData();
  double* ysptr = ys.getData();
  double* Rparptr = const_cast<double *>(options.Rpar.getData());
  int* Iparptr = options.Ipar.getData();
  double* rTolptr = const_cast<double *>(options.rTol.getData());
  double* aTolptr = const_cast<double *>(options.aTol.getData());
  int* Ioptptr = const_cast<int *>(options.Iopt.getData());
  int* IoptSptr = const_cast<int *>(options.Iopts.getData());
  double* Roptptr = const_cast<double *>(options.Ropt.getData());
  int* IPosptr = const_cast<int *>(options.IPos.getData());
  int* IFailptr = options.IFail.getData();
  int* ConvTestptr = const_cast<int *>(options.ConvTest.getData());
  double* upperboundsptr = const_cast<double *>(options.upper_bounds.getData());
  double* lowerboundsptr = const_cast<double *>(options.lower_bounds.getData());
  int* Integration_Failureptr = options.Integ_Fail.getData();

  slimexs_func_handle(nDaeptr,
                      nptr,
                      fcn,
                      jacobian,
                      &t_Begin,  // check this
                      tendptr,
                      yptr,
                      ysptr,
                      Rparptr,
                      Iparptr,
                      rTolptr,
                      aTolptr,
                      &h, // check this
                      Ioptptr,
                      IoptSptr,
                      Roptptr,
                      IPosptr,
                      IFailptr,
                      ConvTestptr,
                      upperboundsptr,
                      lowerboundsptr,
                      Integration_Failureptr);
  return IFailptr[0];
}
