/**
* @file ForwardIntegratorNixe.cpp
*
* =========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                        \n
* =========================================================================\n
* ForwardIntegratorNixe - Part of DyOS                                     \n
* =========================================================================\n
* This file contains the method definitions of the ForwardIntegrator class \n
* =========================================================================\n
* @author Moritz Schmitz, Tjalf Hoffmann
* @date 3.9.2012
*/

#include "ForwardIntegratorNixe.hpp"

/**
* @brief constructor
*
* @param integratorMetaData IntegratorMetaData object describing the model to be solved
* @param order integration mode
* @sa TypeDaeInterface
*/
ForwardIntegratorNixe::ForwardIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                                             const NixeOptions &options):
m_integratorMetaData(integratorMetaData), m_options(options),
m_daeInitialization(new NoDaeInitialization())
{
  const unsigned numStages = m_integratorMetaData->getNumStages();
  m_metaSSEDae = NIXE_SHARED_PTR<MetaSSEDaeInterface>(new MetaSSEDaeInterface(integratorMetaData, options.order));

  /*Set tolerances and options for integration*/

  m_optionsNixe = NIXE_SHARED_PTR<Nixe::Options>(new Nixe::Options);
  m_optionsNixe->SetInitialStepSize(options.initialStepSize);
  m_optionsNixe->SetAbsTolScalar(options.absTolScalar);
  m_optionsNixe->SetRelTolScalar(options.relTolScalar);

  /*Checkpoints are needed in case of a subsequent reverse integration*/

  m_checkpoints.resize(numStages);
  m_initialValueCheckpoints.resize(numStages);

}

ForwardIntegratorNixe::ForwardIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                                             const NixeOptions &options,
                                             const DaeInitialization::Ptr &daeInit):
 m_integratorMetaData(integratorMetaData), m_options(options)
{
  const unsigned numStages = m_integratorMetaData->getNumStages();
  m_metaSSEDae = NIXE_SHARED_PTR<MetaSSEDaeInterface>(new MetaSSEDaeInterface(integratorMetaData, options.order));

  /*Set tolerances and options for integration*/

  m_optionsNixe = NIXE_SHARED_PTR<Nixe::Options>(new Nixe::Options);
  m_optionsNixe->SetInitialStepSize(options.initialStepSize);
  m_optionsNixe->SetAbsTolScalar(options.absTolScalar);
  m_optionsNixe->SetRelTolScalar(options.relTolScalar);

  /*Checkpoints are needed in case of a subsequent reverse integration*/

  m_checkpoints.resize(numStages);
  m_initialValueCheckpoints.resize(numStages);
  m_daeInitialization = daeInit;
}

/**
* @brief standard constructor
*/
ForwardIntegratorNixe::ForwardIntegratorNixe()
{
  /*Set tolerances and options for integration*/

  m_optionsNixe = NIXE_SHARED_PTR<Nixe::Options>(new Nixe::Options);
  m_optionsNixe->SetInitialStepSize(1e-3);
  m_optionsNixe->SetAbsTolScalar(1e-5);
  m_optionsNixe->SetRelTolScalar(1e-5);
}
/**
* @brief destructor
*/
ForwardIntegratorNixe::~ForwardIntegratorNixe()
{
}

/**
* @copydoc GenericIntegratorForward::solve
*
* set stageIndex to -1, as it is incremented before the first call
*/
GenericIntegrator::ResultFlag ForwardIntegratorNixe::solve()
{
  m_stageIndex = -1;
  return GenericIntegratorForward::solve();
}

/**
* @copydoc GenericIntegratorForward::initializeStageIntegration
*/
void ForwardIntegratorNixe::initializeStageIntegration()
{
  m_isFirstIntegrationCall = true;
  m_stageIndex++;
}

/**
* @copydoc GenericIntegratorForward::solveStep
*/
GenericIntegratorForward::SolveStepResult ForwardIntegratorNixe::solveStep(double& startTime,
                                                                           const double endTime,
                                                                           utils::Array<double> &finalStates,
                                                                           utils::Array<double> &finalSensitivities)
{
  m_metaSSEDae->initializeForNewIntegration(startTime, endTime);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = m_metaSSEDae->GetProblemInfo();
  if(m_isFirstIntegrationCall){
    // update forward solver - necessary for call of Initialize
    m_nixeFor.reset(new Nixe::ForwardSolver<Nixe::CompressedColumn> (m_metaSSEDae, m_optionsNixe));
    m_nixeFor->Initialize(); // treats also initialization of checkpoints if
                            // Nixe::ProblemInfo.m_Save is true

    // checkpointing if necessary
    if(problemInfo->Save()){
      m_initialValueCheckpoints[m_stageIndex]=m_nixeFor->GetCheckpoints();
    }
    m_isFirstIntegrationCall = false;
  }
  else{
    m_optionsNixe->SetComputeCIV(true);
  }
  // update forward solver for call of Solve - options might have changed
  m_nixeFor.reset(new Nixe::ForwardSolver<Nixe::CompressedColumn> (m_metaSSEDae, m_optionsNixe));
  Nixe::Status status = m_nixeFor->Solve();
  startTime = m_nixeFor->GetCurrentTime();
  // checkpointing if necessary
  if(problemInfo->Save()){
    NIXE_SHARED_PTR<Nixe::Checkpoints> checkp = m_nixeFor->GetCheckpoints();
    m_checkpoints[m_stageIndex].push_back(checkp);
  }

  m_states=m_nixeFor->GetStates();
  double *statesPtr = m_states->Data();
  assert((unsigned)m_states->Size() == finalStates.getSize());
  for(unsigned i=0; i<finalStates.getSize(); i++){
    finalStates[i] = statesPtr[i];
  }

  storeSensInformation(finalSensitivities);
  GenericIntegratorForward::SolveStepResult retval;
  switch (status) {
  case Stat_Success:
    retval = GenericIntegratorForward::SolveStepSuccess;
    break;
  case Stat_RootFound:
    retval = GenericIntegratorForward::SolveStepFoundRoot;
    break;
  default:
    retval = GenericIntegratorForward::SolveStepFailed;
  }
  return retval;
}

/**
* @brief copy sensitivity result of integration into output array
*
* If no sensitivities were calculated during integration (zerothOrder) nothing is done
* @param[out] finalSensitivities array receiving the sensitivity result
*/
void ForwardIntegratorNixe::storeSensInformation(utils::Array<double> &finalSensitivities) const
{
  if(m_options.order != zerothOrder){
    // array of size 'numStates*m_integratorMetaData::getNumCurrentSensitivityParameters()' to
    // extract the sensitivity values from Nixe after each solved interval.
    // They are transformed afterwards and finally stored in m_integratorMetaData
    NIXE_SHARED_PTR<Nixe::DMatrix> sens;
    sens = m_nixeFor->GetSens();
    const unsigned numStates = m_integratorMetaData->getGenericEso()->getNumStates();
    const unsigned sensSize = numStates*m_integratorMetaData->getNumCurrentSensitivityParameters();
    assert((unsigned)sens->Size() == sensSize);
    assert(finalSensitivities.getSize() == sensSize);
    double *sensPtr = sens->Data();
    for(unsigned i=0; i<sensSize; i++){
      finalSensitivities[i] = sensPtr[i];
    }
  }
}

/**
* @copydoc GenericIntegratorForward::getIntegratorMDPtr
*/
IntegratorMetaData::Ptr ForwardIntegratorNixe::getIntegratorMDPtr() const
{
  return m_integratorMetaData;
}


DaeInitialization::Ptr ForwardIntegratorNixe::getDaeInitializationPtr() const
{
  return m_daeInitialization;
}
