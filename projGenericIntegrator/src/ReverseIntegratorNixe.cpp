/**
* @file ReverseIntegratorNixe.cpp
*
* =========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                        \n
* =========================================================================\n
* ReverseIntegratorNixe - Part of DyOS                                     \n
* =========================================================================\n
* This file contains the method definitions of the ReverseIntegrator class \n
* =========================================================================\n
* @author Moritz Schmitz
* @date 06.06.2012
*/

#include <cassert>
#include "ReverseIntegratorNixe.hpp"

/**
* @brief constructor
*
* @param integratorMetaData IntegratorMetaData object describing the model to be solved
* @param order integration mode
* @sa TypeDaeInterface
*/
ReverseIntegratorNixe::ReverseIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                                             const ForwardIntegratorNixe::NixeOptions &options)
:ForwardIntegratorNixe(integratorMetaData, options)
{
}

ReverseIntegratorNixe::ReverseIntegratorNixe(const IntegratorMetaData::Ptr &integratorMetaData,
                                             const ForwardIntegratorNixe::NixeOptions &options,
                                             const DaeInitialization::Ptr &daeInit)
:ForwardIntegratorNixe(integratorMetaData, options, daeInit)
{
}

/**
* @brief standard constructor
*/
ReverseIntegratorNixe::ReverseIntegratorNixe()
{
}

/**
* @brief destructor
*/
ReverseIntegratorNixe::~ReverseIntegratorNixe()
{
}

/**
* @brief GenericIntegratorReverse::solve
*/
GenericIntegrator::ResultFlag ReverseIntegratorNixe::solve()
{
  m_stageIndex = -1;
  m_revStageIndex = m_integratorMetaData->getNumStages();
  return GenericIntegratorReverse::solve();
}

/**
* @brief GenericIntegratorReverse::solveForward
*/
GenericIntegrator::ResultFlag ReverseIntegratorNixe::solveForward()
{
  m_stageIndex = -1;
  return GenericIntegratorReverse::solveForward();
}

/**
* @brief GenericIntegratorReverse::SolveReverse
*/
void ReverseIntegratorNixe::solveReverse()
{
  m_revStageIndex = m_integratorMetaData->getNumStages();
  GenericIntegratorReverse::solveReverse();
}

/**
* @copydoc ForwardIntegratorNixe::getIntegratorMDPtr
*/
IntegratorMetaData::Ptr ReverseIntegratorNixe::getIntegratorMDPtr() const
{
  return ForwardIntegratorNixe::getIntegratorMDPtr();
}

/**
* @copydoc ForwardIntegratorNixe::solveStep
*/
GenericIntegratorForward::SolveStepResult ReverseIntegratorNixe::solveStep(double& startTime,
                                                                           const double endTime,
                                                                           utils::Array<double> &finalStates,
                                                                           utils::Array<double> &finalSensitivities)
{
  return ForwardIntegratorNixe::solveStep(startTime, endTime, finalStates, finalSensitivities);
}

/**
* @copydoc ForwardIntegratorNixe::solveInterval
*/
GenericIntegrator::ResultFlag ReverseIntegratorNixe::solveInterval()
{
  return ForwardIntegratorNixe::solveInterval();
}

/**
* @copydoc ForwardIntegratorNixe::initializeStageIntegration
*/
void ReverseIntegratorNixe::initializeStageIntegration()
{
  ForwardIntegratorNixe::initializeStageIntegration();
}

/**
* @copydoc GenericIntegratorReverse::initializeStageIntegrationReverse
*/
void ReverseIntegratorNixe::initializeStageIntegrationReverse()
{
  m_numCallsActualStage = 0;
  // m_stageIndex starts with the wrong value and there is no way to reinitialize properly
  // so use revStageIndex here.
  m_revStageIndex--;
}

/**
* @copydoc GenericIntegratorReverse::solveStepReverse
*/
void ReverseIntegratorNixe::solveStepReverse(double& reverseStartTime,
                                             const double reverseEndTime,
                                             utils::Array<double> &adjoints,
                                             utils::Array<double> &lagrangeDerivatives)
{
  m_metaSSEDae->initializeForNewIntegration(reverseEndTime, reverseStartTime);
  NIXE_SHARED_PTR<Nixe::Checkpoints> check;
  const int numCheckPtsInStage = m_checkpoints[m_revStageIndex].size();
  check = m_checkpoints[m_revStageIndex][numCheckPtsInStage-1-m_numCallsActualStage];
  m_numCallsActualStage++;
  m_nixeRev.reset(new Nixe::ReverseSolver<Nixe::CompressedColumn> (m_metaSSEDae, check));
  m_nixeRev->Solve();
  reverseStartTime = reverseEndTime;
  copyAdjointsAndLagrangeDers(adjoints, lagrangeDerivatives);
}

/**
* @copydoc GenericIntegratorReverse::evaluateAtInitialPointReverse
*/
void ReverseIntegratorNixe::evaluateAtInitialPointReverse(utils::Array<double> &adjoints,
                                                          utils::Array<double> &lagrangeDerivatives)
{
  NIXE_SHARED_PTR<Nixe::Checkpoints> check;
  check = m_initialValueCheckpoints[m_revStageIndex];
  m_nixeRev.reset(new Nixe::ReverseSolver<Nixe::CompressedColumn> (m_metaSSEDae, check));
  m_nixeRev->Initialize();

  copyAdjointsAndLagrangeDers(adjoints, lagrangeDerivatives);
}

/**
* @copydoc GenericIntegratorReverse::copyAdjointsAndLagrangeDers
*/
void ReverseIntegratorNixe::copyAdjointsAndLagrangeDers(utils::Array<double> &adjoints,
                                                        utils::Array<double> &lagrangeDerivatives)
{
  NIXE_SHARED_PTR<Nixe::DMatrix> adjointsNixe;
  NIXE_SHARED_PTR<Nixe::Array<double> > lagrangeDersNixe;
  adjointsNixe = m_nixeRev->GetAdjoints();
  lagrangeDersNixe = m_nixeRev->GetDerivatives();

  assert(adjoints.getSize() == (unsigned)adjointsNixe->Size());
  double *adjointsPtr = adjointsNixe->Data();
  for(unsigned i=0; i<adjoints.getSize(); i++){
    adjoints[i] = adjointsPtr[i];
  }

  assert(lagrangeDerivatives.getSize() == (unsigned)lagrangeDersNixe->Size());
  double *lagrangeDersPtr = lagrangeDersNixe->Data();
  for(unsigned i=0; i<lagrangeDerivatives.getSize(); i++){
    lagrangeDerivatives[i] = lagrangeDersPtr[i];
  }
}


/**
* @copydoc GenericIntegratorReverse::getNumAdjoints
*/
unsigned ReverseIntegratorNixe::getNumAdjoints() const
{
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = m_metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numAdjointVectors = problemInfo->NumAdjoints();
  return (unsigned)numStates * (unsigned)numAdjointVectors;
}

/**
* @copydoc GenericIntegratorReverse::getNumLagrangeDers
*/
unsigned ReverseIntegratorNixe::getNumLagrangeDers() const
{
  return (unsigned)m_metaSSEDae->GetProblemInfo()->NumDers();
}

/**
* @copydoc ForwardIntegratorNixe::storeSensInformation
*
* In 1st order reverse integration sensitivity information is not needed
*/
void ReverseIntegratorNixe::storeSensInformation(utils::Array<double> &finalSensitivities) const
{}

DaeInitialization::Ptr ReverseIntegratorNixe::getDaeInitializationPtr() const
{
  return ForwardIntegratorNixe::getDaeInitializationPtr();
}
