/** 
* @file SingleStageIntegration.testsuite
* @brief test cases for GenericIntegrator.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Tests of single-stage integration using a simple car example.        \n
* The reference solution was calculated using Jade and  NIXE.          \n
* =====================================================================\n
*
* @author Moritz Schmitz
* @date 4.4.2012
*/

#include "MetaDataFactories.hpp" //need this for constructing controls more easily

BOOST_AUTO_TEST_SUITE(TestGenericIntegratorSingleStage)

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as ZerothOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestZerothOrder)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 0;
  const int numConstraints = 0;
  const double duration = 1.2;
  const int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 3;
  const double controlParams[3] = {1.0, 0.7, -0.43};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = false;
  const bool controlWithSensitivity = false;
  const bool durationWithSensitivity = false;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSingleStageMetaData::Ptr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();

  ForwardIntegratorNixe::NixeOptions options;
  options.order = zerothOrder;
  ForwardIntegratorNixe zof((integratorMetaData), options);
  BOOST_CHECK_NO_THROW(zof.solve());

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<std::vector<double> > compStates;
  compStates.resize(numContrIntervals);
  for(unsigned i=0; i<unsigned(numContrIntervals); i++){
    compStates[i].resize(numStates);
  }
  //            x_0(t_i)                              x_1(t_i)                               x_2(t_i)
  compStates[0][0] = 0.400000000000000; compStates[0][1] = 0.399946675219205; compStates[0][2] = 0.079994667253949;
  compStates[1][0] = 0.800000000000003; compStates[1][1] = 0.679648746975818; compStates[1][2] = 0.295923817720189;
  compStates[2][0] = 1.200000000000007; compStates[2][1] = 0.507294083496392; compStates[2][2] = 0.533305564438758;

  // compare integrator results with expected results
  utils::Array<double> finalStates(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates.getSize(), finalStates.getData());

  const unsigned finalStateIndex = 2;
  for(unsigned i=finalStateIndex; i<unsigned(numContrIntervals); i++){ // we check only final states
    for(int j=0; j<numStates; j++){
      BOOST_CHECK_CLOSE(finalStates[j], compStates[i][j], THRESHOLD);
    }
  }
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as ZerothOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestZerothOrder2)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 0;
  const int numConstraints = 0;
  const double duration = 21.3;
  const int controlEsoIndex = 10; // corresponds to parameter 'cB0' in mof-file
  const int numContrIntervals = 1;
  const double paramVals[1] = {0.05};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = false;
  const bool controlWithSensitivity = false;
  const bool durationWithSensitivity = false;

  integratorSSMetaData = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     paramVals,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSingleStageMetaData::Ptr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();

  ForwardIntegratorNixe::NixeOptions options;
  options.order = zerothOrder;
  ForwardIntegratorNixe zof((integratorMetaData), options);
  BOOST_CHECK_NO_THROW(zof.solve());

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compStates;
  compStates.resize(numStates);
  
  //            x_0(t_f)                              x_1(t_f)                               x_2(t_f)
  compStates[0] = 0.013132316803311;    compStates[1] = 0.195053364511653;      compStates[2] = 0.657774842539209;
  //            x_3(t_f)                              x_4(t_f)                               x_5(t_f)
  compStates[3] = 1.511111329018938e-04;compStates[4] = 0.001031495849140;      compStates[5] = 28.438143603371014;
  //            x_6(t_f)                              x_7(t_f)                               x_8(t_f)
  compStates[6] = 1.204562908021904e+04;   compStates[7] = 0.690748926371435; 

  // compare integrator results with expected results
  utils::Array<double> finalStates(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates.getSize(), finalStates.getData());

  for(int i=0; i<numStates; i++){// we check only final states
    BOOST_CHECK_CLOSE(finalStates[i], compStates[i], 1e-5);
  }
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderForward)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 3;
  const int initialEsoIndices[3] = {0, 1, 2};
  const double initialValues[3] = {0.0, 0.0, 0.0};
  const double duration = 1.2;
  const int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 3;
  const double controlParams[3] = {1.0, 0.7, -0.43};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = true;
  const bool durationWithSensitivity = false;
  const bool controlWithSensitivity = true;
  const int numConstraints = 0;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSingleStageMetaData::Ptr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();

  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  ForwardIntegratorNixe fof(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(fof.solve());
  std::vector<double> finalSens;
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtDuration;
  compControlSensAtDuration.resize(numStates*numContrIntervals);
  //     dx_0(t_f)/dpi                       dx_1(t_f)/dpi                               dx_2(t_f)/dpi
  compControlSensAtDuration[0] = 0.0; compControlSensAtDuration[1] = 0.398988005032359; compControlSensAtDuration[2] = 0.399553178988916;
  compControlSensAtDuration[3] = 0.0; compControlSensAtDuration[4] = 0.399291308446237; compControlSensAtDuration[5] = 0.239777972134079;
  compControlSensAtDuration[6] = 0.0; compControlSensAtDuration[7] = 0.399774195296211; compControlSensAtDuration[8] = 0.079968359546433;
  
  for(int i=0; i<numStates*numContrIntervals; i++){
    BOOST_CHECK_CLOSE(finalSens[i+9], compControlSensAtDuration[i], THRESHOLD);
  }
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are tested for
* an updated car example
*/
/*BOOST_AUTO_TEST_CASE(TestFirstOrderForwardAllSens)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 4;
  const int initialEsoIndices[4] = {0, 1, 2, 3};
  const double initialValues[4] = {2.0, 4.0, 3.0, 5.0};
  const double duration = 10.0;
  const int controlEsoIndex = 5; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 5;
  const double controlParams[5] = {1.0, 0.7, -0.43, 0.1, -1.0};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const int numConstraints = 0;
  const bool initialsWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const bool durationWithSensitivity = true;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_UPDATED_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSingleStageMetaData::Ptr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();
  const int numControlParameters = numContrIntervals;

  // sensitivities w.r.t. all initial values
  const int numInitialParameters = numInitials; // number of differential states

  const int numDurationParameters = 1; // one global final time

  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  ForwardIntegratorNixe fof(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(fof.solve());
  std::vector<double> finalSens;
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtDuration; 
  compControlSensAtDuration.resize(numStates*numControlParameters);
  std::vector<double> compInitiallSensAtDuration; 
  compInitiallSensAtDuration.resize(numStates*numInitialParameters);
  std::vector<double> compDurationSensAtDuration;
  compDurationSensAtDuration.resize(numStates*numDurationParameters);

  // compare State-Sensitivities with respect to controlparametrization at final Time
  //              dx_0(t_f)/dpi                       dx_1(t_f)/dpi                         dx_2(t_f)/dpi
  compControlSensAtDuration[0] = 17.688179818296300; compControlSensAtDuration[1] = 0.0;  compControlSensAtDuration[2] = 1.932120784511089;
  compControlSensAtDuration[5] = 13.797395524817746; compControlSensAtDuration[6] = 0.0;  compControlSensAtDuration[7] = 1.945769167408612;
  compControlSensAtDuration[10] = 9.899355983491920; compControlSensAtDuration[11] = 0.0; compControlSensAtDuration[12] = 1.962620259046842;
  compControlSensAtDuration[15] = 5.964321627807538; compControlSensAtDuration[16] = 0.0; compControlSensAtDuration[17] = 1.978671328738911;
  compControlSensAtDuration[20] = 1.995546111776400; compControlSensAtDuration[21] = 0.0; compControlSensAtDuration[22] = 1.993803442250187;

  //               dx_3(t_f)/dpi                       dx_4(t_f)/dpi
  compControlSensAtDuration[3] = 29.999999999999897;  compControlSensAtDuration[4] = 0.0;
  compControlSensAtDuration[8] = 29.999999999999957;  compControlSensAtDuration[9] = 0.0;
  compControlSensAtDuration[13] = 0.0;                compControlSensAtDuration[14] = 0.0;
  compControlSensAtDuration[18] = 30.000000000000004; compControlSensAtDuration[19] = 0.0;
  compControlSensAtDuration[23] = 0.0;                compControlSensAtDuration[24] = 0.0;

  for(int i=0; i<numStates*numControlParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[numInitialParameters*numStates + i], compControlSensAtDuration[i], THRESHOLD);
  }

  // compare State-Sensitivities with respect to initial values of differential states at final Time
  //              dx_0(t_f)/dx0i                             dx_1(t_f)/dx0i                           dx_2(t_f)/dx0i
  compInitiallSensAtDuration[0] = 1.0;                compInitiallSensAtDuration[1] = 0.0;   compInitiallSensAtDuration[2] = 0.0;
  compInitiallSensAtDuration[5] = 0.0;                compInitiallSensAtDuration[6] = 1.0;   compInitiallSensAtDuration[7] = 0.0;
  compInitiallSensAtDuration[10] = 9.820058644372743; compInitiallSensAtDuration[11] = 0.0;  compInitiallSensAtDuration[12] = 0.963514084029561;
  compInitiallSensAtDuration[15] = 0.0;               compInitiallSensAtDuration[16] = 0.0;  compInitiallSensAtDuration[17] = 0.0;
  
  //               dx_3(t_f)/dx0i                       dx_4(t_f)/dx0i
  compInitiallSensAtDuration[3] = 0.0;   compInitiallSensAtDuration[4] = 0.0;
  compInitiallSensAtDuration[8] = 0.0;   compInitiallSensAtDuration[9] = 0.0;
  compInitiallSensAtDuration[13] = 0.0;  compInitiallSensAtDuration[14] = 0.0;
  compInitiallSensAtDuration[18] = 1.0;  compInitiallSensAtDuration[19] = 0.0;
  
  for(int i=0; i<numStates*numInitialParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[i], compInitiallSensAtDuration[i], THRESHOLD);
  }

  // compare State-Sensitivities with respect to duration at final Time
  //              dx_0(t_f)/dtf
  compDurationSensAtDuration[0] = 7.284123115136456;
  //              dx_1(t_f)/dtf
  compDurationSensAtDuration[1] = 0.999999999999993;
  //              dx_2(t_f)/dtf
  compDurationSensAtDuration[2] = 0.055761002905124;
  //              dx_3(t_f)/dtf
  compDurationSensAtDuration[3] = 6.399999999999979;
  //              dx_4(t_f)/dtf
  compDurationSensAtDuration[4] = 0.0;
  
  for(int i=0; i<numStates*numDurationParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[(numInitialParameters + numControlParameters)*numStates + i],
                      compDurationSensAtDuration[i], THRESHOLD);
  }
}
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderReverse)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 3;
  const int initialEsoIndices[3] = {0, 1, 2};
  const double initialValues[3] = {0.5, 0.05, 0.0};
  const double duration = 21.3;
  const int controlEsoIndex = 12; // corresponds to parameter 'K1' in mof-file
  const int numControlIntervals = 2;
  const double controlParams[2] = {0.4, 0.5};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const int numConstraints = 5;
  const int constraintEsoIndices[5] = {1, 3, 1, 6, 1}; // cB, cB, cB, r1, summand
  const double constraintTimes[5] = {0.0, 0.32, 0.6, 0.78, 1.0}; // between 0 and 1
  const double LagrMultipliers[5] = {-0.4, 2.43, 0.21, -3.54, 1.4};
  const bool initialsWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const bool durationWithSensitivity = true;

  integratorSSMetaData = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numControlIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     constraintEsoIndices,
                                                     constraintTimes,
                                                     LagrMultipliers);

  // conversion from IntegratorSingleStageMetaData::Ptr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  
  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstRevOrder;
  ReverseIntegratorNixe foRev(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(foRev.solve());

  std::vector<double> firstOrderLagrangeDers;
  integratorMetaData->getLagrangeDers(firstOrderLagrangeDers);


  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  const int numDurationWithSens = 1;
  std::vector<double> compFirstOrderLagrangeDers; 
  compFirstOrderLagrangeDers.resize(numInitials+numControlIntervals+numDurationWithSens,0.0);

  compFirstOrderLagrangeDers[0] = -162164.70282518244;// dLagr/dcA(t0)
  compFirstOrderLagrangeDers[1] = -54550.332852477892;// dLagr/dcB(t0)
  compFirstOrderLagrangeDers[2] = -106571.66961307416;// dLagr/dcC(t0)
  compFirstOrderLagrangeDers[3] =  2841.2975918480415;// dLagr/dK1_1
  compFirstOrderLagrangeDers[4] = -2239.2655410047673;// dLagr/dK1_2
  compFirstOrderLagrangeDers[5] =  165.48557435173961;// dLagr/dtf

  // In the following comparison, we use another tolerance, because the states calculated here in
  // forward mode (which are used in reverse mode to calculated the Lagrange-function derivatives)
  // correspond only within 1e-7 to the states calculated with JADE-NIXE in fo-reverse
  // mode. The states calculated here correspond within 1e-10 to the states calculated with 
  // JADE-NIXE in zof mode.
  // It follows that the comparison values for the Lagrange-function derivatives calculated in
  // JADE-NIXE are based on slightly different states which justifies the usage of a less 
  // restrictive tolerance.

  for(unsigned i=0; i<firstOrderLagrangeDers.size(); i++){
    BOOST_CHECK_CLOSE(firstOrderLagrangeDers[i], compFirstOrderLagrangeDers[i], THRESHOLD);
  }
}

BOOST_AUTO_TEST_CASE(TestSecondOrderReverse)
{
  IntegratorSSMetaData2ndOrder::Ptr integratorSSMD2ndOrder;
  const int numInitials = 3;
  const int initialEsoIndices[3] = {0, 1, 2};
  const double initialValues[3] = {0.5, 0.05, 0.0};
  const double duration = 21.3;
  const int controlEsoIndex = 12; // corresponds to parameter 'K1' in mof-file
  const int numControlIntervals = 2;
  const double controlParams[2] = {0.4, 0.5};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const int numConstraints = 5;
  const int constraintEsoIndices[5] = {1, 3, 1, 6, 1}; // cB, cB, cB, r1, summand
  const double constraintTimes[5] = {0.0, 0.32, 0.6, 0.78, 1.0}; // between 0 and 1
  const double LagrMultipliers[5] = {-0.4, 2.43, 0.21, -3.54, 1.4};
  const bool initialsWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const bool durationWithSensitivity = true;

  integratorSSMD2ndOrder = createExampleFor2ndOrdIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                             numInitials,
                                                             initialEsoIndices,
                                                             initialValues,
                                                             initialsWithSensitivity,
                                                             duration,
                                                             durationWithSensitivity,
                                                             controlEsoIndex,
                                                             numControlIntervals,
                                                             controlParams,
                                                             controlLB,
                                                             controlUB,
                                                             controlWithSensitivity,
                                                             numConstraints,
                                                             constraintEsoIndices,
                                                             constraintTimes,
                                                             LagrMultipliers);

  // conversion from IntegratorSSMD2ndOrderPtr to IntegratorMD2ndOrderPtr
  IntegratorMetaData2ndOrder::Ptr  integratorMD2ndOrder(integratorSSMD2ndOrder);
    
  ForwardIntegratorNixe::NixeOptions options;
  options.order = secondRevOrder;
  Rev2ndOrdIntegratorNixe soRev(integratorMD2ndOrder, options);
  BOOST_CHECK_NO_THROW(soRev.solve());

  std::vector<double> firstAndSecOrderLagrangeDers;
  integratorMD2ndOrder->getLagrangeDers(firstAndSecOrderLagrangeDers);

  
  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  const int numDurationWithSens = 1;
  const unsigned numDecisionVariables = numInitials+numControlIntervals+numDurationWithSens;

  // compare first-order Lagrange derivatives
  std::vector<double> compFirstOrderLagrangeDers; 
  compFirstOrderLagrangeDers.resize(numDecisionVariables);

  compFirstOrderLagrangeDers[0] = -162164.70282518244;// dLagr/dcA(t0)
  compFirstOrderLagrangeDers[1] = -54550.332852477892;// dLagr/dcB(t0)
  compFirstOrderLagrangeDers[2] = -106571.66961307416;// dLagr/dcC(t0)
  compFirstOrderLagrangeDers[3] =  2841.2975918480415;// dLagr/dK1_1
  compFirstOrderLagrangeDers[4] = -2239.2655410048651;// dLagr/dK1_2
  compFirstOrderLagrangeDers[5] =  165.48557435175326;// dLagr/dtf

  for(unsigned i=0; i<numDecisionVariables; i++){
    BOOST_CHECK_CLOSE(firstAndSecOrderLagrangeDers[i],
                      compFirstOrderLagrangeDers[i], THRESHOLD);
  }

  // compare second-order Lagrange derivatives
  const double compSecondOrderLagrangeDers[] = // d2Lagr/dp2
  //       dcA(t0)            dcB(t0)              dcC(t0)        dK1_1              dK1_2                dtf
  {-288990.7034826772, -94581.1688212426, -193991.8527324485,  8865.9821241240, -10085.5355020944, 398.4507618353, // dcA(t0)
   -094581.1688212352, -28455.9362443515,  -65081.6507732319,  2235.3043414902, -3440.6439402276,  202.5634801386, // dcB(t0)
   -193991.8527324422, -65081.6507732332, -128355.4104081647,  2864.5642260246, -6296.4827268330,   70.2657079213, // dcC(t0)
      8865.9821241234,   2235.3043414920,    2864.5642260252,-14558.0191445754,  1831.2894319008, -207.2392909083, // dK1_1
    -10085.5355020944,  -3440.6439402277,   -6296.4827268330, 1831.2894319010,   8605.8626570320,   81.7687569978, // dK1_2
       398.4507695575,    202.5634826876,      70.2657116329, -207.2393305313,     81.7687844192,  -19.6068009011};// dtf

  for(unsigned i=0; i<numDecisionVariables*numDecisionVariables; i++){
    BOOST_CHECK_CLOSE(firstAndSecOrderLagrangeDers[i+numDecisionVariables],
                      compSecondOrderLagrangeDers[i], 1e-1);
  }
}

BOOST_AUTO_TEST_CASE(TestPlotGrid)
{
  GenericEso::Ptr eso(new JadeGenericEso("carFreeFinalTime"));
  
  std::vector<std::string> varNames(eso->getNumVariables());
  eso->getVariableNames(varNames);
  
  
  // initialize controls
  const double intermediateDuration = 4.0;
  const double globalDuration = 10.0;
  std::vector<std::string>::iterator found;
  found = std::find(varNames.begin(), varNames.end(), "alpha");
  BOOST_REQUIRE(found != varNames.end());
  const unsigned alphaIndex = found - varNames.begin();
  
  found = std::find(varNames.begin(), varNames.end(), "accel");
  BOOST_REQUIRE(found != varNames.end());
  const unsigned accelIndex = found - varNames.begin();
  Control accel, alpha;
  
  ControlFactory cf;
  
  FactoryInput::ParameterInput accelInput, alphaInput;
  
  alphaInput.esoIndex = alphaIndex;
  alphaInput.sensType = FactoryInput::ParameterInput::NO;
  alphaInput.type = FactoryInput::ParameterInput::TIME_INVARIANT;
  alphaInput.value = 0.0;
  alphaInput.grids.resize(1);
  alphaInput.grids.front().duration = globalDuration;
  
  alpha = cf.createTimeInvariantParameter(alphaInput);
  
  accelInput.esoIndex = accelIndex;
  accelInput.sensType = FactoryInput::ParameterInput::NO;
  accelInput.type = FactoryInput::ParameterInput::CONTROL;
  
  FactoryInput::ParameterizationGridInput grid1, grid2;
  grid1.duration = intermediateDuration;
  grid1.timePoints.push_back(0.0);
  grid1.timePoints.push_back(1.0);
  grid1.values.push_back(1.0);
  grid2 = grid1;
  grid2.duration = globalDuration - intermediateDuration;
  grid2.values.front() = 2.0;
  accelInput.grids.push_back(grid1), accelInput.grids.push_back(grid2);
  
  accel = cf.createControl(accelInput);
  
  std::vector<Control> controls;
  controls.push_back(alpha);
  controls.push_back(accel);
  
  std::vector<InitialValueParameter::Ptr> initials;
  
  DurationParameter::Ptr durationParameter(new DurationParameter());
  durationParameter->setParameterValue(globalDuration);
  durationParameter->setWithSensitivity(false);
  
  std::vector<double> userGrid;
  
  // construct output MetaSingleStageEso
  IntegratorSingleStageMetaData::Ptr issmd( new IntegratorSingleStageMetaData(eso,
                                                                  controls,
                                                                  initials,
                                                                  durationParameter,
                                                                  userGrid));
  
  
  unsigned numplotGridIntervals = 100;
  std::vector<double> explicitPlotGrid;
  issmd->setPlotGrid(numplotGridIntervals, explicitPlotGrid);
  issmd->activatePlotGrid();
  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  ForwardIntegratorNixe foForw(issmd, options);
  BOOST_CHECK_NO_THROW(foForw.solve());
  
  std::vector<DyosOutput::StageOutput> out = issmd->getOutput();
  
  // check states of ttime, dist and velo  (velo should be a*t, dist should be 1/2a*t�)
  // check timepoints 0 to 40 with accel = 1.0
  BOOST_REQUIRE_EQUAL(out.front().integrator.states[0].name, "ttime");
  BOOST_REQUIRE_EQUAL(out.front().integrator.states[1].name, "velo");
  BOOST_REQUIRE_EQUAL(out.front().integrator.states[2].name, "dist");
  
  unsigned intermediatePlotEnd = 40;
  for(unsigned i=0; i<=intermediatePlotEnd; i++){
    BOOST_CHECK_CLOSE(out.front().integrator.states[0].grid.values[i], i*globalDuration/numplotGridIntervals,
                      THRESHOLD);
    double velo = i*globalDuration/numplotGridIntervals;
    BOOST_CHECK_CLOSE(out.front().integrator.states[1].grid.values[i], velo,
                      THRESHOLD);
    BOOST_CHECK_CLOSE(out.front().integrator.states[2].grid.values[i], velo*velo/2,
                      THRESHOLD);
  }
  const double intermediateTime = intermediatePlotEnd*globalDuration/numplotGridIntervals;
  const double intermediateVelo = intermediateTime;
  const double intermediateDist = intermediateTime*intermediateTime/2;
  
  for(unsigned i=intermediatePlotEnd + 1; i<=numplotGridIntervals; i++){
    BOOST_CHECK_CLOSE(out.front().integrator.states[0].grid.values[i], i*globalDuration/numplotGridIntervals,
                      THRESHOLD);
    double scaledDeltaT = (i - intermediatePlotEnd)*globalDuration/numplotGridIntervals;
    BOOST_CHECK_CLOSE(out.front().integrator.states[1].grid.values[i], intermediateVelo + 2*scaledDeltaT,
                      THRESHOLD);
    BOOST_CHECK_CLOSE(out.front().integrator.states[2].grid.values[i],
                      intermediateDist + scaledDeltaT*intermediateVelo 
                                       + 2*scaledDeltaT*scaledDeltaT/2,
                      THRESHOLD);
  }
}

BOOST_AUTO_TEST_SUITE_END()
