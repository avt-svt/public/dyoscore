/** 
* @file SingleStageIntegrationLimex.testsuite
* @brief test cases for Limex based on generic integrator
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Tests of single-stage integration using a simple car example.        \n
* The reference solution was calculated using JADE and  NIXE.       \n
* =====================================================================\n
*
* @author Fady Assassa
* @date 4.4.2012
*/

BOOST_AUTO_TEST_SUITE(TestGenericIntegratorSingleStageLimex)

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as ZerothOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestZerothOrderLimex)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 0;
  const int numConstraints = 0;
  const double duration = 1.2;
  const int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 3;
  const double controlParams[3] = {1.0, 0.7, -0.43};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = false;
  const bool controlWithSensitivity = false;
  const bool durationWithSensitivity = false;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const unsigned numAlgVars = integratorMetaData->getGenericEso()->getNumAlgebraicVariables();
  // the problem is purley ode!
  BOOST_CHECK_EQUAL(numAlgVars, 0);
  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = LIMEX_INTEGRATION_TOLERANCE;
  initTol.absoluteTolerance = LIMEX_INTEGRATION_TOLERANCE;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());
  BOOST_CHECK_NO_THROW(limex.solve());

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<std::vector<double> > compStates;
  compStates.resize(numContrIntervals);
  const unsigned numStates = integratorMetaData->getGenericEso()->getNumEquations();
 
  for(unsigned i=0; i<unsigned(numContrIntervals); i++){
    compStates[i].resize(numStates);
  }
  //            x_0(t_i)                              x_1(t_i)                               x_2(t_i)
  compStates[0][0] = 0.400000000000000; compStates[0][1] = 0.399946675219205; compStates[0][2] = 0.079994667253949;
  compStates[1][0] = 0.800000000000003; compStates[1][1] = 0.679648746975818; compStates[1][2] = 0.295923817720189;
  compStates[2][0] = 1.200000000000007; compStates[2][1] = 0.507294083496392; compStates[2][2] = 0.533305564438758;

  // compare integrator results with expected results
  utils::Array<double> finalStates(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates.getSize(), finalStates.getData());

  const unsigned finalStateIndex = 2;
  for(unsigned i=finalStateIndex; i<unsigned(numContrIntervals); i++){ // we check only final states
    for(unsigned j=0; j<numStates; j++){
      BOOST_CHECK_CLOSE(finalStates[j], compStates[i][j], 1e-6);
    }
  }
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as ZerothOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestZerothOrder2)
{
  IntegratorSSMetaDataPtr integratorSSMetaData;
  const int numInitials = 0;
  const int numConstraints = 0;
  const double duration = 21.3;
  const int controlEsoIndex = 10; // corresponds to parameter 'cB0' in mof-file
  const int numContrIntervals = 1;
  const double paramVals[1] = {0.05};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = false;
  const bool controlWithSensitivity = false;
  const bool durationWithSensitivity = false;

  integratorSSMetaData = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     paramVals,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();
  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = LIMEX_INTEGRATION_TOLERANCE;
  initTol.absoluteTolerance = LIMEX_INTEGRATION_TOLERANCE;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compStates;
  compStates.resize(numStates);
  
  //            x_0(t_f)                              x_1(t_f)                               x_2(t_f)
  compStates[0] = 0.013132003655880822;    compStates[1] = 0.19504761216964805;      compStates[2] = 0.65777818843146685;
  //            x_3(t_f)                              x_4(t_f)                               x_5(t_f)
  compStates[3] = 0.00015109860079916246;  compStates[4] = 0.0010309324647979079;      compStates[5] = 28.438054291799720;
  //            x_6(t_f)                              x_7(t_f)                               x_8(t_f)
  compStates[6] = 12045.549175822647;   compStates[7] = 0.69075140184673278; 

  // compare integrator results with expected results
  utils::Array<double> finalStates(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates.getSize(), finalStates.getData());

  for(int i=0; i<numStates; i++){// we check only final states
    BOOST_CHECK_CLOSE(finalStates[i], compStates[i], LIMEX_TOLERANCE);
  }
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardLimex)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 3;
  const int initialEsoIndices[3] = {0, 1, 2};
  const double initialValues[3] = {0.0, 0.0, 0.0};
  const double duration = 1.2;
  const int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 3;
  const double controlParams[3] = {1.0, 0.7, -0.43};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = true;
  const bool durationWithSensitivity = false;
  const bool controlWithSensitivity = true;
  const int numConstraints = 0;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const unsigned numAlgVars = integratorMetaData->getGenericEso()->getNumAlgebraicVariables();
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();
  // the problem is purley ode!
  
  std::vector<double> finalSens;
  BOOST_CHECK_EQUAL(numAlgVars, 0);
  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = LIMEX_INTEGRATION_TOLERANCE;
  initTol.absoluteTolerance = LIMEX_INTEGRATION_TOLERANCE;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());
  integratorMetaData->getCurrentSensitivities(finalSens);
  //IntegratorLimex limex2(integratorMetaData);
  BOOST_CHECK_NO_THROW(limex.solve());
  //BOOST_CHECK_NO_THROW(limex.solve());
  finalSens.clear();
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtDuration;
  compControlSensAtDuration.resize(numStates*numContrIntervals);
  //     dx_0(t_f)/dpi                       dx_1(t_f)/dpi                               dx_2(t_f)/dpi
  compControlSensAtDuration[0] = 0.0; compControlSensAtDuration[1] = 0.398988005032359; compControlSensAtDuration[2] = 0.399553178988916;
  compControlSensAtDuration[3] = 0.0; compControlSensAtDuration[4] = 0.399291308446237; compControlSensAtDuration[5] = 0.23977797173130436;
  compControlSensAtDuration[6] = 0.0; compControlSensAtDuration[7] = 0.39977419576220624; compControlSensAtDuration[8] = 0.079968358692287314;
  
  for(int i=0; i<numStates*numContrIntervals; i++){
    BOOST_CHECK_CLOSE(finalSens[i+9], compControlSensAtDuration[i], LIMEX_TOLERANCE);
  }
}



/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardLimex2)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 3;
  const int initialEsoIndices[3] = {0, 1, 2};
  const double initialValues[3] = {0.0, 0.0, 0.0};
  const double duration = 40.0;
  const int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 10;
  const double controlParams[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = false;
  const bool durationWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const int numConstraints = 0;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const unsigned numAlgVars = integratorMetaData->getGenericEso()->getNumAlgebraicVariables();
  // the problem is purley ode!
  
  std::vector<double> finalSensLIMEX;
  BOOST_CHECK_EQUAL(numAlgVars, 0);
  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = LIMEX_INTEGRATION_TOLERANCE;
  initTol.absoluteTolerance = LIMEX_INTEGRATION_TOLERANCE;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());
  integratorMetaData->getCurrentSensitivities(finalSensLIMEX);

  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  ForwardIntegratorNixe fof(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(fof.solve());
  std::vector<double> finalSensNIXE;
  integratorMetaData->getCurrentSensitivities(finalSensNIXE);

  for(unsigned i=0; i<finalSensNIXE.size(); i++){
    BOOST_CHECK_CLOSE(finalSensNIXE[i], finalSensLIMEX[i], LIMEX_TOLERANCE);
  }
}


/**
* @test GenericIntegratorTest - test Limex with DAE initialization
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are tested for
* an updated car example
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardAllSensDaeInitialization)
{
  IntegratorSSMetaDataPtr integratorSSMetaData;
  const int numInitials = 4;
  const int initialEsoIndices[numInitials] = {0, 1, 2, 3};
  const double initialValues[numInitials] = {2.0, 4.0, 3.0, 5.0};
  const double duration = 10.0;
  const int controlEsoIndex = 5; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 5;
  const double controlParams[numContrIntervals] = {1.0, 0.7, -0.43, 0.1, -1.0};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const int numConstraints = 0;
  const bool initialsWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const bool durationWithSensitivity = true;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_UPDATED_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
 
  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();
  const int numControlParameters = numContrIntervals;

  // sensitivities w.r.t. all initial values
  const int numInitialParameters = numInitials; // number of differential states

  const int numDurationParameters = 1; // one global final time

  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = LIMEX_INTEGRATION_TOLERANCE;
  initTol.absoluteTolerance = LIMEX_INTEGRATION_TOLERANCE;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());


  std::vector<double> finalSens;
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtDuration; 
  compControlSensAtDuration.resize(numStates*numControlParameters);
  std::vector<double> compInitiallSensAtDuration; 
  compInitiallSensAtDuration.resize(numStates*numInitialParameters);
  std::vector<double> compDurationSensAtDuration;
  compDurationSensAtDuration.resize(numStates*numDurationParameters);

  // compare State-Sensitivities with respect to controlparametrization at final Time
  //              dx_0(t_f)/dpi                       dx_1(t_f)/dpi                         dx_2(t_f)/dpi
  compControlSensAtDuration[0] = 17.68817955549596; compControlSensAtDuration[1] = 0.0;  compControlSensAtDuration[2] = 1.932120812247657;
  compControlSensAtDuration[5] = 13.79739502449891; compControlSensAtDuration[6] = 0.0;  compControlSensAtDuration[7] = 1.945769187961033;
  compControlSensAtDuration[10] = 9.899355104512301; compControlSensAtDuration[11] = 0.0; compControlSensAtDuration[12] = 1.962620341654356;
  compControlSensAtDuration[15] = 5.964321185693118; compControlSensAtDuration[16] = 0.0; compControlSensAtDuration[17] = 1.978671358215149;
  compControlSensAtDuration[20] = 1.995545440122822; compControlSensAtDuration[21] = 0.0; compControlSensAtDuration[22] = 1.993803561922133;

  //               dx_3(t_f)/dpi                       dx_4(t_f)/dpi
  compControlSensAtDuration[3] = 30.0;  compControlSensAtDuration[4] = 0.0;
  compControlSensAtDuration[8] = 30.0;  compControlSensAtDuration[9] = 0.0;
  compControlSensAtDuration[13] = 0.0;  compControlSensAtDuration[14] = 0.0;
  compControlSensAtDuration[18] = 30.0; compControlSensAtDuration[19] = 0.0;
  compControlSensAtDuration[23] = 0.0;  compControlSensAtDuration[24] = 0.0;

  for(int i=0; i<numStates*numControlParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[numInitialParameters*numStates + i], compControlSensAtDuration[i], LIMEX_TOLERANCE);
    //std::cout << setprecision(16)<< finalSens[numInitialParameters*numStates + i] << std::endl;
  }
 
  // compare State-Sensitivities with respect to initial values of differential states at final Time
  //              dx_0(t_f)/dx0i                             dx_1(t_f)/dx0i                           dx_2(t_f)/dx0i
  compInitiallSensAtDuration[0] = 1.0;                compInitiallSensAtDuration[1] = 0.0;   compInitiallSensAtDuration[2] = 0.0;
  compInitiallSensAtDuration[5] = 0.0;                compInitiallSensAtDuration[6] = 1.0;   compInitiallSensAtDuration[7] = 0.0;
  compInitiallSensAtDuration[10] = 9.820058548208893; compInitiallSensAtDuration[11] = 0.0;  compInitiallSensAtDuration[12] = 0.9635141039531602;
  compInitiallSensAtDuration[15] = 0.0;               compInitiallSensAtDuration[16] = 0.0;  compInitiallSensAtDuration[17] = 0.0;
 
  //               dx_3(t_f)/dx0i                       dx_4(t_f)/dx0i
  compInitiallSensAtDuration[3] = 0.0;   compInitiallSensAtDuration[4] = 0.0;
  compInitiallSensAtDuration[8] = 0.0;   compInitiallSensAtDuration[9] = 0.0;
  compInitiallSensAtDuration[13] = 0.0;  compInitiallSensAtDuration[14] = 0.0;
  compInitiallSensAtDuration[18] = 1.0;  compInitiallSensAtDuration[19] = 0.0;
 
  Logger::Ptr m_logger(new StandardLogger());
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "  ");
  for(int i=0; i<numStates*numInitialParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[i], compInitiallSensAtDuration[i], LIMEX_TOLERANCE);
    //std::cout << setprecision(16)<< finalSens[i] << std::endl;
  }

  // compare State-Sensitivities with respect to duration at final Time
  //              dx_0(t_f)/dtf
  compDurationSensAtDuration[0] = 7.284121630685332;
  //              dx_1(t_f)/dtf
  compDurationSensAtDuration[1] = 0.9999999999999982;
  //              dx_2(t_f)/dtf
  compDurationSensAtDuration[2] = 0.055761003509816137;
  //              dx_3(t_f)/dtf
  compDurationSensAtDuration[3] = 6.400000000000062;
  //              dx_4(t_f)/dtf
  compDurationSensAtDuration[4] = 0.0;
 
  m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "  ");
  for(int i=0; i<numStates*numDurationParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[(numInitialParameters + numControlParameters)*numStates + i],
                      compDurationSensAtDuration[i], LIMEX_TOLERANCE);
    //std::cout << setprecision(16)<< finalSens[(numInitialParameters + numControlParameters)*numStates + i] << std::endl;
  }
}




/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are tested for
* an updated car example
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardAllSens)
{
  IntegratorSSMetaDataPtr integratorSSMetaData;
  const int numInitials = 4;
  const int initialEsoIndices[4] = {0, 1, 2, 3};
  const double initialValues[4] = {2.0, 4.0, 3.0, 5.0};
  const double duration = 10.0;
  const int controlEsoIndex = 5; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 5;
  const double controlParams[5] = {1.0, 0.7, -0.43, 0.1, -1.0};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const int numConstraints = 0;
  const bool initialsWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const bool durationWithSensitivity = true;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_UPDATED_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  
  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  const int numStates = integratorMetaData->getGenericEso()->getNumStates();
  const int numControlParameters = numContrIntervals;

  // sensitivities w.r.t. all initial values
  const int numInitialParameters = numInitials; // number of differential states

  const int numDurationParameters = 1; // one global final time

  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = LIMEX_INTEGRATION_TOLERANCE;
  initTol.absoluteTolerance = LIMEX_INTEGRATION_TOLERANCE;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());
  std::vector<double> finalSens;
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtDuration; 
  compControlSensAtDuration.resize(numStates*numControlParameters);
  std::vector<double> compInitiallSensAtDuration; 
  compInitiallSensAtDuration.resize(numStates*numInitialParameters);
  std::vector<double> compDurationSensAtDuration;
  compDurationSensAtDuration.resize(numStates*numDurationParameters);

  // compare State-Sensitivities with respect to controlparametrization at final Time
  //              dx_0(t_f)/dpi                       dx_1(t_f)/dpi                         dx_2(t_f)/dpi
  compControlSensAtDuration[0] = 17.688179555495964; compControlSensAtDuration[1] = 0.0;  compControlSensAtDuration[2] = 1.9321208122476574;
  compControlSensAtDuration[5] = 13.797395024498908; compControlSensAtDuration[6] = 0.0;  compControlSensAtDuration[7] = 1.9457691879610333;
  compControlSensAtDuration[10] = 9.8993551045123009; compControlSensAtDuration[11] = 0.0; compControlSensAtDuration[12] = 1.9626203416543562;
  compControlSensAtDuration[15] = 5.9643211856931178; compControlSensAtDuration[16] = 0.0; compControlSensAtDuration[17] = 1.9786713582151489;
  compControlSensAtDuration[20] = 1.9955454401228219; compControlSensAtDuration[21] = 0.0; compControlSensAtDuration[22] = 1.9938035619221326;

  //               dx_3(t_f)/dpi                       dx_4(t_f)/dpi
  compControlSensAtDuration[3] = 30.000000000000000;  compControlSensAtDuration[4] = 0.0;
  compControlSensAtDuration[8] = 30.000000000000000;  compControlSensAtDuration[9] = 0.0;
  compControlSensAtDuration[13] = 0.0;                compControlSensAtDuration[14] = 0.0;
  compControlSensAtDuration[18] = 30.000000000000128; compControlSensAtDuration[19] = 0.0;
  compControlSensAtDuration[23] = 0.0;                compControlSensAtDuration[24] = 0.0;

  for(int i=0; i<numStates*numControlParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[numInitialParameters*numStates + i], compControlSensAtDuration[i], LIMEX_TOLERANCE);
  }

  // compare State-Sensitivities with respect to initial values of differential states at final Time
  //              dx_0(t_f)/dx0i                             dx_1(t_f)/dx0i                           dx_2(t_f)/dx0i
  compInitiallSensAtDuration[0] = 1.0;                compInitiallSensAtDuration[1] = 0.0;   compInitiallSensAtDuration[2] = 0.0;
  compInitiallSensAtDuration[5] = 0.0;                compInitiallSensAtDuration[6] = 1.0;   compInitiallSensAtDuration[7] = 0.0;
  compInitiallSensAtDuration[10] = 9.8200585482088929; compInitiallSensAtDuration[11] = 0.0;  compInitiallSensAtDuration[12] = 0.96351410395316017;
  compInitiallSensAtDuration[15] = 0.0;               compInitiallSensAtDuration[16] = 0.0;  compInitiallSensAtDuration[17] = 0.0;
  
  //               dx_3(t_f)/dx0i                       dx_4(t_f)/dx0i
  compInitiallSensAtDuration[3] = 0.0;   compInitiallSensAtDuration[4] = 0.0;
  compInitiallSensAtDuration[8] = 0.0;   compInitiallSensAtDuration[9] = 0.0;
  compInitiallSensAtDuration[13] = 0.0;  compInitiallSensAtDuration[14] = 0.0;
  compInitiallSensAtDuration[18] = 1.0;  compInitiallSensAtDuration[19] = 0.0;
  
  for(int i=0; i<numStates*numInitialParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[i], compInitiallSensAtDuration[i], LIMEX_TOLERANCE);
  }

  // compare State-Sensitivities with respect to duration at final Time
  //              dx_0(t_f)/dtf
  compDurationSensAtDuration[0] = 7.2841216306853322;
  //              dx_1(t_f)/dtf
  compDurationSensAtDuration[1] = 0.99999999999999822;
  //              dx_2(t_f)/dtf
  compDurationSensAtDuration[2] = 0.055761003509816137;
  //              dx_3(t_f)/dtf
  compDurationSensAtDuration[3] = 6.4000000000000616;
  //              dx_4(t_f)/dtf
  compDurationSensAtDuration[4] = 0.0;
  
  for(int i=0; i<numStates*numDurationParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[(numInitialParameters + numControlParameters)*numStates + i],
                      compDurationSensAtDuration[i], LIMEX_TOLERANCE);
  }
}

BOOST_AUTO_TEST_SUITE_END()
