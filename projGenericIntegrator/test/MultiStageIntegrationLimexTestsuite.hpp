/** 
* @file MultiStageIntegrationLimex.testsuite
* @brief test cases for GenericIntegrator.hpp with LIMEX integrator
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Tests of multi-stage integration using a simple artificial 3-stage   \n
* car example. The reference solution was calculated using JADE and \n
* NIXE. The description of the reference model can be found in the     \n
* MATLAB file GenericIntegrator\test\carErweitertMSTest.m              \n
* =====================================================================\n
*
* @author Fady Assassa
* @date 2.7.2012
*/

BOOST_AUTO_TEST_SUITE(TestGenericIntegratorLIMEXMultiStage)

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrder-
*                               ForwardHandler in case of a multistageProblem consisting of 3 
*                               stages of the updated car example
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are
* tested for an updated car example
* The sensitivities for the initials are only tested for the first stage, since no data is available
*/
BOOST_AUTO_TEST_CASE(TestMultiStageFirstOrderForwardAllSens)
{
  IntegratorMultiStageMetaDataDummy::Ptr integratorMSMetaDataDummy;
  integratorMSMetaDataDummy = createMSExampleForZerothAndFirstOrder(CAR_JADE_UPDATED_MODEL_NAME);
  IntegratorMetaData::Ptr integratorMetaData(integratorMSMetaDataDummy);

  const int numStates = 5;
  const int numControlParameters_1 = 5; // equals numIntervals
  const int numInitialParameters_1 = 4; // number of differential states
  const int numControlParameters_2 = 2;
  const int numDurationParameters_2 = 1;
  const int numControlParameters_3 = 4;
  const int numDurationParameters_3 = 1;

  // sensitivity w.r.t. duration
  
  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = LIMEX_INTEGRATION_TOLERANCE;
  initTol.absoluteTolerance = LIMEX_INTEGRATION_TOLERANCE;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());
  
  std::vector<double> sensAtdurations;
  std::vector<double> sensMapped;

  // The following call gives us all the individual stage sensitivities at the final times of each
  // stage. At each final time only the sensitivities with respect to the parameters defined
  // in the corresponding stage are accounted for. No entries dx_i/dp_j with stage i =/= stage j exist
  integratorMSMetaDataDummy->getSensAtStageDurations(sensAtdurations);
  // In contrast to the previous call, this call gives us all the mapped sensitivities but only at the
  // overall final time and not at intermediate durations as tf_2
  integratorMSMetaDataDummy->getCurrentSensitivities(sensMapped);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  //==================================
  // Comparison at the end of stage 1
  //==================================
  std::vector<double> compControlSensAtDuration_1;
  compControlSensAtDuration_1.resize(numStates*numControlParameters_1);
  std::vector<double> compInitiallSensAtDuration_1; 
  compInitiallSensAtDuration_1.resize(numStates*numInitialParameters_1);

  // compare State-Sensitivities with respect to controlparametrization at final time tf_1
  // name convention e.g.: dx_0(tf_1)/dpi_1 => Sensitivity of state x_0 at time tf_1 
  //                       with respect to parameter pi_1, where i is the index of the parameter
  //              dx_0(tf_1)/dpi_1                          dx_1(tf_1)/dpi_1
  compControlSensAtDuration_1[0] = 17.688179555495964; compControlSensAtDuration_1[1] = 0.0;
  compControlSensAtDuration_1[5] = 13.797395024498908; compControlSensAtDuration_1[6] = 0.0;
  compControlSensAtDuration_1[10] = 9.8993551045123009; compControlSensAtDuration_1[11] = 0.0;
  compControlSensAtDuration_1[15] = 5.9643211856931178; compControlSensAtDuration_1[16] = 0.0;
  compControlSensAtDuration_1[20] = 1.9955454401228219; compControlSensAtDuration_1[21] = 0.0;

  //              dx_2(tf_1)/dpi_1                           dx_3(tf_1)/dpi_1
  compControlSensAtDuration_1[2] = 1.9321208122476574;  compControlSensAtDuration_1[3] = 30.0000000000000;
  compControlSensAtDuration_1[7] = 1.9457691879610333;  compControlSensAtDuration_1[8] = 30.0000000000000;
  compControlSensAtDuration_1[12] = 1.9626203416543562; compControlSensAtDuration_1[13] = 0.0;
  compControlSensAtDuration_1[17] = 1.9786713582151489; compControlSensAtDuration_1[18] = 30.000000000000004;
  compControlSensAtDuration_1[22] = 1.9938035619221326; compControlSensAtDuration_1[23] = 0.0;

  //              dx_4(tf_1)/dpi_1
  compControlSensAtDuration_1[4] = 0.0;
  compControlSensAtDuration_1[9] = 0.0;
  compControlSensAtDuration_1[14] = 0.0;
  compControlSensAtDuration_1[19] = 0.0;
  compControlSensAtDuration_1[24] = 0.0;

  for(int i=0; i<numStates*numControlParameters_1; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[numInitialParameters_1 * numStates + i],
                      compControlSensAtDuration_1[i], LIMEX_TOLERANCE);
  }

  // compare State-Sensitivities with respect to initial values of differential states at final Time tf_1
  //              dx_0(tf_1)/dx0i                             dx_1(tf_1)/dx0i
  compInitiallSensAtDuration_1[0] = 1.0;                  compInitiallSensAtDuration_1[1] = 0.0;
  compInitiallSensAtDuration_1[5] = 0.0;                  compInitiallSensAtDuration_1[6] = 1.0;
  compInitiallSensAtDuration_1[10] = 9.8200585482088929;   compInitiallSensAtDuration_1[11] = 0.0;
  compInitiallSensAtDuration_1[15] = 0.0;                 compInitiallSensAtDuration_1[16] = 0.0;
  
  //              dx_2(tf_1)/dx0i                             dx_3(tf_1)/dx0i
  compInitiallSensAtDuration_1[2] = 0.0;                  compInitiallSensAtDuration_1[3] = 0.0;
  compInitiallSensAtDuration_1[7] = 0.0;                  compInitiallSensAtDuration_1[8] = 0.0;
  compInitiallSensAtDuration_1[12] = 0.96351410395316017;   compInitiallSensAtDuration_1[13] = 0.0;
  compInitiallSensAtDuration_1[17] = 0.0;                 compInitiallSensAtDuration_1[18] = 1.0;
  
  //              dx_4(tf_1)/dx0i
  compInitiallSensAtDuration_1[4] = 0.0;
  compInitiallSensAtDuration_1[9] = 0.0;
  compInitiallSensAtDuration_1[14] = 0.0;
  compInitiallSensAtDuration_1[19] = 0.0;

  for(int i=0; i<numStates*numInitialParameters_1; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[i], compInitiallSensAtDuration_1[i], LIMEX_TOLERANCE);
  }

  //==================================
  // Comparison at the end of stage 2
  //==================================
  std::vector<double> compControlSensAtDuration_2;
  compControlSensAtDuration_2.resize(numStates*numControlParameters_2);
  std::vector<double> compFinalSensAtDuration_2; 
  compFinalSensAtDuration_2.resize(numStates*numDurationParameters_2);

  // compare State-Sensitivities with respect to controlparametrization at final time tf_2
  //              dx_0(tf_2)/dpi_2                       dx_1(tf_2)/dpi_2
  compControlSensAtDuration_2[0] = 9.2948629174551982; compControlSensAtDuration_2[1] = 0.0;
  compControlSensAtDuration_2[5] = 3.1091330411637190; compControlSensAtDuration_2[6] = 0.0;

  //              dx_2(tf_2)/dpi_2                       dx_3(tf_2)/dpi_2
  compControlSensAtDuration_2[2] = 2.4507357783652983; compControlSensAtDuration_2[3] = 37.500000000000;
  compControlSensAtDuration_2[7] = 2.4790386150647210; compControlSensAtDuration_2[8] = 37.500000000000099;

  //              dx_4(tf_2)/dpi_2
  compControlSensAtDuration_2[4] = 0.0;
  compControlSensAtDuration_2[9] = 15.000000000000000;

  for(int i=0; i<numStates*numControlParameters_2; i++){
    if(0.0!=sensAtdurations[(numInitialParameters_1 + numControlParameters_1) * numStates + i] && 0.0!=compControlSensAtDuration_2[i]){
      BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1 + numControlParameters_1) * numStates + i], compControlSensAtDuration_2[i], LIMEX_TOLERANCE);
    }else{
      // if the value is exactley zero, the relative check fails if the computed value is not exactley zero. e.g. 1e-16
      BOOST_CHECK_SMALL(compControlSensAtDuration_2[i], LIMEX_TOLERANCE);
    }
  }

  // compare State-Sensitivities with respect to final time parameter tf_2
  //              dx_0(tf_2)/dtf_2                          dx_1(tf_2)/dtf_2
  compFinalSensAtDuration_2[0] = 9.5892028009691348; compFinalSensAtDuration_2[1] = 1.0000000000000016;
  //              dx_2(tf_2)/dtf_2                          dx_3(tf_2)/dtf_2
  compFinalSensAtDuration_2[2] = 1.4748693670497817; compFinalSensAtDuration_2[3] = 23.650000000000013;
  //              dx_4(tf_2)/dtf_2
  compFinalSensAtDuration_2[4] = 0.0;

  for(int i=0; i<numStates*numDurationParameters_2; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1 + numControlParameters_1
                                       + numControlParameters_2)*numStates + i],
                                       compFinalSensAtDuration_2[i], LIMEX_TOLERANCE);
  }
  //==================================
  // Comparison at the end of stage 3
  //==================================
  std::vector<double> compStatesAtDuration_3;
  compStatesAtDuration_3.resize(numStates);
  std::vector<double> compControlSensAtDuration_3;
  compControlSensAtDuration_3.resize(numStates*(numControlParameters_1+numControlParameters_2+numControlParameters_3));
  std::vector<double> compInitiallSensAtDuration_3; 
  compInitiallSensAtDuration_3.resize(numStates*numInitialParameters_1);
  std::vector<double> compFinalSensAtDuration_3; 
  compFinalSensAtDuration_3.resize(numStates*(numDurationParameters_2+numDurationParameters_3));

  // compare States at final time tf_3
  //              x_i(tf_3)
  compStatesAtDuration_3[0] = 233.75377054863478; 
  compStatesAtDuration_3[1] = 31.0000000000003;
  compStatesAtDuration_3[2] = 11.742072188786370;
  compStatesAtDuration_3[3] = 248.7499999999975;
  compStatesAtDuration_3[4] = 1.0;

  utils::Array<double> finalStates_3(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates_3.getSize(), finalStates_3.getData());

  for(int i=0; i<numStates; i++){
    BOOST_CHECK_CLOSE(finalStates_3[i], compStatesAtDuration_3[i], LIMEX_TOLERANCE);
  }

  // compare State-Sensitivities with respect to controlparametrization at final time tf_3
  //              dx_0(tf_3)/dpi_1                       dx_1(tf_3)/dpi_1
  compControlSensAtDuration_3[0] = 48.775169970593275;  compControlSensAtDuration_3[1] = 0.0;
  compControlSensAtDuration_3[5] = 45.103981917359810;  compControlSensAtDuration_3[6] = 0.0;
  compControlSensAtDuration_3[10] = 41.477069791260078; compControlSensAtDuration_3[11] = 0.0;
  compControlSensAtDuration_3[15] = 37.800289805018480; compControlSensAtDuration_3[16] = 0.0;
  compControlSensAtDuration_3[20] = 34.074984689553702; compControlSensAtDuration_3[21] = 0.0;
  //              dx_0(tf_3)/dpi_2                       dx_1(tf_3)/dpi_2
  compControlSensAtDuration_3[25] = 37.249204605212739; compControlSensAtDuration_3[26] = 0.0;
  compControlSensAtDuration_3[30] = 31.386311313269736; compControlSensAtDuration_3[31] = 0.0;
  //              dx_0(tf_3)/dpi_3                       dx_1(tf_3)/dpi_3
  compControlSensAtDuration_3[35] = 30.098810119287769; compControlSensAtDuration_3[36] = 0.0;
  compControlSensAtDuration_3[40] = 21.736968736013587; compControlSensAtDuration_3[41] = 0.0;
  compControlSensAtDuration_3[45] = 13.203254809454027; compControlSensAtDuration_3[46] = 0.0;
  compControlSensAtDuration_3[50] = 4.4587201452347642;  compControlSensAtDuration_3[51] = 0.0;

  //              dx_2(tf_3)/dpi_1                       dx_3(tf_3)/dpi_1
  compControlSensAtDuration_3[2] = 1.6971047343281551;   compControlSensAtDuration_3[3] = 30.000000000000000;
  compControlSensAtDuration_3[7] = 1.7090929717573309;   compControlSensAtDuration_3[8] = 30.000000000000000;
  compControlSensAtDuration_3[12] = 1.7238944130184319;  compControlSensAtDuration_3[13] = 0.0;
  compControlSensAtDuration_3[17] = 1.7379930428884811;  compControlSensAtDuration_3[18] = 30.000000000000128;
  compControlSensAtDuration_3[22] = 1.7512846209249746;  compControlSensAtDuration_3[23] = 0.0;
  //              dx_2(tf_3)/dpi_2                       dx_3(tf_3)/dpi_2
  compControlSensAtDuration_3[27] = 2.2046015257938167;  compControlSensAtDuration_3[28] = 37.500000000000000;
  compControlSensAtDuration_3[32] = 2.2300618293984158;  compControlSensAtDuration_3[33] = 37.500000000000078;
  //              dx_2(tf_3)/dpi_3                       dx_3(tf_3)/dpi_3
  compControlSensAtDuration_3[37] = 2.7314890379900367;  compControlSensAtDuration_3[38] = 44.999999999999893;
  compControlSensAtDuration_3[42] = 2.7997573981921882;  compControlSensAtDuration_3[43] = 45.000000000000028;
  compControlSensAtDuration_3[47] = 2.8761166072205393;  compControlSensAtDuration_3[48] = 45.000000000000107;
  compControlSensAtDuration_3[52] = 2.9599739365903091;  compControlSensAtDuration_3[53] = 0.0;

  //              dx_4(tf_3)/dpi_1
  compControlSensAtDuration_3[4] = 0.0;
  compControlSensAtDuration_3[9] = 0.0;
  compControlSensAtDuration_3[14] = 0.0;
  compControlSensAtDuration_3[19] = 0.0;
  compControlSensAtDuration_3[24] = 0.0;
  //              dx_4(tf_3)/dpi_2
  compControlSensAtDuration_3[29] = 0.0;
  compControlSensAtDuration_3[34] = 0.0;
  //              dx_4(tf_3)/dpi_3
  compControlSensAtDuration_3[39] = 0.0;
  compControlSensAtDuration_3[44] = 0.0;
  compControlSensAtDuration_3[49] = 0.0;
  compControlSensAtDuration_3[54] = 0.0;

  for(int i=0; i<numStates*numControlParameters_1; i++){
    BOOST_CHECK_CLOSE(sensMapped[numInitialParameters_1*numStates + i],
                      compControlSensAtDuration_3[i],LIMEX_TOLERANCE);
  }

  for(int i=0; i<numStates*numControlParameters_2; i++){
    BOOST_CHECK_CLOSE(sensMapped[(numInitialParameters_1+numControlParameters_1)*numStates + i],
                      compControlSensAtDuration_3[numStates*numControlParameters_1+i],LIMEX_TOLERANCE);
  }

  for(int i=0; i<numStates*numControlParameters_3; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1+numControlParameters_1+numDurationParameters_2
                      +numControlParameters_2)*numStates + i],
                      compControlSensAtDuration_3[(numControlParameters_1+numControlParameters_2)
                      *numStates + i],LIMEX_TOLERANCE);
  }
  
  // compare State-Sensitivities with respect to initial values of differential states at final time tf_3
  //              dx_0(tf_3)/dx0i_1                        dx_1(tf_3)/dx0i_1
  compInitiallSensAtDuration_3[0] = 1.0;                  compInitiallSensAtDuration_3[1] = 0.0;
  compInitiallSensAtDuration_3[5] = 0.0;                  compInitiallSensAtDuration_3[6] = 1.0;
  compInitiallSensAtDuration_3[10] = 25.322584852223365;  compInitiallSensAtDuration_3[11] = 0.0;
  compInitiallSensAtDuration_3[15] = 0.0;                 compInitiallSensAtDuration_3[16] = 0.0;
  
  //              dx_2(tf_3)/dx0i_1                       dx_3(tf_3)/dx0i_1
  compInitiallSensAtDuration_3[2] = 0.0;                 compInitiallSensAtDuration_3[3] = 0.0;
  compInitiallSensAtDuration_3[7] = 0.0;                 compInitiallSensAtDuration_3[8] = 0.0;
  compInitiallSensAtDuration_3[12] = 0.84631578783557049;  compInitiallSensAtDuration_3[13] = 0.0;
  compInitiallSensAtDuration_3[17] = 0.0;                compInitiallSensAtDuration_3[18] = 1.0;
  
  //              dx_4(tf_3)/dx0i_1
  compInitiallSensAtDuration_3[4] = 0.0;
  compInitiallSensAtDuration_3[9] = 0.0;
  compInitiallSensAtDuration_3[14] = 0.0;
  compInitiallSensAtDuration_3[19] = 0.0;

  for(int i=0; i<numStates*numInitialParameters_1; i++){
    BOOST_CHECK_CLOSE(sensMapped[i], compInitiallSensAtDuration_3[i], LIMEX_TOLERANCE);
  }

  // compare State-Sensitivities with respect to final time parameter tf_2 (!)
  //              dx_0(tf_3)/dtf_2                          dx_1(tf_3)/dtf_2
  compFinalSensAtDuration_3[0] = 26.412314698742641;     compFinalSensAtDuration_3[1] = 1.000000000000001;
  //              dx_2(tf_3)/dtf_2                          dx_3(tf_3)/dtf_2
  compFinalSensAtDuration_3[2] = 1.3267441091154031;      compFinalSensAtDuration_3[3] = 23.650000000000013;
  //              dx_4(tf_3)/dtf_2
  compFinalSensAtDuration_3[4] = 0.0;

  // compare State-Sensitivities with respect to final time parameter tf_3
  //              dx_0(tf_3)/dtf_3                          dx_1(tf_3)/dtf_3
  compFinalSensAtDuration_3[5] = 13.356285481231893; compFinalSensAtDuration_3[6] = 1.000000000000007;
  //              dx_2(tf_3)/dtf_3                          dx_3(tf_3)/dtf_3
  compFinalSensAtDuration_3[7] = 0.042879935547486925;  compFinalSensAtDuration_3[8] = 5.1250000000000053;
  //              dx_4(tf_3)/dtf_3
  compFinalSensAtDuration_3[9] = -6.9388939039072284e-018;

  for(int i=0; i<numStates*numDurationParameters_2; i++){
    BOOST_CHECK_CLOSE(sensMapped[(numInitialParameters_1 + numControlParameters_1
                                        + numControlParameters_2) * numStates + i],
                                          compFinalSensAtDuration_3[i], LIMEX_TOLERANCE);
  }

  for(int i=0; i<numStates*numDurationParameters_3; i++){
    if( 0.0!= sensAtdurations[(numInitialParameters_1 + numControlParameters_1 +
      numControlParameters_2  +numDurationParameters_2 +
      numControlParameters_3) * numStates + i]
    && 
      0.0 != compFinalSensAtDuration_3[numStates * numDurationParameters_2 + i]){
    BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1 + numControlParameters_1 +
      numControlParameters_2  +numDurationParameters_2 +
      numControlParameters_3) * numStates + i],
      compFinalSensAtDuration_3[numStates * numDurationParameters_2 + i], LIMEX_TOLERANCE);
    }else{
      BOOST_CHECK_SMALL(compFinalSensAtDuration_3[numStates * numDurationParameters_2 + i], LIMEX_TOLERANCE);
    }
  }
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as MultiStage LIMEX
*                               consisting of 3 grid for the controls which corresponds to the 3 multi
*                               stages of the updated car example. we compare the values of nixe and 
*                               limex to see that the behave in the same way.
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are
* tested for an updated car example
* The sensitivities for the initials are only tested for the first stage, since no data is available
*/
BOOST_AUTO_TEST_CASE(TestMultiGridFirstOrderForwardAllSens)
{
  IntegratorMetaData::Ptr integratorMetaData = createMultiGridExampleForZerothAndFirstOrder(CAR_JADE_UPDATED_MODEL_NAME);

  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  ForwardIntegratorNixe fof(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(fof.solve());
  
  IntegratorLimex::InitialTolerance initTol;
  initTol.relativeTolerance = options.relTolScalar;
  initTol.absoluteTolerance = options.absTolScalar;
  IntegratorLimex limex(integratorMetaData, initTol);
  BOOST_CHECK_NO_THROW(limex.solve());

  std::vector<double> sensAtdurationsNIXE;
  integratorMetaData->getCurrentSensitivities(sensAtdurationsNIXE);
  
  std::vector<double> sensAtdurationsLIMEX;
  integratorMetaData->getCurrentSensitivities(sensAtdurationsLIMEX);

  BOOST_REQUIRE(sensAtdurationsLIMEX.size() == sensAtdurationsNIXE.size());
  // the last value is not checked, because the tolerance has to be really small although
  // it is obvious that the value is almost zero. we checked it separately.
  for(unsigned i=0; i<sensAtdurationsLIMEX.size()-1; i++){
    BOOST_CHECK_CLOSE(sensAtdurationsLIMEX[i], sensAtdurationsNIXE[i], 1e-3);
  }
  BOOST_REQUIRE(sensAtdurationsLIMEX.back() > -1e-10 && sensAtdurationsLIMEX.back() < 1e-10);
}
BOOST_AUTO_TEST_SUITE_END()
