/**
* @file MetaDaeInterface2ndOrder.testsuite
* @brief test cases for MetaDaeInterface2ndOrder.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Test of all functions of MetaDaeInterface2ndOrder which is a child   \n
* class of Nixe::DaeInterface                                          \n
* =====================================================================\n
*
* @author Moritz Schmitz
* @date 18.06.2012
*/

BOOST_AUTO_TEST_SUITE(TestMetaDaeInterface2ndOrder)

/**
* @brief Testing the reverse evaluation
*/
BOOST_AUTO_TEST_CASE(TestReverseEvaluation)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 3;
  const int initialEsoIndices[3] = {0, 1, 2};
  const double initialValues[3] = {0.5, 0.05, 0.0};
  const double duration = 21.3;
  const int controlEsoIndex = 12; // corresponds to parameter 'K1' in mof-file
  const int numControlIntervals = 1;
  const double controlParams[1] = {0.5};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const int numConstraints = 0;
  const bool initialsWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const bool durationWithSensitivity = true;

  FactoryInput::IntegratorMetaDataInput imdInput;
  imdInput = createInputForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numControlIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  //********** create IntegratorSingleStageMetaData ***********

  IntegratorMetaDataFactory imdFactory;
  IntegratorSSMetaData2ndOrder::Ptr integratorSSMD2ndOrder(imdFactory.createIntegratorSSMetaData2ndOrder(imdInput));

  integratorSSMD2ndOrder->initializeForFirstIntegration();
  integratorSSMD2ndOrder->initializeForNewIntegrationStep();

  IntegratorSSMetaData2ndOrder::Ptr integratorSSMD2ndOrderPtr(new IntegratorSSMetaData2ndOrder(*integratorSSMD2ndOrder));
  MetaDaeInterface2ndOrder::Ptr metaDae2ndOrderPtr(new MetaDaeInterface2ndOrder(integratorSSMD2ndOrderPtr));
  metaDae2ndOrderPtr->initializeForNewIntegration(integratorSSMD2ndOrderPtr->getStepStartTime(), integratorSSMD2ndOrderPtr->getStepEndTime());
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaDae2ndOrderPtr->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numAdjoints = problemInfo->NumAdjoints();
  const int numDers = problemInfo->NumDers();
  const int numCurSensPar = problemInfo->NumSens();
  double states[] = {0.6, 0.06, 1.0, 1.5, 3.05, 0.23, 3.29, 0.063};
  double parameters[] = {1, 0.5, 0.05, 0, 0.5, 0.05, 0.2, 0.01, 0.0625};
  double adjoints[] = {0.12, -0.02, 4.6, 0.005, 1.3, -0.23, 2.84, 0.061,
                       1.71, 4.12, 3.18, 2.45, -7.65, 2.78, 2.5, 3.2,
                       0.2, 3.12, 7.41, 9.1, -1.54, 3.12, 4.98, -1.4,
                       0.423, -2.45, -6.34, 5.01, 3.16, 1.617, 8.01, 3.54,
                       -5.89, 4.7, -3.51, 7.7, 2.13, -2.7, 4.78, 1.9,
                       3.6, 7.3, 6.12, -8.09, 1.7, 0.25, 0.16, -1.23};
  double sens[] = {0.2, 3.1, 5.4, 9.1, -1.54, -3.52, 4.98, -1.4,
                   1.78, 7.6, 3.7, 5.4, -2.44, 2.7, 8.14, 2.4,
                   5.89, 4.7, -3.51, 8.7, 4.13, -2.7, 4.78, 1.9,
                   3.6, 7.3, 7.12, 8.09, 1.4, 0.45, 0.16, 9.23,
                   0.43, -2.45, -6.34, 5.1, 3.6, 1.67, 8.01, 3.54};
  utils::Array<double> rhsAdjoints(numAdjoints*numStates,0.0);
  utils::Array<double> rhsDers(numDers,0.0);
  metaDae2ndOrderPtr->EvalReverse(problemInfo->StartTime(), states, parameters, sens, adjoints,
                                  rhsAdjoints.getData(), rhsDers.getData(), *problemInfo);

  /***** compare rhsAdjoints 1st order *****/

  const double rhsAdjointsComp[] = {1.605700500000000e+02, 1.601979390000000e+02,
                              1.585617795000000e+02,-94.891500000000000,
                              -71.142000000000000, 1.485623880000000e+03,
                              60.492000000000000, 1.299300000000000};

  for(int i=0; i<numStates; i++){
    BOOST_CHECK_CLOSE(rhsAdjoints[i], rhsAdjointsComp[i], THRESHOLD);
  }

  /***** compare rhsDers 1st order *****/

  double rhsDersComp[] = {0.0, 0.0, 0.0, -0.063900000000000, -22.029058099999993};

  for(int i=0; i<numCurSensPar; i++){
    BOOST_CHECK_CLOSE(rhsDers[i], rhsDersComp[i], THRESHOLD);
  }

  /***** compare rhsAdjoints 2nd order *****/

  const double rhsAdjoints2ndComp[] = {-1.984831980000000e+003, -1.981793535000000e+003, -2.096396587500000e+003,
                                 -0.066882000000000e+003, -0.055167000000000e+003,  8.185112880000000e+003,
                                  0.053250000000000e+003,  0.068160000000000e+003, -2.277991548000000e+003,
                                 -2.275247149500000e+003, -2.132095920000000e+003, -0.026199000000000e+003,
                                 -0.057723000000000e+003, -2.546389440000000e+003,  0.106074000000000e+003,
                                 -0.029820000000000e+003, -1.214821218000000e+003, -1.193155656750000e+003,
                                 -1.271743231500000e+003,  0.302949900000000e+003,  0.097980000000000e+003,
                                  9.464855220000001e+003,  0.170613000000000e+003,  0.075402000000000e+003,
                                  1.789067940000000e+003,  1.795143978000000e+003,  1.790170960500000e+003,
                                  0.013206000000000e+003,  0.320352000000000e+003,  1.580102160000000e+003,
                                  0.101814000000000e+003,  0.040470000000000e+003, -0.077053598000000e+003,
                                 -0.142843870500000e+003, -0.098723558250000e+003, -0.385938000000000e+003,
                                  0.213494000000000e+003, -3.073646760000000e+003,  0.006248000000000e+003,
                                 -0.026138000000000e+003};

  for(unsigned i=numStates; i<rhsAdjoints.getSize(); i++){
    BOOST_CHECK_CLOSE(rhsAdjoints[i], rhsAdjoints2ndComp[i-numStates], THRESHOLD);
  }

  /***** compare rhsDers 2nd order *****/

  const double rhsDers2ndComp[] = {0.000000000000000e+002,  0.000000000000000e+002,  0.000000000000000e+002,
                            -0.313323000000000e+002, -3.846694919999999e+002,  0.000000000000000e+002,
                             0.000000000000000e+002,  0.000000000000000e+002, -1.164875700000000e+002,
                             0.820701223000000e+002,  0.000000000000000e+002,  0.000000000000000e+002,
                             0.000000000000000e+002, -0.646550850000000e+002, -2.804166266499999e+002,
                             0.000000000000000e+002,  0.000000000000000e+002,  0.000000000000000e+002,
                            -0.987894000000000e+002,  2.864132942000000e+002,  0.000000000000000e+002,
                             0.000000000000000e+002,  0.000000000000000e+002,  1.033414050000000e+002,
                             0.355219554000000e+002};

  for(unsigned i=numCurSensPar; i<rhsDers.getSize(); i++){
    BOOST_CHECK_CLOSE(rhsDers[i], rhsDers2ndComp[i-numCurSensPar], THRESHOLD);
  }
}


BOOST_AUTO_TEST_SUITE_END()
