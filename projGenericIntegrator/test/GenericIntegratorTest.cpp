/**
* @file GenericIntegratorTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericIntegratorTest - Part of DyOS                                 \n
* =====================================================================\n
* Test of class GenericIntegrator (Single- and MultiStage)             \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 21.10.2011
*/

#include "GenericIntegrator.hpp"
#include "IntegratorMultiStageMetaDataDummy.hpp"
#include "MetaSSEDaeInterface.hpp"
#include "MetaDaeInterface2ndOrder.hpp"
#include "ForwardIntegratorNixe.hpp"
#include "ReverseIntegratorNixe.hpp"
#include "Rev2ndOrdIntegratorNixe.hpp"
#include "JadeGenericEso.hpp"
#include "Mapping.hpp"
#include "Input.hpp"
#include "MetaDataFactories.hpp"
#include "SolverConfig.hpp"
#ifdef BUILD_LIMEX //HAVE_LIMEX
#include "IntegratorLimex.hpp"
#endif


/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE GenericIntegrator
/**
 * @def BOOST_TEST_DYN_LINK
 * @brief if on unix, link dynamically to boost_unit_test - default is static
 */
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>
#include <vector>

#include "Array.hpp"

// THE INITIAL VALUES OF THE JADE MODEL ARE ALL ZERO
/**
* @def CAR_JADE_MODEL_NAME
* @brief JADE model name used for one test
*/
#define CAR_JADE_MODEL_NAME "carFreeFinalTime"
/**
* @def RAWLINGBATCH_JADE_MODEL_NAME
* @brief JADE model name used for one test
*/
#define RAWLINGBATCH_JADE_MODEL_NAME "rawlingbatch"
/**
* @def BAYER_JADE_MODEL_NAME
* @brief JADE model name used for one test
*/
#define BAYER_JADE_MODEL_NAME "bayer"
/**
* @def CAR_JADE_UPDATED_MODEL_NAME
* @brief JADE model name used for sensitivity test
*/
#define CAR_JADE_UPDATED_MODEL_NAME "carErweitert"
/**
* @def PLANE_JADE_MODEL_NAME
* @brief JADE model name used for Idas root test
*/
#define PLANE_JADE_MODEL_NAME "airplane_d"
/**
* @def THRESHOLD
* @brief general threshold for BOOST_*_CLOSE macros
*/
#define THRESHOLD 1e-6

#define LIMEX_TOLERANCE 1e-5
#define LIMEX_INTEGRATION_TOLERANCE 1e-6

//! @brief a map of corresponding esoIndices
typedef std::map <unsigned, unsigned> iiMap;


using namespace std;

/**
* @brief create an Input struct for Integration test
*
* @param model name of model created by JADE-script
* @param numInitials number of initial parameters
* @param initialEsoIndices eso indices of initial parameters
* @param initialValues values of initial parameters
* @param initialsWithSensitivitiy true if initial sens are wanted
* @param duration value of global duration parameter
* @param durationWithSensitivity true if final time sens are wanted
* @param controlEsoIndex eso index of the control input (only one control created)
* @param numControlIntervals number of intervals on the control grid (only one grid created)
* @param controlParams values of the control parameters (PWC)
* @param controlLB lower bound of the control
* @param controlUB upper bound of the control
* @param controlWithSensitivity true if control sens are wanted
* @param numConstraints number of contsraints
* @param constraintEsoIndices eso indices of constrainted states
* @param constraintTimes time points of the constraints (each is a state point consrtaint)
* @param LagrMultipliers Lagrange multipliers of the constraints
* @return created input struct
*/
FactoryInput::IntegratorMetaDataInput createInputForIntegration(const std::string &model,
                                                    const int numInitials,
                                                    const int *initialEsoIndices,
                                                    const double *initialValues,
                                                    const bool initialsWithSensitivity,
                                                    const double duration,
                                                    const bool durationWithSensitivity,
                                                    const int controlEsoIndex,
                                                    const int numControlIntervals,
                                                    const double *controlParams,
                                                    const double controlLB,
                                                    const double controlUB,
                                                    const bool controlWithSensitivity,
                                                    const int numConstraints,
                                                    const int *constraintEsoIndices,
                                                    const double *constraintTimes,
                                                    const double *LagrMultipliers)
{
  FactoryInput::EsoInput eso;
  eso.model = model;
  eso.type = FactoryInput::EsoInput::JADE;


  FactoryInput::IntegratorMetaDataInput imdInput;
  GenericEsoFactory genEsoFac;


  imdInput.esoPtr = genEsoFac.create(eso);

  //********** set initial value parameters ***********

  imdInput.initials.resize(numInitials);
  for(int i=0; i<numInitials; i++){
    imdInput.initials[i].esoIndex = initialEsoIndices[i];
    imdInput.initials[i].value = initialValues[i];
    imdInput.initials[i].type = FactoryInput::ParameterInput::INITIAL;
    if(initialsWithSensitivity){
      imdInput.initials[i].sensType = FactoryInput::ParameterInput::FULL;
    }
    else{
      imdInput.initials[i].sensType = FactoryInput::ParameterInput::NO;
    }
  }

  //********** set final time ***********

  imdInput.duration.type = FactoryInput::ParameterInput::DURATION;
  if(durationWithSensitivity){
    imdInput.duration.sensType = FactoryInput::ParameterInput::FULL;
  }
  else{
    imdInput.duration.sensType = FactoryInput::ParameterInput::NO;
  }
  imdInput.duration.value = duration;
  imdInput.duration.lowerBound = 1e-3;
  imdInput.duration.upperBound = 1e10;

  //********** set control ***********

  imdInput.controls.resize(1);
  imdInput.controls[0].type = FactoryInput::ParameterInput::CONTROL;
  if(controlWithSensitivity){
    imdInput.controls[0].sensType = FactoryInput::ParameterInput::FULL;
  }
  else{
    imdInput.controls[0].sensType = FactoryInput::ParameterInput::NO;
  }
  imdInput.controls[0].esoIndex = controlEsoIndex;
  imdInput.controls[0].grids.resize(1);
  imdInput.controls[0].grids[0].approxType = PieceWiseConstant;
  imdInput.controls[0].grids[0].duration = duration;
  imdInput.controls[0].grids[0].hasFreeDuration = durationWithSensitivity;
  imdInput.controls[0].grids[0].timePoints.resize(numControlIntervals);
  imdInput.controls[0].grids[0].values.resize(numControlIntervals);
  for(int i=0; i<numControlIntervals; i++){
    imdInput.controls[0].grids[0].timePoints[i] = 0.0+(double)i/(double)numControlIntervals;
  }
  imdInput.controls[0].grids[0].timePoints.push_back(1.0);
  for(int i=0; i<numControlIntervals; i++){
    imdInput.controls[0].grids[0].values[i] = controlParams[i];
  }
  imdInput.controls[0].lowerBound = controlLB;
  imdInput.controls[0].upperBound = controlUB;

  //********** set additional grid due to constraints ***********

  imdInput.userGrid.resize(numConstraints);
  for(int i=0; i<numConstraints; i++){
    imdInput.userGrid[i] = constraintTimes[i];
  }
  return imdInput;
}

/**
* @brief Creates an MetaSingleStageEso-example for integrator test
*
* @param model name of model created by JADE-script
* @param numInitials number of initial sensitivity parameters
* @param initialEsoIndices ESO indices of states with initial sens
* @param initialValues corresponding initial values to initialEsoIndices
* @param initialsWithSensitivity true if initial sens are wanted
* @param duration global final time for integration
* @param durationWithSensitivity true if final time sens are wanted
* @param controlEsoIndex ESO-index of the parameter in the model
* @param numControlIntervals number of parametrization intervals
* @param controlParams PWC parametrization of the control
* @param controlLB lower bound of the control
* @param controlUB upper bound of the control
* @param controlWithSensitivity true if control sens are wanted
* @param numConstraints number of point constraints
* @param constraintEsoIndices ESO indices of constrainted states
* @param constraintTimes times of point constraints
* @param LagrMultipliers Lagrange multipliers of point constraints
* @return pointer to created IntegratorMetaData object
*/
IntegratorSingleStageMetaData::Ptr createExampleForIntegration(const std::string &model,
                                                    const int numInitials,
                                                    const int *initialEsoIndices,
                                                    const double *initialValues,
                                                    const bool initialsWithSensitivity,
                                                    const double duration,
                                                    const bool durationWithSensitivity,
                                                    const int controlEsoIndex,
                                                    const int numControlIntervals,
                                                    const double *controlParams,
                                                    const double controlLB,
                                                    const double controlUB,
                                                    const bool controlWithSensitivity,
                                                    const int numConstraints,
                                                    const int *constraintEsoIndices,
                                                    const double *constraintTimes,
                                                    const double *LagrMultipliers)
{
  FactoryInput::IntegratorMetaDataInput imdInput;
  imdInput = createInputForIntegration(model, numInitials, initialEsoIndices, initialValues,
                                             initialsWithSensitivity, duration,
                                             durationWithSensitivity, controlEsoIndex,
                                             numControlIntervals, controlParams, controlLB,
                                             controlUB, controlWithSensitivity, numConstraints,
                                             constraintEsoIndices, constraintTimes, LagrMultipliers);


  //********** create IntegratorSingleStageMetaData ***********

  IntegratorMetaDataFactory imdFactory;
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData = imdFactory.createIntegratorSingleStageMetaData(imdInput);

  //********** set constraints ***********

  for(int i=0; i<numConstraints; i++){
    integratorSSMetaData->addNonlinearConstraint(constraintEsoIndices[i], 0, constraintTimes[i],
                                                 LagrMultipliers[i]);
  }
  return integratorSSMetaData;
}

/**
* copydoc createExampleForIntegration
*
* This function returns a shared pointer to an object of type IntegratorSSMetaData2ndOrder
*/
IntegratorSSMetaData2ndOrder::Ptr createExampleFor2ndOrdIntegration(const std::string &model,
                                                            const int numInitials,
                                                            const int *initialEsoIndices,
                                                            const double *initialValues,
                                                            const bool initialsWithSensitivity,
                                                            const double duration,
                                                            const bool durationWithSensitivity,
                                                            const int controlEsoIndex,
                                                            const int numControlIntervals,
                                                            const double *controlParams,
                                                            const double controlLB,
                                                            const double controlUB,
                                                            const bool controlWithSensitivity,
                                                            const int numConstraints,
                                                            const int *constraintEsoIndices,
                                                            const double *constraintTimes,
                                                            const double *LagrMultipliers)
{
  FactoryInput::IntegratorMetaDataInput imdInput;
  imdInput = createInputForIntegration(model, numInitials, initialEsoIndices, initialValues,
                                             initialsWithSensitivity, duration,
                                             durationWithSensitivity, controlEsoIndex,
                                             numControlIntervals, controlParams, controlLB,
                                             controlUB, controlWithSensitivity, numConstraints,
                                             constraintEsoIndices, constraintTimes, LagrMultipliers);


  //********** create IntegratorSingleStageMetaData ***********

  IntegratorMetaDataFactory imdFactory;
  IntegratorSSMetaData2ndOrder::Ptr integratorSSMD2ndOrder = imdFactory.createIntegratorSSMetaData2ndOrder(imdInput);

  //********** set constraints ***********

  for(int i=0; i<numConstraints; i++){
    integratorSSMD2ndOrder->addNonlinearConstraint(constraintEsoIndices[i], 0, constraintTimes[i],
                                                 LagrMultipliers[i]);
  }
  return integratorSSMD2ndOrder;
}

/**
* @class IntegratorSSBackwardsTestMetaData
* @brief testclass for single stage reverse integration test
*
* In this class the adjoints and lagrange derivatives can be set manually
* in order to set up the initial state of an MetaData object after a forward integration
*/
class IntegratorSSBackwardsTestMetaData : public IntegratorSingleStageMetaData
{
public:
  /**
  * @brief constructor
  *
  * @param genericEso GenericEso pointer with which the test is to be run
  * @param controls vector of controls
  * @param initial vector of initial value parameter
  * @param globalDuration global duration parameter
  * @param userGrid vectro of additional gridpoints (set by user)
  */
  IntegratorSSBackwardsTestMetaData(const GenericEso::Ptr &genericEso,
                                    const std::vector<Control> &controls,
                                    const std::vector<InitialValueParameter::Ptr> &initials,
                                    const DurationParameter::Ptr &globalDuration,
                                    const std::vector<double> &userGrid)
                                    : IntegratorSingleStageMetaData(genericEso,
                                                                    controls,
                                                                    initials,
                                                                    globalDuration,
                                                                    userGrid){}

  /**
  * @brief set adjoints and lagrange derivatives to new values
  *
  * @param adjoints contains new adjoitns values m_adjoints will be resized
  * @param lagrangeDers contains new lagrange derivatives m_lagrangeDers will be resized
  */
  void setAdjointsAndLagrangeDerivatives(const utils::Array<double> &adjoints,
                                  const utils::Array<double> &lagrangeDers)
  {
    m_adjoints.resize(adjoints.getSize());
    for(unsigned i=0; i<adjoints.getSize(); i++){
      m_adjoints[i] = adjoints[i];
    }
    m_lagrangeDers.resize(lagrangeDers.getSize());
    for(unsigned i=0; i<lagrangeDers.getSize(); i++){
      m_lagrangeDers[i] = lagrangeDers[i];
    }
  }
};

/**
* @class IntegratorMetaDataBackwardsTestFactory
* @brief factory class to create IntegratorSSBackwardsTestMetaData objects
*/
class IntegratorMetaDataBackwardsTestFactory
{
public:
  /**
  * @brief create an IntegratorSSBackwardsTestMetaData object
  * @param input IntegratorMetaDataInput struct used to create the object
  * @return pointer to the created object
  */
  IntegratorSingleStageMetaData *createIntegratorSingleStageMetaData
                            (const struct FactoryInput::IntegratorMetaDataInput &input)const
  {
    ParameterFactory paramFactory;
   // GenericEsoFactory genEsoFactory;
    ControlFactory controlFactory;
    std::vector<Control> controls = controlFactory.createControlVector(input.controls);
    DurationParameter::Ptr globalDuration(paramFactory.createDurationParameter(input.duration));
    globalDuration->setParameterSensActivity(true);
    std::vector<InitialValueParameter::Ptr> initials;
    GenericEso::Ptr genEso(input.esoPtr);
    initials = paramFactory.createInitialValueParameterVector(input.initials, genEso);
    std::vector<double> userGrid = input.userGrid;
    return new IntegratorSSBackwardsTestMetaData(genEso, controls, initials, globalDuration, userGrid);
  }
};


/**
* @brief Creates an MetaSingleStageEso for example Rawlingbatch with one sens parameter
*
* @param order order of integration of type TypeDaeInterface
* @return pointer to created MetaSSEDaeInterface object
*/
MetaSSEDaeInterface::Ptr createRawlingbatchObject(TypeDaeInterface order, bool durationWithSensitivity)
{
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  const int numInitials = 0;
  const int numConstraints = 0;
  const double duration = 21.3;
  const int controlEsoIndex = 10; // parameter cB0
  const int numContrIntervals = 1;
  const double controlParams[1] = {0.05};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = false;
  const bool controlWithSensitivity = true;

  integratorSSMetaData = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  integratorSSMetaData->initializeForFirstIntegration();
  integratorSSMetaData->initializeForNewIntegrationStep();
  MetaSSEDaeInterface::Ptr metaSSEDaePtr(new MetaSSEDaeInterface(integratorSSMetaData,order));
  return metaSSEDaePtr;
}

/**
* @brief Creates an MetaMultiStageEso for 3 stages representing the updated car example where all
*        the possible sensitivities can be tested
*
* @param model name of model created by JADE-script
* @return pointer to created IntegratorMultiStageMetaData
*/
IntegratorMultiStageMetaDataDummy::Ptr createMSExampleForZerothAndFirstOrder(const std::string &model)
{
  //===============================
  //        First Stage
  //===============================

  IntegratorSingleStageMetaData::Ptr integratorMetaDataFirstStage;
  const int numInitials1 = 4;
  const int initialEsoIndices1[4] = {0, 1, 2, 3};
  const double initialValues1[4] = {2.0, 4.0, 3.0, 5.0};
  const int numConstraints1 = 0;
  const double duration1 = 10.0;
  const int controlEsoIndex1 = 5; // corresponds to parameter 'cB0' in mof-file
  const int numContrIntervals1 = 5;
  const double controlParams1[5] = {1.0, 0.7, -0.43, 0.1, -1.0};
  const double controlLB1 = -10.0;
  const double controlUB1 = 10.0;
  const bool initialsWithSensitivity1 = true;
  const bool controlWithSensitivity1 = true;
  const bool durationWithSensitivity1 = false;

  integratorMetaDataFirstStage = createExampleForIntegration(model,
                                                             numInitials1,
                                                             initialEsoIndices1,
                                                             initialValues1,
                                                             initialsWithSensitivity1,
                                                             duration1,
                                                             durationWithSensitivity1,
                                                             controlEsoIndex1,
                                                             numContrIntervals1,
                                                             controlParams1,
                                                             controlLB1,
                                                             controlUB1,
                                                             controlWithSensitivity1,
                                                             numConstraints1,
                                                             NULL,
                                                             NULL,
                                                             NULL);
  // set all initial values free
  //integratorMetaDataFirstStage->setAllInitialValuesFree();

  //===============================
  //        Second Stage
  //===============================

  IntegratorSingleStageMetaData::Ptr integratorMetaDataSecondStage;
  const int numInitials2 = 0;
  const int numConstraints2 = 0;
  const double duration2 = 5.0;
  const int controlEsoIndex2 = 5; // corresponds to parameter 'cB0' in mof-file
  const int numContrIntervals2 = 2;
  const double controlParams2[2] = {0.9, 2.12};
  const double controlLB2 = -5.0;
  const double controlUB2 = 5.0;
  const bool initialsWithSensitivity2 = false;
  const bool controlWithSensitivity2 = true;
  const bool durationWithSensitivity2 = true;

  integratorMetaDataSecondStage = createExampleForIntegration(model,
                                                             numInitials2,
                                                             NULL,
                                                             NULL,
                                                             initialsWithSensitivity2,
                                                             duration2,
                                                             durationWithSensitivity2,
                                                             controlEsoIndex2,
                                                             numContrIntervals2,
                                                             controlParams2,
                                                             controlLB2,
                                                             controlUB2,
                                                             controlWithSensitivity2,
                                                             numConstraints2,
                                                             NULL,
                                                             NULL,
                                                             NULL);
  //===============================
  //        Third Stage
  //===============================

  IntegratorSingleStageMetaData::Ptr integratorMetaDataThirdStage;
  const int numInitials3 = 0;
  const int numConstraints3 = 0;
  const double duration3 = 12.0;
  const int controlEsoIndex3 = 5; // corresponds to parameter 'cB0' in mof-file
  const int numContrIntervals3 = 4;
  const double controlParams3[4] = {0.1, 0.42, 0.58, -0.67};
  const double controlLB3 = -1.0;
  const double controlUB3 = 1.0;
  const bool initialsWithSensitivity3 = false;
  const bool controlWithSensitivity3 = true;
  const bool durationWithSensitivity3 = true;

  integratorMetaDataThirdStage = createExampleForIntegration(model,
                                                             numInitials3,
                                                             NULL,
                                                             NULL,
                                                             initialsWithSensitivity3,
                                                             duration3,
                                                             durationWithSensitivity3,
                                                             controlEsoIndex3,
                                                             numContrIntervals3,
                                                             controlParams3,
                                                             controlLB3,
                                                             controlUB3,
                                                             controlWithSensitivity3,
                                                             numConstraints3,
                                                             NULL,
                                                             NULL,
                                                             NULL);
  //===============================
  //   Multi-Stage Formulation
  //===============================
  std::vector<Mapping::Ptr> mappings;
  std::vector<IntegratorSingleStageMetaData::Ptr> stages;
  Mapping::Ptr mapping_12(new SingleShootingMapping());
  Mapping::Ptr mapping_23(new SingleShootingMapping());
  iiMap varIndexMap_12;
  iiMap varIndexMap_23;
  iiMap eqnIndexMap_12;
  iiMap eqnIndexMap_23;
  int numDiffStates = integratorMetaDataFirstStage->getGenericEso()->getNumDifferentialVariables();
  for(int i=0; i<numDiffStates; i++)
  {
    varIndexMap_12[i] = i;
    varIndexMap_23[i] = i;
    eqnIndexMap_12[i] = i;
    eqnIndexMap_23[i] = i;
  }
  mapping_12->setIndexMaps(varIndexMap_12, eqnIndexMap_12);
  mapping_23->setIndexMaps(varIndexMap_23, eqnIndexMap_23);
  mappings.push_back(mapping_12);
  mappings.push_back(mapping_23);
  stages.push_back(integratorMetaDataFirstStage);
  stages.push_back(integratorMetaDataSecondStage);
  stages.push_back(integratorMetaDataThirdStage);
  IntegratorMultiStageMetaDataDummy::Ptr integratorMultiStageMetaDataDummy(new IntegratorMultiStageMetaDataDummy(mappings, stages));

  return integratorMultiStageMetaDataDummy;

}
/**
* @brief Creates an MetaSingleStageEso which is the same as the multi stage problem in function
*        createMSExampleForZerothAndFirstOrder. Thus a different part of the code is tested but
*        the same results have to be obtained
*
* @param model name of model created by JADE-script
* @return pointer to created IntegratorMultiStageMetaData
*/
IntegratorSingleStageMetaData::Ptr createMultiGridExampleForZerothAndFirstOrder(const std::string &model)
{
  const int numInitials = 4;
  const int initialEsoIndices[4] = {0, 1, 2, 3};
  const double initialValues[4] = {2.0, 4.0, 3.0, 5.0};

  const double duration =52.0;
  const int controlEsoIndex = 5;
  const int numControlIntervals = 5;
  const double controlParams1[5] = {1.0, 0.7, -0.43, 0.1, -1.0};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const int numConstraints = 0;

  const bool initialsWithSensitivity = true;
  const bool controlWithSensitivity = true;
  const bool durationWithSensitivity = true;

  FactoryInput::IntegratorMetaDataInput integratorSSMetaDataInput;
  integratorSSMetaDataInput = createInputForIntegration(model,
                                                        numInitials,
                                                        initialEsoIndices,
                                                        initialValues,
                                                        initialsWithSensitivity,
                                                        duration,
                                                        durationWithSensitivity,
                                                        controlEsoIndex,
                                                        numControlIntervals,
                                                        controlParams1,
                                                        controlLB,
                                                        controlUB,
                                                        controlWithSensitivity,
                                                        numConstraints,
                                                        NULL,
                                                        NULL,
                                                        NULL);

  integratorSSMetaDataInput.controls[0].grids.resize(3);

  FactoryInput::ParameterizationGridInput grid1 = integratorSSMetaDataInput.controls.front().grids[0];
  FactoryInput::ParameterizationGridInput grid2 = grid1;
  FactoryInput::ParameterizationGridInput grid3 = grid2;

  grid1.duration = 10;
  grid1.hasFreeDuration = false;

  const double time2[] = {0.0, 0.5, 1.0};
  const double vals2[] = {0.9, 2.12};
  grid2.timePoints.assign(time2, time2+3);
  grid2.values.assign(vals2, vals2+2);
  grid2.duration = 15;
  grid2.hasFreeDuration = true;

  const double time3[] = {0.0, 0.25, 0.5, 0.75, 1.0};
  const double vals3[] = {0.1, 0.42, 0.58, -0.67};
  grid3.timePoints.assign(time3, time3+5);
  grid3.values.assign(vals3, vals3+4);
  grid3.duration = 27.0;


  integratorSSMetaDataInput.controls.front().grids[0] = grid1;
  integratorSSMetaDataInput.controls.front().grids[1] = grid2;
  integratorSSMetaDataInput.controls.front().grids[2] = grid3;
  integratorSSMetaDataInput.userGrid.clear();

  IntegratorMetaDataFactory imdFactory;
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData = imdFactory.createIntegratorSingleStageMetaData(integratorSSMetaDataInput);

  return integratorSSMetaData;
}
#ifdef BUILD_LIMEX //HAVE_LIMEX
#include "SingleStageIntegrationLimexTestsuite.hpp"
#include "MultiStageIntegrationLimexTestsuite.hpp"
#endif
#include "MetaSSEDaeInterfaceTestsuite.hpp"
#include "MetaDaeInterface2ndOrderTestsuite.hpp"
#include "SingleStageIntegrationNixeTestsuite.hpp"
#include "MultiStageIntegrationNixeTestsuite.hpp"
#ifdef BUILD_IDAS //HAVE_IDAS
#include "SingleStageIntegrationIdasTestsuite.hpp"
#endif
