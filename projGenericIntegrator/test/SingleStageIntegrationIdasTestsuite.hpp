/**
* @file SingleStageIntegrationIdas.testsuite
* @brief test cases for Idas based on generic integrator
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik/Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Tests of single-stage integration using a simple car example.        \n
* The reference solution was calculated using JADE and  NIXE.       \n
* =====================================================================\n
*
* @author Fady Assassa, Adrian Caspari
* @date 4.4.2012, 20.05.2018
*/

#include "BlockDaeInitialization.hpp"
#include "KLUSolver.hpp"
#include "CMinPackSolver.hpp"
#include "IntegratorIdas.hpp"


BOOST_AUTO_TEST_SUITE(TestGenericIntegratorSingleStageIdas)

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as ZerothOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestZerothOrderIdas)
{
  std::cout <<"We are running the IDAS testsuite"<< std::endl;
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  int numInitials = 0;
  int numConstraints = 0;
  double finalTime = 1.2;
  int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  int numContrIntervals = 3;
  double controlParams[3] = {1.0, 0.7, -0.43};
  double controlLB = -10.0;
  double controlUB = 10.0;
  bool initialsWithSensitivity = false;
  bool controlWithSensitivity = false;
  bool finalTimeWithSensitivity = false;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     finalTime,
                                                     finalTimeWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);
  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  unsigned numAlgVars = integratorMetaData->getGenericEso()->getNumAlgebraicVariables();
  // the problem is purley ode!
  BOOST_CHECK_EQUAL(numAlgVars, 0);
  LinearSolver::Ptr linSolver(new KLUSolver());
  NonLinearSolver::Ptr nonLinSolver(new CMinPackSolver());
  DaeInitialization::Ptr daeInitialization(new BlockDaeInitialization(linSolver, nonLinSolver, 1e-10));
  ForwardIntegratorIdas::IdasOptions integratorOptions;
  integratorOptions.m_absoluteTolerance = 1e-10;
  integratorOptions.m_relativeTolerance = 1e-10;
  
  ForwardIntegratorIdas idas(integratorMetaData, false, daeInitialization, integratorOptions);
 
  //BOOST_CHECK_NO_THROW(idas.solve());
  BOOST_CHECK_NO_THROW(idas.solve());

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<std::vector<double> > compStates;
  compStates.resize(numContrIntervals);
  unsigned numStates = integratorMetaData->getGenericEso()->getNumEquations();

  for(unsigned i=0; i<unsigned(numContrIntervals); i++){
    compStates[i].resize(numStates);
  }
  //            x_0(t_i)                              x_1(t_i)                               x_2(t_i)
  compStates[0][0] = 0.400000000000000; compStates[0][1] = 0.399946675219205; compStates[0][2] = 0.079994667253949;
  compStates[1][0] = 0.800000000000003; compStates[1][1] = 0.679648746975818; compStates[1][2] = 0.295923817720189;
  compStates[2][0] = 1.200000000000007; compStates[2][1] = 0.507294083496392; compStates[2][2] = 0.533305564438758;


  // compare integrator results with expected results
  utils::Array<double> finalStates(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates.getSize(), finalStates.getData());

  unsigned finalStateIndex = 2;
  for(unsigned i=finalStateIndex; i<unsigned(numContrIntervals); i++){ // we check only final states
    for(unsigned j=0; j<numStates; j++){
      BOOST_CHECK_CLOSE(finalStates[j], compStates[i][j], 1e-7);
    }
  }
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as ZerothOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestZerothOrder2)
{
  std::cout <<"We are running the IDAS TestZerothOrder2"<< std::endl;
  IntegratorSSMetaDataPtr integratorSSMetaData;
  int numInitials = 0;
  int numConstraints = 0;
  double finalTime = 21.3;
  int controlEsoIndex = 10; // corresponds to parameter 'cB0' in mof-file
  int numContrIntervals = 1;
  double paramVals[1] = {0.05};
  double controlLB = -10.0;
  double controlUB = 10.0;
  bool initialsWithSensitivity = false;
  bool controlWithSensitivity = false;
  bool finalTimeWithSensitivity = false;

  integratorSSMetaData = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     finalTime,
                                                     finalTimeWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     paramVals,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  unsigned numAlgVars = integratorMetaData->getGenericEso()->getNumAlgebraicVariables();
  int numStates = integratorMetaData->getGenericEso()->getNumStates();
  LinearSolver::Ptr linSolver(new KLUSolver());
  NonLinearSolver::Ptr nonLinSolver(new CMinPackSolver());
  DaeInitialization::Ptr daeInitialization(new BlockDaeInitialization(linSolver, nonLinSolver, 1e-10));
  
  ForwardIntegratorIdas::IdasOptions integratorOptions;
  integratorOptions.m_absoluteTolerance = 1e-10;
  integratorOptions.m_relativeTolerance = 1e-10;

  ForwardIntegratorIdas idas(integratorMetaData, false, daeInitialization, integratorOptions);
  BOOST_CHECK_NO_THROW(idas.solve());

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compStates;
  compStates.resize(numStates);

 // integration result from IDAS
compStates[0]	= 0.013131997837035479;
compStates[1]	= 0.19504746841203199;
compStates[2]	= 0.65777826903843006;
compStates[3]	= 0.00015109956497909118;
compStates[4]	= 0.0010309202571455916;
compStates[5]	= 28.438052026841419;
compStates[6]	= 12045.547184447581;
compStates[7]	= 0.69075147084541499;




  // compare integrator results with expected results
  utils::Array<double> finalStates(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates.getSize(), finalStates.getData());

  for(int i=0; i<numStates; i++){// we check only final states
    BOOST_CHECK_CLOSE(finalStates[i], compStates[i], THRESHOLD);
  }
  
}

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardIdas)
{
  std::cout <<"We are running the IDAS TestFirstOrderForwardIdas"<< std::endl;
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  int numInitials = 3;
  int initialEsoIndices[3] = {0, 1, 2};
  double initialValues[3] = {0.0, 0.0, 0.0};
  double finalTime = 1.2;
  int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  int numContrIntervals = 3;
  double controlParams[3] = {1.0, 0.7, -0.43};
  double controlLB = -10.0;
  double controlUB = 10.0;
  bool initialsWithSensitivity = true;
  bool finalTimeWithSensitivity = false;
  bool controlWithSensitivity = true;
  int numConstraints = 0;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     finalTime,
                                                     finalTimeWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  unsigned numAlgVars = integratorMetaData->getGenericEso()->getNumAlgebraicVariables();
  int numStates = integratorMetaData->getGenericEso()->getNumStates();
  // the problem is purley ode!

  std::vector<double> finalSens;
  BOOST_CHECK_EQUAL(numAlgVars, 0);
  LinearSolver::Ptr linSolver(new KLUSolver());
  NonLinearSolver::Ptr nonLinSolver(new CMinPackSolver());
  DaeInitialization::Ptr daeInitialization(new BlockDaeInitialization(linSolver, nonLinSolver, 1e-10));
  
  ForwardIntegratorIdas::IdasOptions integratorOptions;
  integratorOptions.m_absoluteTolerance = 1e-10;
  integratorOptions.m_relativeTolerance = 1e-10;

  ForwardIntegratorIdas idas(integratorMetaData, true, daeInitialization, integratorOptions);

  BOOST_CHECK_NO_THROW(idas.solve());
  integratorMetaData->getCurrentSensitivities(finalSens);
  BOOST_CHECK_NO_THROW(idas.solve());
  finalSens.clear();
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtFinalTime;
  compControlSensAtFinalTime.resize(numStates*numContrIntervals);
  //     dx_0(t_f)/dpi                       dx_1(t_f)/dpi                               dx_2(t_f)/dpi
  compControlSensAtFinalTime[0] = 0.0; compControlSensAtFinalTime[1] = 0.398988005032359; compControlSensAtFinalTime[2] = 0.399553178988916;
  compControlSensAtFinalTime[3] = 0.0; compControlSensAtFinalTime[4] = 0.399291308446237; compControlSensAtFinalTime[5] = 0.23977797145213042;
  compControlSensAtFinalTime[6] = 0.0; compControlSensAtFinalTime[7] = 0.39977419576220624; compControlSensAtFinalTime[8] = 0.079968358692287314;

  for(int i=0; i<numStates*numContrIntervals; i++){
    BOOST_CHECK_CLOSE(finalSens[i+9], compControlSensAtFinalTime[i], 1e-6);
  }
}



/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardIdas2)
{
  std::cout <<"We are running the IDAS TestFirstOrderForwardIdas2"<< std::endl;
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData;
  int numInitials = 3;
  int initialEsoIndices[3] = {0, 1, 2};
  double initialValues[3] = {0.0, 0.0, 0.0};
  double finalTime = 1.0;
  int controlEsoIndex = 3; // corresponds to parameter 'accel' in mof-file
  int numContrIntervals = 10;
  double controlParams[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  double controlLB = -10.0;
  double controlUB = 10.0;
  bool initialsWithSensitivity = false;
  bool finalTimeWithSensitivity = false;
  bool controlWithSensitivity = true;
  int numConstraints = 0;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     finalTime,
                                                     finalTimeWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  unsigned numAlgVars = integratorMetaData->getGenericEso()->getNumAlgebraicVariables();
  int numStates = integratorMetaData->getGenericEso()->getNumStates();
  // the problem is purley ode!

  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  options.absTolScalar = 1e-10;
  options.relTolScalar = 1e-10;
  ForwardIntegratorNixe fof(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(fof.solve());
  std::vector<double> finalSensNIXE;
  integratorMetaData->getCurrentSensitivities(finalSensNIXE);
  std::cout << "Nixe integrated!" << std::endl;

  std::vector<double> finalSensIDAS;
  BOOST_CHECK_EQUAL(numAlgVars, 0);
  LinearSolver::Ptr linSolver(new KLUSolver());
  NonLinearSolver::Ptr nonLinSolver(new CMinPackSolver());
  DaeInitialization::Ptr daeInitialization(new BlockDaeInitialization(linSolver, nonLinSolver, 1e-10));


  ForwardIntegratorIdas::IdasOptions integratorOptions;
  integratorOptions.m_absoluteTolerance = 1e-10;
  integratorOptions.m_relativeTolerance = 1e-10;
  integratorOptions.m_errorControlSen = true;

  ForwardIntegratorIdas idas(integratorMetaData, true, daeInitialization, integratorOptions);

  BOOST_CHECK_NO_THROW(idas.solve());
  integratorMetaData->getCurrentSensitivities(finalSensIDAS);

  double compStates[30];
compStates[	0	]=	0	;
compStates[	1	]=	0.10000000000000001	;
compStates[	2	]= 0.095000056207614397;
compStates[	3	]=	0	;
compStates[	4	]= 0.10000000000000027;
compStates[	5	]= 0.085000056207615346;
compStates[	6	]=	0	;
compStates[	7	]=	0.10000000000000164	;
compStates[	8	]= 0.075000056207615587;
compStates[	9	]=	0	;
compStates[	10	]=	0.099999999999999867	;
compStates[	11	]=	0.065000478766154238	;
compStates[	12	]=	0	;
compStates[	13	]=	0.099999999999999867	;
compStates[	14	]= 0.055000056207614244;
compStates[	15	]=	0	;
compStates[	16	]= 0.099999999999993414;
compStates[	17	]= 0.045000056207612125;
compStates[	18	]=	0	;
compStates[	19	]=	0.10000000000000109	;
compStates[	20	]=	0.035000478766154794	;
compStates[	21	]=	0	;
compStates[	22	]= 0.099999999999993414;
compStates[	23	]= 0.025000478766153501;
compStates[	24	]=	0	;
compStates[	25	]= 0.099999999999993414;
compStates[	26	]= 0.015000478766153852;
compStates[	27	]=	0	;
compStates[	28	]= 0.099999999999993525;
compStates[	29	]= 0.0050004787661542139;

  

  for(unsigned i=0; i<finalSensIDAS.size(); i++){
    BOOST_CHECK_CLOSE(compStates[i], finalSensIDAS[i], 1e-2);
  }
}


/**
* @test GenericIntegratorTest - test Idas with DAE initialization
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are tested for
* an updated car example
*/
/*
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardAllSensDaeInitialization)
{
  std::cout <<"We are running the IDAS TestFirstOrderForwardAllSensDaeInitialization"<< std::endl;
  IntegratorSSMetaDataPtr integratorSSMetaData;
  const int numInitials = 4;
  int initialEsoIndices[numInitials] = {0, 1, 2, 3};
  double initialValues[numInitials] = {2.0, 4.0, 3.0, 5.0};
  double finalTime = 10.0;
  int controlEsoIndex = 5; // corresponds to parameter 'accel' in mof-file
  const int numContrIntervals = 5;
  double controlParams[numContrIntervals] = {1.0, 0.7, -0.43, 0.1, -1.0};
  double controlLB = -10.0;
  double controlUB = 10.0;
  int numConstraints = 0;
  bool initialsWithSensitivity = true;
  bool controlWithSensitivity = true;
  bool finalTimeWithSensitivity = true;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_UPDATED_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     finalTime,
                                                     finalTimeWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  int numStates = integratorMetaData->getGenericEso()->getNumStates();
  int numControlParameters = numContrIntervals;

  // sensitivities w.r.t. all initial values
  int numInitialParameters = numInitials; // number of differential states

  int numFinalTimeParameters = 1; // one global final time
  LinearSolver::Ptr linSolver(new KLUSolver());
  NonLinearSolver::Ptr nonLinSolver(new CMinPackSolver());
  DaeInitialization::Ptr daeInitialization(new BlockDaeInitialization(linSolver, nonLinSolver, 1e-10));
  

  ForwardIntegratorIdas::IdasOptions integratorOptions;
  integratorOptions.m_absoluteTolerance = 1e-10;
  integratorOptions.m_relativeTolerance = 1e-10;
  integratorOptions.m_errorControlSen = true;

  ForwardIntegratorIdas idas(integratorMetaData, true, daeInitialization, integratorOptions);



  BOOST_CHECK_NO_THROW(idas.solve());


  std::vector<double> finalSens;
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtFinalTime;
  compControlSensAtFinalTime.resize(numStates*numControlParameters);
  std::vector<double> compInitialSensAtFinalTime;
  compInitialSensAtFinalTime.resize(numStates*numInitialParameters);
  std::vector<double> compFinalTimeSensAtFinalTime;
  compFinalTimeSensAtFinalTime.resize(numStates*numFinalTimeParameters);

  // compare State-Sensitivities with respect to controlparametrization at final Time
  //              dx_0(t_f)/dpi                       dx_1(t_f)/dpi                         dx_2(t_f)/dpi
  compControlSensAtFinalTime[0] = 17.68817955549596; compControlSensAtFinalTime[1] = 0.0;  compControlSensAtFinalTime[2] = 1.932120812247657;
  compControlSensAtFinalTime[5] = 13.79739502449891; compControlSensAtFinalTime[6] = 0.0;  compControlSensAtFinalTime[7] = 1.945769187961033;
  compControlSensAtFinalTime[10] = 9.899355104512301; compControlSensAtFinalTime[11] = 0.0; compControlSensAtFinalTime[12] = 1.962620341654356;
  compControlSensAtFinalTime[15] = 5.964321185693118; compControlSensAtFinalTime[16] = 0.0; compControlSensAtFinalTime[17] = 1.978671358215149;
  compControlSensAtFinalTime[20] = 1.9955453275254915; compControlSensAtFinalTime[21] = 0.0; compControlSensAtFinalTime[22] = 1.993803561922133;

  //               dx_3(t_f)/dpi                       dx_4(t_f)/dpi
  compControlSensAtFinalTime[3] = 30.0;  compControlSensAtFinalTime[4] = 0.0;
  compControlSensAtFinalTime[8] = 30.0;  compControlSensAtFinalTime[9] = 0.0;
  compControlSensAtFinalTime[13] = 0.0;  compControlSensAtFinalTime[14] = 0.0;
  compControlSensAtFinalTime[18] = 30.0; compControlSensAtFinalTime[19] = 0.0;
  compControlSensAtFinalTime[23] = 0.0;  compControlSensAtFinalTime[24] = 0.0;

  for(int i=0; i<numStates*numControlParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[numInitialParameters*numStates + i], compControlSensAtFinalTime[i], THRESHOLD);
    //std::cout << setprecision(16)<< finalSens[numInitialParameters*numStates + i] << std::endl;
  }

  // compare State-Sensitivities with respect to initial values of differential states at final Time
  //              dx_0(t_f)/dx0i                             dx_1(t_f)/dx0i                           dx_2(t_f)/dx0i
  compInitialSensAtFinalTime[0] = 1.0;                compInitialSensAtFinalTime[1] = 0.0;   compInitialSensAtFinalTime[2] = 0.0;
  compInitialSensAtFinalTime[5] = 0.0;                compInitialSensAtFinalTime[6] = 1.0;   compInitialSensAtFinalTime[7] = 0.0;
  compInitialSensAtFinalTime[10] = 9.820058548208893; compInitialSensAtFinalTime[11] = 0.0;  compInitialSensAtFinalTime[12] = 0.9635141039531602;
  compInitialSensAtFinalTime[15] = 0.0;               compInitialSensAtFinalTime[16] = 0.0;  compInitialSensAtFinalTime[17] = 0.0;

  //               dx_3(t_f)/dx0i                       dx_4(t_f)/dx0i
  compInitialSensAtFinalTime[3] = 0.0;   compInitialSensAtFinalTime[4] = 0.0;
  compInitialSensAtFinalTime[8] = 0.0;   compInitialSensAtFinalTime[9] = 0.0;
  compInitialSensAtFinalTime[13] = 0.0;  compInitialSensAtFinalTime[14] = 0.0;
  compInitialSensAtFinalTime[18] = 1.0;  compInitialSensAtFinalTime[19] = 0.0;

  std::cout << "  " << std::endl;
  for(int i=0; i<numStates*numInitialParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[i], compInitialSensAtFinalTime[i], THRESHOLD);
    //std::cout << setprecision(16)<< finalSens[i] << std::endl;
  }

  // compare State-Sensitivities with respect to finalTime at final Time
  //              dx_0(t_f)/dtf
  compFinalTimeSensAtFinalTime[0] = 7.284121630685332;
  //              dx_1(t_f)/dtf
  compFinalTimeSensAtFinalTime[1] = 0.9999999999999982;
  //              dx_2(t_f)/dtf
  compFinalTimeSensAtFinalTime[2] = 0.0557609942772541;
  //              dx_3(t_f)/dtf
  compFinalTimeSensAtFinalTime[3] = 6.400000000000062;
  //              dx_4(t_f)/dtf
  compFinalTimeSensAtFinalTime[4] = 0.0;

  std::cout << "  " << std::endl;
  for(int i=0; i<numStates*numFinalTimeParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[(numInitialParameters + numControlParameters)*numStates + i],
                      compFinalTimeSensAtFinalTime[i], 1e-3);
    //std::cout << setprecision(16)<< finalSens[(numInitialParameters + numControlParameters)*numStates + i] << std::endl;
  }
}
*/



/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrderForwardHandler
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are tested for
* an updated car example
*/
/*
BOOST_AUTO_TEST_CASE(TestFirstOrderForwardAllSens)
{

  IntegratorSSMetaDataPtr integratorSSMetaData;
  int numInitials = 4;
  int initialEsoIndices[4] = {0, 1, 2, 3};
  double initialValues[4] = {2.0, 4.0, 3.0, 5.0};
  double finalTime = 10.0;
  int controlEsoIndex = 5; // corresponds to parameter 'accel' in mof-file
  int numContrIntervals = 5;
  double controlParams[5] = {1.0, 0.7, -0.43, 0.1, -1.0};
  double controlLB = -10.0;
  double controlUB = 10.0;
  int numConstraints = 0;
  bool initialsWithSensitivity = true;
  bool controlWithSensitivity = true;
  bool finalTimeWithSensitivity = true;

  integratorSSMetaData = createExampleForIntegration(CAR_JADE_UPDATED_MODEL_NAME,
                                                     numInitials,
                                                     initialEsoIndices,
                                                     initialValues,
                                                     initialsWithSensitivity,
                                                     finalTime,
                                                     finalTimeWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMetaData(integratorSSMetaData);
  int numStates = integratorMetaData->getGenericEso()->getNumStates();
  int numControlParameters = numContrIntervals;

  // sensitivities w.r.t. all initial values
  int numInitialParameters = numInitials; // number of differential states

  int numFinalTimeParameters = 1; // one global final time

  LinearSolver::Ptr linSolver(new KLUSolver());
  NonLinearSolver::Ptr nonLinSolver(new CMinPackSolver());
  DaeInitialization::Ptr daeInitialization(new BlockDaeInitialization(linSolver, nonLinSolver, 1e-10));
  

  ForwardIntegratorIdas::IdasOptions integratorOptions;
  integratorOptions.m_absoluteTolerance = 1e-10;
  integratorOptions.m_relativeTolerance = 1e-10;

  ForwardIntegratorIdas idas(integratorMetaData, true, daeInitialization, integratorOptions);


  BOOST_CHECK_NO_THROW(idas.solve());
  std::vector<double> finalSens;
  integratorMetaData->getCurrentSensitivities(finalSens);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  std::vector<double> compControlSensAtFinalTime;
  compControlSensAtFinalTime.resize(numStates*numControlParameters);
  std::vector<double> compInitialSensAtFinalTime;
  compInitialSensAtFinalTime.resize(numStates*numInitialParameters);
  std::vector<double> compFinalTimeSensAtFinalTime;
  compFinalTimeSensAtFinalTime.resize(numStates*numFinalTimeParameters);

  // compare State-Sensitivities with respect to controlparametrization at final Time
  //              dx_0(t_f)/dpi                       dx_1(t_f)/dpi                         dx_2(t_f)/dpi
  compControlSensAtFinalTime[0] = 17.688179555495964; compControlSensAtFinalTime[1] = 0.0;  compControlSensAtFinalTime[2] = 1.9321208122476574;
  compControlSensAtFinalTime[5] = 13.797395024498908; compControlSensAtFinalTime[6] = 0.0;  compControlSensAtFinalTime[7] = 1.9457691879610333;
  compControlSensAtFinalTime[10] = 9.8993551045123009; compControlSensAtFinalTime[11] = 0.0; compControlSensAtFinalTime[12] = 1.9626203416543562;
  compControlSensAtFinalTime[15] = 5.9643211856931178; compControlSensAtFinalTime[16] = 0.0; compControlSensAtFinalTime[17] = 1.9786713582151489;
  compControlSensAtFinalTime[20] = 1.9955453275254915; compControlSensAtFinalTime[21] = 0.0; compControlSensAtFinalTime[22] = 1.9938035619221326;
  
  //               dx_3(t_f)/dpi                       dx_4(t_f)/dpi
  compControlSensAtFinalTime[3] = 30.000000000000000;  compControlSensAtFinalTime[4] = 0.0;
  compControlSensAtFinalTime[8] = 30.000000000000000;  compControlSensAtFinalTime[9] = 0.0;
  compControlSensAtFinalTime[13] = 0.0;                compControlSensAtFinalTime[14] = 0.0;
  compControlSensAtFinalTime[18] = 30.000000000000128; compControlSensAtFinalTime[19] = 0.0;
  compControlSensAtFinalTime[23] = 0.0;                compControlSensAtFinalTime[24] = 0.0;

  for(int i=0; i<numStates*numControlParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[numInitialParameters*numStates + i], compControlSensAtFinalTime[i], 1e-3);
  }

  // compare State-Sensitivities with respect to initial values of differential states at final Time
  //              dx_0(t_f)/dx0i                             dx_1(t_f)/dx0i                           dx_2(t_f)/dx0i
  compInitialSensAtFinalTime[0] = 1.0;                compInitialSensAtFinalTime[1] = 0.0;   compInitialSensAtFinalTime[2] = 0.0;
  compInitialSensAtFinalTime[5] = 0.0;                compInitialSensAtFinalTime[6] = 1.0;   compInitialSensAtFinalTime[7] = 0.0;
  compInitialSensAtFinalTime[10] = 9.8200585482088929; compInitialSensAtFinalTime[11] = 0.0;  compInitialSensAtFinalTime[12] = 0.96351410395316017;
  compInitialSensAtFinalTime[15] = 0.0;               compInitialSensAtFinalTime[16] = 0.0;  compInitialSensAtFinalTime[17] = 0.0;

  //               dx_3(t_f)/dx0i                       dx_4(t_f)/dx0i
  compInitialSensAtFinalTime[3] = 0.0;   compInitialSensAtFinalTime[4] = 0.0;
  compInitialSensAtFinalTime[8] = 0.0;   compInitialSensAtFinalTime[9] = 0.0;
  compInitialSensAtFinalTime[13] = 0.0;  compInitialSensAtFinalTime[14] = 0.0;
  compInitialSensAtFinalTime[18] = 1.0;  compInitialSensAtFinalTime[19] = 0.0;

  for(int i=0; i<numStates*numInitialParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[i], compInitialSensAtFinalTime[i], 1e-3);
  }

  // compare State-Sensitivities with respect to finalTime at final Time
  //              dx_0(t_f)/dtf
  compFinalTimeSensAtFinalTime[0] = 7.2841216306853322;
  //              dx_1(t_f)/dtf
  compFinalTimeSensAtFinalTime[1] = 0.99999999999999822;
  //              dx_2(t_f)/dtf
  compFinalTimeSensAtFinalTime[2] = 0.055760994277254103;
  //              dx_3(t_f)/dtf
  compFinalTimeSensAtFinalTime[3] = 6.4000000000000616;
  //              dx_4(t_f)/dtf
  compFinalTimeSensAtFinalTime[4] = 0.0;

  for(int i=0; i<numStates*numFinalTimeParameters; i++){
    BOOST_CHECK_CLOSE(finalSens[(numInitialParameters + numControlParameters)*numStates + i],
                      compFinalTimeSensAtFinalTime[i], 1e-3);
  }
}
*/
BOOST_AUTO_TEST_SUITE_END()
