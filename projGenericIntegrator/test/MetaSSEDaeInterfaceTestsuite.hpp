/**
* @file MetaSSEDaeInterface.testsuite
* @brief test cases for MetaSSEDaeInterface.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Test of all functions of MetaSSEDaeInterface which is a child        \n
* class of Nixe::DaeInterface                                          \n
* =====================================================================\n
*
* @author Moritz Schmitz
* @date 18.4.2012
*/

BOOST_AUTO_TEST_SUITE(TestMetaSSEDaeInterface)

/**
* @brief Creates an MetaSSEDaeInterface for the Rawlingbatch example as dll
*/
BOOST_AUTO_TEST_CASE(TestInitializationMetaSSEDaeInterface)
{
  MetaSSEDaeInterface::Ptr metaSSEDaePtr;
  TypeDaeInterface order = firstForwOrder;
  const bool durationWithSensitivity = false;
  BOOST_CHECK_NO_THROW(metaSSEDaePtr = createRawlingbatchObject(order, durationWithSensitivity));
}

/**
* @brief Testing GetProblemInfo
*/
BOOST_AUTO_TEST_CASE(TestGetProblemInfo)
{
  const bool withDurationSens = false;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstForwOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  BOOST_CHECK_EQUAL(problemInfo->StartTime(), 0.0);
  BOOST_CHECK_EQUAL(problemInfo->FinalTime(), 1.0);
  BOOST_CHECK_EQUAL(problemInfo->NumStates(), 8);
  BOOST_CHECK_EQUAL(problemInfo->NumSens(), 1);
  BOOST_CHECK_EQUAL(problemInfo->MaxOrderSens(), 1);
  BOOST_CHECK_EQUAL(problemInfo->NumNonZerosMass(), 3);
  BOOST_CHECK_EQUAL(problemInfo->NumNonZerosJac(), 22);
  BOOST_CHECK_EQUAL(problemInfo->NumSwitchFctns(), 0);
}

/**
* @brief Testing the evaluation of the mass matrix
*/
BOOST_AUTO_TEST_CASE(TestEvaluationMassMatrix)
{
  const bool withDurationSens = false;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstForwOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numNonZerosMass = problemInfo->NumNonZerosMass();
  boost::shared_ptr<CompressedColumn> massMatrix (new Nixe::CompressedColumn(numStates,numStates,
                                                  numNonZerosMass));

  /***** define the values for comparison *****/

  const int pcComp[] = {0,1,2,3,3,3,3,3,3};
  const int irComp[] = {0,1,2};
  const double valuesComp[] = {-1.0,-1.0,-1.0};

  /***** compare to calculated values *****/

  metaSSEDae->EvalMass(massMatrix->Interface());
  BOOST_CHECK_EQUAL(massMatrix->nzmax, 3);
  BOOST_CHECK_EQUAL(massMatrix->m, 8);
  BOOST_CHECK_EQUAL(massMatrix->n, 8);
  BOOST_CHECK_EQUAL_COLLECTIONS(massMatrix->pc, massMatrix->pc+numStates+1, pcComp,
                                pcComp+numStates+1);
  BOOST_CHECK_EQUAL_COLLECTIONS(massMatrix->ir, massMatrix->ir+numNonZerosMass, irComp,
                                irComp+numNonZerosMass);
  BOOST_CHECK_EQUAL_COLLECTIONS(massMatrix->values, massMatrix->values+numNonZerosMass, valuesComp,
                                valuesComp+numNonZerosMass);
}

/**
* @brief Testing the forward evaluation
*/
BOOST_AUTO_TEST_CASE(TestForwardEvaluation)
{
  const bool withDurationSens = false;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstForwOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numSens = problemInfo->NumSens();
  const int numNonZerosStateJac = 22;
  const double globDuration = 21.3;
  const bool evalJac = true;
  double states[] = {0.6, 0.06, 1.0, 1.5, 3.05, 0.23, 3.29, 0.063};
  double parameters[] = {1, 0.5, 0.05, 0, 0.5, 0.05, 0.2, 0.01, 0.0625};
  utils::Array<double> rhsStates(numStates,0.0);
  utils::Array<double> sens(numStates*numSens,1.23);
  boost::shared_ptr<Nixe::CompressedColumn> stateJacobian(new Nixe::CompressedColumn(numStates,
                                                    numStates, problemInfo->NumNonZerosJac()));
  utils::Array<double> rhsStatesDtime(numStates, 0.0);
  utils::Array<double> rhsSens(numStates*numSens,0.0);
  metaSSEDae->EvalForward(evalJac, problemInfo->StartTime(),states,parameters, rhsStates.getData(),
                          sens.getData(), stateJacobian->Interface(), rhsStatesDtime.getData(),
                          rhsSens.getData(), *problemInfo);

  /***** define the values for comparison *****/

    /*** jacobian ***/

  const int stateJacPcComp[] = {0,3,7,11,15,18,20,21,22};
  const int stateJacIrComp[] = {3,5,7,3,4,5,7,3,4,5,7,0,1,2,3,1,2,4,5,6,6,7};
  const double stateJacValuesComp[] = {-0.5*globDuration, -32.84*globDuration, -0.2*globDuration,
                                 0.05*globDuration, -0.024*globDuration, -32.84*globDuration,
                                 -0.02*globDuration, 0.003*globDuration, 0.01*globDuration,
                                 -32.84*globDuration, -2.0*globDuration, 1.0*globDuration,
                                 -1.0*globDuration, -1.0*globDuration, 1.0*globDuration,
                                 2.0*globDuration, -1.0*globDuration, 1.0*globDuration,
                                 1.0*globDuration, 24.64*globDuration, 1.0*globDuration,
                                 1.0*globDuration};
    /*** rhsStates ***/

  const double rhsStatesComp[] = {1.5*globDuration, 4.6*globDuration, -4.55*globDuration,
                            1.203*globDuration, 3.05928*globDuration, -54.2844*globDuration,
                            -6.1964*globDuration, -0.9471*globDuration};
    /*** rhsSens ***/

  utils::Array<double> rhsSensComp(numStates,0.0);
  rhsSensComp[7] = 0.02*globDuration; // df/dp = df/dcB0
  cs *stateJacobianCS = cs_spalloc(numStates,numStates,stateJacobian->nzmax,1,-1);
  Nixe::VectorCopy(numStates+1, stateJacobianCS->p, stateJacobian->pc);
  Nixe::VectorCopy(numNonZerosStateJac, stateJacobianCS->i, stateJacobian->ir);
  Nixe::VectorCopy(numNonZerosStateJac, stateJacobianCS->x, stateJacobian->values);
  stateJacobianCS->nz = -1;
    // rhsSens = df/dx * dx/dp + rhsSens, p=cB0
  cs_gaxpy(stateJacobianCS, sens.getData(), rhsSensComp.getData());

  
  /***** compare to calculated values *****/

  BOOST_CHECK_EQUAL(stateJacobian->nzmax, numNonZerosStateJac);
  BOOST_CHECK_EQUAL(stateJacobian->m, 8);
  BOOST_CHECK_EQUAL(stateJacobian->n, 8);
  BOOST_CHECK_EQUAL_COLLECTIONS(stateJacobian->pc, stateJacobian->pc+numStates+1, stateJacPcComp,
                                stateJacPcComp+numStates+1);
  BOOST_CHECK_EQUAL_COLLECTIONS(stateJacobian->ir, stateJacobian->ir+numNonZerosStateJac,
                                stateJacIrComp, stateJacIrComp+numNonZerosStateJac);
  for(int i=0; i<numNonZerosStateJac; i++){
  BOOST_CHECK_CLOSE(stateJacobian->values[i], stateJacValuesComp[i], THRESHOLD);
  }

  for(int i=0; i<numStates; i++){
  BOOST_CHECK_CLOSE(rhsStates[i], rhsStatesComp[i], THRESHOLD);
  }

  for(int i=0; i<numStates; i++){
  BOOST_CHECK_CLOSE(rhsSens[i], rhsSensComp[i], THRESHOLD);
  }
  cs_spfree(stateJacobianCS);
}

/**
* @brief Testing the initial values evaluation
*/
BOOST_AUTO_TEST_CASE(TestInitialEvaluation)
{
  const bool withDurationSens = false;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstForwOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numSens = problemInfo->NumSens();
  double parameters[] = {1, 0.5, 0.05, 0, 0.5, 0.05, 0.2, 0.01, 0.0625};
  utils::Array<double> states(numStates,0.0);
  utils::Array<double> sens(numStates*numSens,0.0);
  metaSSEDae->EvalInitialValues(problemInfo->StartTime(), parameters, states.getData(),
                                sens.getData(), *problemInfo);

  utils::Array<double> sensComp(numStates*numSens,0.0);
  const double statesComp[] = {0.5, 0.05, 0, 0, 0, 0, 0, 0};

  for(int i=0; i<numStates;i++){
    BOOST_CHECK_CLOSE(states[i], statesComp[i],THRESHOLD);
  }
  for(int i=0; i<numStates*numSens;i++){
    BOOST_CHECK_CLOSE(sens[i], sensComp[i],THRESHOLD);
  }
}

/**
* @brief Testing the final values evaluation
*/
BOOST_AUTO_TEST_CASE(TestFinalEvaluation)
{
  double adjointsComp[] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8};
  const int numAdjoints = 8;
  utils::WrappingArray<double> adjointsArray(numAdjoints, adjointsComp);
  utils::Array<double> lagrangeDersArray(1);

  // Definition of an integratorSSMetaData in order to call setAdjointsAndLagrangeDers()
  const int numInitials = 0;
  const int numConstraints = 0;
  const double duration = 21.3;
  const int controlEsoIndex = 10;
  const int numContrIntervals = 1;
  const double controlParams[1] = {0.05};
  const double controlLB = -10.0;
  const double controlUB = 10.0;
  const bool initialsWithSensitivity = false;
  const bool controlWithSensitivity = false;
  const bool durationWithSensitivity = false;

  FactoryInput::IntegratorMetaDataInput imdInput = createInputForIntegration(
                                                     RAWLINGBATCH_JADE_MODEL_NAME,
                                                     numInitials,
                                                     NULL,
                                                     NULL,
                                                     initialsWithSensitivity,
                                                     duration,
                                                     durationWithSensitivity,
                                                     controlEsoIndex,
                                                     numContrIntervals,
                                                     controlParams,
                                                     controlLB,
                                                     controlUB,
                                                     controlWithSensitivity,
                                                     numConstraints,
                                                     NULL,
                                                     NULL,
                                                     NULL);

  IntegratorMetaDataBackwardsTestFactory imdFactory;
  IntegratorSingleStageMetaData::Ptr integratorSSMetaData(imdFactory.createIntegratorSingleStageMetaData(imdInput));

  //********** set constraints ***********
  integratorSSMetaData->setAdjointsAndLagrangeDerivatives(adjointsArray,lagrangeDersArray);

  // Definition of an metaSSEDae in order to call EvalFinalValues()
  MetaSSEDaeInterface::Ptr metaSSEDae(new MetaSSEDaeInterface(integratorSSMetaData,firstRevOrder));
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numSens = problemInfo->NumSens();
  double parameters[] = {1, 0.5, 0.05, 0, 0.5, 0.05, 0.2, 0.01, 0.0625};
  utils::Array<double> states(numStates,0.0);
  utils::Array<double> sens(numStates*numSens,0.0);
  utils::Array<double> adjoints(numAdjoints,0.0);
  metaSSEDae->EvalFinalValues(problemInfo->StartTime(), states.getData(), parameters,
                              sens.getData(), adjoints.getData(), *problemInfo);

  for(int i=0; i<numAdjoints;i++){
    BOOST_CHECK_CLOSE(adjoints[i], adjointsComp[i],THRESHOLD);
  }
}

/**
* @brief Testing the state-jacobian struct
*/
BOOST_AUTO_TEST_CASE(TestStateJacobianStruct)
{
  const bool withDurationSens = false;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstForwOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numNonZerosStateJac = problemInfo->NumNonZerosJac();
  utils::Array<int> rows(numNonZerosStateJac,0);
  utils::Array<int> cols(numNonZerosStateJac,0);
  int numNonZeroDfdx = 0;
  metaSSEDae->GetStateJacobianStruct(numNonZeroDfdx, rows.getData(), cols.getData());

  const int rowsComp[] = {3,5,7,3,4,5,7,3,4,5,7,0,1,2,3,1,2,4,5,6,6,7};// column-major format
  const int colsComp[] = {0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,5,5,6,7};// column-major format

  BOOST_CHECK_EQUAL(numNonZeroDfdx, numNonZerosStateJac);
  BOOST_CHECK_EQUAL_COLLECTIONS(rows.getData(), rows.getData()+numNonZerosStateJac,
                                rowsComp, rowsComp+numNonZerosStateJac);
  BOOST_CHECK_EQUAL_COLLECTIONS(cols.getData(), cols.getData()+numNonZerosStateJac,
                                colsComp, colsComp+numNonZerosStateJac);
}

/**
* @brief Testing the reverse evaluation
*/
BOOST_AUTO_TEST_CASE(TestReverseEvaluation)
{
  const bool withDurationSens = true;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstRevOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const double globDuration = 21.3;
  double states[] = {0.6, 0.06, 1.0, 1.5, 3.05, 0.23, 3.29, 0.063};
  double parameters[] = {1, 0.5, 0.05, 0, 0.5, 0.05, 0.2, 0.01, 0.0625};
  double adjoints[] = {0.12, -0.02, 4.6, 0.005, 1.3, -0.23, 2.84, 0.061};
  utils::Array<double> rhsAdjoints(numStates,0.0);
  const int numCurSensPar = problemInfo->NumDers();
  utils::Array<double> rhsDers(numCurSensPar,0.0);
  double *sens = NULL;
  metaSSEDae->EvalReverse(problemInfo->StartTime(), states, parameters, sens, adjoints,
                          rhsAdjoints.getData(), rhsDers.getData(), *problemInfo);

  /***** compare rhsAdjoints *****/

  double rhsAdjointsComp[] = {7.538500000000001*globDuration, 7.521030000000001*globDuration,
                              7.444215000000002*globDuration,-4.455000000000000*globDuration,
                             -3.340000000000000*globDuration, 69.747599999999990*globDuration,
                              2.840000000000000*globDuration, 0.061000000000000*globDuration};

  for(int i=0; i<numStates; i++){
  BOOST_CHECK_CLOSE(rhsAdjoints[i], rhsAdjointsComp[i], THRESHOLD);
  }

  /***** compare rhsDers *****/

  // the last value of rhsDersComp must not be multiplied by the global final time, because
  // it represents adjoints^T * df/dt_f
  const double rhsDersComp[] = {0.001220000000000*globDuration, -22.029058099999997};

  for(int i=0; i<numCurSensPar; i++){
  BOOST_CHECK_CLOSE(rhsDers[i], rhsDersComp[i], THRESHOLD);
  }
}

/**
* @brief Testing the rhs evaluation
*/
BOOST_AUTO_TEST_CASE(TestRhsEvaluation)
{
  const bool withDurationSens = false;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstForwOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int numEqToEvaluate = 4;
  const double globDuration = 21.3;
  long indices[] = {3,4,5,7};
  double states[] = {0.6, 0.06, 1.0, 1.5, 3.05, 0.23, 3.29, 0.063};
  utils::Array<double> rhsStates(4,0.0);
  metaSSEDae->EvalRhs(numEqToEvaluate, indices, problemInfo->StartTime(), states, numStates,
                      rhsStates.getData());

  const double rhsStatesComp[] = {1.203*globDuration, 3.05928*globDuration, -54.2844*globDuration,
                            -0.9471*globDuration};

  for(int i=0; i<numEqToEvaluate; i++){
  BOOST_CHECK_CLOSE(rhsStates[i], rhsStatesComp[i], THRESHOLD);
  }
}

/**
* @brief Testing state-jacobian subpart evaluation
*/
BOOST_AUTO_TEST_CASE(TestEvalStateJac)
{
  const bool withDurationSens = false;
  MetaSSEDaeInterface::Ptr metaSSEDae = createRawlingbatchObject(firstForwOrder, withDurationSens);
  metaSSEDae->initializeForNewIntegration(0.0, 1.0);
  NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo = metaSSEDae->GetProblemInfo();
  const int numStates = problemInfo->NumStates();
  const int dim = 4;
  const int lenRedJacNonZeros = 7;
  const double globDuration = 21.3;
  int rows[] = {3,4,5,7};
  int cols[] = {2,3,4,5};
  double states[] = {0.6, 0.06, 1.0, 1.5, 3.05, 0.23, 3.29, 0.063};
  int SparsityPattern[] = {0, 1, 4, 6, 8, 11, 12};
  utils::Array<int> jacRowInd(lenRedJacNonZeros);
  utils::Array<int> jacColInd(lenRedJacNonZeros);
  utils::Array<double> jacVal(lenRedJacNonZeros);
  metaSSEDae->EvalStateJac(problemInfo->StartTime(), states, numStates, rows, cols, dim,
                           SparsityPattern, jacRowInd.getData(), jacColInd.getData(),
                           jacVal.getData(), lenRedJacNonZeros);

 int jacRowIndComp[] = {0, 0, 1, 1, 2, 2, 3};
 int jacColIndComp[] = {0, 1, 0, 2, 0, 3, 0};
 double jacValComp[] = {0.003*globDuration, 1.0*globDuration, 0.01*globDuration,
                        1.0*globDuration, -32.84*globDuration, 1.0*globDuration,
                        -2.0*globDuration};

 BOOST_CHECK_EQUAL_COLLECTIONS(jacRowInd.getData(), jacRowInd.getData()+lenRedJacNonZeros,
                               jacRowIndComp, jacRowIndComp+lenRedJacNonZeros);
 BOOST_CHECK_EQUAL_COLLECTIONS(jacColInd.getData(), jacColInd.getData()+lenRedJacNonZeros,
                               jacColIndComp, jacColIndComp+lenRedJacNonZeros);
 for(int i=0; i<lenRedJacNonZeros; i++){
  BOOST_CHECK_CLOSE(jacVal[i], jacValComp[i], THRESHOLD);
  }
}

BOOST_AUTO_TEST_SUITE_END()
