/**
* @file MultiStageIntegrationNixe.testsuite
* @brief test cases for GenericIntegrator.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Tests of multi-stage integration using a simple artificial 3-stage   \n
* car example. The reference solution was calculated using JADE and \n
* NIXE. The description of the reference model can be found in the     \n
* MATLAB file GenericIntegrator\test\carErweitertMSTest.m              \n
* =====================================================================\n
*
* @author Moritz Schmitz, Tjalf Hoffmann
* @date 2.7.2012
*/

BOOST_AUTO_TEST_SUITE(TestGenericIntegratorMultiStage)

/**
* @test GenericIntegratorTest - test if GenericIntegrator is working the same way as FirstOrder-
*                               ForwardHandler in case of a multistageProblem consisting of 3
*                               stages of the updated car example
*
* All sensitivities: initial Sensitivities, control sensitivities and final time sensitivities are
* tested for an updated car example
* The sensitivities for the initials are only tested for the first stage, since no data is available
*/
/*BOOST_AUTO_TEST_CASE(TestMultiStageFirstOrderForwardAllSens)
{
  IntegratorMultiStageMetaDataDummy::Ptr integratorMSMetaDataDummy;
  integratorMSMetaDataDummy = createMSExampleForZerothAndFirstOrder(CAR_JADE_UPDATED_MODEL_NAME);
  IntegratorMetaData::Ptr integratorMetaData(integratorMSMetaDataDummy);

  const int numStates = 5;
  const int numControlParameters_1 = 5; // equals numIntervals
  const int numInitialParameters_1 = 4; // number of differential states
  const int numControlParameters_2 = 2;
  const int numDurationParameters_2 = 1;
  const int numControlParameters_3 = 4;
  const int numDurationParameters_3 = 1;

  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstForwOrder;
  ForwardIntegratorNixe fof(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(fof.solve());
  std::vector<double> sensAtdurations;
  std::vector<double> sensMapped;

  // The following call gives us all the individual stage sensitivities at the final times of each
  // stage. At each final time only the sensitivities with respect to the parameters defined
  // in the corresponding stage are accounted for. No entries dx_i/dp_j with stage i =/= stage j exist
  integratorMSMetaDataDummy->getSensAtStageDurations(sensAtdurations);
  // In contrast to the previous call, this call gives us all the mapped sensitivities but only at the
  // overall final time and not at intermediate durations as tf_2
  integratorMSMetaDataDummy->getCurrentSensitivities(sensMapped);

  // The values for comparison were calculated by NIXE and the AC-SAMMM mex-Interface
  // for the problem formulated with free final time

  //==================================
  // Comparison at the end of stage 1
  //==================================
  std::vector<double> compControlSensAtDuration_1;
  compControlSensAtDuration_1.resize(numStates*numControlParameters_1);
  std::vector<double> compInitiallSensAtDuration_1;
  compInitiallSensAtDuration_1.resize(numStates*numInitialParameters_1);

  // compare State-Sensitivities with respect to controlparametrization at final time tf_1
  // name convention e.g.: dx_0(tf_1)/dpi_1 => Sensitivity of state x_0 at time tf_1
  //                       with respect to parameter pi_1, where i is the index of the parameter
  //              dx_0(tf_1)/dpi_1                          dx_1(tf_1)/dpi_1
  compControlSensAtDuration_1[0] = 17.688179818296302; compControlSensAtDuration_1[1] = 0.0;
  compControlSensAtDuration_1[5] = 13.797395524817746; compControlSensAtDuration_1[6] = 0.0;
  compControlSensAtDuration_1[10] = 9.899355983491921; compControlSensAtDuration_1[11] = 0.0;
  compControlSensAtDuration_1[15] = 5.964321627807538; compControlSensAtDuration_1[16] = 0.0;
  compControlSensAtDuration_1[20] = 1.995546111776400; compControlSensAtDuration_1[21] = 0.0;

  //              dx_2(tf_1)/dpi_1                           dx_3(tf_1)/dpi_1
  compControlSensAtDuration_1[2] = 1.932120784511089;  compControlSensAtDuration_1[3] = 29.999999999999897;
  compControlSensAtDuration_1[7] = 1.945769167408612;  compControlSensAtDuration_1[8] = 29.999999999999957;
  compControlSensAtDuration_1[12] = 1.962620259046842; compControlSensAtDuration_1[13] = 0.0;
  compControlSensAtDuration_1[17] = 1.978671328738911; compControlSensAtDuration_1[18] = 30.000000000000004;
  compControlSensAtDuration_1[22] = 1.993803442250187; compControlSensAtDuration_1[23] = 0.0;

  //              dx_4(tf_1)/dpi_1
  compControlSensAtDuration_1[4] = 0.0;
  compControlSensAtDuration_1[9] = 0.0;
  compControlSensAtDuration_1[14] = 0.0;
  compControlSensAtDuration_1[19] = 0.0;
  compControlSensAtDuration_1[24] = 0.0;

  for(int i=0; i<numStates*numControlParameters_1; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[numInitialParameters_1 * numStates + i],
                      compControlSensAtDuration_1[i], THRESHOLD);
  }

  // compare State-Sensitivities with respect to initial values of differential states at final Time tf_1
  //              dx_0(tf_1)/dx0i                             dx_1(tf_1)/dx0i
  compInitiallSensAtDuration_1[0] = 1.0;                  compInitiallSensAtDuration_1[1] = 0.0;
  compInitiallSensAtDuration_1[5] = 0.0;                  compInitiallSensAtDuration_1[6] = 1.0;
  compInitiallSensAtDuration_1[10] = 9.820058644372743;   compInitiallSensAtDuration_1[11] = 0.0;
  compInitiallSensAtDuration_1[15] = 0.0;                 compInitiallSensAtDuration_1[16] = 0.0;

  //              dx_2(tf_1)/dx0i                             dx_3(tf_1)/dx0i
  compInitiallSensAtDuration_1[2] = 0.0;                  compInitiallSensAtDuration_1[3] = 0.0;
  compInitiallSensAtDuration_1[7] = 0.0;                  compInitiallSensAtDuration_1[8] = 0.0;
  compInitiallSensAtDuration_1[12] = 0.963514084029561;   compInitiallSensAtDuration_1[13] = 0.0;
  compInitiallSensAtDuration_1[17] = 0.0;                 compInitiallSensAtDuration_1[18] = 1.0;

  //              dx_4(tf_1)/dx0i
  compInitiallSensAtDuration_1[4] = 0.0;
  compInitiallSensAtDuration_1[9] = 0.0;
  compInitiallSensAtDuration_1[14] = 0.0;
  compInitiallSensAtDuration_1[19] = 0.0;

  for(int i=0; i<numStates*numInitialParameters_1; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[i], compInitiallSensAtDuration_1[i], THRESHOLD);
  }

  //==================================
  // Comparison at the end of stage 2
  //==================================
  std::vector<double> compControlSensAtDuration_2;
  compControlSensAtDuration_2.resize(numStates*numControlParameters_2);
  std::vector<double> compFinalSensAtDuration_2;
  compFinalSensAtDuration_2.resize(numStates*numDurationParameters_2);

  // compare State-Sensitivities with respect to controlparametrization at final time tf_2
  //              dx_0(tf_2)/dpi_2                       dx_1(tf_2)/dpi_2
  compControlSensAtDuration_2[0] = 9.294863395006297; compControlSensAtDuration_2[1] = 0.0;
  compControlSensAtDuration_2[5] = 3.109133050460827; compControlSensAtDuration_2[6] = 0.0;

  //              dx_2(tf_2)/dpi_2                       dx_3(tf_2)/dpi_2
  compControlSensAtDuration_2[2] = 2.450735725193732; compControlSensAtDuration_2[3] = 37.500000000000114;
  compControlSensAtDuration_2[7] = 2.479038849417310; compControlSensAtDuration_2[8] = 37.500000000000078;

  //              dx_4(tf_2)/dpi_2
  compControlSensAtDuration_2[4] = 0.0;
  compControlSensAtDuration_2[9] = 15.0;

  for(int i=0; i<numStates*numControlParameters_2; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1 + numControlParameters_1) * numStates + i], compControlSensAtDuration_2[i], THRESHOLD);
  }

  // compare State-Sensitivities with respect to final time parameter tf_2
  //              dx_0(tf_2)/dtf_2                          dx_1(tf_2)/dtf_2
  compFinalSensAtDuration_2[0] = 9.589211381598481; compFinalSensAtDuration_2[1] = 1.000000000000001;
  //              dx_2(tf_2)/dtf_2                          dx_3(tf_2)/dtf_2
  compFinalSensAtDuration_2[2] = 1.474869443122393; compFinalSensAtDuration_2[3] = 23.649999999999920;
  //              dx_4(tf_2)/dtf_2
  compFinalSensAtDuration_2[4] = 0.0;

  for(int i=0; i<numStates*numDurationParameters_2; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1 + numControlParameters_1
                                       + numControlParameters_2)*numStates + i],
                                       compFinalSensAtDuration_2[i], THRESHOLD);
  }
  //==================================
  // Comparison at the end of stage 3
  //==================================
  std::vector<double> compStatesAtDuration_3;
  compStatesAtDuration_3.resize(numStates);
  std::vector<double> compControlSensAtDuration_3;
  compControlSensAtDuration_3.resize(numStates*(numControlParameters_1+numControlParameters_2+numControlParameters_3));
  std::vector<double> compInitiallSensAtDuration_3;
  compInitiallSensAtDuration_3.resize(numStates*numInitialParameters_1);
  std::vector<double> compFinalSensAtDuration_3;
  compFinalSensAtDuration_3.resize(numStates*(numDurationParameters_2+numDurationParameters_3));

  // compare States at final time tf_3
  //              x_i(tf_3)
  compStatesAtDuration_3[0] = 233.7537763811496;
  compStatesAtDuration_3[1] = 31.0000000000003;
  compStatesAtDuration_3[2] = 11.7420726759880;
  compStatesAtDuration_3[3] = 248.7499999999975;
  compStatesAtDuration_3[4] = 1.0;

  utils::Array<double> finalStates_3(numStates);
  integratorMetaData->getGenericEso()->getStateValues(finalStates_3.getSize(), finalStates_3.getData());

  for(int i=0; i<numStates; i++){
    BOOST_CHECK_CLOSE(finalStates_3[i], compStatesAtDuration_3[i], THRESHOLD);
  }

  // compare State-Sensitivities with respect to controlparametrization at final time tf_3
  //              dx_0(tf_3)/dpi_1                       dx_1(tf_3)/dpi_1
  compControlSensAtDuration_3[0] = 48.775167411198545;  compControlSensAtDuration_3[1] = 0.0;
  compControlSensAtDuration_3[5] = 45.103979694290203;  compControlSensAtDuration_3[6] = 0.0;
  compControlSensAtDuration_3[10] = 41.477066927689819; compControlSensAtDuration_3[11] = 0.0;
  compControlSensAtDuration_3[15] = 37.800287339704319; compControlSensAtDuration_3[16] = 0.0;
  compControlSensAtDuration_3[20] = 34.074980983960927; compControlSensAtDuration_3[21] = 0.0;
  //              dx_0(tf_3)/dpi_2                       dx_1(tf_3)/dpi_2
  compControlSensAtDuration_3[25] = 37.249203914466221; compControlSensAtDuration_3[26] = 0.0;
  compControlSensAtDuration_3[30] = 31.386313427428540; compControlSensAtDuration_3[31] = 0.0;
  //              dx_0(tf_3)/dpi_3                       dx_1(tf_3)/dpi_3
  compControlSensAtDuration_3[35] = 30.098813008842466; compControlSensAtDuration_3[36] = 0.0;
  compControlSensAtDuration_3[40] = 21.736974407819389; compControlSensAtDuration_3[41] = 0.0;
  compControlSensAtDuration_3[45] = 13.203262990102093; compControlSensAtDuration_3[46] = 0.0;
  compControlSensAtDuration_3[50] = 4.458727204723401;  compControlSensAtDuration_3[51] = 0.0;

  //              dx_2(tf_3)/dpi_1                       dx_3(tf_3)/dpi_1
  compControlSensAtDuration_3[2] = 1.697104542507507;   compControlSensAtDuration_3[3] = 29.999999999999897;
  compControlSensAtDuration_3[7] = 1.709092785064058;   compControlSensAtDuration_3[8] = 29.999999999999957;
  compControlSensAtDuration_3[12] = 1.723894170357730;  compControlSensAtDuration_3[13] = 0.0;
  compControlSensAtDuration_3[17] = 1.737992845505199;  compControlSensAtDuration_3[18] = 30.000000000000004;
  compControlSensAtDuration_3[22] = 1.751284343005557;  compControlSensAtDuration_3[23] = 0.0;
  //              dx_2(tf_3)/dpi_2                       dx_3(tf_3)/dpi_2
  compControlSensAtDuration_3[27] = 2.204601415647613;  compControlSensAtDuration_3[28] = 37.500000000000114;
  compControlSensAtDuration_3[32] = 2.230061977179862;  compControlSensAtDuration_3[33] = 37.500000000000078;
  //              dx_2(tf_3)/dpi_3                       dx_3(tf_3)/dpi_3
  compControlSensAtDuration_3[37] = 2.731488945713034;  compControlSensAtDuration_3[38] = 44.999999999999922;
  compControlSensAtDuration_3[42] = 2.799757395988442;  compControlSensAtDuration_3[43] = 45.000000000000043;
  compControlSensAtDuration_3[47] = 2.876116680921337;  compControlSensAtDuration_3[48] = 45.000000000000000;
  compControlSensAtDuration_3[52] = 2.959973550466088;  compControlSensAtDuration_3[53] = 0.0;

  //              dx_4(tf_3)/dpi_1
  compControlSensAtDuration_3[4] = 0.0;
  compControlSensAtDuration_3[9] = 0.0;
  compControlSensAtDuration_3[14] = 0.0;
  compControlSensAtDuration_3[19] = 0.0;
  compControlSensAtDuration_3[24] = 0.0;
  //              dx_4(tf_3)/dpi_2
  compControlSensAtDuration_3[29] = 0.0;
  compControlSensAtDuration_3[34] = 0.0;
  //              dx_4(tf_3)/dpi_3
  compControlSensAtDuration_3[39] = 0.0;
  compControlSensAtDuration_3[44] = 0.0;
  compControlSensAtDuration_3[49] = 0.0;
  compControlSensAtDuration_3[54] = 0.0;

  for(int i=0; i<numStates*numControlParameters_1; i++){
    BOOST_CHECK_CLOSE(sensMapped[numInitialParameters_1*numStates + i],
                      compControlSensAtDuration_3[i],THRESHOLD);
  }

  for(int i=0; i<numStates*numControlParameters_2; i++){
    BOOST_CHECK_CLOSE(sensMapped[(numInitialParameters_1+numControlParameters_1)*numStates + i],
                      compControlSensAtDuration_3[numStates*numControlParameters_1+i],THRESHOLD);
  }

  for(int i=0; i<numStates*numControlParameters_3; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1+numControlParameters_1+numDurationParameters_2
		              +numControlParameters_2)*numStates + i],
                      compControlSensAtDuration_3[(numControlParameters_1+numControlParameters_2)
                      *numStates + i],THRESHOLD);
  }

  // compare State-Sensitivities with respect to initial values of differential states at final time tf_3
  //              dx_0(tf_3)/dx0i_1                        dx_1(tf_3)/dx0i_1
  compInitiallSensAtDuration_3[0] = 1.0;                  compInitiallSensAtDuration_3[1] = 0.0;
  compInitiallSensAtDuration_3[5] = 0.0;                  compInitiallSensAtDuration_3[6] = 1.0;
  compInitiallSensAtDuration_3[10] = 25.322583442993398;  compInitiallSensAtDuration_3[11] = 0.0;
  compInitiallSensAtDuration_3[15] = 0.0;                 compInitiallSensAtDuration_3[16] = 0.0;

  //              dx_2(tf_3)/dx0i_1                       dx_3(tf_3)/dx0i_1
  compInitiallSensAtDuration_3[2] = 0.0;                 compInitiallSensAtDuration_3[3] = 0.0;
  compInitiallSensAtDuration_3[7] = 0.0;                 compInitiallSensAtDuration_3[8] = 0.0;
  compInitiallSensAtDuration_3[12] = 0.846315686827158;  compInitiallSensAtDuration_3[13] = 0.0;
  compInitiallSensAtDuration_3[17] = 0.0;                compInitiallSensAtDuration_3[18] = 1.0;

  //              dx_4(tf_3)/dx0i_1
  compInitiallSensAtDuration_3[4] = 0.0;
  compInitiallSensAtDuration_3[9] = 0.0;
  compInitiallSensAtDuration_3[14] = 0.0;
  compInitiallSensAtDuration_3[19] = 0.0;

  for(int i=0; i<numStates*numInitialParameters_1; i++){
    BOOST_CHECK_CLOSE(sensMapped[i], compInitiallSensAtDuration_3[i], THRESHOLD);
  }

  // compare State-Sensitivities with respect to final time parameter tf_2 (!)
  //              dx_0(tf_3)/dtf_2                          dx_1(tf_3)/dtf_2
  compFinalSensAtDuration_3[0] = 26.412323809002512;     compFinalSensAtDuration_3[1] = 1.000000000000001;
  //              dx_2(tf_3)/dtf_2                          dx_3(tf_3)/dtf_2
  compFinalSensAtDuration_3[2] = 1.326744140046361;      compFinalSensAtDuration_3[3] = 23.649999999999920;
  //              dx_4(tf_3)/dtf_2
  compFinalSensAtDuration_3[4] = 0.0;

  // compare State-Sensitivities with respect to final time parameter tf_3
  //              dx_0(tf_3)/dtf_3                          dx_1(tf_3)/dtf_3
  compFinalSensAtDuration_3[5] = 13.356287544824083; compFinalSensAtDuration_3[6] = 1.000000000000007;
  //              dx_2(tf_3)/dtf_3                          dx_3(tf_3)/dtf_3
  compFinalSensAtDuration_3[7] = 0.042879945432832;  compFinalSensAtDuration_3[8] = 5.124999999999988;
  //              dx_4(tf_3)/dtf_3
  compFinalSensAtDuration_3[9] = 0.0;

  for(int i=0; i<numStates*numDurationParameters_2; i++){
    BOOST_CHECK_CLOSE(sensMapped[(numInitialParameters_1 + numControlParameters_1
                                        + numControlParameters_2) * numStates + i],
                                          compFinalSensAtDuration_3[i], THRESHOLD);
  }

  for(int i=0; i<numStates*numDurationParameters_3; i++){
    BOOST_CHECK_CLOSE(sensAtdurations[(numInitialParameters_1 + numControlParameters_1 +
      numControlParameters_2  +numDurationParameters_2 +
      numControlParameters_3) * numStates + i],
      compFinalSensAtDuration_3[numStates * numDurationParameters_2 + i], THRESHOLD);
  }
}
*/
/**
* @test GenericIntegratorTest - compare a single stage reverse integration
                                with an equivalent multistage reverse integration
*/
BOOST_AUTO_TEST_CASE(TestFirstOrderReverse)
{
  //set up and integrate single stage problem
  IntegratorMetaData::Ptr integratorMetaData;
  {
    const int numInitials = 3;
    const int initialEsoIndices[3] = {0, 1, 2};
    const double initialValues[3] = {0.5, 0.05, 0.0};
    const double duration = 21.0;
    const int controlEsoIndex = 12; // corresponds to parameter 'K1' in mof-file
    const int numControlIntervals = 2;
    utils::Array<double> controlParams(numControlIntervals);
    for(int i=0; i<numControlIntervals; i++){
      controlParams[i] = double(i)/10.0 + 0.4;
    }
    const double controlLB = -10.0;
    const double controlUB = 10.0;
    const int numConstraints = 7;
    utils::Array<int> constraintEsoIndices(numConstraints);
    utils::Array<double> constraintTimes(numConstraints);
    utils::Array<double> LagrMultipliers(numConstraints);
    for(int i=0; i< numConstraints; i++){
      constraintEsoIndices[i] = 1;// cB
      constraintTimes[i] = double(i)/(numConstraints-1);
      LagrMultipliers[i] = 0.0733*i*i - 0.4*(i+1);
    }
    const bool initialsWithSensitivity = true;
    const bool controlWithSensitivity = true;
    bool durationWithSensitivity = true;

    FactoryInput::IntegratorMetaDataInput integratorSSMetaDataInput;
    integratorSSMetaDataInput = createInputForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                       numInitials,
                                                       initialEsoIndices,
                                                       initialValues,
                                                       initialsWithSensitivity,
                                                       duration,
                                                       durationWithSensitivity,
                                                       controlEsoIndex,
                                                       numControlIntervals,
                                                       controlParams.getData(),
                                                       controlLB,
                                                       controlUB,
                                                       controlWithSensitivity,
                                                       numConstraints,
                                                       constraintEsoIndices.getData(),
                                                       constraintTimes.getData(),
                                                       LagrMultipliers.getData());
    integratorSSMetaDataInput.controls.front().grids.resize(3);
    FactoryInput::ParameterizationGridInput grid1 = integratorSSMetaDataInput.controls.front().grids[0];
    FactoryInput::ParameterizationGridInput grid2 = grid1;
    FactoryInput::ParameterizationGridInput grid3 = grid2;

    grid1.duration = duration/3;
    grid2.duration = duration/3;
    grid3.duration = duration/3;
    grid1.hasFreeDuration = true;
    grid2.hasFreeDuration = true;
    grid3.hasFreeDuration = true;
    for(unsigned i=0; i<grid2.values.size(); i++){
      grid2.values[i] += (grid1.values.size())*0.1;
      grid3.values[i] += (grid1.values.size() + grid2.values.size())*0.1;
    }
    integratorSSMetaDataInput.controls.front().grids[0] = grid1;
    integratorSSMetaDataInput.controls.front().grids[1] = grid2;
    integratorSSMetaDataInput.controls.front().grids[2] = grid3;
    integratorSSMetaDataInput.userGrid.clear();

    IntegratorMetaDataFactory imdFac;
    IntegratorSingleStageMetaData::Ptr issmd(imdFac.createIntegratorSingleStageMetaData(integratorSSMetaDataInput));
    for(int i=0; i<numConstraints; i++){
      issmd->addNonlinearConstraint(constraintEsoIndices[i], 0, constraintTimes[i],
                                                 LagrMultipliers[i]);
    }
    integratorMetaData = issmd;

  }// single stage problem has been set up

  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  
  ForwardIntegratorNixe::NixeOptions options;
  options.order = firstRevOrder;
  ReverseIntegratorNixe foRev(integratorMetaData, options);
  BOOST_CHECK_NO_THROW(foRev.solve());

  std::vector<double> firstOrderLagrangeDersSingleStage;
  integratorMetaData->getLagrangeDers(firstOrderLagrangeDersSingleStage);


  //set up and integrate multi stage problem
  IntegratorMultiStageMetaData::Ptr imsMetaData;
  {
    IntegratorSingleStageMetaData::Ptr stage1, stage2, stage3;
    //general data for all three stages
    const double duration = 7.0;
    const int controlEsoIndex = 12; // corresponds to parameter 'K1' in mof-file
    int numControlIntervals = 2;
    const double controlLB = -10.0;
    const double controlUB = 10.0;
    const int numConstraints = 3;
    utils::Array<double> controlParams(numControlIntervals);
    utils::Array<int> constraintEsoIndices(numConstraints);
    utils::Array<double> constraintTimes(numConstraints);
    utils::Array<double> LagrMultipliers(numConstraints);
    const bool controlWithSensitivity = true;
    const bool durationWithSensitivity = true;
    bool initialsWithSensitivity = false;

    //set up stage 1
    {
      const int numInitials = 3;
      const int initialEsoIndices[3] = {0, 1, 2};
      const double initialValues[3] = {0.5, 0.05, 0.0};
      for(int i=0; i<numControlIntervals; i++){
        controlParams[i] = double(i)/10.0 + 0.4;
      }
      for(int i=0; i< numConstraints; i++){
        constraintEsoIndices[i] = 1;// cB
        constraintTimes[i] = double(i)/(numConstraints-1);
        LagrMultipliers[i] = 0.0733*i*i - 0.4*(i+1);
      }
      initialsWithSensitivity = true;

      stage1 = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                         numInitials,
                                                         initialEsoIndices,
                                                         initialValues,
                                                         initialsWithSensitivity,
                                                         duration,
                                                         durationWithSensitivity,
                                                         controlEsoIndex,
                                                         numControlIntervals,
                                                         controlParams.getData(),
                                                         controlLB,
                                                         controlUB,
                                                         controlWithSensitivity,
                                                         numConstraints,
                                                         constraintEsoIndices.getData(),
                                                         constraintTimes.getData(),
                                                         LagrMultipliers.getData());
    }// set up stage1

    // set up stage2
    {
      //initials only for the first stage
      const int numInitials = 0;
      int *initialEsoIndices = NULL;
      double *initialValues = NULL;
      for(int i=0; i<numControlIntervals; i++){
        controlParams[i] = double(i)/10.0 + 0.6;
      }
      //leave out constraint for 0.0
      for(int i=0; i< numConstraints-1; i++){
        constraintEsoIndices[i] = 1;// cB
        constraintTimes[i] = double(i+1)/(numConstraints-1);
        LagrMultipliers[i] = 0.0733*(i+3)*(i+3) - 0.4*(i+4);
      }

      stage2 = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                         numInitials,
                                                         initialEsoIndices,
                                                         initialValues,
                                                         initialsWithSensitivity,
                                                         duration,
                                                         durationWithSensitivity,
                                                         controlEsoIndex,
                                                         numControlIntervals,
                                                         controlParams.getData(),
                                                         controlLB,
                                                         controlUB,
                                                         controlWithSensitivity,
                                                         numConstraints-1,
                                                         constraintEsoIndices.getData(),
                                                         constraintTimes.getData(),
                                                         LagrMultipliers.getData());
    }// set up stage2

    // set up stage3
    {
     //initials only for the first stage
      const int numInitials = 0;
      int *initialEsoIndices = NULL;
      double *initialValues = NULL;
      for(int i=0; i<numControlIntervals; i++){
        controlParams[i] = double(i)/10.0 + 0.8;
      }
      //leave out constraint for 0.0
      for(int i=0; i< numConstraints-1; i++){
        constraintEsoIndices[i] = 1;// cB
        constraintTimes[i] = double(i+1)/(numConstraints-1);
        LagrMultipliers[i] = 0.0733*(i+5)*(i+5) - 0.4*(i+6);
      }

      stage3 = createExampleForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                         numInitials,
                                                         initialEsoIndices,
                                                         initialValues,
                                                         initialsWithSensitivity,
                                                         duration,
                                                         durationWithSensitivity,
                                                         controlEsoIndex,
                                                         numControlIntervals,
                                                         controlParams.getData(),
                                                         controlLB,
                                                         controlUB,
                                                         controlWithSensitivity,
                                                         numConstraints-1,
                                                         constraintEsoIndices.getData(),
                                                         constraintTimes.getData(),
                                                         LagrMultipliers.getData());
    }// set up stage3

    //create the multi stage metadata
    std::vector<IntegratorSingleStageMetaData::Ptr> stages;
    stages.push_back(stage1);
    stages.push_back(stage2);
    stages.push_back(stage3);

    //create a full state mapping
    std::vector<Mapping::Ptr> mappings;
    for(unsigned i=0; i<stages.size()-1; i++){
      mappings.push_back(Mapping::Ptr(new SingleShootingMapping()));
      const unsigned numVars = stage1->getGenericEso()->getNumVariables();
      iiMap varIndexMap;
      for(unsigned j=0; j<numVars; j++){
        varIndexMap[j] = j;
      }
      const unsigned numEqns = stage1->getGenericEso()->getNumEquations();
      iiMap eqnIndexMap;
      for(unsigned j=0; j<numEqns; j++){
        eqnIndexMap[j] = j;
      }
      mappings[i]->setIndexMaps(varIndexMap, eqnIndexMap);
    }
    imsMetaData = IntegratorMultiStageMetaData::Ptr(new IntegratorMultiStageMetaData(mappings, stages));
  }// set up multistage problem



  // conversion from IntegratorSSMetaDataPtr to IntegratorMetaDataPtr
  IntegratorMetaData::Ptr integratorMSMetaData(imsMetaData);

  options.order = firstRevOrder;
  ReverseIntegratorNixe foMSRev(integratorMSMetaData, options);
  BOOST_CHECK_NO_THROW(foMSRev.solve());

  std::vector<double> firstOrderLagrangeDersMultiStage;
  integratorMSMetaData->getLagrangeDers(firstOrderLagrangeDersMultiStage);

  BOOST_REQUIRE_EQUAL(firstOrderLagrangeDersSingleStage.size(),
                      firstOrderLagrangeDersMultiStage.size());

  // relax threshold a bit, because some values are very close to zero
  // and numerical errors produce rather large relative errors there
  // due to a different sorting of parameters in single stage and multi stage
  // use index vector to map single stage to multi stage
  std::vector<unsigned> indexVector(firstOrderLagrangeDersSingleStage.size());
  BOOST_REQUIRE_EQUAL(firstOrderLagrangeDersSingleStage.size(), 12);
  // in single stage all controls and initial value parameters are first
  // the final time parameters are at the end
  //indices for iniitials
  indexVector[0] = 0;
  indexVector[1] = 1;
  indexVector[2] = 2;
  //then the three grids (each with two parameters)
  indexVector[3] = 3;
  indexVector[4] = 4;
  indexVector[5] = 6;
  indexVector[6] = 7;
  indexVector[7] = 9;
  indexVector[8] = 10;
  //then the three final time parameters
  indexVector[9] = 5;
  indexVector[10] = 8;
  indexVector[11] = 11;
  
  for(unsigned i=0; i<firstOrderLagrangeDersSingleStage.size(); i++){
    BOOST_CHECK_CLOSE(firstOrderLagrangeDersSingleStage[i],
                      firstOrderLagrangeDersMultiStage[indexVector[i]], THRESHOLD*4);
  }
}

/**
* @test GenericIntegratorTest - compare a single stage second order reverse integration
                                with an equivalent multistage second order reverse integration
*/
BOOST_AUTO_TEST_CASE(TestSecondOrderReverse)
{
 //set up and integrate single stage problem
  IntegratorMetaData2ndOrder::Ptr integratorMetaData2ndOrder;
  {
    const int numInitials = 3;
    const int initialEsoIndices[3] = {0, 1, 2};
    const double initialValues[3] = {0.5, 0.05, 0.0};
    const double duration = 21.0;
    const int controlEsoIndex = 12; // corresponds to parameter 'K1' in mof-file
    const int numControlIntervals = 2;
    utils::Array<double> controlParams(numControlIntervals);
    for(int i=0; i<numControlIntervals; i++){
      controlParams[i] = double(i)/10.0 + 0.4;
    }
    const double controlLB = -10.0;
    const double controlUB = 10.0;
    const int numConstraints = 7;
    utils::Array<int> constraintEsoIndices(numConstraints);
    utils::Array<double> constraintTimes(numConstraints);
    utils::Array<double> LagrMultipliers(numConstraints);
    for(int i=0; i< numConstraints; i++){
      constraintEsoIndices[i] = 1;// cB
      constraintTimes[i] = double(i)/(numConstraints-1);
      LagrMultipliers[i] = 0.0733*i*i - 0.4*(i+1);
    }
    const bool initialsWithSensitivity = true;
    const bool controlWithSensitivity = true;
    const bool durationWithSensitivity = true;

    FactoryInput::IntegratorMetaDataInput integratorSSMetaDataInput;
    integratorSSMetaDataInput = createInputForIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                       numInitials,
                                                       initialEsoIndices,
                                                       initialValues,
                                                       initialsWithSensitivity,
                                                       duration,
                                                       durationWithSensitivity,
                                                       controlEsoIndex,
                                                       numControlIntervals,
                                                       controlParams.getData(),
                                                       controlLB,
                                                       controlUB,
                                                       controlWithSensitivity,
                                                       numConstraints,
                                                       constraintEsoIndices.getData(),
                                                       constraintTimes.getData(),
                                                       LagrMultipliers.getData());
    integratorSSMetaDataInput.controls.front().grids.resize(3);
    FactoryInput::ParameterizationGridInput grid1 = integratorSSMetaDataInput.controls.front().grids[0];
    FactoryInput::ParameterizationGridInput grid2 = grid1;
    FactoryInput::ParameterizationGridInput grid3 = grid2;

    grid1.duration = duration/3;
    grid2.duration = duration/3;
    grid3.duration = duration/3;
    grid1.hasFreeDuration = true;
    grid2.hasFreeDuration = true;
    grid3.hasFreeDuration = true;
    for(unsigned i=0; i<grid2.values.size(); i++){
      grid2.values[i] += (grid1.values.size())*0.1;
      grid3.values[i] += (grid1.values.size() + grid2.values.size())*0.1;
    }
    integratorSSMetaDataInput.controls.front().grids[0] = grid1;
    integratorSSMetaDataInput.controls.front().grids[1] = grid2;
    integratorSSMetaDataInput.controls.front().grids[2] = grid3;
    integratorSSMetaDataInput.userGrid.clear();

    IntegratorMetaDataFactory imdFac;
    IntegratorSSMetaData2ndOrder::Ptr issmd(imdFac.createIntegratorSSMetaData2ndOrder(integratorSSMetaDataInput));
    for(int i=0; i<numConstraints; i++){
      issmd->addNonlinearConstraint(constraintEsoIndices[i], 0, constraintTimes[i],
                                                 LagrMultipliers[i]);
    }
    integratorMetaData2ndOrder = issmd;

  }// single stage problem has been set up

  // conversion from IntegratorSSMD2ndOrderPtr to IntegratorMD2ndOrderPtr
  IntegratorMetaData2ndOrder::Ptr  integratorMD2ndOrder(integratorMetaData2ndOrder);

  ForwardIntegratorNixe::NixeOptions options;
  options.order = secondRevOrder;
  Rev2ndOrdIntegratorNixe soRev(integratorMD2ndOrder, options);
  BOOST_CHECK_NO_THROW(soRev.solve());

  std::vector<double> firstAndSecOrderLagrangeDersSingleStage;
  integratorMD2ndOrder->getLagrangeDers(firstAndSecOrderLagrangeDersSingleStage);


   //set up and integrate multi stage problem
  IntegratorMetaData2ndOrder::Ptr integratorMSMetaData2ndOrder;
  {
    IntegratorSSMetaData2ndOrder::Ptr stage1, stage2, stage3;
    //general data for all three stages
    const double duration = 7.0;
    const int controlEsoIndex = 12; // corresponds to parameter 'K1' in mof-file
    const int numControlIntervals = 2;
    const double controlLB = -10.0;
    const double controlUB = 10.0;
    const int numConstraints = 3;
    utils::Array<double> controlParams(numControlIntervals);
    utils::Array<int> constraintEsoIndices(numConstraints);
    utils::Array<double> constraintTimes(numConstraints);
    utils::Array<double> LagrMultipliers(numConstraints);
    const bool controlWithSensitivity = true;
    const bool durationWithSensitivity = true;
    bool initialsWithSensitivity = false;

    //set up stage 1
    {
      const int numInitials = 3;
      const int initialEsoIndices[3] = {0, 1, 2};
      const double initialValues[3] = {0.5, 0.05, 0.0};
      for(int i=0; i<numControlIntervals; i++){
        controlParams[i] = double(i)/10.0 + 0.4;
      }
      for(int i=0; i< numConstraints; i++){
        constraintEsoIndices[i] = 1;// cB
        constraintTimes[i] = double(i)/(numConstraints-1);
        LagrMultipliers[i] = 0.0733*i*i - 0.4*(i+1);
      }
      initialsWithSensitivity = true;

      stage1 = createExampleFor2ndOrdIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                         numInitials,
                                                         initialEsoIndices,
                                                         initialValues,
                                                         initialsWithSensitivity,
                                                         duration,
                                                         durationWithSensitivity,
                                                         controlEsoIndex,
                                                         numControlIntervals,
                                                         controlParams.getData(),
                                                         controlLB,
                                                         controlUB,
                                                         controlWithSensitivity,
                                                         numConstraints,
                                                         constraintEsoIndices.getData(),
                                                         constraintTimes.getData(),
                                                         LagrMultipliers.getData());
    }// set up stage1

    // set up stage2
    {
      //initials only for the first stage
      const int numInitials = 0;
      int *initialEsoIndices = NULL;
      double *initialValues = NULL;
      for(int i=0; i<numControlIntervals; i++){
        controlParams[i] = double(i)/10.0 + 0.6;
      }
      //leave out constraint for 0.0
      for(int i=0; i< numConstraints-1; i++){
        constraintEsoIndices[i] = 1;// cB
        constraintTimes[i] = double(i+1)/(numConstraints-1);
        LagrMultipliers[i] = 0.0733*(i+3)*(i+3) - 0.4*(i+4);
      }

      stage2 = createExampleFor2ndOrdIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                         numInitials,
                                                         initialEsoIndices,
                                                         initialValues,
                                                         initialsWithSensitivity,
                                                         duration,
                                                         durationWithSensitivity,
                                                         controlEsoIndex,
                                                         numControlIntervals,
                                                         controlParams.getData(),
                                                         controlLB,
                                                         controlUB,
                                                         controlWithSensitivity,
                                                         numConstraints-1,
                                                         constraintEsoIndices.getData(),
                                                         constraintTimes.getData(),
                                                         LagrMultipliers.getData());
    }// set up stage2

    // set up stage3
    {
     //initials only for the first stage
      const int numInitials = 0;
      int *initialEsoIndices = NULL;
      double *initialValues = NULL;
      for(int i=0; i<numControlIntervals; i++){
        controlParams[i] = double(i)/10.0 + 0.8;
      }
      //leave out constraint for 0.0
      for(int i=0; i< numConstraints-1; i++){
        constraintEsoIndices[i] = 1;// cB
        constraintTimes[i] = double(i+1)/(numConstraints-1);
        LagrMultipliers[i] = 0.0733*(i+5)*(i+5) - 0.4*(i+6);
      }

      stage3 = createExampleFor2ndOrdIntegration(RAWLINGBATCH_JADE_MODEL_NAME,
                                                         numInitials,
                                                         initialEsoIndices,
                                                         initialValues,
                                                         initialsWithSensitivity,
                                                         duration,
                                                         durationWithSensitivity,
                                                         controlEsoIndex,
                                                         numControlIntervals,
                                                         controlParams.getData(),
                                                         controlLB,
                                                         controlUB,
                                                         controlWithSensitivity,
                                                         numConstraints-1,
                                                         constraintEsoIndices.getData(),
                                                         constraintTimes.getData(),
                                                         LagrMultipliers.getData());
    }// set up stage3

    //create the multi stage metadata
    std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages;
    stages.push_back(stage1);
    stages.push_back(stage2);
    stages.push_back(stage3);

    //create a full state mapping
    std::vector<Mapping::Ptr> mappings;
    for(unsigned i=0; i<stages.size()-1; i++){
      mappings.push_back(Mapping::Ptr(new SingleShootingMapping()));
      const unsigned numVars = stage1->getGenericEso()->getNumVariables();
      iiMap varIndexMap;
      for(unsigned j=0; j<numVars; j++){
        varIndexMap[j] = j;
      }
      const unsigned numEqns = stage1->getGenericEso()->getNumEquations();
      iiMap eqnIndexMap;
      for(unsigned j=0; j<numEqns; j++){
        eqnIndexMap[j] = j;
      }
      mappings[i]->setIndexMaps(varIndexMap, eqnIndexMap);
    }
    integratorMSMetaData2ndOrder = IntegratorMetaData2ndOrder::Ptr(new IntegratorMSMetaData2ndOrder(mappings, stages));
  }// set up multistage problem

  options.order = secondRevOrder;
  Rev2ndOrdIntegratorNixe soRevMS(integratorMSMetaData2ndOrder, options);
  BOOST_CHECK_NO_THROW(soRevMS.solve());



  std::vector<double> firstAndSecOrderLagrangeDersMultiStage;
  integratorMSMetaData2ndOrder->getLagrangeDers(firstAndSecOrderLagrangeDersMultiStage);

  BOOST_REQUIRE_EQUAL(firstAndSecOrderLagrangeDersSingleStage.size(),
                      firstAndSecOrderLagrangeDersMultiStage.size());

  // relax threshold a bit, because some values are very close to zero
  // and numerical errors produce rather large relative errors there
  // the numerical error for the 2ndOrder derivatives seem to be greater than for
  // the 1st order derivatives. Thus the threshold has to be relaxed even more
  // due to a different sorting of parameters in single stage and multi stage
  // use index vector to map single stage to multi stage
  std::vector<unsigned> indexVector(firstAndSecOrderLagrangeDersSingleStage.size());
  BOOST_REQUIRE_EQUAL(firstAndSecOrderLagrangeDersSingleStage.size(), 156);
  // in single stage all controls and initial value parameters are first
  // the final time parameters are at the end
  //indices for iniitials
  indexVector[0] = 0;
  indexVector[1] = 1;
  indexVector[2] = 2;
  //then the three grids (each with two parameters)
  indexVector[3] = 3;
  indexVector[4] = 4;
  indexVector[5] = 6;//here skip first duration
  indexVector[6] = 7;
  indexVector[7] = 9;//here skip second duration
  indexVector[8] = 10;
  //then the three final time parameters
  indexVector[9] = 5;
  indexVector[10] = 8;
  indexVector[11] = 11;
  
  //make changes to 2ndOrder derivatives analogous to first order
  for(unsigned i=1; i<=12; i++){
    for(unsigned j=0; j<12; j++){
      //change rows and colums order - so apply indexvector to both
      indexVector[12*i+j] = 12*(indexVector[i-1]+1) +indexVector[j];
    }
  }
  for(unsigned i=0; i<firstAndSecOrderLagrangeDersSingleStage.size(); i++){
    BOOST_CHECK_CLOSE(firstAndSecOrderLagrangeDersSingleStage[i],
                      firstAndSecOrderLagrangeDersMultiStage[indexVector[i]], THRESHOLD*30);
  }
}

BOOST_AUTO_TEST_SUITE_END()
