#include "EsoAcsammm_Header.hpp"

void res(double * yy, 
double* der_x, double * x, 
double* p,
int* condit, int* lock, int* prev,
int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double par_eta=0;
	double par_cA0=0;
	double par_cB0=0;
	double par_cC0=0;
	double par_K1=0;
	double par_K2=0;
	double par_K3=0;
	double par_K4=0;
	double par_R=0;
	double var_cA=0;
	double der_var_cA=0;
	double var_cB=0;
	double der_var_cB=0;
	double var_cC=0;
	double der_var_cC=0;
	double var_r1=0;
	double der_var_r1=0;
	double var_r2=0;
	double der_var_r2=0;
	double var_y=0;
	double der_var_y=0;
	double var_summand=0;
	double der_var_summand=0;
	double var_summandstart=0;
	double der_var_summandstart=0;
	double var_i_expr=0; // assign var
	double var_div0=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	par_eta = p[0];
	par_cA0 = p[1];
	par_cB0 = p[2];
	par_cC0 = p[3];
	par_K1 = p[4];
	par_K2 = p[5];
	par_K3 = p[6];
	par_K4 = p[7];
	par_R = p[8];
	var_cA = x[0];
	der_var_cA = der_x[0];
	var_cB = x[1];
	der_var_cB = der_x[1];
	var_cC = x[2];
	der_var_cC = der_x[2];
	var_r1 = x[3];
	der_var_r1 = der_x[3];
	var_r2 = x[4];
	der_var_r2 = der_x[4];
	var_y = x[5];
	der_var_y = der_x[5];
	var_summand = x[6];
	der_var_summand = der_x[6];
	var_summandstart = x[7];
	der_var_summandstart = der_x[7];
// temporary assignment i_expr not here
// procedure assignment div0 not here

// #pragma dcc equation begin
// scalar equation 0
yy[i_E] = der_var_cA - (-var_r1);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 1
yy[i_E] = der_var_cB - (var_r1-2 * var_r2);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 2
yy[i_E] = der_var_cC - (var_r1+var_r2);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 3
yy[i_E] = var_r1 - (par_K1 * var_cA-par_K2 * var_cB * var_cC);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 4
yy[i_E] = var_r2 - (par_K3 * var_cB * var_cB-par_K4 * var_cC);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 5
yy[i_E] = var_y - (32.84 *  ( var_cA+var_cB+var_cC ) );
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 6
var_i_expr= ( var_y-par_eta ) ;
acs_div(var_i_expr, par_R, var_div0);
yy[i_E] = var_summand - ( ( var_y-par_eta )  * var_div0);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 7
yy[i_E] = var_summandstart - ( ( var_cA-par_cA0 )  *  ( var_cA-par_cA0 ) + ( var_cB-par_cB0 )  *  ( var_cB-par_cB0 ) + ( var_cC-par_cC0 )  *  ( var_cC-par_cC0 ) );
i_E = i_E+1;
// #pragma dcc equation end

n_c=i_C;
}
