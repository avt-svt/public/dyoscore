#ifndef __ESOACSAMMMHEADER_HPP__
#define __ESOACSAMMMHEADER_HPP__

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "EsoCommon_Header.hpp"


// Determine whether system is Windows
#ifdef _WIN32
#define _OS_IS_WINDOWS_
#elif defined _WINDLL
#define _OS_IS_WINDOWS_
#endif

#if defined _OS_IS_WINDOWS_
#if defined (MAKE_MOFC_DLL)
#define DECLSPEC_MOFC __declspec(dllexport)
#else
#define DECLSPEC_MOFC __declspec(dllimport)
#endif
#else /* defined (_OS_IS_WINDOWS_) */
#define DECLSPEC_MOFC
#endif /* defined (_OS_IS_WINDOWS_) */


//#include "Array.hpp" // for counting the maximal accessed index of cs, fds and ids and then used no more time
using namespace std;

static int acs_cs[20000];
static int acs_cs_c=0;
static double acs_fds[20000];
static double t2_acs_fds[20000];
static int acs_fds_c=0;
static int acs_ids[20000];
static int acs_ids_c=0;


// here are generated functions by acsammm, all declared as extern "C"

extern "C" DECLSPEC_MOFC void get_num_eqns(int& ne);
extern "C" DECLSPEC_MOFC void get_num_vars(int & nv);
extern "C" DECLSPEC_MOFC void get_num_pars(int & np);
extern "C" DECLSPEC_MOFC void get_num_cond(int & nc);
extern "C" DECLSPEC_MOFC void get_var_names(vector<string> *names);
extern "C" DECLSPEC_MOFC void get_par_names(vector<string> *names);
extern "C" DECLSPEC_MOFC void init(double * x, double * p, int &n_x, int& n_p, int& n_c);
extern "C" DECLSPEC_MOFC void res(double * yy, double * der_x, double * x, double * p, int* condit, int* lock, int* prev, int & n_x, int & n_p, int& n_c);
extern "C" DECLSPEC_MOFC void res_block(double * yy, double * der_x, double * x, double * p, int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c, int &n_yy_ind, int* yy_ind);
extern "C" DECLSPEC_MOFC void a1_res(int bmode_1, double* yy, double* a1_yy, double* der_x, double* a1_der_x, double* x, double* a1_x, double* p, double* a1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
extern "C" DECLSPEC_MOFC void t1_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x, double* t1_x, double* p, double* t1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
extern "C" DECLSPEC_MOFC void t1_res_block(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x, double* t1_x, double* p, double* t1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int &n_c, int& n_yy_ind, int* yy_ind);
extern "C" DECLSPEC_MOFC void t2_a1_res(int bmode_1, double* yy, double* t2_yy, double* a1_yy, double* t2_a1_yy, double* der_x, double* t2_der_x, double* a1_der_x, double* t2_a1_der_x, double* x, double* t2_x, double* a1_x, double* t2_a1_x, double* p, double* t2_p, double* a1_p, double* t2_a1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
extern "C" DECLSPEC_MOFC void jsp_res(jsp* yy, jsp* der_x, jsp* x, jsp* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
extern "C" DECLSPEC_MOFC void jsp_t1_res(jsp* yy, jsp* t1_yy, jsp* der_x, jsp* t1_der_x, jsp* x, jsp* t1_x, jsp* p, jsp* t1_p, int& n_x, int& n_p);

extern "C" DECLSPEC_MOFC void eval_cond(int* condit, double* der_x, double* x, double* p, int& n_x, int& n_p, int& n_c);
extern "C" DECLSPEC_MOFC void res_cond(double* condit, double* der_x, double* x, double* p, int& n_x, int& n_p, int& n_c);

#ifdef WIN32
__declspec(dllimport)
#endif
bool cond_lock(int index, int* condit, int* lock, int* prev);
#endif
