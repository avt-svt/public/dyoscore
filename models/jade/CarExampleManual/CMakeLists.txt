cmake_minimum_required(VERSION 2.8)
project(CarExampleManualJade CXX)

add_library(CarExampleManualJade SHARED
 	CarExampleManual.cpp
	CarExampleManual.block_0.cpp
	CarExampleManual.res_0.cpp
	a1_CarExampleManual.res_0.cpp
	jsp_CarExampleManual.res_0.cpp
	t1_CarExampleManual.block_0.cpp
	t1_CarExampleManual.res_0.cpp
	t2_a1_CarExampleManual.res_0.cpp
	CarExampleManual.eval_cond.cpp
	CarExampleManual.res_cond.cpp
)

set_property(TARGET CarExampleManualJade PROPERTY COMPILE_DEFINITIONS MAKE_MOFC_DLL)

target_link_libraries(CarExampleManualJade EsoCommon)

if(MSVC)
  set_target_properties(CarExampleManualJade PROPERTIES 
    COMPILE_FLAGS -wd4068
	FOLDER Models
  )    
endif()

###### Example for DyosUser Version 

install(TARGETS CarExampleManualJade
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)