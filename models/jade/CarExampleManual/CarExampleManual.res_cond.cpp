#include "EsoAcsammm_Header.hpp"

void res_cond(double * cond, 
double * der_x, double * x, 
double * p, int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep cond
{

	int i_E=0;
	int i_C=0;
	int cond___=0;
	int i__=0;
	int j__=0;
	double kk__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_ttime=0;
	double der_var_ttime=0;
	double var_dist=0;
	double der_var_dist=0;
	double var_velo=0;
	double der_var_velo=0;
	double var_accel=0;
	double der_var_accel=0;
	double var_obj=0;
	double der_var_obj=0;
	double par_alpha=0;
	double par_k=0;
	double var_exp0=0; // procedure var
	double var_i_expr=0; // assign var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	int i_dims_as_one=0; // for two dimensions of matrix
	var_ttime = x[0];
	der_var_ttime = der_x[0];
	var_dist = x[1];
	der_var_dist = der_x[1];
	var_velo = x[2];
	der_var_velo = der_x[2];
	var_accel = x[3];
	der_var_accel = der_x[3];
	var_obj = x[4];
	der_var_obj = der_x[4];
	par_alpha = p[0];
	par_k = p[1];
// procedure assignment exp0 not here
// temporary assignment i_expr not here

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

  }
