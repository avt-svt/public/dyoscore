#include "EsoAcsammm_Header.hpp"

void eval_cond(int * cond, 
double * der_x, double * x, 
double * p, int &n_x, int &n_p, int &n_c)
{

	int i_E=0;
	int i_C=0;
	int cond___=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_n_x = 0;//  constant var n_x
	double var_Tin = 0;//  constant var Tin
	double var_b2 = 0;//  constant var b2
	double var_FbinMin = 0;//  constant var FbinMin
	double var_FbinMax = 0;//  constant var FbinMax
	double var_TwMin = 0;//  constant var TwMin
	double var_TwMax = 0;//  constant var TwMax
	double par_TwCur=0;
	double par_FbinCur=0;
	double par_TMin=0;
	double par_FinalTime=0;
	double var_X1=0;
	double der_var_X1=0;
	double var_X2=0;
	double der_var_X2=0;
	double var_X3=0;
	double der_var_X3=0;
	double var_X4=0;
	double der_var_X4=0;
	double var_X5=0;
	double der_var_X5=0;
	double var_X6=0;
	double der_var_X6=0;
	double var_X7=0;
	double der_var_X7=0;
	double var_X8=0;
	double der_var_X8=0;
	double var_X9=0;
	double der_var_X9=0;
	double var_Fbin=0;
	double der_var_Fbin=0;
	double var_Tw=0;
	double der_var_Tw=0;
	double var_derX7=0;
	double der_var_derX7=0;
	double var_i_expr=0; // assign var
	double var_div0=0; // procedure var
	double var_i_expr2=0; // assign var
	double var_div1=0; // procedure var
	double var_exp3=0; // procedure var
	double var_i_expr4=0; // assign var
	double var_i_expr6=0; // assign var
	double var_i_expr7=0; // assign var
	double var_div5=0; // procedure var
	double var_i_expr9=0; // assign var
	double var_div8=0; // procedure var
	double var_exp10=0; // procedure var
	double var_i_expr11=0; // assign var
	double var_i_expr13=0; // assign var
	double var_i_expr14=0; // assign var
	double var_div12=0; // procedure var
	double var_exp15=0; // procedure var
	double var_i_expr16=0; // assign var
	double var_i_expr18=0; // assign var
	double var_div17=0; // procedure var
	double var_exp19=0; // procedure var
	double var_i_expr20=0; // assign var
	double var_i_expr22=0; // assign var
	double var_i_expr23=0; // assign var
	double var_div21=0; // procedure var
	double var_exp24=0; // procedure var
	double var_i_expr25=0; // assign var
	double var_i_expr27=0; // assign var
	double var_i_expr28=0; // assign var
	double var_div26=0; // procedure var
	double var_exp29=0; // procedure var
	double var_i_expr30=0; // assign var
	double var_i_expr32=0; // assign var
	double var_div31=0; // procedure var
	double var_i_expr34=0; // assign var
	double var_i_expr35=0; // assign var
	double var_div33=0; // procedure var
	double var_exp36=0; // procedure var
	double var_i_expr37=0; // assign var
	double var_i_expr39=0; // assign var
	double var_i_expr40=0; // assign var
	double var_div38=0; // procedure var
	double var_exp41=0; // procedure var
	double var_i_expr42=0; // assign var
	double var_i_expr44=0; // assign var
	double var_div43=0; // procedure var
	double var_i_expr46=0; // assign var
	double var_i_expr47=0; // assign var
	double var_div45=0; // procedure var
	double var_exp48=0; // procedure var
	double var_i_expr49=0; // assign var
	double var_i_expr51=0; // assign var
	double var_div50=0; // procedure var
	double var_i_expr53=0; // assign var
	double var_i_expr54=0; // assign var
	double var_div52=0; // procedure var
	double var_exp55=0; // procedure var
	double var_i_expr56=0; // assign var
	double var_i_expr58=0; // assign var
	double var_div57=0; // procedure var
	double var_i_expr60=0; // assign var
	double var_i_expr61=0; // assign var
	double var_div59=0; // procedure var
	double var_exp62=0; // procedure var
	double var_i_expr63=0; // assign var
	double var_i_expr65=0; // assign var
	double var_i_expr66=0; // assign var
	double var_div64=0; // procedure var
	double var_exp67=0; // procedure var
	double var_i_expr68=0; // assign var
	double var_i_expr70=0; // assign var
	double var_i_expr71=0; // assign var
	double var_div69=0; // procedure var
	double var_exp72=0; // procedure var
	double var_i_expr73=0; // assign var
	double var_i_expr75=0; // assign var
	double var_div74=0; // procedure var
	double var_i_expr77=0; // assign var
	double var_div76=0; // procedure var
	double var_exp78=0; // procedure var
	double var_i_expr79=0; // assign var
	double var_i_expr81=0; // assign var
	double var_i_expr82=0; // assign var
	double var_div80=0; // procedure var
	double var_exp83=0; // procedure var
	double var_i_expr84=0; // assign var
	double var_i_expr86=0; // assign var
	double var_i_expr87=0; // assign var
	double var_div85=0; // procedure var
	double var_exp88=0; // procedure var
	double var_i_expr89=0; // assign var
	double var_i_expr91=0; // assign var
	double var_div90=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_n_x = 9;
	var_Tin = 35;
	var_b2 = 6.6667;
	var_FbinMin = 0;
	var_FbinMax = 5.784;
	var_TwMin = 0.02;
	var_TwMax = 0.1;
	par_TwCur = p[0];
	par_FbinCur = p[1];
	par_TMin = p[2];
	par_FinalTime = p[3];
	var_X1 = x[0];
	der_var_X1 = der_x[0];
	var_X2 = x[1];
	der_var_X2 = der_x[1];
	var_X3 = x[2];
	der_var_X3 = der_x[2];
	var_X4 = x[3];
	der_var_X4 = der_x[3];
	var_X5 = x[4];
	der_var_X5 = der_x[4];
	var_X6 = x[5];
	der_var_X6 = der_x[5];
	var_X7 = x[6];
	der_var_X7 = der_x[6];
	var_X8 = x[7];
	der_var_X8 = der_x[7];
	var_X9 = x[8];
	der_var_X9 = der_x[8];
	var_Fbin = x[9];
	der_var_Fbin = der_x[9];
	var_Tw = x[10];
	der_var_Tw = der_x[10];
	var_derX7 = x[11];
	der_var_derX7 = der_x[11];
// temporary assignment i_expr not here
// procedure assignment div0 not here
// temporary assignment i_expr2 not here
// procedure assignment div1 not here
// procedure assignment exp3 not here
// temporary assignment i_expr4 not here
// temporary assignment i_expr6 not here
// temporary assignment i_expr7 not here
// procedure assignment div5 not here
// temporary assignment i_expr9 not here
// procedure assignment div8 not here
// procedure assignment exp10 not here
// temporary assignment i_expr11 not here
// temporary assignment i_expr13 not here
// temporary assignment i_expr14 not here
// procedure assignment div12 not here
// procedure assignment exp15 not here
// temporary assignment i_expr16 not here
// temporary assignment i_expr18 not here
// procedure assignment div17 not here
// procedure assignment exp19 not here
// temporary assignment i_expr20 not here
// temporary assignment i_expr22 not here
// temporary assignment i_expr23 not here
// procedure assignment div21 not here
// procedure assignment exp24 not here
// temporary assignment i_expr25 not here
// temporary assignment i_expr27 not here
// temporary assignment i_expr28 not here
// procedure assignment div26 not here
// procedure assignment exp29 not here
// temporary assignment i_expr30 not here
// temporary assignment i_expr32 not here
// procedure assignment div31 not here
// temporary assignment i_expr34 not here
// temporary assignment i_expr35 not here
// procedure assignment div33 not here
// procedure assignment exp36 not here
// temporary assignment i_expr37 not here
// temporary assignment i_expr39 not here
// temporary assignment i_expr40 not here
// procedure assignment div38 not here
// procedure assignment exp41 not here
// temporary assignment i_expr42 not here
// temporary assignment i_expr44 not here
// procedure assignment div43 not here
// temporary assignment i_expr46 not here
// temporary assignment i_expr47 not here
// procedure assignment div45 not here
// procedure assignment exp48 not here
// temporary assignment i_expr49 not here
// temporary assignment i_expr51 not here
// procedure assignment div50 not here
// temporary assignment i_expr53 not here
// temporary assignment i_expr54 not here
// procedure assignment div52 not here
// procedure assignment exp55 not here
// temporary assignment i_expr56 not here
// temporary assignment i_expr58 not here
// procedure assignment div57 not here
// temporary assignment i_expr60 not here
// temporary assignment i_expr61 not here
// procedure assignment div59 not here
// procedure assignment exp62 not here
// temporary assignment i_expr63 not here
// temporary assignment i_expr65 not here
// temporary assignment i_expr66 not here
// procedure assignment div64 not here
// procedure assignment exp67 not here
// temporary assignment i_expr68 not here
// temporary assignment i_expr70 not here
// temporary assignment i_expr71 not here
// procedure assignment div69 not here
// procedure assignment exp72 not here
// temporary assignment i_expr73 not here
// temporary assignment i_expr75 not here
// procedure assignment div74 not here
// temporary assignment i_expr77 not here
// procedure assignment div76 not here
// procedure assignment exp78 not here
// temporary assignment i_expr79 not here
// temporary assignment i_expr81 not here
// temporary assignment i_expr82 not here
// procedure assignment div80 not here
// procedure assignment exp83 not here
// temporary assignment i_expr84 not here
// temporary assignment i_expr86 not here
// temporary assignment i_expr87 not here
// procedure assignment div85 not here
// procedure assignment exp88 not here
// temporary assignment i_expr89 not here
// temporary assignment i_expr91 not here
// procedure assignment div90 not here

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

  }
