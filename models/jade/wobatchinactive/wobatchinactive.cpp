#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 0.02; // TwCur
	p[1] = 5.784; // FbinCur
	p[2] = 60; // TMin
	p[3] = 1; // FinalTime
	x[0] = 1; // X1
	x[1] = 0; // X2
	x[2] = 0; // X3
	x[3] = 0; // X4
	x[4] = 0; // X5
	x[5] = 0; // X6
	x[6] = 65; // X7
	x[7] = 2; // X8
	x[8] = 0; // X9
	x[9] = 0; // Fbin
	x[10] = 0; // Tw
	x[11] = 0; // derX7
	n_x = 12; n_p = 4; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("X1");
	names->push_back("X2");
	names->push_back("X3");
	names->push_back("X4");
	names->push_back("X5");
	names->push_back("X6");
	names->push_back("X7");
	names->push_back("X8");
	names->push_back("X9");
	names->push_back("Fbin");
	names->push_back("Tw");
	names->push_back("derX7");
}
void get_num_vars(int & nv) {
  nv = 12;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("TwCur");
	names->push_back("FbinCur");
	names->push_back("TMin");
	names->push_back("FinalTime");
}
void get_num_pars(int & np) {
  np = 4;
}


void get_num_eqns(int& ne){
 ne = 12;
}

void get_num_cond(int& nc){
 nc = 0;
}
