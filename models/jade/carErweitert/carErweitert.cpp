#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 1; // a
	x[0] = 2; // s
	x[1] = 4; // t
	x[2] = 3; // v
	x[3] = 5; // obj
	x[4] = 0.5; // deriv
	n_x = 5; n_p = 1; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("s");
	names->push_back("t");
	names->push_back("v");
	names->push_back("obj");
	names->push_back("deriv");
}
void get_num_vars(int & nv) {
  nv = 5;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("a");
}
void get_num_pars(int & np) {
  np = 1;
}


void get_num_eqns(int& ne){
 ne = 5;
}

void get_num_cond(int& nc){
 nc = 0;
}
