#include "EsoAcsammm_Header.hpp"

void res_block_0(double * yy, 
double * der_x, double * x, 
double * p, int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind, int& i_E_start, int& i_E_end)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_alpha = 0;//  constant var alpha
	double var_k1 = 0;//  constant var k1
	double var_k2 = 0;//  constant var k2
	double var_m = 0;//  constant var m
	double par_a=0;
	double var_s=0;
	double der_var_s=0;
	double var_t=0;
	double der_var_t=0;
	double var_v=0;
	double der_var_v=0;
	double var_obj=0;
	double der_var_obj=0;
	double var_deriv=0;
	double der_var_deriv=0;
	double var_div0=0; // procedure var
	double var_tanh1=0; // procedure var
	double var_i_expr=0; // assign var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_alpha = 0.36;
	var_k1 = 15;
	var_k2 = 1;
	var_m = 1000;
	par_a = p[0];
	var_s = x[0];
	der_var_s = der_x[0];
	var_t = x[1];
	der_var_t = der_x[1];
	var_v = x[2];
	der_var_v = der_x[2];
	var_obj = x[3];
	der_var_obj = der_x[3];
	var_deriv = x[4];
	der_var_deriv = der_x[4];
// procedure assignment div0 not here
// procedure assignment tanh1 not here
// temporary assignment i_expr not here

i_E = i_E_start;
while (i_E < i_E_end) {
  i__switch = yy_ind[i_E];
  switch (i__switch) {

  case 0: // scalar equation 0
  yy[i_E] = der_var_s - (var_v);
  i_E = i_E+1;  break;
  case 1: // scalar equation 1
acs_div(var_v, var_m, var_div0);
  yy[i_E] = der_var_v - (par_a-var_alpha * var_v * var_div0);
  i_E = i_E+1;  break;
  case 2: // scalar equation 2
  yy[i_E] = der_var_t - (1);
  i_E = i_E+1;  break;
  case 3: // scalar equation 3
  yy[i_E] = der_var_obj - (var_deriv);
  i_E = i_E+1;  break;
  case 4: // scalar equation 4
var_i_expr=var_m * par_a;
acs_tanh(var_i_expr, var_tanh1);
  yy[i_E] = var_deriv - (var_k2+0.5 * var_k1 * par_a *  ( 1+var_tanh1 ) );
  i_E = i_E+1;  break;
// //default: i_E=i_E+1; break; 
  } // switch
} // end while
} // end of block_* function
void res_block(double * yy, 
double * der_x, double * x, double * p,
int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind)
  #pragma ad indep x p
  #pragma ad dep yy
{
 int i_E = 0;
 int i_E_end = 5;
 int i__start = 0;
 int i__end = 0;
 int i__switch = 0;
while (i_E < n_yy_ind) {
  i__switch = yy_ind[i_E];
i__start = 0;
i__end = 5;
if ((i__switch >= i__start) && (i__switch < i__end)) {
  i__end = i_E+1;
  res_block_0(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c, n_yy_ind, yy_ind, i_E, i__end);
}
i_E = i_E+1; } // while
}  // res_block
