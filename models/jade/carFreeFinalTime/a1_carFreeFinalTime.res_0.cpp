#include "EsoAcsammm_Header.hpp"
/* Copyright (C) 1991-2017 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */
/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */
/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 10.0.0.  Version 10.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2017, fifth edition, plus
   the following additions from Amendment 1 to the fifth edition:
   - 56 emoji characters
   - 285 hentaigana
   - 3 additional Zanabazar Square characters */
/* We do not support C11 <threads.h>.  */
// ++++++++++++++++++++++++++++++++++
// + Code generated by dcc 1.0      +
// ++++++++++++++++++++++++++++++++++
void a1_res(int bmode_1, double* yy, double* a1_yy, double* der_x, double* a1_der_x, double* x, double* a1_x, double* p, double* a1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c)
#pragma ad indep x a1_x p a1_p a1_yy
#pragma ad dep yy a1_x a1_p
{
   int i_E=0;
   int i_C=0;
   int i__=0;
   int j__=0;
   int k__=0;
   int m__=0;
   int r_i=0;
   int r_j=0;
   int i_a=0;
   int i_b=0;
   int i_loop=0;
   int i__switch=0;
   int i__start=0;
   int i__end=0;
   int i__step=0;
   int while_end=0;
   int i_cont=0;
   int j_cont=0;
   int k_cont=0;
   double var_pi=0;
   double a1_var_pi=0;
   double par_accel=0;
   double a1_par_accel=0;
   double par_alpha=0;
   double a1_par_alpha=0;
   double par_tf=0;
   double a1_par_tf=0;
   double var_ttime=0;
   double a1_var_ttime=0;
   double der_var_ttime=0;
   double a1_der_var_ttime=0;
   double var_velo=0;
   double a1_var_velo=0;
   double der_var_velo=0;
   double a1_der_var_velo=0;
   double var_dist=0;
   double a1_var_dist=0;
   double der_var_dist=0;
   double a1_der_var_dist=0;
   int i_i=0;
   int i_j=0;
   int i_z=0;
   double v1_0=0;
   double a1_v1_0=0;
   double v1_1=0;
   double a1_v1_1=0;
   double v1_2=0;
   double a1_v1_2=0;
   double v1_3=0;
   double a1_v1_3=0;
   double v1_4=0;
   double a1_v1_4=0;
   double v1_5=0;
   double a1_v1_5=0;
   double v1_6=0;
   double a1_v1_6=0;
   double v1_7=0;
   double a1_v1_7=0;
   double v1_8=0;
   double a1_v1_8=0;
   double v1_9=0;
   double a1_v1_9=0;
   double v1_10=0;
   double a1_v1_10=0;
   double v1_11=0;
   double a1_v1_11=0;
   double v1_12=0;
   double a1_v1_12=0;
   int save_acs_cs_c=0;
   save_acs_cs_c=acs_cs_c;
   if (bmode_1==1) {
      // augmented forward section
      acs_cs[acs_cs_c]=0; acs_cs_c=acs_cs_c+1;
      acs_fds[acs_fds_c]=var_pi; acs_fds_c=acs_fds_c+1; var_pi=3.14159;
      acs_fds[acs_fds_c]=par_accel; acs_fds_c=acs_fds_c+1; par_accel=p[0];
      acs_fds[acs_fds_c]=par_alpha; acs_fds_c=acs_fds_c+1; par_alpha=p[1];
      acs_fds[acs_fds_c]=par_tf; acs_fds_c=acs_fds_c+1; par_tf=p[2];
      acs_fds[acs_fds_c]=var_ttime; acs_fds_c=acs_fds_c+1; var_ttime=x[0];
      acs_fds[acs_fds_c]=der_var_ttime; acs_fds_c=acs_fds_c+1; der_var_ttime=der_x[0];
      acs_fds[acs_fds_c]=var_velo; acs_fds_c=acs_fds_c+1; var_velo=x[1];
      acs_fds[acs_fds_c]=der_var_velo; acs_fds_c=acs_fds_c+1; der_var_velo=der_x[1];
      acs_fds[acs_fds_c]=var_dist; acs_fds_c=acs_fds_c+1; var_dist=x[2];
      acs_fds[acs_fds_c]=der_var_dist; acs_fds_c=acs_fds_c+1; der_var_dist=der_x[2];
      acs_fds[acs_fds_c]=yy[i_E]; acs_fds_c=acs_fds_c+1; yy[i_E]=der_var_dist-(par_tf*var_velo);
      acs_ids[acs_ids_c]=i_E; acs_ids_c=acs_ids_c+1; i_E=i_E+1;
      acs_fds[acs_fds_c]=yy[i_E]; acs_fds_c=acs_fds_c+1; yy[i_E]=der_var_ttime-(par_tf*1);
      acs_ids[acs_ids_c]=i_E; acs_ids_c=acs_ids_c+1; i_E=i_E+1;
      acs_fds[acs_fds_c]=yy[i_E]; acs_fds_c=acs_fds_c+1; yy[i_E]=der_var_velo-(par_tf*(par_accel-par_alpha*var_velo*var_velo));
      acs_ids[acs_ids_c]=i_E; acs_ids_c=acs_ids_c+1; i_E=i_E+1;
      acs_ids[acs_ids_c]=n_c; acs_ids_c=acs_ids_c+1; n_c=i_C;
      // For the case you need the actual function value at the end of the forward section, use this mode for storing it.
      // reverse section
      while (acs_cs_c>save_acs_cs_c) {
            acs_cs_c=acs_cs_c-1;
            if (acs_cs[acs_cs_c]==0) {
              acs_ids_c=acs_ids_c-1; n_c=acs_ids[acs_ids_c];
              acs_ids_c=acs_ids_c-1; i_E=acs_ids[acs_ids_c];
              acs_fds_c=acs_fds_c-1; yy[i_E]=acs_fds[acs_fds_c];
              v1_0=der_var_velo;
              v1_1=par_tf;
              v1_2=par_accel;
              v1_3=par_alpha;
              v1_4=var_velo;
              v1_5=v1_3*v1_4;
              v1_6=var_velo;
              v1_7=v1_5*v1_6;
              v1_8=v1_2-v1_7;
              v1_9=v1_8;
              v1_10=v1_1*v1_9;
              v1_11=v1_10;
              v1_12=v1_0-v1_11;
              a1_v1_12=a1_yy[i_E]; a1_yy[i_E]=0;
              a1_v1_0=a1_v1_12; a1_v1_11=0-a1_v1_12;
              a1_v1_10=a1_v1_11;
              a1_v1_1=v1_9*a1_v1_10; a1_v1_9=v1_1*a1_v1_10;
              a1_v1_8=a1_v1_9;
              a1_v1_2=a1_v1_8; a1_v1_7=0-a1_v1_8;
              a1_v1_5=v1_6*a1_v1_7; a1_v1_6=v1_5*a1_v1_7;
              a1_var_velo=a1_var_velo+a1_v1_6;
              a1_v1_3=v1_4*a1_v1_5; a1_v1_4=v1_3*a1_v1_5;
              a1_var_velo=a1_var_velo+a1_v1_4;
              a1_par_alpha=a1_par_alpha+a1_v1_3;
              a1_par_accel=a1_par_accel+a1_v1_2;
              a1_par_tf=a1_par_tf+a1_v1_1;
              a1_der_var_velo=a1_der_var_velo+a1_v1_0;
              acs_ids_c=acs_ids_c-1; i_E=acs_ids[acs_ids_c];
              acs_fds_c=acs_fds_c-1; yy[i_E]=acs_fds[acs_fds_c];
              v1_0=der_var_ttime;
              v1_1=par_tf;
              v1_2=1;
              v1_3=v1_1*v1_2;
              v1_4=v1_3;
              v1_5=v1_0-v1_4;
              a1_v1_5=a1_yy[i_E]; a1_yy[i_E]=0;
              a1_v1_0=a1_v1_5; a1_v1_4=0-a1_v1_5;
              a1_v1_3=a1_v1_4;
              a1_v1_1=v1_2*a1_v1_3; a1_v1_2=v1_1*a1_v1_3;
              a1_par_tf=a1_par_tf+a1_v1_1;
              a1_der_var_ttime=a1_der_var_ttime+a1_v1_0;
              acs_ids_c=acs_ids_c-1; i_E=acs_ids[acs_ids_c];
              acs_fds_c=acs_fds_c-1; yy[i_E]=acs_fds[acs_fds_c];
              v1_0=der_var_dist;
              v1_1=par_tf;
              v1_2=var_velo;
              v1_3=v1_1*v1_2;
              v1_4=v1_3;
              v1_5=v1_0-v1_4;
              a1_v1_5=a1_yy[i_E]; a1_yy[i_E]=0;
              a1_v1_0=a1_v1_5; a1_v1_4=0-a1_v1_5;
              a1_v1_3=a1_v1_4;
              a1_v1_1=v1_2*a1_v1_3; a1_v1_2=v1_1*a1_v1_3;
              a1_var_velo=a1_var_velo+a1_v1_2;
              a1_par_tf=a1_par_tf+a1_v1_1;
              a1_der_var_dist=a1_der_var_dist+a1_v1_0;
              acs_fds_c=acs_fds_c-1; der_var_dist=acs_fds[acs_fds_c];
              v1_0=der_x[2];
              a1_v1_0=a1_der_var_dist; a1_der_var_dist=0;
              a1_der_x[2]=a1_der_x[2]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; var_dist=acs_fds[acs_fds_c];
              v1_0=x[2];
              a1_v1_0=a1_var_dist; a1_var_dist=0;
              a1_x[2]=a1_x[2]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; der_var_velo=acs_fds[acs_fds_c];
              v1_0=der_x[1];
              a1_v1_0=a1_der_var_velo; a1_der_var_velo=0;
              a1_der_x[1]=a1_der_x[1]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; var_velo=acs_fds[acs_fds_c];
              v1_0=x[1];
              a1_v1_0=a1_var_velo; a1_var_velo=0;
              a1_x[1]=a1_x[1]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; der_var_ttime=acs_fds[acs_fds_c];
              v1_0=der_x[0];
              a1_v1_0=a1_der_var_ttime; a1_der_var_ttime=0;
              a1_der_x[0]=a1_der_x[0]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; var_ttime=acs_fds[acs_fds_c];
              v1_0=x[0];
              a1_v1_0=a1_var_ttime; a1_var_ttime=0;
              a1_x[0]=a1_x[0]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; par_tf=acs_fds[acs_fds_c];
              v1_0=p[2];
              a1_v1_0=a1_par_tf; a1_par_tf=0;
              a1_p[2]=a1_p[2]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; par_alpha=acs_fds[acs_fds_c];
              v1_0=p[1];
              a1_v1_0=a1_par_alpha; a1_par_alpha=0;
              a1_p[1]=a1_p[1]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; par_accel=acs_fds[acs_fds_c];
              v1_0=p[0];
              a1_v1_0=a1_par_accel; a1_par_accel=0;
              a1_p[0]=a1_p[0]+a1_v1_0;
              acs_fds_c=acs_fds_c-1; var_pi=acs_fds[acs_fds_c];
              v1_0=3.14159;
              a1_v1_0=a1_var_pi; a1_var_pi=0;
            }
          }
       }
       if (bmode_1==2) {
          res(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c);
       }
}
