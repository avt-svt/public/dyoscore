#include "EsoAcsammm_Header.hpp"
// ++++++++++++++++++++++++++++++++++
// + Code generated by dcc 1.0      +
// ++++++++++++++++++++++++++++++++++
void jsp_res(jsp* yy, jsp* der_x, jsp* x, jsp* p, int * condit, int * lock, int * prev, int& n_x, int& n_p, int& n_c)
#pragma ad indep x p
#pragma ad dep yy
{
   int i_E=0; 
   int i_C=0; 
   int i__=0; 
   int j__=0; 
   int k__=0; 
   int m__=0; 
   int r_i=0; 
   int r_j=0; 
   int i_a=0; 
   int i_b=0; 
   int i_loop=0; 
   int i__switch=0; 
   int i__start=0; 
   int i__end=0; 
   int i__step=0; 
   int while_end=0; 
   int i_cont=0; 
   int j_cont=0; 
   int k_cont=0; 
   jsp var_pi=0; 
   jsp par_accel=0; 
   jsp par_alpha=0; 
   jsp par_tf=0; 
   jsp var_ttime=0; 
   jsp der_var_ttime=0; 
   jsp var_velo=0; 
   jsp der_var_velo=0; 
   jsp var_dist=0; 
   jsp der_var_dist=0; 
   int i_i=0; 
   int i_j=0; 
   int i_z=0; 
   var_pi=3.14159;
   par_accel=p[0];
   par_alpha=p[1];
   par_tf=p[2];
   var_ttime=x[0];
   der_var_ttime=der_x[0];
   var_velo=x[1];
   der_var_velo=der_x[1];
   var_dist=x[2];
   der_var_dist=der_x[2];
   yy[i_E]=der_var_dist-(par_tf*var_velo);
   i_E=i_E+1;
   yy[i_E]=der_var_ttime-(par_tf*1);
   i_E=i_E+1;
   yy[i_E]=der_var_velo-(par_tf*(par_accel-par_alpha*var_velo*var_velo));
   i_E=i_E+1;
   n_c=i_C;
}
