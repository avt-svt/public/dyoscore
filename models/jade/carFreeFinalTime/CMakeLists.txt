cmake_minimum_required(VERSION 2.8)


project(carFreeFinalTime CXX)

add_library(carFreeFinalTime SHARED
 	carFreeFinalTime.cpp
	carFreeFinalTime.block_0.cpp
	carFreeFinalTime.res_0.cpp
	a1_carFreeFinalTime.res_0.cpp
	jsp_carFreeFinalTime.res_0.cpp
	t1_carFreeFinalTime.block_0.cpp
	t1_carFreeFinalTime.res_0.cpp
	t2_a1_carFreeFinalTime.res_0.cpp
	carFreeFinalTime.eval_cond.cpp
	carFreeFinalTime.res_cond.cpp
)


set_property(TARGET carFreeFinalTime PROPERTY COMPILE_DEFINITIONS MAKE_MOFC_DLL)

target_link_libraries(carFreeFinalTime debug EsoCommon)
target_link_libraries(carFreeFinalTime optimized EsoCommon)

if(MSVC)
  set_target_properties(carFreeFinalTime PROPERTIES COMPILE_FLAGS -wd4068)
  set_target_properties(carFreeFinalTime PROPERTIES FOLDER  Models)
endif()
