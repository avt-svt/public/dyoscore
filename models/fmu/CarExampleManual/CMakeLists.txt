cmake_minimum_required(VERSION 3.4)
project(CarExampleManualFMU)

set(CarExampleManualFMU_Source  
  ${CMAKE_CURRENT_SOURCE_DIR}/sources/all.c 
)
add_library(CarExampleManualFMU SHARED ${CarExampleManualFMU_Source})

IF(WIN32)
  target_link_libraries(CarExampleManualFMU shlwapi.lib)
ENDIF(WIN32)

target_include_directories(CarExampleManualFMU BEFORE	
  PRIVATE	
  ${CMAKE_CURRENT_SOURCE_DIR}/sources
  ${FMILibPath}/ThirdParty/FMI/default/FMI2
  ${FMILibPath}/FMI_LIB/FMILibrary_2_0_3/ThirdParty/FMI/default/FMI2
)

## copy modelDescription.xml to binary directory.
file (	COPY "${CMAKE_CURRENT_SOURCE_DIR}/modelDescription.xml"
		DESTINATION "${CMAKE_BINARY_DIR}/input/FMI/CarExampleManualFMU"
	)	

## set binary path for dll, discriminating between 64 bit or 32 bit
set_target_properties(CarExampleManualFMU
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/input/FMI/CarExampleManualFMU/lib"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/input/FMI/CarExampleManualFMU/lib"
	FOLDER Models
)

if(WIN32)
  if(CMAKE_SIZEOF_VOID_P EQUAL 8)
	# 64 bit
    set_target_properties(CarExampleManualFMU 	PROPERTIES	
	  RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/input/FMI/CarExampleManualFMU/binaries/win64"
	  RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/input/FMI/CarExampleManualFMU/binaries/win64"		
	)						
  elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
    # 32 bit
    set_target_properties(CarExampleManualFMU 	PROPERTIES	
	  RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/input/FMI/CarExampleManualFMU/binaries/win32"
	  RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/input/FMI/CarExampleManualFMU/binaries/win32"		
	)	
  endif()
endif(WIN32)

###### Example for DyosUser Version 

install(TARGETS CarExampleManualFMU
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)