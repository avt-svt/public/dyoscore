# DyOS

Trunk of DyOS (Dynamic Optimization Software) developed at AVT.PT/AVT.SVT

Note: DyOS source is not executable without dependencies (optimizer, integrator etc.)


DyosCore

=====================================================================\n

Lehrstuhl fuer Systemverfahrenstechnik/Prozesstechnik, RWTH Aachen                     \n
=====================================================================\n

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/. 

