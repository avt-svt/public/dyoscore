import pydyos as pd


# eso input setput

esoInput = pd.EsoInput();
esoInput.type = esoInput.type.FMI;
esoInput.relativeFmuTolerance = 1e-6;
esoInput.model = "C:/Users/AdrianC/Documents/Promotion/DyosTrunkBuild/input/FMI/wobatchinactiveFmu";


# stage input setput

stageInput = pd.StageInput();

stageInput.eso = esoInput;



# integrator stage input

integratorStageInput = pd.IntegratorStageInput();

# stage final time
stageDuration = pd.ParameterInput();
stageDuration.lowerBound = 1000.0;
stageDuration.upperBound = 1000.0;
stageDuration.value = 1000.0;
stageDuration.sensType = stageDuration.sensType_type.NO;
stageDuration.paramType = stageDuration.paramType_type.DURATION;



# parameters

gridParameter1 = pd.ParameterGridInput();
gridParameter1.numIntervals = 8;
gridParameter1.type = gridParameter1.type.PIECEWISE_LINEAR;
gridParameter1.values = [5.784,5.784,5.784,5.784,5.784,5.784,5.784,5.784,5.784];
gridParameter1.adapt.maxAdaptSteps = 8;
gridParameter1.adapt.adaptWave.minRefinementLevel = 1;
gridParameter1.adapt.adaptWave.maxRefinementLevel = 20;
gridParameter1.adapt.adaptWave.horRefinementDepth = 0;
gridParameter1.pcresolution = 2;


parameter1 = pd.ParameterInput();
parameter1.name = "FbinCur";
parameter1.lowerBound=0.0;
parameter1.upperBound=5.784;
parameter1.paramType = parameter1.paramType_type.PROFILE;
parameter1.sensType = parameter1.sensType_type.FULL;
parameter1.grids = [gridParameter1];



gridParameter2 = pd.ParameterGridInput();
gridParameter2.numIntervals = 8;
gridParameter2.type = gridParameter1.type_type.PIECEWISE_LINEAR;
gridParameter2.values = [100e-3]*9;
gridParameter2.adapt.maxAdaptSteps = 8;
gridParameter2.adapt.adaptWave.minRefinementLevel = 1;
gridParameter2.adapt.adaptWave.maxRefinementLevel = 20;
gridParameter2.adapt.adaptWave.horRefinementDepth = 0;
gridParameter2.pcresolution = 2;

parameter2 = pd.ParameterInput();
parameter2.name = "TwCur";
parameter2.lowerBound=0.02;
parameter2.upperBound=0.1;
parameter2.paramType = parameter1.paramType_type.PROFILE;
parameter2.sensType = parameter1.sensType_type.FULL;
parameter2.grids = [gridParameter2];


integratorStageInput.parameters = [parameter1,parameter2];

integratorStageInput.duration = stageDuration;

stageInput.integrator = integratorStageInput;


# optimizer stage input

optimizerStageInput = pd.OptimizerStageInput();


## objective

objective = pd.ConstraintInput();
objective.name="X9";
optimizerStageInput.objective = objective;

## constraints

constraint1 = pd.ConstraintInput();
constraint1.name = "X7";
constraint1.lowerBound = 60;
constraint1.upperBound = 90;
constraint1.type = constraint1.type_type.PATH;


constraint2 = pd.ConstraintInput();
constraint2.name = "X8";
constraint2.lowerBound = 0;
constraint2.upperBound = 5;
constraint2.type = constraint1.type_type.ENDPOINT;


optimizerStageInput.constraints = [constraint1, constraint2];


stageInput.optimizer = optimizerStageInput;

stageInput.optimizer.structureDetection.maxStructureSteps = 8;


# defining stages


Input = pd.Input();

Input.stages = [stageInput];
Input.stages[0].mapping.fullStateMapping = True;
Input.stages[0].treatObjective = True;

Input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-8;
Input.runningMode = Input.runningMode_type.SINGLE_SHOOTING;
Input.integratorInput.type = Input.integratorInput.type_type.LIMEX;
Input.integratorInput.order = Input.integratorInput.order_type.FIRST_FORWARD;
Input.optimizerInput.type = Input.optimizerInput.type_type.SNOPT;

Input.optimizerInput.adaptationOptions.adaptStrategy = Input.optimizerInput.adaptationOptions.adaptStrategy_type.ADAPTATION;
Input.integratorInput.integratorOptions = [["absolute tolerance","1e-6"],\
                                           ["relative tolerance","1e-6"]];
Input.optimizerInput.optimizerOptions = [["major optimality tolerance","1e-6"],\
                                         ["linesearch tolerance","0.99"],\
                                         ["Major Iterations Limit","250"],\
                                         ["Minor Iterations Limit","500"],\
                                         ["function precision","1e-6"]\
                                         ];


# creating pyDyos object

PyDyos = pd.PyDyos()

#setting pyDyos input
PyDyos.Input = Input;

# solving problem

PyDyos.runPyDyos();

# retrieving Output
Output = pd.Output;


Output = PyDyos.Output;
