/**
* @file TestPydyos.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic TestPydyos - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the pydyos module	                             \n
* =====================================================================\n
* @author Adrian Caspari
* @date 24.05.2018
*/


#include "PyDyos.hpp"



BOOST_AUTO_TEST_SUITE(TestPydyos)



BOOST_AUTO_TEST_CASE(TEST_pydyos){

	BOOST_REQUIRE_NO_THROW(PyDyos pydyos(););



  }


BOOST_AUTO_TEST_SUITE_END()





