/**
* @file PyDyOS.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* PyDyOS - Python Frontend of DyOS                    \n
* =====================================================================\n
* @author Adrian Caspari and Yannic Martin Perez, Felix Zimmermann
* @date 27.04.2018
*/


#include <PyDyos.hpp>

std::ostream& operator<<(std::ostream& os, const boost::python::object& o)
{
	return os << boost::python::extract<std::string>(boost::python::str(o))();
}

/**
* @brief runPyDyOS function to start dyos - runDyos
*/
void PyDyos::runPyDyos()
{

	jsonFile = OutputChannel::Ptr(new FileChannel(this->nameOutputFile));

	logger = Logger::Ptr(new StandardLogger());


	channelIndex = Logger::FINAL_OUT_JSON;

	logger->setLoggingChannel(channelIndex, jsonFile);

	Output = runDyos(Input, logger);

};


/**
* @brief runPyDyOS function to start dyos - runDyos
*/
void printString() {

	std::cout << "print something. \n";

};

UserInput::Input PyDyos::getInput() { return this->Input; };

void PyDyos::setInput(UserInput::Input input) { this->Input = input; };


UserOutput::Output PyDyos::getOutput() { return this->Output; };

void PyDyos::setOutput(UserOutput::Output output) { this->Output = output; };

/**
* @brief convertOutputToInput function convert output to input
*/
UserInput::Input PyDyos::convertOutputToInput(UserOutput::Output output)
{
	return this->ioConversion.o2iConvertOutputToInput(output);
};


/**
*@brief setter for nameOutputFile
*/
void PyDyos::setNameOutputFile(std::string nameOutputFile) {

	this->nameOutputFile = nameOutputFile;
}

/**
*@brief getter for nameOutputFile
*/
std::string PyDyos::getNameOutputFile() {

	return this->nameOutputFile;
}



/**
* @namespace pyDyosFunctions including all relevant wrapping functions for UserInput and UserOutput
*/
namespace pyDyosFunctions {

	



/**
*@brief getInputFromJson
*/
UserInput::Input getInputFromJson(std::string filename) {

	UserInput::Input input;
	convertUserInputFromXmlJson(filename, input, DataFormat::JSON);
	return input;
}


/**
*@brief getOutputFromJson
*/
UserOutput::Output getOutputFromJson(std::string filename) {

	UserOutput::Output output;
	convertUserOutputFromXmlJson(filename, output, DataFormat::JSON);
	return output;
}

/**
*@brief convertInputToJson
*/
void convertInputToJson(std::string filename, UserInput::Input input) {

	convertUserInputToXmlJson(filename, input, DataFormat::JSON);

}

/**
*@brief convertOutputToJson
*/
void convertOutputToJson(std::string filename, UserOutput::Output output) {

	convertUserOutputToXmlJson(filename, output, DataFormat::JSON);

}


	/**
	* @brief convert python list to std::vector
	*/
	template<typename T> inline std::vector< T > py_list_to_std_vector(const boost::python::list iterable)
	{
		return std::vector< T >(boost::python::stl_input_iterator< T >(iterable), boost::python::stl_input_iterator< T >());
	}

	/**
	* @brief convert std::vector to python list
	*/
	template <class T> inline boost::python::list std_vector_to_py_list(std::vector<T>& vector) {
		typename std::vector<T>::iterator iter;
		boost::python::list list;
		for (iter = vector.begin(); iter != vector.end(); ++iter) {
			list.append(*iter);
		}
		return list;
	}


	/**
	* @brief convert python list to std::map
	*/
	template<typename T, typename U> inline boost::python::list std_map_xx_to_py_list(std::map<T, U>& map_xx) {
		boost::python::list list;

		for (std::map<T, U>::iterator it = map_xx.begin(); it != map_xx.end(); ++it) {

			boost::python::list subList;
			subList.append(it->first);
			subList.append(it->second);
			list.append(subList);
		}

		return list;
	}

	/**
	* @brief convert std::map to python list
	*/
	template<typename T, typename U> inline std::map<T, U> py_list_to_std_map_xx(const boost::python::list list) {

		std::map<T, U> map;
		for (int i = 0; i < boost::python::len(list); i++) {
			map.insert(std::pair<T, U>(boost::python::extract<T>(list[i][0]), boost::python::extract<U>(list[i][1])));
		}

		return map;
	}

	/**
	* @brief convert C-Array T[] to python list
	*/
	template <class T> inline void array_pointer_to_py_list(int size, T* array, boost::python::list &list) {
		for (int i = 0; i < size; i++) {
			list.append(array[i]);
		}
	}

	/**
	* @brief convert C-Array T[] to python list
	*/
	template <class T> inline boost::python::list array_pointer_to_py_list(int size, T* array) {
		boost::python::list list;
		for (int i = 0; i < size; i++) {
			list.append(array[i]);
		}
		return list;
	}

	/**
	* @brief wrap function for UserInput::Input
	*/
	boost::python::list pydyosGetInputStages(UserInput::Input& input) {

		return std_vector_to_py_list(input.stages);

	}

	/**
	* @brief wrap function for UserInput::Input
	*/
	void pydyosSetInputStages(UserInput::Input& inputPy, boost::python::list inputStagesPy) {

		inputPy.stages = py_list_to_std_vector<UserInput::StageInput>(inputStagesPy);

	}


	// ***************** UserInput::ParameterGridInput ********************



	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	boost::python::list get_parameterGridInput_TimePoints(UserInput::ParameterGridInput& parameterGridInput) {

		return std_vector_to_py_list(parameterGridInput.timePoints);

	}

	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	void set_parameterGridInput_TimePoints(UserInput::ParameterGridInput& parameterGridInput, boost::python::list timePoints) {

		parameterGridInput.timePoints = py_list_to_std_vector<double>(timePoints);

	}



	// *************************************


	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	boost::python::list get_parameterGridInput_Values(UserInput::ParameterGridInput& parameterGridInput) {

		return std_vector_to_py_list(parameterGridInput.values);

	}

	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	void set_parameterGridInput_Values(UserInput::ParameterGridInput& parameterGridInput, boost::python::list values) {

		parameterGridInput.values = py_list_to_std_vector<double>(values);

	}


	// ******************* UserInput::ParameterInput ******************


	/**
	* @brief wrap function for UserInput::ParameterInput
	*/
	boost::python::list get_parameterInput_grids(UserInput::ParameterInput& parameterInput) {

		return std_vector_to_py_list(parameterInput.grids);

	}



	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_parameterInput_grids(UserInput::ParameterInput& parameterInput, boost::python::list grids) {

		parameterInput.grids = py_list_to_std_vector<UserInput::ParameterGridInput>(grids);

	}



	// ******************* UserInput::IntegratorStageInput ******************


	/**
	* @brief wrap function for UserInput::ParameterInput
	*/
	boost::python::list get_integratorStageInput_parameters(UserInput::IntegratorStageInput& integratorStageInput) {

		return std_vector_to_py_list(integratorStageInput.parameters);

	}

	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_integratorStageInput_parameters(UserInput::IntegratorStageInput& integratorStageInput, boost::python::list parameters) {

		integratorStageInput.parameters = py_list_to_std_vector<UserInput::ParameterInput>(parameters);

	}



	/**
	* @brief wrap function for UserInput::ParameterInput
	*/
	boost::python::list get_integratorStageInput_explicitPlotGrid(UserInput::IntegratorStageInput& integratorStageInput) {

		return std_vector_to_py_list(integratorStageInput.explicitPlotGrid);

	}

	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_integratorStageInput_explicitPlotGrid(UserInput::IntegratorStageInput& integratorStageInput, boost::python::list explicitPlotGrid) {

		integratorStageInput.explicitPlotGrid = py_list_to_std_vector<double>(explicitPlotGrid);

	}


	// ******************* UserInput::OptimizerStageInput ******************


	/**
	* @brief wrap function for UserInput::OptimizerStageInput
	*/
	boost::python::list get_OptimizerStageInput_constraints(UserInput::OptimizerStageInput& integratorStageInput) {

		return std_vector_to_py_list(integratorStageInput.constraints);

	}

	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_OptimizerStageInput_constraints(UserInput::OptimizerStageInput& integratorStageInput, boost::python::list constraints) {

		integratorStageInput.constraints = py_list_to_std_vector<UserInput::ConstraintInput>(constraints);

	}




	// ******************* UserInput::StageMapping ******************


	/**
	* @brief wrap function for UserInput::StageMapping
	*/
	boost::python::list get_StageMapping_stateNameMapping(UserInput::StageMapping& stageMapping) {

		boost::python::list list;

		for (std::map<std::string, std::string>::iterator it = stageMapping.stateNameMapping.begin(); it != stageMapping.stateNameMapping.end(); ++it) {

			boost::python::list subList;
			subList.append(it->first);
			subList.append(it->second);
			list.append(subList);
		}

		return list;

	}



	/**
	* @brief wrap function for UserInput::StageMapping
	*/
	void set_StageMapping_stateNameMapping(UserInput::StageMapping& stageMapping, boost::python::list stateNameMapping) {

		for (int i = 0; i < boost::python::len(stateNameMapping); i++) {

			stageMapping.stateNameMapping.insert(std::pair<std::string, std::string>(boost::python::extract<std::string>(stateNameMapping[i][0]), boost::python::extract<std::string>(stateNameMapping[i][1])));
		}

	}



	// ******************* UserInput::MappingInput ******************


	/**
	* @brief wrap function for UserInput::MappingInput
	*/
	boost::python::list get_MappingInput_stateNameMapping(UserInput::MappingInput& mappingInput) {

		boost::python::list list;

		for (std::map<std::string, std::string>::iterator it = mappingInput.stateNameMapping.begin(); it != mappingInput.stateNameMapping.end(); ++it) {

			boost::python::list subList;
			subList.append(it->first);
			subList.append(it->second);
			list.append(subList);
		}

		return list;

	}



	/**
	* @brief wrap function for UserInput::MappingInput
	*/
	void set_MappingInput_stateNameMapping(UserInput::MappingInput& mappingInput, boost::python::list stateNameMapping) {

		for (int i = 0; i < boost::python::len(stateNameMapping); i++) {

			mappingInput.stateNameMapping.insert(std::pair<std::string, std::string>(boost::python::extract<std::string>(stateNameMapping[i][0]), boost::python::extract<std::string>(stateNameMapping[i][1])));
		}

	}





	// ******************* UserInput::IntegratorInput ******************


	/**
	* @brief wrap function for UserInput::IntegratorInput
	*/
	boost::python::list get_IntegratorInput_integratorOptions(UserInput::IntegratorInput& integratorInput) {

		return std_map_xx_to_py_list(integratorInput.integratorOptions);

	}



	/**
	* @brief wrap function for UserInput::IntegratorInput
	*/
	void set_IntegratorInput_integratorOptions(UserInput::IntegratorInput& integratorInput, boost::python::list integratorOptions) {

		integratorInput.integratorOptions = py_list_to_std_map_xx<std::string, std::string>(integratorOptions);

	}










	// ******************* UserInput::OptimizerInput ******************


	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	boost::python::list get_OptimizerInput_optimizerOptions(UserInput::OptimizerInput& optimizerInput) {

		return std_map_xx_to_py_list(optimizerInput.optimizerOptions);

	}



	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	void set_OptimizerInput_optimizerOptions(UserInput::OptimizerInput& optimizerInput, boost::python::list optimizerOptions) {

		optimizerInput.optimizerOptions = py_list_to_std_map_xx<std::string, std::string>(optimizerOptions);

	}

	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	boost::python::list get_OptimizerInput_globalConstraints(UserInput::OptimizerInput& optimizerInput) {

		return std_vector_to_py_list(optimizerInput.globalConstraints);

	}



	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	void set_OptimizerInput_globalConstraints(UserInput::OptimizerInput& optimizerInput, boost::python::list globalConstraints) {

		optimizerInput.globalConstraints = py_list_to_std_vector<UserInput::ConstraintInput>(globalConstraints);

	}





	// ******************* UserInput::ConstraintInput ******************


	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	boost::python::list pydyosGetLagrangeMultiplierVector(UserInput::ConstraintInput& constraintInput) {

		return std_vector_to_py_list(constraintInput.lagrangeMultiplierVector);

	}



	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void pydyosSetLagrangeMultiplierVector(UserInput::ConstraintInput& constraintInput, boost::python::list constraintInputList) {

		constraintInput.lagrangeMultiplierVector = py_list_to_std_vector<double>(constraintInputList);

	}


	// ******************* UserOutput ******************


	// ******************* UserOutput::Output ******************


	/**
	* @brief wrap function for UserOutput::Output
	*/
	boost::python::list get_UserOutput_SolutionHistory(UserOutput::Output& output) {

		return std_vector_to_py_list(output.solutionHistory);

	}

	/**
	* @brief wrap function for UserOutput::Output
	*/
	void set_UserOutput_SolutionHistory(UserOutput::Output& output, boost::python::list solutionHistory) {

		output.solutionHistory = py_list_to_std_vector<UserOutput::Output>(solutionHistory);

	}

	/**
	* @brief wrap function for UserOutput::Output
	*/
	boost::python::list get_UserOutput_stages(UserOutput::Output& output) {

		return std_vector_to_py_list(output.stages);

	}

	/**
	* @brief wrap function for UserOutput::Output
	*/
	void set_UserOutput_stages(UserOutput::Output& output, boost::python::list stages) {

		output.stages = py_list_to_std_vector<UserOutput::StageOutput>(stages);

	}



	// ******************* UserOutput::MappingOutput ******************


	/**
	* @brief wrap function for UserOutput::MappingOutput
	*/
	boost::python::list get_MappingOutput_stateNameMapping(UserOutput::MappingOutput& mappingOutput) {

		return std_map_xx_to_py_list(mappingOutput.stateNameMapping);

	};

	/**
	* @brief wrap function for UserOutput::MappingOutput
	*/
	void set_MappingOutput_stateNameMapping(UserOutput::MappingOutput& mappingOutput, boost::python::list stateNameMapping) {

		mappingOutput.stateNameMapping = py_list_to_std_map_xx<std::string, std::string>(stateNameMapping);

	};


	// ******************* UserOutput::StateGridOutput ******************


	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	boost::python::list get_StateGridOutput_timePoints(UserOutput::StateGridOutput& stateGridOutput) {

		return std_vector_to_py_list(stateGridOutput.timePoints);

	};

	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	void set_StateGridOutput_timePoints(UserOutput::StateGridOutput& stateGridOutput, boost::python::list timePoints) {

		stateGridOutput.timePoints = py_list_to_std_vector<double>(timePoints);

	};


	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	boost::python::list get_StateGridOutput_values(UserOutput::StateGridOutput& stateGridOutput) {

		return std_vector_to_py_list(stateGridOutput.values);

	};

	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	void set_StateGridOutput_values(UserOutput::StateGridOutput& stateGridOutput, boost::python::list values) {

		stateGridOutput.timePoints = py_list_to_std_vector<double>(values);

	};




	// ******************* UserOutput::FirstSensitivityOutput ******************


	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	boost::python::list get_FirstSensitivityOutput_mapParamsCols(UserOutput::FirstSensitivityOutput& firstSensitivityOutput) {

		return std_map_xx_to_py_list(firstSensitivityOutput.mapParamsCols);

	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	void set_FirstSensitivityOutput_mapParamsCols(UserOutput::FirstSensitivityOutput& firstSensitivityOutput, boost::python::list mapParamsCols) {

		firstSensitivityOutput.mapParamsCols = py_list_to_std_map_xx<int, int>(mapParamsCols);

	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	boost::python::list get_FirstSensitivityOutput_mapConstrRows(UserOutput::FirstSensitivityOutput& firstSensitivityOutput) {

		return std_map_xx_to_py_list(firstSensitivityOutput.mapConstrRows);

	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	void set_FirstSensitivityOutput_mapConstrRows(UserOutput::FirstSensitivityOutput& firstSensitivityOutput, boost::python::list mapConstrRows) {

		firstSensitivityOutput.mapConstrRows = py_list_to_std_map_xx<std::string, int>(mapConstrRows);

	};


	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	boost::python::list get_FirstSensitivityOutput_values(UserOutput::FirstSensitivityOutput& firstSensitivityOutput) {

		boost::python::list thislist;

		for (int i = 0; i<firstSensitivityOutput.values.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(firstSensitivityOutput.values[i]);
			thislist.append(sublist);
		}

		return thislist;
	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	void set_FirstSensitivityOutput_values(UserOutput::FirstSensitivityOutput& firstSensitivityOutput, boost::python::list values) {

		firstSensitivityOutput.values.resize(boost::python::len(values));

		for (int i = 0; i < firstSensitivityOutput.values.size(); i++) {

			firstSensitivityOutput.values[i].resize(boost::python::len(values[i]));
			for (int j = 0; j < firstSensitivityOutput.values[i].size(); j++) {

				firstSensitivityOutput.values[i][j] = boost::python::extract<double>(values[i][j]);

			}
		}

	};




	// ******************* UserOutput::ParameterGridOutput ******************


	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_timePoints(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.timePoints);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_timePoints(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list timePoints) {

		parameterGridOutput.timePoints = py_list_to_std_vector<double>(timePoints);

	};



	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_values(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.values);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_values(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list values) {

		parameterGridOutput.values = py_list_to_std_vector<double>(values);

	};



	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_lagrangeMultipliers(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.lagrangeMultipliers);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_lagrangeMultipliers(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list lagrangeMultipliers) {

		parameterGridOutput.lagrangeMultipliers = py_list_to_std_vector<double>(lagrangeMultipliers);

	};


	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_firstSensitivities(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.firstSensitivities);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_firstSensitivities(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list firstSensitivities) {

		parameterGridOutput.firstSensitivities = py_list_to_std_vector<UserOutput::FirstSensitivityOutput>(firstSensitivities);

	};





	// ******************* UserOutput::ParameterOutput ******************


	/**
	* @brief wrap function for UserOutput::ParameterOutput
	*/
	boost::python::list get_ParameterOutput_grids(UserOutput::ParameterOutput& parameterOutput) {

		return std_vector_to_py_list(parameterOutput.grids);

	};

	/**
	* @brief wrap function for UserOutput::ParameterOutput
	*/
	void set_ParameterOutput_grids(UserOutput::ParameterOutput& parameterOutput, boost::python::list grids) {

		parameterOutput.grids = py_list_to_std_vector<UserOutput::ParameterGridOutput>(grids);

	};



	// ******************* UserOutput::IntegratorStageOutput ******************


	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_duration(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.durations);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_duration(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list durations) {

		integratorStageOutput.durations = py_list_to_std_vector<UserOutput::ParameterOutput>(durations);

	};



	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_parameters(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.parameters);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_parameters(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list parameters) {

		integratorStageOutput.parameters = py_list_to_std_vector<UserOutput::ParameterOutput>(parameters);

	};



	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_states(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.states);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_states(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list states) {

		integratorStageOutput.states = py_list_to_std_vector<UserOutput::StateOutput>(states);

	};



	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_adjoints(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.adjoints);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_adjoints(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list adjoints) {

		integratorStageOutput.adjoints = py_list_to_std_vector<UserOutput::StateGridOutput>(adjoints);

	};




	// ******************* UserOutput::ConstraintGridOutput ******************


	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	boost::python::list get_ConstraintGridOutput_timePoints(UserOutput::ConstraintGridOutput& constraintGridOutput) {

		return std_vector_to_py_list(constraintGridOutput.timePoints);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	void set_ConstraintGridOutput_timePoints(UserOutput::ConstraintGridOutput& constraintGridOutput, boost::python::list timePoints) {

		constraintGridOutput.timePoints = py_list_to_std_vector<double>(timePoints);

	};


	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	boost::python::list get_ConstraintGridOutput_values(UserOutput::ConstraintGridOutput& constraintGridOutput) {

		return std_vector_to_py_list(constraintGridOutput.values);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	void set_ConstraintGridOutput_values(UserOutput::ConstraintGridOutput& constraintGridOutput, boost::python::list values) {

		constraintGridOutput.values = py_list_to_std_vector<double>(values);

	};


	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	boost::python::list get_ConstraintGridOutput_lagrangeMultiplier(UserOutput::ConstraintGridOutput& constraintGridOutput) {

		return std_vector_to_py_list(constraintGridOutput.lagrangeMultiplier);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	void set_ConstraintGridOutput_lagrangeMultiplier(UserOutput::ConstraintGridOutput& constraintGridOutput, boost::python::list lagrangeMultiplier) {

		constraintGridOutput.lagrangeMultiplier = py_list_to_std_vector<double>(lagrangeMultiplier);

	};



	// ******************* UserOutput::ConstraintOutput ******************


	/**
	* @brief wrap function for UserOutput::ConstraintOutput
	*/
	boost::python::list get_ConstraintOutput_grids(UserOutput::ConstraintOutput& constraintOutput) {

		return std_vector_to_py_list(constraintOutput.grids);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintOutput
	*/
	void set_ConstraintOutput_grids(UserOutput::ConstraintOutput& constraintOutput, boost::python::list grids) {

		constraintOutput.grids = py_list_to_std_vector<UserOutput::ConstraintGridOutput>(grids);

	};





	// ******************* UserOutput::OptimizerStageOutput ******************


	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	boost::python::list get_OptimizerStageOutput_nonlinearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput) {

		return std_vector_to_py_list(optimizerStageOutput.nonlinearConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	void set_OptimizerStageOutput_nonlinearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput, boost::python::list nonlinearConstraints) {

		optimizerStageOutput.nonlinearConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(nonlinearConstraints);

	};


	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	boost::python::list get_OptimizerStageOutput_linearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput) {

		return std_vector_to_py_list(optimizerStageOutput.linearConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	void set_OptimizerStageOutput_linearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput, boost::python::list linearConstraints) {

		optimizerStageOutput.linearConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(linearConstraints);

	};




	// ******************* UserOutput::StageOutput ******************


	/**
	* @brief wrap function for UserOutput::StageOutput
	*/
	boost::python::list get_StageOutput_stageGrid(UserOutput::StageOutput& stageOutput) {

		return std_vector_to_py_list(stageOutput.stageGrid);

	};

	/**
	* @brief wrap function for UserOutput::StageOutput
	*/
	void set_StageOutput_stageGrid(UserOutput::StageOutput& stageOutput, boost::python::list stageGrid) {

		stageOutput.stageGrid = py_list_to_std_vector<double>(stageGrid);

	};

	GenericEso::Ptr get_StageOutput_GenericEso(UserOutput::StageOutput& stageOutput) {
		return stageOutput.esoPtr;
	}


	// ******************* UserOutput::SecondOrderOutput ******************


	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_indicesHessian(UserOutput::SecondOrderOutput& secondOrderOutput) {

		return std_map_xx_to_py_list(secondOrderOutput.indicesHessian);

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_indicesHessian(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list indicesHessian) {

		secondOrderOutput.indicesHessian = py_list_to_std_map_xx<std::string, int>(indicesHessian);

	};


	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_values(UserOutput::SecondOrderOutput& secondOrderOutput) {

		boost::python::list thislist;

		for (int i = 0; i<secondOrderOutput.values.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(secondOrderOutput.values[i]);
			thislist.append(sublist);
		}

		return thislist;

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_values(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list values) {


		secondOrderOutput.values.resize(boost::python::len(values));

		for (int i = 0; i < secondOrderOutput.values.size(); i++) {

			secondOrderOutput.values[i].resize(boost::python::len(values[i]));
			for (int j = 0; j < secondOrderOutput.values[i].size(); j++) {

				secondOrderOutput.values[i][j] = boost::python::extract<double>(values[i][j]);

			}
		}

	};



	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_constParamHessian(UserOutput::SecondOrderOutput& secondOrderOutput) {

		boost::python::list thislist;

		for (int i = 0; i<secondOrderOutput.constParamHessian.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(secondOrderOutput.constParamHessian[i]);
			thislist.append(sublist);
		}

		return thislist;

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_constParamHessian(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list constParamHessian) {


		secondOrderOutput.constParamHessian.resize(boost::python::len(constParamHessian));

		for (int i = 0; i < secondOrderOutput.constParamHessian.size(); i++) {

			secondOrderOutput.constParamHessian[i].resize(boost::python::len(constParamHessian[i]));
			for (int j = 0; j < secondOrderOutput.constParamHessian[i].size(); j++) {

				secondOrderOutput.constParamHessian[i][j] = boost::python::extract<double>(constParamHessian[i][j]);

			}
		}

	};





	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_compositeAdjoints(UserOutput::SecondOrderOutput& secondOrderOutput) {

		boost::python::list thislist;

		for (int i = 0; i<secondOrderOutput.compositeAdjoints.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(secondOrderOutput.compositeAdjoints[i]);
			thislist.append(sublist);
		}

		return thislist;

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_compositeAdjoints(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list compositeAdjoints) {


		secondOrderOutput.compositeAdjoints.resize(boost::python::len(compositeAdjoints));

		for (int i = 0; i < secondOrderOutput.compositeAdjoints.size(); i++) {

			secondOrderOutput.compositeAdjoints[i].resize(boost::python::len(compositeAdjoints[i]));
			for (int j = 0; j < secondOrderOutput.compositeAdjoints[i].size(); j++) {

				secondOrderOutput.compositeAdjoints[i][j] = boost::python::extract<double>(compositeAdjoints[i][j]);

			}
		}

	};







	// ******************* UserOutput::OptimizerOutput ******************


	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	boost::python::list get_OptimizerOutput_globalConstraints(UserOutput::OptimizerOutput& optimizerOutput) {

		return std_vector_to_py_list(optimizerOutput.globalConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	void set_OptimizerOutput_globalConstraints(UserOutput::OptimizerOutput& optimizerOutput, boost::python::list globalConstraints) {

		optimizerOutput.globalConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(globalConstraints);

	};


	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	boost::python::list get_OptimizerOutput_linearConstraints(UserOutput::OptimizerOutput& optimizerOutput) {

		return std_vector_to_py_list(optimizerOutput.linearConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	void set_OptimizerOutput_linearConstraints(UserOutput::OptimizerOutput& optimizerOutput, boost::python::list linearConstraints) {

		optimizerOutput.linearConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(linearConstraints);

	};

	// ******************* GenericEso ******************

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getVariableNames(GenericEso &genEso) {
		std::vector<string> variableNames;
		genEso.getVariableNames(variableNames);
		return std_vector_to_py_list(variableNames);
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getResiduals(GenericEso &genEso) {
		EsoIndex n_eq = genEso.getNumEquations();
		EsoDouble* residuals = new EsoDouble[n_eq];
		genEso.getAllResiduals(n_eq, residuals);
		boost::python::list list = array_pointer_to_py_list(n_eq, residuals);
		delete[] residuals;
		return list;
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getJacobianStruct(GenericEso &genEso) {
		EsoIndex n_nz = genEso.getNumNonZeroes();
		EsoIndex* rowIdx = new EsoIndex[n_nz];
		EsoIndex* colIdx = new EsoIndex[n_nz];
		genEso.getJacobianStruct(n_nz, rowIdx, colIdx);
		boost::python::list rowIndices = array_pointer_to_py_list(n_nz, rowIdx);
		boost::python::list colIndices = array_pointer_to_py_list(n_nz, colIdx);
		delete[] rowIdx;
		delete[] colIdx;
		boost::python::list list;
		list.append(rowIndices);
		list.append(colIndices);

		return list;
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getJacobianValues(GenericEso &genEso) {
		EsoIndex n_nz = genEso.getNumNonZeroes();
		EsoDouble* values = new EsoDouble[n_nz];
		genEso.getJacobianValues(n_nz, values);
		boost::python::list list = array_pointer_to_py_list(n_nz, values);
		delete[] values;
		return list;
	}


}











/**
* @brief pydyos is the main python module exposed 
*/
BOOST_PYTHON_MODULE(pydyos)
{
	boost::python::def("printString", printString);

	boost::python::class_<PyDyos>("PyDyos")
		.add_property("Input", &PyDyos::getInput, &PyDyos::setInput)
		.add_property("Output", &PyDyos::getOutput, &PyDyos::setOutput)
		.def("runPyDyos", &PyDyos::runPyDyos)
		.add_property("nameOutputFile",&PyDyos::getNameOutputFile, &PyDyos::setNameOutputFile)
		.def("convertOutputToInput", &PyDyos::convertOutputToInput)
		;

	boost::python::def("getInputFromJson", &pyDyosFunctions::getInputFromJson);
	boost::python::def("getOutputFromJson", &pyDyosFunctions::getOutputFromJson);
	boost::python::def("convertInputToJson", &pyDyosFunctions::convertInputToJson);
	boost::python::def("convertOutputToJson", &pyDyosFunctions::convertOutputToJson);
	
	{
		/**
		* @brief expose Input
		*/
		boost::python::scope in_Input = boost::python::class_<UserInput::Input> ("Input")
			.add_property("stages", &pyDyosFunctions::pydyosGetInputStages, &pyDyosFunctions::pydyosSetInputStages)
			.def_readwrite("totalEndTimeLowerBound", &UserInput::Input::totalEndTimeLowerBound)
			.def_readwrite("totalEndTimeUpperBound", &UserInput::Input::totalEndTimeUpperBound)
			.def_readwrite("runningMode", &UserInput::Input::runningMode)
			.def_readwrite("integratorInput", &UserInput::Input::integratorInput)
			.def_readwrite("optimizerInput", &UserInput::Input::optimizerInput)
			.def_readwrite("finalIntegration", &UserInput::Input::finalIntegration)
			;


	
		/**
		* @brief expose enum RunningMode
		*/
		boost::python::enum_<UserInput::Input::RunningMode>("runningMode_type")
			.value("SIMULATION", UserInput::Input::SIMULATION)
			.value("SINGLE_SHOOTING", UserInput::Input::SINGLE_SHOOTING)
			.value("MULTIPLE_SHOOTING", UserInput::Input::MULTIPLE_SHOOTING)
			.value("SENSITIVITY_INTEGRATION", UserInput::Input::SENSITIVITY_INTEGRATION);


	}

	{
		/**
		* @brief expose Input Classes EsoInput
		*/
		boost::python::scope in_EsoInput = boost::python::class_<UserInput::EsoInput>("EsoInput")
			.def_readwrite("model", &UserInput::EsoInput::model)
			.def_readwrite("initialModel", &UserInput::EsoInput::initialModel)
			.def_readwrite("type", &UserInput::EsoInput::type)
#ifdef BUILD_WITH_FMU
			.def_readwrite("relativeFmuTolerance", &UserInput::EsoInput::relativeFmuTolerance)
#endif
			;

		/**
		* @brief expose enum EsoType
		*/
		boost::python::enum_<UserInput::EsoInput::EsoType>("type_type")
			.value("JADE", UserInput::EsoInput::JADE)
#ifdef BUILD_WITH_FMU
			.value("FMI", UserInput::EsoInput::FMI)
#endif
			;

	}

	{
		/**
		* @brief expose Input Classes WaveletAdaptationInput
		*/
		boost::python::class_<UserInput::WaveletAdaptationInput>("WaveletAdaptationInput")
			.def_readwrite("maxRefinementLevel", &UserInput::WaveletAdaptationInput::maxRefinementLevel)
			.def_readwrite("minRefinementLevel", &UserInput::WaveletAdaptationInput::minRefinementLevel)
			.def_readwrite("horRefinementDepth", &UserInput::WaveletAdaptationInput::horRefinementDepth)
			.def_readwrite("verRefinementDepth", &UserInput::WaveletAdaptationInput::verRefinementDepth)
			.def_readwrite("etres", &UserInput::WaveletAdaptationInput::etres)
			.def_readwrite("epsilon", &UserInput::WaveletAdaptationInput::epsilon);
	}


	{
		/**
		* @brief expose Input Classes SWAdaptationInput
		*/
		boost::python::class_<UserInput::SWAdaptationInput>("SWAdaptationInput")
			.def_readwrite("maxRefinementLevel", &UserInput::SWAdaptationInput::maxRefinementLevel)
			.def_readwrite("includeTol", &UserInput::SWAdaptationInput::includeTol);

	}



	{

		/**
		* @brief expose Input Classes AdaptationInput
		*/
		boost::python::scope in_AdaptationInput = boost::python::class_<UserInput::AdaptationInput>("AdaptationInput")
			.def_readwrite("adaptType", &UserInput::AdaptationInput::adaptType)
			.def_readwrite("adaptWave", &UserInput::AdaptationInput::adaptWave)
			.def_readwrite("swAdapt", &UserInput::AdaptationInput::swAdapt)
			.def_readwrite("maxAdaptSteps", &UserInput::AdaptationInput::maxAdaptSteps);


		/**
		* @brief expose enum AdaptationType
		*/
		boost::python::enum_<UserInput::AdaptationInput::AdaptationType>("adaptType_type")
			.value("WAVELET", UserInput::AdaptationInput::WAVELET)
			.value("SWITCHING_FUNCTION", UserInput::AdaptationInput::SWITCHING_FUNCTION);

	}


	{


		/**
		* @brief expose Input Classes StructureDetectionInput
		*/
		boost::python::class_<UserInput::StructureDetectionInput>("StructureDetectionInput")
			.def_readwrite("maxStructureSteps", &UserInput::StructureDetectionInput::maxStructureSteps)
			.def_readwrite("createContinuousGrids", &UserInput::StructureDetectionInput::createContinuousGrids);


	}



	{


		/**
		* @brief expose Input Classes ParameterGridInput
		*/
		boost::python::scope in_ParameterGridInput =  boost::python::class_<UserInput::ParameterGridInput>("ParameterGridInput")
			.def_readwrite("numIntervals", &UserInput::ParameterGridInput::numIntervals)
			.def_readwrite("pcresolution", &UserInput::ParameterGridInput::pcresolution)
			.add_property("timePoints", &pyDyosFunctions::get_parameterGridInput_TimePoints, &pyDyosFunctions::set_parameterGridInput_TimePoints)
			.add_property("values", &pyDyosFunctions::get_parameterGridInput_Values, &pyDyosFunctions::set_parameterGridInput_Values)
			.def_readwrite("duration", &UserInput::ParameterGridInput::duration)
			.def_readwrite("hasFreeDuration", &UserInput::ParameterGridInput::hasFreeDuration)
			.def_readwrite("type", &UserInput::ParameterGridInput::type)
			.def_readwrite("adapt", &UserInput::ParameterGridInput::adapt);

		/**
		* @brief expose enum ApproximationType
		*/
		boost::python::enum_<UserInput::ParameterGridInput::ApproximationType>("type_type")
			.value("PIECEWISE_CONSTANT", UserInput::ParameterGridInput::PIECEWISE_CONSTANT)
			.value("PIECEWISE_LINEAR", UserInput::ParameterGridInput::PIECEWISE_LINEAR);



	}



	{

	

		/**
		* @brief expose Input Classes ParameterInput
		*/
		boost::python::scope in_ParameterInput = boost::python::class_<UserInput::ParameterInput>("ParameterInput")
			.def_readwrite("name", &UserInput::ParameterInput::name)
			.def_readwrite("lowerBound", &UserInput::ParameterInput::lowerBound)
			.def_readwrite("upperBound", &UserInput::ParameterInput::upperBound)
			.def_readwrite("value", &UserInput::ParameterInput::value)
			.add_property("grids", &pyDyosFunctions::get_parameterInput_grids, &pyDyosFunctions::set_parameterInput_grids)
			.def_readwrite("paramType", &UserInput::ParameterInput::paramType)
			.def_readwrite("sensType", &UserInput::ParameterInput::sensType);



		/**
		* @brief expose enum ParameterType
		*/
		boost::python::enum_<UserInput::ParameterInput::ParameterType>("paramType_type")
			.value("INITIAL", UserInput::ParameterInput::INITIAL)
			.value("TIME_INVARIANT", UserInput::ParameterInput::TIME_INVARIANT)
			.value("PROFILE", UserInput::ParameterInput::PROFILE)
			.value("DURATION", UserInput::ParameterInput::DURATION);


		/**
		* @brief expose enum ParameterSensitivityType
		*/
		boost::python::enum_<UserInput::ParameterInput::ParameterSensitivityType>("sensType_type")
			.value("FULL", UserInput::ParameterInput::FULL)
			.value("FRACTIONAL", UserInput::ParameterInput::FRACTIONAL)
			.value("NO", UserInput::ParameterInput::NO);


	}


	{

		/**
		* @brief expose Input Classes IntegratorStageInput
		*/
		boost::python::class_<UserInput::IntegratorStageInput>("IntegratorStageInput")
			.def_readwrite("duration", &UserInput::IntegratorStageInput::duration)
			.add_property("parameters", &pyDyosFunctions::get_integratorStageInput_parameters, &pyDyosFunctions::set_integratorStageInput_parameters)
			.def_readwrite("plotGridResolution", &UserInput::IntegratorStageInput::plotGridResolution)
			.add_property("explicitPlotGrid", &pyDyosFunctions::get_integratorStageInput_explicitPlotGrid, &pyDyosFunctions::set_integratorStageInput_explicitPlotGrid);
			;

	}


	{


		/**
		* @brief expose Input Classes ConstraintInput
		*/
		boost::python::scope in_ConstraintInput = boost::python::class_<UserInput::ConstraintInput>("ConstraintInput")
			.def_readwrite("name", &UserInput::ConstraintInput::name)
			.def_readwrite("lowerBound", &UserInput::ConstraintInput::lowerBound)
			.def_readwrite("upperBound", &UserInput::ConstraintInput::upperBound)
			.def_readwrite("timePoint", &UserInput::ConstraintInput::timePoint)
			.def_readwrite("lagrangeMultiplier", &UserInput::ConstraintInput::lagrangeMultiplier)
			.add_property("lagrangeMultiplierVector", &pyDyosFunctions::pydyosGetLagrangeMultiplierVector, &pyDyosFunctions::pydyosSetLagrangeMultiplierVector)
			.def_readwrite("type", &UserInput::ConstraintInput::type)
			.def_readwrite("excludeZero", &UserInput::ConstraintInput::excludeZero);


		/**
		* @brief expose enum ConstraintType
		*/
		boost::python::enum_<UserInput::ConstraintInput::ConstraintType>("type_type")
			.value("PATH", UserInput::ConstraintInput::PATH)
			.value("POINT", UserInput::ConstraintInput::POINT)
			.value("ENDPOINT", UserInput::ConstraintInput::ENDPOINT)
			.value("MULTIPLE_SHOOTING", UserInput::ConstraintInput::MULTIPLE_SHOOTING);

	}

	{

		/**
		* @brief expose Input Classes OptimizerStageInput
		*/
		boost::python::class_<UserInput::OptimizerStageInput>("OptimizerStageInput")
			.def_readwrite("objective", &UserInput::OptimizerStageInput::objective)
			.add_property("constraints", &pyDyosFunctions::get_OptimizerStageInput_constraints, &pyDyosFunctions::set_OptimizerStageInput_constraints)
			.def_readwrite("structureDetection", &UserInput::OptimizerStageInput::structureDetection);


	}



	{


		/**
		* @brief expose Input Classes StageMappiny
		*/
		boost::python::class_<UserInput::StageMapping>("StageMapping")
			.def_readwrite("fullStateMapping", &UserInput::StageMapping::fullStateMapping)
			.add_property("stateNameMapping", &pyDyosFunctions::get_StageMapping_stateNameMapping, &pyDyosFunctions::set_StageMapping_stateNameMapping)
			;

	}


	{


		/**
		* @brief expose Input Classes StageMappiny
		*/
		boost::python::class_<UserInput::MappingInput>("MappingInput")
			.def_readwrite("fullStateMapping", &UserInput::MappingInput::fullStateMapping)
			.add_property("stateNameMapping", &pyDyosFunctions::get_MappingInput_stateNameMapping, &pyDyosFunctions::set_MappingInput_stateNameMapping)
		;

	}


	{

		/**
		* @brief expose Input Classes StageInput
		*/
		boost::python::class_<UserInput::StageInput>("StageInput")
			.def_readwrite("treatObjective", &UserInput::StageInput::treatObjective)
			.def_readwrite("mapping", &UserInput::StageInput::mapping)
			.def_readwrite("eso", &UserInput::StageInput::eso)
			.def_readwrite("integrator", &UserInput::StageInput::integrator)
			.def_readwrite("optimizer", &UserInput::StageInput::optimizer);



	}



	{

		/**
		* @brief expose Input Classes LinearSolverInput
		*/
		boost::python::scope in_LinearSolverInput = boost::python::class_<UserInput::LinearSolverInput>("LinearSolverInput")
			.def_readwrite("type", &UserInput::LinearSolverInput::type);



		/**
		* @brief expose enum LinearSolverType
		*/
		boost::python::enum_<UserInput::LinearSolverInput::SolverType>("type")
			.value("MA28", UserInput::LinearSolverInput::MA28)
			.value("KLU", UserInput::LinearSolverInput::KLU);



	}



	{

		/**
		* @brief expose Input Classes NonLinearSolverInput
		*/
		boost::python::scope in_NonLinearSolverInput = boost::python::class_<UserInput::NonLinearSolverInput>("NonLinearSolverInput")
			.def_readwrite("type", &UserInput::NonLinearSolverInput::type)
			.def_readwrite("tolerance", &UserInput::NonLinearSolverInput::tolerance);



		/**
		* @brief expose enum NonlinearSolverType
		*/
		boost::python::enum_<UserInput::NonLinearSolverInput::SolverType>("type_type")
			.value("NLEQ1S", UserInput::NonLinearSolverInput::NLEQ1S)
			.value("CMINPACK", UserInput::NonLinearSolverInput::CMINPACK);



	}



	{


		/**
		* @brief expose Input Classes DaeInitializationInput
		*/
		boost::python::scope in_DaeInitializationInput = boost::python::class_<UserInput::DaeInitializationInput>("DaeInitializationInput")
			.def_readwrite("type", &UserInput::DaeInitializationInput::type)
			.def_readwrite("linSolver", &UserInput::DaeInitializationInput::linSolver)
			.def_readwrite("nonLinSolver", &UserInput::DaeInitializationInput::nonLinSolver)
			.def_readwrite("maximumErrorTolerance", &UserInput::DaeInitializationInput::maximumErrorTolerance);


		/**
		* @brief expose enum DaeInitializationType
		*/
		boost::python::enum_<UserInput::DaeInitializationInput::DaeInitializationType>("type_type")
			.value("NO", UserInput::DaeInitializationInput::NO)
			.value("FULL", UserInput::DaeInitializationInput::FULL)
			.value("BLOCK", UserInput::DaeInitializationInput::BLOCK)
#ifdef BUILD_WITH_FMU
			.value("FMI", UserInput::DaeInitializationInput::FMI)
#endif
			;


	}


	{

		/**
		* @brief expose Input Classes IntegratorInput
		*/
		boost::python::scope in_IntegratorInputPy = boost::python::class_<UserInput::IntegratorInput>("IntegratorInput")
			.add_property("integratorOptions", &pyDyosFunctions::get_IntegratorInput_integratorOptions, &pyDyosFunctions::set_IntegratorInput_integratorOptions)
			.def_readwrite("type", &UserInput::IntegratorInput::type)
			.def_readwrite("order", &UserInput::IntegratorInput::order)
			.def_readwrite("daeInit", &UserInput::IntegratorInput::daeInit);

		
		/**
		* @brief expose enum IntegratorType
		*/
		boost::python::enum_<UserInput::IntegratorInput::IntegratorType>("type_type")
			.value("NIXE", UserInput::IntegratorInput::NIXE)
			.value("LIMEX", UserInput::IntegratorInput::LIMEX)
			.value("IDAS", UserInput::IntegratorInput::IDAS);


		/**
		* @brief expose enum IntegrationOrder
		*/
		boost::python::enum_<UserInput::IntegratorInput::IntegrationOrder>("order_type")
			.value("ZEROTH", UserInput::IntegratorInput::ZEROTH)
			.value("FIRST_FORWARD", UserInput::IntegratorInput::FIRST_FORWARD)
			.value("FIRST_REVERSE", UserInput::IntegratorInput::FIRST_REVERSE)
			.value("SECOND_REVERSE", UserInput::IntegratorInput::SECOND_REVERSE);


	}


	{

		/**
		* @brief expose Input Classes AdaptationOptions
		*/
		boost::python::scope in_AdaptationOptions = boost::python::class_<UserInput::AdaptationOptions>("AdaptationOptions")
			.def_readwrite("adaptationThreshold", &UserInput::AdaptationOptions::adaptationThreshold)
			.def_readwrite("intermConstraintViolationTolerance", &UserInput::AdaptationOptions::intermConstraintViolationTolerance)
			.def_readwrite("numOfIntermPoints", &UserInput::AdaptationOptions::numOfIntermPoints)
			.def_readwrite("adaptStrategy", &UserInput::AdaptationOptions::adaptStrategy);

		/**
		* @brief expose enum AdaptiveStrategy
		*/
		boost::python::enum_<UserInput::AdaptationOptions::AdaptiveStrategy>("adaptStrategy_type")
			.value("NOADAPTATION", UserInput::AdaptationOptions::NOADAPTATION)
			.value("ADAPTATION", UserInput::AdaptationOptions::ADAPTATION)
			.value("STRUCTURE_DETECTION", UserInput::AdaptationOptions::STRUCTURE_DETECTION)
			.value("ADAPT_STRUCTURE", UserInput::AdaptationOptions::ADAPT_STRUCTURE);

	}



	{


		/**
		* @brief expose Input Classes OptimizerInput
		*/
		boost::python::scope in_OptimizerInput = boost::python::class_<UserInput::OptimizerInput>("OptimizerInput")
			.add_property("optimizerOptions", &pyDyosFunctions::get_OptimizerInput_optimizerOptions, &pyDyosFunctions::set_OptimizerInput_optimizerOptions)
			.def_readwrite("type", &UserInput::OptimizerInput::type)
			.add_property("globalConstraints", &pyDyosFunctions::get_OptimizerInput_globalConstraints, &pyDyosFunctions::set_OptimizerInput_globalConstraints)
			.def_readwrite("adaptationOptions", &UserInput::OptimizerInput::adaptationOptions)
			.def_readwrite("optimizationMode", &UserInput::OptimizerInput::optimizationMode);


		/**
		* @brief expose enum OptimizerType
		*/
		boost::python::enum_<UserInput::OptimizerInput::OptimizerType>("type_type")
			.value("SNOPT", UserInput::OptimizerInput::SNOPT)
			.value("IPOPT", UserInput::OptimizerInput::IPOPT)
			.value("NPSOL", UserInput::OptimizerInput::NPSOL)
			.value("FILTER_SQP", UserInput::OptimizerInput::FILTER_SQP)
			.value("SENSITIVITY_INTEGRATION", UserInput::OptimizerInput::SENSITIVITY_INTEGRATION);


		/**
		* @brief expose enum OptimizationMode
		*/
		boost::python::enum_<UserInput::OptimizerInput::OptimizationMode>("optimizationMode_type")
			.value("MINIMIZE", UserInput::OptimizerInput::MINIMIZE)
			.value("MAXIMIZE", UserInput::OptimizerInput::MAXIMIZE);


	}






	{
		/**
		* @brief expose Output
		*/
		boost::python::scope in_Output = boost::python::class_<UserOutput::Output>("Output")
			.add_property("solutionHistory", &pyDyosFunctions::get_UserOutput_SolutionHistory, &pyDyosFunctions::set_UserOutput_SolutionHistory)
			.add_property("stages", &pyDyosFunctions::get_UserOutput_stages, &pyDyosFunctions::set_UserOutput_stages)
			.def_readwrite("totalEndTimeLowerBound", &UserOutput::Output::totalEndTimeLowerBound)
			.def_readwrite("totalEndTimeUpperBound", &UserOutput::Output::totalEndTimeUpperBound)
			.def_readwrite("solutionTime", &UserOutput::Output::solutionTime)
			.def_readwrite("runningMode", &UserOutput::Output::runningMode)
			.def_readwrite("integratorOutput", &UserOutput::Output::integratorOutput)
			.def_readwrite("optimizerOutput", &UserOutput::Output::optimizerOutput)
			;



		/**
		* @brief expose enum RunningMode
		*/
		boost::python::enum_<UserOutput::Output::RunningMode>("runningMode_type")
			.value("SIMULATION", UserOutput::Output::SIMULATION)
			.value("SINGLE_SHOOTING", UserOutput::Output::SINGLE_SHOOTING)
			.value("MULTIPLE_SHOOTING", UserOutput::Output::MULTIPLE_SHOOTING)
			.value("SENSITIVITY_INTEGRATION", UserOutput::Output::SENSITIVITY_INTEGRATION);


	}


	{
		/**
		* @brief expose Input Classes EsoOutput
		*/
		boost::python::scope in_EsoInput = boost::python::class_<UserOutput::EsoOutput>("EsoOutput")
			.def_readwrite("model", &UserOutput::EsoOutput::model)
			.def_readwrite("type", &UserOutput::EsoOutput::type)
			#ifdef BUILD_WITH_FMU
			.def_readwrite("relativeFmuTolerance", &UserOutput::EsoOutput::relativeFmuTolerance)
			#endif
			;

		/**
		* @brief expose enum EsoType
		*/
		boost::python::enum_<UserOutput::EsoOutput::EsoType>("type_type")
			.value("JADE", UserOutput::EsoOutput::JADE)
		#ifdef BUILD_WITH_FMU
			.value("FMI", UserOutput::EsoOutput::FMI)
		#endif
			;


	}


	{


		/**
		* @brief expose Ouput Classes MappingOutput
		*/
		boost::python::class_<UserOutput::MappingOutput>("MappingOutput")
			.def_readwrite("fullStateMapping", &UserOutput::MappingOutput::fullStateMapping)
			.add_property("stateNameMapping", &pyDyosFunctions::get_MappingOutput_stateNameMapping, &pyDyosFunctions::set_MappingOutput_stateNameMapping)
			;

	}

	{
		/**
		* @brief expose Ouput Classes StateGridOutput
		*/
		boost::python::class_<UserOutput::StateGridOutput>("StateGridOutput")
			.add_property("timePoints", &pyDyosFunctions::get_StateGridOutput_timePoints, &pyDyosFunctions::set_StateGridOutput_timePoints)
			.add_property("values", &pyDyosFunctions::get_StateGridOutput_values, &pyDyosFunctions::set_StateGridOutput_values)
			;

	}


	{
		/**
		* @brief expose Ouput Classes StateOutput
		*/
		boost::python::class_<UserOutput::StateOutput>("StateOutput")
			.def_readwrite("name", &UserOutput::StateOutput::name)
			.def_readwrite("esoIndex", &UserOutput::StateOutput::esoIndex)
			.def_readwrite("grid", &UserOutput::StateOutput::grid)

			;

	}


	{
		/**
		* @brief expose Ouput Classes StateOutput
		*/
		boost::python::class_<UserOutput::FirstSensitivityOutput>("FirstSensitivityOutput")
			.add_property("mapParamsCols", &pyDyosFunctions::get_FirstSensitivityOutput_mapParamsCols, &pyDyosFunctions::set_FirstSensitivityOutput_mapParamsCols)
			.add_property("mapConstrRows", &pyDyosFunctions::get_FirstSensitivityOutput_mapConstrRows, &pyDyosFunctions::set_FirstSensitivityOutput_mapConstrRows)
			.add_property("values", &pyDyosFunctions::get_FirstSensitivityOutput_values, &pyDyosFunctions::set_FirstSensitivityOutput_values)

			;

	}


	{


		/**
		* @brief expose Output Classes ParameterGridOutput
		*/
		boost::python::scope in_ParameterGridOutput = boost::python::class_<UserOutput::ParameterGridOutput>("ParameterGridOutput")
			.def_readwrite("numIntervals", &UserOutput::ParameterGridOutput::numIntervals)
			.add_property("timePoints", &pyDyosFunctions::get_ParameterGridOutput_timePoints, &pyDyosFunctions::set_ParameterGridOutput_timePoints)
			.add_property("values", &pyDyosFunctions::get_ParameterGridOutput_values, &pyDyosFunctions::set_ParameterGridOutput_values)
			.add_property("lagrangeMultipliers", &pyDyosFunctions::get_ParameterGridOutput_lagrangeMultipliers, &pyDyosFunctions::set_ParameterGridOutput_lagrangeMultipliers)
			.def_readwrite("isFixed", &UserOutput::ParameterGridOutput::isFixed)
			.def_readwrite("duration", &UserOutput::ParameterGridOutput::duration)
			.def_readwrite("hasFreeDuration", &UserOutput::ParameterGridOutput::hasFreeDuration)
			.def_readwrite("type", &UserOutput::ParameterGridOutput::type)
			.add_property("firstSensitivities", &pyDyosFunctions::get_ParameterGridOutput_firstSensitivities, &pyDyosFunctions::set_ParameterGridOutput_firstSensitivities);

		/**
		* @brief expose enum ApproximationType
		*/
		boost::python::enum_<UserOutput::ParameterGridOutput::ApproximationType>("type_type")
			.value("PIECEWISE_CONSTANT", UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT)
			.value("PIECEWISE_LINEAR", UserOutput::ParameterGridOutput::PIECEWISE_LINEAR);



	}



	{


		/**
		* @brief expose Input Classes ParameterOutput
		*/
		boost::python::scope in_ParameterOutput = boost::python::class_<UserOutput::ParameterOutput>("ParameterOutput")
			.def_readwrite("name", &UserOutput::ParameterOutput::name)
			.def_readwrite("isOptimizationParameter", &UserOutput::ParameterOutput::isOptimizationParameter)
			.def_readwrite("lowerBound", &UserOutput::ParameterOutput::lowerBound)
			.def_readwrite("upperBound", &UserOutput::ParameterOutput::upperBound)
			.def_readwrite("value", &UserOutput::ParameterOutput::value)
			.add_property("grids", &pyDyosFunctions::get_ParameterOutput_grids, &pyDyosFunctions::set_ParameterOutput_grids)
			.def_readwrite("paramType", &UserOutput::ParameterOutput::paramType)
			.def_readwrite("sensType", &UserOutput::ParameterOutput::sensType)
			.def_readwrite("lagrangeMultiplier", &UserOutput::ParameterOutput::lagrangeMultiplier)
			;



		/**
		* @brief expose enum ParameterType
		*/
		boost::python::enum_<UserOutput::ParameterOutput::ParameterType>("paramType_type")
			.value("INITIAL", UserOutput::ParameterOutput::INITIAL)
			.value("TIME_INVARIANT", UserOutput::ParameterOutput::TIME_INVARIANT)
			.value("PROFILE", UserOutput::ParameterOutput::PROFILE)
			.value("DURATION", UserOutput::ParameterOutput::DURATION);


		/**
		* @brief expose enum ParameterSensitivityType
		*/
		boost::python::enum_<UserOutput::ParameterOutput::ParameterSensitivityType>("sensType_type")
			.value("FULL", UserOutput::ParameterOutput::FULL)
			.value("FRACTIONAL", UserOutput::ParameterOutput::FRACTIONAL)
			.value("NO", UserOutput::ParameterOutput::NO);


	}



	{

		/**
		* @brief expose Output Classes IntegratorStageOutput
		*/
		boost::python::class_<UserOutput::IntegratorStageOutput>("IntegratorStageOutput")
			.add_property("duration", &pyDyosFunctions::get_IntegratorStageOutput_duration, &pyDyosFunctions::set_IntegratorStageOutput_duration)
			.add_property("parameters", &pyDyosFunctions::get_IntegratorStageOutput_parameters, &pyDyosFunctions::set_IntegratorStageOutput_parameters)
			.add_property("adjoints", &pyDyosFunctions::get_IntegratorStageOutput_adjoints, &pyDyosFunctions::set_IntegratorStageOutput_adjoints)
			.def_readwrite("adjoints2ndOrder", &UserOutput::IntegratorStageOutput::adjoints2ndOrder)
			.add_property("states", &pyDyosFunctions::get_IntegratorStageOutput_states, &pyDyosFunctions::set_IntegratorStageOutput_states)

		;

	}


	{

		/**
		* @brief expose Output Classes ConstraintGridOutput
		*/
		boost::python::scope in_ConstraintGridOutput = boost::python::class_<UserOutput::ConstraintGridOutput>("ConstraintGridOutput")
			.add_property("timePoints", &pyDyosFunctions::get_ConstraintGridOutput_timePoints, &pyDyosFunctions::set_ConstraintGridOutput_timePoints)
			.add_property("values", &pyDyosFunctions::get_ConstraintGridOutput_values, &pyDyosFunctions::set_ConstraintGridOutput_values)
			.add_property("lagrangeMultiplier", &pyDyosFunctions::get_ConstraintGridOutput_lagrangeMultiplier, &pyDyosFunctions::set_ConstraintGridOutput_lagrangeMultiplier);



		/**
		* @brief expose enum ConstraintOutputType
		*/
		boost::python::enum_<UserOutput::ConstraintGridOutput::ConstraintOutputType>("ConstraintOutputType_type")
			.value("UPPER_BOUND", UserOutput::ConstraintGridOutput::UPPER_BOUND)
			.value("LOWER_BOUND", UserOutput::ConstraintGridOutput::LOWER_BOUND)
			.value("INFEASIBLE", UserOutput::ConstraintGridOutput::INFEASIBLE)
			.value("ACTIVE", UserOutput::ConstraintGridOutput::ACTIVE);


	}


	{

		/**
		* @brief expose Output Classes ConstraintOutput
		*/
		boost::python::scope in_ConstraintOutput = boost::python::class_<UserOutput::ConstraintOutput>("ConstraintOutput")
			.def_readwrite("name", &UserOutput::ConstraintOutput::name)
			.def_readwrite("lowerBound", &UserOutput::ConstraintOutput::lowerBound)
			.def_readwrite("upperBound", &UserOutput::ConstraintOutput::upperBound)
			.add_property("grids", &pyDyosFunctions::get_ConstraintOutput_grids, &pyDyosFunctions::set_ConstraintOutput_grids)
			.def_readwrite("type", &UserOutput::ConstraintOutput::type);


		/**
		* @brief expose enum OutputConstraintType
		*/
		boost::python::enum_<UserOutput::ConstraintOutput::ConstraintType>("type_type")
			.value("PATH", UserOutput::ConstraintOutput::PATH)
			.value("POINT", UserOutput::ConstraintOutput::POINT)
			.value("ENDPOINT", UserOutput::ConstraintOutput::ENDPOINT);


	}


	{

		/**
		* @brief expose Output Classes OptimizerStageOutput
		*/
		boost::python::class_<UserOutput::OptimizerStageOutput>("OptimizerStageOutput")
			.def_readwrite("objective", &UserOutput::OptimizerStageOutput::objective)
			.add_property("nonlinearConstraints", &pyDyosFunctions::get_OptimizerStageOutput_nonlinearConstraints, &pyDyosFunctions::set_OptimizerStageOutput_nonlinearConstraints)
			.add_property("linearConstraints", &pyDyosFunctions::get_OptimizerStageOutput_linearConstraints, &pyDyosFunctions::set_OptimizerStageOutput_linearConstraints);


	}



	{

		/**
		* @brief expose Output Classes StageOutput
		*/
		boost::python::class_<UserOutput::StageOutput>("StageOutput")
			.def_readwrite("treatObjective", &UserOutput::StageOutput::treatObjective)
			.def_readwrite("mapping", &UserOutput::StageOutput::mapping)
			.def_readwrite("eso", &UserOutput::StageOutput::eso)
			.def_readwrite("integrator", &UserOutput::StageOutput::integrator)
			.def_readwrite("optimizer", &UserOutput::StageOutput::optimizer)
			.add_property("stageGrid", &pyDyosFunctions::get_StageOutput_stageGrid, &pyDyosFunctions::set_StageOutput_stageGrid)
			.add_property("genEso", &pyDyosFunctions::get_StageOutput_GenericEso);


	}


	{


		/**
		* @brief expose Output Classes IntegratorOutput
		*/
		boost::python::scope in_IntegratorOutput = boost::python::class_<UserOutput::IntegratorOutput>("IntegratorOutput")
			.def_readwrite("type", &UserOutput::IntegratorOutput::type)
			.def_readwrite("order", &UserOutput::IntegratorOutput::order);



		/**
		* @brief expose enum OutputIntegratorType
		*/
		boost::python::enum_<UserOutput::IntegratorOutput::IntegratorType>("type_type")
			.value("NIXE", UserOutput::IntegratorOutput::NIXE)
			.value("LIMEX", UserOutput::IntegratorOutput::LIMEX)
			.value("IDAS", UserOutput::IntegratorOutput::IDAS);


		/**
		* @brief expose enum OutputIntegrationOrder
		*/
		boost::python::enum_<UserOutput::IntegratorOutput::IntegrationOrder>("order_type")
			.value("ZEROTH", UserOutput::IntegratorOutput::ZEROTH)
			.value("FIRST_FORWARD", UserOutput::IntegratorOutput::FIRST_FORWARD)
			.value("FIRST_REVERSE", UserOutput::IntegratorOutput::FIRST_REVERSE)
			.value("SECOND_REVERSE", UserOutput::IntegratorOutput::SECOND_REVERSE);


	}



	{


		/**
		* @brief expose Output Classes SecondOrderOutput
		*/
		boost::python::class_<UserOutput::SecondOrderOutput>("SecondOrderOutput")
			.add_property("indicesHessian", &pyDyosFunctions::get_SecondOrderOutput_indicesHessian, &pyDyosFunctions::set_SecondOrderOutput_indicesHessian)
			.add_property("values", &pyDyosFunctions::get_SecondOrderOutput_values, &pyDyosFunctions::set_SecondOrderOutput_values)
			.add_property("constParamHessian", &pyDyosFunctions::get_SecondOrderOutput_constParamHessian, &pyDyosFunctions::set_SecondOrderOutput_constParamHessian)
			.add_property("compositeAdjoints", &pyDyosFunctions::get_SecondOrderOutput_compositeAdjoints, &pyDyosFunctions::set_SecondOrderOutput_compositeAdjoints);



	}


	{

		/**
		* @brief expose Output Classes OptimizerOutput
		*/
		boost::python::scope in_OptimizerOutput = boost::python::class_<UserOutput::OptimizerOutput>("OptimizerOutput")
			.def_readwrite("type", &UserOutput::OptimizerOutput::type)
			.def_readwrite("resultFlag", &UserOutput::OptimizerOutput::resultFlag)
			.add_property("globalConstraints", &pyDyosFunctions::get_OptimizerOutput_globalConstraints, &pyDyosFunctions::set_OptimizerOutput_globalConstraints)
			.add_property("linearConstraints", &pyDyosFunctions::get_OptimizerOutput_linearConstraints, &pyDyosFunctions::set_OptimizerOutput_linearConstraints)
			.def_readwrite("objVal", &UserOutput::OptimizerOutput::objVal)
			.def_readwrite("intermConstrVio", &UserOutput::OptimizerOutput::intermConstrVio)
			.def_readwrite("secondOrder", &UserOutput::OptimizerOutput::secondOrder);


		/**
		* @brief expose enum OutputOptimizerType
		*/
		boost::python::enum_<UserOutput::OptimizerOutput::OptimizerType>("type_type")
			.value("SNOPT", UserOutput::OptimizerOutput::SNOPT)
			.value("IPOPT", UserOutput::OptimizerOutput::IPOPT)
			.value("NPSOL", UserOutput::OptimizerOutput::NPSOL)
			.value("FILTER_SQP", UserOutput::OptimizerOutput::FILTER_SQP)
			.value("SENSITIVITY_INTEGRATION", UserOutput::OptimizerOutput::SENSITIVITY_INTEGRATION);


		/**
		* @brief expose enum ResultFlag
		*/
		boost::python::enum_<UserOutput::OptimizerOutput::ResultFlag>("resultFlag_type")
			.value("OK", UserOutput::OptimizerOutput::OK)
			.value("TOO_MANY_ITERATIONS", UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS)
			.value("INFEASIBLE", UserOutput::OptimizerOutput::INFEASIBLE)
			.value("WRONG_GRADIENTS", UserOutput::OptimizerOutput::WRONG_GRADIENTS)
			.value("NOT_OPTIMAL", UserOutput::OptimizerOutput::NOT_OPTIMAL)
			.value("FAILED", UserOutput::OptimizerOutput::FAILED);


	}

	{	

		/**
		* @brief expose class GenericEso
		*/
		boost::python::class_<GenericEso, GenericEso::Ptr, boost::noncopyable>("GenericEso", boost::python::no_init)
			.add_property("variableNames", &pyDyosFunctions::GenericEso_getVariableNames)
			.add_property("residuals", &pyDyosFunctions::GenericEso_getResiduals)
			.add_property("jacobianStruct", &pyDyosFunctions::GenericEso_getJacobianStruct) //The first entry list[0] contains the list of row indices and the second entry list[1] contains the column indices
			.add_property("jacobianValues", &pyDyosFunctions::GenericEso_getJacobianValues);

		/**
		* @brief expose class GenericEso2ndOrder
		*/
		boost::python::class_<GenericEso2ndOrder, boost::python::bases<GenericEso>, boost::noncopyable>("GenericEso2ndOrder", boost::python::no_init);

		/**
		* @brief expose class FMIGenericEso
		*/
		boost::python::class_<FMIGenericEso, boost::python::bases<GenericEso2ndOrder>>("FMIGenericEso", boost::python::init<string, double>());

		/**
		* @brief expose class JadeGenericEso
		*/
		boost::python::class_<JadeGenericEso, boost::python::bases<GenericEso2ndOrder>>("JadeGenericEso", boost::python::init<string>());
	}

}



