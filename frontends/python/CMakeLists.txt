cmake_minimum_required(VERSION 3.8)


set(PYDYOS_INC	${CMAKE_CURRENT_SOURCE_DIR}/include/PyDyos.hpp)
set(PYDYOS_SRC	${CMAKE_CURRENT_SOURCE_DIR}/src/PyDyos.cpp)


					  
SET(MSVC_COVERAGE_COMPILE_FLAGS "/bigobj")

add_definitions(${MSVC_COVERAGE_COMPILE_FLAGS})





							
add_library(pydyos SHARED  ${PYDYOS_SRC}  ${PYDYOS_INC})


set_target_properties(pydyos
								PROPERTIES SUFFIX .pyd)
	
target_include_directories(pydyos 	PUBLIC 	${CMAKE_CURRENT_SOURCE_DIR}/include
											${Boost_INCLUDE_DIR}
											${pythonIncDir}
											${CMAKE_BINARY_DIR}/configureHeaders
							)
							
							
							
target_link_libraries(pydyos 	
								${pythonLib}
								${Boost_PYTHON3_LIBRARY}
								Dyos
								InputOutput
						)
						

###########################		
# test 
###########################

add_executable(TestPydyos "${CMAKE_CURRENT_SOURCE_DIR}/test/DyosPydyosTest.cpp" "${CMAKE_CURRENT_SOURCE_DIR}/test/pydyosTestsuite.hpp")					
add_dependencies(TestPydyos pydyos Dyos InputOutput)
target_link_libraries(TestPydyos 
						pydyos
						${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
						Dyos
						InputOutput
						)


###########################		
# IDE folder
###########################

set_target_properties(	pydyos 
						PROPERTIES
						FOLDER PyDyOS					
						)
set_target_properties(	TestPydyos 
						PROPERTIES
						FOLDER Tests					
						)


###########################		
# copy python36.dll
###########################


add_custom_command(
					TARGET TestPydyos POST_BUILD        			# Adds a post-build event to MyTest
					COMMAND ${CMAKE_COMMAND} -E copy_if_different  	# which executes "cmake - E copy_if_different..."
					"${python36_root}/python36.dll"      			# <--this is in-file
					$<TARGET_FILE_DIR:TestPydyos>					# <--this is out-file path
					)                 
		
###########################		
# install
###########################
			

install(TARGETS pydyos
			RUNTIME DESTINATION bin
			LIBRARY DESTINATION lib
			)
install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/test/testPy.py" DESTINATION bin)