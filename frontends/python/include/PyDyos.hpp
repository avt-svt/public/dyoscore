/**
* @file PyDyOS.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* PyDyOS - Python Frontend of DyOS                 \n
* =====================================================================\n
* @author Adrian Caspari 
* @date 30.04.2018
*/

#pragma once
#define BOOST_PYTHON_STATIC_LIB  
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "Logger.hpp"
#include "DyosWithLogger.hpp"
#include "UserInput.hpp"
#include "UserOutput.hpp"
#include "GenericEsoFactory.hpp"
#include <string>
#include <vector>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/scope.hpp>
#include <boost/python/enum.hpp>
#include <boost/python/class.hpp>

#include <Python.h>
#include "InputOutputConversions.hpp"
#include "ConvertToXmlJson.hpp"
#include "ConvertFromXmlJson.hpp"
#include <boost/python/to_python_converter.hpp>
#include "OutputChannel.hpp"






/**
* @class pyDyos
* @brief class pydyos 
*/
class PyDyos {

	IOConversions ioConversion;

	UserInput::Input Input;
	UserOutput::Output Output;

	std::string nameOutputFile;
	
	
	OutputChannel::Ptr jsonFile;
	Logger::Ptr logger;

	Logger::LoggingChannel channelIndex;



public:

	PyDyos() {

		nameOutputFile = "outputPyDyos";


	};
	~PyDyos() {
		
	};


	/**
	*@brief getter for Input
	*/
	UserInput::Input getInput();
	/**
	*@brief setter for Input
	*/
	void setInput(UserInput::Input input);

	/**
	*@brief getter for Output
	*/
	UserOutput::Output getOutput();

	/**
	*@brief setter for Output
	*/
	void setOutput(UserOutput::Output output);

	/**
	*@brief setter for nameOutputFile
	*/
	void setNameOutputFile(std::string nameOutputFile);

	/**
	*@brief getter for nameOutputFile
	*/
	std::string getNameOutputFile();

	/**
	*@brief run DyOS - solve dynamic optimization problem
	*/
	void runPyDyos();

	/**
	*@brief convert input to output
	*/
	UserInput::Input convertOutputToInput(UserOutput::Output);



};