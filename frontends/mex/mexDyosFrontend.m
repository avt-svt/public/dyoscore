% /**
% * @file mexdyosFrontend.m
% *
% * =====================================================================\n
% * &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
% * =====================================================================\n
% * DyOS MEX frontend                                                    \n
% * =====================================================================\n
% * This file contains the a Matlab test script for the Mex file. It uses\n
% * the car example (cf. Manual and dyos/example).
% * =====================================================================\n
% * @author Susanne Sass, Falco Jung
% * @date 28.11.2018
% */

clear ;
clc;

% Please define the directory of your build that holds Dyos.dll and the
% dll of your model.
buildDir = 'C:\DyOS_Build\Release';
%buildDir = 'C:\DyOS_Build\Debug';

currentDirectory = pwd ;
addpath(buildDir);
cd(buildDir);

% Specify the name of your model dll.
modelName = 'CarExampleManualJade';
%modelName = 'CarExampleManualFMU';

esoInput = struct;

esoInput.type = 'JADE';
%esoInput.type = 'FMI';

esoInput.model = [buildDir,'\',modelName];

% Specify integration input.
numStages = 1 ;

integratorStageInput = struct;

  % duration of simulation horizon
  finaltime = struct;
  finaltime.lowerBound = 0.1 / numStages ;
  finaltime.upperBound = 100 / numStages ;
  finaltime.value = 10.0;
  finaltime.sensType = 'FRACTIONAL';
  finaltime.paramType = 'DURATION';
  
integratorStageInput.duration = finaltime;

  % control function k
  parameterGridInput1 = struct;
  parameterGridInput1.numIntervals = 3;
  parameterGridInput1.values = ones(1,parameterGridInput1.numIntervals);
  parameterGridInput1.adapt.maxAdaptSteps = 10;
  parameterGridInput1.pcresolution = 1;
  parameterGridInput1.type = 'PIECEWISE_CONSTANT';
  parameterGridInput1.hasFreeDuration = true;

  control1 = struct;
  control1.name = 'k';
  control1.lowerBound = -2.0;
  control1.upperBound = 2.0;
  control1.paramType = 'PROFILE';
  control1.grids = parameterGridInput1;
  control1.sensType = 'FULL';

integratorStageInput.parameters = control1;

% finer grid for final simulation for plotting output
integratorStageInput.plotGridResolution = 100;

% Specify optimization input.

optimizerStageInput = struct;

  % objective
  objective = struct;
  objective.name = 'obj';
  objective.lowerBound = -100000.0;
  objective.upperBound =  100000.0; 

optimizerStageInput.objective = objective;

  % path constraint on velocity
  constraint1 = struct;
  constraint1.name = 'velo';
  constraint1.type = 'PATH';
  constraint1.lowerBound = -1.6;
  constraint1.upperBound = 1.6;

  % endpoint constraint on velocity
  constraint2 = struct;
  constraint2.name = 'velo';
  constraint2.type = 'ENDPOINT';
  constraint2.lowerBound = 0.0;
  constraint2.upperBound = 0.0;

optimizerStageInput.constraints = [constraint1, constraint2];

% combine integration and optimization input
stageInput = struct;
stageInput.eso = esoInput;
stageInput.integrator = integratorStageInput;
stageInput.optimizer = optimizerStageInput;
stageInput.mapping.fullStateMapping = false; % since single stage
%stageInput.treatObjective = true;

% Define integrator
integratorInput = struct;
integratorInput.type = 'NIXE';
integratorInput.order = 'FIRST_FORWARD';

% Define optimizer
optimizerInput = struct;
optimizerInput.optimizationMode='MINIMIZE';
optimizerInput.type = 'IPOPT';
optimizerInput.optimizerOptions = {'Major Iterations Limit','250';...
                                   'Minor Iterations Limit','500';...
                                   };
optimizerInput.adaptStrategy='ADAPT_STRUCTURE';

% Define running mode and combine previous user input
input = struct;
input.runningMode = 'SINGLE_SHOOTING';
input.stages = stageInput;
input.integratorInput = integratorInput;
input.optimizerInput = optimizerInput;

output = mexDyos(input) 

clear mexDyos;
cd(currentDirectory);

%To provide correct input for Dyos, the names of the struct fields
%must be consistent with the struct fields used in C++.
%In the following, you can find the relevant tree structure:
%input->fields[runningMode('SIMULATION', 'SINGLE_SHOOTING'), stages(struct), integratorInput(struct), optimizerInput(struct),
%              totalEndTimeLowerBound(double), totalEndTimeUpperBound(double)]
%
%optimizerInput->fields[type('SNOPT', 'IPOPT', 'NPSOL', 'FILTER_SQP'), optimizerOptions(nx2 cell containing strings),
%                       optimizationMode('MINIMIZE', 'MAXIMIZE'),
%                       adaptationTreshold(double),
%                       adaptStrategy('NOADAPTION', 'ADAPTATION', 'STRUCTURE_DETECTION', 'ADAPT_STRUCTURE')]
%
%
%integratorInput->fields[type('NIXE', 'LIMEX', 'IDAS'), order('ZEROTH', 'FIRST_FORWARD', 'FIRST_REVERSE', 'SECOND_REVERSE'),
%                        IntegratorOptions(nx2 cell containing strings)]
%
%stages->fields[treatObjective(bool), eso(struct), integrator(struct), optimizer(struct)]
%
%eso->fields[model(string), process(string), type('JADE')]
%
%stagesIntegrator->fields[duration(struct), parameters(struct array)] duration and parameters are structs of parameter type
%
%stagesOptimizer->fields[objective(struct), constraints(struct array),
%                        structureDetection(struct)] objective and constraints are structs of constraint type
%
%structureDetection->fields[maxStructureSteps(int)] 
%
%constraint->fields[name(string), lowerBound(double), upperBound(double), timePoint(double), lagrangeMultiplier(double),
%                   lagrangeMultiplierVector(double array), type('PATH', 'ENDPOINT', 'POINT')]
%
%parameter->fields[name(string), lowerBound(double), upperBound(double), value(double), grids(struct array), 
%                  paramType('PROFILE', 'TIME_INVARIANT', 'DURATION', 'INITIAL'), 
%                  sensType('FULL', 'FRACTIONAL', 'NO')]
%
%grid->fields[numIntervals(int), timePoints(doubleArray), values(doubleArray), duration(double), hasFreeDuration(bool),
%             type('PIECEWISE_LINEAR', PIECEWISE_CONSTANT'), adapt(struct)]
%
%adapt->fields[adaptType('WAVELET'), maxAdaptSteps(int), adaptWave(struct)]
%
%adaptWave->fields[maxRefinementLevel(int), minRefinementLevel(int),
%                  horRefinementLevel(int), verRefinementLevel(int),
%                  etres(int), epsilon(int)]
