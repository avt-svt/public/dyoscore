This frontend will only work on 64bit windows.

To run this m-file, open Matlab and specify your build directory, the name of your model and the corresponding user input.

Note: SNOPT only works in release mode.
Note: Some outputs of optimizers (IPOPT) are not yet shown in Matlab command line. However, this is only a display problem.
