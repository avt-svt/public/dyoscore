#include "ConvertOutput.hpp"

void setEnumFieldApproximationType(mxArray *matlabOutput, UserOutput::ParameterGridOutput::ApproximationType approximationType, char *fieldname, const int index);
void setEnumFieldParameterType(mxArray *matlabOutput, UserOutput::ParameterOutput::ParameterType parameterType, char *fieldname, const int index);
void setEnumFieldParameterSensitivityType(mxArray *matlabOutput, UserOutput::ParameterOutput::ParameterSensitivityType parameterSensitivityType, char *fieldname, const int index);


/**
* @brief converts given dyos-struct into mxArray
*
* @param dyosOut given dyos-struct UserOutput::StateGridOutput
* @param index index of the desired element
* @return mxArray
* @note creates fields timePoints and values
* @author Konstanze K�lle, Tjalf Hoffmann
*/
mxArray *convertStateGridOutput(UserOutput::StateGridOutput dyosOut)
{
  const char *fieldNames[] = {"timePoints", "values"};
  mwSize dims[] = {1,1};
  mxArray* convertedOutput = mxCreateStructArray(2, dims, 2, fieldNames);

  setDoubleVectorField(convertedOutput, dyosOut.timePoints, "timePoints");
  setDoubleVectorField(convertedOutput, dyosOut.values, "values");

  return convertedOutput;
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::StateOutput
* @param index index of the desired element
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertStateOutput(mxArray *matlabOutput, UserOutput::StateOutput dyosOut, const int index)
{
  setStringField(matlabOutput, dyosOut.name, "name", index);
  setScalarField(matlabOutput, dyosOut.esoIndex, "esoIndex", index);
  mxArray *stateGridOutput = convertStateGridOutput(dyosOut.grid);
  checkOutputField(matlabOutput, "grid");
  mxSetField(matlabOutput, index, "grid", stateGridOutput);
}


/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param convertedOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::FirstSensitivityOutput
* @param index index of the desired element
* @note ??
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertFirstSensitivityOutput(mxArray *convertedOutput,
                                   UserOutput::FirstSensitivityOutput dyosOut,
                             const int index)
{
  setDoubleMatrixField(convertedOutput, dyosOut.values, "values", index);

  setMap(convertedOutput, dyosOut.mapConstrRows, "mapConstrRows", index);
  
  setMap(convertedOutput, dyosOut.mapParamsCols, "mapParamsCols", index);
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::ParameterGridOutput
* @param index index of the desired element
* @note ??
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertParameterGridOutput(mxArray* matlabOutput, UserOutput::ParameterGridOutput dyosOut, const int index)
{
  checkOutputField(matlabOutput, "lagrangeMultipliers");
  checkOutputField(matlabOutput, "firstSensitivities");

  setScalarField(matlabOutput, dyosOut.numIntervals, "numIntervals", index);

  setDoubleVectorField(matlabOutput, dyosOut.timePoints, "timePoints", index);
  setDoubleVectorField(matlabOutput, dyosOut.values, "values", index);
  
  
  setDoubleVectorField(matlabOutput, dyosOut.lagrangeMultipliers, "lagrangeMultipliers", index);
  setScalarField(matlabOutput, dyosOut.duration, "duration", index);
  setBoolField(matlabOutput, dyosOut.hasFreeDuration,"hasFreeDuration", index);

  const char *fieldNames[] = {"mapParamsCols", "mapConstrRows", "values"};
  mwSize ndim=2, dims[]={1, dyosOut.firstSensitivities.size()};
  mxArray *firstSensOut = mxCreateStructArray(ndim, dims, 3, fieldNames);
  for(int i=0; i<int(dyosOut.firstSensitivities.size()); i++){
    convertFirstSensitivityOutput(firstSensOut, dyosOut.firstSensitivities[i], i);
  }
  
  mxSetField(matlabOutput, index, "firstSensitivities", firstSensOut);

  setEnumFieldApproximationType(matlabOutput, dyosOut.type, "type", index);
}

/**
* @brief puts given enum into the field in a given Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given enum UserOutput::ParameterGridOutput::ApproximationType
* @param fieldname name of the field
* @param index index of the desired element
* @note raises an Matlab error if field could not be created
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void setEnumFieldApproximationType(mxArray *matlabOutput, UserOutput::ParameterGridOutput::ApproximationType approximationType, char *fieldname, const int index)
{
  std::string matlabEnumString;
  switch (approximationType)
  {
  case UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT:
    matlabEnumString = "PIECEWISE_CONSTANT";
    break;
  case UserOutput::ParameterGridOutput::PIECEWISE_LINEAR:
    matlabEnumString = "PIECEWISE_LINEAR";
    break;
  default:
    std::string errMsg = "Could not create field ";
    errMsg.append(fieldname);
    mexErrMsgTxt(errMsg.c_str());
  }
  setStringField(matlabOutput, matlabEnumString, "type", index);
}


/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::ParameterOutput
* @param index index of the desired element
* @note raises an Matlab error if field grids has not desired length
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertParameterOutput(mxArray *matlabOutput, UserOutput::ParameterOutput dyosOut, const int index)
{
  if(dyosOut.grids.size() > 0){ //normally, this should be the case
    int numDyosOutGrids =dyosOut.grids.size();
    mwSize numDims =2, dims[] = {1, numDyosOutGrids};
    mxArray *grids;
    //if grids do not exist yet, create a new field (not all fieldnames set, some are added later
    if(mxGetFieldNumber(matlabOutput, "grids") < 0){
      mxAddField(matlabOutput, "grids");
    }
    //if grids did not exist before, usually it is a duration, initial or a time invariant parameter
    //so lagrangeMultiplier is a scalar
    checkOutputField(matlabOutput, "lagrangeMultiplier");
    setScalarField(matlabOutput, dyosOut.lagrangeMultiplier, "lagrangeMultiplier", index);
    const char *fieldnames[] = {"numIntervals", "timePoints", "values", "duration", "hasFreeDuration"};
    grids = mxCreateStructArray(numDims, dims, 5, fieldnames);
  
    //adaption may have changed the number of grids - rezize gridvector for output
    // removed resizeStructField, because using the original grid data structure caused an segfault
    // now we create a new structure every time and override the input copy
    //resizeStructField(matlabOutput, "grids", numDyosOutGrids, index);
    
    for(int i=0; i<numDyosOutGrids; i++){
      convertParameterGridOutput(grids, dyosOut.grids[i], i);
    }
    mxSetField(matlabOutput, index, "grids", grids);
  }
  
  setStringField(matlabOutput, dyosOut.name, "name", index);
  setBoolField(matlabOutput, dyosOut.isOptimizationParameter, "isOptimizationParameter", index);
  setScalarField(matlabOutput, dyosOut.lowerBound, "lowerBound", index);
  setScalarField(matlabOutput, dyosOut.upperBound, "upperBound", index);
  setScalarField(matlabOutput, dyosOut.value, "value", index);
  setEnumFieldParameterType(matlabOutput, dyosOut.paramType, "paramType", index);
  setEnumFieldParameterSensitivityType(matlabOutput, dyosOut.sensType, "sensType", index);
}

/**
* @brief puts given enum into the field in a given Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given enum UserOutput::ParameterOutput::ParameterType
* @param fieldname name of the field
* @param index index of the desired element
* @note raises an Matlab error if field could not be created
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void setEnumFieldParameterType(mxArray *matlabOutput, UserOutput::ParameterOutput::ParameterType parameterType, char *fieldname, const int index)
{
  std::string matlabEnumString;
  switch (parameterType)
  {
  case UserOutput::ParameterOutput::INITIAL:
    matlabEnumString = "INITIAL";
    break;
  case UserOutput::ParameterOutput::TIME_INVARIANT:
    matlabEnumString = "TIME_INVARIANT";
    break;
  case UserOutput::ParameterOutput::PROFILE:
    matlabEnumString = "PROFILE";
    break;
  case UserOutput::ParameterOutput::DURATION:
    matlabEnumString = "DURATION";
    break;
  default:
    std::string errMsg = "Could not create field ";
    errMsg.append(fieldname);
    mexErrMsgTxt(errMsg.c_str());
  }
  setStringField(matlabOutput, matlabEnumString, "type", index);
}

/**
* @brief puts given enum into the field in a given Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given enum UserOutput::ParameterOutput::ParameterSensitivityType
* @param fieldname name of the field
* @param index index of the desired element
* @note raises an Matlab error if field could not be created
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void setEnumFieldParameterSensitivityType(mxArray *matlabOutput, UserOutput::ParameterOutput::ParameterSensitivityType parameterSensitivityType, char *fieldname, const int index)
{
  std::string matlabEnumString;
  switch (parameterSensitivityType)
  {
  case UserOutput::ParameterOutput::FULL:
    matlabEnumString = "FULL";
    break;
  case UserOutput::ParameterOutput::FRACTIONAL:
    matlabEnumString = "FRACTIONAL";
    break;
  case UserOutput::ParameterOutput::NO:
    matlabEnumString = "NO";
    break;
  default:
    std::string errMsg = "Could not create field ";
    errMsg.append(fieldname);
    mexErrMsgTxt(errMsg.c_str());
  }
  setStringField(matlabOutput, matlabEnumString, "type", index);
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::IntegratorStageOutput
* @param index index of the desired element
* @note ??
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertIntegratorStageOutput(mxArray *matlabOutput, UserOutput::IntegratorStageOutput dyosOut, const int index)
{

  int numDyosOutDurations = dyosOut.durations.size();
  resizeStructField(matlabOutput, "duration", numDyosOutDurations, index);
  mxArray *duration = mxGetField(matlabOutput, index, "duration");
  if(duration == NULL && numDyosOutDurations > 0){
    mexErrMsgTxt("failed to read out field duration in IntegratorStageOutput struct");
  }
  for(int i=0; i<numDyosOutDurations; i++){
    convertParameterOutput(duration, dyosOut.durations[i], i);
  }
  mxSetField(matlabOutput, index, "duration", duration);

  
  int numPars = dyosOut.parameters.size();
  resizeStructField(matlabOutput, "parameters", numPars, index);
  mxArray *parameters = mxGetField(matlabOutput, index, "parameters");
  if(parameters == NULL && numPars > 0){
    mexErrMsgTxt("failed to read out field parameters in IntegratorStageOutput struct");
  }
  // here we have to add parameters to the output struct (since it is 
  /*if(numPars != mxGetNumberOfElements(parameters)){
    resizeStructField(matlabOutput, "parameters", numPars, index);
    mxArray *parameters = mxGetField(matlabOutput, index, "parameters");
    //mexErrMsgTxt("Size mismatch of intput parameters and output parameters.\n"
    //             "This is propably a bug - report it to the developers.\n");
  }*/
  for(int i=0; i<numPars; i++){
    convertParameterOutput(parameters, dyosOut.parameters[i], i);
  }
  mxSetField(matlabOutput, index, "parameters", parameters);
 
  checkOutputField(matlabOutput, "states");
  const char *fieldNames[] = {"name", "esoIndex", "grid"};
  int numStates = dyosOut.states.size();
  mwSize ndims = 2, dims[] = {1, numStates};
  mxArray* states = mxCreateStructArray(ndims, dims, 3, fieldNames);
  for(int i=0; i<numStates; i++){
    convertStateOutput(states, dyosOut.states[i], i);
  }
  mxSetField(matlabOutput, index, "states", states);
}