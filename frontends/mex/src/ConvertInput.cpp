#include "ConvertInput.hpp"
#include <sstream>

UserInput::Input::RunningMode convertRunningMode(const mxArray *matlabInput);

bool existField(const mxArray *matlabInput, char *fieldname)
{
  if(mxGetFieldNumber(matlabInput, fieldname)>-1){ // check whether field �"fieldname" exists
    return true;
  }
  else{
    return false;
  }
}

/**
* @brief checks if a field exists in a given Matlab-struct
*
* @param matlabInput Matlab struct to be checked
* @param fieldname name of the field
* @return true, if field exists, otherwise false
* @note raises an Matlab error if field does not exist
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void checkField(const mxArray *matlabInput, char *fieldname)
{

  if(!existField(matlabInput, fieldname)){
    mexPrintf("Fieldname not existent: %s\n",fieldname);
    throw DyosMexException();
  }
}

/**
* @brief converts string of the field in a given Matlab-struct to string
*
* @param matlabInput given Matlab struct
* @param fieldname name of the field
* @param index index of the desired element
* @return string
* @note raises an Matlab error if field contains no string
* @author Konstanze K�lle, Tjalf Hoffmann
*/
std::string convertString(const mxArray *matlabInput, char *fieldname, const int index)
{
  mxArray * matlabField;
  checkField(matlabInput, fieldname);
  matlabField  = mxGetField(matlabInput, index, fieldname);
  if(matlabField == NULL){
    mexPrintf("%s is empty.\n", fieldname);
    throw DyosMexException();
  }
  if(!mxIsChar(matlabField)){
    mexPrintf("%s must contain a string!\n", fieldname);
  }
  std::string model = mxArrayToString(matlabField);
      
  return model;
}

/**
* @brief converts scalar of the field in a given Matlab-struct to double
*
* @param matlabInput given Matlab struct
* @param fieldname name of the field
* @param index index of the desired element
* @return double
* @note raises an Matlab error if field contains no scalar
* @author Konstanze K�lle, Tjalf Hoffmann
*/
double convertScalar(const mxArray *matlabInput, char *fieldname, const int index)
{
  checkField(matlabInput, fieldname);
  mxArray* p_variables   = mxGetField(matlabInput, index, fieldname);
  
  if(p_variables == NULL){
    mexPrintf("%s is empty.\n", fieldname);
    throw DyosMexException();
  } 
  // check to make sure the input argument is a scalar
  bool isValid = true;
  if(p_variables == NULL){
    isValid = false;
  }
  else{
    if( !mxIsDouble(p_variables) || mxIsComplex(p_variables)
       || mxIsEmpty(p_variables) || mxGetNumberOfElements(p_variables)!=1 ) {
      isValid = false;
    }
  }
  if(!isValid){
    mexPrintf("%s must contain a scalar!\n", fieldname);
    throw DyosMexException();
  }
  double scalValue = mxGetScalar(p_variables);
  return scalValue;
}

bool isEmptyField(const mxArray *matlabInput, char *fieldname, const int index)
{
  checkField(matlabInput, fieldname);
  mxArray* field = mxGetField(matlabInput, index, fieldname);
  if(field == NULL){
    return true;
  }
  return mxIsEmpty(field);
}

/**
* @brief converts double field of the field in a given Matlab-struct to double vector
*
* @param matlabInput given Matlab struct
* @param fieldname name of the field
* @param index index of the desired element
* @return double vector
* @note raises an Matlab error if field contains no double field
* @author Konstanze K�lle, Tjalf Hoffmann
*/
std::vector<double> convertDoubleVector(const mxArray *matlabInput, char *fieldname, const int index)
{
  checkField(matlabInput, fieldname);
  mxArray *matlabField = mxGetField(matlabInput, index, fieldname);
  if(matlabField == NULL){
    mexPrintf("%s is empty.\n", fieldname);
    throw DyosMexException();
  }
  if(!mxIsDouble(matlabField) || mxIsEmpty(matlabField)){
    mexPrintf("%s  must contain a double field!\n", fieldname);
    throw DyosMexException();
  }
  const int fieldSize = mxGetNumberOfElements(matlabField);
  std::vector<double> convertedVector(fieldSize);
  double *d = mxGetPr(matlabField);
  convertedVector.assign(d,d + fieldSize);
  return convertedVector;
}

/**
* @brief converts boolean of the field in a given Matlab-struct to boolean
*
* @param matlabInput given Matlab struct
* @param fieldname name of the field
* @param index index of the desired element
* @return boolean
* @note raises an Matlab error if field contains no boolean variable
* @author Konstanze K�lle, Tjalf Hoffmann
*/
bool convertBool(const mxArray *matlabInput, char *fieldname, const int index)
{
  checkField(matlabInput, fieldname);
  mxArray *matlabField = mxGetField(matlabInput, index, fieldname);
  if(matlabField == NULL){
    mexPrintf("%s is empty.\n", fieldname);
    throw DyosMexException();
  }
  if(!mxIsLogical(matlabField) || mxIsEmpty(matlabField)){
    mexPrintf("%s must contain a boolean variable!\n", fieldname);
    throw DyosMexException();
  }
  bool convertedBool = mxIsLogicalScalarTrue(matlabField);
  return convertedBool;
}

std::map<std::string, std::string> convertMap(const mxArray *matlabInput, char *fieldname, const int index)
{
  checkField(matlabInput, fieldname);
  std::map<std::string, std::string> convertedMap;
  mxArray *matlabField = mxGetField(matlabInput, index, fieldname);
  if(matlabField == NULL){
    mexPrintf("%s is empty.\n", fieldname);
    throw DyosMexException();
  }
    std::string errMsg = fieldname;
    errMsg.append(" must contain a nx2 cell array of key-value pairs (strings)\n");
  if(!mxIsCell(matlabField)){
    errMsg.append("Input field was no cell array\n");
    mexPrintf(errMsg.c_str());
    throw DyosMexException();
  }
  else if(mxIsEmpty(matlabField)){
    return convertedMap;
  }
  size_t numRows = mxGetM(matlabField);
  size_t numCols = mxGetN(matlabField);
  
  if(numCols != 2){
    errMsg.append("Input cell array has wrong dimensons");
  }
  for(unsigned i=0; i< numRows; i++){
    std::string key, value;
    //Matlab data are column major
    mxArray *keyCell = mxGetCell(matlabField, i);
    mxArray *valueCell = mxGetCell(matlabField, numRows + i);
    if(!mxIsChar(keyCell) || !mxIsChar(valueCell)){
      errMsg.append("Input cell does not contain string pair at row ");
      std::stringstream number;
      number<<i<<"\n";
      errMsg.append(number.str());
      mexPrintf(errMsg.c_str());
      throw DyosMexException();
    }
    key = mxArrayToString(keyCell);
    value = mxArrayToString(valueCell);
    convertedMap[key] = value;
  }
  return convertedMap;
}

/**
* @brief converts fields in a given Matlab-struct to UserInput::StageInput
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return UserInput::StageInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::StageInput convertStageInput(mxArray *matlabInput, const int index)
{
  if(matlabInput == NULL){
    mexPrintf("stage input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("stage input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::StageInput stageInput;
  try{
    checkField(matlabInput, "eso");
    mxArray *eso = mxGetField(matlabInput, index, "eso");
    stageInput.eso = convertEsoInput(eso);
  
    checkField(matlabInput, "integrator");
    mxArray *integrator = mxGetField(matlabInput, index, "integrator");
    stageInput.integrator = convertIntegratorStageInput(integrator);

    if(existField(matlabInput, "optimizer")){
      mxArray *optimizer = mxGetField(matlabInput, index, "optimizer");
      stageInput.optimizer = convertOptimizerStageInput(optimizer);
    }

    if(existField(matlabInput, "treatObjective")){
      stageInput.treatObjective = convertBool(matlabInput, "treatObjective", index);  
    }

    stageInput.mapping.fullStateMapping = true;
  }
  catch(DyosMexException e){
    mexPrintf("error occurred on stage %d\n", (index+1));
    throw e;
  }


  return stageInput;
}

/**
* @brief converts fields in a given Matlab-struct to UserInput::Input
*
* @param matlabInput given Matlab struct
* @return UserInput::Input
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::Input convertInput(const mxArray *matlabInput)
{
  if(matlabInput == NULL){
    mexPrintf("input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::Input input;
  input.runningMode = convertRunningMode(matlabInput);

  if(existField(matlabInput, "totalEndTimeLowerBound")){
    input.totalEndTimeLowerBound = convertScalar(matlabInput, "totalEndTimeLowerBound");
  }
  if(existField(matlabInput, "totalEndTimeUpperBound")){
    input.totalEndTimeUpperBound = convertScalar(matlabInput, "totalEndTimeUpperBound");
  }

  checkField(matlabInput, "stages");
  mxArray *stages = mxGetField(matlabInput, 0, "stages");
  int numStages = mxGetNumberOfElements(stages);
  input.stages.resize(numStages);
  for(int i=0; i<numStages; i++){
    input.stages[i] = convertStageInput(stages, i);
  }
  input.stages.back().mapping.fullStateMapping = false;

  checkField(matlabInput, "integratorInput");
  mxArray *integratorInput = mxGetField(matlabInput, 0, "integratorInput");
  input.integratorInput = convertIntegratorInput(integratorInput);
  
  bool doConvertOptimizerInput = false;
  if(input.runningMode == UserInput::Input::SIMULATION){
    if(existField(matlabInput, "optimizerInput")){
      doConvertOptimizerInput = true;
    }
  }
  else{
    checkField(matlabInput, "optimizerInput");
    doConvertOptimizerInput = true;
  }
  if(doConvertOptimizerInput){
    mxArray *optimizerInput = mxGetField(matlabInput, 0, "optimizerInput");
    input.optimizerInput = convertOptimizerInput(optimizerInput);
  }
  return input;
}

/**
* @brief converts string in a given Matlab-struct to enum UserInput::Input::RunningMode
*
* @param matlabInput given Matlab struct
* @return enum UserInput::Input::RunningMode
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::Input::RunningMode convertRunningMode(const mxArray *matlabInput)
{
  UserInput::Input::RunningMode runningMode;
  std::string typeString = convertString(matlabInput, "runningMode");
  if(typeString.compare("SIMULATION") == 0){
    runningMode = UserInput::Input::SIMULATION;
  }    
  else if(typeString.compare("SINGLE_SHOOTING") == 0){  
    runningMode = UserInput::Input::SINGLE_SHOOTING;
  }
  else if(typeString.compare("MULTIPLE_SHOOTING") == 0){
    runningMode = UserInput::Input::MULTIPLE_SHOOTING;
  }
  else if(typeString.compare("SENSITIVITY_INTEGRATION") == 0){
    runningMode = UserInput::Input::SENSITIVITY_INTEGRATION;
  }
  else{
    mexPrintf("runningMode has no desired value.\n");
    throw DyosMexException();
  }
  return runningMode;
}
	