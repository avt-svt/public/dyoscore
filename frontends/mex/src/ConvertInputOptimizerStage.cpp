#include "ConvertInput.hpp"

UserInput::AdaptationOptions::AdaptiveStrategy convertAdaptiveStrategy(const mxArray *matlabInput)
{
  UserInput::AdaptationOptions::AdaptiveStrategy adaptiveStrategy;
  std::string stringType = convertString(matlabInput, "adaptStrategy");
  if(stringType.compare("ADAPTATION") == 0){
    adaptiveStrategy = UserInput::AdaptationOptions::ADAPTATION;
  }
  else if(stringType.compare("ADAPT_STRUCTURE") == 0){
    adaptiveStrategy = UserInput::AdaptationOptions::ADAPT_STRUCTURE;
  }
  else if(stringType.compare("STRUCTURE_DETECTION") == 0){
    adaptiveStrategy = UserInput::AdaptationOptions::STRUCTURE_DETECTION;
  }
  else if(stringType.compare("NOADAPTATION") == 0){
    adaptiveStrategy = UserInput::AdaptationOptions::NOADAPTATION;
  }
  else{
    mexPrintf("Adaptive strategy has no valid value (ADAPTATION, ADAPT_STRUCTURE, "
                 "NOADAPTATION or STRUCTURE_DETECTION\n");
    throw DyosMexException();
  }
  return adaptiveStrategy;
}

UserInput::OptimizerInput::OptimizationMode convertOptimizationMode(const mxArray *matlabInput)
{
  UserInput::OptimizerInput::OptimizationMode optMode;
  std::string stringMode = convertString(matlabInput, "optimizationMode");
  if(stringMode.compare("MINIMIZE") == 0){
    optMode = UserInput::OptimizerInput::MINIMIZE;
  }
  else if(stringMode.compare("MAXIMIZE") == 0){
    optMode = UserInput::OptimizerInput::MAXIMIZE;
  }
  else{
    mexPrintf("optimization mode has no valid value (MAXIMIZE or MINIMIZE)\n");
    throw DyosMexException();
  }
  return optMode;
}

/**
* @brief converts string in a given Matlab-struct to enum UserInput::ConstraintInput::ConstraintType
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return enum UserInput::ConstraintInput::ConstraintType
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::ConstraintInput::ConstraintType convertConstraintType (const mxArray *matlabInput,
                                                                  const int index)
{
  std::string type = convertString(matlabInput, "type", index);
  
  UserInput::ConstraintInput::ConstraintType convertedType;
  if(type == "PATH"){
    convertedType = UserInput::ConstraintInput::PATH;
  }
  else if(type == "POINT"){
    convertedType = UserInput::ConstraintInput::POINT;
  }
  else if(type == "ENDPOINT"){
    convertedType = UserInput::ConstraintInput::ENDPOINT;
  }
  else{
    mexPrintf(" %s: Constraint type not known. Must be one of PATH, POINT, ENDPOINT", type.c_str());
    throw DyosMexException();
  }
  return convertedType;
}

/**
* @brief converts fields in a given Matlab-struct to UserInput::ConstraintInput
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return UserInput::ConstraintInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::ConstraintInput convertConstraintInput(const mxArray *matlabInput, const int index)
{
  if(matlabInput == NULL){
    mexPrintf("constraint input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("constraint input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::ConstraintInput convertedInput;
  
  convertedInput.name = convertString(matlabInput, "name", index);
  
  if(mxGetFieldNumber(matlabInput, "lowerBound")>-1){
    convertedInput.lowerBound = convertScalar(matlabInput, "lowerBound", index);
  }
  if(mxGetFieldNumber(matlabInput, "upperBound")>-1){
    convertedInput.upperBound = convertScalar(matlabInput, "upperBound", index);
  }
  
  if(mxGetFieldNumber(matlabInput, "type")>-1){
    convertedInput.type = convertConstraintType(matlabInput, index);
  }
  
  if(mxGetFieldNumber(matlabInput, "timePoint")>-1){
    if(!isEmptyField(matlabInput, "timePoint",index)){
      convertedInput.timePoint = convertScalar(matlabInput, "timePoint", index);
    }
  }
  if(mxGetFieldNumber(matlabInput, "lagrangeMultiplier")>-1){
    mxArray* lgMult = mxGetField(matlabInput, index, "lagrangeMultiplier");
    if(mxGetN(lgMult)<=1){
      convertedInput.lagrangeMultiplier = convertScalar(matlabInput, "lagrangeMultiplier", index);
    }
    else{
      convertedInput.lagrangeMultiplierVector = convertDoubleVector(matlabInput, "lagrangeMultiplier", index);
    }
  }
  else{
    if(mxGetFieldNumber(matlabInput, "lagrangeMultiplierVector")>-1){
      convertedInput.lagrangeMultiplierVector = convertDoubleVector(matlabInput, "lagrangeMultiplierVector");
    }
  }
  
  return convertedInput;
}


/**
* @brief converts fields in a given Matlab-struct to UserInput::OptimizerStageInput
*
* @param matlabInput given Matlab struct
* @return UserInput::OptimizerStageInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::OptimizerStageInput convertOptimizerStageInput(const mxArray *matlabInput)
{
  if(matlabInput == NULL){
    mexPrintf("optimizer stage input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("optimizer stage input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::OptimizerStageInput convertedInput;
  checkField(matlabInput, "objective");
  mxArray *objective = mxGetField(matlabInput, 0, "objective");
  try{
    convertedInput.objective = convertConstraintInput(objective, 0);
  }
  catch(DyosMexException e){
    mexPrintf("error occurred for stage objective\n");
    throw e;
  }
  
  checkField(matlabInput, "constraints");
  mxArray *constraints = mxGetField(matlabInput, 0, "constraints");
  
  
  int numConstraints = mxGetNumberOfElements(constraints);
  convertedInput.constraints.resize(numConstraints);
  for(int i=0; i<numConstraints; i++){
    try{
      convertedInput.constraints[i] = convertConstraintInput(constraints, i);
    }
    catch(DyosMexException e){
      mexPrintf("error occured for constraint %d\n", (i+1));
      throw e;
    }
  }
  
  if(mxGetFieldNumber(matlabInput, "structureDetection")>-1){
    mxArray *structureDetection = mxGetField(matlabInput, 0, "structureDetection");
    convertedInput.structureDetection = convertStructureDetectionInput(structureDetection);
  }
  return convertedInput;
}

/**
* @brief converts string in a given Matlab-struct to enum UserInput::OptimizerInput::OptimizerType
*
* @param matlabInput given Matlab struct
* @return enum UserInput::OptimizerInput::OptimizerType
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::OptimizerInput::OptimizerType convertOptimizerType (const mxArray *matlabInput)
{
  std::string type = convertString(matlabInput, "type");
  
  UserInput::OptimizerInput::OptimizerType convertedType;
  if(type == "SNOPT"){
    convertedType = UserInput::OptimizerInput::SNOPT;
  }
  else if(type == "IPOPT"){
    convertedType = UserInput::OptimizerInput::IPOPT;
  }
  else if(type == "NPSOL"){
    convertedType = UserInput::OptimizerInput::NPSOL;
  }
  else if(type == "FILTERSQP"){
    convertedType = UserInput::OptimizerInput::FILTER_SQP;
  }
  else if(type == "SENSITIVITY_INTEGRATION"){
    convertedType = UserInput::OptimizerInput::SENSITIVITY_INTEGRATION;
  }
  else{
    mexPrintf("%s : Optimizer type not known. Must be one of SNOPT, NPSOL, IPOPT, FILTERSQP, SENSITIVITY_INTEGRATION\n", type.c_str());
    throw DyosMexException();
  }
  return convertedType;
}

/**
* @brief converts fields in a given Matlab-struct to UserInput::OptimizerInput
*
* @param matlabInput given Matlab struct
* @return enum UserInput::OptimizerInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::OptimizerInput convertOptimizerInput(const mxArray *matlabInput)
{
  if(matlabInput == NULL){
    mexPrintf("optimizer input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("optimizer input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::OptimizerInput convertedInput;
  if(mxGetFieldNumber(matlabInput, "type")>-1){
    convertedInput.type = convertOptimizerType(matlabInput);
  }
  if(mxGetFieldNumber(matlabInput, "optimizerOptions")>-1){
    convertedInput.optimizerOptions = convertMap(matlabInput, "optimizerOptions");
  }
  if(mxGetFieldNumber(matlabInput, "adaptationThreshold")>-1){
    convertedInput.adaptationOptions.adaptationThreshold = convertScalar(matlabInput, "adaptationThreshold");
  }
  if(mxGetFieldNumber(matlabInput, "adaptStrategy")>-1){
    convertedInput.adaptationOptions.adaptStrategy = convertAdaptiveStrategy(matlabInput);
  }
  if(mxGetFieldNumber(matlabInput, "globalConstraints")>-1){
    mxArray *constraints = mxGetField(matlabInput, 0, "globalConstraints");
    int numConstraints = mxGetNumberOfElements(constraints);
    convertedInput.globalConstraints.resize(numConstraints);
    for(int i=0; i<numConstraints; i++){
      convertedInput.globalConstraints[i] = convertConstraintInput(constraints, i);
    }
  }
  if(mxGetFieldNumber(matlabInput, "optimizationMode")>-1){
    convertedInput.optimizationMode = convertOptimizationMode(matlabInput);
  }
  return convertedInput;
}

UserInput::StructureDetectionInput convertStructureDetectionInput(const mxArray *matlabInput)
{
  UserInput::StructureDetectionInput convertedInput;
  convertedInput.maxStructureSteps = int(convertScalar(matlabInput, "maxStructureSteps"));
  return convertedInput;
}
