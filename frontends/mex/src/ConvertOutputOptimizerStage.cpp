#include "ConvertOutput.hpp"

void setEnumFieldConstraintOutputType(
                    mxArray *matlabOutput,
                    UserOutput::ConstraintGridOutput::ConstraintOutputType constraintOutputType,
                    char *fieldname, const int index);
void setEnumFieldConstraintType(mxArray *matlabOutput,
                                UserOutput::ConstraintOutput::ConstraintType constraintType, 
                                char *fieldname, const int index);
void setEnumFieldResultFlag(mxArray *matlabOutput,
                            UserOutput::OptimizerOutput::ResultFlag resultFlag, char *fieldname);

/**
* @brief converts given dyos-struct into mxArray
*
* @param matlabOutput Matlab output receiving converted output
* @param dyosOut given dyos-struct UserOutput::ConstraintGridOutput
* @param index index of the desired element
* @note creates fields timePoints values and lagrangeMultiplier
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertConstraintGridOutput(mxArray *matlabOutput,
                                 UserOutput::ConstraintGridOutput dyosOut, const int index)
{
  setDoubleVectorField(matlabOutput, dyosOut.timePoints, "timePoints", index);
  setDoubleVectorField(matlabOutput, dyosOut.values, "values", index);
  setDoubleVectorField(matlabOutput, dyosOut.lagrangeMultiplier, "lagrangeMultiplier", index);
  //type not yet implemented in Honeybee

}

/**
* @brief puts given enum into the field in a given Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given enum UserOutput::ConstraintGridOutput::ConstraintOutputType
* @param fieldname name of the field
* @param index index of the desired element
* @note raises an Matlab error if field could not be created
* @author Konstanze K�lle, Tjalf Hoffmann
*/void setEnumFieldConstraintOutputType(
                      mxArray *matlabOutput,
                      UserOutput::ConstraintGridOutput::ConstraintOutputType constraintOutputType,
                      char *fieldname, const int index)
{
  std::string matlabEnumString;
  switch (constraintOutputType)
  {
  case UserOutput::ConstraintGridOutput::UPPER_BOUND:
    matlabEnumString = "UPPER_BOUND";
    break;
  case UserOutput::ConstraintGridOutput::LOWER_BOUND:
    matlabEnumString = "LOWER_BOUND";
    break;
  case UserOutput::ConstraintGridOutput::INFEASIBLE:
    matlabEnumString = "INFEASIBLE";
    break;
  case UserOutput::ConstraintGridOutput::ACTIVE:
    matlabEnumString = "ACTIVE";
    break;
  default:
    std::string errMsg = "Could not create field ";
    errMsg.append(fieldname);
    mexErrMsgTxt(errMsg.c_str());
  }
  setStringField(matlabOutput, matlabEnumString, "ConstraintOutputType", index);
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::ConstraintOutput
* @param index index of the desired element
* @note ??
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertConstraintOutput(mxArray *matlabOutput, 
                             UserOutput::ConstraintOutput dyosOut, const int index)
{
  setStringField(matlabOutput, dyosOut.name, "name", index);
  setScalarField(matlabOutput, dyosOut.lowerBound, "lowerBound", index);
  setScalarField(matlabOutput, dyosOut.upperBound, "upperBound", index);
  setEnumFieldConstraintType(matlabOutput, dyosOut.type, "type", index);

  checkOutputField(matlabOutput, "grids");
  int numGrids = dyosOut.grids.size();
  const char *fieldNames[] = {"timePoints", "values", "lagrangeMultiplier"};
  mwSize nDims = 2, dims[] = {1, numGrids};
  mxArray* grids = mxCreateStructArray(nDims, dims, 3, fieldNames);
  for(int i=0; i<numGrids; i++){
    convertConstraintGridOutput(grids, dyosOut.grids[i], i);
  }
  mxSetField(matlabOutput, index, "grids", grids);
}

/**
* @brief puts given enum into the field in a given Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given enum UserOutput::ConstraintOutput::ConstraintType
* @param fieldname name of the field
* @param index index of the desired element
* @note raises an Matlab error if field could not be created
* @author Konstanze K�lle, Tjalf Hoffmann
*/void setEnumFieldConstraintType(mxArray *matlabOutput,
                                UserOutput::ConstraintOutput::ConstraintType constraintType,
                                char *fieldname, const int index)
{
  std::string matlabEnumString;
  switch (constraintType)
  {
  case UserOutput::ConstraintOutput::PATH:
    matlabEnumString = "PATH";
    break;
  case UserOutput::ConstraintOutput::POINT:
    matlabEnumString = "POINT";
    break;
  case UserOutput::ConstraintOutput::ENDPOINT:
    matlabEnumString = "ENDPOINT";
    break;
  default:
    std::string errMsg = "Could not create field ";
    errMsg.append(fieldname);
    mexErrMsgTxt(errMsg.c_str());
  }
  setStringField(matlabOutput, matlabEnumString, "ConstraintType", index);
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::OptimizerStageOutput
* @param index index of the desired element
* @author Konstanze K�lle, Tjalf Hoffmann
*/void convertOptimizerStageOutput(mxArray *matlabOutput,
                                 UserOutput::OptimizerStageOutput dyosOut, const int index)
{
  mxArray *objective = mxGetField(matlabOutput, index, "objective");
  if(objective == NULL){
    mexErrMsgTxt("failed to read out field objective in OptimizerStageOutput struct");
  }
  convertConstraintOutput(objective, dyosOut.objective, 0);
  mxSetField(matlabOutput, index, "objective", objective);
  
  mxArray *constraints = mxGetField(matlabOutput, index, "constraints");
  if(constraints == NULL){
    mexErrMsgTxt("failed to read out field constraints in OptimizerStageOutput struct");
  }
  int numConstraints = mxGetNumberOfElements(constraints);
  if(numConstraints != dyosOut.nonlinearConstraints.size()){
    mexErrMsgTxt("Size of input and output constraints do not match.\n"
                 "Probably some error occured so that no output was produced\n"
                 "look into mexDyosOutput.json for further information");
  }
  for(int i=0; i<numConstraints; i++){
    convertConstraintOutput(constraints, dyosOut.nonlinearConstraints[i], i);
  }
  mxSetField(matlabOutput, index, "constraints", constraints);
}

mxArray *convertSecondOrderOutput(UserOutput::SecondOrderOutput dyosOut)
{
  const char *fieldNames[] = {"values", "compositeAdjoints"};
  mwSize dims[1] = {1};
  mxArray* convertedOutput = mxCreateStructArray(1, dims, 2, fieldNames);

  setDoubleMatrixField(convertedOutput, dyosOut.values, "values");
  setDoubleMatrixField(convertedOutput, dyosOut.constParamHessian, "constParamHessian");
  setDoubleMatrixField(convertedOutput, dyosOut.compositeAdjoints, "compositeAdjoints");

  return convertedOutput;
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::OptimizerOutput
* @param index index of the desired element
* @note ??
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertOptimizerOutput(mxArray *matlabOutput, UserOutput::OptimizerOutput dyosOut)
{
  checkOutputField(matlabOutput, "secondOrder");
  mxArray* secondOrderOut = convertSecondOrderOutput(dyosOut.secondOrder);
  mxSetField(matlabOutput, 0, "secondOrder", secondOrderOut);

  setEnumFieldResultFlag(matlabOutput, dyosOut.resultFlag, "resultFlag");
}

/**
* @brief puts given enum into the field in a given Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given enum UserOutput::OptimizerOutput::ResultFlag
* @param fieldname name of the field
* @param index index of the desired element
* @note raises an Matlab error if field could not be created
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void setEnumFieldResultFlag(mxArray *matlabOutput,
                            UserOutput::OptimizerOutput::ResultFlag resultFlag, char *fieldname)
{
  std::string matlabEnumString;
  switch (resultFlag)
  {
  case UserOutput::OptimizerOutput::OK:
    matlabEnumString = "OK";
    break;
  case UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS:
    matlabEnumString = "TOO_MANY_ITERATIONS";
    break;
  case UserOutput::OptimizerOutput::INFEASIBLE:
    matlabEnumString = "INFEASIBLE";
    break;
  case UserOutput::OptimizerOutput::WRONG_GRADIENTS:
    matlabEnumString = "WRONG_GRADIENTS";
    break;
  case UserOutput::OptimizerOutput::NOT_OPTIMAL:
    matlabEnumString = "NOT_OPTIMAL";
    break;
  case UserOutput::OptimizerOutput::FAILED:
    matlabEnumString = "FAILED";
    break;
  default:
    std::string errMsg = "Could not create field ";
    errMsg.append(fieldname);
    mexErrMsgTxt(errMsg.c_str());
  }
  setStringField(matlabOutput, matlabEnumString, "resultFlag");
}