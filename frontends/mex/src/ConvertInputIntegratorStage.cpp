#include "ConvertInput.hpp"

UserInput::ParameterGridInput::ApproximationType convertApproximationType(const mxArray *matlabInput, const int index);
UserInput::ParameterInput::ParameterType convertParameterType (const mxArray *matlabInput, const int index);
UserInput::ParameterInput::ParameterSensitivityType convertParameterSensType (const mxArray *matlabInput, const int index);
UserInput::IntegratorInput::IntegratorType convertIntegratorType (const mxArray *matlabInput);
UserInput::IntegratorInput::IntegrationOrder convertIntegrationOrder(const mxArray *matlabInput);
UserInput::DaeInitializationInput::DaeInitializationType convertDaeType(const mxArray *matlabInput);


/**
* @brief converts fields in a given Matlab-struct to UserInput::EsoInput
*
* @param matlabInput given Matlab struct
* @return UserInput::EsoInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::EsoInput convertEsoInput(const mxArray *matlabInput)
{
  UserInput::EsoInput esoInput;
  
  if(matlabInput == NULL){
    mexPrintf("eso input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("eso input must be a structure.\n");
    throw DyosMexException();
  }

  esoInput.model = convertString(matlabInput, "model");



  /*if(mxGetFieldNumber(matlabInput, "process")>-1){
    esoInput.process = convertString(matlabInput, "process");
  }*/
  if(mxGetFieldNumber(matlabInput, "initialModel")>-1){
    esoInput.initialModel = convertString(matlabInput, "initialModel");
  }

  checkField(matlabInput, "type");
  std::string typeString = convertString(matlabInput, "type");

  if(typeString.compare("JADE") == 0){
    esoInput.type = UserInput::EsoInput::JADE;
  }    
#ifdef BUILD_WITH_FMU
  else if (typeString.compare("FMI") == 0) {
	  esoInput.type = UserInput::EsoInput::FMI;
  }
#endif
  else{
    mexPrintf("%s is no valid eso type.\n", typeString);
    throw DyosMexException();
  }
  return esoInput;
}

/**
* @brief converts fields in a given Matlab-struct to UserInput::ParameterGridInput
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return UserInput::ParameterGridInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::ParameterGridInput convertParameterGridInput(const mxArray *matlabInput, const int index)
{
  if(matlabInput == NULL){
    mexPrintf("parameter grid input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("parameter grid input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::ParameterGridInput parameterGridInput;

  try{
    parameterGridInput.type = convertApproximationType(matlabInput, index);

    if(mxGetFieldNumber(matlabInput, "numIntervals")>-1){
      parameterGridInput.numIntervals = unsigned(convertScalar(matlabInput, "numIntervals", index));
    }
    if(mxGetFieldNumber(matlabInput, "duration")>-1){
      parameterGridInput.duration = convertScalar(matlabInput, "duration", index);
    }
    if(mxGetFieldNumber(matlabInput, "hasFreeDuration")>-1){
      parameterGridInput.hasFreeDuration = convertBool(matlabInput, "hasFreeDuration", index);
    }
    if(mxGetFieldNumber(matlabInput, "timePoints")>-1){
      parameterGridInput.timePoints = convertDoubleVector(matlabInput, "timePoints", index);
    }
    if(mxGetFieldNumber(matlabInput, "values")>-1){
      parameterGridInput.values = convertDoubleVector(matlabInput, "values", index);
    }
    if(mxGetFieldNumber(matlabInput, "pcresolution")>-1){
      parameterGridInput.pcresolution = int(convertScalar(matlabInput, "pcresolution", index));
    }
    if(mxGetFieldNumber(matlabInput, "adapt")>-1){
      mxArray *adapt = mxGetField(matlabInput, index, "adapt");
      parameterGridInput.adapt = convertAdaptationInput(adapt);
    }
  }
  catch(DyosMexException e){
    mexPrintf("Error occurred for grid %d\n", (index+1));
    throw e;
  }

  return parameterGridInput;
}

/**
* @brief converts string in a given Matlab-struct to enum UserInput::ParameterGridInput::ApproximationType
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return enum UserInput::ParameterGridInput
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::ParameterGridInput::ApproximationType convertApproximationType(const mxArray *matlabInput, const int index)
{
  UserInput::ParameterGridInput::ApproximationType approximationType;

  std::string	typeString = convertString(matlabInput, "type", index);
  if(typeString.compare("PIECEWISE_CONSTANT") == 0){
    approximationType = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
  }
  else if(typeString.compare("PIECEWISE_LINEAR") == 0){  
    approximationType = UserInput::ParameterGridInput::PIECEWISE_LINEAR;	
  }
  else{
    mexPrintf("type has no desired value.\n");
    throw DyosMexException();
  }
  return approximationType;
}

/**
* @brief converts fields in a given Matlab-struct to UserInput::ParameterInput
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return enum UserInput::ParameterInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::ParameterInput convertParameterInput(const mxArray *matlabInput, const int index)
{
  if(matlabInput == NULL){
    mexPrintf("parameter input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("parameter input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::ParameterInput parameterInput;

  if (mxGetFieldNumber(matlabInput, "name")>-1){
    parameterInput.name = convertString(matlabInput, "name", index);
  }

  if(!isEmptyField(matlabInput, "lowerBound", index)){
    parameterInput.lowerBound = convertScalar(matlabInput, "lowerBound", index);
  }
  if(!isEmptyField(matlabInput, "upperBound", index)){
    parameterInput.upperBound = convertScalar(matlabInput, "upperBound", index);
  }
   
  //value
  if (mxGetFieldNumber(matlabInput, "value")>-1 && !isEmptyField(matlabInput, "value", index)){
    parameterInput.value = convertScalar(matlabInput, "value", index);
  }

  // grids
  if (mxGetFieldNumber(matlabInput, "grids")>-1){
	  mxArray *grids = mxGetField(matlabInput, index, "grids");
    if(grids != NULL){
      int numGrids = mxGetNumberOfElements(grids);
      parameterInput.grids.resize(numGrids);
      for(int i=0; i<numGrids; i++){
        parameterInput.grids[i] = convertParameterGridInput(grids, i);
      }
    }
  }
  parameterInput.sensType = convertParameterSensType(matlabInput, index);
  parameterInput.paramType = convertParameterType(matlabInput, index);

  return parameterInput;
}

/**
* @brief converts string in a given Matlab-struct to enum UserInput::ParameterInput::ParameterType
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return enum UserInput::ParameterInput::ParameterType
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::ParameterInput::ParameterType convertParameterType(const mxArray *matlabInput, const int index)
{
  UserInput::ParameterInput::ParameterType parameterType;
  std::string	typeString = convertString(matlabInput, "paramType", index);
  if(typeString.compare("INITIAL") == 0){
    parameterType = UserInput::ParameterInput::INITIAL;
  }    
    else if(typeString.compare("TIME_INVARIANT") == 0){  
    parameterType = UserInput::ParameterInput::TIME_INVARIANT;	
  }
  else if(typeString.compare("PROFILE") == 0){  
    parameterType = UserInput::ParameterInput::PROFILE;	
  }
  else if(typeString.compare("DURATION") == 0){  
    parameterType = UserInput::ParameterInput::DURATION;	
  }
  else{
    mexPrintf("paramType has no desired value.\n");
    throw DyosMexException();
  }
  return parameterType;
}


/**
* @brief converts string in a given Matlab-struct to enum UserInput::ParameterInput::ParameterSensitivityType
*
* @param matlabInput given Matlab struct
* @param index index of the desired element
* @return enum UserInput::ParameterInput::ParameterSensitivityType
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::ParameterInput::ParameterSensitivityType convertParameterSensType (const mxArray *matlabInput, const int index)
{
  UserInput::ParameterInput::ParameterSensitivityType parameterSensType;
    std::string	typeString = convertString(matlabInput, "sensType", index);
  if(typeString.compare("FULL") == 0){
    parameterSensType = UserInput::ParameterInput::FULL;
  }    
    else if(typeString.compare("FRACTIONAL") == 0){  
    parameterSensType = UserInput::ParameterInput::FRACTIONAL;	
  }
  else if(typeString.compare("NO") == 0){  
    parameterSensType = UserInput::ParameterInput::NO;	
  }
  else{
    mexPrintf("sensType has no desired value.\n");
    throw DyosMexException();
  }
  return parameterSensType;
  }

/**
* @brief converts fields in a given Matlab-struct to UserInput::IntegratorStageInput
*
* @param matlabInput given Matlab struct
* @return UserInput::IntegratorStageInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::IntegratorStageInput convertIntegratorStageInput(const mxArray *matlabInput)
{
  if(matlabInput == NULL){
    mexPrintf("integrator stage input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("integrator stage input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::IntegratorStageInput integratorStageInput;
  if(mxGetFieldNumber(matlabInput, "plotGridResolution")>-1){
    integratorStageInput.plotGridResolution = unsigned(convertScalar(matlabInput, "plotGridResolution"));
  }
  if(mxGetFieldNumber(matlabInput, "explicitPlotGrid")>-1){
    integratorStageInput.explicitPlotGrid = convertDoubleVector(matlabInput, "explicitPlotGrid");
  }
  
  checkField(matlabInput, "duration");
  mxArray *duration = mxGetField(matlabInput, 0, "duration");
  try{
    integratorStageInput.duration = convertParameterInput(duration, 0);
  }
  catch(DyosMexException e){
    mexPrintf("error occured for stage duration\n");
    throw e;
  }

  // parameters
  checkField(matlabInput, "parameters");
    mxArray *parameters = mxGetField(matlabInput, 0, "parameters");
  int numParameters = mxGetNumberOfElements(parameters);
    integratorStageInput.parameters.resize(numParameters);
  for(int i=0; i<numParameters; i++){
    try{
      integratorStageInput.parameters[i] = convertParameterInput(parameters, i);
    }
    catch(DyosMexException e){
      mexPrintf("error occurred for parameter %d\n", (i+1));
      throw e;
    }
  }
  return integratorStageInput;
}

/**
* @brief converts fields in a given Matlab-struct to UserInput::IntegratorInput
*
* @param matlabInput given Matlab struct
* @return UserInput::IntegratorInput
* @note raises an Matlab error if matlabInput is no structure
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::IntegratorInput convertIntegratorInput(const mxArray *matlabInput)
{
  if(matlabInput == NULL){
    mexPrintf("integrator input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("integrator input must be a structure.\n");
    throw DyosMexException();
  }
  UserInput::IntegratorInput integratorInput;
  
  if(mxGetFieldNumber(matlabInput, "type")>-1){
    integratorInput.type = convertIntegratorType(matlabInput);
  }
  if(mxGetFieldNumber(matlabInput, "order")>-1){
    integratorInput.order = convertIntegrationOrder(matlabInput);
  }
  if(mxGetFieldNumber(matlabInput, "integratorOptions")>-1){
    integratorInput.integratorOptions = convertMap(matlabInput, "integratorOptions");
  }
  if(mxGetFieldNumber(matlabInput, "daeInit")>-1){
    mxArray* daeInit = mxGetField(matlabInput, 0, "daeInit");
    integratorInput.daeInit = convertDaeInitializationInput(daeInit);
  }
  return integratorInput;
}

/**
* @brief converts string in a given Matlab-struct to enum UserInput::IntegratorInput::IntegratorType
*
* @param matlabInput given Matlab struct
* @return enum UserInput::IntegratorInput::IntegratorType
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::IntegratorInput::IntegratorType convertIntegratorType (const mxArray *matlabInput)
{
  UserInput::IntegratorInput::IntegratorType integratorType;
  std::string typeString = convertString(matlabInput, "type");
  if(typeString.compare("NIXE") == 0){
    integratorType = UserInput::IntegratorInput::NIXE;
  }    
  else if(typeString.compare("LIMEX") == 0){  
    integratorType = UserInput::IntegratorInput::LIMEX;
  }
  else if(typeString.compare("IDAS") == 0){  
    integratorType = UserInput::IntegratorInput::IDAS;
  }
  else{
    mexPrintf("integratorType has no valud value (NIXE, LIMEX or IDAS)\n");
    throw DyosMexException();
  }
  return integratorType;
}


/**
* @brief converts string in a given Matlab-struct to enum UserInput::IntegratorInput::IntegrationOrder
*
* @param matlabInput given Matlab struct
* @return enum UserInput::IntegratorInput::IntegrationOrder
* @note raises an Matlab error if matlabInput has no desired value
* @author Konstanze K�lle, Tjalf Hoffmann
*/
UserInput::IntegratorInput::IntegrationOrder convertIntegrationOrder(const mxArray *matlabInput)
{
  UserInput::IntegratorInput::IntegrationOrder integrationOrder;
    std::string typeString = convertString(matlabInput, "order");
  if(typeString.compare("ZEROTH") == 0){
    integrationOrder = UserInput::IntegratorInput::ZEROTH;
  }    
    else if(typeString.compare("FIRST_FORWARD") == 0){  
    integrationOrder = UserInput::IntegratorInput::FIRST_FORWARD;	
  }
  else if(typeString.compare("FIRST_REVERSE") == 0){  
    integrationOrder = UserInput::IntegratorInput::FIRST_REVERSE;	
  }
  else if(typeString.compare("SECOND_REVERSE") == 0){  
    integrationOrder = UserInput::IntegratorInput::SECOND_REVERSE;	
  }
  else{
    mexPrintf("integrationOrder has no valid value (ZEROTH, FIRST_FORWARD," 
                                            " FIRST_REVERSE or SECOND_REVERSE\n");
    throw DyosMexException();
  }
  return integrationOrder;
}

UserInput::DaeInitializationInput::DaeInitializationType convertDaeType(const mxArray *matlabInput)
{
  UserInput::DaeInitializationInput::DaeInitializationType daeType;
  std::string typeString = convertString(matlabInput, "type");
  if(typeString.compare("BLOCK") == 0){
    daeType = UserInput::DaeInitializationInput::BLOCK;
  }
  else if(typeString.compare("FULL") == 0){
    daeType = UserInput::DaeInitializationInput::FULL;
  }
  else if(typeString.compare("NO") == 0){
    daeType = UserInput::DaeInitializationInput::NO;
  }
#ifdef BUILD_WITH_FMU
  else if (typeString.compare("FMI") == 0) {
	  daeType = UserInput::DaeInitializationInput::FMI;
  }
#endif
  else{
#ifdef BUILD_WITH_FMU
    mexPrintf("DaeInitialization type has no valid value (BLOCK, FULL, NO, or FMI)\n");
#else
	  mexPrintf("DaeInitialization type has no valid value (BLOCK, FULL, or NO)\n");
#endif
    throw DyosMexException();
  }
  return daeType;
}

UserInput::DaeInitializationInput convertDaeInitializationInput(const mxArray *matlabInput)
{
  UserInput::DaeInitializationInput convertedInput;
  if(mxGetFieldNumber(matlabInput, "maximumErrorTolerance")>-1){
    convertedInput.maximumErrorTolerance = convertScalar(matlabInput, "maximumErrorTolerance");
  }
  if(mxGetFieldNumber(matlabInput, "linSolver")>-1){
    mxArray *linSolverInput = mxGetField(matlabInput, 0, "linSolver");
    convertedInput.linSolver = convertLinearSolverInput(linSolverInput);
  }
  if(mxGetFieldNumber(matlabInput, "nonLinSolver")>-1){
    mxArray *nonLinSolverInput = mxGetField(matlabInput, 0, "nonLinSolver");
    convertedInput.nonLinSolver = convertNonLinearSolverInput(nonLinSolverInput);
  }
  if(mxGetFieldNumber(matlabInput, "type")>-1){
    convertedInput.type = convertDaeType(matlabInput);
  }
  return convertedInput;
}

UserInput::LinearSolverInput convertLinearSolverInput(const mxArray *matlabInput)
{
  UserInput::LinearSolverInput convertedInput;
  std::string typeString = convertString(matlabInput, "type");
  if(typeString.compare("MA28")==0){
    convertedInput.type = UserInput::LinearSolverInput::MA28;
  }
  else{
    mexPrintf("Linear Solver type has no valid value (MA28)\n");
    throw DyosMexException();
  }
  return convertedInput;
}

UserInput::NonLinearSolverInput convertNonLinearSolverInput(const mxArray *matlabInput)
{
  UserInput::NonLinearSolverInput convertedInput;
  if(mxGetFieldNumber(matlabInput, "tolerance")>-1){
    convertedInput.tolerance = convertScalar(matlabInput, "tolerance");
  }
  if(mxGetFieldNumber(matlabInput, "type")>-1){
    std::string typeString = convertString(matlabInput, "type");
    if(typeString.compare("NLEQ1S") == 0){
      convertedInput.type = UserInput::NonLinearSolverInput::NLEQ1S;
    }
	else if(typeString.compare("CMINPACK")==0){
		convertedInput.type = UserInput::NonLinearSolverInput::CMINPACK;
	}
    else{
      mexPrintf("Nonlinear solver type has no valid value(NLEQ1S, CMINPACK)\n");
    }
  }
  return convertedInput;
}

UserInput::AdaptationInput convertAdaptationInput(const mxArray *matlabInput)
{
  UserInput::AdaptationInput convertedInput;
  if(mxGetFieldNumber(matlabInput, "adaptType")>-1){
    std::string adaptTypeString = convertString(matlabInput, "adaptType");
    if(adaptTypeString.compare("WAVELET") == 0){
      convertedInput.adaptType = UserInput::AdaptationInput::WAVELET;
    }
    else{
      mexPrintf("Adaptation type has no valid value (WAVELET).\n");
    }
  }
  
  if(mxGetFieldNumber(matlabInput, "maxAdaptSteps")>-1){
    convertedInput.maxAdaptSteps = int(convertScalar(matlabInput, "maxAdaptSteps"));
  }
  
  if(mxGetFieldNumber(matlabInput, "adaptWave")>-1){
    mxArray *adaptWave = mxGetField(matlabInput, 0, "adaptWave");
    convertedInput.adaptWave = convertWaveletAdaptationInput(adaptWave);
  }
  return convertedInput;
}

UserInput::WaveletAdaptationInput convertWaveletAdaptationInput(const mxArray *matlabInput)
{
  UserInput::WaveletAdaptationInput convertedInput;
  if(mxGetFieldNumber(matlabInput, "epsilon")>-1){
    convertedInput.epsilon = convertScalar(matlabInput, "epsilon");
  }
  if(mxGetFieldNumber(matlabInput, "etres")>-1){
    convertedInput.etres = convertScalar(matlabInput, "etres");
  }
  if(mxGetFieldNumber(matlabInput, "horRefinementDepth")>-1){
   convertedInput.horRefinementDepth = int(convertScalar(matlabInput, "horRefinementDepth"));
  }
  if(mxGetFieldNumber(matlabInput, "verRefinementDepth")>-1){
   convertedInput.verRefinementDepth = int(convertScalar(matlabInput, "verRefinementDepth"));
  }
  if(mxGetFieldNumber(matlabInput, "maxRefinementLevel")>-1){
   convertedInput.maxRefinementLevel = int(convertScalar(matlabInput, "maxRefinementLevel"));
  }
  if(mxGetFieldNumber(matlabInput, "minRefinementLevel")>-1){
   convertedInput.minRefinementLevel = int(convertScalar(matlabInput, "minRefinementLevel"));
  }
  return convertedInput;
}