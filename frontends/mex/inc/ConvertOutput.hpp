#pragma once

#include "mex.h"
#include "UserOutput.hpp"
#include "UtilityFunctions.hpp"

#include <algorithm>

//integrator stage convert output functions
mxArray *convertStateGridOutput(UserOutput::StateGridOutput dyosOut);
void convertStateOutput(mxArray *matlabOutput, UserOutput::StateOutput dyosOut, const int index);
void convertFirstSensitivityOutput(mxArray *convertedOutput,
                                   UserOutput::FirstSensitivityOutput dyosOut, const int index);
void convertParameterGridOutput(mxArray* matlabOutput,
                                UserOutput::ParameterGridOutput dyosOut, const int index);
void convertParameterOutput(mxArray *matlabOutput,
                            UserOutput::ParameterOutput dyosOut, const int index);
void convertIntegratorStageOutput(mxArray *matlabOutput, 
                                  UserOutput::IntegratorStageOutput dyosOut, const int index);

//optimizer stage convert output functions
void convertConstraintGridOutput(mxArray *matlabOutput, 
                                 UserOutput::ConstraintGridOutput dyosOut, const int index);
void convertConstraintOutput(mxArray *matlabOutput, 
                             UserOutput::ConstraintOutput dyosOut, const int index);
void convertOptimizerStageOutput(mxArray *matlabOutput,
                                 UserOutput::OptimizerStageOutput dyosOut, const int index);
mxArray *convertSecondOrderOutput(UserOutput::SecondOrderOutput dyosOut);
void convertOptimizerOutput(mxArray *matlabOutput,
                            UserOutput::OptimizerOutput dyosOut);

//general convert output functions
void convertStageOutput(mxArray *matlabOutput, UserOutput::StageOutput dyosOut, const int index);
void convertOutput(mxArray *matlabOutput, UserOutput::Output dyosOut, int index);

//utility functions
void checkOutputField(mxArray *matlabOutput, char *fieldname);

void setStringField(mxArray *matlabOutput, std::string stringOutput, char *fieldname, const int index = 0);

void setScalarField(mxArray *matlabOutput, double scalar, char *fieldname, const int index = 0);

void setDoubleVectorField(mxArray *matlabOutput, std::vector<double> vec,
                             char *fieldname, const int index = 0);
void setBoolField(mxArray *matlabOutput, bool flag, char *fieldname, const int index = 0);

void setDoubleMatrixField(mxArray *matlabOutput, std::vector<std::vector<double> > vecMat,
                             char *fieldname, const int index = 0);

void resizeStructField(mxArray *matlabOutput, char *fieldname, const int newSize, const int index = 0);


void setMap(mxArray *matlabOutput, std::map<int,int> &mapOutput, char *fieldname, const int index = 0);
void setMap(mxArray *matlabOutput, std::map<std::string,int> &mapOutput, char *fieldname, const int index = 0);
