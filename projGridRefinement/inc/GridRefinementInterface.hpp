/**  @file GridRefinementInterface.hpp
*    @brief Header for grid refinement interface which is used by the signal analysis
*    and eventually the senesitivity based refinement.
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 19.06.2012
*/



#pragma once
#include <vector>
#include "DyosObject.hpp"
#include <boost/shared_ptr.hpp>
#include <string>


/** @class GridRefinementInterface
* @brief abstract class used for the grid refinement. 
*/
class GridRefinementInterface : public DyosObject
{
public:
  enum type{
    WAVELET,
    SWITCHINGFUNCTION
  };

  typedef boost::shared_ptr<GridRefinementInterface> Ptr;
  /** 
   * @brief obtains the new mesh by doing e.g. a signal analysis or a sensitivity analysis.
   * @param[in] grid is the grid including the time points. It has to run between 0 and 1.
   * @param[in] values are the corresponding values at these time points
   */
  //! @todo Uebergabe eines DyOS Outputs statt grid and values. Dann kann man beliebige refinement strategien machen.
  virtual void obtainNewMesh(const std::vector<double> &grid,
                             const std::vector<double> &values)=0;

  /** @brief returns the type of adaptation
    * @return WAVELET or SWITCHINGFUNCTION
    */
  virtual GridRefinementInterface::type getType()const=0;

  /** @brief adds additional data required for adaptation. Not used in wavelet based adaptation */
  virtual void addData(std::map<double, double> &data)=0;

  /**
  * @brief gets the refined grid
  * @return refined grid
  */
  virtual std::vector<double> getRefinedGrid()const=0;
  /**
  * @brief gets the refined grid values
  * @return refined grid values
  */
  virtual std::vector<double> getRefinedValues()const=0;

  virtual void writeOutput(std::string fileName)const=0;
};
