/**  @file GridRefinementInterface.hpp
*    @brief Wrapping of the Grid refinement interface using a signal based analysis.
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 19.06.2012
*/



#pragma once
#include <vector>
#include "GridRefinementInterface.hpp"
#include "GridRefinementInput.hpp"


/** @class GridRefinementWaveletBased 
  * @brief wavelet based grid refinement using the signal analysis
  */
class GridRefinementSWBased:public GridRefinementInterface
{
  enum approxType{
    PWC,
    PWL
  };
  /// include tolerance
  double m_includeTol;
  /// smallest possible resolution 1/pow(2,m_maxResolution)
  int m_maxResolution;

  double m_uBound;
  double m_lBound;

  /// approximation type of control
  approxType m_type;

  /// grid points
  std::vector<double> m_grid;
  /// values
  std::vector<double> m_values;
  /// switching funciton used to adapt the grid
  std::map<double, double> m_swFunction;

  //! @brief empty constructor
  GridRefinementSWBased(){};
public:
  GridRefinementSWBased(FactoryInput::SWAdaptationInput options);

  virtual void obtainNewMesh(const std::vector<double> &grid,
                             const std::vector<double> &values);
  virtual std::vector<double> getRefinedGrid() const;
  virtual std::vector<double> getRefinedValues() const;
  virtual GridRefinementInterface::type getType()const;
  virtual void addData(std::map<double, double> &data);
  virtual void writeOutput(std::string fileName)const{};
};
