/**
* @file GridRefinementFactory.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GridRefinement                                                       \n
* =====================================================================\n
* =====================================================================\n
* @author Fady Assassa
* @date 18.09.2012
*/

#pragma once
#include "GridRefinementFactory.hpp"
#include "GridRefinementWaveletBased.hpp"
#include "GridRefinementSWBased.hpp"


GridRefinementInterface::Ptr GridRefinementFactory::createRefinementInterface
  (const FactoryInput::AdaptationInput &input)
{
  GridRefinementInterface::Ptr out;
  switch(input.adaptType)
  {
  case FactoryInput::AdaptationInput::WaveletBasedRefinement:
    {
      GridRefinementWaveletBased::Ptr waveRefine(new GridRefinementWaveletBased(input.adaptWave));
      out = waveRefine;
      break;
    }
  case FactoryInput::AdaptationInput::SwitchingFunction:
    {
      GridRefinementSWBased::Ptr swRefine(new GridRefinementSWBased(input.swAdapt));
      out = swRefine;
      break;
    }
  default:
    assert(false);
    break;
  }
  out->setLogger(m_logger);
  return out;
}
