/**  @file GridRefinementWaveletBased.cpp
*    @brief Wrapping of the Grid refinement interface using a signal based analysis.
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 19.06.2012
*/



#include "GridRefinementSWBased.hpp"
#include <set>
#include <math.h>
#include "UtilityFunctions.hpp"

using namespace std;
using namespace utils;

/** @copydoc GridRefinementInterface::getRefinedGrid */
std::vector<double> GridRefinementSWBased::getRefinedGrid() const
{
  return m_grid;
}

/** @copydoc GridRefinementInterface::getRefinedValues */
std::vector<double> GridRefinementSWBased::getRefinedValues() const
{
  return m_values;
}
/** @copydoc GridRefinementInterface::obtainNewMesh */
void GridRefinementSWBased::obtainNewMesh(const std::vector<double> &grid,
                                          const std::vector<double> &values)
{
  m_grid = grid;
  m_values = values;

  // copies
  std::set<double> tpoints(m_grid.begin(), m_grid.end());
  vector<double> vals = m_values;
  vals.reserve(1024);

  // iterators for refinement
  vector<double>::iterator it(vals.begin()), itPrev(vals.begin()), itNext(vals.begin());
  std::advance(itNext,1);

  unsigned removeEnd = 0;
  if(m_type == PWL){
    removeEnd = 1;
  }

  // determine new mesh
  for(unsigned k=0; k < m_values.size()-removeEnd; k++){
    const double sw = findMapKey(m_swFunction, (m_grid[k+1] + m_grid[k])/2);
    const double minInterval = double(1)/double( 1 << m_maxResolution ); // bit shift to the left
    const double intervalSize = (m_grid[k+1] - m_grid[k]);
    if(compDbl(m_values[k], m_uBound) || compDbl(m_values[k], m_lBound)){
      if (itNext == vals.end()){
        // do nothing
        break;
      }else if( !compDbl(*itNext,*it) && fabs(sw)>m_includeTol && intervalSize>minInterval){
        if(m_type == PWC && it != vals.begin() && compDbl(*it,*itPrev)){
          // remove point in case of pwc, we don't need it to describe the upper bound as in
          // the linear case
          itNext = vals.erase(it);
          tpoints.erase(m_grid[k]);
          it = itNext;
          std::advance(it,-1);
          itPrev = it;
          if(it != vals.begin())
            std::advance(itPrev, -1);
        }
        // include point next between "it" and "itNext"
        tpoints.insert( m_grid[k] + intervalSize/double(2));
        itPrev = vals.insert(it, m_values[k]);
        std::advance(itPrev,1);
        it = itPrev;
        std::advance(it,1);
        itNext = it;
        if(it != vals.end())
          std::advance(itNext,1);
        if(itNext == vals.end())
          itNext = it;

      }else if(compDbl(*itNext,*it)){
        if(k>0){
          if(compDbl(*it,*itPrev)){
            // remove point at "it"
            it = vals.erase(it);
            tpoints.erase(m_grid[k]);
            itNext = it;
            itPrev = it;
            if(it != vals.end())
              std::advance(itNext,1);
            if(itNext == vals.end())
              itNext = it;

            if(it != vals.begin())
              std::advance(itPrev,-1);
          }else{
            // do nothing, just move iterators
            itPrev = it;
            std::advance(it,1);
            if(it != vals.end())
              std::advance(itNext,1);
            if(itNext == vals.end())
              itNext = it;
          }
        }else{
          // do nothing, just move iterators
          itPrev = it;
          std::advance(it,1);
          if(it != vals.end())
            std::advance(itNext,1);
          if(itNext == vals.end())
            itNext = it;
        }
      }else{
        // do nothing, just move iterators
        itPrev = it;
        std::advance(it,1);
        if(it != vals.end())
          std::advance(itNext,1);
        if(itNext == vals.end())
          itNext = it;
      }
    }else if(m_values[k] < m_uBound && m_values[k] > m_lBound){
      if(fabs(sw) > m_includeTol && intervalSize>minInterval){
        // ad point right of "it"
        tpoints.insert( m_grid[k] + intervalSize/double(2));
        double val = 0;
        if( (k+1) >= m_values.size())
          val = (m_values[k]);
        else
          val = (m_values[k+1] - m_values[k])/2.0;

        itPrev = vals.insert(it, m_values[k]);
        std::advance(itPrev,1);
        it = itPrev;
        *it +=  val;
        std::advance(it,1);
        itNext = it;
        if(it != vals.end())
          std::advance(itNext,1);
        if(itNext == vals.end())
          itNext = it;
      }else{
        // do nothing, just move iterators
        itPrev = it;
        std::advance(it,1);
        if(it != vals.end())
          std::advance(itNext,1);
        if(itNext == vals.end())
          itNext = it;
      }
    }else{ // this case should not exist
      assert(false);
    }
  }
  // override m_grid and m_values with the new grid
  m_grid.assign(tpoints.begin(), tpoints.end());
  m_values = vals;
}

void GridRefinementSWBased::addData(std::map<double, double> &data){
  m_swFunction = data;
}
/** @brief constructor that creates an object of type SignalAnalysis
*   @param[in] opt includes all the values required to create a signal analysis object
*/
GridRefinementSWBased::GridRefinementSWBased(FactoryInput::SWAdaptationInput opt)
{
  if(opt.type == 1)
    m_type = PWC;
  else
    m_type = PWL;

  m_includeTol = opt.includeTol;
  m_maxResolution = opt.maxRefinementLevel;
  m_uBound = opt.uBound;
  m_lBound = opt.lBound;
}



GridRefinementInterface::type GridRefinementSWBased::getType()const
{
  return GridRefinementInterface::SWITCHINGFUNCTION;
}

