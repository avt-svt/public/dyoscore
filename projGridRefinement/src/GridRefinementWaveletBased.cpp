/**  @file GridRefinementWaveletBased.cpp
*    @brief Wrapping of the Grid refinement interface using a signal based analysis.
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 19.06.2012
*/



#include "GridRefinementWaveletBased.hpp"


/** @copydoc GridRefinementInterface::getRefinedGrid */
std::vector<double> GridRefinementWaveletBased::getRefinedGrid() const
{
  return m_signal.getRefinedGrid();
}

/** @copydoc GridRefinementInterface::getRefinedValues */
std::vector<double> GridRefinementWaveletBased::getRefinedValues() const
{
  std::vector<double> out =  m_signal.getRefinedValues();
  if (m_signal.getType() == 1){
    out.pop_back();
  }
  return out;
}
/** @copydoc GridRefinementInterface::obtainNewMesh */
void GridRefinementWaveletBased::obtainNewMesh(const std::vector<double> &grid,
                                              const std::vector<double> &values)
{
  m_signal.obtainNewMesh(grid, values);
}

/** @brief constructor that creates an object of type SignalAnalysis
*   @param[in] opt includes all the values required to create a signal analysis object
*/
GridRefinementWaveletBased::GridRefinementWaveletBased(FactoryInput::OptionsWaveletRefinemet opt)
  :
   m_signal(opt.type, opt.upperBound, opt.lowerBound, opt.thresholdedIndices,
            opt.maxRefinementLevel, opt.minRefinementLevel,
            opt. horRefinementDepth, opt.verRefinementDepth, opt.etres, opt.epsilon)
{
}


GridRefinementInterface::type GridRefinementWaveletBased::getType()const
{
  return GridRefinementInterface::WAVELET;
}


void GridRefinementWaveletBased::writeOutput(std::string fileName)const
{
  m_signal.printWavelet(fileName);
}

