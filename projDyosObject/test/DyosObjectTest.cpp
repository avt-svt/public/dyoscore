/**
* @file DyosObjectTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject  - Part of DyOS                                           \n
* =====================================================================\n
* testsuite fot the Dyos Object module                                 \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 26.11.2012
*/

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE DyosObject
#include "boost/test/unit_test.hpp"

#include "LoggerTestsuite.hpp"
