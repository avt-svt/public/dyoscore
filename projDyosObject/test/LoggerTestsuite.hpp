/**
* @file Logger.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObjet - Part of DyOS                                             \n
* =====================================================================\n
* testsuite for the Logger class                                       \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 26.11.2012
*/
#include "Logger.hpp"
#include "OutputChannel.hpp"
#include "OutputFormatter.hpp"
#include "FieldFormatter.hpp"

BOOST_AUTO_TEST_SUITE(LoggerTest)

/**
* @test LoggerTest - test StandardLogger
*
* @todo: redirect output into a file and check file output
*/
BOOST_AUTO_TEST_CASE(TestStandardLogger)
{
  StandardLogger sl;
  const double d = 1.234;
  OutputFormatter::Ptr dformat(new TypeFormatter<double>(d));
  sl.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, *dformat);
  sl.newlineAndFlush(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);
  const std::string s("Test");
  sl.sendMessage(Logger::INTEGRATOR,
                 OutputChannel::DEBUG_VERBOSITY,
                 "This should not be printed");
  sl.newlineAndFlush(Logger::INTEGRATOR, OutputChannel::DEBUG_VERBOSITY);
  sl.sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, s);
  sl.newlineAndFlush(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY);
  std::vector<int> testVec(2, 5);
  OutputFormatter::Ptr testVecFormat(new FieldFormatter<int>(testVec));
  sl.sendMessage(Logger::ESO, OutputChannel::STANDARD_VERBOSITY, *testVecFormat);
  sl.newlineAndFlush(Logger::ESO, OutputChannel::STANDARD_VERBOSITY);
  utils::Array<float> emptyArray(0);
  OutputFormatter::Ptr emptyArrayFormat(new FieldFormatter<float>(emptyArray));
  sl.sendMessage(Logger::FINAL_OUT, OutputChannel::STANDARD_VERBOSITY, *emptyArrayFormat);
  sl.newlineAndFlush(Logger::FINAL_OUT, OutputChannel::STANDARD_VERBOSITY);

}

/**
* @test LoggerTest - test logger using a FileChannel object as output channel
*
* In this test TypeFormatter and FieldFormatter are tested also.
*/
BOOST_AUTO_TEST_CASE(TestLoggerWithFileChannel)
{
  const std::string testfilename("testLoggerWithFileChannel.txt");
  const std::string firstTestString("First Test String");

  const unsigned firstNumber = 10;
  TypeFormatter<unsigned> firstNumberFormatter(firstNumber);
  std::string firstNumberString("10");

  const double secondNumber = 1.23456;  
  TypeFormatter<double> secondNumberFormatter(secondNumber);
  std::string secondNumberString("1.23456");

  std::vector<long> firstField(3,9);
  FieldFormatter<long> firstFieldFormatter(firstField);
  std::string firstFieldString("(9,9,9)");

  utils::Array<bool> secondField(5,true);
  FieldFormatter<bool> secondFieldFormatter(secondField);
  std::string secondFieldString("(1,1,1,1,1)");


  const int *thirdField = NULL;
  const unsigned fieldLength = 0;
  FieldFormatter<int> thirdFieldFormatter(thirdField, fieldLength);
  std::string thirdFieldString("()");
  // start scope of Logger l. At the end of the scope the logger should close the file
  {
    Logger l;
    OutputChannel::Ptr fileChannel(new FileChannel(testfilename));
    l.setLoggingChannel(Logger::DYOS_OUT, fileChannel);
    // log a string
    l.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, firstTestString);
    l.newlineAndFlush(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);

    // log numbers
    l.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, firstNumberFormatter);
    l.newlineAndFlush(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);
    l.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, secondNumberFormatter);
    l.newlineAndFlush(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);

    // log fields
    l.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, firstFieldFormatter);
    l.newlineAndFlush(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);
    l.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, secondFieldFormatter);
    l.newlineAndFlush(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);
    l.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, thirdFieldFormatter);
    l.newlineAndFlush(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY);
  }
  //end of Logger scope. The file used by the logger should be closed now
  std::string lineString;
  char line[256];
  std::ifstream fileChecker(testfilename.c_str());
  BOOST_REQUIRE(fileChecker.is_open());
  fileChecker.getline(line, 256);

  lineString.assign(line);
  BOOST_CHECK_EQUAL(lineString, firstTestString);

  fileChecker>>lineString;
  BOOST_CHECK_EQUAL(lineString, firstNumberString);
  fileChecker>>lineString;
  BOOST_CHECK_EQUAL(lineString, secondNumberString);

  fileChecker>>lineString;
  BOOST_CHECK_EQUAL(lineString, firstFieldString);
  fileChecker>>lineString;
  BOOST_CHECK_EQUAL(lineString, secondFieldString);
  fileChecker>>lineString;
  BOOST_CHECK_EQUAL(lineString, thirdFieldString);

  fileChecker.close();

}

/**
* @brief LoggerTest - test Logger using a MultiChannel object as output channel
*
* In this case the multi channel uses two different file channels
*/
BOOST_AUTO_TEST_CASE(TestLoggerWithMultiChannel)
{
  const std::string testfilename1("testLoggerWithMultiChannel1.txt");
  const std::string testfilename2("testLoggerWithMultiChannel2.txt");
  const std::string testString("testString");


  // start scope of Logger l. At the end of the scope the logger should close the file
  {
    Logger l;
    MultiChannel::Ptr multiChannel(new MultiChannel());
    OutputChannel::Ptr fileChannel1(new FileChannel(testfilename1));
    OutputChannel::Ptr fileChannel2(new FileChannel(testfilename2));

    multiChannel->addChannel(fileChannel1);
    multiChannel->addChannel(fileChannel2);

    l.setLoggingChannel(Logger::DYOS_OUT, multiChannel);
    // log a string
    l.sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, testString);
  }
  //end of Logger scope. The file used by the logger should be closed now
  std::ifstream fileChecker;
  std::string fileContent;
  fileChecker.open(testfilename1.c_str());
  BOOST_REQUIRE(fileChecker.is_open());

  fileChecker>>fileContent;
  BOOST_CHECK_EQUAL(fileContent, testString);

  fileChecker.close();

  fileChecker.open(testfilename2.c_str());
  BOOST_REQUIRE(fileChecker.is_open());

  fileChecker>>fileContent;
  BOOST_CHECK_EQUAL(fileContent, testString);

  fileChecker.close();
}
BOOST_AUTO_TEST_SUITE_END()
