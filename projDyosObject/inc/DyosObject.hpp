/**
* @file DyosObject.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the declaration of the DyosObject class. All other\n
* Dyos important classes of Dyos modules inherit directly or indirectly\n
* from this class.                                                     \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 09.11.2012
*/
#pragma once

//#include "boost/shared_ptr.hpp"
#include <memory>
#include "Logger.hpp"

//define some globals for Dyos
//todo remove redundancy with UserInput.hpp
#ifndef DYOS_DBL_MAX
#define DYOS_DBL_MAX 1e300
#endif

class DyosObject
{
protected:
  Logger::Ptr m_logger;
public:
  typedef std::shared_ptr<DyosObject> Ptr;
  DyosObject();
  virtual ~DyosObject();
  
  virtual void setLogger(const Logger::Ptr &logger);
};