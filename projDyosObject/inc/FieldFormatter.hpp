/**
* @file OutputFormatter.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the declaration and definition of the             \n
* FieldFormatter classes. FieldFormatter are OutputFormatters that     \n
* create a foratted string representing an array or vector             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.11.2012
*/

#pragma once

#include "OutputFormatter.hpp"
#include "Array.hpp"

template<class T>
class FieldFormatter : public OutputFormatter
{
protected:
  utils::WrappingArray<const T> m_array;
  FieldFormatter()
  {}
public:
  FieldFormatter(const T* data, const unsigned fieldSize) : m_array(fieldSize, data)
  {}
  
  FieldFormatter(const utils::Array<T> &arrayIn) : m_array(arrayIn.getSize(), arrayIn.getData())
  {}
  
  /**
  * @brief constructor for a vector
  *
  * @note this does not work with bool vectors, because &vec[0] is no L-value
  *       in case of bool type. If needed a special FieldFormatter for bool
  *       vectors must be implemented
  */
  FieldFormatter(const std::vector<T> &vec) : m_array(0, NULL)
  {
    if(!vec.empty()){
      m_array.setData(vec.size(), &vec[0]);
    }
  }
  
  virtual ~FieldFormatter()
  {}

  virtual void putDataIntoStream()
  {
    m_sstream<<m_array;
  }
};

