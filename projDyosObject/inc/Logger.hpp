/**
* @file Logger.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the declaration of the Logger class.              \n
* The Logger class manages the IO of the complete Dyos module          \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 28.11.2012
*/

#pragma once
//#include "boost/shared_ptr.hpp"
#include <memory>
#include "OutputFormatter.hpp"
#include "OutputChannel.hpp"
#include <string>
#include <iostream>
#include <sstream>
#include <map>


/**
* @class Logger
* @brief The Logger manages the distribution of Logging messages.
*
* Usually a set of data is stored in an OutputFormatter object. If a
* LoggingChannel is set, the set of data is then converted into a string and
* send into the output stream of an OutputChannel object.
*/
class Logger
{
public:
  typedef std::shared_ptr<Logger> Ptr;

  enum LoggingChannel{
    DYOS_OUT,
    INTEGRATOR,
    OPTIMIZER,
    ESO,
    FINAL_OUT, ///< print output struct
    INPUT_OUT_JSON, ///< print input struct in Json format
    INPUT_OUT_XML, ///< print input struct in xml format
    FINAL_OUT_JSON, ///< print output struct in Json format
    FINAL_OUT_XML, ///< print output struct in xml format
    STATENAMES_OUT, ///< print output in format of statenames.dat like old dyos
    FINALSTATES_OUT ///< print output in format of finalstate.dat like old dyos
  };
  
protected:
  std::map<Logger::LoggingChannel, OutputChannel::Ptr> m_channelMap;
public:

  virtual void sendMessage(const LoggingChannel loggingChannel,
                           const OutputChannel::VerbosityLevel verbosityLevel,
                           const std::string &message);

  virtual void sendMessage(const LoggingChannel loggingChannel,
                           const OutputChannel::VerbosityLevel verbosityLevel,
                           OutputFormatter &formatter);

  virtual void newline(const LoggingChannel loggingChannel,
                       const OutputChannel::VerbosityLevel verbosityLevel);
  virtual void newlineAndFlush(const LoggingChannel loggingChannel,
                               const OutputChannel::VerbosityLevel verbosityLevel);
  virtual void flushAll();

  virtual void setLoggingChannel(const LoggingChannel channelIndex,
                                 const OutputChannel::Ptr &outputChannel);
};

/**
* @class StandardLogger
* @brief Logger using standard output channels cout, clog and cerr
*
* Debug channels are not set by this class
*/
class StandardLogger : public Logger
{
public:
  StandardLogger();
};


class StdCapture
{
public:
  StdCapture();

  ~StdCapture();


  void beginCapture();

  bool endCapture();

  std::string getCapture() const;

private:
    bool m_capturing;
    bool m_init;
    std::string m_captured;
    std::stringstream m_buffer;
    std::streambuf * m_oldBuffer; 
};