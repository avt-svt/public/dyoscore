/**
* @file OutputChannel.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the declaration of the OutputChannel class        \n
* The OutputChannel redirects an ostream to a certain target           \n
* used by the Logger class                                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 26.11.2012
*/
#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
//#include <boost/shared_ptr.hpp>
#include <memory>

/**
* @class OutputChannel
* @brief interface for output channel used by Logger
*
* The Logger redirects all data and messages that are to be printed to
* OutputChannel objects that print the data to an output device
*/
class OutputChannel
{
public:
  enum VerbosityLevel{
    DEBUG_VERBOSITY,
    WARNING_VERBOSITY,
    STANDARD_VERBOSITY,
    ERROR_VERBOSITY
  };
  /// shared pointer type for this class
  typedef std::shared_ptr<OutputChannel> Ptr;

protected:
  VerbosityLevel m_verbosity;
  OutputChannel();
  virtual void print(const std::string &message) = 0;
public:
  virtual void print(const std::string &message,const VerbosityLevel verbosityLevel);
  virtual void setVerbosity(VerbosityLevel verbosity);
  virtual void flush() = 0;
};

/**
* @class StandardOut
* @brief output channel writing to cout
*/
class StandardOut : public OutputChannel
{
public:
  /// shared pointer type for this class
  typedef std::shared_ptr<StandardOut> Ptr;
  virtual void print(const std::string &message);
  virtual void flush();
};

/**
* @class StandardErr
* @brief output channel writing to cerr
*/
class StandardErr : public OutputChannel
{
public:
  StandardErr();
  /// shared pointer type for this class
  typedef std::shared_ptr<StandardErr> Ptr;
  virtual void print(const std::string &message);
  virtual void flush();
};

/**
* @class StandardLog
* @brief output channel writing to clog
*/
class StandardLog : public OutputChannel
{
public:
  /// shared pointer type for this class
  typedef std::shared_ptr<StandardLog> Ptr;
  virtual void print(const std::string &message);
  virtual void flush();
};

/**
* @class FileChannel
* @brief output channel writing to a file
*/
class FileChannel : public OutputChannel
{
public:
  /// shared pointer type for this class
  typedef std::shared_ptr<FileChannel> Ptr;
protected:
  /// filestream writing the output - doesnn't change during object lifetime
  std::ofstream m_file;
public:
  FileChannel(const std::string &filename);
  
  virtual ~FileChannel();
  
  virtual void print(const std::string &message);
  virtual void flush();
};

/**
* @class MultiChannel
* @brief otuput channel writing to any number of output channel
*/
class MultiChannel : public OutputChannel
{
public:
  /// shared pointer type for this class
  typedef std::shared_ptr<MultiChannel> Ptr;
protected:
  /// list of output channels the output is forwarded to
  std::vector<OutputChannel::Ptr> m_channel;
  virtual void print(const std::string &message);
public:
  virtual void addChannel(const OutputChannel::Ptr &newChannel);
  
  virtual void print(const std::string &message, 
                     const OutputChannel::VerbosityLevel verbosityLevel);
  virtual void flush();
};
