/**
* @file OutputFormatter.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the declaration of the OutputFormatter class and  \n
* TypeFormatter classes.                                               \n
* The OutputFormatter manages conversions of types into a string and is\n
* used by the Logger class                                             \n
* TypeFormatters are OutputFormatters for any type that can be put into\n
* an output stream                                                     \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 26.11.2012
*/

#pragma once
#include <iostream>
#include <sstream>
//#include "boost/shared_ptr.hpp"
#include <memory>

class OutputFormatter
{
protected:
  std::stringstream m_sstream;
  virtual void putDataIntoStream() = 0;
public:
  typedef std::shared_ptr<OutputFormatter> Ptr;
  virtual std::string toString()
  {
    m_sstream.clear();
    putDataIntoStream();
    return m_sstream.str();
  }
};


template<class T>
class TypeFormatter : public OutputFormatter
{
protected:
  TypeFormatter(){}
  const T *m_data;
  virtual void putDataIntoStream()
  {
    m_sstream<<*m_data;
  }
  
public:
  TypeFormatter(const T *data)
  {
    m_data = data;
  }
  
  TypeFormatter(const T &data)
  {
    m_data = &data;
  }
  
  TypeFormatter(const std::shared_ptr<T> &data)
  {
    m_data = data.get();
  }
};

class Newline : public OutputFormatter
{
protected:
  unsigned m_numNewlines;
public:
  Newline(const unsigned numNewlines = 1)
  {
    m_numNewlines = numNewlines;
  }
  
  virtual void putDataIntoStream()
  {
    for(unsigned i=0; i<m_numNewlines; i++){
      m_sstream<<std::endl;
    }
  }
};