/**
* @file DyosObject.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the implementation of the DyosObject class.       \n
* All other Dyos important classes of Dyos modules inherit directly or \n
* indirectly from this class.                                          \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 09.11.2012
*/

#include "DyosObject.hpp"

/**
* @brief standard constructor
* 
* create objects for standard use
* StandardLogger is used as logger.
*/
DyosObject::DyosObject()
{
  m_logger = Logger::Ptr(new StandardLogger());
}

/**
* @brief destructor
*/
DyosObject::~DyosObject()
{}

/**
* @brief set a new logger to this DyosObject
*
* @param logger pointer to the new Logger to be set
*/
void DyosObject::setLogger(const Logger::Ptr &logger)
{
  m_logger = logger;
}
