/**
* @file Logger.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the definition of the Logger class.               \n
* The Logger class manages the IO of the complete Dyos module          \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 09.11.2012
*/

#include "Logger.hpp"

/**
* @brief send message to given output channel
*
* @param loggingChannel key for the specified output channel
* @param message message to be send to the output channel
*/
void Logger::sendMessage(const LoggingChannel loggingChannel,
                         const OutputChannel::VerbosityLevel verbosityLevel,
                         const std::string &message)
{
  if(m_channelMap.count(loggingChannel) > 0){
    m_channelMap[loggingChannel]->print(message, verbosityLevel);
  }
}

/**
* @brief send data to given output channel
*
* @param loggingChannel key for the specified output channel
* @param foratter OutputFormatter containing the data. If an output channel
*                 has been set for the given loggingChannel, the formatter
*                 converts the data to a string that is send to the output
*                 channel.
*/
void Logger::sendMessage(const LoggingChannel loggingChannel,
                         const OutputChannel::VerbosityLevel verbosityLevel,
                         OutputFormatter &formatter)
{
  if(m_channelMap.count(loggingChannel) > 0){
    m_channelMap[loggingChannel]->print(formatter.toString(), verbosityLevel);
  }
}

/**
* @brief set an output channel for a given key
*
* @param channelIndex key used in the channel map for the output channel
* @param outputChannel output channel set for the given key
*/
void Logger::setLoggingChannel(const LoggingChannel channelIndex,
                               const OutputChannel::Ptr &outputChannel)
{
  m_channelMap[channelIndex] = outputChannel;
}

void Logger::newline(const LoggingChannel loggingChannel,
                     const OutputChannel::VerbosityLevel verbosityLevel)
{
  Newline n(1);
  sendMessage(loggingChannel, verbosityLevel, n);
}

void Logger::newlineAndFlush(const LoggingChannel loggingChannel,
                             const OutputChannel::VerbosityLevel verbosityLevel)
{
  if(m_channelMap.count(loggingChannel) > 0){
    newline(loggingChannel, verbosityLevel);
    m_channelMap[loggingChannel]->flush();
  }
}

void Logger::flushAll()
{
  std::map<LoggingChannel, OutputChannel::Ptr>::iterator iter;
  for(iter = m_channelMap.begin(); iter != m_channelMap.end(); iter++){
    iter->second->flush();
  }
}

/**
* @brief standard constructor
*
* sets all non-debug channels to standard channels (out, err, log)
*/
StandardLogger::StandardLogger()
{
  OutputChannel::Ptr out(new StandardOut());
  OutputChannel::Ptr err(new StandardErr());
  OutputChannel::Ptr log(new StandardLog());
  m_channelMap[DYOS_OUT] = out;
  m_channelMap[INTEGRATOR] = out;
  m_channelMap[OPTIMIZER] = out;
  //m_channelMap[ESO] = out;
}


StdCapture::StdCapture()
{
  m_oldBuffer = NULL;
  m_capturing = false;
}

StdCapture::~StdCapture()
{
  if (m_capturing)
  {
    endCapture();
  }
}


void StdCapture::beginCapture()
{
  if (m_capturing)
      endCapture();
  std::cout.flush();
  m_oldBuffer = std::cout.rdbuf(m_buffer.rdbuf());
  
  if(m_oldBuffer == NULL){
    m_capturing = false;
  }
  else{
    m_capturing = true;
  }
}

bool StdCapture::endCapture()
{
  if (!m_capturing)
      return false;

  m_captured.append(m_buffer.str());
  std::cout.rdbuf(m_oldBuffer);
  m_capturing = false;
  return true;
}

std::string StdCapture::getCapture() const
{
  std::string::size_type idx = m_captured.find_last_not_of("\r\n");
  if (idx == std::string::npos)
  {
    return m_captured;
  }
  else
  {
    return m_captured.substr(0, idx+1);
  }
}
