cmake_minimum_required(VERSION 3.11)

set(DYOS_OBJECT_SRC
  src/DyosObject.cpp
  src/Logger.cpp
  src/OutputChannel.cpp
)

set(DYOS_OBJECT_USER_INC
  inc/Logger.hpp
  inc/OutputChannel.hpp
  inc/OutputFormatter.hpp
)

set(DYOS_OBJECT_INC
  ${DYOS_OBJECT_USER_INC}
  inc/DyosObject.hpp
  inc/FieldFormatter.hpp
)

add_library(DyosObject ${DYOS_OBJECT_SRC} ${DYOS_OBJECT_INC})

target_link_libraries(DyosObject
  CppUtils
)

target_include_directories(DyosObject PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/inc
)

##### test of DyosObject

set(TEST_DYOS_OBJECT_SRC
  test/DyosObjectTest.cpp
  test/LoggerTestsuite.hpp
)

add_executable(TestDyosObject ${TEST_DYOS_OBJECT_SRC})
target_link_libraries(TestDyosObject
  DyosObject
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)
set_target_properties(TestDyosObject PROPERTIES FOLDER Tests) 


###### DyosUser Version 

install(TARGETS DyosObject
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)

install(FILES ${DYOS_OBJECT_USER_INC} DESTINATION include)