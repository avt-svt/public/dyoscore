/**
* @file FMILoader.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic FMILoader - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the FMILoader module	                             \n
* =====================================================================\n
* @author Adrian Caspari
* @date 16.10.2017
*/


/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/

/* // this is the used model in the FMU
model TrivialModel
  Real x1,x2;
  output Real y( start = 6);
  input Real p(start=1);

initial equation 
  x1= 1 + x2;
  x2=4;

equation 
  der(x1) = -p*y + x2 - x1;
  der(x2)=-x1;
  y = x1^2;
end TrivialModel;

Advanced.GenerateAnalyticJacobian = true
*/


#include "DyosFmiLoader.hpp"
#include "FMILoaderTestConfig.hpp"




BOOST_AUTO_TEST_SUITE(TestFMILoader)

BOOST_AUTO_TEST_CASE(TestFMILoaderConstructor){

	
  std::string path = PATH_FMI_LOADER_TEST_FMI;




  BOOST_CHECK_NO_THROW(FMILoader FMILoaderObj(path.data(),1e-8));




  }

  BOOST_AUTO_TEST_CASE(TestFMILoaderGetFunctions){
  

  std::string path = PATH_FMI_LOADER_TEST_FMI;
  FMILoader FMILoaderObj2(path.data(),1e-8);



  BOOST_CHECK_EQUAL(FMILoaderObj2.getNumberOutputVariables(),1);
  BOOST_CHECK_EQUAL(FMILoaderObj2.Get_num_pars(),1);
  BOOST_CHECK_EQUAL(FMILoaderObj2.getNumberContinousStates(),2); 
  BOOST_CHECK_EQUAL(FMILoaderObj2.Get_num_eqns(),3);
  BOOST_CHECK_EQUAL(FMILoaderObj2.getNumberDifferentialEquations(),2);
  BOOST_CHECK_EQUAL(FMILoaderObj2.getNumberAlgebraicEquations(),1);
  BOOST_CHECK_EQUAL(FMILoaderObj2.getNumberNonZeros(),5);
  BOOST_CHECK_EQUAL(FMILoaderObj2.getNumberInitialNonZeroes(),1);
  BOOST_CHECK_EQUAL(FMILoaderObj2.getNumberDifferentialNonZeros(),2);
  BOOST_CHECK_EQUAL(FMILoaderObj2.Get_num_cond(),0);
  BOOST_CHECK_EQUAL(FMILoaderObj2.Get_num_vars(),3);
  
  // check variable names of FMU
  std::vector<std::string> names;
  std::vector<std::string> namesCorrect; 
  namesCorrect.push_back("x1");
  namesCorrect.push_back("x2");
  namesCorrect.push_back("y");
  names.resize(FMILoaderObj2.Get_num_vars());
  FMILoaderObj2.GetVarNames(&names);
  for(int i=0; i<FMILoaderObj2.Get_num_vars(); i++){
    BOOST_CHECK_EQUAL(namesCorrect[i],names[i]);
  }




  // check parmeter names of FMU
  std::vector<std::string> namesPar;
  std::vector<std::string> namesParCorrect; 
  namesParCorrect.push_back("p");
  namesPar.resize(FMILoaderObj2.Get_num_pars());
  FMILoaderObj2.GetParNames(&namesPar);
  for(int i=0; i<FMILoaderObj2.Get_num_pars(); i++){
    BOOST_CHECK_EQUAL(namesParCorrect[i],namesPar[i]);
  }


  // check initial values for variables and parameters


  int n_x=0;
  int n_p= 0 ;
  int n_c=10;
  //double* xValue=new double[FMILoaderObj2.Get_num_vars()];

  std::vector<double> xValue(FMILoaderObj2.Get_num_vars());

  //double* p = new double[FMILoaderObj2.Get_num_pars()];
  std::vector<double> p(FMILoaderObj2.Get_num_pars());

  FMILoaderObj2.Init(xValue.data(), p.data(), n_x, n_p, n_c);
  BOOST_CHECK_EQUAL(n_x,3);
  BOOST_CHECK_EQUAL(n_p,1);
  BOOST_CHECK_EQUAL(n_c,0);
  BOOST_CHECK_EQUAL(xValue[0],5);
  BOOST_CHECK_EQUAL(xValue[1],4);
  BOOST_CHECK_EQUAL(xValue[2],25);
  BOOST_CHECK_EQUAL(p[0],1);


 // check set input values and states 
 //double* yy = new double[FMILoaderObj2.Get_num_vars()];
 std::vector<double> yy(FMILoaderObj2.Get_num_vars());

 //double* der_x = new double[FMILoaderObj2.Get_num_vars()];
 std::vector<double> der_x(FMILoaderObj2.Get_num_vars());

 der_x[0]=1;
 der_x[1]=1;

 //int* condit = new int;
 //int* lock=new int;
 //int* prev=new int;

 //int condit;
 //std::unique_ptr<int> lock(new int);
 int lock = 0;
 int prev = 0;
 int condit = 0;

 xValue[2]=1;
 FMILoaderObj2.Res(yy.data(), der_x.data(), xValue.data(),  p.data(), &condit,  &lock, &prev,  n_x,  n_p,  n_c);
 BOOST_CHECK_EQUAL(yy[0],27);
 BOOST_CHECK_EQUAL(yy[1],6);
 BOOST_CHECK_EQUAL(yy[2],-24);

 int n_yy_ind = 2;
 int yy_ind[2] = {0,2}; 

 yy.resize(n_yy_ind);
 yy[0] = 0;
 yy[1] = 0;
 xValue[0]=4;
 xValue[1]=5;
 xValue[2]=1;
 der_x[0]=1;
 der_x[1]=1;
 p[0] =2;
 FMILoaderObj2.Res_block( yy.data(),  der_x.data(),  xValue.data(),  p.data(), &condit, &lock, &prev, n_x, n_p, n_c,n_yy_ind,  yy_ind);

 BOOST_CHECK_EQUAL(yy[0],32);
 BOOST_CHECK_EQUAL(yy[1],-15);

 // check Jacobian calculation 

 
 //double* t1_yy = new double[FMILoaderObj2.Get_num_vars()];
 std::vector<double> t1_yy(FMILoaderObj2.Get_num_vars());

 //double* t1_der_x = new double[3];
 std::vector<double> t1_der_x(3);


 t1_der_x[0] = 1;
 t1_der_x[1] = 1;
 t1_der_x[2] = 1;


 //double* t1_x = new double[3];
 std::vector<double> t1_x(3);
 t1_x[0] = 1; 
 t1_x[1] = 1; 
 t1_x[2] = 1; 

 //double* t1_p = new double; 
 std::vector<double> t1_p(1);
 t1_p[0] = 1; //seed paramters
 xValue[0]=4;
 xValue[1]=5;
 xValue[2]=1;
 FMILoaderObj2.T1_res(yy.data(), t1_yy.data(), der_x.data(), t1_der_x.data(), xValue.data(), t1_x.data(),  p.data(),  
                     t1_p.data(), &condit, &lock, &prev, n_x, n_p, n_c);
 BOOST_CHECK_EQUAL(t1_yy[0],33);
 BOOST_CHECK_EQUAL(t1_yy[1],2);
 BOOST_CHECK_EQUAL(t1_yy[2],-7);

 ///// 2 check jacobian ///////////

 t1_der_x[0] = 0;
 t1_der_x[1] = 0;
 t1_der_x[2] = 0;

 t1_x[0] = 1.0; 
 t1_x[1] = 0.0; 
 t1_x[2] = 0.0; 

 t1_p[0] = 0.0;

 xValue[0]=4;
 xValue[1]=5;
 xValue[2]=1;

 FMILoaderObj2.T1_res(yy.data(), t1_yy.data(), der_x.data(), t1_der_x.data(), xValue.data(), t1_x.data(),  p.data(),  
                     t1_p.data(), &condit, &lock, &prev, n_x, n_p, n_c);




 BOOST_CHECK_CLOSE(t1_yy[0],17,THRESHOLD);
 BOOST_CHECK_CLOSE(t1_yy[1],1,THRESHOLD);
 BOOST_CHECK_CLOSE(t1_yy[2],-8,THRESHOLD);

 std::vector<jsp> yy_jsp(FMILoaderObj2.Get_num_eqns());
  std::vector<jsp> yy_jsp_correct(FMILoaderObj2.Get_num_eqns());
 std::vector<jsp> der_x_jsp(FMILoaderObj2.Get_num_eqns());
 std::vector<jsp> xValue_jsp(FMILoaderObj2.Get_num_eqns());
 std::vector<jsp> p_jsp(FMILoaderObj2.Get_num_pars());
 
 yy_jsp[0].v = 1;
 yy_jsp[1].v = 1;
 yy_jsp[2].v = 1;
 
 yy_jsp_correct[0].v = 0;
 yy_jsp_correct[1].v = 0;
 yy_jsp_correct[2].v = 0;


 der_x_jsp[0].v = 0;
 der_x_jsp[1].v = 0;
 der_x_jsp[2].v = 0;
 der_x_jsp[0].nz.insert(1);
 der_x_jsp[1].nz.insert(2);
 der_x_jsp[2].nz.insert(3);

 xValue_jsp[0].v = 1;
 xValue_jsp[1].v = 1;
 xValue_jsp[2].v = 1;
 xValue_jsp[0].nz.insert(4);
 xValue_jsp[1].nz.insert(5);
 xValue_jsp[2].nz.insert(6);

 p_jsp[0].v=1;
 p_jsp[0].nz.insert(7);


 FMILoaderObj2.Jsp_res(yy_jsp.data(), der_x_jsp.data(), xValue_jsp.data(),  p_jsp.data(), &condit, &lock, &prev, n_x,  n_p,  n_c);

 
 yy_jsp_correct[0] = der_x_jsp[0] - xValue_jsp[0] - xValue_jsp[1]-p_jsp[0];
 yy_jsp_correct[1] = der_x_jsp[1] - xValue_jsp[0];
 yy_jsp_correct[2] = xValue_jsp[0] - xValue_jsp[2];

 

 for (int i=0; i<FMILoaderObj2.Get_num_vars(); i++){

	  BOOST_CHECK_EQUAL(yy_jsp[i].v, yy_jsp_correct[i].v);

	  std::vector<int> dependency (yy_jsp[i].nz.size());
	  std::vector<int> dependencyCorrect (yy_jsp_correct[i].nz.size());

	  std::set<int>::iterator itDep = yy_jsp[i].nz.begin();
	  std::set<int>::iterator itDepCorrect = yy_jsp_correct[i].nz.begin();

	  for (int j=0; j < (int) yy_jsp[i].nz.size(); j++){

		  dependency[j]= *itDep;
		  dependencyCorrect[j]= *itDepCorrect;
		  
		  BOOST_CHECK_EQUAL(dependency[j], dependencyCorrect[j]);

		  itDep++;
		  itDepCorrect++;
	  }
	  
	
 }

  }



  BOOST_AUTO_TEST_CASE(TestGetAlgebraicVariable) {

	  FMILoader FMILoaderObj(PATH_FMI_LOADER_TEST_FMI,1e-8);



	  std::vector <double> y_alg(FMILoaderObj.getNumberAlgebraicEquations());
	  std::vector <double>  y_alg_real(FMILoaderObj.getNumberAlgebraicEquations());

	  y_alg_real[0] = 16;


	  std::vector <double> xValue(FMILoaderObj.getNumberContinousStates());

	  xValue[0] = 4;
	  xValue[1] = 2;



	  std::vector <double> p(FMILoaderObj.Get_num_pars());
	  p[0] = 1;


	  std::vector <int> condit(FMILoaderObj.Get_num_cond());
	  std::vector <int> lock(FMILoaderObj.Get_num_cond());
	  std::vector <int> prev(FMILoaderObj.Get_num_cond());

	  int n_x = FMILoaderObj.getNumberContinousStates();
	  int n_p = FMILoaderObj.Get_num_pars();
	  int n_c = FMILoaderObj.Get_num_cond();
	  int n_y = FMILoaderObj.getNumberAlgebraicEquations();

	  FMILoaderObj.getAlgebraicVariables(y_alg.data(), xValue.data(), p.data(),
		   condit.data(), lock.data(), prev.data(), n_x, n_p, n_y, n_c);
	 
	  for (int i = 0; i < n_y;i++) {

		  BOOST_CHECK_CLOSE(y_alg[i], y_alg_real[i], THRESHOLD);

	  }


  }



  BOOST_AUTO_TEST_CASE(TestGetDerivativeVariable) {

	  FMILoader FMILoaderObj(PATH_FMI_LOADER_TEST_FMI,1e-8);



	  std::vector <double> x_der(FMILoaderObj.getNumberContinousStates());
	  std::vector <double> x_der_real(FMILoaderObj.getNumberContinousStates());

	  x_der_real[0] = -18;
	  x_der_real[1] = -4;


	  std::vector <double> xValue(FMILoaderObj.getNumberContinousStates());

	  xValue[0] = 4;
	  xValue[1] = 2;



	  std::vector <double> p(FMILoaderObj.Get_num_pars());
	  p[0] = 1;


	  std::vector <int> condit(FMILoaderObj.Get_num_cond());
	  std::vector <int> lock(FMILoaderObj.Get_num_cond());
	  std::vector <int> prev(FMILoaderObj.Get_num_cond());

	  int n_x = FMILoaderObj.getNumberContinousStates();
	  int n_p = FMILoaderObj.Get_num_pars();
	  int n_c = FMILoaderObj.Get_num_cond();

	  FMILoaderObj.getDerivativeVariables(x_der.data(), xValue.data(), p.data(),
		  condit.data(), lock.data(), prev.data(), n_x, n_p,n_c);

	  for (int i = 0; i < n_x;i++) {

		  BOOST_CHECK_CLOSE(x_der[i], x_der_real[i], THRESHOLD);

	  }


  }




  BOOST_AUTO_TEST_CASE(TestFMILoaderInitialModel) {

	  FMILoader FMILoaderObj(PATH_FMI_LOADER_TEST_FMI,1e-8);


	
	  std::vector <double> yy(FMILoaderObj.Get_num_vars());

	  std::vector <double> yyStart(FMILoaderObj.Get_num_vars());

	  yyStart[0] = 27;
	  yyStart[1] = 6;
	  yyStart[2] = 0;

	  std::vector <double> t1_yy(FMILoaderObj.Get_num_vars());

	  std::vector <double> t1_yyStart(FMILoaderObj.Get_num_vars());

	  t1_yyStart[0] = 35;
	  t1_yyStart[1] = 1;
	  t1_yyStart[2] = -9;

	  std::vector <double> t1_der_x(FMILoaderObj.Get_num_vars());


	  t1_der_x[0] = 0;
	  t1_der_x[1] = 0;
	  t1_der_x[2] = 0;

	  std::vector <double> t1_x(FMILoaderObj.Get_num_vars());
	  t1_x[0] = 1;
	  t1_x[1] = 1;
	  t1_x[2] = 1;

	  std::vector <double> t1_p(FMILoaderObj.Get_num_pars());

	  t1_p[0] = 1; //seed paramters

	  std::vector <double> xValue(FMILoaderObj.Get_num_vars());

	  xValue[0] = 0;
	  xValue[1] = 0;
	  xValue[2] = 0;

	  std::vector <double> xValueStart(FMILoaderObj.Get_num_vars());

	  xValueStart[0] = 5;
	  xValueStart[1] = 4;
	  xValueStart[2] = 25;

	  std::vector <double> der_x(FMILoaderObj.Get_num_vars());

	  der_x[0] = 1;
	  der_x[1] = 1;


	  std::vector <double> p(FMILoaderObj.Get_num_pars());
	  p[0] = 1;


	  std::vector <int> condit (FMILoaderObj.Get_num_cond());
	  std::vector <int> lock (FMILoaderObj.Get_num_cond());
	  std::vector <int> prev (FMILoaderObj.Get_num_cond());

	  int n_x = FMILoaderObj.Get_num_vars();
	  int n_p = FMILoaderObj.Get_num_pars();
	  int n_c= FMILoaderObj.Get_num_cond();

	  FMILoaderObj.T1_res_initial(yy.data(), t1_yy.data(), der_x.data(), t1_der_x.data(), xValue.data(), t1_x.data(), p.data(),
		 t1_p.data(), condit.data(), lock.data(), prev.data(), n_x, n_p, n_c);

	  for (int i = 0; i < n_x;i++) {

		  BOOST_CHECK_CLOSE(xValue[i], xValueStart[i], THRESHOLD);
		  BOOST_CHECK_CLOSE(yy[i], yyStart[i], THRESHOLD);
		  BOOST_CHECK_CLOSE(t1_yy[i], t1_yyStart[i], THRESHOLD);


	  }


  }



  BOOST_AUTO_TEST_CASE(TestGetEsoIndex) {

	  FMILoader FMILoaderObj(PATH_FMI_LOADER_TEST_FMI,1e-8);

	  fmi2_import_variable_list_t * variableListFMI = FMILoaderObj.get_variableList();

	  size_t allVariableNumber;
	  FMILoaderObj.getNumberAllVariables(allVariableNumber);

	  size_t numInputs;
	  FMILoaderObj.getNumberInputs(numInputs);

	  int numContStates = FMILoaderObj.getNumberContinousStates();

	  std::vector<size_t> esoIndex(allVariableNumber + numInputs + numContStates);

	  for (int i = 0; i < (int) allVariableNumber +(int)  numInputs + (int) numContStates; i++) {

		  fmi2_import_variable_t * variable = fmi2_import_get_variable(variableListFMI, i);
		  FMILoaderObj.getEsoVarIndex(esoIndex[i], variable);



	  }

	  std::vector<size_t> esoIndexFromFMUIndex(allVariableNumber + numInputs + numContStates);

	  for (size_t i = 0; i < allVariableNumber + numInputs + numContStates; i++) {

		  size_t indexFMU = i + 1;
		  FMILoaderObj.getEsoIndexFromFMUIndex(esoIndexFromFMUIndex[i], indexFMU);

	  }
	  

	  std::vector<int> esoIndexCorrect(allVariableNumber + numInputs + numContStates);
	  esoIndexCorrect[0] = 0;
	  esoIndexCorrect[1] = 4;
	  esoIndexCorrect[2] = 1;
	  esoIndexCorrect[3] = 5;
	  esoIndexCorrect[4] = 2;
	  esoIndexCorrect[5] = 3;

	  for (int i = 0; i < (int) allVariableNumber + (int) numInputs + (int) numContStates; i++) {

		  BOOST_CHECK_EQUAL(esoIndexCorrect[i], esoIndexFromFMUIndex[i]);
		  BOOST_CHECK_EQUAL(esoIndexCorrect[i], esoIndex[i]);

	  }




  }


/*
  BOOST_AUTO_TEST_CASE(TestHessianCalculation) {

	  FMILoader FMILoaderObj(PATH_FMI_LOADER_TEST_FMI, 1e-8);

	  std::vector<double> yy(3);
	  std::vector<double> t2_yy = { 1,1,1 };
	  std::vector<double> a1_yy = { 0,0,0,0,1,1,1 };
	  std::vector<double> t2_a1_yy = { 1,1,1 };
	  std::vector<double> der_x = {1,1,1};
	  std::vector<double> t2_der_x = { 1,1,1 };
	  std::vector<double> a1_der_x = { 0,0,0 };
	  std::vector<double> t2_a1_der_x = { 0,0,0 };
	  std::vector<double> x = { 1,1,1 };
	  std::vector<double> t2_x = { 1,1,1 };
	  std::vector<double> a1_x = {0,0,0 };
	  std::vector<double> t2_a1_x = { 0,0,0 };
	  std::vector<double> p = { 1};
	  std::vector<double> t2_p = { 1 };
	  std::vector<double> a1_p = { 0};
	  std::vector<double> t2_a1_p = { 0 };

	  std::vector<int> condit = { 0 };
	  std::vector<int> lock = { 0 };
	  std::vector<int> prev = { 0 };

	  int n_x = 3;
	  int n_p = 1;
	  int n_c = 0;

	  FMILoaderObj.T2_a1_res(yy.data(), t2_yy.data(), a1_yy.data(), t2_a1_yy.data(),
		  der_x.data(), t2_der_x.data(), a1_der_x.data(), t2_a1_der_x.data(),
		  x.data(), t2_x.data(), a1_x.data(), t2_a1_x.data(), p.data(),
		  t2_p.data(), a1_p.data(), t2_a1_p.data(), condit.data(), lock.data(), prev.data(), n_x, n_p, n_c);



  }
  */


BOOST_AUTO_TEST_SUITE_END()





