/**
* @file FMILoader.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic FMILoader - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the FMILoader module	                             \n
* =====================================================================\n
* @author Adrian Caspari
* @date 16.10.2017
*/

#ifndef _FMILOADERTESTCONFIG_H_
#define _FMILOADERTESTCONFIG_H_


// Path to fmi.fmu for FmiLoaderTest
#cmakedefine PATH_FMI_LOADER_TEST_FMI "@PATH_FMI_LOADER_TEST_FMI@"



#endif