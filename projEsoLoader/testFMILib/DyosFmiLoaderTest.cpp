/**
* @file DyosFmiLoaderTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the Generic Eso module	                             \n
* =====================================================================\n
* @author Adrian Caspari and Johannes Faust
* @date 26.10.2017
*/


/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
//*/
//#define BOOST_TEST_MODULE Test_FMI_Loader
//#include "boost/test/unit_test.hpp"

#define BOOST_TEST_MODULE TEST_FMILOADER

#include "DyosFmiLoader.hpp"



#include "boost/test/unit_test.hpp"
#include "boost/test/floating_point_comparison.hpp"


#include "jsp.hpp" // adca is not happy at all

#define THRESHOLD 1e-9



#include "FMILoaderTestsuite.hpp"
