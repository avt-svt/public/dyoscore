/**
* @file FmiLoader.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Header with declaration of class FmiLoader                       \n
* =====================================================================\n
* @author Adrian Caspari
* @date 11.10.2017
*/

#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>



//in order to create directory for FMU unzipping
//#include <direct.h>
#include <boost/filesystem.hpp>


#include <string>
#include <iostream>
#include <vector>
#include "jsp.hpp" // adca is not happy at all


#include <fmilib.h>
#include <FMI2/fmi2_import_variable_list.h>
#include <FMI2/fmi2TypesPlatform.h>
#include <FMI2/fmi2Functions.h>



//class LibraryWrapper;

/**
* @class FMILoader
*
* @brief Class that encapsulates all commands from FMIGenericEso to the model library
*
* The class FMILoader links at runtime an FMI-model by FMILib.dll and redirects the calls to its
* member functions to the functions defined in the 'model'.fmi.
* This class treats DA-systems given by M*der_x = f(x,p). All member functions treat the residual
* system given by
*
* Res(x,p,der_x) = M*der_x - f(x,p),
*
* where x are the states and p the parameters.
* In the following equations we refer to z, defined as z = [x p der_x]^T.
*/


class FMILoader{
public:


  void GetVarNames(std::vector<std::string> * names);
  void GetParNames(std::vector<std::string> * names);
  void GetAllVarNames(std::vector<std::string> * names);
  void Init(double * x, double * p, int &n_x, int& n_p, int& n_c);


  void Res(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
  void ResAlgebraic(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
  void ResDifferential(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
  void getAlgebraicVariables(double* y, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_y, int& n_c);
  void getDerivativeVariables(double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  void Res_block(double * yy, double * der_x, double * x, double * p,
		 int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
                 int &n_yy_ind, int* yy_ind);
  void Res_cond(double* yy, double* der_x, double* x, double* p,
 		int &n_x, int &n_p, int &n_c);
  void Eval_cond(int* yy, double* der_x, double* x, double* p,
		 int &n_x, int &n_p, int &n_c);
  void T1_res_block(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
                    double* t1_x, double* p, double* t1_p,
		    int* condit, int* lock, int* prev, int& n_x, int& n_p, int &n_c,
                    int& n_yy_ind, int* yy_ind);
  void T1_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
              double* t1_x, double* p, double* t1_p,
	      int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  void T1_res_without_res(double* t1_yy, double* der_x, double* t1_der_x, double* x,
	  double* t1_x, double* p, double* t1_p,
	  int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  void T1_res_block_without_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
	  double* t1_x, double* p, double* t1_p,
	  int* condit, int* lock, int* prev, int& n_x, int& n_p, int &n_c,
	  int& n_yy_ind, int* yy_ind);

  void T2_a1_res(double* yy, double* t2_yy, double* a1_yy, double* t2_a1_yy,
	  double* der_x, double* t2_der_x, double* a1_der_x, double* t2_a1_der_x,
	  double* x, double* t2_x, double* a1_x, double* t2_a1_x, double* p,
	  double* t2_p, double* a1_p, double* t2_a1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

   void Jsp_res(jsp* yy, jsp* der_x, jsp* x, jsp* p, int* condit, int* lock, int* prev,
  	       int& n_x, int& n_p, int& n_c);
   void T1_res_initial(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x, double* t1_x, double* p, double* t1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

   // getter for initial dependencies
   void getInitialDependencies(std::vector<size_t> & initialDep);
   void getStartIndexInitial(std::vector<size_t> & InitialStartIndex);

   void getEsoVarIndex(size_t & index, fmi2_import_variable_t * variable);
   void getEsoIndexFromFMUIndex(size_t & EsoIndex, size_t & FmuIndex);

   

   //FMILoader(const char* FMUPath,const char* FMUName);
   FMILoader(const std::string FMUPath, double relFmuTolerance);

   ~FMILoader();

   int getNumberOutputVariables();
   int getNumberContinousStates();
   int Get_num_eqns();
   int Get_num_vars();
   int Get_num_pars();
   int Get_num_cond();
   int getNumberDifferentialEquations();
   int getNumberAlgebraicEquations();
   int getNumberNonZeros(); 
   int getNumberInitialNonZeroes();
   void getNumberInitialUnkowns(size_t & initUnkowns);
   void getEsoEquationIndexFromFMUIndex(size_t & esoEquationIndex, size_t & FmuIndex);
   void getEsoIndexFromInitialFMUIndex(size_t & EsoIndex, size_t & FmuIndex);
   void getEsoEquationIndexFromInitialFMUIndex(size_t & esoEquationIndex, size_t & FmuIndex); 
   int getNumberDifferentialNonZeros();
   void getNumberAllVariables(size_t & numberAllVariables);
   void getNumberInputs(size_t & numberAllVariables);
   void getDerivativeValues(const size_t n_diff_var, double *derivatives);

   fmi2_value_reference_t* getAllVariableReferenceList();


   fmi2_real_t getIndependentVariable();

   fmi2_status_t fmistatus;

   fmi2_import_variable_list_t* get_variableList();

   bool IndexVariableIsContinuousState(const size_t & FmuIndex);
   bool IndexVariableIsDerivative(const size_t & FmuIndex);

protected:

  //! Handle for shared object
  // name of Library
  std::string m_FMUName;  
  std::string m_FMUPath;
  std::string m_unzipPath;
  std::string m_FMUFullPath;


  fmi2_callback_functions_t m_callBackFunctions;
  jm_callbacks m_callbacks;
  fmi_import_context_t* m_context;
  fmi_version_enu_t m_version;
  jm_status_enu_t m_status;
  fmi2_import_t* m_fmu;	


  size_t m_numberOutputVariables;
  size_t m_numberInputVariables;
  size_t m_numberEquations;
  size_t m_numberAllVariables;
  size_t m_numberContinousStates;
  size_t m_numberDerivatives;
  size_t m_numberOfEventIndicators;
  size_t m_numberIndependentVariables;
  size_t m_numberInitialUnkowns;
  size_t m_numberInitialNonZeroes;
 

  // dependency member
  size_t *m_startIndexOutput, *m_dependencyOutput, *m_startIndexDiff, *m_dependencyDiff = NULL;
  char* m_factorKindOutput, *m_factorKindDiff = NULL;


  // variable lists
  fmi2_import_variable_list_t* m_outputVariableList;
  fmi2_import_variable_list_t* m_inputVariableList;
  fmi2_import_variable_list_t* m_variableList;
  fmi2_import_variable_list_t* m_stateList;
  fmi2_import_variable_list_t* m_independentVariableList;
  fmi2_import_variable_list_t* m_initialUnkownList;
  fmi2_import_variable_list_t* m_allVariableList;
  fmi2_import_variable_list_t* m_derivativeList;
  fmi2_import_variable_list_t* m_derivativesOutputsList;
  fmi2_import_variable_list_t* m_statesInputsList;

  fmi2_value_reference_t* m_allVariableReferenceList;
  fmi2_value_reference_t* m_inputVariableReferenceList; 
  fmi2_value_reference_t* m_stateReferenceList;
  fmi2_value_reference_t* m_outputVariableReferenceList;
  fmi2_value_reference_t* m_derivativeRefernceList;
  fmi2_value_reference_t* m_derivativesOutputsReferenceList; 
  fmi2_value_reference_t* m_statesInputsReferenceList;
  fmi2_value_reference_t* m_initialUnkownReferenceList;




  // buffer variables

  vector<fmi2_real_t> m_derivatives;
  vector<fmi2_real_t> m_fmuOutput;
  std::vector<size_t> m_initialDependencies;
  std::vector<size_t> m_startIndexInitial;
  std::vector<fmi2_real_t> m_currentDirectionalDerivativeStates;
  std::vector<fmi2_real_t> m_currentDirectionalDerivativeParameters;
  fmi2_import_model_counts_t  m_modelCounts;



  // variable names
  vector<std::string> VariableNames;
  vector<std::string> InputNames;


  static fmi2_value_reference_t* getValueReferenceList(fmi2_import_variable_list_t* referenceList);


  static int VarTypeFilterInput(fmi2_import_variable_t* vl,  void * varType);
  static int VarTypeFilterIndependent(fmi2_import_variable_t* vl,  void * varType);

  fmi2_import_variable_list_t* fmi2_import_get_states(fmi2_import_t* fmu);
 

};

