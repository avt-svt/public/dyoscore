/**
* @file FmiLoader.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Member definitions of class FmiLoader                            \n
* =====================================================================\n
* @author Adrian Caspari
* @date 11.10.2017
*/

#define MAKE_FMILOADER_DLL

#include <DyosFmiLoader.hpp>
#include <iostream>

FMILoader::FMILoader(const std::string FMUPath, double relFmuTolerance) {

	// convert char pointer to string
	m_FMUPath = FMUPath;

	bool useCompressedFmu = false;

	if (m_FMUPath.substr(m_FMUPath.length() - 4) == ".fmu") {

		useCompressedFmu = true;

	}

	m_callbacks.malloc = malloc;
	m_callbacks.calloc = calloc;
	m_callbacks.realloc = realloc;
	m_callbacks.free = free;
	m_callbacks.logger = jm_default_logger;
	m_callbacks.log_level = jm_log_level_nothing;//jm_log_level_debug; //jm_log_level_info; // jm_log_level_warning; //jm_log_level_nothing;
	m_callbacks.context = 0;



	m_context = fmi_import_allocate_context(&m_callbacks);

	m_FMUFullPath = m_FMUPath;
	m_unzipPath = m_FMUPath;

	//unzipping

	if (useCompressedFmu) {

	
				m_unzipPath = m_unzipPath.substr(0, m_unzipPath.size() - 4);

				m_unzipPath.append("_");

				m_unzipPath.append("unzip");


				boost::filesystem::path pathBoost(m_unzipPath);
				boost::filesystem::create_directories(pathBoost);


			m_version = fmi_import_get_fmi_version(m_context, m_FMUFullPath.data(), m_unzipPath.data());

			if (m_version != fmi_version_2_0_enu) {
				throw std::runtime_error("DyOS Fmi Loader : Only fmi version 2.0 is supported by this code. FMU path may be wrong.\n");
				}
	}



	// creating fmu handle
	m_fmu = fmi2_import_parse_xml(m_context, m_unzipPath.data(), NULL);
	if (!m_fmu) {
		throw std::runtime_error("Dyos Fmi Loader: Error parsing XML, exiting!");
	}

	if (fmi2_import_get_fmu_kind(m_fmu) == fmi2_fmu_kind_cs) {
		throw std::runtime_error("Dyos Fmi Loader: Only ME 2.0 is supported by this code!");
	}

	
	m_callBackFunctions.logger = fmi2_log_forwarding;
	m_callBackFunctions.allocateMemory = calloc;
	m_callBackFunctions.freeMemory = free;
	m_callBackFunctions.stepFinished = NULL;
	m_callBackFunctions.componentEnvironment = m_fmu;

	// FMILib loader of model.dll
	m_status = fmi2_import_create_dllfmu(m_fmu, fmi2_fmu_kind_me, NULL);
	// instantiate FMU
	fmi2_string_t instanceName = "ModelInstance";
	m_status = fmi2_import_instantiate(m_fmu, instanceName, fmi2_model_exchange, NULL, fmi2False);

	if (m_status == jm_status_error) {
		throw std::runtime_error("Dyos Fmi Loader: Could not create the DLL loading mechanism(C-API test)!");
	}

	fmi2_boolean_t toleranceDefined = fmi2_true;
	fmi2_boolean_t stopTimeDefined = fmi2_true;
	fmi2_real_t tolerance = relFmuTolerance;
	fmi2_real_t startTime = 0;
	fmi2_real_t stopTime  = 1;

	fmistatus = fmi2_import_setup_experiment(m_fmu, toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
	fmistatus = fmi2_import_enter_initialization_mode(m_fmu);
	fmistatus = fmi2_import_exit_initialization_mode(m_fmu);

	fmi2_import_collect_model_counts(m_fmu, &m_modelCounts);

	// setting fixed fmu attributs
	// list of all variables

	// sort order 0 is obligate due to original modelDesciption.xml 
	// maybe implement a map from variable index to value reference
	int variableListSortOrder = 0;
	m_variableList = fmi2_import_get_variable_list(m_fmu, variableListSortOrder);


	// setting output variables
	m_outputVariableList = fmi2_import_get_outputs_list(m_fmu);
	m_numberOutputVariables = fmi2_import_get_variable_list_size(m_outputVariableList);
	m_outputVariableReferenceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_outputVariableList);



	m_fmuOutput.resize(m_numberOutputVariables); // replaced pointer c array by vector

												 // setting input variables
	fmi2_import_variable_filter_function_ft filter = &(FMILoader::VarTypeFilterInput);
	fmi2_causality_enu_t varTypeInput = fmi2_causality_enu_t::fmi2_causality_enu_input;
	m_inputVariableList = fmi2_import_filter_variables(m_variableList, filter, &varTypeInput);

	m_numberInputVariables = fmi2_import_get_variable_list_size(m_inputVariableList);
	m_inputVariableReferenceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_inputVariableList);
	// error if no input defined. This means that no parameter can be changed!

	if (m_numberInputVariables == 0) {
		throw std::runtime_error("No variable has causality=input in the FMU's model description xml!");

	}

	// setting continous states



	//// lists of variables as members
	m_derivativeList = fmi2_import_get_derivatives_list(m_fmu);
	m_numberDerivatives = fmi2_import_get_variable_list_size(m_derivativeList);
	m_derivativeRefernceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_derivativeList);
	m_stateList = fmi2_import_get_states(m_fmu);
	m_numberContinousStates = fmi2_import_get_variable_list_size(m_stateList);
	m_stateReferenceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_stateList);
	// list of inputs and states
	m_statesInputsList = fmi2_import_join_var_list(m_stateList, m_inputVariableList);
	m_statesInputsReferenceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_statesInputsList);
	// states and outputs needed for partial derivatives
	m_derivativesOutputsList = fmi2_import_join_var_list(m_derivativeList, m_outputVariableList);
	m_derivativesOutputsReferenceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_derivativesOutputsList);
	// independent variable list
	fmi2_import_variable_filter_function_ft filterIndependent = &(FMILoader::VarTypeFilterIndependent);
	fmi2_causality_enu_t varTypeIndependent = fmi2_causality_enu_t::fmi2_causality_enu_independent;
	m_independentVariableList = fmi2_import_filter_variables(m_variableList, filterIndependent, &varTypeIndependent);


	// setting number all Variables and all variable list (all = output + states in fmu = algebraic + differential states in math sense)
	m_numberAllVariables = m_numberContinousStates + m_numberOutputVariables;
	m_allVariableList = fmi2_import_join_var_list(m_stateList, m_outputVariableList);
	m_allVariableReferenceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_allVariableList);



	m_currentDirectionalDerivativeStates.resize(m_numberAllVariables);
	m_currentDirectionalDerivativeParameters.resize(m_numberAllVariables);



	m_derivatives.resize(m_numberDerivatives,0.0);



	// event indicators 
	m_numberOfEventIndicators = fmi2_import_get_number_of_event_indicators(m_fmu);

	// number of independent variables
	m_numberIndependentVariables = m_modelCounts.num_independent;

	// initial variables 

	m_initialUnkownList = fmi2_import_get_initial_unknowns_list(m_fmu);
	m_numberInitialUnkowns = fmi2_import_get_variable_list_size(m_initialUnkownList);
	m_initialUnkownReferenceList = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(m_initialUnkownList);

	
	
	size_t *startIndexInitial = NULL;
	size_t * dependencyInitial = NULL;
	char* factorKindInitial = NULL;
	fmi2_import_get_initial_unknowns_dependencies(m_fmu, &startIndexInitial, &dependencyInitial, &factorKindInitial);

	if (startIndexInitial != NULL) {
		m_numberInitialNonZeroes = startIndexInitial[m_numberInitialUnkowns];




		m_initialDependencies.resize(m_numberInitialNonZeroes);
		m_startIndexInitial.resize(m_numberInitialUnkowns + 1);




		for (int i = 0; i < (int)m_numberInitialUnkowns + 1; i++) {

			m_startIndexInitial[i] = startIndexInitial[i];

		}



		for (int i = 0; i < (int)m_numberInitialNonZeroes; i++) {

			m_initialDependencies[i] = dependencyInitial[i];

		}
	}



	//// dependencies needed for jacobi structure calculation Jsp.
	fmi2_import_get_outputs_dependencies(m_fmu, &m_startIndexOutput, &m_dependencyOutput, &m_factorKindOutput);
	//dependencies of differential equations
	fmi2_import_get_derivatives_dependencies(m_fmu, &m_startIndexDiff, &m_dependencyDiff, &m_factorKindDiff);





	// variable names 
	//VariableNames = new char*[m_numberContinousStates + m_numberContinousStates];

	VariableNames.resize(m_numberContinousStates + m_numberOutputVariables);


	for (int i = 0; i< (int)m_numberContinousStates; i++) {

		VariableNames[i] = (char*)fmi2_import_get_variable_name(fmi2_import_get_variable(m_stateList, i));

	}

	for (int i = 0; i< (int)m_numberOutputVariables; i++) {

		VariableNames[m_numberContinousStates + i] = (char*)fmi2_import_get_variable_name(fmi2_import_get_variable(m_outputVariableList, i));

		if(VariableNames[m_numberContinousStates + i]=="CPUtime" || VariableNames[m_numberContinousStates + i] == "EventCounter")
			throw std::invalid_argument("Please do not use '" + VariableNames[m_numberContinousStates + i] + "' as variable name. Please change the name. \n");

	}


	// parameter names
	//InputNames = new char*[m_numberInputVariables];

	InputNames.resize(m_numberInputVariables);
	for (int i = 0; i< (int)m_numberInputVariables; i++) {

		InputNames[i] = (char*)fmi2_import_get_variable_name(fmi2_import_get_variable(m_inputVariableList, i));

	}





}


 	  FMILoader::~FMILoader(){

		  fmi2_import_terminate(m_fmu);

		  


		  fmi2_import_free_variable_list(m_outputVariableList);
		  fmi2_import_free_variable_list(m_inputVariableList);
		  fmi2_import_free_variable_list(m_variableList);
		  fmi2_import_free_variable_list(m_stateList);
		  fmi2_import_free_variable_list(m_independentVariableList);
		  fmi2_import_free_variable_list(m_initialUnkownList);
		  fmi2_import_free_variable_list(m_allVariableList);
		  fmi2_import_free_variable_list(m_derivativeList);
		  fmi2_import_free_variable_list(m_derivativesOutputsList);
		  fmi2_import_free_variable_list(m_statesInputsList);



		  fmi2_import_free_instance(m_fmu);
		
		fmi2_import_destroy_dllfmu(m_fmu);

		fmi2_import_free(m_fmu);
		fmi_import_free_context(m_context);


		

		  /*m_outputVariableReferenceList
		  fmi2_import_free_variable_list(m_outputVariableReferenceList);
		  free(m_allVariableReferenceList);
		  free(m_inputVariableReferenceList); 
		  free(m_stateReferenceList);
		  free(m_derivativeRefernceList);
		  free(m_derivativesOutputsReferenceList); 
		  free(m_statesInputsReferenceList);
		  free(m_initialUnkownReferenceList);
		  */
		//fmi_import_rmdir(&m_callbacks, m_unzipPath.data());
		
	
	  }








/**
* @brief Filter function for selecting input variables from a variable list vl, 
* is not a member function of FmiLoader
*
* @return returns 1 if variable is input and 0 if not
*/
int FMILoader::VarTypeFilterInput(fmi2_import_variable_t* vl, void * varType){


	if(fmi2_import_get_causality(vl)==fmi2_causality_enu_t::fmi2_causality_enu_input){
		// std::cout << "VarTypeFilterInput = 1" << std::endl;
		return 1;
		
	
	}
	
	else {
		
		// std::cout << "VarTypeFilterInput = 0" << std::endl;
		return 0;
	}

}




/**
* @brief Filter function for selecting independent variable (at most 1) from a variable list vl, 
* is not a member function of FmiLoader
*
* @return returns 1 if variable is input and 0 if not
*/
int FMILoader::VarTypeFilterIndependent(fmi2_import_variable_t* vl, void * varType){


	if(fmi2_import_get_causality(vl)==fmi2_causality_enu_t::fmi2_causality_enu_independent){
		// std::cout << "VarTypeFilterInput = 1" << std::endl;
		return 1;
		
	
	}
	
	else {
		
		// std::cout << "VarTypeFilterInput = 0" << std::endl;
		return 0;
	}

}




/**
* @brief Functions extracts continous states by evaluating the derivative variable reference.
*
* @return returns continous states list
*/
fmi2_import_variable_list_t* FMILoader::fmi2_import_get_states(fmi2_import_t* fmu){



	fmi2_import_variable_t* varDer = fmi2_import_get_variable(m_derivativeList, 0);
	fmi2_import_variable_t* varStateOfDer = (fmi2_import_variable_t*)fmi2_import_get_real_variable_derivative_of((fmi2_import_real_variable_t*)varDer);
	fmi2_import_variable_list_t* stateList;
	stateList=fmi2_import_create_var_list(fmu,varStateOfDer);
	
	
	for(int i = 1; i < (int)m_numberDerivatives; i++) {
            fmi2_import_variable_t* varDer = fmi2_import_get_variable(m_derivativeList, i);
			fmi2_import_variable_t* varStateOfDer = (fmi2_import_variable_t*)fmi2_import_get_real_variable_derivative_of((fmi2_import_real_variable_t*)varDer);
			fmi2_import_var_list_push_back(stateList,varStateOfDer);
	}

	
	return stateList;

	

}



/**
* @brief Getter of number of Continous States.
*
* @return returns number of continous states 
*/
int FMILoader::getNumberContinousStates(){

	return (int) m_numberContinousStates;

}





fmi2_import_variable_list_t* FMILoader::get_variableList(){


	return m_variableList;

}



/**
* @brief Getter for the number of states (differential and algebraic) of the model
*
* @return number of states
*/
int FMILoader::Get_num_vars(){

	return (int) m_numberAllVariables;
}



/**
* @brief Getter for the number of Output Variables of the FMU model (variables defined as output eg in modelica)
*
* @return number of output Variables
*/
	  int FMILoader::getNumberOutputVariables(){
	  
		return (int) m_numberOutputVariables;
	  }



/**
* @brief Getter for the number of Input Variables of the FMU model (variables defined as input eg in modelica)
*
* @return number of input Variables
*/
	  int FMILoader::Get_num_pars(){
	  
		return (int) m_numberInputVariables;

	  }



/**
* @brief Getter for the number of equations (differential and algebraic) of the model
*
* @return number of equations
*/
	  int FMILoader::Get_num_eqns(){
	  
	  
	  
		  return (int) m_numberContinousStates + m_numberOutputVariables;
	  
	  
	  }


/**
* @brief Getter for the number of differential equations of the model
*
* @return number of differential equations
*/
	  int FMILoader::getNumberDifferentialEquations(){
	  
	  
	  
		  return (int) m_numberContinousStates;
	  
	  
	  }
	  
/**
* @brief Getter for the number of algebraic equations of the model
*
* @return number of algebraic equations
*/
	  int FMILoader::getNumberAlgebraicEquations(){
	  
	  
		return (int) m_numberOutputVariables;
	  
	  
	  }
	  
/**
* @brief Get number of non zero Jacobian entries of the model
*
* @return number of non zero Jacobian entries
*/
int FMILoader::getNumberNonZeros(){

	size_t nvOut;
	nvOut = fmi2_import_get_variable_list_size(m_outputVariableList);


	size_t numberNonZeroDerivativeDependencies;
	numberNonZeroDerivativeDependencies = m_startIndexDiff[m_numberDerivatives];

	size_t numberOutputDependencies;
	numberOutputDependencies = m_startIndexOutput[nvOut];


	return (int) numberOutputDependencies + numberNonZeroDerivativeDependencies;		
	  }

/**
* @brief Get number of non zero Jacobian entries of the model
*
* @return number of non zero Jacobian entries
*/
int FMILoader::getNumberInitialNonZeroes(){


	return m_numberInitialNonZeroes;

	  
}



	  
/**
* @brief Get number of non zero Jacobian entries of the model
*
* @return number of non zero Jacobian entries
*/
int FMILoader::getNumberDifferentialNonZeros(){



	return (int)m_numberContinousStates;
	  
	  
	  }

/**
* @brief Get number of conditional expressions (events) 
*
* @return number of conditional expressions
*/
int FMILoader::Get_num_cond(){
	//numberOfEventIndicators
	return m_numberOfEventIndicators;

}

/**
* @brief Get list of variable references
*
* @return list of variable references
*/
fmi2_value_reference_t* FMILoader::getAllVariableReferenceList(){
	return m_allVariableReferenceList;
}




fmi2_value_reference_t* FMILoader::getValueReferenceList(fmi2_import_variable_list_t* variableList){

	const size_t sizeList=  fmi2_import_get_variable_list_size(variableList);
	fmi2_value_reference_t* referenceList = (fmi2_value_reference_t*)fmi2_import_clone_variable_list(variableList);

	for (int i = 0; i < (int)sizeList; i++){
	

		fmi2_import_variable_t* variable = fmi2_import_get_variable(variableList, i);
		referenceList[i] = fmi2_import_get_variable_vr(variable);
	
	
	}


	return referenceList;

}


/**
* @brief Get independent variable (if there is one)
* @return value of independent variable
*/
fmi2_real_t FMILoader::getIndependentVariable(){


	fmi2_real_t* value = nullptr;
	fmi2_import_get_real(m_fmu, fmi2_import_get_value_referece_list(m_independentVariableList),m_numberIndependentVariables,value);

	return *value;

}



/**
* @brief Getter for the names of the states (differential and algebraic)
*
* @param[out] names of all differential and algebraic states
*/
  void FMILoader::GetVarNames(std::vector<std::string> * names){
		
		names->clear();
		
	  for (int i=0; i <( int)m_numberAllVariables; i++){
		   
		  names->push_back(VariableNames[i]); 
		  //(*names)[i] = VariableNames[i];

	  }

  }


  /**
* @brief Getter for the names of the all variables (differential and algebraic states and parameters (=inputs))
*
* @param[out] names of all differential and algebraic states
*/
  void FMILoader::GetAllVarNames(std::vector<std::string> * names){
		
		names->clear();
		
	  for (int i=0; i <( int)m_numberAllVariables; i++){
		   
		  names->push_back(VariableNames[i]); 
		  //(*names)[i] = VariableNames[i];

	  }

	  	  for (int i=0; i <( int)m_numberInputVariables; i++){
		   
		  names->push_back(InputNames[i]); 
		  //(*names)[i] = InputNames[i];

	  }

  }



/**
* @brief Getter for the names of the parameters (input variables)
*
* @param[out] names of all parameters (input variables)
*/
  void FMILoader::GetParNames(std::vector<std::string> * names){


	  names->clear();
	

	  for (int i=0; i <( int)m_numberInputVariables; i++){
		   
		  names->push_back(InputNames[i]); 
		  //(*names)[i] = InputNames[i];

	  }

  }



/**
* @brief Initializes all states and parameters
*
* The initial values are defined in the .fmu-file or if not, default values (0) are given.
*
* @param[out] x initial values of the states of size n_x
* @param[out] p initial values of the parameters of size n_p
* @param[out] n_x size of the states x
* @param[out] n_p size of the parameters p
*/
void FMILoader::Init(double * x, double * p, int &n_x, int& n_p, int& n_c){


fmistatus = fmi2_import_enter_initialization_mode(m_fmu);



fmistatus = fmi2_import_get_real(m_fmu, m_allVariableReferenceList, m_numberAllVariables, x);
fmistatus = fmi2_import_get_real(m_fmu, m_inputVariableReferenceList, m_numberInputVariables, p);


fmistatus = fmi2_import_exit_initialization_mode(m_fmu);

n_x = m_numberAllVariables;
n_p = m_numberInputVariables;
n_c = m_numberOfEventIndicators;


  }









/**
* @brief Evaluates the residual of the whole equation system
*
* The function evaluates Res(x,p,der_x) = M*der_x - f(x,p).
*
* @param[out] yy of size Get_num_eqns() contains the residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void FMILoader::Res(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c){

	// conditions (events in FMU) are currently not implemented


	// set parameters p to FMU
	fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p); 
	// set all states x to FMU 
	fmistatus = fmi2_import_set_real(m_fmu, m_stateReferenceList, m_numberContinousStates , x); 
	// get right hand side of ODE (derivatives of states)
	fmistatus = fmi2_import_get_derivatives(m_fmu, m_derivatives.data(), m_numberContinousStates);
	// get ouput values
	fmistatus = fmi2_import_get_real(m_fmu, m_outputVariableReferenceList, m_numberOutputVariables , m_fmuOutput.data()); 

	
	
	// res_differential = der_x - FMUrhs; 

	for (int i=0; i <( int)m_numberContinousStates; i++) {
		yy[i] = der_x[i] - m_derivatives[i]; 
	}


	// res_alg = xalg - yFMU;

	for (int i=0; i <( int)m_numberOutputVariables; i++) {
		yy[i+m_numberContinousStates] = x[i+m_numberContinousStates] - m_fmuOutput[i]; 
	}

	
}





/**
* @brief Evaluates the algebraic residual of the whole equation system
*
* The function evaluates Res(x,p,der_x) = M*der_x - f(x,p).
*
* @param[out] yy of size of number algebraic equations (=algebraic variables) contains the algebraic residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void FMILoader::ResAlgebraic(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c) {

	// conditions (events in FMU) are currently not implemented


	// set parameters p to FMU
	fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p);
	// set all states x to FMU 
	fmistatus = fmi2_import_set_real(m_fmu, m_stateReferenceList, m_numberContinousStates, x);

	// get ouput values
	fmistatus = fmi2_import_get_real(m_fmu, m_outputVariableReferenceList, m_numberOutputVariables, m_fmuOutput.data());

	// res_alg = xalg - yFMU;

	for (int i = 0; i <(int)m_numberOutputVariables; i++) {
		yy[i] = x[i + m_numberContinousStates] - m_fmuOutput[i];
	}


}

/**
* @brief Evaluates the algebraic equations of the whole equation system
*
* The function evaluates y = g(x,p). This is required in DyOS for DAE initialization.
*
* @param[out] y of size of number algebraic equations (=algebraic variables) contains the algebraic residuals of the equation system
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
* @param[in] n_y size of the algebraic varialbes y
*/
void FMILoader::getAlgebraicVariables(double* y, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_y, int& n_c) {

	// conditions (events in FMU) are currently not implemented


	// set parameters p to FMU
	fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p);
	// set all states x to FMU 
	fmistatus = fmi2_import_set_real(m_fmu, m_stateReferenceList, m_numberContinousStates, x);

	// get ouput values
	fmistatus = fmi2_import_get_real(m_fmu, m_outputVariableReferenceList, m_numberOutputVariables, m_fmuOutput.data());

	// res_alg = xalg - yFMU;

	for (int i = 0; i <(int)n_y; i++) {
		y[i] =  m_fmuOutput[i];
	}


}



/**
* @brief Evaluates the derivative equations of the whole equation system
*
* The function evaluates x' = f(x,p). This is required in DyOS for DAE initialization.
*
* @param[out] x' of size of number differential equations (=algebraic variables) contains the algebraic residuals of the equation system
* @param[in] x of size n_x contains the values of the differential states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the differnetial states x
* @param[in] n_p size of the parameters p
* @param[in] n_c number conditions
*/
void FMILoader::getDerivativeVariables(double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c) {

	// conditions (events in FMU) are currently not implemented


	// set parameters p to FMU
	fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p);
	// set all states x to FMU 
	fmistatus = fmi2_import_set_real(m_fmu, m_stateReferenceList, m_numberContinousStates, x);

	// get differential values
	fmistatus = fmi2_import_get_derivatives(m_fmu, m_derivatives.data(), m_numberContinousStates);

	// res_alg = xalg - yFMU;

	for (int i = 0; i <(int)n_x; i++) {
		der_x[i] = m_derivatives[i];
	}


}



/**
* @brief Evaluates the differential residual of the whole equation system
*
* The function evaluates Res(x,p,der_x) = M*der_x - f(x,p).
*
* @param[out] yy of size of number differential equations (=differential variables) contains the differential residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void FMILoader::ResDifferential(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c) {

	// set parameters p to FMU
	fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p);
	// set all states x to FMU 
	fmistatus = fmi2_import_set_real(m_fmu, m_stateReferenceList, m_numberContinousStates, x);
	// get right hand side of ODE (derivatives of states)
	fmistatus = fmi2_import_get_derivatives(m_fmu, m_derivatives.data(), m_numberContinousStates);
	



	// res_differential = der_x - FMUrhs; 

	for (int i = 0; i <(int)m_numberContinousStates; i++) {
		yy[i] = der_x[i] - m_derivatives[i];
	}

//#pragma loop(hint_parallel(8)) 
//	for (int i = 0; i <(int)m_numberOutputVariables; i++) {
//		yy[m_numberContinousStates+i] = der_x[m_numberContinousStates+i] - 0;
//	}


}


/**
* @brief Block-wise evaluation of the residuals
*
* The function evaluates blocks of the residual Res(x,p,der_x).
*
* @param[out] yy of size n_yy_ind contains the residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
* @param[in] n_yy_ind size of residuals yy and indices yy_ind
* @param[in] yy_ind indices of residuals to be evaluated
*/
void FMILoader::Res_block(double * yy, double * der_x, double * x, double * p,
		 int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
                 int &n_yy_ind, int* yy_ind){


					 
						// set parameters p to FMU
						fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p); 
						// set all states x to FMU 
						fmistatus = fmi2_import_set_real(m_fmu, m_stateReferenceList, m_numberContinousStates , x); 

					
						

						fmi2_real_t value = 0;
				
					 for(int i=0; i<( int)n_yy_ind; i++){

						 

						 if(yy_ind[i] < (int) m_numberContinousStates){



						     // get the reals of a sublist of the fmu RHS

							 fmi2_import_variable_list_t* derivativeSublist = fmi2_import_get_sublist(m_derivativeList, yy_ind[i], yy_ind[i]);
							 fmi2_value_reference_t* derivativeReferenceSublist = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(derivativeSublist);

							 fmistatus = fmi2_import_get_real(m_fmu, derivativeReferenceSublist
								 , 1 , &value);
								 
							 yy[i] = der_x[yy_ind[i]] - value;
						
							 fmi2_import_free_variable_list(derivativeSublist);

						 }
						 else {
							 
							 // output variables
							 // get some output variables according to desired outputs

							 fmi2_import_variable_list_t* outputVariableSublist = fmi2_import_get_sublist(m_outputVariableList, yy_ind[i] - m_numberContinousStates, yy_ind[i] - m_numberContinousStates);
							 fmi2_value_reference_t* outputVariableReferenceSublist = (fmi2_value_reference_t*)fmi2_import_get_value_referece_list(outputVariableSublist);
						
							 fmistatus = fmi2_import_get_real(m_fmu, outputVariableReferenceSublist
								 , 1 , &value);
								
							 yy[i] = x[yy_ind[i]] - value;
							 fmi2_import_free_variable_list(outputVariableSublist);
						
						 }

					 }
				
  }


/**
* @brief Evaluation of the first-order tangent-linear derivatives of the residuals
*
* The function calls the first-order tangent-linear derivative code generated for the
* residuals Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size 'Get_num_eqns()'.
*
* output definition: t1_yy = d(Res(z))/dz * t1_z
*
* @param[out] yy contains the residuals of the equation system
* @param[out] t1_yy contains the derivatives of the residuals of the equation system
* @param[in] der_x contains the values of der_x
* @param[in] t1_der_x seed vector multiplying the Jacobian part relative to the derivated
*            states
* @param[in] x contains the values of the states
* @param[in] t1_x seed vector multiplying the Jacobian part relative to the states
* @param[in] p contains the values of the parameters
* @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters pinstant

*/
void FMILoader::T1_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
              double* t1_x, double* p, double* t1_p,
	      int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c){
    // evaluate residual 
	Res(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c);

	// der_x = f(x,p), differntial equation
	//     y = g(x,p), output equation
	// t1yy = d(res)/(d der_x) t1_der_x + d(y)/d(y) t1_y - d(f,g)/d(x) t1_x - d(f,g)/d(p) t1_p 
	//
	// directional derivatives
	
	// states
	//fmi2_real_t* directionalDerivativeStates = new fmi2_real_t [n_x] ; 
	//std::vector<fmi2_real_t> directionalDerivativeStates (m_numberAllVariables);
	
 
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//fmilib has swapped the order in inputs to the get_directional_derivative!
	// fmi2GetDirectionalDerivative(fmu -> c, z_ref, nz, v_ref, nv, dv, dz)

	fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_stateReferenceList, m_numberContinousStates,
		m_derivativesOutputsReferenceList, m_numberAllVariables,
		 t1_x, m_currentDirectionalDerivativeStates.data());
	
	/*
	for(int i=0; i<( int)m_numberAllVariables; i++){

		t1_yy[i] = 0-directionalDerivativeStates[i]; 

	}
	*/
	

	// parameters (d res/ d p)
    //fmi2_real_t* directionalDerivativeParameters = new fmi2_real_t [n_p] ; 
	//std::vector<fmi2_real_t> directionalDerivativeParameters (m_numberAllVariables);


	//fmilib has swapped the order in inputs to the get_directional_derivative!
    fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_inputVariableReferenceList, m_numberInputVariables,
		m_derivativesOutputsReferenceList, m_numberAllVariables, t1_p, m_currentDirectionalDerivativeParameters.data());
	
	/*
	for(int i=0; i<( int)m_numberAllVariables; i++){

		t1_yy[i] -= directionalDerivativeParameters[i]; 



	}
	*/

	// derivated states (d res/ d der_x) = d (der_x - f(x) ) / d der_x = unity
	// assuming der_x = f(x) and NOT M*der_x = f(x), as this form is reducibly to the ODE form
	// should be the unity matrix 

	for(int i=0; i<( int)m_numberAllVariables; i++){
		t1_yy[i] = 0- m_currentDirectionalDerivativeStates[i] - m_currentDirectionalDerivativeParameters[i];
		
		/*
		if (i<(int) m_numberContinousStates)
			t1_yy[i] += t1_der_x[i];  // may be unsafe
		else
			t1_yy[i] += t1_x[i]; 

			*/
	}


	for (int i = 0; i < (int)m_numberContinousStates; i++) {
		t1_yy[i] += t1_der_x[i];  // may be unsafe
	}
	

	for (int i = 0; i < (int)m_numberAllVariables-m_numberContinousStates; i++) {
		t1_yy[m_numberContinousStates+i] += t1_x[m_numberContinousStates+i];  // may be unsafe
	}
	


    // outputs

	/*
	for(int i=0; i<( int)m_numberOutputVariables; i++) {
		t1_yy[i+m_numberContinousStates] += t1_x[i+m_numberContinousStates]; 
	}
	*/


		
}

/**
* @brief Evaluation of the first-order tangent-linear derivatives of the residuals
*
* The function calls the first-order tangent-linear derivative code generated for the
* residuals Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size 'Get_num_eqns()'.
*
* output definition: t1_yy = d(Res(z))/dz * t1_z
*
* @param[out] t1_yy contains the derivatives of the residuals of the equation system
* @param[in] der_x contains the values of der_x
* @param[in] t1_der_x seed vector multiplying the Jacobian part relative to the derivated
*            states
* @param[in] x contains the values of the states
* @param[in] t1_x seed vector multiplying the Jacobian part relative to the states
* @param[in] p contains the values of the parameters
* @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters pinstant

*/

void FMILoader::T1_res_without_res(double* t1_yy, double* der_x, double* t1_der_x, double* x,
              double* t1_x, double* p, double* t1_p,
	      int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c){

	// der_x = f(x,p), differntial equation
	//     y = g(x,p), output equation
	// t1yy = d(res)/(d der_x) t1_der_x + d(y)/d(y) t1_y - d(f,g)/d(x) t1_x - d(f,g)/d(p) t1_p 
	//
	// directional derivatives
	
 
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//fmilib has swapped the order in inputs to the get_directional_derivative!
	// fmi2GetDirectionalDerivative(fmu -> c, z_ref, nz, v_ref, nv, dv, dz)

	// set parameters p to FMU
	fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p);
	// set all states x to FMU 
	fmistatus = fmi2_import_set_real(m_fmu, m_stateReferenceList, m_numberContinousStates, x);
	// get right hand side of ODE (derivatives of states)
	//fmistatus = fmi2_import_get_derivatives(m_fmu, m_derivatives.data(), m_numberContinousStates);
	// get ouput values
	//fmistatus = fmi2_import_get_real(m_fmu, m_outputVariableReferenceList, m_numberOutputVariables, m_fmuOutput.data());

	fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_stateReferenceList, m_numberContinousStates,
		m_derivativesOutputsReferenceList, m_numberAllVariables,
		 t1_x, m_currentDirectionalDerivativeStates.data());

	//fmilib has swapped the order in inputs to the get_directional_derivative!
    fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_inputVariableReferenceList, m_numberInputVariables,
		m_derivativesOutputsReferenceList, m_numberAllVariables, t1_p, m_currentDirectionalDerivativeParameters.data());
	

	// derivated states (d res/ d der_x) = d (der_x - f(x) ) / d der_x = unity
	// assuming der_x = f(x) and NOT M*der_x = f(x), as this form is reducibly to the ODE form
	// should be the unity matrix 

	for(int i=0; i<( int)m_numberAllVariables; i++){
		t1_yy[i] = 0- m_currentDirectionalDerivativeStates[i] - m_currentDirectionalDerivativeParameters[i];

	}


	for (int i = 0; i < (int)m_numberContinousStates; i++) {
		t1_yy[i] += t1_der_x[i];  // may be unsafe
	}
	

	for (int i = 0; i < (int)m_numberAllVariables-m_numberContinousStates; i++) {
		t1_yy[m_numberContinousStates+i] += t1_x[m_numberContinousStates+i];  // may be unsafe
	}
	

		
}


/**
* @brief Evaluation of the first-order tangent-linear derivatives of the residuals
*
* The function calls the first-order tangent-linear derivative code generated by dcc for the
* residuals Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size 'Get_num_eqns()'.
*
* output definition: t1_yy = d(Res(z))/dz * t1_z
*
* @param[out] yy contains the residuals of the equation system
* @param[out] t1_yy contains the derivatives of the residuals of the equation system
* @param[out] x contains the values of the states
* @param[in] der_x contains the values of der_x
* @param[in] t1_der_x seed vector multiplying the Jacobian part relative to the derivated
*            states
* @param[in] t1_x seed vector multiplying the Jacobian part relative to the states
* @param[in] p contains the values of the parameters
* @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void FMILoader::T1_res_initial(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
	double* t1_x, double* p, double* t1_p,
	int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c) {

	fmistatus = fmi2_import_enter_initialization_mode(m_fmu);
	

	
	//set parameters p to FMU
	fmistatus = fmi2_import_set_real(m_fmu, m_inputVariableReferenceList, n_p, p);

	// get initial values of states (differential + algebraic)
	fmistatus = fmi2_import_get_real(m_fmu,m_allVariableReferenceList,m_numberAllVariables,x);

	fmistatus = fmi2_import_exit_initialization_mode(m_fmu);

	// evaluate residual 
	Res(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c);

	// der_x = f(x,p), differntial equation
	//     y = g(x,p), output equation
	// t1yy = d(res)/(d der_x) t1_der_x + d(y)/d(y) t1_y - d(f,g)/d(x) t1_x - d(f,g)/d(p) t1_p 
	//
	// directional derivatives
	// states
	//fmi2_real_t* directionalDerivativeStates = new fmi2_real_t [n_x] ; 
	//std::vector<fmi2_real_t> directionalDerivativeStates(m_numberAllVariables);

	
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//fmilib has swapped the order in inputs to the get_directional_derivative!
	/*fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_stateReferenceList, m_numberContinousStates,
		m_derivativesOutputsReferenceList, m_numberAllVariables,
		t1_x, directionalDerivativeStates.data());*/
		// start initialization mode

		// start initialization mode

//	fmistatus = fmi2_import_enter_initialization_mode(m_fmu);
	fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_stateReferenceList, m_numberContinousStates,
		m_derivativesOutputsReferenceList, m_numberAllVariables,
		t1_x, m_currentDirectionalDerivativeStates.data());

	
	/*
	for (int i = 0; i<(int)m_numberAllVariables; i++) {

		t1_yy[i] = -directionalDerivativeStates[i];

	}*/

	// parameters (d res/ d p)
	//fmi2_real_t* directionalDerivativeParameters = new fmi2_real_t [n_p] ; 
	//std::vector<fmi2_real_t> directionalDerivativeParameters(m_numberAllVariables);
	

	//fmilib has swapped the order in inputs to the get_directional_derivative!
	fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_inputVariableReferenceList, m_numberInputVariables,
		m_derivativesOutputsReferenceList, m_numberAllVariables, t1_p, m_currentDirectionalDerivativeParameters.data());
	
	/*
	for (int i = 0; i<(int)m_numberAllVariables; i++) {
		t1_yy[i] -= directionalDerivativeParameters[i];
	}*/
	// derivated states (d res/ d der_x) = d (der_x - f(x) ) / d der_x = unity
	// assuming der_x = f(x) and NOT M*der_x = f(x), as this form is reducibly to the ODE form
	// should be the unity matrix 
	// and output
	for (int i = 0; i<(int)m_numberAllVariables; i++) {
		t1_yy[i] = -m_currentDirectionalDerivativeStates[i] - m_currentDirectionalDerivativeParameters[i];

		/*
		if(i<(int) m_numberContinousStates)
			t1_yy[i] += t1_der_x[i];  // may be unsafe
		else
			t1_yy[i] += t1_x[i];
			*/
	}
	

	for (int i = 0; i < (int)m_numberContinousStates; i++) {
		t1_yy[i] += t1_der_x[i];  // may be unsafe
	}

	for (int i = 0; i < (int)m_numberAllVariables - m_numberContinousStates; i++) {
		t1_yy[m_numberContinousStates + i] += t1_x[m_numberContinousStates + i];  // may be unsafe
	}

	// outputs 
	/*
	for (int i = 0; i<(int)m_numberOutputVariables; i++) {
		t1_yy[i + m_numberContinousStates] += t1_x[i + m_numberContinousStates];
	}
	*/

	// exit initialization mode
	//fmistatus = fmi2_import_exit_initialization_mode(m_fmu);
	
}

/**
* @brief Evaluates the sparsity pattern of the residual of the whole equation system with data type jsp
*
* The function evaluates sparsity pattern of Res(x,p,der_x) = M*der_x - f(x,p).
*
* @param[out] yy of size Get_num_eqns() contains the sparsity pattern residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/

void FMILoader::Jsp_res(jsp* yy, jsp* der_x, jsp* x, jsp* p, int* condit, int* lock, int* prev,
	int& n_x, int& n_p, int& n_c) {

	// clear all dependencies
	for (int i = 0; i < Get_num_eqns(); i++) {
		yy[i] = 0;
	}

	
	int j = 0; // index for dependencies
	// loop over all differential equation
	for (int i = 1; i < (int)m_numberContinousStates+1; i++) {
		// loop over all dependencies in each equation
		// loop only if there is a dependency in the differential equations
		if (m_startIndexDiff != 0) {
			for (j; j < (int)m_startIndexDiff[i]; j++) {
				// dependencies might also be a parameter!!!
				// we need to get the state/parameter from the index of the variable
				//printf("dependency: %i \n", dependency[j]); 
				//printf("causality: %i \n", fmi2_import_get_causality(fmi2_import_get_variable(m_variableList, dependency[j]-1)));
				if (fmi2_import_get_causality(fmi2_import_get_variable(m_variableList, m_dependencyDiff[j] - 1)) == fmi2_causality_enu_t::fmi2_causality_enu_input) {
					// variable is input

					// compare reference value of current dependency value reference to all value references in variable input list
					// if identical references appear, add respective input to yy 
					for (int indexInput = 0; indexInput < (int)m_numberInputVariables; indexInput++) {

						if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_inputVariableList, indexInput)) == fmi2_import_get_variable_vr(fmi2_import_get_variable(m_variableList, m_dependencyDiff[j] - 1))) {

							yy[i - 1] = yy[i - 1] - p[indexInput];
							break;

						}

					}
				}
				else { //state

					for (int indexState = 0; indexState < (int)m_numberContinousStates; indexState++) {
						//printf("Ref1: %i \n", fmi2_import_get_variable_vr(fmi2_import_get_variable(m_variableList, dependency[j]-1)));
						//printf("Ref2: %i \n", fmi2_import_get_variable_vr(fmi2_import_get_variable(m_stateList, indexState)));
						if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_stateList, indexState)) == fmi2_import_get_variable_vr(fmi2_import_get_variable(m_variableList, m_dependencyDiff[j] - 1))) {

							yy[i - 1] = yy[i - 1] - x[indexState];
							break;
						}
					}
				}

			}
		}
		// residuum depends also on the derivative
		yy[i - 1] = yy[i - 1] + der_x[i - 1];
	}//end for differential equations
	


	
	j = 0; // index for dependencies starting at index 0
	// loop over output equations
	for (int i = 1; i < (int)m_numberOutputVariables + 1; i++) {
		// loop over all dependencies in each equation
		for (j; j < (int)m_startIndexOutput[i]; j++) {
			// dependencies might also be a parameter!!!
			// we need to get the state/parameter from the index of the variable

		
			if (fmi2_import_get_causality(fmi2_import_get_variable(m_variableList, m_dependencyOutput[j]-1)) == fmi2_causality_enu_t::fmi2_causality_enu_input) {
				// variable is input

				// compare reference value of current dependency value reference to all value references in variable input list
				// if identical references appear, add respective input to yy 
				for (int indexInput = 0; indexInput < (int) m_numberInputVariables; indexInput++) {

					if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_inputVariableList, indexInput)) == fmi2_import_get_variable_vr(fmi2_import_get_variable(m_variableList, m_dependencyOutput[j]-1))) {

						yy[i - 1 + m_numberContinousStates] = yy[i - 1 + m_numberContinousStates] - p[indexInput];
						break;

					}

				}
			}
			else { //state

				for (int indexState = 0; indexState < (int)m_numberContinousStates; indexState++) {

					if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_stateList, indexState)) == fmi2_import_get_variable_vr(fmi2_import_get_variable(m_variableList, m_dependencyOutput[j]-1))) {
						yy[i - 1 + m_numberContinousStates] = yy[i - 1 + m_numberContinousStates] - x[indexState];
						break;
					}
				}
			}// if dependency is a state or input
			// output residuum depends on output variable itself
			yy[i - 1 + m_numberContinousStates] = yy[i - 1 + m_numberContinousStates] + x[i - 1 + m_numberContinousStates];
		}
	}//end for output equations





}


void FMILoader::Res_cond(double* yy, double* der_x, double* x, double* p,
	int &n_x, int &n_p, int &n_c) {
	//nothing happens here
	for (int i = 0; i < n_c;i++) {
		yy[i] = 0;
	}
	
}

void FMILoader::Eval_cond(int* yy, double* der_x, double* x, double* p,
	int &n_x, int &n_p, int &n_c) {
	//nothing happens here
}



/**
 * @brief Calculates first-order tangent-linear derivatives of parts of the DA-System.
 *
 * This functions calls the code generated by dcc calculating first-order forward derivatives.
 * The actual values of the states, the parameters and the derivated states are taken from the
 * members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *
 * output definition: t1_residuals = dF(z)/dz * t1_z
 *
 * @param[in] n_yy_ind number of equations whose derivatives are calculated
 * @param[in] yy_ind indices of equations whose derivatives are calculated
 * @param[in] t1_states seed vector multiplying the Jacobian part relative to the states
 * @param[in] t1_der_states seed vector multiplying the Jacobian part relative to the derivated
 *            states
 * @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
 * @param[out] residuals residuum values of selected equations
 * @param[out] t1_residuals product of the jacobian matrix according to the selected equations and
 *             the seed vectors
 */
void FMILoader::T1_res_block(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
	double* t1_x, double* p, double* t1_p,
	int* condit, int* lock, int* prev, int& n_x, int& n_p, int &n_c,
	int& n_yy_ind, int* yy_ind) {


		// may be implemented more efficiently, by taking only the required states and equations. Therefore, new reference lists must be created.

		// evaluate residual 
	Res_block(yy, der_x, x, p, condit, lock,  prev, n_x, n_p, n_c, n_yy_ind, yy_ind);

	// der_x = f(x,p), differntial equation
	//     y = g(x,p), output equation
	// t1yy = d(res)/(d der_x) t1_der_x + d(y)/d(y) t1_y - d(f,g)/d(x) t1_x - d(f,g)/d(p) t1_p 
	//
	// directional derivatives
	
	// states


	// select the correct fmi variable list corresponding to the equatioin indices


	fmi2_import_variable_list_t* variableListStatesTemp;	

	variableListStatesTemp = fmi2_import_create_var_list(m_fmu, fmi2_import_get_variable(m_derivativesOutputsList,yy_ind[0]));


	// discreminate between differential state and output

	for (int i=1; i<(int)n_yy_ind; i++){

		fmi2_import_var_list_push_back(variableListStatesTemp, fmi2_import_get_variable(m_derivativesOutputsList,yy_ind[i]));
	}


	const fmi2_value_reference_t* variableListStatesTempReferenceList = fmi2_import_get_value_referece_list(variableListStatesTemp);
	


	std::vector<fmi2_real_t> directionalDerivativeStates (n_yy_ind);
 
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//fmilib has swapped the order in inputs to the get_directional_derivative!
	// d res/d x

	fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_stateReferenceList, m_numberContinousStates,
			variableListStatesTempReferenceList, n_yy_ind, t1_x, directionalDerivativeStates.data()); 	
	
	for(int i=0; i<( int)n_yy_ind; i++){

		t1_yy[i] = -directionalDerivativeStates[i]; 

	}


	// parameters (d res/ d p)
    //fmi2_real_t* directionalDerivativeParameters = new fmi2_real_t [n_p] ; 




	std::vector<fmi2_real_t> directionalDerivativeParameters (n_yy_ind);

	//fmilib has swapped the order in inputs to the get_directional_derivative!

		fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_inputVariableReferenceList, m_numberInputVariables,
			variableListStatesTempReferenceList, n_yy_ind, t1_p, directionalDerivativeParameters.data()); 
	
	for(int i=0; i<( int)n_yy_ind; i++){

		t1_yy[i] -= directionalDerivativeParameters[i]; 



	}
	// derivated states (d res/ d der_x) = d (der_x - f(x) ) / d der_x = unity
	// assuming der_x = f(x) and NOT M*der_x = f(x), as this form is reducibly to the ODE form
	// should be the unity matrix 
	// and output

	for(int i=0; i<( int)n_yy_ind; i++){
		if (yy_ind[i]<(int) m_numberContinousStates)
			t1_yy[i] += t1_der_x[yy_ind[i]];  // may be unsafe
		else 
			t1_yy[i] += t1_x[yy_ind[i]]; 
	}



	fmi2_import_free_variable_list(variableListStatesTemp);


}


/**
* @brief Calculates first-order tangent-linear derivatives of parts of the DA-System.
*
* This functions calls the code generated by dcc calculating first-order forward derivatives.
* The actual values of the states, the parameters and the derivated states are taken from the
* members m_states, m_parametersACS and m_der_states.
* For explanation: z = [states p der_states]^T
*                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
*                  all arrays with suffix '_p' are of size (number of parameters)
*                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
*
* output definition: t1_residuals = dF(z)/dz * t1_z
*
* @param[in] n_yy_ind number of equations whose derivatives are calculated
* @param[in] yy_ind indices of equations whose derivatives are calculated
* @param[in] t1_states seed vector multiplying the Jacobian part relative to the states
* @param[in] t1_der_states seed vector multiplying the Jacobian part relative to the derivated
*            states
* @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
* @param[out] residuals residuum values of selected equations
* @param[out] t1_residuals product of the jacobian matrix according to the selected equations and
*             the seed vectors
*/
void FMILoader::T1_res_block_without_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
	double* t1_x, double* p, double* t1_p,
	int* condit, int* lock, int* prev, int& n_x, int& n_p, int &n_c,
	int& n_yy_ind, int* yy_ind) {


	// may be implemented more efficiently, by taking only the required states and equations. Therefore, new reference lists must be created.


	// der_x = f(x,p), differntial equation
	//     y = g(x,p), output equation
	// t1yy = d(res)/(d der_x) t1_der_x + d(y)/d(y) t1_y - d(f,g)/d(x) t1_x - d(f,g)/d(p) t1_p 
	//
	// directional derivatives

	// states


	// select the correct fmi variable list corresponding to the equatioin indices


	fmi2_import_variable_list_t* variableListStatesTemp;

	variableListStatesTemp = fmi2_import_create_var_list(m_fmu, fmi2_import_get_variable(m_derivativesOutputsList, yy_ind[0]));


	// discreminate between differential state and output

	for (int i = 1; i<(int)n_yy_ind; i++) {

		fmi2_import_var_list_push_back(variableListStatesTemp, fmi2_import_get_variable(m_derivativesOutputsList, yy_ind[i]));
	}


	const fmi2_value_reference_t* variableListStatesTempReferenceList = fmi2_import_get_value_referece_list(variableListStatesTemp);



	std::vector<fmi2_real_t> directionalDerivativeStates(n_yy_ind);

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//fmilib has swapped the order in inputs to the get_directional_derivative!
	// d res/d x

	fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_stateReferenceList, m_numberContinousStates,
		variableListStatesTempReferenceList, n_yy_ind, t1_x, directionalDerivativeStates.data());

	for (int i = 0; i<(int)n_yy_ind; i++) {

		t1_yy[i] = -directionalDerivativeStates[i];

	}


	// parameters (d res/ d p)
	//fmi2_real_t* directionalDerivativeParameters = new fmi2_real_t [n_p] ; 




	std::vector<fmi2_real_t> directionalDerivativeParameters(n_yy_ind);

	//fmilib has swapped the order in inputs to the get_directional_derivative!

	fmistatus = fmi2_import_get_directional_derivative(m_fmu, m_inputVariableReferenceList, m_numberInputVariables,
		variableListStatesTempReferenceList, n_yy_ind, t1_p, directionalDerivativeParameters.data());

	for (int i = 0; i<(int)n_yy_ind; i++) {

		t1_yy[i] -= directionalDerivativeParameters[i];



	}
	// derivated states (d res/ d der_x) = d (der_x - f(x) ) / d der_x = unity
	// assuming der_x = f(x) and NOT M*der_x = f(x), as this form is reducibly to the ODE form
	// should be the unity matrix 
	// and output

	for (int i = 0; i<(int)n_yy_ind; i++) {
		if (yy_ind[i]<(int)m_numberContinousStates)
			t1_yy[i] += t1_der_x[yy_ind[i]];  // may be unsafe
		else
			t1_yy[i] += t1_x[yy_ind[i]];
	}



	fmi2_import_free_variable_list(variableListStatesTemp);


}

/**
* @brief getter for Initial dependencies array
*
* @param[out] Initial Dependencies Array
*
*/
void FMILoader::getInitialDependencies(std::vector<size_t> & initialDep) {

	initialDep.resize(m_initialDependencies.size());
	initialDep = m_initialDependencies;

}



/**
* @brief getter for initial start index array
*
* @param[out] Initial start Index Array
*
*/
void FMILoader::getStartIndexInitial(std::vector<size_t> & InitialStartIndex) {

	InitialStartIndex.resize(m_startIndexInitial.size());
	InitialStartIndex = m_startIndexInitial;


}


/**
* @brief get the ESO index to a variable 
*
* @param[out] Index of variable in ESO
*
* @param[in] Variable
*
*/
void FMILoader::getEsoVarIndex(size_t & index, fmi2_import_variable_t * variable) {


	// DEBUG
	// print variable name!
	//const char* name ;
	//name = fmi2_import_get_variable_name(variable); 
		//printf("Variable name: %s \n", name);
	index = -1;

	// variable is input
	if (fmi2_import_get_causality(variable) == fmi2_causality_enu_t::fmi2_causality_enu_input) {

		for (int indexInput = 0; indexInput < (int) m_numberInputVariables; indexInput++) {

			if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_inputVariableList, indexInput)) == fmi2_import_get_variable_vr(variable)) {

				index = m_numberAllVariables + indexInput;
				break;
			}

		}



	}
	else { // is a ESO state variable


		for (int indexOutput = 0; indexOutput < (int)m_numberOutputVariables; indexOutput++) {

			if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_outputVariableList, indexOutput)) == fmi2_import_get_variable_vr(variable)) {

				index = m_numberContinousStates + indexOutput;
				break;

			}
		}

		if (index == -1) {
			for (int indexStateCont = 0; indexStateCont < (int)m_numberContinousStates; indexStateCont++) {

				if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_stateList, indexStateCont)) == fmi2_import_get_variable_vr(variable)) {

					index = indexStateCont;
					break;

				}
			}
		}

		if (index == -1) { // if variable is derivative variable
			for (int indexStateCont = 0; indexStateCont < (int)m_numberContinousStates; indexStateCont++) {

				if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_derivativeList, indexStateCont)) == fmi2_import_get_variable_vr(variable)) {

					index = m_numberContinousStates + m_numberOutputVariables + m_numberInputVariables + indexStateCont;
					break;

				}
			}
		}

		if (index == -1) {
			// maybe write name into error message?
			printf("Could not find the variable in the FMU variable lists (getEsoVarIndex).\n");
			throw std::invalid_argument("Could not find the variable in the FMU variable lists (getEsoVarIndex).\n");
			exit(1);
		}
	}
		
}




void FMILoader::getNumberAllVariables(size_t & numberAllVariables) {

	numberAllVariables = m_numberAllVariables;

}


void FMILoader::getNumberInputs(size_t & numberInputs) {

	numberInputs = m_numberInputVariables;

}



/**
* @brief getESO index from FMU index
*
* @param[out] ESO index of variable
*
* @param[in] FMU inex of variable
*
*/
void FMILoader::getEsoIndexFromFMUIndex(size_t & EsoIndex, size_t & FmuIndex) {


	fmi2_import_variable_t * variable = fmi2_import_get_variable(m_variableList, FmuIndex-1);

	getEsoVarIndex(EsoIndex, variable);

}

/**
* @brief getESO index from initial FMU index
*
* @param[out] ESO index of variable
*
* @param[in] FMU inex of variable of initial variable list
*
*/
void FMILoader::getEsoIndexFromInitialFMUIndex(size_t & EsoIndex, size_t & FmuIndex) {


	fmi2_import_variable_t * variable = fmi2_import_get_variable(m_initialUnkownList, FmuIndex-1);

	getEsoVarIndex(EsoIndex, variable);

}





void FMILoader::getNumberInitialUnkowns(size_t & initUnkowns) {

	initUnkowns = m_numberInitialUnkowns;

}



void FMILoader::getEsoEquationIndexFromFMUIndex(size_t & esoEquationIndex, size_t & FmuIndex) {

	fmi2_import_variable_t * variable = fmi2_import_get_variable(m_variableList, FmuIndex - 1);

    esoEquationIndex = -1;


	for (int indexOutput = 0; indexOutput < (int)m_numberOutputVariables; indexOutput++) {
			// FmuIndex corresponds to output equation
			if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_outputVariableList, indexOutput)) == fmi2_import_get_variable_vr(variable)) {
				// in ESO differential equations are first
				esoEquationIndex = m_numberContinousStates + indexOutput;
				break;
			}
		}




		if (esoEquationIndex == -1) { // if variable is derivative variable
			for (int indexStateCont = 0; indexStateCont < (int)m_numberContinousStates; indexStateCont++) {
				// FmuIndex corresponds to differential equation
				if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_derivativeList, indexStateCont)) == fmi2_import_get_variable_vr(variable)) {
					esoEquationIndex = indexStateCont;
					break;

				}
			}
		}

		if (esoEquationIndex == -1) {
			printf("Could not find the variable in the FMU variable lists (getEsoEquationIndexFromFMUIndex).\n");
			throw std::invalid_argument("Could not find the variable in the FMU variable lists (getEsoEquationIndexFromFMUIndex).\n");
			exit(1);
		}
	}



/**
* @brief getESO index from FMU index of initial model fmu
*
* @param[out] ESO index of variable
*
* @param[in] FMU index of variable in initial model fmu
*
*/
void FMILoader::getEsoEquationIndexFromInitialFMUIndex(size_t & esoEquationIndex, size_t & FmuIndex) {

	fmi2_import_variable_t * variable = fmi2_import_get_variable(m_initialUnkownList, FmuIndex - 1);

	const char* name ;
	name = fmi2_import_get_variable_name(variable); 
		//printf("Variable name: %s \n", name);

    esoEquationIndex = -1;


	for (int indexOutput = 0; indexOutput < (int)m_numberOutputVariables; indexOutput++) {
			// FmuIndex corresponds to output equation
			if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_outputVariableList, indexOutput)) == fmi2_import_get_variable_vr(variable)) {
				// in ESO differential equations are first
				esoEquationIndex = m_numberContinousStates + indexOutput;
				break;
			}
		}




		if (esoEquationIndex == -1) { // if variable is derivative variable
			for (int indexStateCont = 0; indexStateCont < (int)m_numberContinousStates; indexStateCont++) {
				// FmuIndex corresponds to differential equation
				if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_derivativeList, indexStateCont)) == fmi2_import_get_variable_vr(variable)) {
					esoEquationIndex = indexStateCont;
					break;

				}
			}
		}



		if (esoEquationIndex == -1) { // if variable is derivative variable
			for (int indexStateCont = 0; indexStateCont < (int)m_numberContinousStates; indexStateCont++) {
				// FmuIndex corresponds to differential equation
				if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_stateList, indexStateCont)) == fmi2_import_get_variable_vr(variable)) {
							if (esoEquationIndex == -1) {
					printf("Variable is state variable. Wrong function call. There is no corresponding ESO equation. (getEsoEquationIndexFromInitialFMUIndex).\n");
					throw std::invalid_argument("Variable is state variable. Wrong function call. There is no corresponding ESO equation.(getEsoEquationIndexFromInitialFMUIndex).\n");
					exit(1);
		}

				}
			}
		}

		if (esoEquationIndex == -1) {
			printf("Could not find the variable in the FMU variable lists (getEsoEquationIndexFromInitialFMUIndex).\n");
			throw std::invalid_argument("Could not find the variable in the FMU variable lists (getEsoEquationIndexFromInitialFMUIndex).\n");
			exit(1);
		}
	}



/**
* @brief evaluates if variable is state variable from initial unkown list
*
* @param[out] true if variable is state, false otherwise
*
* @param[in] FMU index of variable in initial model fmu
*
*/
bool FMILoader::IndexVariableIsContinuousState(const size_t & FmuIndex) {

	fmi2_import_variable_t * variable = fmi2_import_get_variable(m_initialUnkownList, FmuIndex - 1);

			bool result=false;

			for (int indexStateCont = 0; indexStateCont < (int)m_numberContinousStates; indexStateCont++) {
				// FmuIndex corresponds to differential equation
				if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_stateList, indexStateCont)) == fmi2_import_get_variable_vr(variable)) {
					
					result = true;
					break;

				}
				
			}
		

			return result;
	}


/**
* @brief evaluates if variable is derivative variable from initial unkown list
*
* @param[out] true if variable is derivative, false otherwise
*
* @param[in] FMU index of variable in initial model fmu
*
*/
bool FMILoader::IndexVariableIsDerivative(const size_t & FmuIndex) {

	fmi2_import_variable_t * variable = fmi2_import_get_variable(m_initialUnkownList, FmuIndex - 1);

			bool result=false;

			for (int indexStateCont = 0; indexStateCont < (int)m_numberContinousStates; indexStateCont++) {
				// FmuIndex corresponds to differential equation
				if (fmi2_import_get_variable_vr(fmi2_import_get_variable(m_derivativeList, indexStateCont)) == fmi2_import_get_variable_vr(variable)) {
					
					result = true;
					break;

				}
				
			}
		

			return result;
	}


/** @copydoc GenericEso::getDerivativeValues(EsoIndex,EsoDouble*)
*/
void FMILoader::getDerivativeValues(const size_t n_diff_var, double *derivatives){

	fmistatus = fmi2_import_get_derivatives(m_fmu, derivatives, n_diff_var);


}



/**
* @brief Evaluation of the second-order tangent-linear over adjoint derivatives of the DA-System
*
* The function calls the second-order tangent-linear over adjoint derivative code generated by dcc
* for the residuals Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size 'Get_num_eqns()'.
* <.,.> represents the inner dot product with appropriate dimensions.
*
* output definition: a1_z = (d(Res(z))/dz)^T * a1_yy;
*                    t2_a1_z = <d^2(Res(z))/dzdz, a1_yy, t2_z> + <d(Res(z))/dz, t2_a1_yy>;
*
* @param[out] yy contains the residuals of the equation system
* @param[in] t2_yy is 0 on output is not used by dcc (@todo documentation by dcc)
* @param[in] a1_yy seed vector multiplying the Jacobian matrix to calculate a1_z and the Hessian
*            matrix to calculate t2_a1_z.
* @param[in] t2_a1_yy seed vector to define the jacobian part of t2_a1_z
* @param[in] der_x contains the values of der_x
* @param[in] t2_der_x array that defines the last n_x entries of t2_z
* @param[in,out] a1_der_x contains on output the sum of a1_der_x and the product of the transposed
*             Jacobian, relative to the derivated states, with a1_yy
* @param[in,out] t2_a1_der_x array that defines the last n_x entries of output array t2_a1_z
* @param[in] x contains the values of the states
* @param[in] t2_x array that defines the first n_x entries of t2_z
* @param[in,out] a1_x contains on output the sum of a1_x and product of the transposed Jacobian,
*             relative to the states, with a1_yy
* @param[in,out] t2_a1_x array that defines the first n_x entries of output array t2_a1_z
* @param[in] p contains the values of the parameters
* @param[in] t2_p array that defines the entries from index n_x til index
*            (n_x+n_p-1) of t2_z
* @param[in,out] a1_p contains on output the sum of a1_p and the product of the transposed Jacobian,
*             relative to the parameters, with a1_yy
* @param[in,out] t2_a1_p array that defines the entries from index n_x til index
*            (n_x+n_p-1) of output array t2_a1_z
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void FMILoader::T2_a1_res(double* yy, double* t2_yy, double* a1_yy, double* t2_a1_yy,
	double* der_x, double* t2_der_x, double* a1_der_x, double* t2_a1_der_x,
	double* x, double* t2_x, double* a1_x, double* t2_a1_x, double* p,
	double* t2_p, double* a1_p, double* t2_a1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c)
{

	throw std::runtime_error("Hessian is not implemented yet for fmu.");

	/*
	std::vector<std::vector<std::vector<double > > > m_hessian;
	std::vector<double> m_states_hessian;
	std::vector<double> m_inputs_hessian;
	vector<fmi2_real_t> m_derivatives_hessian;




	for (int i = 0; i < m_numberAllVariables; i++) {
		a1_x[i] = 0;
		t2_a1_x[i] = 0;
	}

	for (int i = 0; i < m_numberInputVariables; i++) {
		a1_p[i] = 0;
		t2_a1_p[i] = 0;
	}

	bool evaluateNew = false;

	for (int i = 0; i < m_numberAllVariables; i++) {

		if (m_states_hessian[i] != yy[i]) {
			evaluateNew = true; 
			break;
		}
		else m_states_hessian[i] = yy[i];
	}

	if (evaluateNew)
	for (int i = 0; i < m_numberInputVariables; i++) {

		if (m_inputs_hessian[i] != p[i]) {
			evaluateNew = true;
			break;
		}
		else m_inputs_hessian[i] = p[i];
	}

	if (evaluateNew)
	for (int i = 0; i < m_numberDerivatives; i++) {

		if (m_derivatives_hessian[i] != der_x[i]) {
			evaluateNew = true;
			break;
		}
		else m_derivatives_hessian[i] = der_x[i];
	}



	// seed vectors for jabocian
	std::vector<double>t1_der_x(n_x, 0);
	std::vector<double>t1_x(n_x, 0);
	std::vector<double>t1_p(n_p, 0);


	Res(yy, der_x, x,  p, condit,  lock,  prev,  n_x, n_p, n_c);
	std::vector<double>t1_yy(n_x);
	std::vector<double>t1_yy_dx(n_x);
	std::vector<double>x_dx(n_x);
	std::vector<double>p_dx(n_p);

	for (int i = 0; i < x_dx.size(); i++) {
		x_dx[i] = x[i];
	}
	for (int i = 0; i < p_dx.size(); i++) {
		p_dx[i] = p[i];
	}

	// 1. go over x = [diff. states, alg. states]^T
	for (int i = 0; i < n_x; i++) {

		

			t1_x[i] = 1;

			T1_res_without_res(t1_yy.data(), der_x, t1_der_x.data(), x,
				t1_x.data(), p, t1_p.data(),
				condit, lock, prev, n_x, n_p, n_c);

			// sum jacobian values up
			for (int j = 0; j < m_numberAllVariables; j++) {
				a1_x[i] = a1_x[i] + t1_yy[j] * a1_yy[j];
				t2_a1_x[i] = t2_a1_x[i] + t1_yy[j] * t2_a1_yy[j];
			}


			// hessian calculation
			// calculate finite difference dx
			// start from i because of the symmetry
			if (evaluateNew) {


				for (int j = i; j < m_numberAllVariables; j++) {
					x_dx[j] = x[j] + m_finiteDifference;



					T1_res_without_res(t1_yy_dx.data(), der_x, t1_der_x.data(), x_dx.data(),
						t1_x.data(), p, t1_p.data(),
						condit, lock, prev, n_x, n_p, n_c);


					for (int numRes = 0; numRes < m_numberAllVariables; numRes++) {
						m_hessian[numRes][i][j] = (t1_yy_dx[numRes] - t1_yy[numRes]) / (m_finiteDifference);
						// hessian is symmetric
						m_hessian[numRes][j][i] = m_hessian[numRes][i][j];
					}

					x_dx[j] = x[j];
				}



				for (int j = i; j < m_numberInputVariables; j++) {
					p_dx[j] = p[j] + m_finiteDifference;
					T1_res_without_res(t1_yy_dx.data(), der_x, t1_der_x.data(), x,
						t1_x.data(), p_dx.data(), t1_p.data(),
						condit, lock, prev, n_x, n_p, n_c);


					for (int numRes = 0; numRes < m_numberAllVariables; numRes++) {
						m_hessian[numRes][i][m_numberAllVariables + j] = (t1_yy_dx[numRes] - t1_yy[numRes]) / (m_finiteDifference);
						// hessian is symmetric
						m_hessian[numRes][m_numberAllVariables + j][i] = m_hessian[numRes][i][m_numberAllVariables + j];
					}

					p_dx[j] = p[j];
				}

			}
			// set seet to 0 again.
			t1_x[i] = 0;
		
	}

	// 2. go over p
	for (int i = 0; i < n_p; i++) {

		
			t1_p[i] = 1;

			T1_res_without_res(t1_yy.data(), der_x, t1_der_x.data(), x,
				t1_x.data(), p, t1_p.data(),
				condit, lock, prev, n_x, n_p, n_c);

			// sum jacobian vaues up
			for (int j = 0; j < m_numberAllVariables; j++) {
				a1_p[i] = a1_p[i] + t1_yy[j] * a1_yy[j];
				t2_a1_p[i] = t2_a1_p[i] + t1_yy[j] * t2_a1_yy[j];
			}


			// hessian calculation
			// calculate finite difference dx
			// start from i because of the symmetry
			if (evaluateNew) {
				for (int j = i; j < m_numberInputVariables; j++) {
					p_dx[j] = p[j] + m_finiteDifference;
					T1_res_without_res(t1_yy_dx.data(), der_x, t1_der_x.data(), x,
						t1_x.data(), p_dx.data(), t1_p.data(),
						condit, lock, prev, n_x, n_p, n_c);


					for (int numRes = 0; numRes < m_numberAllVariables; numRes++) {
						m_hessian[numRes][m_numberAllVariables + i][m_numberAllVariables + j] = (t1_yy_dx[numRes] - t1_yy[numRes]) / (m_finiteDifference);
						// hessian is symmetric
						m_hessian[numRes][m_numberAllVariables + j][m_numberAllVariables + i] = m_hessian[numRes][m_numberAllVariables + i][m_numberAllVariables + j];
					}

					p_dx[j] = p[j];
				}
			}

			t1_p[i] = 0;
		

	}


	// 3. go over der_x
	for (int i = 0; i < m_numberAllVariables; i++) {
		
			//t1_der_x[i] = 1;

			//T1_res_without_res(t1_yy.data(), der_x, t1_der_x.data(), x,
			//	t1_x.data(), p, t1_p.data(),
			//	condit, lock, prev, n_x, n_p, n_c);

			// sum jacobian vaues up
			//for (int j = 0; j < n_x; j++) {
				//a1_der_x[i] = a1_der_x[i] + t1_yy[i] * a1_yy[i];
				
			//}

			a1_der_x[i] = 1*a1_yy[m_numberInputVariables + m_numberAllVariables + i];


			//t1_der_x[i] = 0;
		
	}

	
	// output calculation

	for (int i = 0; i < m_numberAllVariables; i++) {
		t2_a1_der_x[i] = 0;
	}


	for (int k = 0; k < n_x; k++) {
		for (int i = 0; i < m_numberAllVariables; i++) {
			for (int j = 0; j < m_numberAllVariables; j++) {
				t2_a1_x[k] = t2_a1_x[k] + m_hessian[i][j][k] * a1_yy[i] * t2_x[j];
			}
			for (int j = 0; j < m_numberInputVariables; j++) {
				t2_a1_x[k] = t2_a1_x[k] + m_hessian[i][m_numberAllVariables+j][k] * a1_yy[i] * t2_p[j];
			}
		}
	}
	for (int k = 0; k < n_p; k++) {
		for (int i = 0; i < m_numberAllVariables; i++) {
			for (int j = 0; j <  m_numberAllVariables; j++) {
				t2_a1_p[k] = t2_a1_p[k] + m_hessian[i][j][n_x+k] * a1_yy[i] * t2_x[j];
			}
			for (int j = 0; j < m_numberInputVariables; j++) {
				t2_a1_p[k] = t2_a1_p[k] + m_hessian[i][m_numberAllVariables + j][n_x + k] * a1_yy[i] * t2_p[j];
			}
		}
	}

	*/

}

