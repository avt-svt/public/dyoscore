/**
* @file JadeLoader.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Member definitions of class JadeLoader                            \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 15.9.2011
*/

#include "DyosJadeLoader.hpp"
#include <iostream>
#include <cstdlib>

/**
* @brief loads a function from a dll
*
* @param[out] ptPtFunction pointer of a pointer to the function whose call is redirected to the
*            function of same type with name 'functionName' in the library 'library'
* @param[in] library is a library where the address of a function with name 'functionName' can be
*            retrieved
* @param[in] functionName of the function who should be addressed when the function where 'ptPtFunction'
*            points to is called
*/
/*
template<typename t_function> void myLoadFunction(t_function **ptPtFunction, HINSTANCE& library,
                                                  const char* functionName)
{
  *(void**) (ptPtFunction) = (GetProcAddress(library, functionName));
  const DWORD myError = GetLastError();
  if(myError){
    std::cerr << "Could not load function " << functionName << ", error code is: " << myError
      << std::endl;
    exit(1);
  }
  }*/

/**
* @brief constructor
*
* The constructor takes as argument the name (without the suffix ".dll") of a dynamic library
* created by Jade. The library contains all the necessary information about the dynamic model
* originally coded in Modelica. Furthermore all first-order derivatives in tangent-linear and
* adjoint mode as well as second-order derivatives in tangent over tangent and tangent over adjoint
* mode are available within this dynamic library corresponding to the Jade standard.
*
* @param[in] libname name of the dynamic model library to be loaded
*/
  JadeLoader::JadeLoader(const std::string& libname) :
    Loader(libname),
    m_Libname(libname)
  {
    if(!isLoaded()) {
      std::cerr << "Could not find " << m_Libname.c_str() << std::endl;
      std::cerr << "Opening dynamic link library failed.\n";
      exit(1);
    }

  m_Get_num_eqns = NULL;
  m_Get_num_vars = NULL;
  m_Get_num_pars = NULL;
  m_Get_num_cond = NULL;
  m_GetVarNames = NULL;
  m_GetParNames = NULL;
  m_Init = NULL;
  m_Res = NULL ;
  m_Res_block = NULL;
  m_Res_cond = NULL;
  m_Eval_cond = NULL;
  m_T1_res_block = NULL;
  m_A1_res = NULL;
  m_T1_res = NULL;
  m_T2_a1_res = NULL;
  m_Jsp_res = NULL;

  loadFcn(m_Get_num_eqns, "get_num_eqns");
  loadFcn(m_Get_num_vars, "get_num_vars");
  loadFcn(m_Get_num_pars, "get_num_pars");
  loadFcn(m_Get_num_cond, "get_num_cond");
  loadFcn(m_GetVarNames, "get_var_names");
  loadFcn(m_GetParNames, "get_par_names");
  loadFcn(m_Init, "init");
  loadFcn(m_Res, "res");
  loadFcn(m_Res_block, "res_block");
  loadFcn(m_Res_cond, "res_cond");
  loadFcn(m_Eval_cond, "eval_cond");
  loadFcn(m_T1_res_block, "t1_res_block");
  loadFcn(m_A1_res, "a1_res");
  loadFcn(m_T1_res, "t1_res");
  loadFcn(m_T2_a1_res, "t2_a1_res");
  loadFcn(m_Jsp_res, "jsp_res");
}

/**
* @brief destructor
*/
JadeLoader::~JadeLoader()
{
}

/**
* @brief Getter for the number of equations (differential and algebraic) of the model
*
* @return number of equations
*/
int JadeLoader::Get_num_eqns()
{
  int ne;
  m_Get_num_eqns(ne);
  return ne;
}

/**
* @brief Getter for the number of states (differential and algebraic) of the model
*
* @return number of states
*/
int JadeLoader::Get_num_vars()
{
  int nv;
  m_Get_num_vars(nv);
  return nv;
}

/**
* @brief Getter for the number of (time-invariant) parameters (not constants) of the model
*
* @return number of parameters
*/
int JadeLoader::Get_num_pars()
{
  int np;
  m_Get_num_pars(np);
  return np;
}

int JadeLoader::Get_num_cond()
{
  int nc;
  m_Get_num_cond(nc);
  return nc;
}

/**
* @brief Getter for the names of the states
*
* @param[out] names of all states ordered corresponding to the declaration in the .mof-file
*/
void JadeLoader::GetVarNames(std::vector<std::string> *names)
{
  m_GetVarNames(names);
}

/**
* @brief Getter for the names of the parameters
*
* @param[out] names of all parameters ordered corresponding to the declaration in the .mof-file
*/
void JadeLoader::GetParNames(std::vector<std::string> *names)
{
  m_GetParNames(names);
}

/**
* @brief Initializes all states and parameters
*
* The initial values are defined in the .mof-file or if not, default values (0) are given.
*
* @param[out] x initial values of the states of size n_x
* @param[out] p initial values of the parameters of size n_p
* @param[out] n_x size of the states x
* @param[out] n_p size of the parameters p
*/
void JadeLoader::Init(double * x, double * p, int &n_x, int& n_p, int& n_c)
{
  m_Init(x, p, n_x,n_p, n_c);
}

/**
* @brief Evaluates the residual of the whole equation system
*
* The function evaluates Res(x,p,der_x) = M*der_x - f(x,p).
*
* @param[out] yy of size Get_num_eqns() contains the residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void JadeLoader::Res(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c)
{
  m_Res(yy,der_x,x, p, condit, lock, prev, n_x, n_p, n_c);
}

void JadeLoader::Res_cond(double* yy, double* der_x, double* x, double* p, int& n_x, int& n_p, int& n_c)
{
  m_Res_cond(yy, der_x, x, p, n_x, n_p, n_c);
}

void JadeLoader::Eval_cond(int* yy, double* der_x, double* x, double* p, int& n_x, int& n_p, int& n_c)
{
  m_Eval_cond(yy, der_x, x, p, n_x, n_p, n_c);
}

/**
* @brief Block-wise evaluation of the residuals
*
* The function evaluates blocks of the residual Res(x,p,der_x).
*
* @param[out] yy of size n_yy_ind contains the residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
* @param[in] n_yy_ind size of residuals yy and indices yy_ind
* @param[in] yy_ind indices of residuals to be evaluated
*/
void JadeLoader::Res_block(double * yy, double * der_x, double * x, double * p,
			      int* condit, int* lock, int* prev,
			      int &n_x,int &n_p, int &n_c, int &n_yy_ind, int* yy_ind)
{
  m_Res_block(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c, n_yy_ind, yy_ind);
}

/**
* @brief Block-wise evaluation of the first-order tangent-linear derivatives of the residuals
*
* The function calls the first-order tangent-linear derivative code generated by dcc for the
* residuals Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size n_yy_ind.
*
* output definition: t1_yy = d(Res(z))/dz * t1_z
*
* @param[out] yy contains the block-residuals of the equation system
* @param[out] t1_yy contains the derivatives of the block-residuals of the equation system
* @param[in] der_x contains the values of der_x
* @param[in] t1_der_x seed vector multiplying the Jacobian part relative to the derivated
*            states
* @param[in] x contains the values of the states
* @param[in] t1_x seed vector multiplying the Jacobian part relative to the states
* @param[in] p contains the values of the parameters
* @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
* @param[in] n_yy_ind size of residuals yy and indices yy_ind
* @param[in] yy_ind indices of residuals to be evaluated
*/
void JadeLoader::T1_res_block(double* yy, double* t1_yy, double* der_x, double* t1_der_x,
                                 double* x, double* t1_x, double* p, double* t1_p,
				 int* condit, int* lock, int* prev,
				 int& n_x, int& n_p, int& n_c, int& n_yy_ind, int* yy_ind)
{
  m_T1_res_block(yy, t1_yy, der_x, t1_der_x, x, t1_x, p, t1_p, condit, lock, prev, n_x, n_p, n_c, n_yy_ind, yy_ind);
}

/**
* @brief Evaluation of the first-order adjoint derivatives of the residuals
*
* The function calls the first-order adjoint derivative code generated by dcc for the residuals
* Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size 'Get_num_eqns()'.
*
* output definition: a1_z = a1_z + (d(Res(z))/dz)^T * a1_yy
*
* @param[in] bmode_1 has to be 1 on entry (@todo documentation by dcc)
* @param[out] yy contains the residuals of the equation system
* @param[in] a1_yy seed vector multiplying the transposed Jacobian
* @param[in] der_x contains the values of der_x
* @param[out] a1_der_x contains on output the sum of a1_der_x and the product of the transposed
*             Jacobian, relative to the derivated states, with a1_yy
* @param[in] x contains the values of the states
* @param[out] a1_x contains on output the sum of a1_x and the product of the transposed Jacobian,
*             relative to the states, with a1_yy
* @param[in] p contains the values of the parameters
* @param[out] a1_p contains on output the sum of a1_p and the product of the transposed Jacobian,
*             relative to the parameters, with a1_yy
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void JadeLoader::A1_res(int bmode_1, double* yy, double* a1_yy, double* der_x, double* a1_der_x,
                           double* x, double* a1_x, double* p, double* a1_p,
			   int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c)
{
  m_A1_res(bmode_1,  yy, a1_yy, der_x, a1_der_x,  x, a1_x,  p,  a1_p, condit, lock, prev, n_x, n_p, n_c);
}

/**
* @brief Evaluation of the first-order tangent-linear derivatives of the residuals
*
* The function calls the first-order tangent-linear derivative code generated by dcc for the
* residuals Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size 'Get_num_eqns()'.
*
* output definition: t1_yy = d(Res(z))/dz * t1_z
*
* @param[out] yy contains the residuals of the equation system
* @param[out] t1_yy contains the derivatives of the residuals of the equation system
* @param[in] der_x contains the values of der_x
* @param[in] t1_der_x seed vector multiplying the Jacobian part relative to the derivated
*            states
* @param[in] x contains the values of the states
* @param[in] t1_x seed vector multiplying the Jacobian part relative to the states
* @param[in] p contains the values of the parameters
* @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void JadeLoader::T1_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
                           double* t1_x, double* p, double* t1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c)
{
  m_T1_res(yy,t1_yy, der_x, t1_der_x, x, t1_x,  p,  t1_p, condit, lock, prev, n_x, n_p, n_c);
}

/**
* @brief Evaluation of the second-order tangent-linear over adjoint derivatives of the DA-System
*
* The function calls the second-order tangent-linear over adjoint derivative code generated by dcc
* for the residuals Res(x,p,der_x).
*
* All arrays with suffix 'x' or 'der_x' are of size n_x, all arrays with suffix 'p'
* are of size n_p and all arrays with suffix 'yy' are of size 'Get_num_eqns()'.
* <.,.> represents the inner dot product with appropriate dimensions.
*
* output definition: a1_z = a1_z + (d(Res(z))/dz)^T * a1_yy;
*                    t2_a1_z = t2_a1_z + <d^2(Res(z))/dzdz, a1_yy, t2_z> + <d(Res(z))/dz, t2_a1_yy>;
*
* @param[in] bmode_1 has to be 1 on entry (@todo documentation by dcc)
* @param[out] yy contains the residuals of the equation system
* @param[in] t2_yy is 0 on output is not used by dcc (@todo documentation by dcc)
* @param[in] a1_yy seed vector multiplying the Jacobian matrix to calculate a1_z and the Hessian
*            matrix to calculate t2_a1_z.
* @param[in] t2_a1_yy seed vector to define the jacobian part of t2_a1_z
* @param[in] der_x contains the values of der_x
* @param[in] t2_der_x array that defines the last n_x entries of t2_z
* @param[in,out] a1_der_x contains on output the sum of a1_der_x and the product of the transposed
*             Jacobian, relative to the derivated states, with a1_yy
* @param[in,out] t2_a1_der_x array that defines the last n_x entries of output array t2_a1_z
* @param[in] x contains the values of the states
* @param[in] t2_x array that defines the first n_x entries of t2_z
* @param[in,out] a1_x contains on output the sum of a1_x and product of the transposed Jacobian,
*             relative to the states, with a1_yy
* @param[in,out] t2_a1_x array that defines the first n_x entries of output array t2_a1_z
* @param[in] p contains the values of the parameters
* @param[in] t2_p array that defines the entries from index n_x til index
*            (n_x+n_p-1) of t2_z
* @param[in,out] a1_p contains on output the sum of a1_p and the product of the transposed Jacobian,
*             relative to the parameters, with a1_yy
* @param[in,out] t2_a1_p array that defines the entries from index n_x til index
*            (n_x+n_p-1) of output array t2_a1_z
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void JadeLoader::T2_a1_res(int bmode_1, double* yy, double* t2_yy, double* a1_yy, double* t2_a1_yy,
                              double* der_x, double* t2_der_x, double* a1_der_x, double* t2_a1_der_x,
                              double* x, double* t2_x, double* a1_x, double* t2_a1_x, double* p,
                              double* t2_p, double* a1_p, double* t2_a1_p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c)
{
  m_T2_a1_res(bmode_1,yy,t2_yy,a1_yy,t2_a1_yy,der_x,t2_der_x, a1_der_x, t2_a1_der_x, x, t2_x, a1_x, t2_a1_x, p, t2_p, a1_p, t2_a1_p, condit, lock, prev, n_x, n_p, n_c);
}

/**
* @brief Evaluates the residual of the whole equation system with data type double overloaded by jsp
*
* The function evaluates Res(x,p,der_x) = M*der_x - f(x,p).
*
* @param[out] yy of size Get_num_eqns() contains the residuals of the equation system
* @param[in] der_x of size n_x contains the values of der_x
* @param[in] x of size n_x contains the values of the states
* @param[in] p of size n_p contains the values of the parameters
* @param[in] n_x size of the states x
* @param[in] n_p size of the parameters p
*/
void JadeLoader::Jsp_res(jsp* yy, jsp* der_x, jsp* x, jsp* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c)
{
  m_Jsp_res(yy, der_x, x,  p, condit, lock, prev, n_x, n_p, n_c);
}
