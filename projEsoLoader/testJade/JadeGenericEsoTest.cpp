/** 
* @file JadeGenericEsoTest.cpp
*
* @author Moritz Schmitz
* @date 20.9.2011
*/
#include "GenericEso.hpp"
#include "JadeGenericEso.hpp"

#define BOOST_TEST_MODULE JadeGenericEso 
#include "boost/test/unit_test.hpp"
#include <boost/test/floating_point_comparison.hpp>
#include <vector>

#include "Array.hpp"

#define TEST_Jade_MODEL_NAME "carFreeFinalTime"

#define THRESHOLD 1e-13

using namespace std;

BOOST_AUTO_TEST_SUITE(TestJadeGenericEso)

/**
* @test JadeGenericEsoTest - test if JadeGenericEso is working the same way as Jade_Eso
*
* @author Moritz Schmitz
* @date 20.9.2011
*/
BOOST_AUTO_TEST_CASE(TestInitializationJadeGenericEso)
{
  try{
	BOOST_CHECK_NO_THROW(JadeGenericEso(TEST_Jade_MODEL_NAME));
  }
  catch(exception &e)
  {
    BOOST_ERROR(e.what());
  }
  catch(...)
  {
    BOOST_ERROR("Unexpected exception was thrown by JadeGenericEso object");
  }
}

/**
* @test  GenericEsoTest - test all residual funcions
*/
BOOST_AUTO_TEST_CASE(TestGetResiduals)
{
  try{
    JadeGenericEso JadeGenericEso(TEST_Jade_MODEL_NAME);
    unsigned numAlgebraicEquations=0;
    unsigned numDifferentialEquations=3;
    unsigned numEquations=numAlgebraicEquations+numDifferentialEquations;
  

    utils::Array<double> residuals(numEquations);
    std::vector<double> compare(numEquations);

    compare[0]=0;
	  compare[1]=-1;
	  compare[2]=-2;
    JadeGenericEso.getAllResiduals(residuals);

    BOOST_REQUIRE_EQUAL(residuals.getSize(), numEquations);
    for(unsigned i=0;i<residuals.getSize(); i++){
      BOOST_CHECK_CLOSE(residuals[i], compare[i], THRESHOLD);
    }
    
    utils::Array<double> differentialResiduals(numDifferentialEquations);
    JadeGenericEso.getDifferentialResiduals(differentialResiduals);
    BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
    for(unsigned i=0;i<differentialResiduals.getSize(); i++){
      BOOST_CHECK_CLOSE(differentialResiduals[i], compare[i], THRESHOLD);
    }

    utils::Array<double> algebraicResiduals(numAlgebraicEquations);
    JadeGenericEso.getAlgebraicResiduals(algebraicResiduals);
    compare.erase(compare.begin(), compare.begin()+numDifferentialEquations);
    BOOST_REQUIRE_EQUAL(algebraicResiduals.getSize(), numAlgebraicEquations);
    for(unsigned i=0;i<algebraicResiduals.getSize(); i++){
      BOOST_CHECK_CLOSE(algebraicResiduals[i], compare[i], THRESHOLD);
    }

    
  }
  catch(exception &e)
  {
    BOOST_ERROR(e.what());
  }
  catch(...)
  {
    BOOST_ERROR("Unexpected exception was thrown by JadeGenericEso object");
  }

}

/**
* @test GenericEsoTest - test all variable functions
*/
BOOST_AUTO_TEST_CASE(TestVariablesESO)
{
  try{
    JadeGenericEso JadeGenericEso(TEST_Jade_MODEL_NAME);
    unsigned numAlgebraicVariables = 0;
    unsigned numDifferentialVariables = 3;
    unsigned numAssignedVariables = 2;
    unsigned numVariables = numAssignedVariables + numDifferentialVariables + numAlgebraicVariables;
    double defaultValue=0.0;

    BOOST_CHECK_EQUAL(numVariables, JadeGenericEso.getNumVariables());
    BOOST_CHECK_EQUAL(numAlgebraicVariables, JadeGenericEso.getNumAlgebraicVariables());
    BOOST_CHECK_EQUAL(numDifferentialVariables, JadeGenericEso.getNumDifferentialVariables());
    BOOST_CHECK_EQUAL(numAssignedVariables, JadeGenericEso.getNumAssignedVariables());

    utils::Array<double> differentialVals(numDifferentialVariables);
    vector<double> compareDiff(numDifferentialVariables);

    compareDiff[0]= defaultValue;
    compareDiff[1]= defaultValue;
    compareDiff[2]= defaultValue;

    JadeGenericEso.getDifferentialVariableValues(differentialVals);

    BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
    for(unsigned i=0;i<differentialVals.getSize(); i++){
      BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], THRESHOLD);
    }
    
    utils::Array<double> assignedVals(numAssignedVariables);
    vector<double> compareAssign(numAssignedVariables);
    compareAssign[0] = 2.0;
    compareAssign[1] = 0.0025;

    JadeGenericEso.getAssignedVariableValues(assignedVals);
    
    BOOST_REQUIRE_EQUAL(assignedVals.getSize(), numAssignedVariables);
    for(unsigned i=0;i<assignedVals.getSize(); i++){
      BOOST_CHECK_CLOSE(assignedVals[i], compareAssign[i], THRESHOLD);
    }
    
    //utils::Array<double> algebraicVals(numAlgebraicVariables);
    //vector<double> compareAlgeb(numAlgebraicVariables);
    //compareAlgeb[0] = defaultValue;
    //compareAlgeb[1] = defaultValue;

    //JadeGenericEso.getAlgebraicVariableValues(algebraicVals);
    //
    //BOOST_REQUIRE_EQUAL(algebraicVals.getSize(), numAlgebraicVariables);
    //for(unsigned i=0;i<algebraicVals.getSize(); i++){
    //  BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], THRESHOLD);
    //}


    utils::Array<double> allVariables(numVariables);
    vector<double> compareVariables(numVariables);

    compareVariables[0]=defaultValue;
    compareVariables[1]=defaultValue;
    compareVariables[2]=defaultValue;
    compareVariables[3]=2.0;
    compareVariables[4]=0.0025;

    JadeGenericEso.getAllVariableValues(allVariables);

    BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
    for(unsigned i=0; i<numVariables; i++){
      BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], THRESHOLD);
    }
    
}
  catch(exception &e)
  {
    BOOST_ERROR(e.what());
  }
  catch(...)
  {
    BOOST_ERROR("Unexpected exception was thrown by JadeGenericEso object");
  }

}

/**
* @test GenericEsoTest - set variables and test all variable functions and check for correct residuals
*/
BOOST_AUTO_TEST_CASE(TestSetVariablesESO)
{
  try{
    JadeGenericEso JadeGenericEso(TEST_Jade_MODEL_NAME);
    unsigned numAlgebraicVariables = 0;
    unsigned numDifferentialVariables = 3;
    unsigned numAssignedVariables = 2;
    unsigned numVariables = numAssignedVariables + numDifferentialVariables + numAlgebraicVariables;
    double defaultValue=1.0;

    BOOST_CHECK_EQUAL(numVariables, JadeGenericEso.getNumVariables());
    BOOST_CHECK_EQUAL(numAlgebraicVariables, JadeGenericEso.getNumAlgebraicVariables());
    BOOST_CHECK_EQUAL(numDifferentialVariables, JadeGenericEso.getNumDifferentialVariables());
    BOOST_CHECK_EQUAL(numAssignedVariables, JadeGenericEso.getNumAssignedVariables());

    utils::Array<double> differentialVals(numDifferentialVariables);
    utils::Array<double> compareDiff(numDifferentialVariables);

    compareDiff[0]= defaultValue*2;
    compareDiff[1]= defaultValue*3;
    compareDiff[2]= defaultValue*4;

    JadeGenericEso.setDifferentialVariableValues(compareDiff);
    JadeGenericEso.getDifferentialVariableValues(differentialVals);

    BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
    for(unsigned i=0;i<differentialVals.getSize(); i++){
      BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], THRESHOLD);
    }
    
    utils::Array<double> assignedVals(numAssignedVariables);
    utils::Array<double> compareAssign(numAssignedVariables);
    compareAssign[0] = 1.3*3;
    compareAssign[1] = 3.7*8;
    JadeGenericEso.setAssignedVariableValues(compareAssign);
    JadeGenericEso.getAssignedVariableValues(assignedVals);
    
    BOOST_REQUIRE_EQUAL(assignedVals.getSize(), numAssignedVariables);
    for(unsigned i=0;i<assignedVals.getSize(); i++){
      BOOST_CHECK_CLOSE(assignedVals[i], compareAssign[i], THRESHOLD);
    }
    
    //utils::Array<double> algebraicVals(numAlgebraicVariables);
    //utils::Array<double> compareAlgeb(numAlgebraicVariables);
    //compareAlgeb[0] = defaultValue+15;
    //compareAlgeb[1] = defaultValue-10;
    //JadeGenericEso.setAlgebraicVariableValues(compareAlgeb);
    //JadeGenericEso.getAlgebraicVariableValues(algebraicVals);
    //
    //BOOST_REQUIRE_EQUAL(algebraicVals.getSize(), numAlgebraicVariables);
    //for(unsigned i=0;i<algebraicVals.getSize(); i++){
    //  BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], THRESHOLD);
    //}


    utils::Array<double> allVariables(numVariables);
    utils::Array<double> compareVariables(numVariables);

    compareVariables[0]=defaultValue+1;
    compareVariables[1]=defaultValue*2;
    compareVariables[2]=defaultValue+3;
    compareVariables[3]=1.3+7;
    compareVariables[4]=3.7*8;

    JadeGenericEso.setAllVariableValues(compareVariables);
    JadeGenericEso.getAllVariableValues(allVariables);

    vector<string> variableNames(numVariables);
    JadeGenericEso.getVariableNames(variableNames);

    BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
    for(unsigned i=0; i<numVariables; i++){
      BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], THRESHOLD);
    }
    unsigned numEqn = JadeGenericEso.getNumEquations();
    utils::Array<double> residuals(numEqn);
    JadeGenericEso.getAllResiduals(residuals);

    utils::Array<double> compareResiduals(numEqn);

    compareResiduals[0] = -2;
    compareResiduals[1] = -1;
    compareResiduals[2] = 110.1;

    BOOST_REQUIRE_EQUAL(residuals.getSize(), numEqn);
    for(unsigned i=0;i<numEqn; i++){
      BOOST_CHECK_CLOSE(residuals[i], compareResiduals[i], THRESHOLD);
    }

}
  catch(exception &e)
  {
    BOOST_ERROR(e.what());
  }
  catch(...)
  {
    BOOST_ERROR("Unexpected exception was thrown by JadeGenericEso object");
  }

}

/**
* @test GenericEsoTest - check if Jacobian and differential Jacobian struct information is correct
*/
BOOST_AUTO_TEST_CASE(testJacobian)
{
  JadeGenericEso JadeGenericEso(TEST_Jade_MODEL_NAME);
  unsigned numJacobianEntries = 4;
  unsigned numDiffJacobianEntries = 3;

  BOOST_CHECK_EQUAL(numJacobianEntries, JadeGenericEso.getNumNonZeroes());
  BOOST_CHECK_EQUAL(numDiffJacobianEntries, JadeGenericEso.getNumDifferentialNonZeroes());

  vector<int> compareRowIndices(numJacobianEntries);
  compareRowIndices[0] = 0;
  compareRowIndices[1] = 2;
  compareRowIndices[2] = 2;
  compareRowIndices[3] = 2;

  vector<int> compareColIndices(numJacobianEntries);
  compareColIndices[0] = 1;
  compareColIndices[1] = 1;
  compareColIndices[2] = 3;
  compareColIndices[3] = 4;

  vector<int> rowIndices, colIndices;
  JadeGenericEso.getJacobianStruct(rowIndices, colIndices);

  BOOST_CHECK_EQUAL_COLLECTIONS(rowIndices.begin(), rowIndices.end(), compareRowIndices.begin(), compareRowIndices.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(colIndices.begin(), colIndices.end(), compareColIndices.begin(), compareColIndices.end());

}
BOOST_AUTO_TEST_CASE(testDiffJacobianStruct)
{
  JadeGenericEso JadeGenericEso(TEST_Jade_MODEL_NAME);
  unsigned numDiffJacobianEntries = 3;
  std::vector<int> rowIndices(numDiffJacobianEntries);
  std::vector<int> colIndices(numDiffJacobianEntries);
  JadeGenericEso.getDiffJacobianStruct(rowIndices, colIndices);

  vector<int> compareRowIndices(numDiffJacobianEntries);
  compareRowIndices[0] = 0;
  compareRowIndices[1] = 1;
  compareRowIndices[2] = 2;

  vector<int> compareColIndices(numDiffJacobianEntries);
  compareColIndices[0] = 2;
  compareColIndices[1] = 0;
  compareColIndices[2] = 1;

  BOOST_CHECK_EQUAL_COLLECTIONS(rowIndices.begin(), rowIndices.end(), compareRowIndices.begin(), compareRowIndices.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(colIndices.begin(), colIndices.end(), compareColIndices.begin(), compareColIndices.end());
}

BOOST_AUTO_TEST_CASE(testBounds)
{
  JadeGenericEso JadeGenericEso(TEST_Jade_MODEL_NAME);
  std::vector<double> lowerBounds(JadeGenericEso.getNumVariables());
  std::vector<double> upperBounds(JadeGenericEso.getNumVariables());
  JadeGenericEso.getAllBounds(lowerBounds, upperBounds);
}

BOOST_AUTO_TEST_CASE(testIndependentVariable)
{
  double d=3;
  JadeGenericEso JadeGenericEso(TEST_Jade_MODEL_NAME);
  JadeGenericEso.setIndependentVariable(d);
  BOOST_CHECK_EQUAL(d, JadeGenericEso.getIndependentVariable());

}

BOOST_AUTO_TEST_SUITE_END()