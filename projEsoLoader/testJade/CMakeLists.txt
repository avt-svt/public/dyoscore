set (TEST_Jade_GENERIC_ESO_SRC JadeGenericEsoTest.cpp)


add_executable(TestJadeGenericEso ${TEST_Jade_GENERIC_ESO_SRC})

target_link_libraries(TestJadeGenericEso
	CppUtils
	GenericEso
	)
	
add_dependencies(TestJadeGenericEso CppUtils)
set_target_properties(TestJadeGenericEso PROPERTIES FOLDER Tests)


	
